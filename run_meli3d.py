import os
import argparse
from argparse import Namespace
import glob
import time
import subprocess
import logging
import shutil
from meli3d import chemical_vae
from meli3d import vina_dock
from meli3d import interaction_similarity
from meli3d import RC_similarity
from meli3d import Homology_modeling

def split_substrates(input_smiles, sub_smiles):
    substrate_list = []
    if sub_smiles == 'all_substrates':
        substrates = input_smiles.split('>>')[0].strip()
        for each_substrate in substrates.split('.'):
            substrate_list.append(each_substrate)
    else:
        substrates = input_smiles.split('>>')[0].strip()
        for each_substrate in substrates.split('.'):
            if each_substrate == sub_smiles:
                substrate_list.append(each_substrate)
    return substrate_list
def retrieve_enzyme_homology_modeling(output_dir, interaction_sim_cutoff):
    hm_candidates = []
    with open(output_dir+'/Interaction_similarity.txt', 'r') as fr:
        for line in fr:
            tok = line.split('\t')
            pdb = tok[0].split('_')[0]
            lig = tok[0].split('_')[1]
            interaction_sim = tok[1]
            if float(interaction_sim) > interaction_sim_cutoff:
                hm_candidates.append(pdb+'_'+lig)
    hm_candidates = list(set(hm_candidates))
    return hm_candidates
def retrieve_enzyme_sequence(output_dir, interaction_sim_cutoff):
    fw = open(output_dir+'/MELI3D_enzyme_output.txt', 'w')
    fw.write('PDB_ID\tChain\tResolution\tEC_number\tLigand_ID\tCatalytic_sites\tPubmed_ID\tUniprot_ID\tEnzyme_sequence_from_structure\n')
    with open(output_dir+'/Interaction_similarity.txt', 'r') as fr:
        for line in fr:
            tok = line.split('\t')
            q_pdb = tok[0].split('_')[0]
            q_lig = tok[0].split('_')[1]
            inter_sim = tok[1]
            if float(inter_sim) > interaction_sim_cutoff:
                with open('./data/BioLiP_updated_set/BioLiP_UP.out') as fr_pdb:
                    for pdb_line in fr_pdb:
                        p_tok = pdb_line.split('\t')
                        pdb = p_tok[0].strip()
                        lig = p_tok[4].strip()
                        if q_pdb == pdb and q_lig == lig:
                            chain = p_tok[1]
                            resolution = p_tok[2]
                            catal_site = p_tok[10]
                            EC_num = p_tok[11]
                            pubmed_id = p_tok[19]
                            uniprot_id = p_tok[18]
                            enzyme_seq = p_tok[19]
                            fw.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n' %(pdb, chain, resolution, EC_num, lig, catal_site, pubmed_id, uniprot_id, enzyme_seq))
    fw.close()
    return
def retrieve_enzyme_sequence_hm(output_dir, matching_file, inter_file, blast_file, interaction_sim_cutoff):
    orgn_list = []
    with open(inter_file) as fr_inter:
        for line in fr_inter:
            tok = line.split('\t')
            pdb_tok = tok[0].split('_')
            pdb_num = '%s_%s' %(pdb_tok[0], pdb_tok[1])
            inter_sim = tok[1]
            if float(inter_sim) > interaction_sim_cutoff:
                orgn_list.append(pdb_num)
    list(set(orgn_list))
    orgn_genus = []
    with open(matching_file) as fr_mat:
        for line in fr_mat:
            orgn_full = line.split('\t')[0]
            pdb_id = line.split('\t')[1].strip()
            if pdb_id in orgn_list:
                orgn_tok = orgn_full.split('_')
                orgn_id = orgn_tok[0]
                orgn_genus.append(orgn_id)
    list(set(orgn_genus))
    fw = open(output_dir+'MELI3D_enzyme_hm_output.txt', 'w')
    fw.write('Organism\tEnzyme\tPDB_ID\tGI_number\tSeq_Identity\tEnzyme_sequence_from_blast\n')
    with open(blast_file) as fr_blast:
        for line in fr_blast:
            tok = line.split('\t')
            orgn = tok[1]
            genus = orgn.split()[0]
            if genus in orgn_genus:
                enzyme = tok[2]
                pdb = tok[3]
                gi_num = tok[0]
                seq_iden = tok[5]
                seq = tok[7]
                fw.write('%s\t%s\t%s\t%s\t%s\t%s\n' %(orgn, enzyme, pdb, gi_num, seq_iden, seq))
    fw.close()
    return

def main():
    start = time.time()
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', dest='input', default='input', help="SMILES describing the reactions e.g.) reactant_smiles>>product_smiles")
    parser.add_argument('-o', '--output', dest='output', default='output', help="Gene sequences having potential catalytic activity")
    parser.add_argument('-m', '--homology-modeling', dest='hm_generation', action='store_true', default=False, help="Run homology modeling")
    parser.add_argument('-s', '--substrate', dest='substrate', default='all_substrates', help="define the smiles of target substrate")

    #######################################################################
    ## Path variables for third-party programs (mgltools, RDT, and PLIP) ##
    ## and data sets (BioLiP and in-house reference data)                ##
    #######################################################################
    MGLTools_DIR = '/data1/user_home/shuanchen/anaconda3/pkgs/mgltools-1.5.6-1/'
    MGLToolsPkg_DIR = MGLTools_DIR + 'MGLToolsPckgs/AutoDockTools/Utilities24/'
    MGLTools_PYTHON_DIR = MGLTools_DIR + 'bin/pythonsh'
    BIOLIP_DIR = './data/BioLiP_updated_set/'
    reference_DB_RDT = './data/RDT_txt_results_20181206_substrates/'
    RDT_path = './bin/RDT1.5.1.jar'
    PLIP_path = './bin/plip-stable/plip/plipcmd'

    ###########################################
    ## Cut-off parameters                    ##
    ###########################################
    rc_sim_cutoff = 0.1
    structure_sim_cutoff = 0.9
    interaction_sim_cutoff = 0.5
    hm_interaction_sim_cutoff = 0.7

    ###########################################
    ## AutoDock Vina and Modeller parameters ##
    ###########################################
    vina_mode_num = 15
    vina_energy_range = 4
    vina_exhaustiveness = 12
    vina_cpu_num = 4
    vina_grid_point = 5
    modeller_model_num = 5
    modeller_blast_hit_size = 300
    modeller_parsing_orgn_max_num = 100
    modeller_parsing_seq_identity = 0.3

    logging.info(BIOLIP_DIR)
    logging.info(MGLTools_DIR)

    options = parser.parse_args()
    input_reaction = options.input
    output_dir = options.output
    homology_model = options.hm_generation
    sub_smiles = options.substrate

    if MGLTools_DIR[-1] == '/':
        pass
    else:
        MGLTools_DIR = MGLTools_DIR+'/'

    try:
        shutil.rmtree(output_dir)
    except:
        pass
    os.mkdir(output_dir)

    RC_sim_results = RC_similarity.run_RC_sim(input_reaction, output_dir, reference_DB_RDT, RDT_path, rc_sim_cutoff)
    struc_sim_results = chemical_vae.run_chemvae_similarity(input_reaction, structure_sim_cutoff)
    RC_set = set(RC_sim_results.keys())
    struc_set = set(struc_sim_results.keys())
    candidates = list(RC_set | struc_set)
    logging.info(candidates)

    with open(output_dir+os.sep+'RC_similarity.txt', 'w') as fp:
        for rc_sim in RC_sim_results.keys():
            fp.write('%s\t%s\n' %(rc_sim, RC_sim_results[rc_sim]))
    with open(output_dir+os.sep+'Structure_similarity.txt', 'w') as fp:
        for struc_sim in struc_sim_results.keys():
            fp.write('%s\t%s\n' %(struc_sim, struc_sim_results[struc_sim]))
    with open(output_dir+os.sep+'Input_reaction_smiles.txt', 'w') as fp:
        fp.write('%s\n' %(input_reaction))

    original_docking_output_dir = output_dir+'/Docking_results/'
    os.mkdir(original_docking_output_dir)

    plip_output_dir = output_dir+'/PLIP_results/'
    os.mkdir(plip_output_dir)

    substrate_list = split_substrates(input_reaction, sub_smiles)
    substrate_id_cnt = 1
    ##### Start docking simulations #####
    for each_smiles in substrate_list:
        substrate_id = 'SUBSTRATE_%s'%(substrate_id_cnt)
        substrate_id_cnt += 1

        docking_substrate_output_dir = original_docking_output_dir+'/%s/'%(substrate_id)
        os.mkdir(docking_substrate_output_dir)

        with open(docking_substrate_output_dir+'substrate.smi', 'w') as fp:
            fp.write('%s\n'%(each_smiles))

        fp_results = open(output_dir+'/Interaction_similarity.txt', 'a')

        file_cnt = 1
        for each_result in candidates:
            docking_output_dir = docking_substrate_output_dir
            smiles = each_smiles
            pdb_id = each_result
            file_id = pdb_id+'_%s'%file_cnt
            file_cnt += 1

            ligand_file, receptor_file, prot_chain = vina_dock.search_db_ligand_file(pdb_id, BIOLIP_DIR)
            if ligand_file != False and receptor_file != False:
                logging.info('Substrate: %s, File: %s (%s), PDB ID: %s'%(each_smiles, file_id, len(candidates), pdb_id))

                docking_output_dir = docking_output_dir+'/%s/'%(file_id)
                os.mkdir(docking_output_dir)

                prot_chain = vina_dock.run_vina_docking(file_id, smiles, pdb_id, docking_output_dir, BIOLIP_DIR, MGLToolsPkg_DIR, MGLTools_PYTHON_DIR, vina_mode_num, vina_energy_range, vina_exhaustiveness, vina_cpu_num, vina_grid_point)
                docking_pdbqt_file = docking_output_dir+'/Docking_result_%s'%file_id
                natural_ligand_file = docking_output_dir+'/natural_ligand_%s.pdbqt'%file_id

                cryst_plip_folder = interaction_similarity.run_PLIP_crystal(prot_chain, pdb_id, natural_ligand_file, plip_output_dir, substrate_id, file_id, BIOLIP_DIR, PLIP_path)
                docking_plip_folder = interaction_similarity.run_PLIP_pdbqt(prot_chain, pdb_id, docking_pdbqt_file, plip_output_dir, substrate_id, file_id, BIOLIP_DIR, PLIP_path)

                interaction_sim_info = interaction_similarity.calculate_interaction_similarity(prot_chain, pdb_id, cryst_plip_folder, docking_plip_folder)
                for k in interaction_sim_info.keys():
                    fp_results.write(str(k) + '\t' + str(interaction_sim_info[k]) + '\n')
            else:
                docking_output_dir = docking_output_dir+'/%s_ERROR/'%(file_id)
                os.mkdir(docking_output_dir)

                logging.info("Cannot find %s PDB ID in the BioLiP database."%(pdb_id))
                fp = open(docking_output_dir+'ERROR.txt', 'w')
                fp.write("Cannot find %s PDB ID in the BioLiP database."%(pdb_id))
                fp.close()
        fp_results.close()
        retrieve_enzyme_sequence(output_dir, interaction_sim_cutoff)

        ##### Start homology modeling #####
        if homology_model:
            hm_output_dir = output_dir+'/HomologyModel/%s/'%substrate_id
            if not os.path.exists(hm_output_dir):
                os.makedirs(hm_output_dir)
            hm_candidates = retrieve_enzyme_homology_modeling(output_dir, interaction_sim_cutoff)
            print(hm_candidates)
            for pdb_lig in hm_candidates:
                tok = pdb_lig.split('_')
                pdb_id = tok[0]
                lig_id = tok[1]
                hm_output_pdb_dir = hm_output_dir+'/%s/'%(pdb_id)
                if not(os.path.exists(hm_output_pdb_dir)):
                    os.mkdir(hm_output_pdb_dir)
                logging.info('Start fasta download from PDB')
                Homology_modeling.pdb_fasta_download(hm_output_pdb_dir, pdb_id)
                logging.info('Finish fasta download from PDB')
                logging.info('Start BLAST search')
                Homology_modeling.blast_search_sequence(hm_output_pdb_dir, pdb_id, modeller_blast_hit_size)
                logging.info('Finish BLAST search')
                Homology_modeling.blast_output_parsing(hm_output_pdb_dir, pdb_id, modeller_parsing_orgn_max_num, modeller_parsing_seq_identity)
                logging.info('Start homology modeling')
                Homology_modeling.align_template(BIOLIP_DIR, hm_output_pdb_dir, pdb_id, lig_id)
                Homology_modeling.model_building(hm_output_pdb_dir, pdb_id, modeller_model_num)
                logging.info('Finish homology modeling')

                hm_docking_output_dir = hm_output_pdb_dir+'docking_results/'
                if not os.path.exists(hm_docking_output_dir):
                    os.mkdir(hm_docking_output_dir)
                hm_plip_output_dir = hm_output_pdb_dir+'PLIP_results/'
                if not os.path.exists(hm_plip_output_dir):
                    os.mkdir(hm_plip_output_dir)

                crystal_file_list = glob.glob(plip_output_dir+substrate_id+'/'+pdb_lig+'*/crystal/complex_1/*.xml')
                plip_xml_file = crystal_file_list[0]
                shutil.copy(plip_xml_file, hm_plip_output_dir+pdb_lig+'_plip.xml')
                with open(hm_docking_output_dir+'substrate.smi', 'w') as fp:
                    fp.write('%s\n'%(each_smiles))
                ## Target substrate for docking simulaitons
                smiles = each_smiles
                ## homology models for docking simulations
                pdb_file = glob.glob(hm_output_pdb_dir+'*.pdb')
                basename = os.path.basename(pdb_file[0])
                pdb_chain = os.path.splitext(basename)[0][-1]

                hm_model_files = glob.glob(hm_output_pdb_dir+'/template_files/*.pdb')
                file_cnt = 1
                for hm_model in hm_model_files:
                    file_id = pdb_id+'_%s'%file_cnt
                    file_cnt += 1
                    hm_base = os.path.basename(hm_model)
                    hm_model_name = os.path.splitext(hm_base)[0]
                    with open(hm_output_pdb_dir+'homology-models_file-id.txt', 'a') as hm_fw:
                        hm_fw.write(hm_model_name+'\t'+file_id+'\n')
                    natural_ligand_file, receptor_file, prot_chain = vina_dock.hm_search_db_ligand_file(pdb_id, lig_id, pdb_chain, BIOLIP_DIR)
                    if natural_ligand_file != False and receptor_file != False:
                        vina_dock.run_hm_vina_docking(natural_ligand_file, receptor_file, prot_chain, file_id, smiles, pdb_id, hm_docking_output_dir, BIOLIP_DIR, MGLToolsPkg_DIR, MGLTools_PYTHON_DIR, vina_mode_num, vina_energy_range,vina_exhaustiveness, vina_cpu_num, vina_grid_point)
                        docking_plip_folder = interaction_similarity.run_hm_PLIP_pdbqt(hm_model, hm_docking_output_dir, hm_plip_output_dir, file_id, PLIP_path)
                        natural_plip_file = hm_plip_output_dir+pdb_lig+'_plip.xml'
                        interaction_sim_info = interaction_similarity.hm_calculate_interaction_similarity(file_id, natural_plip_file, lig_id, docking_plip_folder)
                        with open(hm_output_pdb_dir+'Interaction_similarity.txt', 'a') as is_fw:
                            for k in interaction_sim_info.keys():
                                is_fw.write(str(k)+'\t'+str(interaction_sim_info[k])+'\n')
                    else:
                        error_folder = hm_plip_output_dir+'%s_ERROR'%file_id
                        os.mkdir(error_folder)

                matching_file = hm_output_pdb_dir+'homology-models_file-id.txt'
                inter_file = hm_output_pdb_dir+'Interaction_similarity.txt'
                blast_file = hm_output_pdb_dir+'BLAST_parsing_results.txt'
                retrieve_enzyme_sequence_hm(hm_output_pdb_dir, matching_file, inter_file, blast_file, hm_interaction_sim_cutoff)

    logging.info(time.strftime("Elapsed time %H:%M:%S", time.gmtime(time.time() - start)))

if __name__ == '__main__':
    main()

