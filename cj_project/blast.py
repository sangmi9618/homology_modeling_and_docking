import glob, os
import Homology_modeling

output_dir = './ddh_sequences/'
seqs = glob.glob('./ddh_sequences/*.fasta')
xmls = glob.glob('./ddh_sequences/*.xml')
output_dir = './ddh_sequences/'
for seq in seqs:
    seq_id = seq.split('/')[-1].split('.fasta')[0]
    print(seq_id)
    Homology_modeling.blast_search_sequence(output_dir,seq_id,300)
