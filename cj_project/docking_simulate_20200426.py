# simulate docking (jupyter notebook error --> using ipython notebook on putty)
from docking_utils import *
import pandas as pd
import glob, os

vina_mode_num = 10
vina_energy_range = 10
vina_exhaustiveness = 20
vina_cpu_num = 30

df = pd.read_csv('./template_matching.csv', header=None, index_col=0)
df.index = df.index.str.strip()

output_dir='./ddh_sequences/docking_with_cofactor_20200426/'
enzyme_pdbqt = glob.glob(output_dir + 'docking_input/receptor_file/*.pdbqt')
#os.mkdir(output_dir+'docking_output/')
for pdbqt in enzyme_pdbqt:
    seq_id = pdbqt.split('/')[-1].split('_complex.pdbqt')[0]
    print(seq_id)
    temp_id = df.loc[seq_id][1]
    config_docking_file = './docking_grid_files/config_%s_grid.txt'%(temp_id)
    if not os.path.exists(config_docking_file):
        continue
    
    ligand_list = ['L-2-Amino-6-oxopimelate']
    #ligand_list=['NADPH']
    for li in ligand_list:
        docking_output_dir=output_dir+'docking_output/%s'%li
        if not os.path.exists(docking_output_dir):
            os.mkdir(docking_output_dir)

        ligand_docking_file = '/data/user_home/smlee/meli3d_shuan/cj_project/substrate_docking_input/%s.pdbqt'%li
        
        receptor_docking_file = pdbqt
        output_file = docking_output_dir+'/Docking_result_%s.pdbqt'%(seq_id)
        log_file = docking_output_dir+'/Docking_log_%s.log'%(seq_id)

        vina_docking(receptor_docking_file, ligand_docking_file, config_docking_file, output_file, log_file, vina_mode_num, vina_energy_range, vina_exhaustiveness, vina_cpu_num)
