import subprocess
import glob,os 
import time

output_dir = './ddh_sequences/'
seqs = glob.glob('./ddh_sequences/*.fasta')
os.mkdir(output_dir+'BLAST_template_selection')

start = time.time()
for seq in seqs:
    seq_id = seq.split('/')[-1].split('.fasta')[0]
    non_id = []
    with open("./ddh_sequences/BLAST_parsing_results.txt","r") as fr: 
        for li in fr.readlines():
            non_id.append(li.split()[0])
                                        
    if seq_id not in non_id:
        print(seq_id)
        subprocess.call("blastp -query %s -db template_nr.fasta -out ./ddh_sequences/BLAST_template_selection/%s_blast.xml -outfmt 5"%(seq, seq_id), shell=True)

        
print(time.strftime("Elapsed time %H:%M:%S", time.gmtime(time.time() - start)))