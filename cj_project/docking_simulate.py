# simulate docking (jupyter notebook error --> using ipython notebook on putty)
from docking_utils import *
import pandas as pd
import glob, os

df = pd.read_csv('./template_matching.csv', header=None, index_col=0)
df.index = df.index.str.strip()

vina_mode_num = 10
vina_energy_range = 10
vina_exhaustiveness = 20
vina_cpu_num = 20

output_dir='./metA_sequences/'
enzyme_pdbqt = glob.glob(output_dir+'docking_input/*')
#os.mkdir(output_dir+'docking_output')
for pdbqt in enzyme_pdbqt:
    seq_id = pdbqt.split('/')[-1].split('.pdbqt')[0]
    temp_id = df.loc[seq_id][1]
#     if temp_id =='5X5H_A':
#         continue
    config_docking_file = './docking_grid_files/config_%s_grid.txt'%(temp_id)
    if not os.path.exists(config_docking_file):
        continue
    #ligand_list =['acetyl-CoA', 'succinyl-CoA']
    #ligand_list = ['L-2-Amino-6-oxopimelate']
    ligand_list = ['O-Acetylhomoserine', 'O-Succinylhomoserine']
    for li in ligand_list:
        docking_output_dir=output_dir+'docking_output/%s'%li
        if not os.path.exists(docking_output_dir):
            os.mkdir(docking_output_dir)
        
        ligand_docking_file = './substrate_docking_input/%s.pdbqt'%li
        receptor_docking_file = pdbqt
        output_file = docking_output_dir+'/Docking_result_%s'%(seq_id)
        log_file = docking_output_dir+'/Docking_log_%s.log'%(seq_id)

        vina_docking(receptor_docking_file, ligand_docking_file, config_docking_file, output_file, log_file, vina_mode_num, vina_energy_range, vina_exhaustiveness, vina_cpu_num)