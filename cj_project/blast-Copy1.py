import glob, os
import Homology_modeling
import time

output_dir = './metYnZ_sequences/'
seqs = glob.glob('./metYnZ_sequences/*.fasta')
#xmls = glob.glob('./ddh_sequences/*.xml')
output_dir = './metYnZ_sequences/'

start = time.time()
for seq in seqs[:1]:
    seq_id = seq.split('/')[-1].split('.fasta')[0]
    print(seq_id)
    Homology_modeling.blast_search_sequence(output_dir,seq_id,300)
    
print(time.strftime("Elapsed time %H:%M:%S", time.gmtime(time.time() - start)))