import os, glob
import shutil
import subprocess


vina_output = glob.glob('./*.pdbqt')


output_dir = './split_ligand_file'
if not os.path.exists(output_dir):
    os.mkdir(output_dir)

for fi in vina_output:
    subprocess.call('vina_split --input {}'.format(fi), shell=True)
    print(fi.split('.pdbqt')[0])
    shutil.copy('%s_ligand_01.pdbqt'%fi.split('.pdbqt')[0], output_dir)
    for split in glob.glob('%s_ligand*'%(fi.split('.pdbqt')[0])):
        os.remove(split)
    

