import glob, os
import Homology_modeling

output_dir = './metX_sequences/'
seqs = glob.glob('./metX_sequences/*.fasta')
xmls = glob.glob('./metX_sequences/*.xml')
output_dir = './metX_sequences/'

done_seqs = [xml.split('_blast')[0] for xml in xmls]
seqs = [seq.split('.fasta')[0] for seq in seqs]
seqs = set(seqs) - set(done_seqs)

for seq in seqs:
    seq_id = seq.split('/')[-1].split('.fasta')[0]
    print(seq_id)
    Homology_modeling.blast_search_sequence(output_dir,seq_id,300)
