import pandas as pd
import os
from vina_dock import run_vina_docking
vina_mode_num = 15
vina_energy_range = 4
vina_exhaustiveness = 12
vina_cpu_num = 4
vina_grid_point = 5
MGLTools_DIR = '/data1/user_home/shuanchen/anaconda3/pkgs/mgltools-1.5.6-1/'
MGLToolsPkg_DIR = MGLTools_DIR + 'MGLToolsPckgs/AutoDockTools/Utilities24/'
MGLTools_PYTHON_DIR = MGLTools_DIR + 'bin/pythonsh'
BIOLIP_DIR = './data/BioLiP_updated_set/'
reference_DB_RDT = './data/RDT_txt_results_20181206_substrates/'
RDT_path = './bin/RDT1.5.1.jar'
PLIP_path = './bin/plip-stable/plip/plipcmd'

protein_list = ['1iep', 'ihck']
docking_output_dir = '../data/smi_files'

source = pd.read_csv('../data/smi_files/smi.csv')

for i, file_id in enumerate(source['name']):
    for pdb_id in protein_list:
        smiles = source['smiles'][i]
        run_vina_docking(file_id, smiles, pdb_id, docking_output_dir, BIOLIP_DIR, MGLToolsPkg_DIR, MGLTools_PYTHON_DIR, 
                         vina_mode_num, vina_energy_range, vina_exhaustiveness, vina_cpu_num, vina_grid_point)
