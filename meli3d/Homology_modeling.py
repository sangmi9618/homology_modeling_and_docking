import os
import glob
import shutil
from Bio.Blast import NCBIWWW, NCBIXML
from Bio import SeqIO
from modeller import *
import urllib.request
from xml.dom.minidom import parse,parseString 
from modeller import *
from modeller.automodel import *

def pdb_fasta_download(output_dir, pdb_id):
    pdb_url = 'https://www.rcsb.org/pdb/download/downloadFile.do?fileFormat=fastachain&compression=NO&structureId='+pdb_id+'&chainId=A' 
    save_file = output_dir+pdb_id+'.fasta'
    urllib.request.urlretrieve(pdb_url, save_file)
    return

def blast_search_sequence(output_dir, pdb_id, hit_size):
    fasta_file = output_dir+pdb_id+'.fasta'
    blast_output_file = output_dir+pdb_id+'_blast.xml'

    record = SeqIO.read(fasta_file, format='fasta')
    blast_results = NCBIWWW.qblast('blastp', 'nr', record.format('fasta'), hitlist_size=hit_size)
    
    with open(blast_output_file, 'w') as out_handle:
        out_handle.write(blast_results.read())
    blast_results.close()
    return

def get_orgn_name(gi_num):
    url = 'https://taxonomy.jgi-psf.org/gi/'+gi_num
    res = urllib.request.urlopen(url)
    data = res.read()
    results = data.decode('utf-8')
    tok = results.split('"')
    orgn = tok[5]
    return orgn

def blast_output_parsing(output_dir, pdb_id, output_file_num, seq_iden_thresh):
    blast_file = output_dir+pdb_id+'_blast.xml'
    dom = parse(blast_file)
    hits = dom.getElementsByTagName('Hit')

    mod_orgn = []
    mod_genus = []
    cnt = 0
    fw = open(output_dir+'BLAST_parsing_results.txt', 'w')
    for hit in hits:
        if cnt == output_file_num:
            break
        hit_id_pre = hit.getElementsByTagName('Hit_id')[0]
        hit_id = hit_id_pre.firstChild.data
        hit_id_tok = hit_id.split('|')
        if hit_id_tok[0] == 'gi':
            gi_num = hit_id_tok[1]
            
            hit_def_pre = hit.getElementsByTagName('Hit_def')[0]
            hit_def = hit_def_pre.firstChild.data
            if '>' in hit_def:
                gene_name = hit_def.split('>')[0]
            else:
                gene_name = hit_def
            
            hit_accession_pre = hit.getElementsByTagName('Hit_accession')[0]
            hit_accession = hit_accession_pre.firstChild.data
            if len(hit_accession) == 6 and hit_accession[-1] == 'A':
                blast_pdb = hit_accession.split('_')[0]
            else:
                blast_pdb = 'No_PDB_ID'
            
            hit_len_pre = hit.getElementsByTagName('Hit_len')[0]
            hit_len = hit_len_pre.firstChild.data
            
            hsps = hit.getElementsByTagName('Hsp')
            for hsp in hsps:
                hsp_iden_pre = hsp.getElementsByTagName('Hsp_identity')[0]
                hsp_iden = hsp_iden_pre.firstChild.data
                seq_iden = float(hsp_iden)/int(hit_len)
                if seq_iden > seq_iden_thresh and seq_iden != 1.0 and blast_pdb != pdb_id.upper():
                    orgn_name = get_orgn_name(gi_num)
                    name_tok = orgn_name.split()
                    if len(name_tok) == 1:
                        continue
                    hsp_evalue_pre = hsp.getElementsByTagName('Hsp_evalue')[0]
                    hsp_evalue = hsp_evalue_pre.firstChild.data

                    hsp_seq_pre = hsp.getElementsByTagName('Hsp_hseq')[0]
                    hsp_seq = hsp_seq_pre.firstChild.data
                    if orgn_name not in mod_orgn:
                        tok_orgn_name = orgn_name.split()
                        genus = tok_orgn_name[0]
                        if genus not in mod_genus:
                            cnt += 1
                            mod_genus.append(genus)
                            mod_orgn.append(orgn_name)
                            fw.write('%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n' % (gi_num, orgn_name, gene_name, blast_pdb, hit_len, seq_iden, hsp_evalue, hsp_seq))
    fw.close()

def make_PIR_file(output_dir):
    if not(os.path.isdir(output_dir+'PIR_files/')):
        os.makedirs(os.path.join(output_dir, 'PIR_files/'))
    with open(output_dir+'BLAST_parsing_results.txt','r') as fr:
        for line in fr:
            tok = line.split('\t')
            orgn = tok[1].strip()
            orgn = orgn.replace(' ', '_')
            if '-' in orgn:
                orgn = orgn.replace('-', '_')
            if '/' in orgn:
                orgn = orgn.replace('/', '_')
            if ':' in orgn:
                orgn = orgn.replace(':', '_')
            if ';' in orgn:
                orgn = orgn.replace(';', '_')
            if '[' in orgn:
                orgn = orgn.replace('[', '')
            if ']' in orgn:
                orgn = orgn.replace(']', '')
            if '(' in orgn:
                orgn = orgn.replace('(', '')
            if ')' in orgn:
                orgn = orgn.replace(')', '')
            if '\'' in orgn:
                orgn = orgn.replace('\'', '')
            gene_name = tok[2]
            if ':' in gene_name:
                gene_name = orgn.replace(':', ';')
            pdb_exist = tok[3]
            length = tok[4]
            seq_iden = tok[5]
            target_seq = tok[7]
            target_seq = target_seq.split('\n')[0]
            with open(output_dir+'/PIR_files/'+orgn+'.ali', 'w') as fw:
                fw.write('>P1;%s\n' %(orgn))
                fw.write('sequence:%s:%s:%s:%s:%s::::\n' %(orgn, gene_name, pdb_exist, length, seq_iden))
                fw.write('%s*\n' %(target_seq))
    return orgn

def align_template(BIOLIP_DIR, output_dir, pdb_id, lig_id):
    make_PIR_file(output_dir)
    biolip_receptor_dir = BIOLIP_DIR+'receptor/' 
    biolip_ligand_dir = BIOLIP_DIR+'ligand/'
    if not(os.path.isdir(output_dir+'template_files/')):
        os.makedirs(os.path.join(output_dir, 'template_files/'))
    
    ligand_files = glob.glob(os.path.join(biolip_ligand_dir, '*'))
    for ligand in ligand_files:
        lig_basename = os.path.basename(ligand)
        lig_pdb_id = lig_basename.split('_')[0]
        lig_lig_id = lig_basename.split('_')[1]
        chain_id = lig_basename.split('_')[2]
        if pdb_id == lig_pdb_id and lig_id == lig_lig_id:
            if os.path.exists(biolip_receptor_dir+pdb_id+chain_id+'.pdb'):
                shutil.copy(biolip_receptor_dir+pdb_id+chain_id+'.pdb', output_dir)
                break
    pir_output_dir = output_dir+'PIR_files/'
    pir_files = glob.glob(os.path.join(pir_output_dir,'*'))
    for pir in pir_files:
        with open(pir, 'r') as fr_pir:
            lines = fr_pir.read().splitlines()
            tok = lines[1].split(':')

            orgn = lines[0].split(';')[1]
            env = environ()
            aln = alignment(env)
            mdl = model(env, file=output_dir+pdb_id+chain_id, model_segment=('FIRST:'+chain_id, 'LAST:'+chain_id))
            aln.append_model(mdl, align_codes=pdb_id+chain_id, atom_files=output_dir+pdb_id+chain_id+'.pdb')
            aln.append(file=pir, align_codes=orgn)
            aln.align2d()
            aln.write(file=pir_output_dir+orgn+'-'+pdb_id+'.ali', alignment_format='PIR')
            aln.write(file=pir_output_dir+orgn+'-'+pdb_id+'.pap', alignment_format='PAP')
    return

def model_building(output_dir, pdb_id, model_num):
    pdb_id = pdb_id.lower()
    pdb_file_list = glob.glob(os.path.join(output_dir, pdb_id+'*.pdb'))
    pdb_file_ext = os.path.basename(pdb_file_list[0])
    pdb_file = os.path.splitext(pdb_file_ext)[0]
    pdb_chain = pdb_file[-1]

    PIR_dir = output_dir+'PIR_files/'
    TEM_dir = output_dir+'template_files/'
    ali_files = glob.glob(os.path.join(PIR_dir, '*'))
    for ali_file in ali_files:
        if pdb_id == ali_file[-8:-4] and 'ali' == ali_file[-3:]:
            base_ext = os.path.basename(ali_file)
            base = os.path.splitext(base_ext)[0]
            orgn = base.split('-')[0]
            env = environ()
            a = automodel(env, alnfile=ali_file, knowns=pdb_id+pdb_chain, sequence=orgn, assess_methods=(assess.DOPE))
            a.starting_model = 1
            a.ending_model = model_num
            a.make()
            mv_files = glob.glob(orgn+'*')
            for f in mv_files:
                if os.path.splitext(f)[1] == '.pdb':
                    shutil.move(f, TEM_dir)
                else:
                    os.remove(f)
    return
