import subprocess
import os
import glob
from xml.etree.ElementTree import parse
from xml.sax import make_parser
from xml.sax import handler
from xml.dom.minidom import parse, parseString

def check_interaction(interaction_type, each_binding_site):
    residues = each_binding_site.getElementsByTagName(interaction_type)
    interaction_list = []
    for each_residue in residues:
        resnr = each_residue.getElementsByTagName("resnr")
        resnr = resnr[0].firstChild.data

        restype = each_residue.getElementsByTagName("restype")
        restype = restype[0].firstChild.data

        interaction = '%s_%s'%(resnr, restype)
        interaction_list.append(interaction)
    interaction_list = [str(interaction) for interaction in interaction_list]
    return interaction_list

def hm_check_interaction(interaction_type, each_binding_site):
    residues = each_binding_site.getElementsByTagName(interaction_type)
    interaction_list = []
    for each_residue in residues:
        restype = each_residue.getElementsByTagName('restype')
        restype = restype[0].firstChild.data

        interaction = restype
        interaction_list.append(interaction)
    interaction_list = [str(interaction) for interaction in interaction_list]
    return interaction_list

def parse_interaction(xml_file, target_chain):
    interaction_type_candidates = ['hydrophobic_interaction', 'hydrogen_bond', 'water_bridge', 'salt_bridge', 'metal_complex', 'pi_stack', 'halogen_bond']
    dom = parse(xml_file)

    final_interaction_info = {}
    binding_sites = dom.getElementsByTagName("bindingsite")
    for each_binding_site in binding_sites:
        interaction_info = each_binding_site.getAttribute('has_interactions')
        if interaction_info:
            chain_flag = False
            chain_name_obj = each_binding_site.getElementsByTagName("interacting_chain")
            chain_names = []
            for item in chain_name_obj:
                chain_names.append(item.firstChild.data)
            if len(chain_names) == 1:
                if chain_names[0] == target_chain:
                    chain_flag = True
            if chain_flag:
                for interaction_type in interaction_type_candidates:
                    results = check_interaction(interaction_type, each_binding_site)
                    final_interaction_info[interaction_type] = results
    return final_interaction_info

def hm_parse_interaction(xml_file, lig_id):
    interaction_type_candidates = ['hydrophobic_interaction', 'hydrogen_bond', 'water_bridge', 'salt_bridge', 'metal_complex', 'pi_stack', 'halogen_bond']
    dom = parse(xml_file)

    final_interaction_info = {}
    binding_sites = dom.getElementsByTagName('bindingsite')
    for each_binding_site in binding_sites:
        interaction_info = each_binding_site.getAttribute('has_interactions')
        if interaction_info:
            cmpd_name_info = each_binding_site.getElementsByTagName('longname')
            cmpd_name = cmpd_name_info[0].firstChild.data
            if cmpd_name == lig_id or cmpd_name == 'UNL':
                for interaction_type in interaction_type_candidates:
                    results = hm_check_interaction(interaction_type, each_binding_site)
                    final_interaction_info[interaction_type] = results

    return final_interaction_info

def calculate_interaction_similarity(prot_chain, pdb_id, cryst_plip_folder, docking_plip_folder):
    result = {}
    cryst_xml_file = cryst_plip_folder+'/report.xml'
    interaction_info1 = parse_interaction(cryst_xml_file, prot_chain)

    folders = glob.glob(docking_plip_folder+'/complex*/')
    for f in folders:
        f_tok = f.split('/')[-2]
        complex_num = f_tok.split('_')[-1]
        complex_id = '%s_%s' %(pdb_id, complex_num)
        xml_file = f+'report.xml'

        interaction_info2 = parse_interaction(xml_file, prot_chain)

        tanimoto_similarity_info = []
        for each_key in interaction_info1:
            res_list_a = interaction_info1[each_key]
            if each_key in interaction_info2.keys():
                res_list_b = interaction_info2[each_key]
            else:
                res_list_b = [0]

            if res_list_a == [] and res_list_b == []:
                continue

            tanimoto_score = len(set(res_list_a)&set(res_list_b))/float(len(set(res_list_a)|set(res_list_b)))
            tanimoto_similarity_info.append(tanimoto_score)
        if len(tanimoto_similarity_info) == 0:
            continue
        else: tanimoto_avg = sum(tanimoto_similarity_info, 0.0) / len(tanimoto_similarity_info)
        result[complex_id] = tanimoto_avg
    return result

def hm_calculate_interaction_similarity(file_id, natural_plip_file, lig_id, docking_plip_file):
    result = {}
    interaction_info_1 = hm_parse_interaction(natural_plip_file, lig_id)

    folders = glob.glob(docking_plip_file+'/complex*/')
    for f in folders:
        f_tok = f.split('/')[-2]
        complex_num = f_tok.split('_')[-1]
        complex_id = '%s_%s' %(file_id, complex_num)
        xml_file = f+'report.xml'
        interaction_info_2 = hm_parse_interaction(xml_file, lig_id)

        tanimoto_similarity_info = []
        for each_key in interaction_info_1:
            res_list_a = interaction_info_1[each_key]
            if each_key in interaction_info_2.keys():
                res_list_b = interaction_info_2[each_key]
            else:
                res_list_b = [0]
            if res_list_a == [] and res_list_b == []:
                continue
            tanimoto_score = len(set(res_list_a)&set(res_list_b))/float(len(set(res_list_a)|set(res_list_b)))
            tanimoto_similarity_info.append(tanimoto_score)
        if len(tanimoto_similarity_info) == 0:
            continue
        else: tanimoto_avg = sum(tanimoto_similarity_info, 0.0) / len(tanimoto_similarity_info)
        result[complex_id] = tanimoto_avg
    return result

def make_complex_pdb(prot_chain, pdb_id, pdb_file, model_num, BIOLIB_DIR, plip_output_dir, substrate_id, file_id, remark):
    pdb = pdb_id.split('_')[0]
    receptor_files = glob.glob(BIOLIB_DIR+'receptor/%s.pdb'%(pdb+prot_chain))
    if len(receptor_files) > 0:
        receptor = receptor_files[0].strip()
        current_folder = os.getcwd()
        new_folder = current_folder+'/'+plip_output_dir+'/'+substrate_id+'/'+file_id+'/'+remark
        os.makedirs(new_folder)
        if remark == 'docking':
            for num in range(1, model_num):
                ligand = os.path.splitext(pdb_file)[0]+'_%s.pdb'%num
                os.system('cat %s %s | grep -v \'^TER   \' > %s/complex_%s.pdb'%(receptor, ligand, new_folder, num))
        elif remark == 'crystal':
            ligand = pdb_file
            os.system('cat %s %s | grep -v \'^TER   \' > %s/complex_1.pdb'%(receptor, ligand, new_folder))

    return new_folder

def make_complex_pdb_hm(hm_model, docking_pdb_multi_model, model_num, plip_output_dir, file_id):
    receptor = hm_model
    pro_receptor = os.path.splitext(hm_model)[0]+'_processed.pdb'

    os.system('sed \'100,$s/END/TER/g\' %s > %s'%(receptor, pro_receptor))
    current_folder = os.getcwd()
    new_folder = current_folder+'/'+plip_output_dir+file_id+'/'
    if not os.path.exists(new_folder):
        os.mkdir(new_folder)
    for num in range(1, model_num):
        ligand = os.path.splitext(docking_pdb_multi_model)[0]+'_%s.pdb'%num
        os.system('cat %s %s | grep -v \'^TER   \' > %s/complex_%s.pdb'%(pro_receptor, ligand, new_folder, num))
    os.remove(pro_receptor)
    return new_folder

def split_pdb(multi_model_pdb):
    f_num = 1
    pdb = ''
    with open(multi_model_pdb, 'r') as fr:
        lines = fr.readlines()
        for line in lines:
            if line[:6] == 'ENDMDL':
                f_name = os.path.splitext(multi_model_pdb)[0]+'_%s.pdb'%f_num 
                f_num += 1
                with open(f_name, 'w') as fw:
                    fw.write(pdb)
                    pdb = ''
            else:
                pdb += line
    return f_num

def make_pdb_from_pdbqt(pdbqt_file):
    docking_result_file = '%s'%pdbqt_file
    docking_pdb_file = os.path.splitext(docking_result_file)[0] + '.pdb'
    os.system('cut -c-66 %s > %s' % (docking_result_file, docking_pdb_file))

    pdb =''
    with open(docking_pdb_file, 'r') as fr:
        lines = fr.readlines()
        for line in lines:
            if 'ROOT' in line or 'BRANCH' in line or 'TORSDOF' in line:
                continue
            else:
                pdb += line
    with open(docking_pdb_file, 'w') as fw:
        fw.write(pdb)

    return docking_pdb_file

def hm_make_pdb_from_pdbqt(pdbqt_file, file_id):
    docking_result_file = '%sDocking_result_%s.pdbqt'%(pdbqt_file, file_id)
    docking_pdb_file = os.path.splitext(docking_result_file)[0] + '.pdb'
    os.system('cut -c-66 %s > %s' % (docking_result_file, docking_pdb_file))

    pdb =''
    with open(docking_pdb_file, 'r') as fr:
        lines = fr.readlines()
        for line in lines:
            if 'ROOT' in line or 'BRANCH' in line or 'TORSDOF' in line:
                continue
            else:
                pdb += line
    with open(docking_pdb_file, 'w') as fw:
        fw.write(pdb)

    return docking_pdb_file

def run_PLIP_crystal(prot_chain, pdb_id, natural_ligand_file, plip_output_dir, substrate_id, file_id, BIOLIB_DIR, plip_path, remark='crystal'):
    natural_ligand_pdb_file = make_pdb_from_pdbqt(natural_ligand_file)
    model_num = 1
    complex_folder = make_complex_pdb(prot_chain, pdb_id, natural_ligand_pdb_file, model_num, BIOLIB_DIR, plip_output_dir, substrate_id, file_id, remark)
    crystal_complex_file = complex_folder+'/complex_1.pdb'
    folder_name =  os.path.splitext(crystal_complex_file)[0]
    subprocess.call(
        ['python', plip_path, '-f', crystal_complex_file, '-o', folder_name, '-x'])
    rm_file = complex_folder+'/complex_1.pdb'
    os.remove(rm_file)
    return folder_name

def run_PLIP_pdbqt(prot_chain, pdb_id, docking_pdbqt_file, plip_output_dir, substrate_id, file_id, BIOLIP_DIR, plip_path, remark='docking'):
    docking_pdb_multi_model = make_pdb_from_pdbqt(docking_pdbqt_file)
    model_num = split_pdb(docking_pdb_multi_model)
    complex_folder = make_complex_pdb(prot_chain, pdb_id, docking_pdb_multi_model, model_num, BIOLIP_DIR, plip_output_dir, substrate_id, file_id, remark)
    complex_files = glob.glob(complex_folder+'/complex*.pdb')
    for docking_file in complex_files:
        basename = os.path.basename(docking_file)
        folder_name = complex_folder+'/%s' %(os.path.splitext(basename)[0])
        subprocess.call(
            ['python', plip_path, '-f', docking_file,  '-o', folder_name, '-x'])
    for f in complex_files:
        os.remove(f)
    return complex_folder

def run_hm_PLIP_pdbqt(hm_model, docking_pdbqt_file, plip_output_dir, file_id, plip_path):
    docking_pdb_multi_model = hm_make_pdb_from_pdbqt(docking_pdbqt_file, file_id)
    model_num = split_pdb(docking_pdb_multi_model)
    complex_folder = make_complex_pdb_hm(hm_model, docking_pdb_multi_model, model_num, plip_output_dir, file_id)
    complex_files = glob.glob(complex_folder+'/complex*.pdb')
    for docking_file in complex_files:
        basename = os.path.basename(docking_file)
        folder_name = complex_folder+'/%s'%(os.path.splitext(basename)[0])
        subprocess.call(
            ['python', plip_path, '-f', docking_file, '-o', folder_name, '-x'])
    for f in complex_files:
        os.remove(f)
    return complex_folder

