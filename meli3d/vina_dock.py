import subprocess
import os
import glob
from .docking_utils import *

def search_db_ligand_file(pdb_id, BIOLIP_DIR):
    '''
    receptor_pdb_id = pdb_id.split('_')[0]
    ligand_files = glob.glob(BIOLIP_DIR+'/ligand/%s.pdb'%(pdb_id))
    if len(ligand_files) > 0:
        ligand_file = ligand_files[0].strip()
        basename = os.path.basename(ligand_file)
        lig_chain = basename.split('_')[2].upper()
    else:
        return False, False, False
    '''
    receptor_files = glob.glob(BIOLIP_DIR+'/receptor/%sA.pdb'%(pdb_id))

    if len(receptor_files) > 0:
        for receptor in receptor_files:
            basename = os.path.basename(receptor)
            prot_chain = basename.split('.')[0][-1]    
            receptor_file = receptor.strip()
            break
    else:
        return False, False

    return receptor_file, prot_chain

def hm_search_db_ligand_file(pdb_id, lig_id, prot_chain, BIOLIP_DIR):
    ligand_files = glob.glob(BIOLIP_DIR+'/ligand/%s*.pdb'%(pdb_id))
    lig_file = BIOLIP_DIR+'/ligand/%s_%s_%s_1.pdb'%(pdb_id, lig_id, prot_chain)
    if lig_file in ligand_files:
        ligand_file = lig_file
        if os.path.exists(BIOLIP_DIR+'/receptor/%s%s.pdb'%(pdb_id, prot_chain)):
            receptor_file = BIOLIP_DIR+'/receptor/%s%s.pdb'%(pdb_id, prot_chain)
            return ligand_file, receptor_file, prot_chain
    return False, False, False

def run_vina_docking(file_id, smiles, pdb_id, output_dir, BIOLIP_DIR, MGLTools_DIR, MGL_python_DIR, mode_num, energy_range, exhaustiveness, cpu_num, grid_point):
    ligand_file = output_dir+'/%s.smi'%(file_id)
    with open(ligand_file, 'w') as fp:
        fp.write('%s'%(smiles))

    output_file = output_dir+'/%s.mol'%(file_id)

    make_mol_from_smi(ligand_file, output_file)

    new_input_file = output_file
    stable_output_sdf = output_dir+'/%s_optimized.pdb'%(file_id)
    ligand_energy_optimization('pdb', new_input_file, stable_output_sdf)

    output_file = output_dir+'/%s.pdbqt'%(file_id)
    prepare_ligand_with_autodock_tools(MGL_python_DIR, MGLTools_DIR, stable_output_sdf, output_file, isRigid=False)
    
    receptor_file, prot_chain = search_db_ligand_file(pdb_id, BIOLIP_DIR)
    ligand_files = glob.glob(BIOLIP_DIR+'/ligand/%s*.pdb'%(pdb_id))
    print ('#############')
    print (ligand_files)
    ligand_file = ligand_files[0].strip()

    print (ligand_file)
    ## prepare .mol file of natural ligand
    new_ligand_output_file = output_dir+'/natural_ligand_%s.pdbqt'%(file_id)
    prepare_ligand_with_autodock_tools(MGL_python_DIR, MGLTools_DIR, ligand_file, new_ligand_output_file, isRigid=False)

    receptor_output = output_dir+'/Receptor_%s.pdbqt'%(pdb_id)
    prepare_receptor_with_autodock_tools(MGL_python_DIR, MGLTools_DIR, receptor_file, receptor_output)

    receptor_output_file = output_dir+'/Receptor_%s_trim.pdbqt'%(pdb_id)
    trim_pdb_file(receptor_output, receptor_output_file)

    output_grid_file = output_dir+'/%s_grid.gpf'%(file_id)
    calculate_grid_box(MGL_python_DIR, MGLTools_DIR, new_ligand_output_file, receptor_output_file, output_grid_file, [grid_point, grid_point, grid_point])

    ## binding grid confirm
    grid_info = read_grid(output_grid_file, grid_point)

    conf_grid_output_file = output_dir+'/config_%s_grid.txt'%(file_id)
    make_vina_conf_file(grid_info, conf_grid_output_file)

    ligand_docking_file = output_file
    receptor_docking_file = receptor_output_file
    config_docking_file = conf_grid_output_file
    output_file = output_dir+'/Docking_result_%s'%(file_id)
    log_file = output_dir+'/Docking_log_%s.log'%(file_id)

    vina_docking(receptor_docking_file, ligand_docking_file, config_docking_file, output_file, log_file, mode_num, energy_range, exhaustiveness, cpu_num)

    return prot_chain

def run_hm_vina_docking(natural_ligand_file, receptor_file, prot_chain, file_id, smiles, pdb_id, output_dir, BIOLIP_DIR, MGLTools_DIR, MGL_python_DIR, mode_num, energy_range, exhaustiveness, cpu_num, grid_point):
    ligand_file = output_dir+'/%s.smi'%(file_id)
    with open(ligand_file, 'w') as fp:
        fp.write('%s'%(smiles))
    output_file = output_dir+'/%s.mol'%(file_id)
    make_mol_from_smi(ligand_file, output_file)
    new_input_file = output_file
    stable_output_sdf = output_dir+'/%s_optimized.pdb'%(file_id)
    ligand_energy_optimization('pdb', new_input_file, stable_output_sdf)
    output_file = output_dir+'%s.pdbqt'%(file_id)
    prepare_ligand_with_autodock_tools(MGL_python_DIR, MGLTools_DIR, stable_output_sdf, output_file, isRigid=False)

    new_ligand_output_file = output_dir+'/natural_ligand_%s.pdbqt'%(file_id)
    prepare_ligand_with_autodock_tools(MGL_python_DIR, MGLTools_DIR, natural_ligand_file, new_ligand_output_file, isRigid=False)

    receptor_output = output_dir+'/Receptor_%s.pdbqt'%(pdb_id)
    prepare_receptor_with_autodock_tools(MGL_python_DIR, MGLTools_DIR, receptor_file, receptor_output)
    receptor_output_file = output_dir+'/Receptor_%s_trim.pdbqt'%(pdb_id)
    trim_pdb_file(receptor_output, receptor_output_file)

    output_grid_file = output_dir+'/%s_grid.gpf'%(file_id)
    calculate_grid_box(MGL_python_DIR, MGLTools_DIR, new_ligand_output_file, receptor_output_file, output_grid_file, [grid_point, grid_point, grid_point])
    grid_info = read_grid(output_grid_file, grid_point)
    conf_grid_output_file = output_dir+'/config_%s_grid.txt'%(file_id)

    make_vina_conf_file(grid_info, conf_grid_output_file)
    ligand_docking_file = output_file
    receptor_docking_file = receptor_output_file
    config_docking_file = conf_grid_output_file
    output_file = output_dir+'/Docking_result_%s.pdbqt'%(file_id)
    log_file = output_dir+'/Docking_log_%s.log'%(file_id)

    vina_docking(receptor_docking_file, ligand_docking_file, config_docking_file, output_file, log_file, mode_num, energy_range, exhaustiveness, cpu_num)
    return

