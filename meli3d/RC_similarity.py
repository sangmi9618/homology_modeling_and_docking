import subprocess
import sys
import shutil
import glob
import os

def TC_calc(query_RC_list, target_RC_list):
    a = len(query_RC_list)
    b = len(target_RC_list)
    c = len(set(query_RC_list)&set(target_RC_list))
    TC_sim = float(c) / (a+b-c)
    return TC_sim

def parsing_target_RC(ref_DB_dir):
    trg_files = glob.glob(ref_DB_dir+'*')
    new_t_RC_dict = {}
    for trg_file in trg_files:
        base = os.path.basename(trg_file)
        pdb_id = os.path.splitext(base)[0]
        with open(trg_file, 'r') as fr:
            lines = fr.read().splitlines()
            for idx, val in enumerate(lines):
                if 'FINGERPRINTS RC' in val:
                    RC = lines[idx+1]
                    t_RC = RC[1:-1].split(',')
                    new_t_RC = []
                    for t_c in t_RC:
                        t = t_c.split(':')[0].strip()
                        new_t_RC.append(t)
                    new_t_RC_dict[pdb_id] = new_t_RC
    return new_t_RC_dict

def parsing_query_RC(output_dir):
    q_fname = output_dir+'/ECBLAST_smiles_ANNONATE.txt'
    with open(q_fname, 'r') as fr:
        lines = fr.read().splitlines()
        for idx, val in enumerate(lines):
            if 'FINGERPRINTS RC' in val:
                RC = lines[idx+1]
                q_RC = RC[1:-1].split(',')
                new_q_RC = []
                for q_c in q_RC:
                    q = q_c.split(':')[0].strip()
                    new_q_RC.append(q)
                return new_q_RC
        print('Input reaction have not reaction centers') 
        sys.exit(1)

def run_RDT(output, RDT_path, sub_smi, pro_smi):
    print ('Generate a .rxn file and identify the chemical transformation patterns: ', sub_smi,">>", pro_smi)
    subprocess.call(['java', '-jar', RDT_path, '-Q', 'SMI', '-q', '"' + sub_smi + '>>' + pro_smi + '"', '-g', '-j','ANNOTATE', '-f', 'TEXT', '-x'])
    shutil.copy('./ECBLAST_smiles_ANNONATE.txt', output)
    rm_files = glob.glob('./ECBLAST_smiles_ANNONATE*')
    for f in rm_files:
        os.remove(f)
    return

def run_RC_sim(input_reaction, output_dir, ref_DB_dir, RDT_path, threshold):
    rxn_smi = input_reaction
    sub_smi = ''
    pro_smi = ''
    if '>>' in rxn_smi:
        tok_smi = rxn_smi.split('>>')
        sub_smi = tok_smi[0]
        pro_smi = tok_smi[1]
    else:
        print('Input SMILES must have reaction form (reactants_smiles>>product_smiles')
        sys.exit(1)

    run_RDT(output_dir, RDT_path, sub_smi, pro_smi)
    
    RC_list_query = parsing_query_RC(output_dir)
    RC_dic_target = parsing_target_RC(ref_DB_dir)
    pdb_candidates = {}
    for pdb_id in RC_dic_target.keys():
        RC_list_trg = RC_dic_target[pdb_id]
        TC_sim = TC_calc(RC_list_query, RC_list_trg)
        if TC_sim > threshold:
            tok = pdb_id.split('_')
            pdb = tok[0].lower()
            lig = tok[1].upper()
            candidate = pdb+'_'+lig
            pdb_candidates[candidate] = TC_sim
    return pdb_candidates

