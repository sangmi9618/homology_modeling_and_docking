import math
from os import environ
environ['KERAS_BACKEND'] = 'tensorflow'
# vae stuff
from chemvae.vae_utils import VAEUtils
from chemvae import mol_utils as mu
# import scientific py
import numpy as np
import pandas as pd
# rdkit stuff
from rdkit.Chem import AllChem as Chem
from rdkit.Chem import PandasTools
# plotting stuff
import matplotlib.pyplot as plt
import matplotlib as mpl

vae = VAEUtils(directory='./data/models/zinc_gpu')

def read_ref_db():
    ref_db_info = {}
    ref_file_dir = './data/ref_dataset_chemicalVAE_substrate_non-redundant.txt'
    with open(ref_file_dir, 'r') as fp:
        fp.readline()
        for line in fp:
            sptlist = line.strip().split('\t')
            ec_number = sptlist[0].strip()
            species = sptlist[1].strip()

            pdb_id = sptlist[2].strip()
            lig_id = sptlist[3].strip()
            substrates = sptlist[4].strip()
            substrates = substrates.replace('[', '')
            substrates = substrates.replace(']', '')
            substrate_list = substrates.split(',')

            products = sptlist[5].strip()
            products = products.replace('[', '')
            products = products.replace(']', '')
            product_list = products.split(',')

            substrate_vec_list = []
            product_vec_list = []
            for each_val in substrate_list:
                substrate_vec_list.append(np.float(each_val))
            for each_val in product_list:
                product_vec_list.append(np.float(each_val))

            ref_db_info[(pdb_id, lig_id)] = [substrate_vec_list, product_vec_list]
    return ref_db_info

def cosine_similarity(a, b):
    return sum([i*j for i,j in zip(a, b)])/(math.sqrt(sum([i*i for i in a]))*math.sqrt(sum([i*i for i in b])))

def split_substrates_products(input_smiles):
    substrate_list = []
    substrates = input_smiles.split('>>')[0].strip()
    for each_substrate in substrates.split('.'):
        substrate_list.append(each_substrate)

    product_list = []
    products = input_smiles.split('>>')[1].strip()
    for each_product in products.split('.'):
        product_list.append(each_product)
    return substrate_list, product_list

def convert_smiles_to_vector(smiles_list):
    global vae
    feature_vector_list = []
    for smiles in smiles_list:
        try:
            smiles2 = mu.canon_smiles(smiles)
            X_1 = vae.smiles_to_hot(smiles2, canonize_smiles=True)
            z_1 = vae.encode(X_1)

            feature_vector = list(z_1[0])
            feature_vector_list.append(feature_vector)
        except:
            pass
    return feature_vector_list

def run_chemvae_similarity(input_smiles, threshold=0.8):
    ref_db_info = read_ref_db()

    substrate_list, product_list = split_substrates_products(input_smiles)
    substrate_vec = convert_smiles_to_vector(substrate_list)
    product_vec = convert_smiles_to_vector(product_list)

    sum_substrate_vec = np.sum(substrate_vec, axis=0)
    sum_product_vec = np.sum(product_vec, axis=0)

    filtered_results = {}
    for each_key in ref_db_info:
        db_substrate_vec = ref_db_info[each_key][0]
        db_product_vec = ref_db_info[each_key][1]
        substrate_sim = cosine_similarity(db_substrate_vec, sum_substrate_vec)
        product_sim = cosine_similarity(db_product_vec, sum_product_vec)
        mean_sim = (substrate_sim+product_sim)/2.0
        if mean_sim >= threshold:
            pdb_id = each_key[0].lower()
            lig_id = each_key[1].upper()
            filtered_results[pdb_id+'_'+lig_id] = mean_sim

    return filtered_results

