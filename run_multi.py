import pandas as pd
import os
from meli3d import vina_dock
from meli3d import docking_utils

vina_mode_num = 15
vina_energy_range = 4
vina_exhaustiveness = 12
vina_cpu_num = 4
vina_grid_point = 5
MGLTools_DIR = '/data1/user_home/shuanchen/anaconda3/pkgs/mgltools-1.5.6-1/'
MGLToolsPkg_DIR = MGLTools_DIR + 'MGLToolsPckgs/AutoDockTools/Utilities24/'
MGLTools_PYTHON_DIR = MGLTools_DIR + 'bin/pythonsh'
BIOLIP_DIR = './data/BioLiP_updated_set/'
reference_DB_RDT = './data/RDT_txt_results_20181206_substrates/'
RDT_path = './bin/RDT1.5.1.jar'
PLIP_path = './bin/plip-stable/plip/plipcmd'

source = pd.read_csv('./input/smi.csv')

for i, file_id in enumerate(source['ligand_name']):
    pdb_id = source['pdb_id'][i]
    smiles = source['ligand_smiles'][i]       
    os.mkdir('./output/%s' % (file_id))
    os.mkdir('./output/%s/%s' % (file_id, pdb_id))
    docking_output_dir = ('./output/%s/%s' % (file_id, pdb_id))
    vina_dock.run_vina_docking(file_id, smiles, pdb_id, docking_output_dir, BIOLIP_DIR, MGLToolsPkg_DIR, MGLTools_PYTHON_DIR, vina_mode_num, vina_energy_range, vina_exhaustiveness, vina_cpu_num, vina_grid_point)
