
//
FINGERPRINTS BC

ORDER_CHANGED
[C%N*C%N:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1:1.0, [CH2]:1.0, [CH]:2.0, [CH]N([CH])[CH]:1.0, [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]:1.0, [C]:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]:1.0, [C]C(=[C])N=C([CH2])[CH2]:1.0, [C]C(=[C])N=C([CH2])[CH2]>>[C]C(=[C])NC([CH2])[CH2]:1.0, [C]C(=[C])NC([CH2])[CH2]:1.0, [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O:1.0, [C]C=[CH]:1.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C=[CH]:1.0, [C]N=C(C[NH])C[NH]:1.0, [C]N=C(C[NH])C[NH]>>[C]NC(C[NH])C[NH]:1.0, [C]N=[C]:1.0, [C]N=[C]>>[C]N[CH]:1.0, [C]NC(C[NH])C[NH]:1.0, [C]NCC1=NC([C])=[C]NC1>>[C]NCC1NC([C])=[C]NC1:1.0, [C]N[CH]:1.0, [N+]:1.0, [NH]:1.0, [NH]C([CH2])[CH2]:1.0, [N]:2.0, [N]=C([CH2])[CH2]:1.0, [N]=C([CH2])[CH2]>>[NH]C([CH2])[CH2]:1.0, [N]C(=O)C=1N=C(CNC1[NH])C[NH]>>[N]C(=O)C=1NC(CNC1[NH])C[NH]:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH]:1.0, [C]:1.0, [NH]:1.0, [N]:1.0]


ID=Reaction Center at Level: 1 (4)
[[C]N=[C]:1.0, [C]N[CH]:1.0, [NH]C([CH2])[CH2]:1.0, [N]=C([CH2])[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]C(=[C])N=C([CH2])[CH2]:1.0, [C]C(=[C])NC([CH2])[CH2]:1.0, [C]N=C(C[NH])C[NH]:1.0, [C]NC(C[NH])C[NH]:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[NH]C([CH2])[CH2]:1.0, [N]=C([CH2])[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[[C]N=C(C[NH])C[NH]:1.0, [C]NC(C[NH])C[NH]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]N=[C]>>[C]N[CH]
2: [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]
3: [C]C[CH]>>[C]C=[CH]
4: [N]=C([CH2])[CH2]>>[NH]C([CH2])[CH2]

MMP Level 2
1: [C]C(=[C])N=C([CH2])[CH2]>>[C]C(=[C])NC([CH2])[CH2]
2: [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]
3: [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]
4: [C]N=C(C[NH])C[NH]>>[C]NC(C[NH])C[NH]

MMP Level 3
1: [N]C(=O)C=1N=C(CNC1[NH])C[NH]>>[N]C(=O)C=1NC(CNC1[NH])C[NH]
2: [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O
3: O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1
4: [C]NCC1=NC([C])=[C]NC1>>[C]NCC1NC([C])=[C]NC1


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00005	[O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O]
2: R:M00003, P:M00004	[O=c1nc(N)[nH]c2NCC(=Nc12)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)O>>O=c1nc(N)[nH]c2NCC(Nc12)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)O, O=c1nc(N)[nH]c2NCC(=Nc12)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)O>>O=c1nc(N)[nH]c2NCC(Nc12)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)O]


//
SELECTED AAM MAPPING
[H+:81].[O:79]=[C:78]([OH:80])[CH2:77][CH2:76][CH:72]([NH:71][C:69](=[O:70])[C:66]:1:[CH:65]:[CH:64]:[C:63](:[CH:68]:[CH:67]1)[NH:62][CH2:61][C:58]2=[N:59][C:60]=3[C:50](=[O:49])[N:51]=[C:52]([NH2:53])[NH:54][C:55]3[NH:56][CH2:57]2)[C:73](=[O:74])[OH:75].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:5][N:6]([CH:7]=[CH:8][CH2:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48]>>[O:79]=[C:78]([OH:80])[CH2:77][CH2:76][CH:72]([NH:71][C:69](=[O:70])[C:66]:1:[CH:65]:[CH:64]:[C:63](:[CH:68]:[CH:67]1)[NH:62][CH2:61][CH:58]2[NH:59][C:60]=3[C:50](=[O:49])[N:51]=[C:52]([NH2:53])[NH:54][C:55]3[NH:56][CH2:57]2)[C:73](=[O:74])[OH:75].[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:9]:[CH:8]:[CH:7]:[N+:6](:[CH:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=5, 6=4, 7=2, 8=1, 9=3, 10=10, 11=47, 12=45, 13=12, 14=11, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=43, 28=37, 29=26, 30=25, 31=27, 32=28, 33=29, 34=30, 35=35, 36=34, 37=33, 38=32, 39=31, 40=36, 41=38, 42=39, 43=40, 44=41, 45=42, 46=44, 47=46, 48=48, 49=81, 50=57, 51=58, 52=59, 53=60, 54=55, 55=56, 56=54, 57=52, 58=51, 59=50, 60=49, 61=53, 62=61, 63=62, 64=63, 65=64, 66=65, 67=66, 68=67, 69=68, 70=69, 71=70, 72=71, 73=72, 74=76, 75=77, 76=78, 77=79, 78=80, 79=73, 80=74, 81=75}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=57, 2=58, 3=59, 4=60, 5=55, 6=56, 7=54, 8=52, 9=51, 10=50, 11=49, 12=53, 13=61, 14=62, 15=63, 16=64, 17=65, 18=66, 19=67, 20=68, 21=69, 22=70, 23=71, 24=72, 25=76, 26=77, 27=78, 28=79, 29=80, 30=73, 31=74, 32=75, 33=6, 34=5, 35=4, 36=9, 37=8, 38=7, 39=10, 40=47, 41=45, 42=12, 43=11, 44=13, 45=14, 46=15, 47=16, 48=17, 49=18, 50=19, 51=20, 52=21, 53=22, 54=23, 55=24, 56=43, 57=37, 58=26, 59=25, 60=27, 61=28, 62=29, 63=30, 64=35, 65=34, 66=33, 67=32, 68=31, 69=36, 70=38, 71=39, 72=40, 73=41, 74=42, 75=44, 76=46, 77=48, 78=2, 79=1, 80=3}

