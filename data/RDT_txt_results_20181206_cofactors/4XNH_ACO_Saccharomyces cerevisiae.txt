
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-S:1.0]

//
FINGERPRINTS RC
[O=C(SCC[NH])C>>[NH]CCS:1.0, O=C(SC[CH2])C:1.0, O=C(SC[CH2])C>>SC[CH2]:1.0, O=C(S[CH2])C:1.0, O=C([S])C:1.0, SC[CH2]:1.0, S[CH2]:1.0, [C]:1.0, [C]S[CH2]:1.0, [C]S[CH2]>>S[CH2]:1.0, [SH]:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[C]:1.0, [S]:1.0]


ID=Reaction Center at Level: 1 (2)
[O=C([S])C:1.0, [C]S[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(SC[CH2])C:1.0, O=C(S[CH2])C:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]S[CH2]>>S[CH2]

MMP Level 2
1: O=C(SC[CH2])C>>SC[CH2]

MMP Level 3
1: O=C(SCC[NH])C>>[NH]CCS


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C>>O=C(NCCS)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:49]=[C:50]([S:1][CH2:2][CH2:3][NH:4][C:5](=[O:6])[CH2:7][CH2:8][NH:9][C:10](=[O:11])[CH:12]([OH:13])[C:14]([CH3:15])([CH3:16])[CH2:17][O:18][P:19](=[O:20])([OH:21])[O:22][P:23](=[O:24])([OH:25])[O:26][CH2:27][CH:28]1[O:29][CH:30]([N:31]:2:[CH:32]:[N:33]:[C:34]:3:[C:35](:[N:36]:[CH:37]:[N:38]:[C:39]32)[NH2:40])[CH:41]([OH:42])[CH:43]1[O:44][P:45](=[O:46])([OH:47])[OH:48])[CH3:51]>>[O:6]=[C:5]([NH:4][CH2:3][CH2:2][SH:1])[CH2:7][CH2:8][NH:9][C:10](=[O:11])[CH:12]([OH:13])[C:14]([CH3:15])([CH3:16])[CH2:17][O:18][P:19](=[O:20])([OH:21])[O:22][P:23](=[O:24])([OH:25])[O:26][CH2:27][CH:28]1[O:29][CH:30]([N:31]:2:[CH:32]:[N:33]:[C:34]:3:[C:35](:[N:36]:[CH:37]:[N:38]:[C:39]32)[NH2:40])[CH:41]([OH:42])[CH:43]1[O:44][P:45](=[O:46])([OH:47])[OH:48]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=51, 2=2, 3=1, 4=3, 5=4, 6=5, 7=6, 8=7, 9=8, 10=9, 11=10, 12=11, 13=12, 14=13, 15=14, 16=16, 17=17, 18=18, 19=19, 20=20, 21=21, 22=22, 23=23, 24=24, 25=25, 26=26, 27=27, 28=28, 29=29, 30=30, 31=45, 32=43, 33=32, 34=31, 35=33, 36=34, 37=35, 38=36, 39=41, 40=40, 41=39, 42=38, 43=37, 44=42, 45=44, 46=46, 47=47, 48=48, 49=49, 50=50, 51=15}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=15, 2=14, 3=16, 4=17, 5=18, 6=19, 7=20, 8=21, 9=22, 10=23, 11=24, 12=25, 13=26, 14=27, 15=28, 16=43, 17=41, 18=30, 19=29, 20=31, 21=32, 22=33, 23=34, 24=39, 25=38, 26=37, 27=36, 28=35, 29=40, 30=42, 31=44, 32=45, 33=46, 34=47, 35=48, 36=12, 37=10, 38=11, 39=9, 40=8, 41=7, 42=2, 43=1, 44=3, 45=4, 46=5, 47=6, 48=13}

