
//
FINGERPRINTS BC

FORMED_CLEAVED
[C#O:1.0, C-C:2.0, C=O:2.0, C@C:2.0, O=O:1.0]

ORDER_CHANGED
[C-C*C@C:1.0, C-O*C=O:2.0, C=C*C@C:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)C[CH2]:1.0, O=C(O)[CH2]:3.0, O=C1N=[C][C]=C1[CH]:1.0, O=C1N[C][C]=C1C:1.0, O=O:4.0, O=O>>O:3.0, O=O>>[C-]#[O+]:3.0, [C-]:1.0, [C-]#[O+]:4.0, [CH3]:2.0, [CH]:3.0, [CH]=C1N=[C]C([CH2])=C1C:1.0, [CH]=C1N=[C]C([CH2])=C1C>>[C-]#[O+]:1.0, [CH]=C1N=[C]C([CH2])=C1C>>[CH]C=1N[C]=C([CH2])C1C:1.0, [CH]=C1[C]=[C]C(=[CH])N1:1.0, [CH]=C1[C]=[C]C(=[CH])[N-]1:1.0, [CH]=C1[C]=[C]C(=[CH])[N-]1>>[CH]=C1[C]=[C]C(=[CH])N1:1.0, [CH]C1=[C][C]=C([CH])N1:1.0, [CH]C1=[C][C]=NC1=CC=2N[C]=[C]C2C>>[CH]C=1N[C]=C([CH2])C1C:1.0, [CH]C=1N[C]=C([CH2])C1C:1.0, [CH]C=1[C]=[C]C(=[CH])N1:1.0, [CH]C=1[C]=[C]C(=[CH])N1>>[CH]C1=[C][C]=C([CH])N1:1.0, [CH]C=1[C]=[C]NC1C=C2N=[C][C]=C2C>>[CH]C1=[C][C]NC1=CC=2N[C]=[C]C2C:1.0, [C]:12.0, [C]=C([CH])[NH]:3.0, [C]=C([CH])[NH]>>[C]C(=O)[NH]:1.0, [C]=C([CH])[NH]>>[C]C(=[CH])[NH]:1.0, [C]=C([NH])C=C1N=C([CH])C(=C1C=C)C>>O=C1N=C([CH])C(=C1C=C)C:1.0, [C]=C([NH])C=C1N=C([CH])C([CH2])=C1C>>[C]C([NH])=Cc1[nH]c([CH])c([CH2])c1C:1.0, [C]=CC=1N[C]=[C]C1C:2.0, [C]=CC=1N[C]=[C]C1C>>O=C1N[C][C]=C1C:1.0, [C]=CC=1N[C]=[C]C1[CH]:1.0, [C]=CC=1N[C]=[C]C1[CH]>>[C]C=C1N[C][C]=C1[CH]:1.0, [C]=O:2.0, [C]=O>>[C]O:1.0, [C]C:2.0, [C]C(=O)[NH]:1.0, [C]C(=[CH])[NH]:1.0, [C]C(=[C])C:4.0, [C]C(=[C])C>>[C-]#[O+]:1.0, [C]C(=[C])C>>[C]C(=[C])C:1.0, [C]C([NH])=CC(=[C])[NH]:1.0, [C]C([N])=CC(=[C])[NH]:2.0, [C]C([N])=CC(=[C])[NH]>>[C]C(=[C])C:1.0, [C]C([N])=CC(=[C])[NH]>>[C]C([NH])=CC(=[C])[NH]:1.0, [C]C([N])=Cc1[nH]c([CH])c([CH])c1C>>O=C1NC(=[CH])C([CH])=C1C:1.0, [C]C([N])=Cc1[nH]c([CH])c(c1C=C)C>>[C]=C([NH])C=C1NC(=O)C(=C1C=C)C:1.0, [C]C([N])=O:1.0, [C]C([N])=[CH]:2.0, [C]C([N])=[CH]>>[C]=C([CH])[NH]:1.0, [C]C([N])=[CH]>>[C]C([N])=O:1.0, [C]C=C1N=C(C=[C])C([CH2])=C1C>>[C]=Cc1[nH]c(C=[C])c(c1[CH2])C:1.0, [C]C=C1N=C([CH])C(=C1C)C[CH2]>>[C]=Cc1[nH]c([CH])c(c1C)C[CH2]:1.0, [C]C=C1N=[C][C]=C1C:1.0, [C]C=C1N=[C][C]=C1C>>[C]=CC=1N[C]=[C]C1C:1.0, [C]C=C1N=[C][C]=C1[CH]:1.0, [C]C=C1N=[C][C]=C1[CH]>>O=C1N=[C][C]=C1[CH]:1.0, [C]C=C1N[C][C]=C1[CH]:1.0, [C]C=[C]:3.0, [C]C=[C]>>[C]C:1.0, [C]C=[C]>>[C]C=[C]:1.0, [C]C=c1[n-]c(=C[C])c(c1[CH2])C>>[C]C=c1[nH]c(=C[C])c(c1[CH2])C:1.0, [C]C>>[C-]#[O+]:1.0, [C]CCC([O-])=O>>[C]CCC(=O)O:1.0, [C]N=[C]:1.0, [C]N=[C]>>[C]N[C]:1.0, [C]N[C]:2.0, [C]O:1.0, [C][N-][C]:1.0, [C][N-][C]>>[C]N[C]:1.0, [C][O-]:1.0, [C][O-]>>[C]=O:1.0, [N-]:1.0, [NH]:2.0, [N]:1.0, [O+]:1.0, [O-]:1.0, [O-]C(=O)C[CH2]:1.0, [O-]C(=O)C[CH2]>>O=C(O)C[CH2]:3.0, [O-]C(=O)[CH2]:3.0, [O-]C(=O)[CH2]>>O=C(O)[CH2]:3.0, [OH]:1.0, [O]:4.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (7)
[O:1.0, [C-]:1.0, [CH3]:2.0, [CH]:2.0, [C]:6.0, [O+]:1.0, [O]:4.0]


ID=Reaction Center at Level: 1 (11)
[O:1.0, O=O:2.0, [C-]#[O+]:2.0, [C]=C([CH])[NH]:1.0, [C]=O:2.0, [C]C:2.0, [C]C(=O)[NH]:1.0, [C]C(=[C])C:2.0, [C]C([N])=O:1.0, [C]C([N])=[CH]:1.0, [C]C=[C]:2.0]


ID=Reaction Center at Level: 2 (13)
[O:1.0, O=C1N=[C][C]=C1[CH]:1.0, O=C1N[C][C]=C1C:1.0, O=O:2.0, [C-]#[O+]:2.0, [CH]=C1N=[C]C([CH2])=C1C:1.0, [CH]C=1N[C]=C([CH2])C1C:1.0, [C]=CC=1N[C]=[C]C1C:1.0, [C]C(=O)[NH]:1.0, [C]C(=[C])C:2.0, [C]C([N])=CC(=[C])[NH]:2.0, [C]C([N])=O:1.0, [C]C=C1N=[C][C]=C1[CH]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (5)
[[CH]:2.0, [C]:6.0, [O-]:1.0, [OH]:1.0, [O]:2.0]


ID=Reaction Center at Level: 1 (9)
[O=C(O)[CH2]:1.0, [C]=C([CH])[NH]:2.0, [C]=O:2.0, [C]C(=[CH])[NH]:1.0, [C]C([N])=[CH]:1.0, [C]C=[C]:2.0, [C]O:1.0, [C][O-]:1.0, [O-]C(=O)[CH2]:1.0]


ID=Reaction Center at Level: 2 (10)
[O=C(O)C[CH2]:1.0, O=C(O)[CH2]:2.0, [C]=CC=1N[C]=[C]C1C:1.0, [C]=CC=1N[C]=[C]C1[CH]:1.0, [C]C([NH])=CC(=[C])[NH]:1.0, [C]C([N])=CC(=[C])[NH]:1.0, [C]C=C1N=[C][C]=C1C:1.0, [C]C=C1N[C][C]=C1[CH]:1.0, [O-]C(=O)C[CH2]:1.0, [O-]C(=O)[CH2]:2.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=O>>[C-]#[O+]
2: [C][O-]>>[C]=O
3: [C]C([N])=[CH]>>[C]C([N])=O
4: [O-]C(=O)[CH2]>>O=C(O)[CH2]
5: [C]C([N])=[CH]>>[C]=C([CH])[NH]
6: O=O>>O
7: [C]=C([CH])[NH]>>[C]C(=[CH])[NH]
8: [C][N-][C]>>[C]N[C]
9: [C]C>>[C-]#[O+]
10: [C]C=[C]>>[C]C=[C]
11: [C]=C([CH])[NH]>>[C]C(=O)[NH]
12: [C]=O>>[C]O
13: [C]N=[C]>>[C]N[C]
14: [C]C(=[C])C>>[C]C(=[C])C
15: [C]C=[C]>>[C]C

MMP Level 2
1: O=O>>[C-]#[O+]
2: [O-]C(=O)[CH2]>>O=C(O)[CH2]
3: [C]C=C1N=[C][C]=C1[CH]>>O=C1N=[C][C]=C1[CH]
4: [O-]C(=O)C[CH2]>>O=C(O)C[CH2]
5: [C]C=C1N=[C][C]=C1C>>[C]=CC=1N[C]=[C]C1C
6: O=O>>O
7: [C]=CC=1N[C]=[C]C1[CH]>>[C]C=C1N[C][C]=C1[CH]
8: [CH]=C1[C]=[C]C(=[CH])[N-]1>>[CH]=C1[C]=[C]C(=[CH])N1
9: [C]C(=[C])C>>[C-]#[O+]
10: [C]C([N])=CC(=[C])[NH]>>[C]C([NH])=CC(=[C])[NH]
11: [C]=CC=1N[C]=[C]C1C>>O=C1N[C][C]=C1C
12: [O-]C(=O)[CH2]>>O=C(O)[CH2]
13: [CH]C=1[C]=[C]C(=[CH])N1>>[CH]C1=[C][C]=C([CH])N1
14: [CH]=C1N=[C]C([CH2])=C1C>>[CH]C=1N[C]=C([CH2])C1C
15: [C]C([N])=CC(=[C])[NH]>>[C]C(=[C])C

MMP Level 3
1: O=O>>[C-]#[O+]
2: [O-]C(=O)C[CH2]>>O=C(O)C[CH2]
3: [C]=C([NH])C=C1N=C([CH])C(=C1C=C)C>>O=C1N=C([CH])C(=C1C=C)C
4: [C]CCC([O-])=O>>[C]CCC(=O)O
5: [C]=C([NH])C=C1N=C([CH])C([CH2])=C1C>>[C]C([NH])=Cc1[nH]c([CH])c([CH2])c1C
6: O=O>>O
7: [C]C([N])=Cc1[nH]c([CH])c(c1C=C)C>>[C]=C([NH])C=C1NC(=O)C(=C1C=C)C
8: [C]C=c1[n-]c(=C[C])c(c1[CH2])C>>[C]C=c1[nH]c(=C[C])c(c1[CH2])C
9: [CH]=C1N=[C]C([CH2])=C1C>>[C-]#[O+]
10: [CH]C=1[C]=[C]NC1C=C2N=[C][C]=C2C>>[CH]C1=[C][C]NC1=CC=2N[C]=[C]C2C
11: [C]C([N])=Cc1[nH]c([CH])c([CH])c1C>>O=C1NC(=[CH])C([CH])=C1C
12: [O-]C(=O)C[CH2]>>O=C(O)C[CH2]
13: [C]C=C1N=C(C=[C])C([CH2])=C1C>>[C]=Cc1[nH]c(C=[C])c(c1[CH2])C
14: [C]C=C1N=C([CH])C(=C1C)C[CH2]>>[C]=Cc1[nH]c([CH])c(c1C)C[CH2]
15: [CH]C1=[C][C]=NC1=CC=2N[C]=[C]C2C>>[CH]C=1N[C]=C([CH2])C1C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[[O-]C(=O)CCc1c2[n-]c(cc3nc(cc4[nH]c(cc5nc(c2)C(=C5C)CCC(=O)O)c(C=C)c4C)C(C=C)=C3C)c1C>>O=C1N=C(C=c2[nH]c(=Cc3[nH]c(C=C4NC(=O)C(=C4C=C)C)c(c3CCC(=O)O)C)c(c2C)CCC(=O)O)C(=C1C=C)C, [O-]C(=O)CCc1c2[n-]c(cc3nc(cc4[nH]c(cc5nc(c2)C(=C5C)CCC(=O)O)c(C=C)c4C)C(C=C)=C3C)c1C>>O=C1N=C(C=c2[nH]c(=Cc3[nH]c(C=C4NC(=O)C(=C4C=C)C)c(c3CCC(=O)O)C)c(c2C)CCC(=O)O)C(=C1C=C)C, [O-]C(=O)CCc1c2[n-]c(cc3nc(cc4[nH]c(cc5nc(c2)C(=C5C)CCC(=O)O)c(C=C)c4C)C(C=C)=C3C)c1C>>O=C1N=C(C=c2[nH]c(=Cc3[nH]c(C=C4NC(=O)C(=C4C=C)C)c(c3CCC(=O)O)C)c(c2C)CCC(=O)O)C(=C1C=C)C, [O-]C(=O)CCc1c2[n-]c(cc3nc(cc4[nH]c(cc5nc(c2)C(=C5C)CCC(=O)O)c(C=C)c4C)C(C=C)=C3C)c1C>>O=C1N=C(C=c2[nH]c(=Cc3[nH]c(C=C4NC(=O)C(=C4C=C)C)c(c3CCC(=O)O)C)c(c2C)CCC(=O)O)C(=C1C=C)C, [O-]C(=O)CCc1c2[n-]c(cc3nc(cc4[nH]c(cc5nc(c2)C(=C5C)CCC(=O)O)c(C=C)c4C)C(C=C)=C3C)c1C>>O=C1N=C(C=c2[nH]c(=Cc3[nH]c(C=C4NC(=O)C(=C4C=C)C)c(c3CCC(=O)O)C)c(c2C)CCC(=O)O)C(=C1C=C)C, [O-]C(=O)CCc1c2[n-]c(cc3nc(cc4[nH]c(cc5nc(c2)C(=C5C)CCC(=O)O)c(C=C)c4C)C(C=C)=C3C)c1C>>O=C1N=C(C=c2[nH]c(=Cc3[nH]c(C=C4NC(=O)C(=C4C=C)C)c(c3CCC(=O)O)C)c(c2C)CCC(=O)O)C(=C1C=C)C, [O-]C(=O)CCc1c2[n-]c(cc3nc(cc4[nH]c(cc5nc(c2)C(=C5C)CCC(=O)O)c(C=C)c4C)C(C=C)=C3C)c1C>>O=C1N=C(C=c2[nH]c(=Cc3[nH]c(C=C4NC(=O)C(=C4C=C)C)c(c3CCC(=O)O)C)c(c2C)CCC(=O)O)C(=C1C=C)C, [O-]C(=O)CCc1c2[n-]c(cc3nc(cc4[nH]c(cc5nc(c2)C(=C5C)CCC(=O)O)c(C=C)c4C)C(C=C)=C3C)c1C>>O=C1N=C(C=c2[nH]c(=Cc3[nH]c(C=C4NC(=O)C(=C4C=C)C)c(c3CCC(=O)O)C)c(c2C)CCC(=O)O)C(=C1C=C)C, [O-]C(=O)CCc1c2[n-]c(cc3nc(cc4[nH]c(cc5nc(c2)C(=C5C)CCC(=O)O)c(C=C)c4C)C(C=C)=C3C)c1C>>O=C1N=C(C=c2[nH]c(=Cc3[nH]c(C=C4NC(=O)C(=C4C=C)C)c(c3CCC(=O)O)C)c(c2C)CCC(=O)O)C(=C1C=C)C, [O-]C(=O)CCc1c2[n-]c(cc3nc(cc4[nH]c(cc5nc(c2)C(=C5C)CCC(=O)O)c(C=C)c4C)C(C=C)=C3C)c1C>>O=C1N=C(C=c2[nH]c(=Cc3[nH]c(C=C4NC(=O)C(=C4C=C)C)c(c3CCC(=O)O)C)c(c2C)CCC(=O)O)C(=C1C=C)C, [O-]C(=O)CCc1c2[n-]c(cc3nc(cc4[nH]c(cc5nc(c2)C(=C5C)CCC(=O)O)c(C=C)c4C)C(C=C)=C3C)c1C>>O=C1N=C(C=c2[nH]c(=Cc3[nH]c(C=C4NC(=O)C(=C4C=C)C)c(c3CCC(=O)O)C)c(c2C)CCC(=O)O)C(=C1C=C)C, [O-]C(=O)CCc1c2[n-]c(cc3nc(cc4[nH]c(cc5nc(c2)C(=C5C)CCC(=O)O)c(C=C)c4C)C(C=C)=C3C)c1C>>O=C1N=C(C=c2[nH]c(=Cc3[nH]c(C=C4NC(=O)C(=C4C=C)C)c(c3CCC(=O)O)C)c(c2C)CCC(=O)O)C(=C1C=C)C]
2: R:M00001, P:M00005	[[O-]C(=O)CCc1c2[n-]c(cc3nc(cc4[nH]c(cc5nc(c2)C(=C5C)CCC(=O)O)c(C=C)c4C)C(C=C)=C3C)c1C>>[C-]#[O+]]
3: R:M00003, P:M00005	[O=O>>[C-]#[O+]]
4: R:M00003, P:M00006	[O=O>>O]


//
SELECTED AAM MAPPING
[Fe+2:45].[O:43]=[O:44].[O:1]=[C:2]([O-:3])[CH2:4][CH2:5][C:6]:1:[C:7]:2:[N-:8]:[C:9](:[CH:10]:[C:11]3:[N:12]:[C:13](:[CH:14]:[C:15]:4:[NH:16]:[C:17](:[CH:18]:[C:19]5:[N:20]:[C:21](:[CH:22]2)[C:23](=[C:24]5[CH3:25])[CH2:26][CH2:27][C:28](=[O:29])[OH:30]):[C:31]([CH:32]=[CH2:33]):[C:34]4[CH3:35])[C:36]([CH:37]=[CH2:38])=[C:39]3[CH3:40]):[C:41]1[CH3:42]>>[Fe+2:45].[C-:25]#[O+:44].[O:46]=[C:13]1[N:12]=[C:11]([CH:10]=[C:9]2[NH:8][C:7](=[CH:22][C:21]:3:[NH:20]:[C:19]([CH:18]=[C:17]4[NH:16][C:15](=[O:47])[C:34](=[C:31]4[CH:32]=[CH2:33])[CH3:35]):[C:24](:[C:23]3[CH2:26][CH2:27][C:28](=[O:29])[OH:30])[CH3:14])[C:6](=[C:41]2[CH3:42])[CH2:5][CH2:4][C:2](=[O:3])[OH:1])[C:39](=[C:36]1[CH:37]=[CH2:38])[CH3:40].[OH2:43]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=42, 2=41, 3=6, 4=7, 5=22, 6=21, 7=20, 8=19, 9=18, 10=17, 11=31, 12=34, 13=15, 14=16, 15=14, 16=13, 17=36, 18=39, 19=11, 20=12, 21=10, 22=9, 23=8, 24=40, 25=37, 26=38, 27=35, 28=32, 29=33, 30=24, 31=23, 32=26, 33=27, 34=28, 35=29, 36=30, 37=25, 38=5, 39=4, 40=2, 41=1, 42=3, 43=45, 44=43, 45=44}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=33, 2=32, 3=31, 4=8, 5=9, 6=10, 7=24, 8=23, 9=12, 10=11, 11=13, 12=14, 13=19, 14=18, 15=16, 16=17, 17=15, 18=22, 19=20, 20=21, 21=30, 22=25, 23=26, 24=27, 25=28, 26=29, 27=7, 28=6, 29=5, 30=4, 31=3, 32=2, 33=1, 34=40, 35=39, 36=43, 37=41, 38=42, 39=34, 40=35, 41=36, 42=37, 43=38, 44=46, 45=44, 46=45, 47=47}

