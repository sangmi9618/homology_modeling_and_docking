
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)OC([CH])([CH2])C:1.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>[C]OP(=O)(O)O:1.0, OCC(O)(C)C(O)[CH2]>>O=P(O)(O)OC(C)(CO)C(O)[CH2]:1.0, [CH]C(O)([CH2])C:1.0, [CH]C(O)([CH2])C>>O=P(O)(O)OC([CH])([CH2])C:1.0, [C]O:1.0, [C]O>>[C]O[P]:1.0, [C]OP(=O)(O)O:1.0, [C]O[P]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC([CH])([CH2])C:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[C]O[P]:1.0, [O]P(=O)(O)O:2.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)OC([CH])([CH2])C:1.0, O=P(O)(O)O[P]:1.0, [C]OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]P(=O)(O)O>>[O]P(=O)(O)O
2: [C]O>>[C]O[P]
3: [P]O[P]>>[P]O

MMP Level 2
1: O=P(O)(O)O[P]>>[C]OP(=O)(O)O
2: [CH]C(O)([CH2])C>>O=P(O)(O)OC([CH])([CH2])C
3: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O

MMP Level 3
1: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC([CH])([CH2])C
2: OCC(O)(C)C(O)[CH2]>>O=P(O)(O)OC(C)(CO)C(O)[CH2]
3: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)OCC(O)C(OP(=O)(O)O)(C)CO)C(O)C2O]
3: R:M00002, P:M00004	[O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)OCC(O)C(O)(C)CO)C(O)C2O>>O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)OCC(O)C(OP(=O)(O)O)(C)CO)C(O)C2O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[CH:6]=[CH:7][N:8]1[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][CH2:22][CH:23]([OH:24])[C:25]([OH:26])([CH3:27])[CH2:28][OH:29])[CH:30]([OH:31])[CH:32]2[OH:33].[O:34]=[P:35]([OH:36])([OH:37])[O:38][P:39](=[O:40])([OH:41])[O:42][P:43](=[O:44])([OH:45])[O:46][CH2:47][CH:48]1[O:49][CH:50]([N:51]:2:[CH:52]:[N:53]:[C:54]:3:[C:55](:[N:56]:[CH:57]:[N:58]:[C:59]32)[NH2:60])[CH:61]([OH:62])[CH:63]1[OH:64]>>[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[CH:6]=[CH:7][N:8]1[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][CH2:22][CH:23]([OH:24])[C:25]([O:26][P:35](=[O:34])([OH:36])[OH:37])([CH3:27])[CH2:28][OH:29])[CH:30]([OH:31])[CH:32]2[OH:33].[O:40]=[P:39]([OH:38])([OH:41])[O:42][P:43](=[O:44])([OH:45])[O:46][CH2:47][CH:48]1[O:49][CH:50]([N:51]:2:[CH:52]:[N:53]:[C:54]:3:[C:55](:[N:56]:[CH:57]:[N:58]:[C:59]32)[NH2:60])[CH:61]([OH:62])[CH:63]1[OH:64]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=57, 2=58, 3=59, 4=54, 5=55, 6=56, 7=60, 8=53, 9=52, 10=51, 11=50, 12=61, 13=63, 14=48, 15=49, 16=47, 17=46, 18=43, 19=44, 20=45, 21=42, 22=39, 23=40, 24=41, 25=38, 26=35, 27=34, 28=36, 29=37, 30=64, 31=62, 32=27, 33=25, 34=28, 35=29, 36=23, 37=22, 38=21, 39=18, 40=19, 41=20, 42=17, 43=14, 44=15, 45=16, 46=13, 47=12, 48=11, 49=30, 50=32, 51=9, 52=10, 53=8, 54=7, 55=6, 56=4, 57=3, 58=2, 59=1, 60=5, 61=33, 62=31, 63=24, 64=26}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=57, 2=58, 3=59, 4=54, 5=55, 6=56, 7=60, 8=53, 9=52, 10=51, 11=50, 12=61, 13=63, 14=48, 15=49, 16=47, 17=46, 18=43, 19=44, 20=45, 21=42, 22=39, 23=38, 24=40, 25=41, 26=64, 27=62, 28=31, 29=25, 30=32, 31=33, 32=23, 33=22, 34=21, 35=18, 36=19, 37=20, 38=17, 39=14, 40=15, 41=16, 42=13, 43=12, 44=11, 45=34, 46=36, 47=9, 48=10, 49=8, 50=7, 51=6, 52=4, 53=3, 54=2, 55=1, 56=5, 57=37, 58=35, 59=24, 60=26, 61=27, 62=28, 63=29, 64=30}

