
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:2.0, C=O:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(O)C(=O)C:1.0, O=C(O)C(N)C:1.0, O=C(O)C(N)C>>O=C(O)C(=O)C:2.0, O=C(O)C(N)C>>[C]C(=[C])CN:1.0, O=P(O)(O)O[CH2]>>[O-]P([O-])(=O)O[CH2]:2.0, O=[CH]:1.0, O=[CH]>>[C]=O:1.0, [CH2]:1.0, [CH2]N:1.0, [CH]:2.0, [CH]N:1.0, [CH]N>>[CH2]N:1.0, [C]:1.0, [C]=O:1.0, [C]C(=O)C:2.0, [C]C(=[C])C=O:1.0, [C]C(=[C])C=O>>O=C(O)C(=O)C:1.0, [C]C(=[C])C=O>>[C]C(=[C])CN:1.0, [C]C(=[C])CN:1.0, [C]C(N)C:2.0, [C]C(N)C>>[C]C(=O)C:1.0, [C]C(N)C>>[C]CN:1.0, [C]C(O)=C(C=O)C(=[CH])[CH2]>>[C]C(O)=C(C(=[CH])[CH2])CN:1.0, [C]C=O:2.0, [C]C=O>>[C]C(=O)C:1.0, [C]C=O>>[C]CN:1.0, [C]CN:2.0, [NH2]:2.0, [O-]:2.0, [O-][P]:2.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>[O]P([O-])([O-])=O:2.0, [O]P([O-])([O-])=O:2.0, [P]O:2.0, [P]O>>[O-][P]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[CH2]:1.0, [CH]:2.0, [C]:1.0, [NH2]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (8)
[O=[CH]:1.0, [CH2]N:1.0, [CH]N:1.0, [C]=O:1.0, [C]C(=O)C:1.0, [C]C(N)C:1.0, [C]C=O:1.0, [C]CN:1.0]


ID=Reaction Center at Level: 2 (8)
[O=C(O)C(=O)C:1.0, O=C(O)C(N)C:1.0, [C]C(=O)C:1.0, [C]C(=[C])C=O:1.0, [C]C(=[C])CN:1.0, [C]C(N)C:1.0, [C]C=O:1.0, [C]CN:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(=O)C:1.0, [C]C(N)C:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C(=O)C:1.0, O=C(O)C(N)C:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: O=[CH]>>[C]=O
2: [P]O>>[O-][P]
3: [C]C(N)C>>[C]C(=O)C
4: [CH]N>>[CH2]N
5: [C]C=O>>[C]CN

MMP Level 2
1: [C]C=O>>[C]C(=O)C
2: [O]P(=O)(O)O>>[O]P([O-])([O-])=O
3: O=C(O)C(N)C>>O=C(O)C(=O)C
4: [C]C(N)C>>[C]CN
5: [C]C(=[C])C=O>>[C]C(=[C])CN

MMP Level 3
1: [C]C(=[C])C=O>>O=C(O)C(=O)C
2: O=P(O)(O)O[CH2]>>[O-]P([O-])(=O)O[CH2]
3: O=C(O)C(N)C>>O=C(O)C(=O)C
4: O=C(O)C(N)C>>[C]C(=[C])CN
5: [C]C(O)=C(C=O)C(=[CH])[CH2]>>[C]C(O)=C(C(=[CH])[CH2])CN


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)C>>[O-]P([O-])(=O)OCc1cnc(c(O)c1CN)C]
2: R:M00001, P:M00004	[O=C(O)C(N)C>>O=C(O)C(=O)C]
3: R:M00002, P:M00003	[O=Cc1c(O)c(ncc1COP(=O)(O)O)C>>[O-]P([O-])(=O)OCc1cnc(c(O)c1CN)C, O=Cc1c(O)c(ncc1COP(=O)(O)O)C>>[O-]P([O-])(=O)OCc1cnc(c(O)c1CN)C]
4: R:M00002, P:M00004	[O=Cc1c(O)c(ncc1COP(=O)(O)O)C>>O=C(O)C(=O)C]


//
SELECTED AAM MAPPING
[O:1]=[CH:2][C:3]:1:[C:4]([OH:5]):[C:6](:[N:7]:[CH:8]:[C:9]1[CH2:10][O:11][P:12](=[O:13])([OH:14])[OH:15])[CH3:16].[O:17]=[C:18]([OH:19])[CH:20]([NH2:21])[CH3:22]>>[O:17]=[C:18]([OH:19])[C:20](=[O:1])[CH3:22].[O:13]=[P:12]([O-:15])([O-:14])[O:11][CH2:10][C:9]:1:[CH:8]:[N:7]:[C:6](:[C:4]([OH:5]):[C:3]1[CH2:2][NH2:21])[CH3:16]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=22, 2=20, 3=18, 4=17, 5=19, 6=21, 7=16, 8=6, 9=7, 10=8, 11=9, 12=3, 13=4, 14=5, 15=2, 16=1, 17=10, 18=11, 19=12, 20=13, 21=14, 22=15}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=16, 2=10, 3=9, 4=8, 5=7, 6=13, 7=11, 8=12, 9=14, 10=15, 11=6, 12=5, 13=2, 14=1, 15=3, 16=4, 17=22, 18=20, 19=21, 20=18, 21=17, 22=19}

