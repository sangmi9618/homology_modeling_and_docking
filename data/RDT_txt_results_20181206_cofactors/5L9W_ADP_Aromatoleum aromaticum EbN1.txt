
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0, C-O:1.0, O-P:2.0]

//
FINGERPRINTS RC
[O:3.0, O=C(C([CH])=[CH])C>>O=C(O)CC(=O)C([CH])=[CH]:1.0, O=C(O)[CH2]:1.0, O=P(O)(O)O:3.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O:1.0, O>>O=P(O)(O)O:2.0, O>>[P]O:1.0, [CH2]:1.0, [CH3]:1.0, [C]:2.0, [C]C:1.0, [C]C(=O)C:1.0, [C]C(=O)C>>[C]C(=O)CC(=O)O:1.0, [C]C(=O)CC(=O)O:1.0, [C]C>>[C]C[C]:1.0, [C]CC(=O)O:1.0, [C]C[C]:1.0, [C][O-]:1.0, [O-]:1.0, [O-]C(=O)O:3.0, [O-]C(=O)O>>O=C(O)[CH2]:1.0, [O-]C(=O)O>>[C]C(=O)CC(=O)O:1.0, [O-]C(=O)O>>[C]CC(=O)O:1.0, [OH]:2.0, [O]:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (7)
[O:1.0, [CH2]:1.0, [C]:2.0, [O-]:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (9)
[O:1.0, O=C(O)[CH2]:1.0, O=P(O)(O)O:1.0, [C]C[C]:1.0, [C][O-]:1.0, [O-]C(=O)O:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (7)
[O:1.0, O=P(O)(O)O:2.0, O=P(O)(O)O[P]:1.0, [C]C(=O)CC(=O)O:1.0, [C]CC(=O)O:1.0, [O-]C(=O)O:2.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O>>[P]O
2: [C]C>>[C]C[C]
3: [O-]C(=O)O>>O=C(O)[CH2]
4: [P]O[P]>>[P]O
5: [O]P(=O)(O)O>>O=P(O)(O)O

MMP Level 2
1: O>>O=P(O)(O)O
2: [C]C(=O)C>>[C]C(=O)CC(=O)O
3: [O-]C(=O)O>>[C]CC(=O)O
4: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
5: O=P(O)(O)O[P]>>O=P(O)(O)O

MMP Level 3
1: O>>O=P(O)(O)O
2: O=C(C([CH])=[CH])C>>O=C(O)CC(=O)C([CH])=[CH]
3: [O-]C(=O)O>>[C]C(=O)CC(=O)O
4: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
5: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00006	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00007	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)O]
3: R:M00002, P:M00008	[O=C(c1ccccc1)C>>O=C(O)CC(=O)c1ccccc1]
4: R:M00003, P:M00008	[[O-]C(=O)O>>O=C(O)CC(=O)c1ccccc1]
5: R:M00004, P:M00007	[O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[H+:46].[O:41]=[C:42]([O-:45])[OH:43].[O:32]=[C:33]([C:34]:1:[CH:35]:[CH:36]:[CH:37]:[CH:38]:[CH:39]1)[CH3:40].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31].[OH2:44]>>[O:41]=[C:42]([OH:43])[CH2:40][C:33](=[O:32])[C:34]:1:[CH:39]:[CH:38]:[CH:37]:[CH:36]:[CH:35]1.[O:1]=[P:2]([OH:4])([OH:44])[OH:3].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=40, 33=33, 34=32, 35=34, 36=35, 37=36, 38=37, 39=38, 40=39, 41=42, 42=41, 43=44, 44=43, 45=45, 46=46}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=17, 5=18, 6=19, 7=23, 8=16, 9=15, 10=14, 11=13, 12=24, 13=26, 14=11, 15=12, 16=10, 17=9, 18=6, 19=7, 20=8, 21=5, 22=2, 23=1, 24=3, 25=4, 26=27, 27=25, 28=42, 29=41, 30=40, 31=43, 32=44, 33=37, 34=36, 35=35, 36=34, 37=39, 38=38, 39=32, 40=33, 41=31, 42=29, 43=28, 44=30}

