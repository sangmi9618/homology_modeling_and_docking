
//
FINGERPRINTS BC

ORDER_CHANGED
[C-O*C=O:1.0]

//
FINGERPRINTS RC
[O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1:1.0, O=CCC(C)C>>OCCC(C)C:1.0, O=CC[CH]:1.0, O=CC[CH]>>[CH]CCO:2.0, O=C[CH2]:2.0, O=C[CH2]>>OC[CH2]:2.0, O=[CH]:1.0, O=[CH]>>O[CH2]:1.0, OC[CH2]:2.0, O[CH2]:1.0, [CH2]:2.0, [CH]:2.0, [CH]CCO:1.0, [CH]N([CH])[CH]:1.0, [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]:1.0, [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O:1.0, [C]C=[CH]:1.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C=[CH]:1.0, [N+]:1.0, [N]:1.0, [OH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH2]:1.0, [CH]:1.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C[CH2]:1.0, O=[CH]:1.0, OC[CH2]:1.0, O[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=CC[CH]:1.0, O=C[CH2]:1.0, OC[CH2]:1.0, [CH]CCO:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=C[CH2]>>OC[CH2]
2: [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]
3: [C]C[CH]>>[C]C=[CH]
4: O=[CH]>>O[CH2]

MMP Level 2
1: O=CC[CH]>>[CH]CCO
2: [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]
3: [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]
4: O=C[CH2]>>OC[CH2]

MMP Level 3
1: O=CCC(C)C>>OCCC(C)C
2: [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O
3: O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1
4: O=CC[CH]>>[CH]CCO


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=CCC(C)C>>OCCC(C)C, O=CCC(C)C>>OCCC(C)C]
2: R:M00002, P:M00005	[O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O]


//
SELECTED AAM MAPPING
[H+:55].[O:49]=[CH:50][CH2:51][CH:52]([CH3:53])[CH3:54].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:5][N:6]([CH:7]=[CH:8][CH2:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48]>>[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:9]:[CH:8]:[CH:7]:[N+:6](:[CH:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48].[OH:49][CH2:50][CH2:51][CH:52]([CH3:53])[CH3:54]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=53, 2=52, 3=54, 4=51, 5=50, 6=49, 7=9, 8=8, 9=7, 10=6, 11=5, 12=4, 13=2, 14=1, 15=3, 16=10, 17=47, 18=45, 19=12, 20=11, 21=13, 22=14, 23=15, 24=16, 25=17, 26=18, 27=19, 28=20, 29=21, 30=22, 31=23, 32=24, 33=43, 34=37, 35=26, 36=25, 37=27, 38=28, 39=29, 40=30, 41=35, 42=34, 43=33, 44=32, 45=31, 46=36, 47=38, 48=39, 49=40, 50=41, 51=42, 52=44, 53=46, 54=48, 55=55}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=53, 2=52, 3=54, 4=51, 5=50, 6=49, 7=6, 8=5, 9=4, 10=9, 11=8, 12=7, 13=10, 14=47, 15=45, 16=12, 17=11, 18=13, 19=14, 20=15, 21=16, 22=17, 23=18, 24=19, 25=20, 26=21, 27=22, 28=23, 29=24, 30=43, 31=37, 32=26, 33=25, 34=27, 35=28, 36=29, 37=30, 38=35, 39=34, 40=33, 41=32, 42=31, 43=36, 44=38, 45=39, 46=40, 47=41, 48=42, 49=44, 50=46, 51=48, 52=2, 53=1, 54=3}

