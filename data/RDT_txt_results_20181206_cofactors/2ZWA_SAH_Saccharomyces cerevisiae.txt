
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:2.0, C-S:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

//
FINGERPRINTS RC
[O=C(O)C(N)C[CH2]>>[O]C(=O)C(NC(=O)OC)C[CH2]:1.0, O=C(O)C([CH2])N>>O=C(OC)C([NH])[CH2]:1.0, O=C(OC)N[CH]:1.0, O=C([CH])O:1.0, O=C([CH])O>>O=C([CH])OC:1.0, O=C([CH])OC:1.0, O=C([NH])OC:1.0, O=C=O:3.0, O=C=O>>O=C(OC)N[CH]:2.0, O=C=O>>O=C([NH])OC:1.0, O=C=O>>[C]C([CH2])NC(=O)OC:1.0, O=C=O>>[O]C(=O)[NH]:1.0, S([CH2])[CH2]:1.0, [CH2][S+]([CH2])C:2.0, [CH2][S+]([CH2])C>>S([CH2])[CH2]:1.0, [CH2][S+]([CH2])C>>[C]OC:1.0, [CH3]:2.0, [CH]CSC[CH2]:1.0, [CH]C[S+](C)C[CH2]:1.0, [CH]C[S+](C)C[CH2]>>O=C([NH])OC:1.0, [CH]C[S+](C)C[CH2]>>[CH]CSC[CH2]:1.0, [CH]N:1.0, [CH]N>>[C]N[CH]:1.0, [C]:2.0, [C]=O:1.0, [C]=O>>[C]OC:1.0, [C]C([CH2])N:1.0, [C]C([CH2])N>>[C]C([CH2])NC([O])=O:1.0, [C]C([CH2])NC([O])=O:1.0, [C]N[CH]:1.0, [C]O:1.0, [C]O>>[C]OC:1.0, [C]OC:3.0, [NH2]:1.0, [NH]:1.0, [OH]:1.0, [O]:3.0, [O]C:1.0, [O]C(=O)[NH]:1.0, [O]C([CH])C[S+](C)CC[CH]>>[O]C([CH])CSCC[CH]:1.0, [S+]:1.0, [S+]C:1.0, [S+]C>>[O]C:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[CH3]:3.0, [C]:1.0, [NH]:1.0, [O]:2.0, [S+]:1.0]


ID=Reaction Center at Level: 1 (6)
[[CH2][S+]([CH2])C:1.0, [C]N[CH]:1.0, [C]OC:2.0, [O]C:2.0, [O]C(=O)[NH]:1.0, [S+]C:1.0]


ID=Reaction Center at Level: 2 (7)
[O=C(OC)N[CH]:1.0, O=C([CH])OC:1.0, O=C([NH])OC:1.0, [CH2][S+]([CH2])C:1.0, [CH]C[S+](C)C[CH2]:1.0, [C]C([CH2])NC([O])=O:1.0, [C]OC:2.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (2)
[[C]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (4)
[O=C=O:1.0, [C]=O:1.0, [C]OC:1.0, [O]C(=O)[NH]:1.0]


ID=Reaction Center at Level: 2 (3)
[O=C(OC)N[CH]:1.0, O=C([NH])OC:1.0, O=C=O:2.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=C=O>>[O]C(=O)[NH]
2: [CH2][S+]([CH2])C>>S([CH2])[CH2]
3: [C]=O>>[C]OC
4: [CH]N>>[C]N[CH]
5: [C]O>>[C]OC
6: [S+]C>>[O]C

MMP Level 2
1: O=C=O>>O=C(OC)N[CH]
2: [CH]C[S+](C)C[CH2]>>[CH]CSC[CH2]
3: O=C=O>>O=C([NH])OC
4: [C]C([CH2])N>>[C]C([CH2])NC([O])=O
5: O=C([CH])O>>O=C([CH])OC
6: [CH2][S+]([CH2])C>>[C]OC

MMP Level 3
1: O=C=O>>[C]C([CH2])NC(=O)OC
2: [O]C([CH])C[S+](C)CC[CH]>>[O]C([CH])CSCC[CH]
3: O=C=O>>O=C(OC)N[CH]
4: O=C(O)C(N)C[CH2]>>[O]C(=O)C(NC(=O)OC)C[CH2]
5: O=C(O)C([CH2])N>>O=C(OC)C([NH])[CH2]
6: [CH]C[S+](C)C[CH2]>>O=C([NH])OC


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(O)C(N)CCSCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00005	[O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=c1c2ncn(c2n(c3nc(c(n13)CCC(NC(=O)OC)C(=O)OC)C)C)C4OC(CO)C(O)C4O]
3: R:M00002, P:M00005	[O=C=O>>O=c1c2ncn(c2n(c3nc(c(n13)CCC(NC(=O)OC)C(=O)OC)C)C)C4OC(CO)C(O)C4O, O=C=O>>O=c1c2ncn(c2n(c3nc(c(n13)CCC(NC(=O)OC)C(=O)OC)C)C)C4OC(CO)C(O)C4O]
4: R:M00003, P:M00005	[O=c1c2ncn(c2n(c3nc(c(n13)CCC(N)C(=O)O)C)C)C4OC(CO)C(O)C4O>>O=c1c2ncn(c2n(c3nc(c(n13)CCC(NC(=O)OC)C(=O)OC)C)C)C4OC(CO)C(O)C4O, O=c1c2ncn(c2n(c3nc(c(n13)CCC(N)C(=O)O)C)C)C4OC(CO)C(O)C4O>>O=c1c2ncn(c2n(c3nc(c(n13)CCC(NC(=O)OC)C(=O)OC)C)C)C4OC(CO)C(O)C4O]


//
SELECTED AAM MAPPING
[O:59]=[C:60]=[O:61].[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][C:8]:1:[C:9](:[N:10]:[C:11]2:[N:12]1[C:13](=[O:14])[C:15]:3:[N:16]:[CH:17]:[N:18](:[C:19]3[N:20]2[CH3:21])[CH:22]4[O:23][CH:24]([CH2:25][OH:26])[CH:27]([OH:28])[CH:29]4[OH:30])[CH3:31].[O:32]=[C:33]([OH:34])[CH:35]([NH2:36])[CH2:37][CH2:38][S+:39]([CH3:40])[CH2:41][CH:42]1[O:43][CH:44]([N:45]:2:[CH:46]:[N:47]:[C:48]:3:[C:49](:[N:50]:[CH:51]:[N:52]:[C:53]32)[NH2:54])[CH:55]([OH:56])[CH:57]1[OH:58]>>[O:32]=[C:33]([OH:34])[CH:35]([NH2:36])[CH2:37][CH2:38][S:39][CH2:41][CH:42]1[O:43][CH:44]([N:45]:2:[CH:46]:[N:47]:[C:48]:3:[C:49](:[N:50]:[CH:51]:[N:52]:[C:53]32)[NH2:54])[CH:55]([OH:56])[CH:57]1[OH:58].[O:59]=[C:60]([O:61][CH3:40])[NH:5][CH:4]([C:2](=[O:1])[O:3][CH3:62])[CH2:6][CH2:7][C:8]:1:[C:9](:[N:10]:[C:11]2:[N:12]1[C:13](=[O:14])[C:15]:3:[N:16]:[CH:17]:[N:18](:[C:19]3[N:20]2[CH3:21])[CH:22]4[O:23][CH:24]([CH2:25][OH:26])[CH:27]([OH:28])[CH:29]4[OH:30])[CH3:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=40, 2=39, 3=38, 4=37, 5=35, 6=33, 7=32, 8=34, 9=36, 10=41, 11=42, 12=57, 13=55, 14=44, 15=43, 16=45, 17=46, 18=47, 19=48, 20=53, 21=52, 22=51, 23=50, 24=49, 25=54, 26=56, 27=58, 28=60, 29=59, 30=61, 31=31, 32=9, 33=8, 34=12, 35=13, 36=14, 37=15, 38=19, 39=20, 40=11, 41=10, 42=21, 43=18, 44=17, 45=16, 46=22, 47=29, 48=27, 49=24, 50=23, 51=25, 52=26, 53=28, 54=30, 55=7, 56=6, 57=4, 58=2, 59=1, 60=3, 61=5}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=55, 2=56, 3=57, 4=52, 5=53, 6=54, 7=58, 8=51, 9=50, 10=49, 11=48, 12=59, 13=61, 14=46, 15=47, 16=45, 17=44, 18=43, 19=42, 20=40, 21=38, 22=37, 23=39, 24=41, 25=62, 26=60, 27=36, 28=14, 29=13, 30=17, 31=18, 32=19, 33=20, 34=24, 35=25, 36=16, 37=15, 38=26, 39=23, 40=22, 41=21, 42=27, 43=34, 44=32, 45=29, 46=28, 47=30, 48=31, 49=33, 50=35, 51=12, 52=11, 53=6, 54=7, 55=8, 56=9, 57=10, 58=5, 59=2, 60=1, 61=3, 62=4}

