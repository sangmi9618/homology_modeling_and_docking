
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-Cl:1.0, C-S:1.0]

//
FINGERPRINTS RC
[Cl:3.0, S[CH2]:1.0, S[CH2]>>[C]S[CH2]:1.0, [CH]CS:1.0, [CH]CS>>[C]=C([CH])SC[CH]:1.0, [C]:2.0, [C]=C(Cl)[CH]:2.0, [C]=C(Cl)[CH]>>Cl:1.0, [C]=C(Cl)[CH]>>[C]=C([S])[CH]:1.0, [C]=C([CH])SC[CH]:1.0, [C]=C([S])[CH]:1.0, [C]C([NH])CS>>[C]C([NH])CSC(C=[CH])=C([N+])[CH]:1.0, [C]Cl:1.0, [C]Cl>>Cl:1.0, [C]S[CH2]:1.0, [Cl]:1.0, [N+]C([CH])=C(Cl)C=[CH]:1.0, [N+]C([CH])=C(Cl)C=[CH]>>Cl:1.0, [N+]C([CH])=C(Cl)C=[CH]>>[N+]C([CH])=C(S[CH2])C=[CH]:1.0, [N+]C([CH])=C(S[CH2])C=[CH]:1.0, [O-][N+](=O)C=1C=[C]C=CC1Cl>>[O-][N+](=O)C=1C=[C]C=CC1SC[CH]:1.0, [SH]:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[Cl:1.0, [C]:2.0, [Cl]:1.0, [S]:1.0]


ID=Reaction Center at Level: 1 (5)
[Cl:1.0, [C]=C(Cl)[CH]:1.0, [C]=C([S])[CH]:1.0, [C]Cl:1.0, [C]S[CH2]:1.0]


ID=Reaction Center at Level: 2 (5)
[Cl:1.0, [C]=C(Cl)[CH]:1.0, [C]=C([CH])SC[CH]:1.0, [N+]C([CH])=C(Cl)C=[CH]:1.0, [N+]C([CH])=C(S[CH2])C=[CH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]=C(Cl)[CH]>>[C]=C([S])[CH]
2: [C]Cl>>Cl
3: S[CH2]>>[C]S[CH2]

MMP Level 2
1: [N+]C([CH])=C(Cl)C=[CH]>>[N+]C([CH])=C(S[CH2])C=[CH]
2: [C]=C(Cl)[CH]>>Cl
3: [CH]CS>>[C]=C([CH])SC[CH]

MMP Level 3
1: [O-][N+](=O)C=1C=[C]C=CC1Cl>>[O-][N+](=O)C=1C=[C]C=CC1SC[CH]
2: [N+]C([CH])=C(Cl)C=[CH]>>Cl
3: [C]C([NH])CS>>[C]C([NH])CSC(C=[CH])=C([N+])[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)CNC(=O)C(NC(=O)CCC(N)C(=O)O)CS>>[O-][N+](=O)c1ccc(SCC(NC(=O)CCC(N)C(=O)O)C(=O)NCC(=O)O)c(c1)[N+]([O-])=O]
2: R:M00002, P:M00003	[[O-][N+](=O)c1ccc(Cl)c(c1)[N+]([O-])=O>>[O-][N+](=O)c1ccc(SCC(NC(=O)CCC(N)C(=O)O)C(=O)NCC(=O)O)c(c1)[N+]([O-])=O]
3: R:M00002, P:M00004	[[O-][N+](=O)c1ccc(Cl)c(c1)[N+]([O-])=O>>Cl]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH2:4][NH:5][C:6](=[O:7])[CH:8]([NH:9][C:10](=[O:11])[CH2:12][CH2:13][CH:14]([NH2:15])[C:16](=[O:17])[OH:18])[CH2:19][SH:20].[O:21]=[N+:22]([O-:23])[C:24]:1:[CH:25]:[CH:26]:[C:27]([Cl:28]):[C:29](:[CH:30]1)[N+:31](=[O:32])[O-:33]>>[ClH:28].[O:1]=[C:2]([OH:3])[CH2:4][NH:5][C:6](=[O:7])[CH:8]([NH:9][C:10](=[O:11])[CH2:12][CH2:13][CH:14]([NH2:15])[C:16](=[O:17])[OH:18])[CH2:19][S:20][C:27]:1:[CH:26]:[CH:25]:[C:24](:[CH:30]:[C:29]1[N+:31](=[O:32])[O-:33])[N+:22](=[O:21])[O-:23]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=13, 2=12, 3=10, 4=11, 5=9, 6=8, 7=19, 8=20, 9=6, 10=7, 11=5, 12=4, 13=2, 14=1, 15=3, 16=14, 17=16, 18=17, 19=18, 20=15, 21=25, 22=26, 23=27, 24=29, 25=30, 26=24, 27=22, 28=21, 29=23, 30=31, 31=32, 32=33, 33=28}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=23, 2=22, 3=21, 4=26, 5=25, 6=24, 7=30, 8=31, 9=32, 10=27, 11=28, 12=29, 13=20, 14=19, 15=8, 16=6, 17=7, 18=5, 19=4, 20=2, 21=1, 22=3, 23=9, 24=10, 25=11, 26=12, 27=13, 28=14, 29=16, 30=17, 31=18, 32=15, 33=33}

