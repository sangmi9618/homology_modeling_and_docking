
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0, C-S:1.0, O-P:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O=P(O)(O)O:3.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O:1.0, O>>O=P(O)(O)O:2.0, O>>[P]O:1.0, S(C)C[CH2]:1.0, S(C)C[CH2]>>[CH]C[S+](C)C[CH2]:1.0, S([CH2])C:1.0, S([CH2])C>>[CH2][S+]([CH2])C:1.0, [CH2]:2.0, [CH2][S+]([CH2])C:1.0, [CH]:2.0, [CH]CCSC>>[O]C([CH])C[S+](C)CC[CH]:1.0, [CH]C[S+](C)C[CH2]:1.0, [N]C1OC(CO[P])C(O)C1O>>[N]C1OC(C[S+]([CH2])C)C(O)C1O:1.0, [OH]:3.0, [O]:2.0, [O]C([CH])COP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, [O]C([CH])CO[P]:1.0, [O]C([CH])CO[P]>>[O]C([CH])C[S+]([CH2])C:1.0, [O]C([CH])C[S+]([CH2])C:1.0, [O]C([CH])[CH2]:2.0, [O]C([CH])[CH2]>>[O]C([CH])[CH2]:1.0, [O]CC1O[CH][CH]C1O:1.0, [O]CC1O[CH][CH]C1O>>[S+]CC1O[CH][CH]C1O:1.0, [O]C[CH]:1.0, [O]C[CH]>>[S+]C[CH]:1.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OCC1O[CH][CH]C1O>>OC1[CH][CH]OC1C[S+](C)C[CH2]:1.0, [O]P(=O)(O)OC[CH]:1.0, [O]P(=O)(O)OC[CH]>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:3.0, [P]O[CH2]:1.0, [P]O[CH2]>>[P]O:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0, [S+]:1.0, [S+]CC1O[CH][CH]C1O:1.0, [S+]C[CH]:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (6)
[O:1.0, [CH2]:2.0, [OH]:1.0, [O]:2.0, [P]:2.0, [S+]:1.0]


ID=Reaction Center at Level: 1 (9)
[O:1.0, O=P(O)(O)O:1.0, [CH2][S+]([CH2])C:1.0, [O]C[CH]:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[P]:1.0, [S+]C[CH]:1.0]


ID=Reaction Center at Level: 2 (8)
[O:1.0, O=P(O)(O)O:2.0, O=P(O)(O)O[P]:1.0, [CH]C[S+](C)C[CH2]:1.0, [O]C([CH])CO[P]:1.0, [O]C([CH])C[S+]([CH2])C:1.0, [O]P(=O)(O)OC[CH]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (1)
[[O]C([CH])[CH2]:2.0]


ID=Reaction Center at Level: 2 (2)
[[O]CC1O[CH][CH]C1O:1.0, [S+]CC1O[CH][CH]C1O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: O>>[P]O
2: [O]C[CH]>>[S+]C[CH]
3: [O]P(=O)(O)O>>O=P(O)(O)O
4: [O]C([CH])[CH2]>>[O]C([CH])[CH2]
5: [P]O[P]>>[P]O
6: [P]O[CH2]>>[P]O
7: S([CH2])C>>[CH2][S+]([CH2])C

MMP Level 2
1: O>>O=P(O)(O)O
2: [O]C([CH])CO[P]>>[O]C([CH])C[S+]([CH2])C
3: O=P(O)(O)O[P]>>O=P(O)(O)O
4: [O]CC1O[CH][CH]C1O>>[S+]CC1O[CH][CH]C1O
5: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
6: [O]P(=O)(O)OC[CH]>>[O]P(=O)(O)O
7: S(C)C[CH2]>>[CH]C[S+](C)C[CH2]

MMP Level 3
1: O>>O=P(O)(O)O
2: [O]P(=O)(O)OCC1O[CH][CH]C1O>>OC1[CH][CH]OC1C[S+](C)C[CH2]
3: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O
4: [N]C1OC(CO[P])C(O)C1O>>[N]C1OC(C[S+]([CH2])C)C(O)C1O
5: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
6: [O]C([CH])COP(=O)(O)O[P]>>O=P(O)(O)O[P]
7: [CH]CCSC>>[O]C([CH])C[S+](C)CC[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)O]
2: R:M00001, P:M00005	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)O, O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)O]
3: R:M00001, P:M00006	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O, O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
4: R:M00002, P:M00006	[O=C(O)C(N)CCSC>>O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
5: R:M00003, P:M00004	[O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[O:32]=[C:33]([OH:34])[CH:35]([NH2:36])[CH2:37][CH2:38][S:39][CH3:40].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31].[OH2:41]>>[O:32]=[C:33]([OH:34])[CH:35]([NH2:36])[CH2:37][CH2:38][S+:39]([CH3:40])[CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31].[O:1]=[P:2]([OH:4])([OH:41])[OH:3].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[OH:13]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=40, 33=39, 34=38, 35=37, 36=35, 37=33, 38=32, 39=34, 40=36, 41=41}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=39, 2=38, 3=37, 4=40, 5=41, 6=30, 7=29, 8=28, 9=31, 10=32, 11=33, 12=34, 13=35, 14=36, 15=9, 16=8, 17=7, 18=6, 19=4, 20=2, 21=1, 22=3, 23=5, 24=10, 25=11, 26=26, 27=24, 28=13, 29=12, 30=14, 31=15, 32=16, 33=17, 34=22, 35=21, 36=20, 37=19, 38=18, 39=23, 40=25, 41=27}

