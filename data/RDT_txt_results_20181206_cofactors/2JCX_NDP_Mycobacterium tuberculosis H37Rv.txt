
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:2.0]

ORDER_CHANGED
[C-O*C=O:1.0]

//
FINGERPRINTS RC
[O=C(C)C(O)C(O)[CH2]:1.0, O=C(C)C(O)C(O)[CH2]>>OCC(O)(C)C(O)[CH2]:2.0, O=C(C)C([CH])O:1.0, O=C(C)C([CH])O>>OCC(O)(C)C(O)[CH2]:1.0, O=C(C)C([CH])O>>[CH]C(O)(C)CO:2.0, O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1:1.0, O=C([CH])C:3.0, O=C([CH])C>>[CH]C(O)([CH2])C:1.0, O=C([CH])C>>[C]CO:2.0, OCC(O)(C)C(O)[CH2]:1.0, O[CH2]:1.0, [CH2]:2.0, [CH3]:2.0, [CH]:2.0, [CH]C(O)(C)CO:1.0, [CH]C(O)([CH2])C:2.0, [CH]N([CH])[CH]:1.0, [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]:1.0, [C]:2.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=O:1.0, [C]=O>>O[CH2]:1.0, [C]C:2.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]:1.0, [C]C([CH])O:1.0, [C]C([CH])O>>[CH]C(O)([CH2])C:1.0, [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O:1.0, [C]C=[CH]:1.0, [C]C>>[C]C:1.0, [C]CO:2.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C=[CH]:1.0, [N+]:1.0, [N]:1.0, [OH]:1.0, [O]:1.0, [O]CC(O)C(O)C(=O)C>>[O]CC(O)C(O)(C)CO:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH3]:2.0, [C]:2.0]


ID=Reaction Center at Level: 1 (3)
[O=C([CH])C:1.0, [CH]C(O)([CH2])C:1.0, [C]C:2.0]


ID=Reaction Center at Level: 2 (4)
[O=C(C)C([CH])O:1.0, O=C([CH])C:1.0, OCC(O)(C)C(O)[CH2]:1.0, [CH]C(O)([CH2])C:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH2]:1.0, [C]:1.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([CH])C:1.0, O[CH2]:1.0, [C]=O:1.0, [C]CO:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(C)C([CH])O:1.0, O=C([CH])C:1.0, [CH]C(O)(C)CO:1.0, [C]CO:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C([CH])O>>[CH]C(O)([CH2])C
2: [C]C>>[C]C
3: [C]C[CH]>>[C]C=[CH]
4: [C]=O>>O[CH2]
5: [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]
6: O=C([CH])C>>[C]CO

MMP Level 2
1: O=C(C)C(O)C(O)[CH2]>>OCC(O)(C)C(O)[CH2]
2: O=C([CH])C>>[CH]C(O)([CH2])C
3: [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]
4: O=C([CH])C>>[C]CO
5: [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]
6: O=C(C)C([CH])O>>[CH]C(O)(C)CO

MMP Level 3
1: [O]CC(O)C(O)C(=O)C>>[O]CC(O)C(O)(C)CO
2: O=C(C)C([CH])O>>OCC(O)(C)C(O)[CH2]
3: O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1
4: O=C(C)C([CH])O>>[CH]C(O)(C)CO
5: [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O
6: O=C(C)C(O)C(O)[CH2]>>OCC(O)(C)C(O)[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00005	[O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O]
2: R:M00003, P:M00004	[O=C(C)C(O)C(O)COP(=O)(O)O>>O=P(O)(O)OCC(O)C(O)(C)CO, O=C(C)C(O)C(O)COP(=O)(O)O>>O=P(O)(O)OCC(O)C(O)(C)CO, O=C(C)C(O)C(O)COP(=O)(O)O>>O=P(O)(O)OCC(O)C(O)(C)CO, O=C(C)C(O)C(O)COP(=O)(O)O>>O=P(O)(O)OCC(O)C(O)(C)CO]


//
SELECTED AAM MAPPING
[H+:62].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:5][N:6]([CH:7]=[CH:8][CH2:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48].[O:49]=[C:50]([CH3:51])[CH:52]([OH:53])[CH:54]([OH:55])[CH2:56][O:57][P:58](=[O:59])([OH:60])[OH:61]>>[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:9]:[CH:8]:[CH:7]:[N+:6](:[CH:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48].[O:59]=[P:58]([OH:60])([OH:61])[O:57][CH2:56][CH:54]([OH:55])[C:52]([OH:53])([CH3:51])[CH2:50][OH:49]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=5, 6=4, 7=2, 8=1, 9=3, 10=10, 11=47, 12=45, 13=12, 14=11, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=43, 28=37, 29=26, 30=25, 31=27, 32=28, 33=29, 34=30, 35=35, 36=34, 37=33, 38=32, 39=31, 40=36, 41=38, 42=39, 43=40, 44=41, 45=42, 46=44, 47=46, 48=48, 49=62, 50=51, 51=50, 52=49, 53=52, 54=54, 55=56, 56=57, 57=58, 58=59, 59=60, 60=61, 61=55, 62=53}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=59, 2=57, 3=60, 4=61, 5=55, 6=54, 7=53, 8=50, 9=49, 10=51, 11=52, 12=56, 13=58, 14=6, 15=5, 16=4, 17=9, 18=8, 19=7, 20=10, 21=47, 22=45, 23=12, 24=11, 25=13, 26=14, 27=15, 28=16, 29=17, 30=18, 31=19, 32=20, 33=21, 34=22, 35=23, 36=24, 37=43, 38=37, 39=26, 40=25, 41=27, 42=28, 43=29, 44=30, 45=35, 46=34, 47=33, 48=32, 49=31, 50=36, 51=38, 52=39, 53=40, 54=41, 55=42, 56=44, 57=46, 58=48, 59=2, 60=1, 61=3}

