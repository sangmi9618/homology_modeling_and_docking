
//
FINGERPRINTS BC

ORDER_CHANGED
[C%C*C%C:1.0, C%N*C%N:2.0, O-O*O=O:1.0]

//
FINGERPRINTS RC
[O=C1N[C]NC2=C1NC([CH])=[C]N2[CH2]>>O=C1N[C]N=C2C1=NC([CH])=[C]N2[CH2]:1.0, O=O:4.0, O=O>>OO:6.0, O=c1[nH]c(=O)c2N[C]=C([CH])N(c2[nH]1)C[CH]>>O=C1N=C2C(=N[C]=C([CH])N2C[CH])C(=O)N1:1.0, OO:4.0, [C]:4.0, [C]=C([N])NC(=O)[NH]:1.0, [C]=C([N])NC(=O)[NH]>>[C]C([N])=NC(=O)[NH]:1.0, [C]=C([N])[NH]:1.0, [C]=C([N])[NH]>>[C]C([N])=[N]:1.0, [C]=CC=1NC(=C([NH])[N]C1[CH])C(=O)[NH]>>[C]=CC=1N=C(C(=[N])[N]C1[CH])C(=O)[NH]:1.0, [C]C(=[C])NC(=[C])[CH]:1.0, [C]C(=[C])NC(=[C])[CH]>>[C]C([C])=NC(=[C])[CH]:1.0, [C]C(=[C])[NH]:1.0, [C]C(=[C])[NH]>>[C]C([C])=[N]:1.0, [C]C([C])=NC(=[C])[CH]:1.0, [C]C([C])=[N]:1.0, [C]C([N])=NC(=O)[NH]:1.0, [C]C([N])=[N]:1.0, [C]N([CH2])C=1NC(=O)N[C]C1[NH]>>[C]N([CH2])C1=NC(=O)N[C]C1=[N]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]:1.0, [C]N=[C]:2.0, [C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]NC(=C([C])[NH])N([C])[CH2]>>[C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]NC(=C([N])[NH])C(=O)[NH]:1.0, [C]NC(=C([N])[NH])C(=O)[NH]>>[C]N=C(C([N])=[N])C(=O)[NH]:1.0, [C]N[C]:2.0, [C]N[C]>>[C]N=[C]:2.0, [NH]:2.0, [N]:2.0, [OH]:2.0, [O]:2.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (5)
[[C]:4.0, [NH]:2.0, [N]:2.0, [OH]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (8)
[O=O:2.0, OO:2.0, [C]=C([N])[NH]:1.0, [C]C(=[C])[NH]:1.0, [C]C([C])=[N]:1.0, [C]C([N])=[N]:1.0, [C]N=[C]:2.0, [C]N[C]:2.0]


ID=Reaction Center at Level: 2 (10)
[O=O:2.0, OO:2.0, [C]=C([N])NC(=O)[NH]:1.0, [C]C(=[C])NC(=[C])[CH]:1.0, [C]C([C])=NC(=[C])[CH]:1.0, [C]C([N])=NC(=O)[NH]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]:1.0, [C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]NC(=C([N])[NH])C(=O)[NH]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C(=[C])[NH]>>[C]C([C])=[N]
2: [C]=C([N])[NH]>>[C]C([N])=[N]
3: O=O>>OO
4: [C]N[C]>>[C]N=[C]
5: [C]N[C]>>[C]N=[C]

MMP Level 2
1: [C]NC(=C([N])[NH])C(=O)[NH]>>[C]N=C(C([N])=[N])C(=O)[NH]
2: [C]NC(=C([C])[NH])N([C])[CH2]>>[C]N=C(C([C])=[N])N([C])[CH2]
3: O=O>>OO
4: [C]=C([N])NC(=O)[NH]>>[C]C([N])=NC(=O)[NH]
5: [C]C(=[C])NC(=[C])[CH]>>[C]C([C])=NC(=[C])[CH]

MMP Level 3
1: O=C1N[C]NC2=C1NC([CH])=[C]N2[CH2]>>O=C1N[C]N=C2C1=NC([CH])=[C]N2[CH2]
2: O=c1[nH]c(=O)c2N[C]=C([CH])N(c2[nH]1)C[CH]>>O=C1N=C2C(=N[C]=C([CH])N2C[CH])C(=O)N1
3: O=O>>OO
4: [C]N([CH2])C=1NC(=O)N[C]C1[NH]>>[C]N([CH2])C1=NC(=O)N[C]C1=[N]
5: [C]=CC=1NC(=C([NH])[N]C1[CH])C(=O)[NH]>>[C]=CC=1N=C(C(=[N])[N]C1[CH])C(=O)[NH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C>>O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1, O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C>>O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1, O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C>>O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1, O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C>>O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1]
2: R:M00002, P:M00004	[O=O>>OO]


//
SELECTED AAM MAPPING
[O:54]=[O:55].[O:1]=[C:2]1[NH:3][C:4](=[O:5])[C:6]=2[NH:7][C:8]:3:[CH:9]:[C:10](:[C:11](:[CH:12]:[C:13]3[N:14]([C:15]2[NH:16]1)[CH2:17][CH:18]([OH:19])[CH:20]([OH:21])[CH:22]([OH:23])[CH2:24][O:25][P:26](=[O:27])([OH:28])[O:29][P:30](=[O:31])([OH:32])[O:33][CH2:34][CH:35]4[O:36][CH:37]([N:38]:5:[CH:39]:[N:40]:[C:41]:6:[C:42](:[N:43]:[CH:44]:[N:45]:[C:46]65)[NH2:47])[CH:48]([OH:49])[CH:50]4[OH:51])[CH3:52])[CH3:53]>>[O:1]=[C:2]1[N:16]=[C:15]2[C:6](=[N:7][C:8]:3:[CH:9]:[C:10](:[C:11](:[CH:12]:[C:13]3[N:14]2[CH2:17][CH:18]([OH:19])[CH:20]([OH:21])[CH:22]([OH:23])[CH2:24][O:25][P:26](=[O:27])([OH:28])[O:29][P:30](=[O:31])([OH:32])[O:33][CH2:34][CH:35]4[O:36][CH:37]([N:38]:5:[CH:39]:[N:40]:[C:41]:6:[C:42](:[N:43]:[CH:44]:[N:45]:[C:46]65)[NH2:47])[CH:48]([OH:49])[CH:50]4[OH:51])[CH3:52])[CH3:53])[C:4](=[O:5])[NH:3]1.[OH:54][OH:55]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=53, 2=10, 3=9, 4=8, 5=13, 6=12, 7=11, 8=52, 9=14, 10=15, 11=6, 12=7, 13=4, 14=5, 15=3, 16=2, 17=1, 18=16, 19=17, 20=18, 21=20, 22=22, 23=24, 24=25, 25=26, 26=27, 27=28, 28=29, 29=30, 30=31, 31=32, 32=33, 33=34, 34=35, 35=50, 36=48, 37=37, 38=36, 39=38, 40=39, 41=40, 42=41, 43=46, 44=45, 45=44, 46=43, 47=42, 48=47, 49=49, 50=51, 51=23, 52=21, 53=19, 54=54, 55=55}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=50, 2=9, 3=8, 4=7, 5=12, 6=11, 7=10, 8=49, 9=13, 10=4, 11=3, 12=2, 13=1, 14=53, 15=51, 16=52, 17=5, 18=6, 19=14, 20=15, 21=17, 22=19, 23=21, 24=22, 25=23, 26=24, 27=25, 28=26, 29=27, 30=28, 31=29, 32=30, 33=31, 34=32, 35=47, 36=45, 37=34, 38=33, 39=35, 40=36, 41=37, 42=38, 43=43, 44=42, 45=41, 46=40, 47=39, 48=44, 49=46, 50=48, 51=20, 52=18, 53=16, 54=54, 55=55}

