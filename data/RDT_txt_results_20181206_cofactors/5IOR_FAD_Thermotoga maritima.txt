
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%N:2.0, C-C:1.0]

ORDER_CHANGED
[C%C*C%C:1.0, C%N*C%N:2.0, O-O*O=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C1C=CN([CH])[C]N1>>O=C1N[C]N([CH])C=C1C:1.0, O=C1N[C]NC2=C1NC([CH])=[C]N2[CH2]>>O=C1N[C]N=C2C1=NC([CH])=[C]N2[CH2]:1.0, O=O:4.0, O=O>>OO:6.0, O=c1[nH]c(=O)c2N[C]=C([CH])N(c2[nH]1)C[CH]>>O=C1N=C2C(=N[C]=C([CH])N2C[CH])C(=O)N1:1.0, OO:4.0, [CH2]:1.0, [CH3]:1.0, [CH]:3.0, [CH]C(=[CH])N1C[N][CH]C1:1.0, [CH]C(=[CH])N1C[N][CH]C1>>[CH]C(=[CH])NC[CH]:1.0, [CH]C(=[CH])NC[CH]:1.0, [C]:5.0, [C]=C([N])NC(=O)[NH]:1.0, [C]=C([N])NC(=O)[NH]>>[C]C([N])=NC(=O)[NH]:1.0, [C]=C([N])[NH]:1.0, [C]=C([N])[NH]>>[C]C([N])=[N]:1.0, [C]=CC=1NC(=C([NH])[N]C1[CH])C(=O)[NH]>>[C]=CC=1N=C(C(=[N])[N]C1[CH])C(=O)[NH]:1.0, [C]C:1.0, [C]C(=[CH])C:2.0, [C]C(=[C])N1CN(C([CH])=[CH])CC1[CH2]>>[N]C=C(C(=O)[NH])C:1.0, [C]C(=[C])N1C[N]CC1[CH2]:1.0, [C]C(=[C])N1C[N]CC1[CH2]>>[C]C(=[C])NC([CH2])[CH2]:1.0, [C]C(=[C])NC(=[C])[CH]:1.0, [C]C(=[C])NC(=[C])[CH]>>[C]C([C])=NC(=[C])[CH]:1.0, [C]C(=[C])NC([CH2])[CH2]:1.0, [C]C(=[C])[NH]:1.0, [C]C(=[C])[NH]>>[C]C([C])=[N]:1.0, [C]C([C])=NC(=[C])[CH]:1.0, [C]C([C])=[N]:1.0, [C]C([N])=NC(=O)[NH]:1.0, [C]C([N])=[N]:1.0, [C]C1=[C]NCC2N1CN([C])C2>>[C]NCC1NC([C])=[C]NC1:1.0, [C]C=[CH]:1.0, [C]C=[CH]>>[C]C(=[CH])C:1.0, [C]N([CH2])C=1NC(=O)N[C]C1[NH]>>[C]N([CH2])C1=NC(=O)N[C]C1=[N]:1.0, [C]N([CH2])[CH2]:1.0, [C]N([CH2])[CH2]>>[C]N[CH2]:1.0, [C]N([CH])[CH2]:1.0, [C]N([CH])[CH2]>>[C]N[CH]:1.0, [C]N1CN(C(=C[CH])C=[CH])CC1[CH2]>>[CH]C=C(C=[CH])NCC([NH])[CH2]:1.0, [C]N1CN2C(=C([NH])NCC2C1)C([N])=O>>[N]C(=O)C=1NC(CNC1[NH])C[NH]:1.0, [C]N1C[N]CC1C[NH]:1.0, [C]N1C[N]CC1C[NH]>>[C]NC(C[NH])C[NH]:1.0, [C]N1[CH]CN([C])C1:1.0, [C]N1[CH]CN([C])C1>>[C]C(=[CH])C:1.0, [C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]:1.0, [C]N=[C]:2.0, [C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]NC(=C([C])[NH])N([C])[CH2]>>[C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]NC(=C([N])[NH])C(=O)[NH]:1.0, [C]NC(=C([N])[NH])C(=O)[NH]>>[C]N=C(C([N])=[N])C(=O)[NH]:1.0, [C]NC(C[NH])C[NH]:1.0, [C]N[CH2]:1.0, [C]N[CH]:1.0, [C]N[C]:2.0, [C]N[C]>>[C]N=[C]:2.0, [NH]:4.0, [NH]C([CH2])[CH2]:1.0, [N]:4.0, [N]C([CH2])[CH2]:1.0, [N]C([CH2])[CH2]>>[NH]C([CH2])[CH2]:1.0, [N]C=C(C(=O)[NH])C:1.0, [N]C=CC(=O)[NH]:1.0, [N]C=CC(=O)[NH]>>[N]C=C(C(=O)[NH])C:1.0, [N]C[N]:1.0, [N]C[N]>>[C]C:1.0, [OH]:2.0, [O]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[CH2]:2.0, [CH3]:1.0, [C]:1.0, [N]:2.0]


ID=Reaction Center at Level: 1 (5)
[[C]C:1.0, [C]C(=[CH])C:1.0, [C]N([CH2])[CH2]:1.0, [C]N([CH])[CH2]:1.0, [N]C[N]:2.0]


ID=Reaction Center at Level: 2 (5)
[[CH]C(=[CH])N1C[N][CH]C1:1.0, [C]C(=[CH])C:1.0, [C]C(=[C])N1C[N]CC1[CH2]:1.0, [C]N1[CH]CN([C])C1:2.0, [N]C=C(C(=O)[NH])C:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (5)
[[C]:4.0, [NH]:2.0, [N]:2.0, [OH]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (8)
[O=O:2.0, OO:2.0, [C]=C([N])[NH]:1.0, [C]C(=[C])[NH]:1.0, [C]C([C])=[N]:1.0, [C]C([N])=[N]:1.0, [C]N=[C]:2.0, [C]N[C]:2.0]


ID=Reaction Center at Level: 2 (10)
[O=O:2.0, OO:2.0, [C]=C([N])NC(=O)[NH]:1.0, [C]C(=[C])NC(=[C])[CH]:1.0, [C]C([C])=NC(=[C])[CH]:1.0, [C]C([N])=NC(=O)[NH]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]:1.0, [C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]NC(=C([N])[NH])C(=O)[NH]:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (2)
[[NH]C([CH2])[CH2]:1.0, [N]C([CH2])[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[[C]N1C[N]CC1C[NH]:1.0, [C]NC(C[NH])C[NH]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]N[C]>>[C]N=[C]
2: [N]C([CH2])[CH2]>>[NH]C([CH2])[CH2]
3: O=O>>OO
4: [N]C[N]>>[C]C
5: [C]=C([N])[NH]>>[C]C([N])=[N]
6: [C]N([CH])[CH2]>>[C]N[CH]
7: [C]C=[CH]>>[C]C(=[CH])C
8: [C]C(=[C])[NH]>>[C]C([C])=[N]
9: [C]N[C]>>[C]N=[C]
10: [C]N([CH2])[CH2]>>[C]N[CH2]

MMP Level 2
1: [C]C(=[C])NC(=[C])[CH]>>[C]C([C])=NC(=[C])[CH]
2: [C]N1C[N]CC1C[NH]>>[C]NC(C[NH])C[NH]
3: O=O>>OO
4: [C]N1[CH]CN([C])C1>>[C]C(=[CH])C
5: [C]NC(=C([C])[NH])N([C])[CH2]>>[C]N=C(C([C])=[N])N([C])[CH2]
6: [C]C(=[C])N1C[N]CC1[CH2]>>[C]C(=[C])NC([CH2])[CH2]
7: [N]C=CC(=O)[NH]>>[N]C=C(C(=O)[NH])C
8: [C]NC(=C([N])[NH])C(=O)[NH]>>[C]N=C(C([N])=[N])C(=O)[NH]
9: [C]=C([N])NC(=O)[NH]>>[C]C([N])=NC(=O)[NH]
10: [CH]C(=[CH])N1C[N][CH]C1>>[CH]C(=[CH])NC[CH]

MMP Level 3
1: [C]=CC=1NC(=C([NH])[N]C1[CH])C(=O)[NH]>>[C]=CC=1N=C(C(=[N])[N]C1[CH])C(=O)[NH]
2: [C]C1=[C]NCC2N1CN([C])C2>>[C]NCC1NC([C])=[C]NC1
3: O=O>>OO
4: [C]C(=[C])N1CN(C([CH])=[CH])CC1[CH2]>>[N]C=C(C(=O)[NH])C
5: O=c1[nH]c(=O)c2N[C]=C([CH])N(c2[nH]1)C[CH]>>O=C1N=C2C(=N[C]=C([CH])N2C[CH])C(=O)N1
6: [C]N1CN2C(=C([NH])NCC2C1)C([N])=O>>[N]C(=O)C=1NC(CNC1[NH])C[NH]
7: O=C1C=CN([CH])[C]N1>>O=C1N[C]N([CH])C=C1C
8: O=C1N[C]NC2=C1NC([CH])=[C]N2[CH2]>>O=C1N[C]N=C2C1=NC([CH])=[C]N2[CH2]
9: [C]N([CH2])C=1NC(=O)N[C]C1[NH]>>[C]N([CH2])C1=NC(=O)N[C]C1=[N]
10: [C]N1CN(C(=C[CH])C=[CH])CC1[CH2]>>[CH]C=C(C=[CH])NCC([NH])[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00005	[O=c1nc(N)[nH]c2NCC3N(c12)CN(c4ccc(cc4)C(=O)NC(C(=O)O)CCC(=O)O)C3>>O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)O)C(O)C2]
2: R:M00001, P:M00006	[O=c1nc(N)[nH]c2NCC3N(c12)CN(c4ccc(cc4)C(=O)NC(C(=O)O)CCC(=O)O)C3>>O=c1nc(N)[nH]c2NCC(Nc12)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)O, O=c1nc(N)[nH]c2NCC3N(c12)CN(c4ccc(cc4)C(=O)NC(C(=O)O)CCC(=O)O)C3>>O=c1nc(N)[nH]c2NCC(Nc12)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)O, O=c1nc(N)[nH]c2NCC3N(c12)CN(c4ccc(cc4)C(=O)NC(C(=O)O)CCC(=O)O)C3>>O=c1nc(N)[nH]c2NCC(Nc12)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)O]
3: R:M00002, P:M00005	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)O)C(O)C2>>O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)O)C(O)C2]
4: R:M00003, P:M00007	[O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C>>O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1, O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C>>O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1, O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C>>O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1, O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C>>O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1]
5: R:M00004, P:M00008	[O=O>>OO]


//
SELECTED AAM MAPPING
[O:107]=[O:108].[O:84]=[C:83]([OH:85])[CH2:82][CH2:81][CH:77]([NH:76][C:74](=[O:75])[C:71]:1:[CH:70]:[CH:69]:[C:68](:[CH:73]:[CH:72]1)[N:67]2[CH2:66][N:64]3[C:65]=4[C:55](=[O:54])[N:56]=[C:57]([NH2:58])[NH:59][C:60]4[NH:61][CH2:62][CH:63]3[CH2:86]2)[C:78](=[O:79])[OH:80].[O:87]=[C:88]1[CH:89]=[CH:90][N:91]([C:92](=[O:93])[NH:94]1)[CH:95]2[O:96][CH:97]([CH2:98][O:99][P:100](=[O:101])([OH:102])[OH:103])[CH:104]([OH:105])[CH2:106]2.[O:1]=[C:2]1[NH:3][C:4](=[O:5])[C:6]=2[NH:7][C:8]:3:[CH:9]:[C:10](:[C:11](:[CH:12]:[C:13]3[N:14]([C:15]2[NH:16]1)[CH2:17][CH:18]([OH:19])[CH:20]([OH:21])[CH:22]([OH:23])[CH2:24][O:25][P:26](=[O:27])([OH:28])[O:29][P:30](=[O:31])([OH:32])[O:33][CH2:34][CH:35]4[O:36][CH:37]([N:38]:5:[CH:39]:[N:40]:[C:41]:6:[C:42](:[N:43]:[CH:44]:[N:45]:[C:46]65)[NH2:47])[CH:48]([OH:49])[CH:50]4[OH:51])[CH3:52])[CH3:53]>>[O:84]=[C:83]([OH:85])[CH2:82][CH2:81][CH:77]([NH:76][C:74](=[O:75])[C:71]:1:[CH:72]:[CH:73]:[C:68](:[CH:69]:[CH:70]1)[NH:67][CH2:86][CH:63]2[NH:64][C:65]=3[C:55](=[O:54])[N:56]=[C:57]([NH2:58])[NH:59][C:60]3[NH:61][CH2:62]2)[C:78](=[O:79])[OH:80].[O:1]=[C:2]1[N:16]=[C:15]2[C:6](=[N:7][C:8]:3:[CH:9]:[C:10](:[C:11](:[CH:12]:[C:13]3[N:14]2[CH2:17][CH:18]([OH:19])[CH:20]([OH:21])[CH:22]([OH:23])[CH2:24][O:25][P:26](=[O:27])([OH:28])[O:29][P:30](=[O:31])([OH:32])[O:33][CH2:34][CH:35]4[O:36][CH:37]([N:38]:5:[CH:39]:[N:40]:[C:41]:6:[C:42](:[N:43]:[CH:44]:[N:45]:[C:46]65)[NH2:47])[CH:48]([OH:49])[CH:50]4[OH:51])[CH3:52])[CH3:53])[C:4](=[O:5])[NH:3]1.[O:87]=[C:88]1[NH:94][C:92](=[O:93])[N:91]([CH:90]=[C:89]1[CH3:66])[CH:95]2[O:96][CH:97]([CH2:98][O:99][P:100](=[O:101])([OH:102])[OH:103])[CH:104]([OH:105])[CH2:106]2.[OH:107][OH:108]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=62, 2=63, 3=86, 4=67, 5=66, 6=64, 7=65, 8=60, 9=61, 10=59, 11=57, 12=56, 13=55, 14=54, 15=58, 16=68, 17=69, 18=70, 19=71, 20=72, 21=73, 22=74, 23=75, 24=76, 25=77, 26=81, 27=82, 28=83, 29=84, 30=85, 31=78, 32=79, 33=80, 34=106, 35=104, 36=97, 37=96, 38=95, 39=91, 40=90, 41=89, 42=88, 43=87, 44=94, 45=92, 46=93, 47=98, 48=99, 49=100, 50=101, 51=102, 52=103, 53=105, 54=53, 55=10, 56=9, 57=8, 58=13, 59=12, 60=11, 61=52, 62=14, 63=15, 64=6, 65=7, 66=4, 67=5, 68=3, 69=2, 70=1, 71=16, 72=17, 73=18, 74=20, 75=22, 76=24, 77=25, 78=26, 79=27, 80=28, 81=29, 82=30, 83=31, 84=32, 85=33, 86=34, 87=35, 88=50, 89=48, 90=37, 91=36, 92=38, 93=39, 94=40, 95=41, 96=46, 97=45, 98=44, 99=43, 100=42, 101=47, 102=49, 103=51, 104=23, 105=21, 106=19, 107=107, 108=108}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=94, 2=93, 3=92, 4=91, 5=89, 6=90, 7=88, 8=87, 9=86, 10=95, 11=106, 12=104, 13=97, 14=96, 15=98, 16=99, 17=100, 18=101, 19=102, 20=103, 21=105, 22=62, 23=63, 24=64, 25=65, 26=60, 27=61, 28=59, 29=57, 30=56, 31=55, 32=54, 33=58, 34=66, 35=67, 36=68, 37=69, 38=70, 39=71, 40=72, 41=73, 42=74, 43=75, 44=76, 45=77, 46=81, 47=82, 48=83, 49=84, 50=85, 51=78, 52=79, 53=80, 54=50, 55=9, 56=8, 57=7, 58=12, 59=11, 60=10, 61=49, 62=13, 63=4, 64=3, 65=2, 66=1, 67=53, 68=51, 69=52, 70=5, 71=6, 72=14, 73=15, 74=17, 75=19, 76=21, 77=22, 78=23, 79=24, 80=25, 81=26, 82=27, 83=28, 84=29, 85=30, 86=31, 87=32, 88=47, 89=45, 90=34, 91=33, 92=35, 93=36, 94=37, 95=38, 96=43, 97=42, 98=41, 99=40, 100=39, 101=44, 102=46, 103=48, 104=20, 105=18, 106=16, 107=107, 108=108}

