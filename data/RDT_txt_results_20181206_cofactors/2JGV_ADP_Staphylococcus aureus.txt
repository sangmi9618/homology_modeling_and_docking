
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O[CH2]:1.0, O[CH2]:1.0, O[CH2]>>[P]O[CH2]:1.0, [C]CO:1.0, [C]CO>>[C]COP(=O)(O)O:1.0, [C]COP(=O)(O)O:1.0, [OH]:2.0, [O]:2.0, [O]C([CH])(O)CO>>[O]C([CH])(O)COP(=O)(O)O:1.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[C]COP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[O]P(=O)(O)O:2.0, [P]O[CH2]:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[P]:1.0, [C]COP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[P]>>[P]O
2: O[CH2]>>[P]O[CH2]
3: [O]P(=O)(O)O>>[O]P(=O)(O)O

MMP Level 2
1: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
2: [C]CO>>[C]COP(=O)(O)O
3: O=P(O)(O)O[P]>>O=P(O)(O)O[CH2]

MMP Level 3
1: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
2: [O]C([CH])(O)CO>>[O]C([CH])(O)COP(=O)(O)O
3: [O]P(=O)(O)OP(=O)(O)O>>[C]COP(=O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2>>O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)OP(=O)(O)O)C(O)C2]
2: R:M00001, P:M00004	[O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2>>O=P(O)(O)OCC1OC(O)(COP(=O)(O)O)C(O)C1O]
3: R:M00002, P:M00004	[O=P(O)(O)OCC1OC(O)(CO)C(O)C1O>>O=P(O)(O)OCC1OC(O)(COP(=O)(O)O)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[NH:3][C:4](=[O:5])[N:6]([CH:7]=[C:8]1[CH3:9])[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][P:23](=[O:24])([OH:25])[OH:26])[CH:27]([OH:28])[CH2:29]2.[O:30]=[P:31]([OH:32])([OH:33])[O:34][CH2:35][CH:36]1[O:37][C:38]([OH:39])([CH2:40][OH:41])[CH:42]([OH:43])[CH:44]1[OH:45]>>[O:1]=[C:2]1[NH:3][C:4](=[O:5])[N:6]([CH:7]=[C:8]1[CH3:9])[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[OH:22])[CH:27]([OH:28])[CH2:29]2.[O:30]=[P:31]([OH:32])([OH:33])[O:34][CH2:35][CH:36]1[O:37][C:38]([OH:39])([CH2:40][O:41][P:23](=[O:24])([OH:25])[OH:26])[CH:42]([OH:43])[CH:44]1[OH:45]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=4, 6=5, 7=3, 8=2, 9=1, 10=10, 11=29, 12=27, 13=12, 14=11, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=25, 28=26, 29=28, 30=35, 31=36, 32=44, 33=42, 34=38, 35=37, 36=40, 37=41, 38=39, 39=43, 40=45, 41=34, 42=31, 43=30, 44=32, 45=33}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=4, 6=5, 7=3, 8=2, 9=1, 10=10, 11=25, 12=23, 13=12, 14=11, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=24, 26=31, 27=32, 28=44, 29=42, 30=34, 31=33, 32=36, 33=37, 34=38, 35=39, 36=40, 37=41, 38=35, 39=43, 40=45, 41=30, 42=27, 43=26, 44=28, 45=29}

