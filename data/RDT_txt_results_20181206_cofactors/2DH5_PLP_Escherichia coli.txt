
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:2.0, C=O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)C(N)CO:1.0, O=C(O)C(N)CO>>[C]C(=[C])CN:1.0, O=[CH]:1.0, O=[CH]>>O:1.0, [CH2]:1.0, [CH2]N:1.0, [CH]:2.0, [CH]N:1.0, [CH]N>>[CH2]N:1.0, [C]C(=[C])C=O:1.0, [C]C(=[C])C=O>>O:1.0, [C]C(=[C])C=O>>[C]C(=[C])CN:1.0, [C]C(=[C])CN:1.0, [C]C(O)=C(C=O)C(=[CH])[CH2]>>[C]C(O)=C(C(=[CH])[CH2])CN:1.0, [C]C([CH2])N:2.0, [C]C([CH2])N>>[C]CN:1.0, [C]C=O:2.0, [C]C=O>>O:1.0, [C]C=O>>[C]CN:1.0, [C]CN:2.0, [NH2]:2.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[O:1.0, [CH2]:1.0, [CH]:2.0, [NH2]:2.0, [O]:1.0]


ID=Reaction Center at Level: 1 (7)
[O:1.0, O=[CH]:1.0, [CH2]N:1.0, [CH]N:1.0, [C]C([CH2])N:1.0, [C]C=O:1.0, [C]CN:1.0]


ID=Reaction Center at Level: 2 (7)
[O:1.0, O=C(O)C(N)CO:1.0, [C]C(=[C])C=O:1.0, [C]C(=[C])CN:1.0, [C]C([CH2])N:1.0, [C]C=O:1.0, [C]CN:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH]N>>[CH2]N
2: O=[CH]>>O
3: [C]C=O>>[C]CN

MMP Level 2
1: [C]C([CH2])N>>[C]CN
2: [C]C=O>>O
3: [C]C(=[C])C=O>>[C]C(=[C])CN

MMP Level 3
1: O=C(O)C(N)CO>>[C]C(=[C])CN
2: [C]C(=[C])C=O>>O
3: [C]C(O)=C(C=O)C(=[CH])[CH2]>>[C]C(O)=C(C(=[CH])[CH2])CN


//
REACTION MMP (RPAIR)
1: R:M00002, P:M00004	[O=C(O)C(N)CO>>O=P(O)(O)OCc1cnc(c(O)c1CN)C]
2: R:M00003, P:M00004	[O=Cc1c(O)c(ncc1COP(=O)(O)O)C>>O=P(O)(O)OCc1cnc(c(O)c1CN)C]
3: R:M00003, P:M00005	[O=Cc1c(O)c(ncc1COP(=O)(O)O)C>>O]


//
SELECTED AAM MAPPING
[O:1]=[CH:2][C:3]:1:[C:4]([OH:5]):[C:6](:[N:7]:[CH:8]:[C:9]1[CH2:10][O:11][P:12](=[O:13])([OH:14])[OH:15])[CH3:16].[O:18]=[C:19]([OH:20])[CH:21]([NH2:17])[CH2:22][OH:23].[OH:24][CH2:25][CH2:26][SH:27]>>[O:13]=[P:12]([OH:15])([OH:14])[O:11][CH2:10][C:9]:1:[CH:8]:[N:7]:[C:6](:[C:4]([OH:5]):[C:3]1[CH2:2][NH2:17])[CH3:16].[OH2:1]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=25, 2=26, 3=27, 4=24, 5=22, 6=20, 7=18, 8=17, 9=19, 10=21, 11=23, 12=16, 13=6, 14=7, 15=8, 16=9, 17=3, 18=4, 19=5, 20=2, 21=1, 22=10, 23=11, 24=12, 25=13, 26=14, 27=15}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=16, 2=10, 3=9, 4=8, 5=7, 6=13, 7=11, 8=12, 9=14, 10=15, 11=6, 12=5, 13=2, 14=1, 15=3, 16=4, 17=17}

