
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)C[NH]:1.0, O=C(O)C[NH]>>O:1.0, O=C(O)[CH2]:2.0, O=C(O)[CH2]>>O:1.0, [C]:1.0, [C]O:1.0, [C]O>>O:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[O:1.0, [C]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (3)
[O:1.0, O=C(O)[CH2]:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (3)
[O:1.0, O=C(O)C[NH]:1.0, O=C(O)[CH2]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]O>>O

MMP Level 2
1: O=C(O)[CH2]>>O

MMP Level 3
1: O=C(O)C[NH]>>O


//
REACTION MMP (RPAIR)
1: R:M00002, P:M00003	[O=C(O)CNC(=O)C(NC(=O)CCC(N)C(=O)O)CS>>O]


//
SELECTED AAM MAPPING
[O:23]=[C:24]([OH:1])[CH2:25][NH:26][C:27](=[O:28])[CH:29]([NH:30][C:31](=[O:32])[CH2:33][CH2:34][CH:35]([NH2:36])[C:37](=[O:38])[OH:39])[CH2:40][SH:41].[O:2]=[C:3]([OH:4])[CH2:5][CH2:6][CH2:7][CH2:8][CH2:9][CH2:10][CH2:11][CH:12]1[CH:13]=[CH:14][C:15](=[O:16])[CH:17]1[CH2:18][CH:19]=[CH:20][CH2:21][CH3:22]>>[OH2:1]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=21, 2=20, 3=19, 4=18, 5=17, 6=16, 7=11, 8=12, 9=13, 10=14, 11=15, 12=10, 13=9, 14=8, 15=7, 16=6, 17=5, 18=4, 19=2, 20=1, 21=3, 22=34, 23=33, 24=31, 25=32, 26=30, 27=29, 28=40, 29=41, 30=27, 31=28, 32=26, 33=25, 34=23, 35=22, 36=24, 37=35, 38=37, 39=38, 40=39, 41=36}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=1}

