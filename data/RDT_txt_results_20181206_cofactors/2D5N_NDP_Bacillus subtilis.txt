
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%O:1.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1:1.0, [CH2]:2.0, [CH]:4.0, [CH]C(O)C(O)C[NH]:1.0, [CH]C(O)[CH2]:2.0, [CH]C([CH])O:1.0, [CH]C([CH])O>>[CH]C(O)[CH2]:1.0, [CH]C[NH]:1.0, [CH]N([CH])[CH]:1.0, [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]:1.0, [CH]O:1.0, [CH]O[CH]:1.0, [CH]O[CH]>>[CH]O:1.0, [CH][N+]([CH])=[CH]:1.0, [C]=C([NH])NC1OC([CH2])C(O)C1O>>[C]=C([NH])NCC(O)C([CH])O:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]:1.0, [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O:1.0, [C]C=[CH]:1.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C=[CH]:1.0, [C]NC1OC(C[O])C(O)C1O>>[O]CC(O)C([CH])O:1.0, [C]NC1OC([CH2])C(O)C1O>>[C]NCC(O)C(O)C(O)[CH2]:1.0, [C]NC1O[CH][CH]C1O:1.0, [C]NC1O[CH][CH]C1O>>[C]NCC([CH])O:1.0, [C]NCC([CH])O:1.0, [N+]:1.0, [NH]C1OC([CH2])[CH][CH]1:1.0, [NH]C1OC([CH2])[CH][CH]1>>[CH]C(O)[CH2]:1.0, [NH]C1O[CH]C(O)C1O:1.0, [NH]C1O[CH]C(O)C1O>>[CH]C(O)C(O)C[NH]:1.0, [N]:1.0, [OH]:1.0, [O]:1.0, [O]C([CH])[NH]:1.0, [O]C([CH])[NH]>>[CH]C[NH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (2)
[[CH]O[CH]:1.0, [O]C([CH])[NH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[C]NC1O[CH][CH]C1O:1.0, [NH]C1OC([CH2])[CH][CH]1:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH2]:1.0, [CH]:3.0]


ID=Reaction Center at Level: 1 (4)
[[CH]C(O)[CH2]:1.0, [CH]C([CH])O:1.0, [CH]C[NH]:1.0, [O]C([CH])[NH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[CH]C(O)C(O)C[NH]:1.0, [C]NC1O[CH][CH]C1O:1.0, [C]NCC([CH])O:1.0, [NH]C1O[CH]C(O)C1O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]
2: [CH]O[CH]>>[CH]O
3: [O]C([CH])[NH]>>[CH]C[NH]
4: [C]C[CH]>>[C]C=[CH]
5: [CH]C([CH])O>>[CH]C(O)[CH2]

MMP Level 2
1: [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]
2: [NH]C1OC([CH2])[CH][CH]1>>[CH]C(O)[CH2]
3: [C]NC1O[CH][CH]C1O>>[C]NCC([CH])O
4: [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]
5: [NH]C1O[CH]C(O)C1O>>[CH]C(O)C(O)C[NH]

MMP Level 3
1: [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O
2: [C]NC1OC(C[O])C(O)C1O>>[O]CC(O)C([CH])O
3: [C]=C([NH])NC1OC([CH2])C(O)C1O>>[C]=C([NH])NCC(O)C([CH])O
4: O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1
5: [C]NC1OC([CH2])C(O)C1O>>[C]NCC(O)C(O)C(O)[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00005	[O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O]
2: R:M00003, P:M00004	[O=c1[nH]c(=O)c(N)c([nH]1)NC2OC(COP(=O)(O)O)C(O)C2O>>O=c1[nH]c(=O)c(N)c([nH]1)NCC(O)C(O)C(O)COP(=O)(O)O, O=c1[nH]c(=O)c(N)c([nH]1)NC2OC(COP(=O)(O)O)C(O)C2O>>O=c1[nH]c(=O)c(N)c([nH]1)NCC(O)C(O)C(O)COP(=O)(O)O, O=c1[nH]c(=O)c(N)c([nH]1)NC2OC(COP(=O)(O)O)C(O)C2O>>O=c1[nH]c(=O)c(N)c([nH]1)NCC(O)C(O)C(O)COP(=O)(O)O]


//
SELECTED AAM MAPPING
[H+:72].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:5][N:6]([CH:7]=[CH:8][CH2:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48].[O:49]=[C:50]1[NH:51][C:52](=[O:53])[C:54]([NH2:55])=[C:56]([NH:57]1)[NH:58][CH:59]2[O:60][CH:61]([CH2:62][O:63][P:64](=[O:65])([OH:66])[OH:67])[CH:68]([OH:69])[CH:70]2[OH:71]>>[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:9]:[CH:8]:[CH:7]:[N+:6](:[CH:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48].[O:49]=[C:50]1[NH:51][C:52](=[O:53])[C:54]([NH2:55])=[C:56]([NH:57]1)[NH:58][CH2:59][CH:70]([OH:71])[CH:68]([OH:69])[CH:61]([OH:60])[CH2:62][O:63][P:64](=[O:65])([OH:66])[OH:67]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=5, 6=4, 7=2, 8=1, 9=3, 10=10, 11=47, 12=45, 13=12, 14=11, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=43, 28=37, 29=26, 30=25, 31=27, 32=28, 33=29, 34=30, 35=35, 36=34, 37=33, 38=32, 39=31, 40=36, 41=38, 42=39, 43=40, 44=41, 45=42, 46=44, 47=46, 48=48, 49=72, 50=62, 51=61, 52=68, 53=70, 54=59, 55=60, 56=58, 57=56, 58=54, 59=52, 60=53, 61=51, 62=50, 63=49, 64=57, 65=55, 66=71, 67=69, 68=63, 69=64, 70=65, 71=66, 72=67}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=59, 2=60, 3=62, 4=64, 5=66, 6=67, 7=68, 8=69, 9=70, 10=71, 11=65, 12=63, 13=61, 14=58, 15=56, 16=54, 17=52, 18=53, 19=51, 20=50, 21=49, 22=57, 23=55, 24=6, 25=5, 26=4, 27=9, 28=8, 29=7, 30=10, 31=47, 32=45, 33=12, 34=11, 35=13, 36=14, 37=15, 38=16, 39=17, 40=18, 41=19, 42=20, 43=21, 44=22, 45=23, 46=24, 47=43, 48=37, 49=26, 50=25, 51=27, 52=28, 53=29, 54=30, 55=35, 56=34, 57=33, 58=32, 59=31, 60=36, 61=38, 62=39, 63=40, 64=41, 65=42, 66=44, 67=46, 68=48, 69=2, 70=1, 71=3}

