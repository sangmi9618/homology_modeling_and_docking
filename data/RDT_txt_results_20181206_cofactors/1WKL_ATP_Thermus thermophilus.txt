
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:2.0, O=P(O)(O)O[P]>>O=P(O)(O)OP(=O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O[P]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:4.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:2.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O>>[P]O[P]:1.0, [P]O[P]:2.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]P(=O)(O)O:2.0, [P]O[P]:2.0]


ID=Reaction Center at Level: 2 (2)
[O=P(O)(O)O[P]:2.0, [O]P(=O)(O)OP(=O)(O)O:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]P(=O)(O)O>>[O]P(=O)(O)O
2: [P]O>>[P]O[P]
3: [P]O[P]>>[P]O

MMP Level 2
1: O=P(O)(O)O[P]>>O=P(O)(O)O[P]
2: [O]P(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O
3: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O

MMP Level 3
1: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O
2: O=P(O)(O)O[P]>>O=P(O)(O)OP(=O)(O)O[P]
3: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2O]
3: R:M00002, P:M00004	[O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)O)C(O)C2O>>O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2O]


//
SELECTED AAM MAPPING
[O:32]=[C:33]1[N:34]=[C:35]([NH2:36])[CH:37]=[CH:38][N:39]1[CH:40]2[O:41][CH:42]([CH2:43][O:44][P:45](=[O:46])([OH:47])[O:48][P:49](=[O:50])([OH:51])[OH:52])[CH:53]([OH:54])[CH:55]2[OH:56].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:32]=[C:33]1[N:34]=[C:35]([NH2:36])[CH:37]=[CH:38][N:39]1[CH:40]2[O:41][CH:42]([CH2:43][O:44][P:45](=[O:46])([OH:47])[O:48][P:49](=[O:50])([OH:51])[O:52][P:2](=[O:1])([OH:3])[OH:4])[CH:53]([OH:54])[CH:55]2[OH:56].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=37, 33=38, 34=39, 35=33, 36=32, 37=34, 38=35, 39=36, 40=40, 41=55, 42=53, 43=42, 44=41, 45=43, 46=44, 47=45, 48=46, 49=47, 50=48, 51=49, 52=50, 53=51, 54=52, 55=54, 56=56}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=49, 2=50, 3=51, 4=46, 5=47, 6=48, 7=52, 8=45, 9=44, 10=43, 11=42, 12=53, 13=55, 14=40, 15=41, 16=39, 17=38, 18=35, 19=36, 20=37, 21=34, 22=31, 23=30, 24=32, 25=33, 26=56, 27=54, 28=6, 29=7, 30=8, 31=2, 32=1, 33=3, 34=4, 35=5, 36=9, 37=28, 38=26, 39=11, 40=10, 41=12, 42=13, 43=14, 44=15, 45=16, 46=17, 47=18, 48=19, 49=20, 50=21, 51=22, 52=23, 53=24, 54=25, 55=27, 56=29}

