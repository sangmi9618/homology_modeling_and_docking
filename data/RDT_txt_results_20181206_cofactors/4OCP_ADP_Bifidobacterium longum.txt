
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[CH]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O[CH]:1.0, [CH]:2.0, [CH]O:1.0, [CH]O>>[P]O[CH]:1.0, [CH]OC(O)C([CH])[NH]:1.0, [CH]OC(O)C([CH])[NH]>>O=P(O)(O)OC(O[CH])C([CH])[NH]:1.0, [CH]OC(O)C([CH])[NH]>>[P]OC(O[CH])C([CH])[NH]:1.0, [C]NC1C(O)OC([CH2])[CH]C1O>>[C]NC1C(O)[CH]C(OC1OP(=O)(O)O)[CH2]:1.0, [OH]:2.0, [O]:2.0, [O]C([CH])O:2.0, [O]C([CH])O>>[O]C([CH])OP(=O)(O)O:1.0, [O]C([CH])O>>[O]C([O])[CH]:1.0, [O]C([CH])OP(=O)(O)O:1.0, [O]C([O])[CH]:1.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]C([CH])OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]OC(O[CH])C([CH])[NH]:1.0, [P]O[CH]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[O]P(=O)(O)O:2.0, [P]O[CH]:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)O[CH]:1.0, O=P(O)(O)O[P]:1.0, [O]C([CH])OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]C([CH])O:1.0, [O]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[CH]OC(O)C([CH])[NH]:1.0, [P]OC(O[CH])C([CH])[NH]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [P]O[P]>>[P]O
2: [CH]O>>[P]O[CH]
3: [O]C([CH])O>>[O]C([O])[CH]
4: [O]P(=O)(O)O>>[O]P(=O)(O)O

MMP Level 2
1: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
2: [O]C([CH])O>>[O]C([CH])OP(=O)(O)O
3: [CH]OC(O)C([CH])[NH]>>[P]OC(O[CH])C([CH])[NH]
4: O=P(O)(O)O[P]>>O=P(O)(O)O[CH]

MMP Level 3
1: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
2: [CH]OC(O)C([CH])[NH]>>O=P(O)(O)OC(O[CH])C([CH])[NH]
3: [C]NC1C(O)OC([CH2])[CH]C1O>>[C]NC1C(O)[CH]C(OC1OP(=O)(O)O)[CH2]
4: [O]P(=O)(O)OP(=O)(O)O>>[O]C([CH])OP(=O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(NC1C(O)OC(CO)C(O)C1O)C>>O=C(NC1C(O)C(O)C(OC1OP(=O)(O)O)CO)C, O=C(NC1C(O)OC(CO)C(O)C1O)C>>O=C(NC1C(O)C(O)C(OC1OP(=O)(O)O)CO)C]
2: R:M00002, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(NC1C(O)C(O)C(OC1OP(=O)(O)O)CO)C]
3: R:M00002, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]


//
SELECTED AAM MAPPING
[O:32]=[C:33]([NH:34][CH:35]1[CH:36]([OH:37])[O:38][CH:39]([CH2:40][OH:41])[CH:42]([OH:43])[CH:44]1[OH:45])[CH3:46].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:32]=[C:33]([NH:34][CH:35]1[CH:44]([OH:45])[CH:42]([OH:43])[CH:39]([O:38][CH:36]1[O:37][P:2](=[O:1])([OH:3])[OH:4])[CH2:40][OH:41])[CH3:46].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=46, 2=33, 3=32, 4=34, 5=35, 6=44, 7=42, 8=39, 9=38, 10=36, 11=37, 12=40, 13=41, 14=43, 15=45, 16=24, 17=25, 18=26, 19=21, 20=22, 21=23, 22=27, 23=20, 24=19, 25=18, 26=17, 27=28, 28=30, 29=15, 30=16, 31=14, 32=13, 33=10, 34=11, 35=12, 36=9, 37=6, 38=7, 39=8, 40=5, 41=2, 42=1, 43=3, 44=4, 45=31, 46=29}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=46, 2=29, 3=28, 4=30, 5=31, 6=32, 7=34, 8=36, 9=37, 10=38, 11=39, 12=40, 13=41, 14=42, 15=43, 16=44, 17=45, 18=35, 19=33, 20=20, 21=21, 22=22, 23=17, 24=18, 25=19, 26=23, 27=16, 28=15, 29=14, 30=13, 31=24, 32=26, 33=11, 34=12, 35=10, 36=9, 37=6, 38=7, 39=8, 40=5, 41=2, 42=1, 43=3, 44=4, 45=27, 46=25}

