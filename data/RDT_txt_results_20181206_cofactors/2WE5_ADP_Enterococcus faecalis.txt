
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, O-P:2.0]

ORDER_CHANGED
[C-O*C=O:1.0]

//
FINGERPRINTS RC
[N:3.0, O=C(OP(=O)(O)O)N:1.0, O=C(OP(=O)(O)O)N>>O=C=O:3.0, O=C(OP(=O)(O)O)N>>[O]P(=O)(O)OP(=O)(O)O:1.0, O=C(O[P])N:1.0, O=C(O[P])N>>N:1.0, O=C(O[P])N>>O=C=O:1.0, O=C=O:3.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)OP(=O)(O)O[P]:1.0, [C]:2.0, [C]=O:1.0, [C]N:1.0, [C]N>>N:1.0, [C]OP(=O)(O)O:1.0, [C]OP(=O)(O)O>>O=P(O)(O)O[P]:1.0, [C]O[P]:1.0, [C]O[P]>>[C]=O:1.0, [NH2]:1.0, [OH]:1.0, [O]:3.0, [O]C(=O)N:2.0, [O]C(=O)N>>N:1.0, [O]C(=O)N>>O=C=O:1.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O>>[P]O[P]:1.0, [P]O[P]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[N:1.0, [C]:1.0, [NH2]:1.0, [O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (6)
[N:1.0, [C]N:1.0, [C]O[P]:1.0, [O]C(=O)N:1.0, [O]P(=O)(O)O:2.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (7)
[N:1.0, O=C(OP(=O)(O)O)N:1.0, O=C(O[P])N:1.0, O=P(O)(O)O[P]:1.0, [C]OP(=O)(O)O:1.0, [O]C(=O)N:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (2)
[[C]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (4)
[O=C=O:1.0, [C]=O:1.0, [C]O[P]:1.0, [O]C(=O)N:1.0]


ID=Reaction Center at Level: 2 (3)
[O=C(OP(=O)(O)O)N:1.0, O=C(O[P])N:1.0, O=C=O:2.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]N>>N
2: [O]C(=O)N>>O=C=O
3: [P]O>>[P]O[P]
4: [C]O[P]>>[C]=O
5: [O]P(=O)(O)O>>[O]P(=O)(O)O

MMP Level 2
1: [O]C(=O)N>>N
2: O=C(O[P])N>>O=C=O
3: [O]P(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O
4: O=C(OP(=O)(O)O)N>>O=C=O
5: [C]OP(=O)(O)O>>O=P(O)(O)O[P]

MMP Level 3
1: O=C(O[P])N>>N
2: O=C(OP(=O)(O)O)N>>O=C=O
3: O=P(O)(O)O[P]>>O=P(O)(O)OP(=O)(O)O[P]
4: O=C(OP(=O)(O)O)N>>O=C=O
5: O=C(OP(=O)(O)O)N>>[O]P(=O)(O)OP(=O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00002, P:M00003	[O=C(OP(=O)(O)O)N>>O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
3: R:M00002, P:M00004	[O=C(OP(=O)(O)O)N>>N]
4: R:M00002, P:M00005	[O=C(OP(=O)(O)O)N>>O=C=O, O=C(OP(=O)(O)O)N>>O=C=O]


//
SELECTED AAM MAPPING
[O:28]=[C:29]([O:30][P:31](=[O:32])([OH:33])[OH:34])[NH2:35].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][CH2:10][CH:11]1[O:12][CH:13]([N:14]:2:[CH:15]:[N:16]:[C:17]:3:[C:18](:[N:19]:[CH:20]:[N:21]:[C:22]32)[NH2:23])[CH:24]([OH:25])[CH:26]1[OH:27]>>[O:28]=[C:29]=[O:30].[O:32]=[P:31]([OH:33])([OH:34])[O:3][P:2](=[O:1])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][CH2:10][CH:11]1[O:12][CH:13]([N:14]:2:[CH:15]:[N:16]:[C:17]:3:[C:18](:[N:19]:[CH:20]:[N:21]:[C:22]32)[NH2:23])[CH:24]([OH:25])[CH:26]1[OH:27].[NH3:35]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=17, 5=18, 6=19, 7=23, 8=16, 9=15, 10=14, 11=13, 12=24, 13=26, 14=11, 15=12, 16=10, 17=9, 18=6, 19=7, 20=8, 21=5, 22=2, 23=1, 24=3, 25=4, 26=27, 27=25, 28=29, 29=28, 30=35, 31=30, 32=31, 33=32, 34=33, 35=34}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=35, 33=33, 34=32, 35=34}

