
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%O:1.0, C-C:1.0, C-S:1.0, C=O:1.0]

ORDER_CHANGED
[C%C*C%C:3.0, C%N*C%N:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(O)C[CH2]>>[O-]C(=O)C[CH2]:1.0, O=C(O)[CH2]:2.0, O=C(O)[CH2]>>[C]C(OC(=O)[CH2])C:1.0, O=C(O)[CH2]>>[O-]C(=O)[CH2]:1.0, S([CH2])[CH2]:1.0, [CH2]:1.0, [CH2][S+]([CH2])C:2.0, [CH2][S+]([CH2])C>>S([CH2])[CH2]:1.0, [CH2][S+]([CH2])C>>[C]C([N-])([CH2])C:1.0, [CH3]:2.0, [CH]:4.0, [CH]=C1[C][C]=C([N-]1)[CH2]:1.0, [CH]=C1[C][C]=C([N-]1)[CH2]>>[CH]C=1[C][C]=C(N1)[CH2]:1.0, [CH]C(=[CH])[NH]:1.0, [CH]C=1[C][C]=C(N1)[CH2]:1.0, [CH]CSC[CH2]:1.0, [CH]C[S+](C)C[CH2]:1.0, [CH]C[S+](C)C[CH2]>>[CH]CSC[CH2]:1.0, [CH]C[S+](C)C[CH2]>>[C]CC1([N-][C][C]=C1[CH2])C:1.0, [C]:9.0, [C]1=[C]C([CH2])=C([N-]1)CC2=N[C]=[C]C2([CH2])C>>[C]1[C]=C([CH2])C([N-]1)=CC2=N[C]=[C]C2([CH2])C:1.0, [C]1[C]C([CH2])C(=N1)C=C2[N-][C]=[C]C2([CH2])C>>[C]1[C]C([CH2])C(=CC2=N[C]=[C]C2([CH2])C)N1:1.0, [C]=C([N-])[CH2]:2.0, [C]=C([N-])[CH2]>>[C]C([N-])([CH2])C:1.0, [C]=C([N-])[CH2]>>[C]C([N-])=[CH]:1.0, [C]=C([N])C1(N=[C][CH]C1([CH2])C)C(=O)C>>[C]=C([N])C12N[C][CH]C2(C)CC(=O)OC1C:1.0, [C]=CC1=NC(C(=[C])[N])(C(=O)C)C([CH2])(C)C1[CH2]>>[C]C=C1NC(C(=[C])[N])(C([O])C)C([CH2])(C)C1[CH2]:1.0, [C]=CC1=N[C]=[C]C1([CH2])C:1.0, [C]=CC1=N[C][C]C1[CH2]:1.0, [C]=CC1=N[C][C]C1[CH2]>>[C]C=C1N[C][C]C1[CH2]:1.0, [C]=O:1.0, [C]C:1.0, [C]C(=O)C:2.0, [C]C(=O)C>>[C]C([O])C:1.0, [C]C(=[N])C=C([CH])[NH]:1.0, [C]C(=[N])CC(=[C])[N-]:1.0, [C]C(=[N])CC(=[C])[N-]>>[C]C([N-])=CC([C])=[N]:1.0, [C]C(=[N])Cc1[n-]c([CH2])c([CH2])c1C[CH2]>>[C]C(=[N])C=C1[N-]C([CH2])(C([CH2])=C1C[CH2])C:1.0, [C]C(=[N])[CH]:1.0, [C]C(OC(=O)[CH2])C:1.0, [C]C([C])([N])C(=O)C:1.0, [C]C([C])([N])C(=O)C>>[C]OC(C)C([C])([C])[NH]:1.0, [C]C([N-])([CH2])C:2.0, [C]C([N-])=CC(=[N])[CH]:1.0, [C]C([N-])=CC(=[N])[CH]>>[C]C(=[N])C=C([CH])[NH]:1.0, [C]C([N-])=CC([C])=[N]:1.0, [C]C([N-])=CC1=NC([C])([C])C([CH2])(C)C1C[CH2]>>[C]C(=[N])C=C1NC([C])([CH])C([CH2])(C)C1C[CH2]:1.0, [C]C([N-])=[CH]:2.0, [C]C([N-])=[CH]>>[C]C(=[N])[CH]:1.0, [C]C([O])C:1.0, [C]C1([CH])[C][CH]C(=[CH])N1:1.0, [C]C1([C])[C][CH]C([CH])=N1:1.0, [C]C1([C])[C][CH]C([CH])=N1>>[C]C1([CH])[C][CH]C(=[CH])N1:1.0, [C]C=C1N[C][C]C1[CH2]:1.0, [C]C=C1[N-]C(=C([CH2])C1([CH2])C)C[C]>>[C]CC=1N=C(C=[C])C([CH2])(C1[CH2])C:1.0, [C]C=C1[N-][C]=[C]C1([CH2])C:1.0, [C]C=C1[N-][C]=[C]C1([CH2])C>>[C]=CC1=N[C]=[C]C1([CH2])C:1.0, [C]C=C1[N-][C][C]=C1[CH2]:1.0, [C]C=[C]:3.0, [C]C=[C]>>[C]C=[C]:1.0, [C]CC(=O)O>>[C]C1([NH])[C]CC(=O)OC1C:1.0, [C]CC1(C([N-]C([CH2])=C1[CH2])=CC(=[N])[CH])C>>[C]CC1(C(=NC([CH2])=C1[CH2])C=C([CH])[NH])C:1.0, [C]CC1([N-][C][C]=C1[CH2])C:1.0, [C]CC=1[N-][C]=[C]C1[CH2]:2.0, [C]CC=1[N-][C]=[C]C1[CH2]>>[C]C=C1[N-][C][C]=C1[CH2]:1.0, [C]CC=1[N-][C]=[C]C1[CH2]>>[C]CC1([N-][C][C]=C1[CH2])C:1.0, [C]C[C]:1.0, [C]C[C]>>[C]C=[C]:1.0, [C]Cc1c([CH2])c([n-]c1CC(=[C])[N-])[CH2]>>[C]CC1=C([CH2])C(=[CH])[N-]C1(C)CC(=[C])[N]:1.0, [C]N=[C]:2.0, [C]N=[C]>>[C]N[C]:1.0, [C]N[C]:1.0, [C]O:2.0, [C]O>>[C]O[CH]:1.0, [C]O>>[C][O-]:1.0, [C]OC(C)C([C])([C])[NH]:1.0, [C]O[CH]:1.0, [C][N-][C]:1.0, [C][N-][C]>>[C]N=[C]:1.0, [C][O-]:1.0, [N-]:1.0, [NH]:1.0, [N]:2.0, [N]=C([CH])[CH]:1.0, [N]=C([CH])[CH]>>[CH]C(=[CH])[NH]:1.0, [O-]:1.0, [O-]C(=O)[CH2]:1.0, [OH]:2.0, [O]:2.0, [O]C([CH])C[S+](C)CC[CH]>>[O]C([CH])CSCC[CH]:1.0, [S+]:1.0, [S+]C:1.0, [S+]C>>[C]C:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[CH3]:2.0, [CH]:1.0, [C]:2.0, [O]:2.0, [S+]:1.0]


ID=Reaction Center at Level: 1 (8)
[[CH2][S+]([CH2])C:1.0, [C]=O:1.0, [C]C:1.0, [C]C(=O)C:1.0, [C]C([N-])([CH2])C:1.0, [C]C([O])C:1.0, [C]O[CH]:1.0, [S+]C:1.0]


ID=Reaction Center at Level: 2 (8)
[[CH2][S+]([CH2])C:1.0, [CH]C[S+](C)C[CH2]:1.0, [C]C(=O)C:1.0, [C]C(OC(=O)[CH2])C:1.0, [C]C([C])([N])C(=O)C:1.0, [C]C([N-])([CH2])C:1.0, [C]CC1([N-][C][C]=C1[CH2])C:1.0, [C]OC(C)C([C])([C])[NH]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (6)
[[CH2]:1.0, [CH]:3.0, [C]:6.0, [N-]:1.0, [NH]:1.0, [N]:2.0]


ID=Reaction Center at Level: 1 (10)
[[CH]C(=[CH])[NH]:1.0, [C]=C([N-])[CH2]:1.0, [C]C(=[N])[CH]:1.0, [C]C([N-])=[CH]:2.0, [C]C=[C]:3.0, [C]C[C]:1.0, [C]N=[C]:2.0, [C]N[C]:1.0, [C][N-][C]:1.0, [N]=C([CH])[CH]:1.0]


ID=Reaction Center at Level: 2 (14)
[[CH]=C1[C][C]=C([N-]1)[CH2]:1.0, [CH]C=1[C][C]=C(N1)[CH2]:1.0, [C]=CC1=N[C]=[C]C1([CH2])C:1.0, [C]=CC1=N[C][C]C1[CH2]:1.0, [C]C(=[N])C=C([CH])[NH]:1.0, [C]C(=[N])CC(=[C])[N-]:1.0, [C]C([N-])=CC(=[N])[CH]:1.0, [C]C([N-])=CC([C])=[N]:1.0, [C]C1([CH])[C][CH]C(=[CH])N1:1.0, [C]C1([C])[C][CH]C([CH])=N1:1.0, [C]C=C1N[C][C]C1[CH2]:1.0, [C]C=C1[N-][C]=[C]C1([CH2])C:1.0, [C]C=C1[N-][C][C]=C1[CH2]:1.0, [C]CC=1[N-][C]=[C]C1[CH2]:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[C]:2.0]


ID=Reaction Center at Level: 1 (2)
[[C]=C([N-])[CH2]:1.0, [C]C([N-])([CH2])C:1.0]


ID=Reaction Center at Level: 2 (2)
[[C]CC1([N-][C][C]=C1[CH2])C:1.0, [C]CC=1[N-][C]=[C]C1[CH2]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C[C]>>[C]C=[C]
2: [C]O>>[C]O[CH]
3: [C]C([N-])=[CH]>>[C]C(=[N])[CH]
4: [N]=C([CH])[CH]>>[CH]C(=[CH])[NH]
5: [CH2][S+]([CH2])C>>S([CH2])[CH2]
6: [C]=C([N-])[CH2]>>[C]C([N-])=[CH]
7: [C]O>>[C][O-]
8: [C]=C([N-])[CH2]>>[C]C([N-])([CH2])C
9: [C]C=[C]>>[C]C=[C]
10: [C][N-][C]>>[C]N=[C]
11: [C]C(=O)C>>[C]C([O])C
12: [C]N=[C]>>[C]N[C]
13: [S+]C>>[C]C

MMP Level 2
1: [C]C(=[N])CC(=[C])[N-]>>[C]C([N-])=CC([C])=[N]
2: O=C(O)[CH2]>>[C]C(OC(=O)[CH2])C
3: [C]C=C1[N-][C]=[C]C1([CH2])C>>[C]=CC1=N[C]=[C]C1([CH2])C
4: [C]=CC1=N[C][C]C1[CH2]>>[C]C=C1N[C][C]C1[CH2]
5: [CH]C[S+](C)C[CH2]>>[CH]CSC[CH2]
6: [C]CC=1[N-][C]=[C]C1[CH2]>>[C]C=C1[N-][C][C]=C1[CH2]
7: O=C(O)[CH2]>>[O-]C(=O)[CH2]
8: [C]CC=1[N-][C]=[C]C1[CH2]>>[C]CC1([N-][C][C]=C1[CH2])C
9: [C]C([N-])=CC(=[N])[CH]>>[C]C(=[N])C=C([CH])[NH]
10: [CH]=C1[C][C]=C([N-]1)[CH2]>>[CH]C=1[C][C]=C(N1)[CH2]
11: [C]C([C])([N])C(=O)C>>[C]OC(C)C([C])([C])[NH]
12: [C]C1([C])[C][CH]C([CH])=N1>>[C]C1([CH])[C][CH]C(=[CH])N1
13: [CH2][S+]([CH2])C>>[C]C([N-])([CH2])C

MMP Level 3
1: [C]1=[C]C([CH2])=C([N-]1)CC2=N[C]=[C]C2([CH2])C>>[C]1[C]=C([CH2])C([N-]1)=CC2=N[C]=[C]C2([CH2])C
2: [C]CC(=O)O>>[C]C1([NH])[C]CC(=O)OC1C
3: [C]CC1(C([N-]C([CH2])=C1[CH2])=CC(=[N])[CH])C>>[C]CC1(C(=NC([CH2])=C1[CH2])C=C([CH])[NH])C
4: [C]C([N-])=CC1=NC([C])([C])C([CH2])(C)C1C[CH2]>>[C]C(=[N])C=C1NC([C])([CH])C([CH2])(C)C1C[CH2]
5: [O]C([CH])C[S+](C)CC[CH]>>[O]C([CH])CSCC[CH]
6: [C]C(=[N])Cc1[n-]c([CH2])c([CH2])c1C[CH2]>>[C]C(=[N])C=C1[N-]C([CH2])(C([CH2])=C1C[CH2])C
7: O=C(O)C[CH2]>>[O-]C(=O)C[CH2]
8: [C]Cc1c([CH2])c([n-]c1CC(=[C])[N-])[CH2]>>[C]CC1=C([CH2])C(=[CH])[N-]C1(C)CC(=[C])[N]
9: [C]1[C]C([CH2])C(=N1)C=C2[N-][C]=[C]C2([CH2])C>>[C]1[C]C([CH2])C(=CC2=N[C]=[C]C2([CH2])C)N1
10: [C]C=C1[N-]C(=C([CH2])C1([CH2])C)C[C]>>[C]CC=1N=C(C=[C])C([CH2])(C1[CH2])C
11: [C]=C([N])C1(N=[C][CH]C1([CH2])C)C(=O)C>>[C]=C([N])C12N[C][CH]C2(C)CC(=O)OC1C
12: [C]=CC1=NC(C(=[C])[N])(C(=O)C)C([CH2])(C)C1[CH2]>>[C]C=C1NC(C(=[C])[N])(C([O])C)C([CH2])(C)C1[CH2]
13: [CH]C[S+](C)C[CH2]>>[C]CC1([N-][C][C]=C1[CH2])C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(O)C(N)CCSCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00005	[O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>[O-]C(=O)CCC=1C=2[N-]C(C1CC(=O)O)(C)CC=3N=C(C=C4NC5(C=6N=C(C2)C(C6CC(=O)O)(C)CCC(=O)O)C(OC(=O)CC5(C)C4CCC(=O)O)C)C(C3CCC(=O)O)(C)CC(=O)O]
3: R:M00002, P:M00005	[O=C(O)Cc1c2[n-]c(c1CCC(=O)O)CC3=NC(=C(CC(=O)O)C3(C)CCC(=O)O)C4(N=C(C=C5[N-]C(=C(CCC(=O)O)C5(C)CC(=O)O)C2)C(CCC(=O)O)C4(C)CC(=O)O)C(=O)C>>[O-]C(=O)CCC=1C=2[N-]C(C1CC(=O)O)(C)CC=3N=C(C=C4NC5(C=6N=C(C2)C(C6CC(=O)O)(C)CCC(=O)O)C(OC(=O)CC5(C)C4CCC(=O)O)C)C(C3CCC(=O)O)(C)CC(=O)O, O=C(O)Cc1c2[n-]c(c1CCC(=O)O)CC3=NC(=C(CC(=O)O)C3(C)CCC(=O)O)C4(N=C(C=C5[N-]C(=C(CCC(=O)O)C5(C)CC(=O)O)C2)C(CCC(=O)O)C4(C)CC(=O)O)C(=O)C>>[O-]C(=O)CCC=1C=2[N-]C(C1CC(=O)O)(C)CC=3N=C(C=C4NC5(C=6N=C(C2)C(C6CC(=O)O)(C)CCC(=O)O)C(OC(=O)CC5(C)C4CCC(=O)O)C)C(C3CCC(=O)O)(C)CC(=O)O, O=C(O)Cc1c2[n-]c(c1CCC(=O)O)CC3=NC(=C(CC(=O)O)C3(C)CCC(=O)O)C4(N=C(C=C5[N-]C(=C(CCC(=O)O)C5(C)CC(=O)O)C2)C(CCC(=O)O)C4(C)CC(=O)O)C(=O)C>>[O-]C(=O)CCC=1C=2[N-]C(C1CC(=O)O)(C)CC=3N=C(C=C4NC5(C=6N=C(C2)C(C6CC(=O)O)(C)CCC(=O)O)C(OC(=O)CC5(C)C4CCC(=O)O)C)C(C3CCC(=O)O)(C)CC(=O)O, O=C(O)Cc1c2[n-]c(c1CCC(=O)O)CC3=NC(=C(CC(=O)O)C3(C)CCC(=O)O)C4(N=C(C=C5[N-]C(=C(CCC(=O)O)C5(C)CC(=O)O)C2)C(CCC(=O)O)C4(C)CC(=O)O)C(=O)C>>[O-]C(=O)CCC=1C=2[N-]C(C1CC(=O)O)(C)CC=3N=C(C=C4NC5(C=6N=C(C2)C(C6CC(=O)O)(C)CCC(=O)O)C(OC(=O)CC5(C)C4CCC(=O)O)C)C(C3CCC(=O)O)(C)CC(=O)O, O=C(O)Cc1c2[n-]c(c1CCC(=O)O)CC3=NC(=C(CC(=O)O)C3(C)CCC(=O)O)C4(N=C(C=C5[N-]C(=C(CCC(=O)O)C5(C)CC(=O)O)C2)C(CCC(=O)O)C4(C)CC(=O)O)C(=O)C>>[O-]C(=O)CCC=1C=2[N-]C(C1CC(=O)O)(C)CC=3N=C(C=C4NC5(C=6N=C(C2)C(C6CC(=O)O)(C)CCC(=O)O)C(OC(=O)CC5(C)C4CCC(=O)O)C)C(C3CCC(=O)O)(C)CC(=O)O, O=C(O)Cc1c2[n-]c(c1CCC(=O)O)CC3=NC(=C(CC(=O)O)C3(C)CCC(=O)O)C4(N=C(C=C5[N-]C(=C(CCC(=O)O)C5(C)CC(=O)O)C2)C(CCC(=O)O)C4(C)CC(=O)O)C(=O)C>>[O-]C(=O)CCC=1C=2[N-]C(C1CC(=O)O)(C)CC=3N=C(C=C4NC5(C=6N=C(C2)C(C6CC(=O)O)(C)CCC(=O)O)C(OC(=O)CC5(C)C4CCC(=O)O)C)C(C3CCC(=O)O)(C)CC(=O)O, O=C(O)Cc1c2[n-]c(c1CCC(=O)O)CC3=NC(=C(CC(=O)O)C3(C)CCC(=O)O)C4(N=C(C=C5[N-]C(=C(CCC(=O)O)C5(C)CC(=O)O)C2)C(CCC(=O)O)C4(C)CC(=O)O)C(=O)C>>[O-]C(=O)CCC=1C=2[N-]C(C1CC(=O)O)(C)CC=3N=C(C=C4NC5(C=6N=C(C2)C(C6CC(=O)O)(C)CCC(=O)O)C(OC(=O)CC5(C)C4CCC(=O)O)C)C(C3CCC(=O)O)(C)CC(=O)O, O=C(O)Cc1c2[n-]c(c1CCC(=O)O)CC3=NC(=C(CC(=O)O)C3(C)CCC(=O)O)C4(N=C(C=C5[N-]C(=C(CCC(=O)O)C5(C)CC(=O)O)C2)C(CCC(=O)O)C4(C)CC(=O)O)C(=O)C>>[O-]C(=O)CCC=1C=2[N-]C(C1CC(=O)O)(C)CC=3N=C(C=C4NC5(C=6N=C(C2)C(C6CC(=O)O)(C)CCC(=O)O)C(OC(=O)CC5(C)C4CCC(=O)O)C)C(C3CCC(=O)O)(C)CC(=O)O, O=C(O)Cc1c2[n-]c(c1CCC(=O)O)CC3=NC(=C(CC(=O)O)C3(C)CCC(=O)O)C4(N=C(C=C5[N-]C(=C(CCC(=O)O)C5(C)CC(=O)O)C2)C(CCC(=O)O)C4(C)CC(=O)O)C(=O)C>>[O-]C(=O)CCC=1C=2[N-]C(C1CC(=O)O)(C)CC=3N=C(C=C4NC5(C=6N=C(C2)C(C6CC(=O)O)(C)CCC(=O)O)C(OC(=O)CC5(C)C4CCC(=O)O)C)C(C3CCC(=O)O)(C)CC(=O)O, O=C(O)Cc1c2[n-]c(c1CCC(=O)O)CC3=NC(=C(CC(=O)O)C3(C)CCC(=O)O)C4(N=C(C=C5[N-]C(=C(CCC(=O)O)C5(C)CC(=O)O)C2)C(CCC(=O)O)C4(C)CC(=O)O)C(=O)C>>[O-]C(=O)CCC=1C=2[N-]C(C1CC(=O)O)(C)CC=3N=C(C=C4NC5(C=6N=C(C2)C(C6CC(=O)O)(C)CCC(=O)O)C(OC(=O)CC5(C)C4CCC(=O)O)C)C(C3CCC(=O)O)(C)CC(=O)O, O=C(O)Cc1c2[n-]c(c1CCC(=O)O)CC3=NC(=C(CC(=O)O)C3(C)CCC(=O)O)C4(N=C(C=C5[N-]C(=C(CCC(=O)O)C5(C)CC(=O)O)C2)C(CCC(=O)O)C4(C)CC(=O)O)C(=O)C>>[O-]C(=O)CCC=1C=2[N-]C(C1CC(=O)O)(C)CC=3N=C(C=C4NC5(C=6N=C(C2)C(C6CC(=O)O)(C)CCC(=O)O)C(OC(=O)CC5(C)C4CCC(=O)O)C)C(C3CCC(=O)O)(C)CC(=O)O]


//
SELECTED AAM MAPPING
[Co+2:92].[O:1]=[C:2]([OH:3])[CH2:4][C:5]:1:[C:6]2:[N-:7]:[C:8](:[C:9]1[CH2:10][CH2:11][C:12](=[O:13])[OH:14])[CH2:15][C:16]3=[N:17][C:18](=[C:19]([CH2:20][C:21](=[O:22])[OH:23])[C:24]3([CH3:25])[CH2:26][CH2:27][C:28](=[O:29])[OH:30])[C:31]4([N:32]=[C:33]([CH:34]=[C:35]5[N-:36][C:37](=[C:38]([CH2:39][CH2:40][C:41](=[O:42])[OH:43])[C:44]5([CH3:45])[CH2:46][C:47](=[O:48])[OH:49])[CH2:50]2)[CH:51]([CH2:52][CH2:53][C:54](=[O:55])[OH:56])[C:57]4([CH3:58])[CH2:59][C:60](=[O:61])[OH:62])[C:63](=[O:93])[CH3:64].[O:65]=[C:66]([OH:67])[CH:68]([NH2:69])[CH2:70][CH2:71][S+:72]([CH3:73])[CH2:74][CH:75]1[O:76][CH:77]([N:78]:2:[CH:79]:[N:80]:[C:81]:3:[C:82](:[N:83]:[CH:84]:[N:85]:[C:86]32)[NH2:87])[CH:88]([OH:89])[CH:90]1[OH:91]>>[Co+2:92].[O:13]=[C:12]([O-:14])[CH2:11][CH2:10][C:9]=1[C:8]=2[N-:7][C:6]([C:5]1[CH2:4][C:2](=[O:1])[OH:3])([CH3:73])[CH2:50][C:37]=3[N:36]=[C:35]([CH:34]=[C:33]4[NH:32][C:31]5([C:18]=6[N:17]=[C:16]([CH:15]2)[C:24]([C:19]6[CH2:20][C:21](=[O:22])[OH:23])([CH3:25])[CH2:26][CH2:27][C:28](=[O:29])[OH:30])[CH:63]([O:62][C:60](=[O:61])[CH2:59][C:57]5([CH3:58])[CH:51]4[CH2:52][CH2:53][C:54](=[O:55])[OH:56])[CH3:64])[C:44]([C:38]3[CH2:39][CH2:40][C:41](=[O:42])[OH:43])([CH3:45])[CH2:46][C:47](=[O:48])[OH:49].[O:65]=[C:66]([OH:67])[CH:68]([NH2:69])[CH2:70][CH2:71][S:72][CH2:74][CH:75]1[O:76][CH:77]([N:78]:2:[CH:79]:[N:80]:[C:81]:3:[C:82](:[N:83]:[CH:84]:[N:85]:[C:86]32)[NH2:87])[CH:88]([OH:89])[CH:90]1[OH:91]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=74, 2=73, 3=72, 4=71, 5=69, 6=67, 7=66, 8=68, 9=70, 10=75, 11=76, 12=91, 13=89, 14=78, 15=77, 16=79, 17=80, 18=81, 19=82, 20=87, 21=86, 22=85, 23=84, 24=83, 25=88, 26=90, 27=92, 28=65, 29=63, 30=64, 31=31, 32=18, 33=19, 34=24, 35=16, 36=17, 37=15, 38=8, 39=9, 40=5, 41=6, 42=7, 43=50, 44=37, 45=38, 46=44, 47=35, 48=34, 49=33, 50=32, 51=51, 52=57, 53=58, 54=59, 55=60, 56=61, 57=62, 58=52, 59=53, 60=54, 61=55, 62=56, 63=36, 64=45, 65=46, 66=47, 67=48, 68=49, 69=39, 70=40, 71=41, 72=42, 73=43, 74=4, 75=2, 76=1, 77=3, 78=10, 79=11, 80=12, 81=13, 82=14, 83=25, 84=26, 85=27, 86=28, 87=29, 88=30, 89=20, 90=21, 91=22, 92=23, 93=93}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=84, 2=85, 3=86, 4=81, 5=82, 6=83, 7=87, 8=80, 9=79, 10=78, 11=77, 12=88, 13=90, 14=75, 15=76, 16=74, 17=73, 18=72, 19=71, 20=69, 21=67, 22=66, 23=68, 24=70, 25=91, 26=89, 27=53, 28=40, 29=23, 30=24, 31=29, 32=28, 33=26, 34=25, 35=27, 36=7, 37=6, 38=10, 39=9, 40=8, 41=16, 42=17, 43=55, 44=54, 45=19, 46=18, 47=20, 48=21, 49=22, 50=47, 51=45, 52=44, 53=42, 54=43, 55=41, 56=46, 57=48, 58=49, 59=50, 60=51, 61=52, 62=61, 63=62, 64=63, 65=64, 66=65, 67=56, 68=57, 69=58, 70=59, 71=60, 72=15, 73=11, 74=12, 75=13, 76=14, 77=5, 78=4, 79=2, 80=1, 81=3, 82=34, 83=35, 84=36, 85=37, 86=38, 87=39, 88=30, 89=31, 90=32, 91=33, 92=92}

