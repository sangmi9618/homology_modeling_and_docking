
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:2.0, C-S:1.0]

ORDER_CHANGED
[C%C*C%C:3.0]

STEREO_CHANGED
[C(R/S):4.0]

//
FINGERPRINTS RC
[S([CH2])[CH2]:1.0, [CH2]:3.0, [CH2][S+]([CH2])C:2.0, [CH2][S+]([CH2])C>>S([CH2])[CH2]:1.0, [CH2][S+]([CH2])C>>[C]C([CH])([CH2])C:1.0, [CH3]:2.0, [CH]:5.0, [CH]=C1N=[C]C([CH2])(C)C1C[CH2]:1.0, [CH]=C1N[C]C([CH2])(C)C1C[CH2]:1.0, [CH]C(=[CH])[NH]:1.0, [CH]C=1[C][CH]C(=[CH])N1:1.0, [CH]CSC[CH2]:1.0, [CH]C[S+](C)C[CH2]:1.0, [CH]C[S+](C)C[CH2]>>[CH]CSC[CH2]:1.0, [CH]C[S+](C)C[CH2]>>[C]CC1(C(=[CH])N[C]C1[CH2])C:1.0, [C]:12.0, [C]1=[C]C([CH2])=C(N1)CC=2N[C]=[C]C2[CH2]>>[C]1[CH]C([CH2])(C(=CC=2N[C]=[C]C2[CH2])N1)C:1.0, [C]1=[C]C([CH2])=C(N1)CC=2N[C]=[C]C2[CH2]>>[C]1[C]=NC(=CC=2N[C]=[C]C2[CH2])C1[CH2]:1.0, [C]1=[C]C([CH2])=C(N1)CC=2N[C]=[C]C2[CH2]>>[C]1[C]C([CH2])C(=CC2=N[C][CH]C2([CH2])C)N1:1.0, [C]=1NC([CH2])=C(C1[CH2])C[CH2]:2.0, [C]=1NC([CH2])=C(C1[CH2])C[CH2]>>[CH]=C1N=[C]C([CH2])(C)C1C[CH2]:1.0, [C]=1NC([CH2])=C(C1[CH2])C[CH2]>>[CH]=C1N[C]C([CH2])(C)C1C[CH2]:1.0, [C]=1[C]=C([CH2])NC1[CH2]:1.0, [C]=1[C]=C([CH2])NC1[CH2]>>[CH]C=1[C][CH]C(=[CH])N1:1.0, [C]=C([NH])C=C([N])[CH]:1.0, [C]=C([NH])CC(=[C])[NH]:3.0, [C]=C([NH])CC(=[C])[NH]>>[C]=C([NH])C=C([N])[CH]:1.0, [C]=C([NH])CC(=[C])[NH]>>[C]C(=[N])C=C([CH])[NH]:1.0, [C]=C([NH])CC(=[C])[NH]>>[C]C([NH])=CC(=[C])[NH]:1.0, [C]=C([NH])Cc1[nH]c([CH2])c([CH2])c1C[CH2]>>[C]=C([NH])C=C1N=C([CH])C([CH2])(C)C1C[CH2]:1.0, [C]=C([NH])Cc1[nH]c([CH2])c([CH2])c1C[CH2]>>[C]C(=[N])C=C1NC(=[CH])C([CH2])(C)C1C[CH2]:1.0, [C]=C([NH])[CH2]:3.0, [C]=C([NH])[CH2]>>[CH]C(=[CH])[NH]:1.0, [C]=C([NH])[CH2]>>[C]C(=[CH])[NH]:1.0, [C]=C([NH])[CH2]>>[N]C([CH])=[CH]:1.0, [C]C:1.0, [C]C(=[CH])[NH]:1.0, [C]C(=[C])[CH2]:4.0, [C]C(=[C])[CH2]>>[C]C([CH])([CH2])C:2.0, [C]C(=[C])[CH2]>>[C]C([C])[CH2]:2.0, [C]C(=[N])C=C([CH])[NH]:1.0, [C]C([CH])([CH2])C:3.0, [C]C([C])[CH2]:2.0, [C]C([NH])=CC(=[C])[NH]:1.0, [C]C=C1N=[C][C]C1[CH2]:1.0, [C]C=C1N[C][CH]C1([CH2])C:1.0, [C]C=C1N[C][C]C1[CH2]:1.0, [C]C=[C]:3.0, [C]CC1(C(=[CH])N[C]C1[CH2])C:1.0, [C]CC1(C([CH])=N[C]C1[CH2])C:1.0, [C]CC=1C(=[C]NC1[CH2])[CH2]:2.0, [C]CC=1C(=[C]NC1[CH2])[CH2]>>[C]CC1(C(=[CH])N[C]C1[CH2])C:1.0, [C]CC=1C(=[C]NC1[CH2])[CH2]>>[C]CC1(C([CH])=N[C]C1[CH2])C:1.0, [C]CC=1N[C]=[C]C1[CH2]:3.0, [C]CC=1N[C]=[C]C1[CH2]>>[C]C=C1N=[C][C]C1[CH2]:1.0, [C]CC=1N[C]=[C]C1[CH2]>>[C]C=C1N[C][CH]C1([CH2])C:1.0, [C]CC=1N[C]=[C]C1[CH2]>>[C]C=C1N[C][C]C1[CH2]:1.0, [C]C[C]:3.0, [C]C[C]>>[C]C=[C]:3.0, [C]Cc1[nH]c([CH2])c(c1CC(=O)O)C[CH2]>>[C]=CC1=NC(=[CH])C(C[CH2])C1(C)CC(=O)O:1.0, [C]Cc1[nH]c([CH2])c(c1CC(=O)O)C[CH2]>>[C]C=C1NC(=[CH])C(C[CH2])C1(C)CC(=O)O:1.0, [C]Cc1[nH]c([CH2])c(c1CC[C])C[C]>>[C]C=C1N=C([CH])C(C)(C[C])C1CC[C]:1.0, [C]Cc1[nH]c([CH2])c(c1CC[C])C[C]>>[C]C=C1NC(=[CH])C(C)(C[C])C1CC[C]:1.0, [C]Cc1[nH]c(c([CH2])c1[CH2])C[C]>>[C]C=C1N=C(C=[C])C([CH2])(C)C1[CH2]:1.0, [C]Cc1c([CH2])c([CH2])[nH]c1CC(=[C])[NH]>>[C]CC1(C(=CC(=[C])[NH])NC(=[CH])C1[CH2])C:1.0, [C]N=[C]:1.0, [C]N[C]:1.0, [C]N[C]>>[C]N=[C]:1.0, [NH]:1.0, [N]:1.0, [N]C([CH])=[CH]:1.0, [O]C([CH])C[S+](C)CC[CH]>>[O]C([CH])CSCC[CH]:1.0, [S+]:1.0, [S+]C:1.0, [S+]C>>[C]C:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH3]:3.0, [C]:2.0, [S+]:1.0]


ID=Reaction Center at Level: 1 (4)
[[CH2][S+]([CH2])C:1.0, [C]C:2.0, [C]C([CH])([CH2])C:2.0, [S+]C:1.0]


ID=Reaction Center at Level: 2 (5)
[[CH2][S+]([CH2])C:1.0, [CH]C[S+](C)C[CH2]:1.0, [C]C([CH])([CH2])C:2.0, [C]CC1(C(=[CH])N[C]C1[CH2])C:1.0, [C]CC1(C([CH])=N[C]C1[CH2])C:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[CH2]:3.0, [CH]:3.0, [C]:6.0]


ID=Reaction Center at Level: 1 (6)
[[CH]C(=[CH])[NH]:1.0, [C]=C([NH])[CH2]:3.0, [C]C(=[CH])[NH]:1.0, [C]C=[C]:3.0, [C]C[C]:3.0, [N]C([CH])=[CH]:1.0]


ID=Reaction Center at Level: 2 (8)
[[C]=C([NH])C=C([N])[CH]:1.0, [C]=C([NH])CC(=[C])[NH]:3.0, [C]C(=[N])C=C([CH])[NH]:1.0, [C]C([NH])=CC(=[C])[NH]:1.0, [C]C=C1N=[C][C]C1[CH2]:1.0, [C]C=C1N[C][CH]C1([CH2])C:1.0, [C]C=C1N[C][C]C1[CH2]:1.0, [C]CC=1N[C]=[C]C1[CH2]:3.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:2.0, [C]:6.0]


ID=Reaction Center at Level: 1 (3)
[[C]C(=[C])[CH2]:4.0, [C]C([CH])([CH2])C:2.0, [C]C([C])[CH2]:2.0]


ID=Reaction Center at Level: 2 (6)
[[CH]=C1N=[C]C([CH2])(C)C1C[CH2]:1.0, [CH]=C1N[C]C([CH2])(C)C1C[CH2]:1.0, [C]=1NC([CH2])=C(C1[CH2])C[CH2]:2.0, [C]CC1(C(=[CH])N[C]C1[CH2])C:1.0, [C]CC1(C([CH])=N[C]C1[CH2])C:1.0, [C]CC=1C(=[C]NC1[CH2])[CH2]:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]=C([NH])[CH2]>>[C]C(=[CH])[NH]
2: [C]C[C]>>[C]C=[C]
3: [C]C(=[C])[CH2]>>[C]C([C])[CH2]
4: [C]=C([NH])[CH2]>>[N]C([CH])=[CH]
5: [CH2][S+]([CH2])C>>S([CH2])[CH2]
6: [C]C[C]>>[C]C=[C]
7: [C]N[C]>>[C]N=[C]
8: [S+]C>>[C]C
9: [C]C(=[C])[CH2]>>[C]C([CH])([CH2])C
10: [C]C[C]>>[C]C=[C]
11: [C]C(=[C])[CH2]>>[C]C([CH])([CH2])C
12: [C]C(=[C])[CH2]>>[C]C([C])[CH2]
13: [C]=C([NH])[CH2]>>[CH]C(=[CH])[NH]

MMP Level 2
1: [C]CC=1N[C]=[C]C1[CH2]>>[C]C=C1N[C][CH]C1([CH2])C
2: [C]=C([NH])CC(=[C])[NH]>>[C]C([NH])=CC(=[C])[NH]
3: [C]=1NC([CH2])=C(C1[CH2])C[CH2]>>[CH]=C1N[C]C([CH2])(C)C1C[CH2]
4: [C]CC=1N[C]=[C]C1[CH2]>>[C]C=C1N=[C][C]C1[CH2]
5: [CH]C[S+](C)C[CH2]>>[CH]CSC[CH2]
6: [C]=C([NH])CC(=[C])[NH]>>[C]C(=[N])C=C([CH])[NH]
7: [C]=1[C]=C([CH2])NC1[CH2]>>[CH]C=1[C][CH]C(=[CH])N1
8: [CH2][S+]([CH2])C>>[C]C([CH])([CH2])C
9: [C]CC=1C(=[C]NC1[CH2])[CH2]>>[C]CC1(C(=[CH])N[C]C1[CH2])C
10: [C]=C([NH])CC(=[C])[NH]>>[C]=C([NH])C=C([N])[CH]
11: [C]CC=1C(=[C]NC1[CH2])[CH2]>>[C]CC1(C([CH])=N[C]C1[CH2])C
12: [C]=1NC([CH2])=C(C1[CH2])C[CH2]>>[CH]=C1N=[C]C([CH2])(C)C1C[CH2]
13: [C]CC=1N[C]=[C]C1[CH2]>>[C]C=C1N[C][C]C1[CH2]

MMP Level 3
1: [C]Cc1c([CH2])c([CH2])[nH]c1CC(=[C])[NH]>>[C]CC1(C(=CC(=[C])[NH])NC(=[CH])C1[CH2])C
2: [C]1=[C]C([CH2])=C(N1)CC=2N[C]=[C]C2[CH2]>>[C]1[CH]C([CH2])(C(=CC=2N[C]=[C]C2[CH2])N1)C
3: [C]Cc1[nH]c([CH2])c(c1CC[C])C[C]>>[C]C=C1NC(=[CH])C(C)(C[C])C1CC[C]
4: [C]=C([NH])Cc1[nH]c([CH2])c([CH2])c1C[CH2]>>[C]=C([NH])C=C1N=C([CH])C([CH2])(C)C1C[CH2]
5: [O]C([CH])C[S+](C)CC[CH]>>[O]C([CH])CSCC[CH]
6: [C]1=[C]C([CH2])=C(N1)CC=2N[C]=[C]C2[CH2]>>[C]1[C]C([CH2])C(=CC2=N[C][CH]C2([CH2])C)N1
7: [C]Cc1[nH]c(c([CH2])c1[CH2])C[C]>>[C]C=C1N=C(C=[C])C([CH2])(C)C1[CH2]
8: [CH]C[S+](C)C[CH2]>>[C]CC1(C(=[CH])N[C]C1[CH2])C
9: [C]Cc1[nH]c([CH2])c(c1CC(=O)O)C[CH2]>>[C]C=C1NC(=[CH])C(C[CH2])C1(C)CC(=O)O
10: [C]1=[C]C([CH2])=C(N1)CC=2N[C]=[C]C2[CH2]>>[C]1[C]=NC(=CC=2N[C]=[C]C2[CH2])C1[CH2]
11: [C]Cc1[nH]c([CH2])c(c1CC(=O)O)C[CH2]>>[C]=CC1=NC(=[CH])C(C[CH2])C1(C)CC(=O)O
12: [C]Cc1[nH]c([CH2])c(c1CC[C])C[C]>>[C]C=C1N=C([CH])C(C)(C[C])C1CC[C]
13: [C]=C([NH])Cc1[nH]c([CH2])c([CH2])c1C[CH2]>>[C]C(=[N])C=C1NC(=[CH])C([CH2])(C)C1C[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(O)C(N)CCSCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(O)Cc1c2C=C3N=C(C=C4NC(=Cc5[nH]c(c(c5CC(=O)O)CCC(=O)O)Cc([nH]2)c1CCC(=O)O)C(C)(CC(=O)O)C4CCC(=O)O)C(C)(CC(=O)O)C3CCC(=O)O]
3: R:M00002, P:M00004	[O=C(O)Cc1c2[nH]c(c1CCC(=O)O)Cc3[nH]c(c(c3CC(=O)O)CCC(=O)O)Cc4[nH]c(c(c4CC(=O)O)CCC(=O)O)Cc5[nH]c(c(c5CCC(=O)O)CC(=O)O)C2>>O=C(O)Cc1c2C=C3N=C(C=C4NC(=Cc5[nH]c(c(c5CC(=O)O)CCC(=O)O)Cc([nH]2)c1CCC(=O)O)C(C)(CC(=O)O)C4CCC(=O)O)C(C)(CC(=O)O)C3CCC(=O)O, O=C(O)Cc1c2[nH]c(c1CCC(=O)O)Cc3[nH]c(c(c3CC(=O)O)CCC(=O)O)Cc4[nH]c(c(c4CC(=O)O)CCC(=O)O)Cc5[nH]c(c(c5CCC(=O)O)CC(=O)O)C2>>O=C(O)Cc1c2C=C3N=C(C=C4NC(=Cc5[nH]c(c(c5CC(=O)O)CCC(=O)O)Cc([nH]2)c1CCC(=O)O)C(C)(CC(=O)O)C4CCC(=O)O)C(C)(CC(=O)O)C3CCC(=O)O, O=C(O)Cc1c2[nH]c(c1CCC(=O)O)Cc3[nH]c(c(c3CC(=O)O)CCC(=O)O)Cc4[nH]c(c(c4CC(=O)O)CCC(=O)O)Cc5[nH]c(c(c5CCC(=O)O)CC(=O)O)C2>>O=C(O)Cc1c2C=C3N=C(C=C4NC(=Cc5[nH]c(c(c5CC(=O)O)CCC(=O)O)Cc([nH]2)c1CCC(=O)O)C(C)(CC(=O)O)C4CCC(=O)O)C(C)(CC(=O)O)C3CCC(=O)O, O=C(O)Cc1c2[nH]c(c1CCC(=O)O)Cc3[nH]c(c(c3CC(=O)O)CCC(=O)O)Cc4[nH]c(c(c4CC(=O)O)CCC(=O)O)Cc5[nH]c(c(c5CCC(=O)O)CC(=O)O)C2>>O=C(O)Cc1c2C=C3N=C(C=C4NC(=Cc5[nH]c(c(c5CC(=O)O)CCC(=O)O)Cc([nH]2)c1CCC(=O)O)C(C)(CC(=O)O)C4CCC(=O)O)C(C)(CC(=O)O)C3CCC(=O)O, O=C(O)Cc1c2[nH]c(c1CCC(=O)O)Cc3[nH]c(c(c3CC(=O)O)CCC(=O)O)Cc4[nH]c(c(c4CC(=O)O)CCC(=O)O)Cc5[nH]c(c(c5CCC(=O)O)CC(=O)O)C2>>O=C(O)Cc1c2C=C3N=C(C=C4NC(=Cc5[nH]c(c(c5CC(=O)O)CCC(=O)O)Cc([nH]2)c1CCC(=O)O)C(C)(CC(=O)O)C4CCC(=O)O)C(C)(CC(=O)O)C3CCC(=O)O, O=C(O)Cc1c2[nH]c(c1CCC(=O)O)Cc3[nH]c(c(c3CC(=O)O)CCC(=O)O)Cc4[nH]c(c(c4CC(=O)O)CCC(=O)O)Cc5[nH]c(c(c5CCC(=O)O)CC(=O)O)C2>>O=C(O)Cc1c2C=C3N=C(C=C4NC(=Cc5[nH]c(c(c5CC(=O)O)CCC(=O)O)Cc([nH]2)c1CCC(=O)O)C(C)(CC(=O)O)C4CCC(=O)O)C(C)(CC(=O)O)C3CCC(=O)O, O=C(O)Cc1c2[nH]c(c1CCC(=O)O)Cc3[nH]c(c(c3CC(=O)O)CCC(=O)O)Cc4[nH]c(c(c4CC(=O)O)CCC(=O)O)Cc5[nH]c(c(c5CCC(=O)O)CC(=O)O)C2>>O=C(O)Cc1c2C=C3N=C(C=C4NC(=Cc5[nH]c(c(c5CC(=O)O)CCC(=O)O)Cc([nH]2)c1CCC(=O)O)C(C)(CC(=O)O)C4CCC(=O)O)C(C)(CC(=O)O)C3CCC(=O)O, O=C(O)Cc1c2[nH]c(c1CCC(=O)O)Cc3[nH]c(c(c3CC(=O)O)CCC(=O)O)Cc4[nH]c(c(c4CC(=O)O)CCC(=O)O)Cc5[nH]c(c(c5CCC(=O)O)CC(=O)O)C2>>O=C(O)Cc1c2C=C3N=C(C=C4NC(=Cc5[nH]c(c(c5CC(=O)O)CCC(=O)O)Cc([nH]2)c1CCC(=O)O)C(C)(CC(=O)O)C4CCC(=O)O)C(C)(CC(=O)O)C3CCC(=O)O, O=C(O)Cc1c2[nH]c(c1CCC(=O)O)Cc3[nH]c(c(c3CC(=O)O)CCC(=O)O)Cc4[nH]c(c(c4CC(=O)O)CCC(=O)O)Cc5[nH]c(c(c5CCC(=O)O)CC(=O)O)C2>>O=C(O)Cc1c2C=C3N=C(C=C4NC(=Cc5[nH]c(c(c5CC(=O)O)CCC(=O)O)Cc([nH]2)c1CCC(=O)O)C(C)(CC(=O)O)C4CCC(=O)O)C(C)(CC(=O)O)C3CCC(=O)O, O=C(O)Cc1c2[nH]c(c1CCC(=O)O)Cc3[nH]c(c(c3CC(=O)O)CCC(=O)O)Cc4[nH]c(c(c4CC(=O)O)CCC(=O)O)Cc5[nH]c(c(c5CCC(=O)O)CC(=O)O)C2>>O=C(O)Cc1c2C=C3N=C(C=C4NC(=Cc5[nH]c(c(c5CC(=O)O)CCC(=O)O)Cc([nH]2)c1CCC(=O)O)C(C)(CC(=O)O)C4CCC(=O)O)C(C)(CC(=O)O)C3CCC(=O)O, O=C(O)Cc1c2[nH]c(c1CCC(=O)O)Cc3[nH]c(c(c3CC(=O)O)CCC(=O)O)Cc4[nH]c(c(c4CC(=O)O)CCC(=O)O)Cc5[nH]c(c(c5CCC(=O)O)CC(=O)O)C2>>O=C(O)Cc1c2C=C3N=C(C=C4NC(=Cc5[nH]c(c(c5CC(=O)O)CCC(=O)O)Cc([nH]2)c1CCC(=O)O)C(C)(CC(=O)O)C4CCC(=O)O)C(C)(CC(=O)O)C3CCC(=O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH2:4][C:5]:1:[C:6]2:[NH:7]:[C:8](:[C:9]1[CH2:10][CH2:11][C:12](=[O:13])[OH:14])[CH2:15][C:16]:3:[NH:17]:[C:18](:[C:19](:[C:20]3[CH2:21][C:22](=[O:23])[OH:24])[CH2:25][CH2:26][C:27](=[O:28])[OH:29])[CH2:30][C:31]:4:[NH:32]:[C:33](:[C:34](:[C:35]4[CH2:36][C:37](=[O:38])[OH:39])[CH2:40][CH2:41][C:42](=[O:43])[OH:44])[CH2:45][C:46]:5:[NH:47]:[C:48](:[C:49](:[C:50]5[CH2:51][CH2:52][C:53](=[O:54])[OH:55])[CH2:56][C:57](=[O:58])[OH:59])[CH2:60]2.[O:61]=[C:62]([OH:63])[CH:64]([NH2:65])[CH2:66][CH2:67][S+:68]([CH3:69])[CH2:70][CH:71]1[O:72][CH:73]([N:74]:2:[CH:75]:[N:76]:[C:77]:3:[C:78](:[N:79]:[CH:80]:[N:81]:[C:82]32)[NH2:83])[CH:84]([OH:85])[CH:86]1[OH:87]>>[H+:89].[O:38]=[C:37]([OH:39])[CH2:36][C:35]:1:[C:31]:2[CH:30]=[C:18]3[N:17]=[C:16]([CH:15]=[C:8]4[NH:7][C:6](=[CH:60][C:48]:5:[NH:47]:[C:46](:[C:50](:[C:49]5[CH2:56][C:57](=[O:58])[OH:59])[CH2:51][CH2:52][C:53](=[O:54])[OH:55])[CH2:45][C:33](:[NH:32]2):[C:34]1[CH2:40][CH2:41][C:42](=[O:43])[OH:44])[C:5]([CH3:69])([CH2:4][C:2](=[O:1])[OH:3])[CH:9]4[CH2:10][CH2:11][C:12](=[O:13])[OH:14])[C:20]([CH3:88])([CH2:21][C:22](=[O:23])[OH:24])[CH:19]3[CH2:25][CH2:26][C:27](=[O:28])[OH:29].[O:61]=[C:62]([OH:63])[CH:64]([NH2:65])[CH2:66][CH2:67][S:68][CH2:70][CH:71]1[O:72][CH:73]([N:74]:2:[CH:75]:[N:76]:[C:77]:3:[C:78](:[N:79]:[CH:80]:[N:81]:[C:82]32)[NH2:83])[CH:84]([OH:85])[CH:86]1[OH:87]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=69, 2=68, 3=67, 4=66, 5=64, 6=62, 7=61, 8=63, 9=65, 10=70, 11=71, 12=86, 13=84, 14=73, 15=72, 16=74, 17=75, 18=76, 19=77, 20=82, 21=81, 22=80, 23=79, 24=78, 25=83, 26=85, 27=87, 28=45, 29=33, 30=34, 31=35, 32=31, 33=32, 34=30, 35=18, 36=19, 37=20, 38=16, 39=17, 40=15, 41=8, 42=9, 43=5, 44=6, 45=7, 46=60, 47=48, 48=49, 49=50, 50=46, 51=47, 52=51, 53=52, 54=53, 55=54, 56=55, 57=56, 58=57, 59=58, 60=59, 61=4, 62=2, 63=1, 64=3, 65=10, 66=11, 67=12, 68=13, 69=14, 70=21, 71=22, 72=23, 73=24, 74=25, 75=26, 76=27, 77=28, 78=29, 79=36, 80=37, 81=38, 82=39, 83=40, 84=41, 85=42, 86=43, 87=44}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=81, 2=82, 3=83, 4=78, 5=79, 6=80, 7=84, 8=77, 9=76, 10=75, 11=74, 12=85, 13=87, 14=72, 15=73, 16=71, 17=70, 18=69, 19=68, 20=66, 21=64, 22=63, 23=65, 24=67, 25=88, 26=86, 27=40, 28=39, 29=45, 30=12, 31=11, 32=10, 33=9, 34=8, 35=7, 36=6, 37=5, 38=33, 39=31, 40=32, 41=30, 42=18, 43=19, 44=20, 45=16, 46=17, 47=15, 48=14, 49=13, 50=21, 51=22, 52=23, 53=24, 54=25, 55=26, 56=27, 57=28, 58=29, 59=34, 60=35, 61=36, 62=37, 63=38, 64=4, 65=2, 66=1, 67=3, 68=57, 69=51, 70=52, 71=53, 72=54, 73=55, 74=56, 75=58, 76=59, 77=60, 78=61, 79=62, 80=46, 81=47, 82=48, 83=49, 84=50, 85=41, 86=42, 87=43, 88=44, 89=89}

