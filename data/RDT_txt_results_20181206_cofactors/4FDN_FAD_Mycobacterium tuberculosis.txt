
//
FINGERPRINTS BC

ORDER_CHANGED
[C%C*C%C:1.0, C%N*C%N:2.0, C-O*C=O:1.0, O-O*O=O:1.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O=C([CH])[CH]:2.0, O=C1N=C2C(=N[C]=C([CH])N2C[CH])C(=O)N1>>O=c1[nH]c(=O)c2N[C]=C([CH])N(c2[nH]1)C[CH]:1.0, O=C1N[C]N=C2C1=NC([CH])=[C]N2[CH2]>>O=C1N[C]NC2=C1NC([CH])=[C]N2[CH2]:1.0, O=C1[CH]OC([CH2])C1O:1.0, O=O:4.0, O=O>>OO:6.0, OC1[CH]OC([CH2])C1O:1.0, OC1[CH]OC([CH2])C1O>>O=C1[CH]OC([CH2])C1O:1.0, OO:4.0, [CH]:3.0, [CH]C([CH])O:3.0, [CH]C([CH])O>>O=C([CH])[CH]:2.0, [CH]C([CH])O>>[C]C([CH])O:1.0, [CH]O:1.0, [CH]O>>[C]=O:1.0, [C]:5.0, [C]=C([N])NC(=O)[NH]:1.0, [C]=C([N])[NH]:1.0, [C]=CC=1N=C(C(=[N])[N]C1[CH])C(=O)[NH]>>[C]=CC=1NC(=C([NH])[N]C1[CH])C(=O)[NH]:1.0, [C]=O:1.0, [C]C(=[C])NC(=[C])[CH]:1.0, [C]C(=[C])[NH]:1.0, [C]C([CH])O:1.0, [C]C([C])=NC(=[C])[CH]:1.0, [C]C([C])=NC(=[C])[CH]>>[C]C(=[C])NC(=[C])[CH]:1.0, [C]C([C])=[N]:1.0, [C]C([C])=[N]>>[C]C(=[C])[NH]:1.0, [C]C([N])=NC(=O)[NH]:1.0, [C]C([N])=NC(=O)[NH]>>[C]=C([N])NC(=O)[NH]:1.0, [C]C([N])=[N]:1.0, [C]C([N])=[N]>>[C]=C([N])[NH]:1.0, [C]N([CH2])C1=NC(=O)N[C]C1=[N]>>[C]N([CH2])C=1NC(=O)N[C]C1[NH]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]>>[C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]>>[C]NC(=C([N])[NH])C(=O)[NH]:1.0, [C]N=[C]:2.0, [C]N=[C]>>[C]N[C]:2.0, [C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]NC(=C([N])[NH])C(=O)[NH]:1.0, [C]N[C]:2.0, [NH]:2.0, [N]:2.0, [OH]:3.0, [O]:3.0, [O]C1OC(CO)C(O)C1O>>[O]C1OC(CO)C(O)C1=O:1.0, [O]C1O[CH]C(O)C1=O:1.0, [O]C1O[CH]C(O)C1O:1.0, [O]C1O[CH]C(O)C1O>>[O]C1O[CH]C(O)C1=O:2.0, [P]OC1OC([CH2])C(O)C1O>>O=C1C(OC([CH2])C1O)O[P]:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (6)
[[CH]:1.0, [C]:5.0, [NH]:2.0, [N]:2.0, [OH]:3.0, [O]:3.0]


ID=Reaction Center at Level: 1 (12)
[O=C([CH])[CH]:1.0, O=O:2.0, OO:2.0, [CH]C([CH])O:1.0, [CH]O:1.0, [C]=C([N])[NH]:1.0, [C]=O:1.0, [C]C(=[C])[NH]:1.0, [C]C([C])=[N]:1.0, [C]C([N])=[N]:1.0, [C]N=[C]:2.0, [C]N[C]:2.0]


ID=Reaction Center at Level: 2 (14)
[O=C([CH])[CH]:1.0, O=O:2.0, OO:2.0, [CH]C([CH])O:1.0, [C]=C([N])NC(=O)[NH]:1.0, [C]C(=[C])NC(=[C])[CH]:1.0, [C]C([C])=NC(=[C])[CH]:1.0, [C]C([N])=NC(=O)[NH]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]:1.0, [C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]NC(=C([N])[NH])C(=O)[NH]:1.0, [O]C1O[CH]C(O)C1=O:1.0, [O]C1O[CH]C(O)C1O:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:3.0, [C]:1.0]


ID=Reaction Center at Level: 1 (3)
[O=C([CH])[CH]:1.0, [CH]C([CH])O:2.0, [C]C([CH])O:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C1[CH]OC([CH2])C1O:1.0, OC1[CH]OC([CH2])C1O:1.0, [O]C1O[CH]C(O)C1=O:1.0, [O]C1O[CH]C(O)C1O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]O>>[C]=O
2: O=O>>OO
3: [C]N=[C]>>[C]N[C]
4: [C]C([N])=[N]>>[C]=C([N])[NH]
5: [CH]C([CH])O>>O=C([CH])[CH]
6: [C]C([C])=[N]>>[C]C(=[C])[NH]
7: [CH]C([CH])O>>[C]C([CH])O
8: [C]N=[C]>>[C]N[C]

MMP Level 2
1: [CH]C([CH])O>>O=C([CH])[CH]
2: O=O>>OO
3: [C]C([N])=NC(=O)[NH]>>[C]=C([N])NC(=O)[NH]
4: [C]N=C(C([C])=[N])N([C])[CH2]>>[C]NC(=C([C])[NH])N([C])[CH2]
5: [O]C1O[CH]C(O)C1O>>[O]C1O[CH]C(O)C1=O
6: [C]N=C(C([N])=[N])C(=O)[NH]>>[C]NC(=C([N])[NH])C(=O)[NH]
7: OC1[CH]OC([CH2])C1O>>O=C1[CH]OC([CH2])C1O
8: [C]C([C])=NC(=[C])[CH]>>[C]C(=[C])NC(=[C])[CH]

MMP Level 3
1: [O]C1O[CH]C(O)C1O>>[O]C1O[CH]C(O)C1=O
2: O=O>>OO
3: [C]N([CH2])C1=NC(=O)N[C]C1=[N]>>[C]N([CH2])C=1NC(=O)N[C]C1[NH]
4: O=C1N=C2C(=N[C]=C([CH])N2C[CH])C(=O)N1>>O=c1[nH]c(=O)c2N[C]=C([CH])N(c2[nH]1)C[CH]
5: [P]OC1OC([CH2])C(O)C1O>>O=C1C(OC([CH2])C1O)O[P]
6: O=C1N[C]N=C2C1=NC([CH])=[C]N2[CH2]>>O=C1N[C]NC2=C1NC([CH])=[C]N2[CH2]
7: [O]C1OC(CO)C(O)C1O>>[O]C1OC(CO)C(O)C1=O
8: [C]=CC=1N=C(C(=[N])[N]C1[CH])C(=O)[NH]>>[C]=CC=1NC(=C([NH])[N]C1[CH])C(=O)[NH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=P(O)(OCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)C)OC1OC(CO)C(O)C1O>>O=C1C(OC(CO)C1O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)C, O=P(O)(OCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)C)OC1OC(CO)C(O)C1O>>O=C1C(OC(CO)C1O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)C, O=P(O)(OCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)C)OC1OC(CO)C(O)C1O>>O=C1C(OC(CO)C1O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)C]
2: R:M00002, P:M00005	[O=O>>OO]
3: R:M00003, P:M00006	[O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C, O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C, O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C, O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C]


//
SELECTED AAM MAPPING
[O:118]=[O:119].[O:65]=[C:66]1[N:67]=[C:68]2[C:69](=[N:70][C:71]:3:[CH:72]:[C:73](:[C:74](:[CH:75]:[C:76]3[N:77]2[CH2:78][CH:79]([OH:80])[CH:81]([OH:82])[CH:83]([OH:84])[CH2:85][O:86][P:87](=[O:88])([OH:89])[O:90][P:91](=[O:92])([OH:93])[O:94][CH2:95][CH:96]4[O:97][CH:98]([N:99]:5:[CH:100]:[N:101]:[C:102]:6:[C:103](:[N:104]:[CH:105]:[N:106]:[C:107]65)[NH2:108])[CH:109]([OH:110])[CH:111]4[OH:112])[CH3:113])[CH3:114])[C:115](=[O:116])[NH:117]1.[O:1]=[P:2]([OH:3])([O:4][CH2:5][CH:6]=[C:7]([CH3:8])[CH2:9][CH2:10][CH:11]=[C:12]([CH3:13])[CH2:14][CH2:15][CH:16]=[C:17]([CH3:18])[CH2:19][CH2:20][CH:21]=[C:22]([CH3:23])[CH2:24][CH2:25][CH:26]=[C:27]([CH3:28])[CH2:29][CH2:30][CH:31]=[C:32]([CH3:33])[CH2:34][CH2:35][CH:36]=[C:37]([CH3:38])[CH2:39][CH2:40][CH:41]=[C:42]([CH3:43])[CH2:44][CH2:45][CH:46]=[C:47]([CH3:48])[CH2:49][CH2:50][CH:51]=[C:52]([CH3:53])[CH3:54])[O:55][CH:56]1[O:57][CH:58]([CH2:59][OH:60])[CH:61]([OH:62])[CH:63]1[OH:64]>>[O:65]=[C:66]1[NH:117][C:115](=[O:116])[C:69]=2[NH:70][C:71]:3:[CH:72]:[C:73](:[C:74](:[CH:75]:[C:76]3[N:77]([C:68]2[NH:67]1)[CH2:78][CH:79]([OH:80])[CH:81]([OH:82])[CH:83]([OH:84])[CH2:85][O:86][P:87](=[O:88])([OH:89])[O:90][P:91](=[O:92])([OH:93])[O:94][CH2:95][CH:96]4[O:97][CH:98]([N:99]:5:[CH:100]:[N:101]:[C:102]:6:[C:103](:[N:104]:[CH:105]:[N:106]:[C:107]65)[NH2:108])[CH:109]([OH:110])[CH:111]4[OH:112])[CH3:113])[CH3:114].[O:64]=[C:63]1[CH:56]([O:57][CH:58]([CH2:59][OH:60])[CH:61]1[OH:62])[O:55][P:2](=[O:1])([OH:3])[O:4][CH2:5][CH:6]=[C:7]([CH3:8])[CH2:9][CH2:10][CH:11]=[C:12]([CH3:13])[CH2:14][CH2:15][CH:16]=[C:17]([CH3:18])[CH2:19][CH2:20][CH:21]=[C:22]([CH3:23])[CH2:24][CH2:25][CH:26]=[C:27]([CH3:28])[CH2:29][CH2:30][CH:31]=[C:32]([CH3:33])[CH2:34][CH2:35][CH:36]=[C:37]([CH3:38])[CH2:39][CH2:40][CH:41]=[C:42]([CH3:43])[CH2:44][CH2:45][CH:46]=[C:47]([CH3:48])[CH2:49][CH2:50][CH:51]=[C:52]([CH3:54])[CH3:53].[OH:118][OH:119]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=54, 2=52, 3=51, 4=50, 5=49, 6=47, 7=46, 8=45, 9=44, 10=42, 11=41, 12=40, 13=39, 14=37, 15=36, 16=35, 17=34, 18=32, 19=31, 20=30, 21=29, 22=27, 23=26, 24=25, 25=24, 26=22, 27=21, 28=20, 29=19, 30=17, 31=16, 32=15, 33=14, 34=12, 35=11, 36=10, 37=9, 38=7, 39=6, 40=5, 41=4, 42=2, 43=1, 44=3, 45=55, 46=56, 47=63, 48=61, 49=58, 50=57, 51=59, 52=60, 53=62, 54=64, 55=8, 56=13, 57=18, 58=23, 59=28, 60=33, 61=38, 62=43, 63=48, 64=53, 65=118, 66=119, 67=114, 68=73, 69=72, 70=71, 71=76, 72=75, 73=74, 74=113, 75=77, 76=68, 77=67, 78=66, 79=65, 80=117, 81=115, 82=116, 83=69, 84=70, 85=78, 86=79, 87=81, 88=83, 89=85, 90=86, 91=87, 92=88, 93=89, 94=90, 95=91, 96=92, 97=93, 98=94, 99=95, 100=96, 101=111, 102=109, 103=98, 104=97, 105=99, 106=100, 107=101, 108=102, 109=107, 110=106, 111=105, 112=104, 113=103, 114=108, 115=110, 116=112, 117=84, 118=82, 119=80}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=64, 2=62, 3=61, 4=60, 5=59, 6=57, 7=56, 8=55, 9=54, 10=52, 11=51, 12=50, 13=49, 14=47, 15=46, 16=45, 17=44, 18=42, 19=41, 20=40, 21=39, 22=37, 23=36, 24=35, 25=34, 26=32, 27=31, 28=30, 29=29, 30=27, 31=26, 32=25, 33=24, 34=22, 35=21, 36=20, 37=19, 38=17, 39=16, 40=15, 41=14, 42=11, 43=12, 44=13, 45=10, 46=3, 47=2, 48=1, 49=8, 50=5, 51=4, 52=6, 53=7, 54=9, 55=18, 56=23, 57=28, 58=33, 59=38, 60=43, 61=48, 62=53, 63=58, 64=63, 65=118, 66=119, 67=117, 68=74, 69=73, 70=72, 71=77, 72=76, 73=75, 74=116, 75=78, 76=79, 77=70, 78=71, 79=68, 80=69, 81=67, 82=66, 83=65, 84=80, 85=81, 86=82, 87=84, 88=86, 89=88, 90=89, 91=90, 92=91, 93=92, 94=93, 95=94, 96=95, 97=96, 98=97, 99=98, 100=99, 101=114, 102=112, 103=101, 104=100, 105=102, 106=103, 107=104, 108=105, 109=110, 110=109, 111=108, 112=107, 113=106, 114=111, 115=113, 116=115, 117=87, 118=85, 119=83}

