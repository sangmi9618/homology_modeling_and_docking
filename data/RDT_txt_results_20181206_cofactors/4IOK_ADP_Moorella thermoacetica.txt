
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0, O-P:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=CN(C([CH])=[CH])C[CH]:1.0, O=CO:3.0, O=CO>>O=CN(C([CH])=[CH])C[CH]:1.0, O=CO>>O=P(O)(O)O:2.0, O=CO>>[C]N([CH2])C=O:1.0, O=CO>>[N]C=O:1.0, O=P(O)(O)O:3.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O:1.0, [CH]:4.0, [CH]C(=[CH])NC[CH]:1.0, [CH]C(=[CH])NC[CH]>>O=CN(C([CH])=[CH])C[CH]:1.0, [CH]C=C(C=[CH])NCC([NH])[CH2]>>O=CN(C(=C[CH])C=[CH])CC([NH])[CH2]:1.0, [CH]O:1.0, [CH]O>>[P]O:1.0, [C]N([CH2])C=O:1.0, [C]N([CH])[CH2]:1.0, [C]NC(C[NH])C[NH]:1.0, [C]NC(C[NH])C[NH]>>[C]NC(C[N])C[NH]:1.0, [C]NC(C[N])C[NH]:1.0, [C]NCC1NC([C])=[C]NC1>>[C]C1=[C]NCC(N1)CN([C])[CH]:1.0, [C]N[CH2]:1.0, [C]N[CH2]>>[C]N([CH])[CH2]:1.0, [NH]:1.0, [NH]C([CH2])[CH2]:2.0, [NH]C([CH2])[CH2]>>[NH]C([CH2])[CH2]:1.0, [N]:1.0, [N]C=O:1.0, [OH]:3.0, [O]:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[CH]:2.0, [N]:1.0, [OH]:2.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (8)
[O=CO:1.0, O=P(O)(O)O:1.0, [CH]O:1.0, [C]N([CH])[CH2]:1.0, [N]C=O:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (6)
[O=CN(C([CH])=[CH])C[CH]:1.0, O=CO:2.0, O=P(O)(O)O:2.0, O=P(O)(O)O[P]:1.0, [C]N([CH2])C=O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (1)
[[NH]C([CH2])[CH2]:2.0]


ID=Reaction Center at Level: 2 (2)
[[C]NC(C[NH])C[NH]:1.0, [C]NC(C[N])C[NH]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: O=CO>>[N]C=O
2: [C]N[CH2]>>[C]N([CH])[CH2]
3: [CH]O>>[P]O
4: [O]P(=O)(O)O>>O=P(O)(O)O
5: [NH]C([CH2])[CH2]>>[NH]C([CH2])[CH2]
6: [P]O[P]>>[P]O

MMP Level 2
1: O=CO>>[C]N([CH2])C=O
2: [CH]C(=[CH])NC[CH]>>O=CN(C([CH])=[CH])C[CH]
3: O=CO>>O=P(O)(O)O
4: O=P(O)(O)O[P]>>O=P(O)(O)O
5: [C]NC(C[NH])C[NH]>>[C]NC(C[N])C[NH]
6: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O

MMP Level 3
1: O=CO>>O=CN(C([CH])=[CH])C[CH]
2: [CH]C=C(C=[CH])NCC([NH])[CH2]>>O=CN(C(=C[CH])C=[CH])CC([NH])[CH2]
3: O=CO>>O=P(O)(O)O
4: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O
5: [C]NCC1NC([C])=[C]NC1>>[C]C1=[C]NCC(N1)CN([C])[CH]
6: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00005	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)O]
3: R:M00002, P:M00005	[O=CO>>O=P(O)(O)O]
4: R:M00002, P:M00006	[O=CO>>O=CN(c1ccc(cc1)C(=O)NC(C(=O)O)CCC(=O)O)CC2Nc3c(=O)nc(N)[nH]c3NC2]
5: R:M00003, P:M00006	[O=c1nc(N)[nH]c2NCC(Nc12)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)O>>O=CN(c1ccc(cc1)C(=O)NC(C(=O)O)CCC(=O)O)CC2Nc3c(=O)nc(N)[nH]c3NC2, O=c1nc(N)[nH]c2NCC(Nc12)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)O>>O=CN(c1ccc(cc1)C(=O)NC(C(=O)O)CCC(=O)O)CC2Nc3c(=O)nc(N)[nH]c3NC2]


//
SELECTED AAM MAPPING
[O:64]=[CH:65][OH:66].[O:31]=[C:30]([OH:32])[CH2:29][CH2:28][CH:24]([NH:23][C:21](=[O:22])[C:18]:1:[CH:17]:[CH:16]:[C:15](:[CH:20]:[CH:19]1)[NH:14][CH2:13][CH:10]2[NH:11][C:12]=3[C:2](=[O:1])[N:3]=[C:4]([NH2:5])[NH:6][C:7]3[NH:8][CH2:9]2)[C:25](=[O:26])[OH:27].[O:33]=[P:34]([OH:35])([OH:36])[O:37][P:38](=[O:39])([OH:40])[O:41][P:42](=[O:43])([OH:44])[O:45][CH2:46][CH:47]1[O:48][CH:49]([N:50]:2:[CH:51]:[N:52]:[C:53]:3:[C:54](:[N:55]:[CH:56]:[N:57]:[C:58]32)[NH2:59])[CH:60]([OH:61])[CH:62]1[OH:63]>>[O:64]=[CH:65][N:14]([C:15]:1:[CH:16]:[CH:17]:[C:18](:[CH:19]:[CH:20]1)[C:21](=[O:22])[NH:23][CH:24]([C:25](=[O:26])[OH:27])[CH2:28][CH2:29][C:30](=[O:31])[OH:32])[CH2:13][CH:10]2[NH:11][C:12]=3[C:2](=[O:1])[N:3]=[C:4]([NH2:5])[NH:6][C:7]3[NH:8][CH2:9]2.[O:33]=[P:34]([OH:35])([OH:36])[OH:66].[O:39]=[P:38]([OH:37])([OH:40])[O:41][P:42](=[O:43])([OH:44])[O:45][CH2:46][CH:47]1[O:48][CH:49]([N:50]:2:[CH:51]:[N:52]:[C:53]:3:[C:54](:[N:55]:[CH:56]:[N:57]:[C:58]32)[NH2:59])[CH:60]([OH:61])[CH:62]1[OH:63]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=56, 2=57, 3=58, 4=53, 5=54, 6=55, 7=59, 8=52, 9=51, 10=50, 11=49, 12=60, 13=62, 14=47, 15=48, 16=46, 17=45, 18=42, 19=43, 20=44, 21=41, 22=38, 23=39, 24=40, 25=37, 26=34, 27=33, 28=35, 29=36, 30=63, 31=61, 32=65, 33=64, 34=66, 35=9, 36=10, 37=11, 38=12, 39=7, 40=8, 41=6, 42=4, 43=3, 44=2, 45=1, 46=5, 47=13, 48=14, 49=15, 50=16, 51=17, 52=18, 53=19, 54=20, 55=21, 56=22, 57=23, 58=24, 59=28, 60=29, 61=30, 62=31, 63=32, 64=25, 65=26, 66=27}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=54, 2=55, 3=56, 4=51, 5=52, 6=53, 7=57, 8=50, 9=49, 10=48, 11=47, 12=58, 13=60, 14=45, 15=46, 16=44, 17=43, 18=40, 19=41, 20=42, 21=39, 22=36, 23=35, 24=37, 25=38, 26=61, 27=59, 28=64, 29=63, 30=62, 31=65, 32=66, 33=34, 34=23, 35=24, 36=25, 37=32, 38=33, 39=31, 40=29, 41=28, 42=26, 43=27, 44=30, 45=22, 46=3, 47=2, 48=1, 49=4, 50=5, 51=6, 52=7, 53=8, 54=9, 55=10, 56=11, 57=12, 58=13, 59=17, 60=18, 61=19, 62=20, 63=21, 64=14, 65=15, 66=16}

