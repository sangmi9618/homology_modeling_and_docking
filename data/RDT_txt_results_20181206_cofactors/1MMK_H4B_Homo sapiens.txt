
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:2.0, O=O:1.0]

ORDER_CHANGED
[C%C*C%C:1.0, C%N*C%N:1.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O=C1N=[C]NC=2NCC([CH])NC12>>O=C1N=[C]NC2=NCC([CH])NC12O:1.0, O=O:4.0, O=O>>[CH]C(=[CH])O:1.0, O=O>>[CH]C=C(O)C=[CH]:1.0, O=O>>[C]C([C])([NH])O:1.0, O=O>>[C]O:2.0, O=O>>[N]C(=O)C(O)(N[CH])C(=[N])[NH]:1.0, O=c1nc(N)[nH]c2NC[CH]Nc12>>O=C1N=C(N)NC2=NC[CH]NC12O:1.0, [CH]:3.0, [CH]C(=[CH])O:2.0, [CH]C([CH])O:2.0, [CH]C([CH])O>>[CH]C([CH])O:1.0, [CH]C=C(O)C=[CH]:1.0, [CH]C=CC=[CH]:1.0, [CH]C=CC=[CH]>>[CH]C=C(O)C=[CH]:1.0, [CH]C=[CH]:1.0, [CH]C=[CH]>>[CH]C(=[CH])O:1.0, [C]:5.0, [C]=1C=CC=CC1>>OC=1C=C[C]=CC1:1.0, [C]=C([NH])NC[CH]:1.0, [C]=C([NH])NC[CH]>>[C]C([NH])=NC[CH]:1.0, [C]=C([NH])[NH]:1.0, [C]=C([NH])[NH]>>[C]C(=[N])[NH]:1.0, [C]=N[CH2]:1.0, [C]C(=[C])[NH]:1.0, [C]C(=[C])[NH]>>[C]C([C])([NH])O:1.0, [C]C(=[N])[NH]:1.0, [C]C([C])([NH])O:2.0, [C]C([NH])=NC[CH]:1.0, [C]NC(=N[CH2])C([C])([NH])O:1.0, [C]NC(C[NH])C(O)C(O)C>>[C]NC(C[N])C(O)C(O)C:1.0, [C]NC(N[CH2])=C([C])[NH]:1.0, [C]NC(N[CH2])=C([C])[NH]>>[C]NC(=N[CH2])C([C])([NH])O:1.0, [C]NC=1NCC([CH])NC1[C]>>[C]NC1=NCC([CH])NC1([C])O:1.0, [C]N[CH2]:1.0, [C]N[CH2]>>[C]=N[CH2]:1.0, [C]O:2.0, [NH]:1.0, [NH]C([CH2])C(O)C(O)C:2.0, [NH]C([CH2])C(O)C(O)C>>[NH]C([CH2])C(O)C(O)C:1.0, [N]:1.0, [N]C(=O)C(N[CH])=C([NH])[NH]:1.0, [N]C(=O)C(N[CH])=C([NH])[NH]>>[N]C(=O)C(O)(N[CH])C(=[N])[NH]:1.0, [N]C(=O)C(O)(N[CH])C(=[N])[NH]:1.0, [OH]:2.0, [O]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [OH]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (4)
[O=O:2.0, [CH]C(=[CH])O:1.0, [C]C([C])([NH])O:1.0, [C]O:2.0]


ID=Reaction Center at Level: 2 (5)
[O=O:2.0, [CH]C(=[CH])O:1.0, [CH]C=C(O)C=[CH]:1.0, [C]C([C])([NH])O:1.0, [N]C(=O)C(O)(N[CH])C(=[N])[NH]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:4.0, [NH]:1.0, [N]:1.0]


ID=Reaction Center at Level: 1 (6)
[[C]=C([NH])[NH]:1.0, [C]=N[CH2]:1.0, [C]C(=[C])[NH]:1.0, [C]C(=[N])[NH]:1.0, [C]C([C])([NH])O:1.0, [C]N[CH2]:1.0]


ID=Reaction Center at Level: 2 (6)
[[C]=C([NH])NC[CH]:1.0, [C]C([NH])=NC[CH]:1.0, [C]NC(=N[CH2])C([C])([NH])O:1.0, [C]NC(N[CH2])=C([C])[NH]:1.0, [N]C(=O)C(N[CH])=C([NH])[NH]:1.0, [N]C(=O)C(O)(N[CH])C(=[N])[NH]:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:2.0, [C]:2.0]


ID=Reaction Center at Level: 1 (3)
[[CH]C([CH])O:2.0, [C]C(=[C])[NH]:1.0, [C]C([C])([NH])O:1.0]


ID=Reaction Center at Level: 2 (3)
[[NH]C([CH2])C(O)C(O)C:2.0, [N]C(=O)C(N[CH])=C([NH])[NH]:1.0, [N]C(=O)C(O)(N[CH])C(=[N])[NH]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]=C([NH])[NH]>>[C]C(=[N])[NH]
2: [C]C(=[C])[NH]>>[C]C([C])([NH])O
3: O=O>>[C]O
4: [C]N[CH2]>>[C]=N[CH2]
5: [CH]C=[CH]>>[CH]C(=[CH])O
6: O=O>>[C]O
7: [CH]C([CH])O>>[CH]C([CH])O

MMP Level 2
1: [C]NC(N[CH2])=C([C])[NH]>>[C]NC(=N[CH2])C([C])([NH])O
2: [N]C(=O)C(N[CH])=C([NH])[NH]>>[N]C(=O)C(O)(N[CH])C(=[N])[NH]
3: O=O>>[C]C([C])([NH])O
4: [C]=C([NH])NC[CH]>>[C]C([NH])=NC[CH]
5: [CH]C=CC=[CH]>>[CH]C=C(O)C=[CH]
6: O=O>>[CH]C(=[CH])O
7: [NH]C([CH2])C(O)C(O)C>>[NH]C([CH2])C(O)C(O)C

MMP Level 3
1: O=c1nc(N)[nH]c2NC[CH]Nc12>>O=C1N=C(N)NC2=NC[CH]NC12O
2: O=C1N=[C]NC=2NCC([CH])NC12>>O=C1N=[C]NC2=NCC([CH])NC12O
3: O=O>>[N]C(=O)C(O)(N[CH])C(=[N])[NH]
4: [C]NC=1NCC([CH])NC1[C]>>[C]NC1=NCC([CH])NC1([C])O
5: [C]=1C=CC=CC1>>OC=1C=C[C]=CC1
6: O=O>>[CH]C=C(O)C=[CH]
7: [C]NC(C[NH])C(O)C(O)C>>[C]NC(C[N])C(O)C(O)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C(O)C(N)Cc1ccccc1>>O=C(O)C(N)Cc1ccc(O)cc1]
2: R:M00002, P:M00005	[O=c1nc(N)[nH]c2NCC(Nc12)C(O)C(O)C>>O=C1N=C(N)NC2=NCC(NC12O)C(O)C(O)C, O=c1nc(N)[nH]c2NCC(Nc12)C(O)C(O)C>>O=C1N=C(N)NC2=NCC(NC12O)C(O)C(O)C, O=c1nc(N)[nH]c2NCC(Nc12)C(O)C(O)C>>O=C1N=C(N)NC2=NCC(NC12O)C(O)C(O)C, O=c1nc(N)[nH]c2NCC(Nc12)C(O)C(O)C>>O=C1N=C(N)NC2=NCC(NC12O)C(O)C(O)C]
3: R:M00003, P:M00004	[O=O>>O=C(O)C(N)Cc1ccc(O)cc1]
4: R:M00003, P:M00005	[O=O>>O=C1N=C(N)NC2=NCC(NC12O)C(O)C(O)C]


//
SELECTED AAM MAPPING
[O:30]=[O:31].[O:18]=[C:19]([OH:20])[CH:21]([NH2:22])[CH2:23][C:24]:1:[CH:25]:[CH:26]:[CH:27]:[CH:28]:[CH:29]1.[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]=2[NH:8][CH2:9][CH:10]([NH:11][C:12]12)[CH:13]([OH:14])[CH:15]([OH:16])[CH3:17]>>[O:18]=[C:19]([OH:20])[CH:21]([NH2:22])[CH2:23][C:24]:1:[CH:25]:[CH:26]:[C:27]([OH:31]):[CH:28]:[CH:29]1.[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]2=[N:8][CH2:9][CH:10]([NH:11][C:12]12[OH:30])[CH:13]([OH:14])[CH:15]([OH:16])[CH3:17]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=27, 2=26, 3=25, 4=24, 5=29, 6=28, 7=23, 8=21, 9=19, 10=18, 11=20, 12=22, 13=17, 14=15, 15=13, 16=10, 17=9, 18=8, 19=7, 20=12, 21=11, 22=2, 23=1, 24=3, 25=4, 26=6, 27=5, 28=14, 29=16, 30=30, 31=31}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=26, 2=27, 3=28, 4=30, 5=31, 6=25, 7=24, 8=22, 9=20, 10=19, 11=21, 12=23, 13=29, 14=18, 15=16, 16=14, 17=10, 18=9, 19=8, 20=7, 21=12, 22=11, 23=2, 24=1, 25=3, 26=4, 27=6, 28=5, 29=13, 30=15, 31=17}

