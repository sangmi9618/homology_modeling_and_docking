
//
FINGERPRINTS BC

//
FINGERPRINTS RC
[O=C(O)C[CH2]>>[O-]C(=O)C[CH2]:1.0, O=C(O)[CH2]:1.0, O=C(O)[CH2]>>[O-]C(=O)[CH2]:1.0, [CH]=C1[C]=[C]C(=[CH])N1:1.0, [CH]=C1[C]=[C]C(=[CH])N1>>[CH]C=1[C]=[C]C(=[CH])N1:1.0, [CH]=C1[C]=[C]C(=[CH])[N-]1:1.0, [CH]C=1[C]=[C]C(=[CH])N1:2.0, [CH]C=1[C]=[C]C(=[CH])N1>>[CH]=C1[C]=[C]C(=[CH])[N-]1:1.0, [C]C=C1N=C(C=[C])C(=C1[CH2])C>>[C]C=c1[n-]c(=C[C])c(c1[CH2])C:1.0, [C]C=c1[nH]c(=C[C])c(c1[CH])C>>[C]C=C1N=C(C=[C])C(=C1[CH])C:1.0, [C]N=[C]:2.0, [C]N=[C]>>[C][N-][C]:1.0, [C]N[C]:1.0, [C]N[C]>>[C]N=[C]:1.0, [C]O:1.0, [C]O>>[C][O-]:1.0, [C][N-][C]:1.0, [C][O-]:1.0, [N-]:1.0, [NH]:1.0, [N]:2.0, [O-]:1.0, [O-]C(=O)[CH2]:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]N[C]>>[C]N=[C]
2: [C]N=[C]>>[C][N-][C]
3: [C]O>>[C][O-]

MMP Level 2
1: [CH]=C1[C]=[C]C(=[CH])N1>>[CH]C=1[C]=[C]C(=[CH])N1
2: [CH]C=1[C]=[C]C(=[CH])N1>>[CH]=C1[C]=[C]C(=[CH])[N-]1
3: O=C(O)[CH2]>>[O-]C(=O)[CH2]

MMP Level 3
1: [C]C=c1[nH]c(=C[C])c(c1[CH])C>>[C]C=C1N=C(C=[C])C(=C1[CH])C
2: [C]C=C1N=C(C=[C])C(=C1[CH2])C>>[C]C=c1[n-]c(=C[C])c(c1[CH2])C
3: O=C(O)C[CH2]>>[O-]C(=O)C[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)CCC=1c2nc(cc3[nH]c(cc4[nH]c(cc5nc(c2)C(=C5C)CCC(=O)O)c(c4C=C)C)c(c3C=C)C)C1C>>[O-]C(=O)CCc1c2[n-]c(cc3nc(cc4[nH]c(cc5nc(c2)C(=C5C)CCC(=O)O)c(C=C)c4C)C(C=C)=C3C)c1C, O=C(O)CCC=1c2nc(cc3[nH]c(cc4[nH]c(cc5nc(c2)C(=C5C)CCC(=O)O)c(c4C=C)C)c(c3C=C)C)C1C>>[O-]C(=O)CCc1c2[n-]c(cc3nc(cc4[nH]c(cc5nc(c2)C(=C5C)CCC(=O)O)c(C=C)c4C)C(C=C)=C3C)c1C, O=C(O)CCC=1c2nc(cc3[nH]c(cc4[nH]c(cc5nc(c2)C(=C5C)CCC(=O)O)c(c4C=C)C)c(c3C=C)C)C1C>>[O-]C(=O)CCc1c2[n-]c(cc3nc(cc4[nH]c(cc5nc(c2)C(=C5C)CCC(=O)O)c(C=C)c4C)C(C=C)=C3C)c1C]


//
SELECTED AAM MAPPING
[Fe+2:43].[O:1]=[C:2]([OH:3])[CH2:4][CH2:5][C:6]=1[C:7]:2:[N:8]:[C:9](:[CH:10]:[C:11]:3:[NH:12]:[C:13](:[CH:14]:[C:15]:4:[NH:16]:[C:17](:[CH:18]:[C:19]5:[N:20]:[C:21](:[CH:22]2)[C:23](=[C:24]5[CH3:25])[CH2:26][CH2:27][C:28](=[O:29])[OH:30]):[C:31](:[C:32]4[CH:33]=[CH2:34])[CH3:35]):[C:36](:[C:37]3[CH:38]=[CH2:39])[CH3:40])[C:41]1[CH3:42]>>[H+:44].[Fe+2:43].[O:29]=[C:28]([O-:30])[CH2:27][CH2:26][C:23]:1:[C:21]:2:[N-:20]:[C:19](:[CH:18]:[C:17]3:[N:16]:[C:15](:[CH:14]:[C:13]:4:[NH:12]:[C:11](:[CH:10]:[C:9]5:[N:8]:[C:7](:[CH:22]2)[C:6](=[C:41]5[CH3:42])[CH2:5][CH2:4][C:2](=[O:1])[OH:3]):[C:37]([CH:38]=[CH2:39]):[C:36]4[CH3:40])[C:32]([CH:33]=[CH2:34])=[C:31]3[CH3:35]):[C:24]1[CH3:25]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=35, 2=31, 3=32, 4=15, 5=14, 6=13, 7=36, 8=37, 9=11, 10=12, 11=10, 12=9, 13=41, 14=6, 15=7, 16=8, 17=22, 18=21, 19=23, 20=24, 21=19, 22=20, 23=18, 24=17, 25=16, 26=25, 27=26, 28=27, 29=28, 30=29, 31=30, 32=5, 33=4, 34=2, 35=1, 36=3, 37=42, 38=38, 39=39, 40=40, 41=33, 42=34, 43=43}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=42, 2=41, 3=6, 4=7, 5=22, 6=21, 7=20, 8=19, 9=18, 10=17, 11=31, 12=34, 13=15, 14=16, 15=14, 16=13, 17=36, 18=39, 19=11, 20=12, 21=10, 22=9, 23=8, 24=40, 25=37, 26=38, 27=35, 28=32, 29=33, 30=24, 31=23, 32=26, 33=27, 34=28, 35=29, 36=30, 37=25, 38=5, 39=4, 40=2, 41=1, 42=3, 43=43, 44=44}

