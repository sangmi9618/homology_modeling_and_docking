
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:2.0, C-O:2.0, O-P:3.0]

//
FINGERPRINTS RC
[O:3.0, O=C(N)CC[CH]>>O=C(O)CC[CH]:1.0, O=C(N)C[CH2]:1.0, O=C(N)C[CH2]>>O=C(O)C[CH2]:1.0, O=C(N)C[CH2]>>O=C(O[P])N:1.0, O=C(O)C[CH2]:1.0, O=C(O)[CH2]:2.0, O=C(OP(=O)(O)O)N:1.0, O=C(O[P])N:1.0, O=C([CH2])N:2.0, O=C([CH2])N>>O=C(O)[CH2]:1.0, O=C([CH2])N>>[O]C(=O)N:1.0, O=P(O)(O)O:3.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O:1.0, O>>O=P(O)(O)O:2.0, O>>[P]O:1.0, [C]:4.0, [C]N:2.0, [C]N>>[C]N:1.0, [C]O:2.0, [C]O>>[C]O[P]:1.0, [C]O[P]:1.0, [C][O-]:1.0, [C][O-]>>[C]O:1.0, [NH2]:2.0, [O-]:1.0, [O-]C(=O)O:4.0, [O-]C(=O)O>>O=C(O)C[CH2]:1.0, [O-]C(=O)O>>O=C(O)[CH2]:1.0, [O-]C(=O)O>>O=C(OP(=O)(O)O)N:3.0, [O-]C(=O)O>>O=C(O[P])N:1.0, [O-]C(=O)O>>[O]C(=O)N:1.0, [OH]:4.0, [O]:2.0, [O]C(=O)N:2.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (7)
[O:1.0, [C]:4.0, [NH2]:2.0, [O-]:1.0, [OH]:2.0, [O]:2.0, [P]:3.0]


ID=Reaction Center at Level: 1 (13)
[O:1.0, O=C(O)[CH2]:1.0, O=C([CH2])N:1.0, O=P(O)(O)O:1.0, [C]N:2.0, [C]O:1.0, [C]O[P]:1.0, [C][O-]:1.0, [O-]C(=O)O:1.0, [O]C(=O)N:1.0, [O]P(=O)(O)O:2.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (13)
[O:1.0, O=C(N)C[CH2]:1.0, O=C(O)C[CH2]:1.0, O=C(O)[CH2]:1.0, O=C(OP(=O)(O)O)N:1.0, O=C(O[P])N:1.0, O=C([CH2])N:1.0, O=P(O)(O)O:2.0, O=P(O)(O)O[P]:1.0, [C]OP(=O)(O)O:1.0, [O-]C(=O)O:2.0, [O]C(=O)N:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]P(=O)(O)O>>O=P(O)(O)O
2: O=C([CH2])N>>O=C(O)[CH2]
3: [C]N>>[C]N
4: [O-]C(=O)O>>[O]C(=O)N
5: [C]O>>[C]O[P]
6: [P]O[P]>>[P]O
7: O>>[P]O
8: [C][O-]>>[C]O

MMP Level 2
1: O=P(O)(O)O[P]>>O=P(O)(O)O
2: O=C(N)C[CH2]>>O=C(O)C[CH2]
3: O=C([CH2])N>>[O]C(=O)N
4: [O-]C(=O)O>>O=C(O[P])N
5: [O-]C(=O)O>>O=C(OP(=O)(O)O)N
6: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
7: O>>O=P(O)(O)O
8: [O-]C(=O)O>>O=C(O)[CH2]

MMP Level 3
1: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O
2: O=C(N)CC[CH]>>O=C(O)CC[CH]
3: O=C(N)C[CH2]>>O=C(O[P])N
4: [O-]C(=O)O>>O=C(OP(=O)(O)O)N
5: [O-]C(=O)O>>O=C(OP(=O)(O)O)N
6: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
7: O>>O=P(O)(O)O
8: [O-]C(=O)O>>O=C(O)C[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00005	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00006	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)O]
3: R:M00002, P:M00007	[O=C(O)C(N)CCC(=O)N>>O=C(O)CCC(N)C(=O)O]
4: R:M00002, P:M00008	[O=C(O)C(N)CCC(=O)N>>O=C(OP(=O)(O)O)N]
5: R:M00003, P:M00007	[[O-]C(=O)O>>O=C(O)CCC(N)C(=O)O]
6: R:M00003, P:M00008	[[O-]C(=O)O>>O=C(OP(=O)(O)O)N, [O-]C(=O)O>>O=C(OP(=O)(O)O)N]
7: R:M00004, P:M00006	[O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[O:42]=[C:43]([O-:44])[OH:45].[O:32]=[C:33]([OH:34])[CH:35]([NH2:36])[CH2:37][CH2:38][C:39](=[O:40])[NH2:41].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31].[OH2:46]>>[O:40]=[C:39]([OH:44])[CH2:38][CH2:37][CH:35]([NH2:36])[C:33](=[O:32])[OH:34].[O:42]=[C:43]([O:45][P:47](=[O:48])([OH:49])[OH:50])[NH2:41].[O:1]=[P:2]([OH:3])([OH:4])[OH:46].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=37, 33=38, 34=39, 35=40, 36=41, 37=35, 38=33, 39=32, 40=34, 41=36, 42=43, 43=42, 44=45, 45=44, 46=46}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=17, 5=18, 6=19, 7=23, 8=16, 9=15, 10=14, 11=13, 12=24, 13=26, 14=11, 15=12, 16=10, 17=9, 18=6, 19=7, 20=8, 21=5, 22=2, 23=1, 24=3, 25=4, 26=27, 27=25, 28=48, 29=47, 30=46, 31=49, 32=50, 33=32, 34=31, 35=29, 36=28, 37=30, 38=33, 39=35, 40=36, 41=37, 42=34, 43=39, 44=38, 45=45, 46=40, 47=41, 48=42, 49=43, 50=44}

