
//
FINGERPRINTS BC

FORMED_CLEAVED
[N-P:1.0, O-P:1.0]

//
FINGERPRINTS RC
[O=P(O)(O)NC(=N[CH2])N>>N([CH2])=C(N)N:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)OP(=O)(O)O[P]:1.0, O=P([NH])(O)O:1.0, O=P([NH])(O)O>>[O]P(=O)(O)O:1.0, [C]N:1.0, [C]NP(=O)(O)O:1.0, [C]NP(=O)(O)O>>O=P(O)(O)O[P]:1.0, [C]N[P]:1.0, [C]N[P]>>[C]N:1.0, [NH2]:1.0, [NH]:1.0, [N]=C(N)N:1.0, [N]=C(N)NP(=O)(O)O:1.0, [N]=C(N)NP(=O)(O)O>>[N]=C(N)N:1.0, [N]=C(N)NP(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O:1.0, [OH]:1.0, [O]:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O>>[P]O[P]:1.0, [P]O[P]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[NH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (4)
[O=P([NH])(O)O:1.0, [C]N[P]:1.0, [O]P(=O)(O)O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)O[P]:1.0, [C]NP(=O)(O)O:1.0, [N]=C(N)NP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O>>[P]O[P]
2: [C]N[P]>>[C]N
3: O=P([NH])(O)O>>[O]P(=O)(O)O

MMP Level 2
1: [O]P(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O
2: [N]=C(N)NP(=O)(O)O>>[N]=C(N)N
3: [C]NP(=O)(O)O>>O=P(O)(O)O[P]

MMP Level 3
1: O=P(O)(O)O[P]>>O=P(O)(O)OP(=O)(O)O[P]
2: O=P(O)(O)NC(=N[CH2])N>>N([CH2])=C(N)N
3: [N]=C(N)NP(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00002, P:M00003	[O=C(O)C(N)CCCN=C(N)NP(=O)(O)O>>O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
3: R:M00002, P:M00004	[O=C(O)C(N)CCCN=C(N)NP(=O)(O)O>>O=C(O)C(N)CCCN=C(N)N]


//
SELECTED AAM MAPPING
[O:28]=[C:29]([OH:30])[CH:31]([NH2:32])[CH2:33][CH2:34][CH2:35][N:36]=[C:37]([NH2:38])[NH:39][P:40](=[O:41])([OH:42])[OH:43].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][CH2:10][CH:11]1[O:12][CH:13]([N:14]:2:[CH:15]:[N:16]:[C:17]:3:[C:18](:[N:19]:[CH:20]:[N:21]:[C:22]32)[NH2:23])[CH:24]([OH:25])[CH:26]1[OH:27]>>[O:28]=[C:29]([OH:30])[CH:31]([NH2:32])[CH2:33][CH2:34][CH2:35][N:36]=[C:37]([NH2:38])[NH2:39].[O:41]=[P:40]([OH:42])([OH:43])[O:3][P:2](=[O:1])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][CH2:10][CH:11]1[O:12][CH:13]([N:14]:2:[CH:15]:[N:16]:[C:17]:3:[C:18](:[N:19]:[CH:20]:[N:21]:[C:22]32)[NH2:23])[CH:24]([OH:25])[CH:26]1[OH:27]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=17, 5=18, 6=19, 7=23, 8=16, 9=15, 10=14, 11=13, 12=24, 13=26, 14=11, 15=12, 16=10, 17=9, 18=6, 19=7, 20=8, 21=5, 22=2, 23=1, 24=3, 25=4, 26=27, 27=25, 28=34, 29=33, 30=31, 31=29, 32=28, 33=30, 34=32, 35=35, 36=36, 37=37, 38=38, 39=39, 40=40, 41=41, 42=42, 43=43}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=38, 33=37, 34=35, 35=33, 36=32, 37=34, 38=36, 39=39, 40=40, 41=41, 42=42, 43=43}

