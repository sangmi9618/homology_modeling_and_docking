
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-S:2.0, O=O:1.0, O=S:1.0, S-S:1.0]

//
FINGERPRINTS RC
[O=O:4.0, O=O>>O=S:1.0, O=O>>[O-]S([O-])=O:4.0, O=O>>[O-][S]:1.0, O=S:1.0, SS[CH2]:2.0, SS[CH2]>>S[CH2]:1.0, SS[CH2]>>[O-]S([O-])=O:1.0, S[CH2]:1.0, [CH]CS:1.0, [CH]CSS:1.0, [CH]CSS>>[CH]CS:1.0, [CH]CSS>>[O-]S([O-])=O:1.0, [C]C([NH])CSS>>[C]C([NH])CS:1.0, [O-]:1.0, [O-]S([O-])=O:4.0, [O-][S]:1.0, [O]:3.0, [SH]:2.0, [S]:2.0, [S]S:1.0, [S]S>>[O-]S([O-])=O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[O-]:2.0, [O]:3.0, [SH]:1.0, [S]:4.0]


ID=Reaction Center at Level: 1 (6)
[O=O:2.0, O=S:1.0, SS[CH2]:1.0, [O-]S([O-])=O:3.0, [O-][S]:2.0, [S]S:1.0]


ID=Reaction Center at Level: 2 (4)
[O=O:2.0, SS[CH2]:1.0, [CH]CSS:1.0, [O-]S([O-])=O:6.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [S]S>>[O-]S([O-])=O
2: SS[CH2]>>S[CH2]
3: O=O>>[O-][S]
4: O=O>>O=S

MMP Level 2
1: SS[CH2]>>[O-]S([O-])=O
2: [CH]CSS>>[CH]CS
3: O=O>>[O-]S([O-])=O
4: O=O>>[O-]S([O-])=O

MMP Level 3
1: [CH]CSS>>[O-]S([O-])=O
2: [C]C([NH])CSS>>[C]C([NH])CS
3: O=O>>[O-]S([O-])=O
4: O=O>>[O-]S([O-])=O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)CNC(=O)C(NC(=O)CCC(N)C(=O)O)CSS>>[O-]S([O-])=O]
2: R:M00001, P:M00004	[O=C(O)CNC(=O)C(NC(=O)CCC(N)C(=O)O)CSS>>O=C(O)CNC(=O)C(NC(=O)CCC(N)C(=O)O)CS]
3: R:M00002, P:M00003	[O=O>>[O-]S([O-])=O, O=O>>[O-]S([O-])=O]


//
SELECTED AAM MAPPING
[O:22]=[O:23].[O:1]=[C:2]([OH:3])[CH2:4][NH:5][C:6](=[O:7])[CH:8]([NH:9][C:10](=[O:11])[CH2:12][CH2:13][CH:14]([NH2:15])[C:16](=[O:17])[OH:18])[CH2:19][S:20][SH:21]>>[H+:25].[O:1]=[C:2]([OH:3])[CH2:4][NH:5][C:6](=[O:7])[CH:8]([NH:9][C:10](=[O:11])[CH2:12][CH2:13][CH:14]([NH2:15])[C:16](=[O:17])[OH:18])[CH2:19][SH:20].[O:22]=[S:21]([O-:23])[O-:24]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=13, 2=12, 3=10, 4=11, 5=9, 6=8, 7=19, 8=20, 9=21, 10=6, 11=7, 12=5, 13=4, 14=2, 15=1, 16=3, 17=14, 18=16, 19=17, 20=18, 21=15, 22=22, 23=23}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=23, 2=22, 3=21, 4=24, 5=13, 6=12, 7=10, 8=11, 9=9, 10=8, 11=19, 12=20, 13=6, 14=7, 15=5, 16=4, 17=2, 18=1, 19=3, 20=14, 21=16, 22=17, 23=18, 24=15, 25=25}

