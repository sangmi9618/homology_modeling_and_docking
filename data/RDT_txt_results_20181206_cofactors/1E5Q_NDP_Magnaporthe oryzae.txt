
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C=O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(N)C=1C=[N+]C=CC1>>O=C(N)C1=C[N]C=CC1:1.0, O=C(O)C(NCC[CH2])C[CH2]>>O=C(O)C(N)C[CH2]:1.0, O=CC[CH2]:1.0, O=C[CH2]:2.0, O=[CH]:1.0, O>>O=CC[CH2]:1.0, O>>O=C[CH2]:1.0, O>>O=[CH]:1.0, [CH2]:2.0, [CH]:2.0, [CH]N:1.0, [CH]N([CH])[CH]:1.0, [CH]NCC[CH2]:1.0, [CH]NCC[CH2]>>O=CC[CH2]:1.0, [CH]N[CH2]:1.0, [CH]N[CH2]>>[CH]N:1.0, [CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]>>[CH]N([CH])[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN(C=[CH])C([O])[CH]:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])C=C[CH]>>[C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C([CH2])N:1.0, [C]C([CH2])NCCC[CH2]>>O=CCC[CH2]:1.0, [C]C([CH2])NC[CH2]:1.0, [C]C([CH2])NC[CH2]>>[C]C([CH2])N:1.0, [C]C=[CH]:1.0, [C]C=[CH]>>[C]C[CH]:1.0, [C]C[CH]:1.0, [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]C1=CN(C=CC1)C2O[CH][CH]C2O:1.0, [N+]:1.0, [NH2]:1.0, [NH]:1.0, [NH]C[CH2]:1.0, [NH]C[CH2]>>O=C[CH2]:1.0, [N]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[O:1.0, [CH2]:1.0, [CH]:1.0, [NH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=C[CH2]:1.0, O=[CH]:1.0, [CH]N[CH2]:1.0, [NH]C[CH2]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=CC[CH2]:1.0, O=C[CH2]:1.0, [CH]NCC[CH2]:1.0, [C]C([CH2])NC[CH2]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C=[CH]>>[C]C[CH]
2: O>>O=[CH]
3: [CH]N[CH2]>>[CH]N
4: [CH][N+]([CH])=[CH]>>[CH]N([CH])[CH]
5: [NH]C[CH2]>>O=C[CH2]

MMP Level 2
1: [C]C(=[CH])C=C[CH]>>[C]C(=[CH])CC=[CH]
2: O>>O=C[CH2]
3: [C]C([CH2])NC[CH2]>>[C]C([CH2])N
4: [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN(C=[CH])C([O])[CH]
5: [CH]NCC[CH2]>>O=CC[CH2]

MMP Level 3
1: O=C(N)C=1C=[N+]C=CC1>>O=C(N)C1=C[N]C=CC1
2: O>>O=CC[CH2]
3: O=C(O)C(NCC[CH2])C[CH2]>>O=C(O)C(N)C[CH2]
4: [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]C1=CN(C=CC1)C2O[CH][CH]C2O
5: [C]C([CH2])NCCC[CH2]>>O=CCC[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00007	[O>>O=CCCCC(N)C(=O)O]
2: R:M00002, P:M00006	[O=C(O)CCC(NCCCCC(N)C(=O)O)C(=O)O>>O=C(O)CCC(N)C(=O)O]
3: R:M00002, P:M00007	[O=C(O)CCC(NCCCCC(N)C(=O)O)C(=O)O>>O=CCCCC(N)C(=O)O]
4: R:M00003, P:M00004	[O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O, O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O]


//
SELECTED AAM MAPPING
[O:49]=[C:50]([OH:51])[CH2:52][CH2:53][CH:54]([NH:55][CH2:56][CH2:57][CH2:58][CH2:59][CH:60]([NH2:61])[C:62](=[O:63])[OH:64])[C:65](=[O:66])[OH:67].[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:5]:[CH:6]:[CH:7]:[N+:8](:[CH:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48].[OH2:68]>>[H+:69].[O:68]=[CH:56][CH2:57][CH2:58][CH2:59][CH:60]([NH2:61])[C:62](=[O:63])[OH:64].[O:49]=[C:50]([OH:51])[CH2:52][CH2:53][CH:54]([NH2:55])[C:65](=[O:66])[OH:67].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:9][N:8]([CH:7]=[CH:6][CH2:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=68, 2=58, 3=57, 4=56, 5=55, 6=54, 7=53, 8=52, 9=50, 10=49, 11=51, 12=65, 13=66, 14=67, 15=59, 16=60, 17=62, 18=63, 19=64, 20=61, 21=6, 22=5, 23=4, 24=9, 25=8, 26=7, 27=10, 28=47, 29=45, 30=12, 31=11, 32=13, 33=14, 34=15, 35=16, 36=17, 37=18, 38=19, 39=20, 40=21, 41=22, 42=23, 43=24, 44=43, 45=37, 46=26, 47=25, 48=27, 49=28, 50=29, 51=30, 52=35, 53=34, 54=33, 55=32, 56=31, 57=36, 58=38, 59=39, 60=40, 61=41, 62=42, 63=44, 64=46, 65=48, 66=2, 67=1, 68=3}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=5, 6=4, 7=2, 8=1, 9=3, 10=10, 11=47, 12=45, 13=12, 14=11, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=43, 28=37, 29=26, 30=25, 31=27, 32=28, 33=29, 34=30, 35=35, 36=34, 37=33, 38=32, 39=31, 40=36, 41=38, 42=39, 43=40, 44=41, 45=42, 46=44, 47=46, 48=48, 49=69, 50=53, 51=52, 52=50, 53=49, 54=51, 55=54, 56=56, 57=57, 58=58, 59=55, 60=62, 61=61, 62=60, 63=59, 64=63, 65=64, 66=66, 67=67, 68=68, 69=65}

