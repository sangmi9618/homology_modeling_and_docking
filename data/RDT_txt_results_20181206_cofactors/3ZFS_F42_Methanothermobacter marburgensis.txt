
//
FINGERPRINTS BC

ORDER_CHANGED
[C%C*C%C:1.0, C-O*C=O:1.0]

//
FINGERPRINTS RC
[O=C([CH])[CH]:2.0, O=C([CH])[CH]>>[CH]C(=[CH])O:2.0, O=C([NH])C=1C=C(C=[CH])C(=[CH])[N]C1[NH]>>O=C([NH])C1=C([NH])[N]C([CH])=C(C=[CH])C1:1.0, [CH2]:1.0, [CH]:1.0, [CH]C(=[CH])O:2.0, [C]:4.0, [C]=C([CH])[CH2]:1.0, [C]=CC(=O)C=[CH]:1.0, [C]=CC(=O)C=[CH]>>[C]C=C(O)C=[CH]:2.0, [C]=O:1.0, [C]=O>>[C]O:1.0, [C]C(=[C])C=C([C])[CH]:1.0, [C]C(=[C])C=C([C])[CH]>>[C]C(=[C])CC(=[C])[CH]:1.0, [C]C(=[C])CC(=[C])[CH]:1.0, [C]C([CH])=[CH]:1.0, [C]C([CH])=[CH]>>[C]=C([CH])[CH2]:1.0, [C]C1=[C]N([CH2])C2=C[C]C=CC2=C1>>[C]C1=[C]N([CH2])C=2C=[C]C=CC2C1:1.0, [C]C=C(C=[CH])C([N])=[CH]:1.0, [C]C=C(C=[CH])C([N])=[CH]>>[C]CC(C=[CH])=C([N])[CH]:1.0, [C]C=C(O)C=[CH]:1.0, [C]C=[C]:1.0, [C]C=[C]>>[C]C[C]:1.0, [C]CC(C=[CH])=C([N])[CH]:1.0, [C]C[C]:1.0, [C]O:1.0, [N]C=1[C]C=CC(=O)C1>>[N]C1=[C]C=CC(O)=C1:1.0, [OH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (5)
[[CH2]:1.0, [CH]:1.0, [C]:4.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (8)
[O=C([CH])[CH]:1.0, [CH]C(=[CH])O:1.0, [C]=C([CH])[CH2]:1.0, [C]=O:1.0, [C]C([CH])=[CH]:1.0, [C]C=[C]:1.0, [C]C[C]:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (8)
[O=C([CH])[CH]:1.0, [CH]C(=[CH])O:1.0, [C]=CC(=O)C=[CH]:1.0, [C]C(=[C])C=C([C])[CH]:1.0, [C]C(=[C])CC(=[C])[CH]:1.0, [C]C=C(C=[CH])C([N])=[CH]:1.0, [C]C=C(O)C=[CH]:1.0, [C]CC(C=[CH])=C([N])[CH]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C=[C]>>[C]C[C]
2: [C]=O>>[C]O
3: [C]C([CH])=[CH]>>[C]=C([CH])[CH2]
4: O=C([CH])[CH]>>[CH]C(=[CH])O

MMP Level 2
1: [C]C(=[C])C=C([C])[CH]>>[C]C(=[C])CC(=[C])[CH]
2: O=C([CH])[CH]>>[CH]C(=[CH])O
3: [C]C=C(C=[CH])C([N])=[CH]>>[C]CC(C=[CH])=C([N])[CH]
4: [C]=CC(=O)C=[CH]>>[C]C=C(O)C=[CH]

MMP Level 3
1: O=C([NH])C=1C=C(C=[CH])C(=[CH])[N]C1[NH]>>O=C([NH])C1=C([NH])[N]C([CH])=C(C=[CH])C1
2: [C]=CC(=O)C=[CH]>>[C]C=C(O)C=[CH]
3: [C]C1=[C]N([CH2])C2=C[C]C=CC2=C1>>[C]C1=[C]N([CH2])C=2C=[C]C=CC2C1
4: [N]C=1[C]C=CC(=O)C1>>[N]C1=[C]C=CC(O)=C1


//
REACTION MMP (RPAIR)
1: R:M00002, P:M00003	[O=c1ccc-2cc3c(=O)[nH]c(=O)[nH]c3n(c2c1)CC(O)C(O)C(O)COP(=O)(O)OC(C(=O)NC(C(=O)O)CCC(=O)NC(C(=O)O)CCC(=O)O)C>>O=c1[nH]c(=O)c2c([nH]1)N(c3cc(O)ccc3C2)CC(O)C(O)C(O)COP(=O)(O)OC(C(=O)NC(C(=O)O)CCC(=O)NC(C(=O)O)CCC(=O)O)C, O=c1ccc-2cc3c(=O)[nH]c(=O)[nH]c3n(c2c1)CC(O)C(O)C(O)COP(=O)(O)OC(C(=O)NC(C(=O)O)CCC(=O)NC(C(=O)O)CCC(=O)O)C>>O=c1[nH]c(=O)c2c([nH]1)N(c3cc(O)ccc3C2)CC(O)C(O)C(O)COP(=O)(O)OC(C(=O)NC(C(=O)O)CCC(=O)NC(C(=O)O)CCC(=O)O)C, O=c1ccc-2cc3c(=O)[nH]c(=O)[nH]c3n(c2c1)CC(O)C(O)C(O)COP(=O)(O)OC(C(=O)NC(C(=O)O)CCC(=O)NC(C(=O)O)CCC(=O)O)C>>O=c1[nH]c(=O)c2c([nH]1)N(c3cc(O)ccc3C2)CC(O)C(O)C(O)COP(=O)(O)OC(C(=O)NC(C(=O)O)CCC(=O)NC(C(=O)O)CCC(=O)O)C, O=c1ccc-2cc3c(=O)[nH]c(=O)[nH]c3n(c2c1)CC(O)C(O)C(O)COP(=O)(O)OC(C(=O)NC(C(=O)O)CCC(=O)NC(C(=O)O)CCC(=O)O)C>>O=c1[nH]c(=O)c2c([nH]1)N(c3cc(O)ccc3C2)CC(O)C(O)C(O)COP(=O)(O)OC(C(=O)NC(C(=O)O)CCC(=O)NC(C(=O)O)CCC(=O)O)C]


//
SELECTED AAM MAPPING
[HH:54].[O:1]=[C:2]1[CH:3]=[CH:4][C:5]2=[CH:6][C:7]=3[C:8](=[O:9])[NH:10][C:11](=[O:12])[NH:13][C:14]3[N:15]([C:16]2=[CH:17]1)[CH2:18][CH:19]([OH:20])[CH:21]([OH:22])[CH:23]([OH:24])[CH2:25][O:26][P:27](=[O:28])([OH:29])[O:30][CH:31]([C:32](=[O:33])[NH:34][CH:35]([C:36](=[O:37])[OH:38])[CH2:39][CH2:40][C:41](=[O:42])[NH:43][CH:44]([C:45](=[O:46])[OH:47])[CH2:48][CH2:49][C:50](=[O:51])[OH:52])[CH3:53]>>[O:51]=[C:50]([OH:52])[CH2:49][CH2:48][CH:44]([NH:43][C:41](=[O:42])[CH2:40][CH2:39][CH:35]([NH:34][C:32](=[O:33])[CH:31]([O:30][P:27](=[O:28])([OH:29])[O:26][CH2:25][CH:23]([OH:24])[CH:21]([OH:22])[CH:19]([OH:20])[CH2:18][N:15]1[C:16]:2:[CH:17]:[C:2]([OH:1]):[CH:3]:[CH:4]:[C:5]2[CH2:6][C:7]=3[C:8](=[O:9])[NH:10][C:11](=[O:12])[NH:13][C:14]31)[CH3:53])[C:36](=[O:37])[OH:38])[C:45](=[O:46])[OH:47]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=54, 2=53, 3=31, 4=32, 5=33, 6=34, 7=35, 8=39, 9=40, 10=41, 11=42, 12=43, 13=44, 14=48, 15=49, 16=50, 17=51, 18=52, 19=45, 20=46, 21=47, 22=36, 23=37, 24=38, 25=30, 26=27, 27=28, 28=29, 29=26, 30=25, 31=23, 32=21, 33=19, 34=18, 35=15, 36=16, 37=17, 38=2, 39=1, 40=3, 41=4, 42=5, 43=6, 44=7, 45=14, 46=13, 47=11, 48=12, 49=10, 50=8, 51=9, 52=20, 53=22, 54=24}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=47, 2=16, 3=14, 4=15, 5=13, 6=12, 7=11, 8=10, 9=8, 10=9, 11=7, 12=6, 13=5, 14=4, 15=2, 16=1, 17=3, 18=51, 19=52, 20=53, 21=48, 22=49, 23=50, 24=17, 25=18, 26=19, 27=20, 28=21, 29=22, 30=23, 31=25, 32=27, 33=29, 34=30, 35=31, 36=37, 37=38, 38=39, 39=46, 40=45, 41=43, 42=44, 43=42, 44=40, 45=41, 46=36, 47=35, 48=33, 49=32, 50=34, 51=28, 52=26, 53=24}

