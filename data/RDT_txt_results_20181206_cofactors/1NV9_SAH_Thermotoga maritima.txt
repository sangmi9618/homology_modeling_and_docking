
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-S:1.0]

//
FINGERPRINTS RC
[S([CH2])[CH2]:1.0, [CH2][S+]([CH2])C:2.0, [CH2][S+]([CH2])C>>S([CH2])[CH2]:1.0, [CH3]:1.0, [CH]CSC[CH2]:1.0, [CH]C[S+](C)C[CH2]:1.0, [CH]C[S+](C)C[CH2]>>[CH]CSC[CH2]:1.0, [O]C([CH])C[S+](C)CC[CH]>>[O]C([CH])CSCC[CH]:1.0, [S+]:1.0, [S+]C:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH3]:1.0, [S+]:1.0]


ID=Reaction Center at Level: 1 (2)
[[CH2][S+]([CH2])C:1.0, [S+]C:1.0]


ID=Reaction Center at Level: 2 (2)
[[CH2][S+]([CH2])C:1.0, [CH]C[S+](C)C[CH2]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH2][S+]([CH2])C>>S([CH2])[CH2]

MMP Level 2
1: [CH]C[S+](C)C[CH2]>>[CH]CSC[CH2]

MMP Level 3
1: [O]C([CH])C[S+](C)CC[CH]>>[O]C([CH])CSCC[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(O)C(N)CCSCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][S+:8]([CH3:27])[CH2:9][CH:10]1[O:11][CH:12]([N:13]:2:[CH:14]:[N:15]:[C:16]:3:[C:17](:[N:18]:[CH:19]:[N:20]:[C:21]32)[NH2:22])[CH:23]([OH:24])[CH:25]1[OH:26]>>[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][S:8][CH2:9][CH:10]1[O:11][CH:12]([N:13]:2:[CH:14]:[N:15]:[C:16]:3:[C:17](:[N:18]:[CH:19]:[N:20]:[C:21]32)[NH2:22])[CH:23]([OH:24])[CH:25]1[OH:26]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=4, 6=2, 7=1, 8=3, 9=5, 10=10, 11=11, 12=26, 13=24, 14=13, 15=12, 16=14, 17=15, 18=16, 19=17, 20=22, 21=21, 22=20, 23=19, 24=18, 25=23, 26=25, 27=27}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=19, 2=20, 3=21, 4=16, 5=17, 6=18, 7=22, 8=15, 9=14, 10=13, 11=12, 12=23, 13=25, 14=10, 15=11, 16=9, 17=8, 18=7, 19=6, 20=4, 21=2, 22=1, 23=3, 24=5, 25=26, 26=24}

