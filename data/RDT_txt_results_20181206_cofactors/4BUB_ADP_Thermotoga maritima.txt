
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0, O-P:2.0]

STEREO_CHANGED
[C(R/S):3.0]

//
FINGERPRINTS RC
[O=C(N[CH])C([NH])[CH2]:1.0, O=C(O)C(N)CC[CH2]>>O=C(O)C(N)CC[CH2]:1.0, O=C(O)C(N)CC[CH2]>>O=C([CH])NC(C(=O)O)CC[CH2]:1.0, O=C(O)C(N)C[CH2]:3.0, O=C(O)C(N)C[CH2]>>O=C(O)C(N)C[CH2]:1.0, O=C(O)C(N)C[CH2]>>O=C(O)C(NC(=O)C([NH])[CH2])C[CH2]:1.0, O=C(O)C(N)C[CH2]>>[C]NC(C(=O)O)C[CH2]:1.0, O=C(O)C([NH])[CH2]:1.0, O=C(O)C([NH])[CH2]>>O=C(N[CH])C([NH])[CH2]:1.0, O=C([CH])O:2.0, O=C([CH])O>>O=C([CH])[NH]:1.0, O=C([CH])[NH]:1.0, O=P(O)(O)O:2.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O:1.0, [CH]:6.0, [CH]N:1.0, [CH]N>>[C]N[CH]:1.0, [C]:2.0, [C]C([CH2])N:4.0, [C]C([CH2])N>>[C]C([CH2])N:1.0, [C]C([CH2])N>>[C]C([CH2])NC(=O)[CH]:1.0, [C]C([CH2])N>>[C]C([NH])[CH2]:1.0, [C]C([CH2])NC(=O)[CH]:1.0, [C]C([NH])[CH2]:1.0, [C]NC(C(=O)O)C[CH2]:1.0, [C]NC(C(=O)O)C[CH2]>>[C]NC(C(=O)NC([C])[CH2])C[CH2]:1.0, [C]NC1C([O])[CH]C(OC1OP([O])(=O)O)[CH2]>>[C]NC1C([O])[CH]C(OC1OP([O])(=O)O)[CH2]:1.0, [C]N[CH]:1.0, [C]O:1.0, [NH2]:1.0, [NH]:1.0, [OH]:2.0, [O]:1.0, [O]C([O])[CH]:2.0, [O]C([O])[CH]>>[O]C([O])[CH]:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]OC(O[CH])C([CH])[NH]:2.0, [P]OC(O[CH])C([CH])[NH]>>[P]OC(O[CH])C([CH])[NH]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[C]:2.0, [NH]:1.0, [OH]:2.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (8)
[O=C([CH])O:1.0, O=C([CH])[NH]:1.0, O=P(O)(O)O:1.0, [C]N[CH]:1.0, [C]O:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (7)
[O=C(N[CH])C([NH])[CH2]:1.0, O=C(O)C([NH])[CH2]:1.0, O=C([CH])O:1.0, O=P(O)(O)O:2.0, O=P(O)(O)O[P]:1.0, [C]C([CH2])NC(=O)[CH]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:6.0]


ID=Reaction Center at Level: 1 (3)
[[C]C([CH2])N:3.0, [C]C([NH])[CH2]:1.0, [O]C([O])[CH]:2.0]


ID=Reaction Center at Level: 2 (3)
[O=C(O)C(N)C[CH2]:3.0, [C]NC(C(=O)O)C[CH2]:1.0, [P]OC(O[CH])C([CH])[NH]:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: [P]O[P]>>[P]O
2: [O]C([O])[CH]>>[O]C([O])[CH]
3: [CH]N>>[C]N[CH]
4: [C]C([CH2])N>>[C]C([NH])[CH2]
5: [C]C([CH2])N>>[C]C([CH2])N
6: O=C([CH])O>>O=C([CH])[NH]
7: [O]P(=O)(O)O>>O=P(O)(O)O

MMP Level 2
1: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
2: [P]OC(O[CH])C([CH])[NH]>>[P]OC(O[CH])C([CH])[NH]
3: [C]C([CH2])N>>[C]C([CH2])NC(=O)[CH]
4: O=C(O)C(N)C[CH2]>>[C]NC(C(=O)O)C[CH2]
5: O=C(O)C(N)C[CH2]>>O=C(O)C(N)C[CH2]
6: O=C(O)C([NH])[CH2]>>O=C(N[CH])C([NH])[CH2]
7: O=P(O)(O)O[P]>>O=P(O)(O)O

MMP Level 3
1: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
2: [C]NC1C([O])[CH]C(OC1OP([O])(=O)O)[CH2]>>[C]NC1C([O])[CH]C(OC1OP([O])(=O)O)[CH2]
3: O=C(O)C(N)C[CH2]>>O=C(O)C(NC(=O)C([NH])[CH2])C[CH2]
4: O=C(O)C(N)CC[CH2]>>O=C([CH])NC(C(=O)O)CC[CH2]
5: O=C(O)C(N)CC[CH2]>>O=C(O)C(N)CC[CH2]
6: [C]NC(C(=O)O)C[CH2]>>[C]NC(C(=O)NC([C])[CH2])C[CH2]
7: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(OC(C(=O)NC(C(=O)NC(C(=O)O)CCC(=O)O)C)C)C3NC(=O)C)C(O)C2O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(OC(C(=O)NC(C(=O)NC(C(=O)NC(C(=O)O)CCCC(N)C(=O)O)CCC(=O)O)C)C)C3NC(=O)C)C(O)C2O, O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(OC(C(=O)NC(C(=O)NC(C(=O)O)CCC(=O)O)C)C)C3NC(=O)C)C(O)C2O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(OC(C(=O)NC(C(=O)NC(C(=O)NC(C(=O)O)CCCC(N)C(=O)O)CCC(=O)O)C)C)C3NC(=O)C)C(O)C2O]
2: R:M00002, P:M00004	[O=C(O)C(N)CCCC(N)C(=O)O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(OC(C(=O)NC(C(=O)NC(C(=O)NC(C(=O)O)CCCC(N)C(=O)O)CCC(=O)O)C)C)C3NC(=O)C)C(O)C2O, O=C(O)C(N)CCCC(N)C(=O)O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(OC(C(=O)NC(C(=O)NC(C(=O)NC(C(=O)O)CCCC(N)C(=O)O)CCC(=O)O)C)C)C3NC(=O)C)C(O)C2O, O=C(O)C(N)CCCC(N)C(=O)O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(OC(C(=O)NC(C(=O)NC(C(=O)NC(C(=O)O)CCCC(N)C(=O)O)CCC(=O)O)C)C)C3NC(=O)C)C(O)C2O]
3: R:M00003, P:M00005	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
4: R:M00003, P:M00006	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH2:4][CH2:5][CH:6]([NH:7][C:8](=[O:9])[CH:10]([NH:11][C:12](=[O:13])[CH:14]([O:15][CH:16]1[CH:17]([OH:18])[CH:19]([O:20][CH:21]([O:22][P:23](=[O:24])([OH:25])[O:26][P:27](=[O:28])([OH:29])[O:30][CH2:31][CH:32]2[O:33][CH:34]([N:35]3[CH:36]=[CH:37][C:38](=[O:39])[NH:40][C:41]3=[O:42])[CH:43]([OH:44])[CH:45]2[OH:46])[CH:47]1[NH:48][C:49](=[O:50])[CH3:51])[CH2:52][OH:53])[CH3:54])[CH3:55])[C:56](=[O:57])[OH:102].[O:89]=[C:90]([OH:91])[CH:92]([NH2:93])[CH2:94][CH2:95][CH2:96][CH:97]([NH2:98])[C:99](=[O:100])[OH:101].[O:58]=[P:59]([OH:60])([OH:61])[O:62][P:63](=[O:64])([OH:65])[O:66][P:67](=[O:68])([OH:69])[O:70][CH2:71][CH:72]1[O:73][CH:74]([N:75]:2:[CH:76]:[N:77]:[C:78]:3:[C:79](:[N:80]:[CH:81]:[N:82]:[C:83]32)[NH2:84])[CH:85]([OH:86])[CH:87]1[OH:88]>>[O:1]=[C:2]([OH:3])[CH2:4][CH2:5][CH:6]([NH:7][C:8](=[O:9])[CH:10]([NH:11][C:12](=[O:13])[CH:14]([O:15][CH:16]1[CH:17]([OH:18])[CH:19]([O:20][CH:21]([O:22][P:23](=[O:24])([OH:25])[O:26][P:27](=[O:28])([OH:29])[O:30][CH2:31][CH:32]2[O:33][CH:34]([N:35]3[CH:36]=[CH:37][C:38](=[O:39])[NH:40][C:41]3=[O:42])[CH:43]([OH:44])[CH:45]2[OH:46])[CH:47]1[NH:48][C:49](=[O:50])[CH3:51])[CH2:52][OH:53])[CH3:54])[CH3:55])[C:56](=[O:57])[NH:93][CH:92]([C:90](=[O:89])[OH:91])[CH2:94][CH2:95][CH2:96][CH:97]([NH2:98])[C:99](=[O:100])[OH:101].[O:58]=[P:59]([OH:60])([OH:61])[OH:103].[O:64]=[P:63]([OH:62])([OH:65])[O:66][P:67](=[O:68])([OH:69])[O:70][CH2:71][CH:72]1[O:73][CH:74]([N:75]:2:[CH:76]:[N:77]:[C:78]:3:[C:79](:[N:80]:[CH:81]:[N:82]:[C:83]32)[NH2:84])[CH:85]([OH:86])[CH:87]1[OH:88]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=55, 2=10, 3=8, 4=9, 5=7, 6=6, 7=5, 8=4, 9=2, 10=1, 11=3, 12=56, 13=57, 14=58, 15=11, 16=12, 17=13, 18=14, 19=54, 20=15, 21=16, 22=17, 23=19, 24=20, 25=21, 26=47, 27=48, 28=49, 29=50, 30=51, 31=22, 32=23, 33=24, 34=25, 35=26, 36=27, 37=28, 38=29, 39=30, 40=31, 41=32, 42=45, 43=43, 44=34, 45=33, 46=35, 47=36, 48=37, 49=38, 50=39, 51=40, 52=41, 53=42, 54=44, 55=46, 56=52, 57=53, 58=18, 59=96, 60=95, 61=93, 62=91, 63=90, 64=92, 65=94, 66=97, 67=98, 68=100, 69=101, 70=102, 71=99, 72=82, 73=83, 74=84, 75=79, 76=80, 77=81, 78=85, 79=78, 80=77, 81=76, 82=75, 83=86, 84=88, 85=73, 86=74, 87=72, 88=71, 89=68, 90=69, 91=70, 92=67, 93=64, 94=65, 95=66, 96=63, 97=60, 98=59, 99=61, 100=62, 101=89, 102=87}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=55, 2=10, 3=8, 4=9, 5=7, 6=6, 7=5, 8=4, 9=2, 10=1, 11=3, 12=56, 13=57, 14=58, 15=59, 16=63, 17=64, 18=65, 19=66, 20=68, 21=69, 22=70, 23=67, 24=60, 25=61, 26=62, 27=11, 28=12, 29=13, 30=14, 31=54, 32=15, 33=16, 34=47, 35=21, 36=20, 37=19, 38=17, 39=18, 40=52, 41=53, 42=22, 43=23, 44=24, 45=25, 46=26, 47=27, 48=28, 49=29, 50=30, 51=31, 52=32, 53=45, 54=43, 55=34, 56=33, 57=35, 58=36, 59=37, 60=38, 61=39, 62=40, 63=41, 64=42, 65=44, 66=46, 67=48, 68=49, 69=50, 70=51, 71=90, 72=91, 73=92, 74=87, 75=88, 76=89, 77=93, 78=86, 79=85, 80=84, 81=83, 82=94, 83=96, 84=81, 85=82, 86=80, 87=79, 88=76, 89=77, 90=78, 91=75, 92=72, 93=71, 94=73, 95=74, 96=97, 97=95, 98=100, 99=99, 100=98, 101=101, 102=102}

