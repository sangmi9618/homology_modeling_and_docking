
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0, C=O:1.0, O-P:2.0, O=O:1.0]

ORDER_CHANGED
[C%N*C%N:1.0, C-C*C=C:1.0, C-O*C=O:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(O)C([NH])[CH2]:1.0, O=C(O)C([NH])[CH2]>>O=C=O:2.0, O=C(O)C1N[C]SC1:1.0, O=C(O)C1N[C]SC1>>O=C1N=[C]SC1:1.0, O=C(O)C1N[C]SC1>>O=C=O:1.0, O=C([CH])O:2.0, O=C([CH])O>>O=C=O:2.0, O=C([CH])[CH]:2.0, O=C([CH])[CH]>>[O-]C([CH])=[CH]:2.0, O=C1C=C[C]C([S])=C1>>[O-]C=1C=C[C]=C([S])C1:1.0, O=C1N=[C]SC1:1.0, O=C=O:3.0, O=O:4.0, O=O>>O=C1N=[C]SC1:1.0, O=O>>O=P(O)(O)O[P]:1.0, O=O>>[C]=O:1.0, O=O>>[N]C(=O)[CH2]:1.0, O=O>>[O]P(=O)(O)O:1.0, O=O>>[P]O:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]:1.0, O=P(O)(O[P])O[P]:1.0, O=P(O)(O[P])O[P]>>O=P(O)(O)O[P]:1.0, [CH]:1.0, [CH]c1nc(sc1=[CH])=C2SC[CH]N2>>[CH]c1nc(sc1[CH])C2=N[C]CS2:1.0, [C]:9.0, [C]=C([N])[S]:1.0, [C]=C([N])[S]>>[C]C(=[N])[S]:1.0, [C]=C([S])[NH]:1.0, [C]=C([S])[NH]>>[C]C(=[N])[S]:1.0, [C]=C1SCC(N1)C(=O)O>>[C]C1=NC(=O)CS1:1.0, [C]=CC(=O)C=[CH]:1.0, [C]=CC(=O)C=[CH]>>[C]C=C([O-])C=[CH]:2.0, [C]=O:3.0, [C]=O>>[C][O-]:1.0, [C]C(=[N])[S]:2.0, [C]C([NH])[CH2]:1.0, [C]C([NH])[CH2]>>[N]C(=O)[CH2]:1.0, [C]C1=NC(=O)CS1:1.0, [C]C1NC(=[C])SC1:1.0, [C]C1NC(=[C])SC1>>[C]C1=NC(=O)CS1:1.0, [C]C1NC(SC1)=C2N=[C][C]S2>>O=C1N=C(SC1)C2=N[C]=[C]S2:1.0, [C]C=C([O-])C=[CH]:1.0, [C]N=[C]:1.0, [C]N[CH]:1.0, [C]N[CH]>>[C]N=[C]:1.0, [C]O:1.0, [C]O>>[C]=O:1.0, [C][O-]:1.0, [NH]:1.0, [N]:1.0, [N]=C([S])C1=N[C]=[C]S1:1.0, [N]=C([S])C1=N[C]CS1:1.0, [N]C(=O)[CH2]:2.0, [N]C([S])=C1SCC(N1)C(=O)O>>[N]=C([S])C1=NC(=O)CS1:1.0, [N]C([S])=C1SC[CH]N1:1.0, [N]C([S])=C1SC[CH]N1>>[N]=C([S])C1=N[C]CS1:1.0, [O-]:1.0, [O-]C([CH])=[CH]:2.0, [OH]:3.0, [O]:6.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)OP(=O)(O)OP(=O)(O)O>>O=P(O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [O]P([O])(=O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0, [S]C([NH])=C1N=[C][C]S1:1.0, [S]C([NH])=C1N=[C][C]S1>>[N]=C([S])C1=N[C]=[C]S1:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[CH]:1.0, [C]:2.0, [OH]:1.0, [O]:4.0, [P]:2.0]


ID=Reaction Center at Level: 1 (9)
[O=C([CH])O:1.0, O=O:2.0, [C]=O:1.0, [C]C([NH])[CH2]:1.0, [N]C(=O)[CH2]:1.0, [O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (9)
[O=C(O)C([NH])[CH2]:1.0, O=C(O)C1N[C]SC1:1.0, O=C1N=[C]SC1:1.0, O=O:2.0, O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[P]:1.0, [N]C(=O)[CH2]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (6)
[[C]:8.0, [NH]:1.0, [N]:1.0, [O-]:1.0, [OH]:1.0, [O]:2.0]


ID=Reaction Center at Level: 1 (12)
[O=C([CH])O:1.0, O=C([CH])[CH]:1.0, O=C=O:1.0, [C]=C([N])[S]:1.0, [C]=C([S])[NH]:1.0, [C]=O:2.0, [C]C(=[N])[S]:2.0, [C]N=[C]:1.0, [C]N[CH]:1.0, [C]O:1.0, [C][O-]:1.0, [O-]C([CH])=[CH]:1.0]


ID=Reaction Center at Level: 2 (13)
[O=C(O)C([NH])[CH2]:1.0, O=C([CH])O:1.0, O=C([CH])[CH]:1.0, O=C=O:2.0, [C]=CC(=O)C=[CH]:1.0, [C]C1=NC(=O)CS1:1.0, [C]C1NC(=[C])SC1:1.0, [C]C=C([O-])C=[CH]:1.0, [N]=C([S])C1=N[C]=[C]S1:1.0, [N]=C([S])C1=N[C]CS1:1.0, [N]C([S])=C1SC[CH]N1:1.0, [O-]C([CH])=[CH]:1.0, [S]C([NH])=C1N=[C][C]S1:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]C([NH])[CH2]:1.0, [N]C(=O)[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C1N[C]SC1:1.0, O=C1N=[C]SC1:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]=O>>[C][O-]
2: [C]N[CH]>>[C]N=[C]
3: O=C([CH])[CH]>>[O-]C([CH])=[CH]
4: O=O>>[C]=O
5: [P]O[P]>>[P]O
6: [C]=C([S])[NH]>>[C]C(=[N])[S]
7: [C]=C([N])[S]>>[C]C(=[N])[S]
8: O=C([CH])O>>O=C=O
9: O=O>>[P]O
10: [C]O>>[C]=O
11: [O]P([O])(=O)O>>[O]P(=O)(O)O
12: [C]C([NH])[CH2]>>[N]C(=O)[CH2]

MMP Level 2
1: O=C([CH])[CH]>>[O-]C([CH])=[CH]
2: [C]C1NC(=[C])SC1>>[C]C1=NC(=O)CS1
3: [C]=CC(=O)C=[CH]>>[C]C=C([O-])C=[CH]
4: O=O>>[N]C(=O)[CH2]
5: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
6: [N]C([S])=C1SC[CH]N1>>[N]=C([S])C1=N[C]CS1
7: [S]C([NH])=C1N=[C][C]S1>>[N]=C([S])C1=N[C]=[C]S1
8: O=C(O)C([NH])[CH2]>>O=C=O
9: O=O>>[O]P(=O)(O)O
10: O=C([CH])O>>O=C=O
11: O=P(O)(O[P])O[P]>>O=P(O)(O)O[P]
12: O=C(O)C1N[C]SC1>>O=C1N=[C]SC1

MMP Level 3
1: [C]=CC(=O)C=[CH]>>[C]C=C([O-])C=[CH]
2: [N]C([S])=C1SCC(N1)C(=O)O>>[N]=C([S])C1=NC(=O)CS1
3: O=C1C=C[C]C([S])=C1>>[O-]C=1C=C[C]=C([S])C1
4: O=O>>O=C1N=[C]SC1
5: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]
6: [C]C1NC(SC1)=C2N=[C][C]S2>>O=C1N=C(SC1)C2=N[C]=[C]S2
7: [CH]c1nc(sc1=[CH])=C2SC[CH]N2>>[CH]c1nc(sc1[CH])C2=N[C]CS2
8: O=C(O)C1N[C]SC1>>O=C=O
9: O=O>>O=P(O)(O)O[P]
10: O=C(O)C([NH])[CH2]>>O=C=O
11: [O]P(=O)(O)OP(=O)(O)OP(=O)(O)O>>O=P(O)(O)OP(=O)(O)O
12: [C]=C1SCC(N1)C(=O)O>>[C]C1=NC(=O)CS1


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C1C=Cc2nc(sc2=C1)=C3SCC(N3)C(=O)O>>[O-]c1ccc2nc(sc2c1)C3=NC(=O)CS3, O=C1C=Cc2nc(sc2=C1)=C3SCC(N3)C(=O)O>>[O-]c1ccc2nc(sc2c1)C3=NC(=O)CS3, O=C1C=Cc2nc(sc2=C1)=C3SCC(N3)C(=O)O>>[O-]c1ccc2nc(sc2c1)C3=NC(=O)CS3, O=C1C=Cc2nc(sc2=C1)=C3SCC(N3)C(=O)O>>[O-]c1ccc2nc(sc2c1)C3=NC(=O)CS3, O=C1C=Cc2nc(sc2=C1)=C3SCC(N3)C(=O)O>>[O-]c1ccc2nc(sc2c1)C3=NC(=O)CS3, O=C1C=Cc2nc(sc2=C1)=C3SCC(N3)C(=O)O>>[O-]c1ccc2nc(sc2c1)C3=NC(=O)CS3]
2: R:M00001, P:M00005	[O=C1C=Cc2nc(sc2=C1)=C3SCC(N3)C(=O)O>>O=C=O, O=C1C=Cc2nc(sc2=C1)=C3SCC(N3)C(=O)O>>O=C=O]
3: R:M00002, P:M00004	[O=O>>[O-]c1ccc2nc(sc2c1)C3=NC(=O)CS3]
4: R:M00002, P:M00008	[O=O>>O=P(O)(O)OP(=O)(O)O]
5: R:M00003, P:M00007	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
6: R:M00003, P:M00008	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:50]=[O:51].[O:48]=[C:47]([OH:49])[CH:45]1[NH:46][C:42]([S:43][CH2:44]1)=[C:38]2[N:37]=[C:36]3[CH:35]=[CH:34][C:33](=[O:32])[CH:41]=[C:40]3[S:39]2.[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:48]=[C:47]=[O:49].[O:50]=[C:45]1[N:46]=[C:42]([S:43][CH2:44]1)[C:38]:2:[N:37]:[C:36]:3:[CH:35]:[CH:34]:[C:33]([O-:32]):[CH:41]:[C:40]3:[S:39]2.[O:11]=[P:10]([OH:9])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[OH:51].[OH2:52]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=44, 2=45, 3=46, 4=42, 5=38, 6=37, 7=36, 8=35, 9=34, 10=33, 11=32, 12=41, 13=40, 14=39, 15=43, 16=47, 17=48, 18=49, 19=50, 20=51, 21=24, 22=25, 23=26, 24=21, 25=22, 26=23, 27=27, 28=20, 29=19, 30=18, 31=17, 32=28, 33=30, 34=15, 35=16, 36=14, 37=13, 38=10, 39=11, 40=12, 41=9, 42=6, 43=7, 44=8, 45=5, 46=2, 47=1, 48=3, 49=4, 50=31, 51=29}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=29, 2=25, 3=24, 4=26, 5=27, 6=28, 7=30, 8=31, 9=32, 10=38, 11=39, 12=37, 13=35, 14=34, 15=33, 16=36, 17=50, 18=49, 19=51, 20=52, 21=16, 22=17, 23=18, 24=13, 25=14, 26=15, 27=19, 28=12, 29=11, 30=10, 31=9, 32=20, 33=22, 34=7, 35=8, 36=6, 37=5, 38=2, 39=1, 40=3, 41=4, 42=23, 43=21, 44=42, 45=41, 46=40, 47=43, 48=44, 49=45, 50=46, 51=47, 52=48}

