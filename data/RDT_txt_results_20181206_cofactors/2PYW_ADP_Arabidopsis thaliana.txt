
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[CH]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O[CH]:1.0, OC1OC([CH2])C(O)C1O>>O=P(O)(O)OC1OC([CH2])C(O)C1O:1.0, OC1O[CH][CH]C1O:1.0, OC1O[CH][CH]C1O>>O=P(O)(O)OC1O[CH][CH]C1O:1.0, OC1O[CH][CH]C1O>>[P]OC1O[CH][CH]C1O:1.0, [CH]:2.0, [CH]O:1.0, [CH]O>>[P]O[CH]:1.0, [OH]:2.0, [O]:2.0, [O]C([CH])O:2.0, [O]C([CH])O>>[O]C([CH])OP(=O)(O)O:1.0, [O]C([CH])O>>[O]C([O])[CH]:1.0, [O]C([CH])OP(=O)(O)O:1.0, [O]C([O])[CH]:1.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]C([CH])OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]OC1O[CH][CH]C1O:1.0, [P]O[CH]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[O]P(=O)(O)O:2.0, [P]O[CH]:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)O[CH]:1.0, O=P(O)(O)O[P]:1.0, [O]C([CH])OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]C([CH])O:1.0, [O]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[OC1O[CH][CH]C1O:1.0, [P]OC1O[CH][CH]C1O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [O]C([CH])O>>[O]C([O])[CH]
2: [O]P(=O)(O)O>>[O]P(=O)(O)O
3: [P]O[P]>>[P]O
4: [CH]O>>[P]O[CH]

MMP Level 2
1: OC1O[CH][CH]C1O>>[P]OC1O[CH][CH]C1O
2: O=P(O)(O)O[P]>>O=P(O)(O)O[CH]
3: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
4: [O]C([CH])O>>[O]C([CH])OP(=O)(O)O

MMP Level 3
1: OC1OC([CH2])C(O)C1O>>O=P(O)(O)OC1OC([CH2])C(O)C1O
2: [O]P(=O)(O)OP(=O)(O)O>>[O]C([CH])OP(=O)(O)O
3: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
4: OC1O[CH][CH]C1O>>O=P(O)(O)OC1O[CH][CH]C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[OC1OC(CSC)C(O)C1O>>O=P(O)(O)OC1OC(CSC)C(O)C1O, OC1OC(CSC)C(O)C1O>>O=P(O)(O)OC1OC(CSC)C(O)C1O]
2: R:M00002, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
3: R:M00002, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OC1OC(CSC)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31].[OH:32][CH:33]1[O:34][CH:35]([CH2:36][S:37][CH3:38])[CH:39]([OH:40])[CH:41]1[OH:42]>>[O:1]=[P:2]([OH:3])([OH:4])[O:32][CH:33]1[O:34][CH:35]([CH2:36][S:37][CH3:38])[CH:39]([OH:40])[CH:41]1[OH:42].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=38, 2=37, 3=36, 4=35, 5=39, 6=41, 7=33, 8=34, 9=32, 10=42, 11=40, 12=24, 13=25, 14=26, 15=21, 16=22, 17=23, 18=27, 19=20, 20=19, 21=18, 22=17, 23=28, 24=30, 25=15, 26=16, 27=14, 28=13, 29=10, 30=11, 31=12, 32=9, 33=6, 34=7, 35=8, 36=5, 37=2, 38=1, 39=3, 40=4, 41=31, 42=29}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=17, 5=18, 6=19, 7=23, 8=16, 9=15, 10=14, 11=13, 12=24, 13=26, 14=11, 15=12, 16=10, 17=9, 18=6, 19=7, 20=8, 21=5, 22=2, 23=1, 24=3, 25=4, 26=27, 27=25, 28=38, 29=37, 30=36, 31=35, 32=39, 33=41, 34=33, 35=34, 36=32, 37=29, 38=28, 39=30, 40=31, 41=42, 42=40}

