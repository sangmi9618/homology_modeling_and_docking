
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)C[NH]:1.0, O=C(O)C[NH]>>O:1.0, O=C(O)[CH2]:2.0, O=C(O)[CH2]>>O:1.0, [C]:1.0, [C]O:1.0, [C]O>>O:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[O:1.0, [C]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (3)
[O:1.0, O=C(O)[CH2]:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (3)
[O:1.0, O=C(O)C[NH]:1.0, O=C(O)[CH2]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]O>>O

MMP Level 2
1: O=C(O)[CH2]>>O

MMP Level 3
1: O=C(O)C[NH]>>O


//
REACTION MMP (RPAIR)
1: R:M00002, P:M00003	[O=C(O)CNC(=O)C(NC(=O)CCC(N)C(=O)O)CS>>O]


//
SELECTED AAM MAPPING
[O:44]=[C:45]([OH:1])[CH2:46][NH:47][C:48](=[O:49])[CH:50]([NH:51][C:52](=[O:53])[CH2:54][CH2:55][CH:56]([NH2:57])[C:58](=[O:59])[OH:60])[CH2:61][SH:62].[O:2]=[C:3]([OH:4])[CH2:5][CH2:6][C:7]:1:[C:8]2:[NH:9]:[C:10](:[C:11]1[CH3:12])[CH2:13][C:14]:3:[NH:15]:[C:16](:[C:17](:[C:18]3[CH:19]=[CH2:20])[CH3:21])[CH2:22][C:23]:4:[NH:24]:[C:25](:[C:26](:[C:27]4[CH:28]=[CH2:29])[CH3:30])[CH2:31][C:32]:5:[NH:33]:[C:34](:[C:35](:[C:36]5[CH3:37])[CH2:38][CH2:39][C:40](=[O:41])[OH:42])[CH2:43]2>>[OH2:1]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=36, 2=35, 3=31, 4=30, 5=24, 6=25, 7=26, 8=22, 9=23, 10=21, 11=15, 12=16, 13=17, 14=13, 15=14, 16=12, 17=9, 18=10, 19=6, 20=7, 21=8, 22=42, 23=33, 24=34, 25=37, 26=38, 27=39, 28=40, 29=41, 30=32, 31=5, 32=4, 33=2, 34=1, 35=3, 36=11, 37=18, 38=19, 39=20, 40=27, 41=28, 42=29, 43=55, 44=54, 45=52, 46=53, 47=51, 48=50, 49=61, 50=62, 51=48, 52=49, 53=47, 54=46, 55=44, 56=43, 57=45, 58=56, 59=58, 60=59, 61=60, 62=57}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=1}

