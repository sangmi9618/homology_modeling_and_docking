
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0, C-S:1.0]

//
FINGERPRINTS RC
[S([CH2])[CH2]:1.0, [CH2][S+]([CH2])C:2.0, [CH2][S+]([CH2])C>>S([CH2])[CH2]:1.0, [CH2][S+]([CH2])C>>[C]C(=[C])C:1.0, [CH3]:2.0, [CH]:1.0, [CH]CSC[CH2]:1.0, [CH]C[S+](C)C[CH2]:1.0, [CH]C[S+](C)C[CH2]>>[CH]CSC[CH2]:1.0, [CH]C[S+](C)C[CH2]>>[C]=C([O])C(=C([CH])O)C:1.0, [C]:1.0, [C]=C([O])C(=C([CH])O)C:1.0, [C]=C([O])C=C([CH])O:1.0, [C]=C([O])C=C([CH])O>>[C]=C([O])C(=C([CH])O)C:1.0, [C]C:1.0, [C]C(=[C])C:2.0, [C]C=[C]:1.0, [C]C=[C]>>[C]C(=[C])C:1.0, [C]Oc1cc(O)ccc1[C]>>[C]Oc1c([C])ccc(O)c1C:1.0, [O]C([CH])C[S+](C)CC[CH]>>[O]C([CH])CSCC[CH]:1.0, [S+]:1.0, [S+]C:1.0, [S+]C>>[C]C:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH3]:2.0, [C]:1.0, [S+]:1.0]


ID=Reaction Center at Level: 1 (4)
[[CH2][S+]([CH2])C:1.0, [C]C:1.0, [C]C(=[C])C:1.0, [S+]C:1.0]


ID=Reaction Center at Level: 2 (4)
[[CH2][S+]([CH2])C:1.0, [CH]C[S+](C)C[CH2]:1.0, [C]=C([O])C(=C([CH])O)C:1.0, [C]C(=[C])C:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH2][S+]([CH2])C>>S([CH2])[CH2]
2: [C]C=[C]>>[C]C(=[C])C
3: [S+]C>>[C]C

MMP Level 2
1: [CH]C[S+](C)C[CH2]>>[CH]CSC[CH2]
2: [C]=C([O])C=C([CH])O>>[C]=C([O])C(=C([CH])O)C
3: [CH2][S+]([CH2])C>>[C]C(=[C])C

MMP Level 3
1: [O]C([CH])C[S+](C)CC[CH]>>[O]C([CH])CSCC[CH]
2: [C]Oc1cc(O)ccc1[C]>>[C]Oc1c([C])ccc(O)c1C
3: [CH]C[S+](C)C[CH2]>>[C]=C([O])C(=C([CH])O)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(O)C(N)CCSCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=c1oc2c(ccc(O)c2C)c(O)c1NC(=O)c3ccc(O)c(c3)CC=C(C)C]
3: R:M00002, P:M00004	[O=c1oc2cc(O)ccc2c(O)c1NC(=O)c3ccc(O)c(c3)CC=C(C)C>>O=c1oc2c(ccc(O)c2C)c(O)c1NC(=O)c3ccc(O)c(c3)CC=C(C)C]


//
SELECTED AAM MAPPING
[O:29]=[C:30]([OH:31])[CH:32]([NH2:33])[CH2:34][CH2:35][S+:36]([CH3:37])[CH2:38][CH:39]1[O:40][CH:41]([N:42]:2:[CH:43]:[N:44]:[C:45]:3:[C:46](:[N:47]:[CH:48]:[N:49]:[C:50]32)[NH2:51])[CH:52]([OH:53])[CH:54]1[OH:55].[O:1]=[C:2]1[O:3][C:4]:2:[CH:5]:[C:6]([OH:7]):[CH:8]:[CH:9]:[C:10]2[C:11]([OH:12])=[C:13]1[NH:14][C:15](=[O:16])[C:17]:3:[CH:18]:[CH:19]:[C:20]([OH:21]):[C:22](:[CH:23]3)[CH2:24][CH:25]=[C:26]([CH3:28])[CH3:27]>>[O:29]=[C:30]([OH:31])[CH:32]([NH2:33])[CH2:34][CH2:35][S:36][CH2:38][CH:39]1[O:40][CH:41]([N:42]:2:[CH:43]:[N:44]:[C:45]:3:[C:46](:[N:47]:[CH:48]:[N:49]:[C:50]32)[NH2:51])[CH:52]([OH:53])[CH:54]1[OH:55].[O:1]=[C:2]1[O:3][C:4]:2:[C:10](:[CH:9]:[CH:8]:[C:6]([OH:7]):[C:5]2[CH3:37])[C:11]([OH:12])=[C:13]1[NH:14][C:15](=[O:16])[C:17]:3:[CH:18]:[CH:19]:[C:20]([OH:21]):[C:22](:[CH:23]3)[CH2:24][CH:25]=[C:26]([CH3:27])[CH3:28]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=37, 2=36, 3=35, 4=34, 5=32, 6=30, 7=29, 8=31, 9=33, 10=38, 11=39, 12=54, 13=52, 14=41, 15=40, 16=42, 17=43, 18=44, 19=45, 20=50, 21=49, 22=48, 23=47, 24=46, 25=51, 26=53, 27=55, 28=27, 29=26, 30=25, 31=24, 32=22, 33=20, 34=19, 35=18, 36=17, 37=23, 38=15, 39=16, 40=14, 41=13, 42=11, 43=10, 44=4, 45=5, 46=6, 47=8, 48=9, 49=7, 50=3, 51=2, 52=1, 53=12, 54=21, 55=28}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=48, 2=49, 3=50, 4=45, 5=46, 6=47, 7=51, 8=44, 9=43, 10=42, 11=41, 12=52, 13=54, 14=39, 15=40, 16=38, 17=37, 18=36, 19=35, 20=33, 21=31, 22=30, 23=32, 24=34, 25=55, 26=53, 27=11, 28=10, 29=8, 30=7, 31=6, 32=5, 33=4, 34=3, 35=2, 36=1, 37=14, 38=12, 39=13, 40=15, 41=16, 42=17, 43=18, 44=24, 45=23, 46=21, 47=20, 48=19, 49=22, 50=25, 51=26, 52=27, 53=28, 54=29, 55=9}

