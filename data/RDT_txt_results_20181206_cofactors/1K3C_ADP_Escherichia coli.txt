
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0, O-P:2.0]

ORDER_CHANGED
[C-C*C=C:1.0, C-O*C=O:2.0]

//
FINGERPRINTS RC
[O=C(O)C(=O)CC(=O)O>>O=C(O)C(OP(=O)(O)O)=C:1.0, O=C(O)C(=O)CC(=O)O>>O=C(O)C(O[P])=C:1.0, O=C(O)C(O[P])=C:1.0, O=C(O)[CH2]:2.0, O=C(O)[CH2]>>O=C=O:2.0, O=C=O:3.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>[C]OP(=O)(O)O:1.0, [CH2]:2.0, [C]:4.0, [C]=C:1.0, [C]=O:2.0, [C]=O>>[C]O[P]:1.0, [C]C(=O)CC(=O)O:1.0, [C]C(=O)CC(=O)O>>O=C=O:1.0, [C]C(=O)CC(=O)O>>[C]C([O])=C:1.0, [C]C(=O)[CH2]:2.0, [C]C(=O)[CH2]>>[C]C(OP(=O)(O)O)=C:1.0, [C]C(=O)[CH2]>>[C]C([O])=C:1.0, [C]C(OP(=O)(O)O)=C:1.0, [C]C([O])=C:2.0, [C]CC(=O)C(=O)O:1.0, [C]CC(=O)C(=O)O>>O=C(O)C(OP(=O)(O)O)=C:1.0, [C]CC(=O)C(=O)O>>O=C(O)C(O[P])=C:1.0, [C]CC(=O)O:1.0, [C]CC(=O)O>>O=C=O:2.0, [C]C[C]:1.0, [C]C[C]>>[C]=C:1.0, [C]O:1.0, [C]O>>[C]=O:1.0, [C]OP(=O)(O)O:1.0, [C]O[P]:1.0, [OH]:2.0, [O]:4.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[C]C(OP(=O)(O)O)=C:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[CH2]:1.0, [C]:1.0, [O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[O=C(O)[CH2]:1.0, [C]C[C]:1.0, [C]O[P]:1.0, [O]P(=O)(O)O:2.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (6)
[O=P(O)(O)O[P]:1.0, [C]C(=O)CC(=O)O:1.0, [C]C(OP(=O)(O)O)=C:1.0, [C]CC(=O)O:1.0, [C]OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH2]:2.0, [C]:4.0, [OH]:1.0, [O]:3.0]


ID=Reaction Center at Level: 1 (9)
[O=C(O)[CH2]:1.0, O=C=O:1.0, [C]=C:1.0, [C]=O:2.0, [C]C(=O)[CH2]:1.0, [C]C([O])=C:1.0, [C]C[C]:1.0, [C]O:1.0, [C]O[P]:1.0]


ID=Reaction Center at Level: 2 (9)
[O=C(O)C(O[P])=C:1.0, O=C(O)[CH2]:1.0, O=C=O:2.0, [C]C(=O)CC(=O)O:1.0, [C]C(=O)[CH2]:1.0, [C]C(OP(=O)(O)O)=C:1.0, [C]C([O])=C:1.0, [C]CC(=O)C(=O)O:1.0, [C]CC(=O)O:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C(=O)[CH2]>>[C]C([O])=C
2: [C]=O>>[C]O[P]
3: O=C(O)[CH2]>>O=C=O
4: [C]C[C]>>[C]=C
5: [O]P(=O)(O)O>>[O]P(=O)(O)O
6: [C]O>>[C]=O
7: [P]O[P]>>[P]O

MMP Level 2
1: [C]CC(=O)C(=O)O>>O=C(O)C(O[P])=C
2: [C]C(=O)[CH2]>>[C]C(OP(=O)(O)O)=C
3: [C]CC(=O)O>>O=C=O
4: [C]C(=O)CC(=O)O>>[C]C([O])=C
5: O=P(O)(O)O[P]>>[C]OP(=O)(O)O
6: O=C(O)[CH2]>>O=C=O
7: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O

MMP Level 3
1: O=C(O)C(=O)CC(=O)O>>O=C(O)C(OP(=O)(O)O)=C
2: [C]CC(=O)C(=O)O>>O=C(O)C(OP(=O)(O)O)=C
3: [C]C(=O)CC(=O)O>>O=C=O
4: O=C(O)C(=O)CC(=O)O>>O=C(O)C(O[P])=C
5: [O]P(=O)(O)OP(=O)(O)O>>[C]C(OP(=O)(O)O)=C
6: [C]CC(=O)O>>O=C=O
7: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(O)C(OP(=O)(O)O)=C]
3: R:M00002, P:M00004	[O=C(O)C(=O)CC(=O)O>>O=C(O)C(OP(=O)(O)O)=C, O=C(O)C(=O)CC(=O)O>>O=C(O)C(OP(=O)(O)O)=C, O=C(O)C(=O)CC(=O)O>>O=C(O)C(OP(=O)(O)O)=C]
4: R:M00002, P:M00005	[O=C(O)C(=O)CC(=O)O>>O=C=O, O=C(O)C(=O)CC(=O)O>>O=C=O]


//
SELECTED AAM MAPPING
[O:32]=[C:33]([OH:34])[C:35](=[O:36])[CH2:37][C:38](=[O:39])[OH:40].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:39]=[C:38]=[O:40].[O:32]=[C:33]([OH:34])[C:35]([O:36][P:2](=[O:1])([OH:3])[OH:4])=[CH2:37].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=37, 33=35, 34=36, 35=33, 36=32, 37=34, 38=38, 39=39, 40=40}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=17, 5=18, 6=19, 7=23, 8=16, 9=15, 10=14, 11=13, 12=24, 13=26, 14=11, 15=12, 16=10, 17=9, 18=6, 19=7, 20=8, 21=5, 22=2, 23=1, 24=3, 25=4, 26=27, 27=25, 28=37, 29=31, 30=29, 31=28, 32=30, 33=32, 34=33, 35=34, 36=35, 37=36, 38=39, 39=38, 40=40}

