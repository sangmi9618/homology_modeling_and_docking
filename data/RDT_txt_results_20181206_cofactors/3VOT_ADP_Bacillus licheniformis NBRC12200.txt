
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0, O-P:2.0]

//
FINGERPRINTS RC
[O=C(N[CH])C([CH2])N:1.0, O=C(O)C(N)C>>O=C(O)C(NC(=O)C([CH2])N)C:1.0, O=C(O)C(N)C[CH]>>[C]C(NC(=O)C(N)C[CH])C:1.0, O=C(O)C([CH2])N:1.0, O=C(O)C([CH2])N>>O=C(N[CH])C([CH2])N:1.0, O=C(O)C([CH2])N>>O=P(O)(O)O:1.0, O=C([CH])O:2.0, O=C([CH])O>>O=C([CH])[NH]:1.0, O=C([CH])O>>O=P(O)(O)O:1.0, O=C([CH])[NH]:1.0, O=P(O)(O)O:3.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O:1.0, [CH]N:1.0, [CH]N>>[C]N[CH]:1.0, [C]:2.0, [C]C(N)C:1.0, [C]C(N)C>>[C]C(NC(=O)[CH])C:1.0, [C]C(NC(=O)[CH])C:1.0, [C]N[CH]:1.0, [C]O:1.0, [C]O>>[P]O:1.0, [NH2]:1.0, [NH]:1.0, [OH]:3.0, [O]:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[C]:2.0, [NH]:1.0, [OH]:2.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (8)
[O=C([CH])O:1.0, O=C([CH])[NH]:1.0, O=P(O)(O)O:1.0, [C]N[CH]:1.0, [C]O:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (7)
[O=C(N[CH])C([CH2])N:1.0, O=C(O)C([CH2])N:1.0, O=C([CH])O:1.0, O=P(O)(O)O:2.0, O=P(O)(O)O[P]:1.0, [C]C(NC(=O)[CH])C:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=C([CH])O>>O=C([CH])[NH]
2: [O]P(=O)(O)O>>O=P(O)(O)O
3: [C]O>>[P]O
4: [P]O[P]>>[P]O
5: [CH]N>>[C]N[CH]

MMP Level 2
1: O=C(O)C([CH2])N>>O=C(N[CH])C([CH2])N
2: O=P(O)(O)O[P]>>O=P(O)(O)O
3: O=C([CH])O>>O=P(O)(O)O
4: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
5: [C]C(N)C>>[C]C(NC(=O)[CH])C

MMP Level 3
1: O=C(O)C(N)C[CH]>>[C]C(NC(=O)C(N)C[CH])C
2: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O
3: O=C(O)C([CH2])N>>O=P(O)(O)O
4: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
5: O=C(O)C(N)C>>O=C(O)C(NC(=O)C([CH2])N)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00005	[O=C(O)C(N)CC(C)C>>O=P(O)(O)O]
2: R:M00001, P:M00006	[O=C(O)C(N)CC(C)C>>O=C(O)C(NC(=O)C(N)CC(C)C)C]
3: R:M00002, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
4: R:M00002, P:M00005	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)O]
5: R:M00003, P:M00006	[O=C(O)C(N)C>>O=C(O)C(NC(=O)C(N)CC(C)C)C]


//
SELECTED AAM MAPPING
[O:41]=[C:42]([OH:43])[CH:44]([NH2:45])[CH3:46].[O:32]=[C:33]([OH:34])[CH:35]([NH2:36])[CH2:37][CH:38]([CH3:39])[CH3:40].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:41]=[C:42]([OH:43])[CH:44]([NH:45][C:33](=[O:32])[CH:35]([NH2:36])[CH2:37][CH:38]([CH3:39])[CH3:40])[CH3:46].[O:1]=[P:2]([OH:3])([OH:4])[OH:34].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=39, 2=38, 3=40, 4=37, 5=35, 6=33, 7=32, 8=34, 9=36, 10=24, 11=25, 12=26, 13=21, 14=22, 15=23, 16=27, 17=20, 18=19, 19=18, 20=17, 21=28, 22=30, 23=15, 24=16, 25=14, 26=13, 27=10, 28=11, 29=12, 30=9, 31=6, 32=7, 33=8, 34=5, 35=2, 36=1, 37=3, 38=4, 39=31, 40=29, 41=46, 42=44, 43=42, 44=41, 45=43, 46=45}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=17, 5=18, 6=19, 7=23, 8=16, 9=15, 10=14, 11=13, 12=24, 13=26, 14=11, 15=12, 16=10, 17=9, 18=6, 19=7, 20=8, 21=5, 22=2, 23=1, 24=3, 25=4, 26=27, 27=25, 28=44, 29=43, 30=42, 31=45, 32=46, 33=41, 34=31, 35=29, 36=28, 37=30, 38=32, 39=33, 40=34, 41=35, 42=37, 43=38, 44=39, 45=40, 46=36}

