
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

STEREO_CHANGED
[C(R/S):4.0]

//
FINGERPRINTS RC
[O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[CH]>>O=P(O)(O)OP(=O)(O)O[CH]:1.0, O=P(O)(O)O[P]:2.0, O=P(O)(O)O[P]>>O=P(O)(O)O[P]:1.0, [CH]:8.0, [OH]:2.0, [O]:2.0, [O]C([CH])C(O[P])C([O])[CH]:8.0, [O]C([CH])C(O[P])C([O])[CH]>>[O]C([CH])C(O[P])C([O])[CH]:4.0, [O]C([CH])[CH]:8.0, [O]C([CH])[CH]>>[O]C([CH])[CH]:4.0, [O]C1[CH]C([O])C(O[P])C(OP(=O)(O)O)C1O[P]>>[O]C1[CH]C([O])C(O[P])C(OP(=O)(O)O)C1O[P]:4.0, [O]P(=O)(O)O:4.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:2.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O>>[P]O[P]:1.0, [P]O[P]:2.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]P(=O)(O)O:2.0, [P]O[P]:2.0]


ID=Reaction Center at Level: 2 (2)
[O=P(O)(O)O[P]:2.0, [O]P(=O)(O)OP(=O)(O)O:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:8.0]


ID=Reaction Center at Level: 1 (1)
[[O]C([CH])[CH]:8.0]


ID=Reaction Center at Level: 2 (1)
[[O]C([CH])C(O[P])C([O])[CH]:8.0]



//
TRANSFORMATIONS
MMP Level 1
1: [O]C([CH])[CH]>>[O]C([CH])[CH]
2: [P]O[P]>>[P]O
3: [O]C([CH])[CH]>>[O]C([CH])[CH]
4: [O]P(=O)(O)O>>[O]P(=O)(O)O
5: [P]O>>[P]O[P]

MMP Level 2
1: [O]C([CH])C(O[P])C([O])[CH]>>[O]C([CH])C(O[P])C([O])[CH]
2: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
3: [O]C([CH])C(O[P])C([O])[CH]>>[O]C([CH])C(O[P])C([O])[CH]
4: O=P(O)(O)O[P]>>O=P(O)(O)O[P]
5: [O]P(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O

MMP Level 3
1: [O]C1[CH]C([O])C(O[P])C(OP(=O)(O)O)C1O[P]>>[O]C1[CH]C([O])C(O[P])C(OP(=O)(O)O)C1O[P]
2: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
3: [O]C1[CH]C([O])C(O[P])C(OP(=O)(O)O)C1O[P]>>[O]C1[CH]C([O])C(O[P])C(OP(=O)(O)O)C1O[P]
4: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O
5: O=P(O)(O)O[CH]>>O=P(O)(O)OP(=O)(O)O[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OC1C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)OP(=O)(O)O)C(OP(=O)(O)O)C1OP(=O)(O)O]
3: R:M00002, P:M00004	[O=P(O)(O)OC1C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)O)C1OP(=O)(O)O>>O=P(O)(O)OC1C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)OP(=O)(O)O)C(OP(=O)(O)O)C1OP(=O)(O)O, O=P(O)(O)OC1C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)O)C1OP(=O)(O)O>>O=P(O)(O)OC1C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)OP(=O)(O)O)C(OP(=O)(O)O)C1OP(=O)(O)O, O=P(O)(O)OC1C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)O)C1OP(=O)(O)O>>O=P(O)(O)OC1C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)OP(=O)(O)O)C(OP(=O)(O)O)C1OP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:34]=[P:33]([OH:35])([OH:36])[O:32][CH:31]1[CH:6]([O:5][P:2](=[O:1])([OH:3])[OH:4])[CH:7]([O:8][P:9](=[O:10])([OH:11])[OH:12])[CH:13]([O:14][P:15](=[O:16])([OH:17])[OH:18])[CH:19]([O:20][P:21](=[O:22])([OH:24])[OH:23])[CH:25]1[O:26][P:27](=[O:28])([OH:29])[OH:30].[O:37]=[P:38]([OH:39])([OH:40])[O:41][P:42](=[O:43])([OH:44])[O:45][P:46](=[O:47])([OH:48])[O:49][CH2:50][CH:51]1[O:52][CH:53]([N:54]:2:[CH:55]:[N:56]:[C:57]:3:[C:58](:[N:59]:[CH:60]:[N:61]:[C:62]32)[NH2:63])[CH:64]([OH:65])[CH:66]1[OH:67]>>[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH:6]1[CH:7]([O:8][P:9](=[O:10])([OH:11])[OH:12])[CH:13]([O:14][P:15](=[O:16])([OH:17])[OH:18])[CH:19]([O:20][P:21](=[O:22])([OH:23])[O:24][P:38](=[O:37])([OH:39])[OH:40])[CH:25]([O:26][P:27](=[O:28])([OH:29])[OH:30])[CH:31]1[O:32][P:33](=[O:34])([OH:35])[OH:36].[O:43]=[P:42]([OH:41])([OH:44])[O:45][P:46](=[O:47])([OH:48])[O:49][CH2:50][CH:51]1[O:52][CH:53]([N:54]:2:[CH:55]:[N:56]:[C:57]:3:[C:58](:[N:59]:[CH:60]:[N:61]:[C:62]32)[NH2:63])[CH:64]([OH:65])[CH:66]1[OH:67]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=60, 2=61, 3=62, 4=57, 5=58, 6=59, 7=63, 8=56, 9=55, 10=54, 11=53, 12=64, 13=66, 14=51, 15=52, 16=50, 17=49, 18=46, 19=47, 20=48, 21=45, 22=42, 23=43, 24=44, 25=41, 26=38, 27=37, 28=39, 29=40, 30=67, 31=65, 32=31, 33=25, 34=19, 35=13, 36=7, 37=6, 38=5, 39=2, 40=1, 41=3, 42=4, 43=8, 44=9, 45=10, 46=11, 47=12, 48=14, 49=15, 50=16, 51=17, 52=18, 53=20, 54=21, 55=22, 56=23, 57=24, 58=26, 59=27, 60=28, 61=29, 62=30, 63=32, 64=33, 65=34, 66=35, 67=36}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=60, 2=61, 3=62, 4=57, 5=58, 6=59, 7=63, 8=56, 9=55, 10=54, 11=53, 12=64, 13=66, 14=51, 15=52, 16=50, 17=49, 18=46, 19=47, 20=48, 21=45, 22=42, 23=41, 24=43, 25=44, 26=67, 27=65, 28=35, 29=29, 30=19, 31=13, 32=7, 33=6, 34=5, 35=2, 36=1, 37=3, 38=4, 39=8, 40=9, 41=10, 42=11, 43=12, 44=14, 45=15, 46=16, 47=17, 48=18, 49=20, 50=21, 51=22, 52=23, 53=24, 54=25, 55=26, 56=27, 57=28, 58=30, 59=31, 60=32, 61=33, 62=34, 63=36, 64=37, 65=38, 66=39, 67=40}

