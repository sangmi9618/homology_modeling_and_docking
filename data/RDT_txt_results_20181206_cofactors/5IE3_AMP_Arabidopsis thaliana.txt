
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0, C-S:1.0, O-P:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(O)C(=O)O:1.0, O=C(O)C(=O)O>>O=C(O)C(=O)SC[CH2]:1.0, O=C(O)C(=O)O>>O=C(O)C(=O)S[CH2]:1.0, O=C(O)C(=O)O>>O=P(O)(O)O[P]:1.0, O=C(O)C(=O)S[CH2]:1.0, O=C([NH])C(O)C([CH2])(C)C:2.0, O=C([NH])C(O)C([CH2])(C)C>>O=C([NH])C(O)C([CH2])(C)C:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]:1.0, O=P(O)(O[P])O[P]:1.0, O=P(O)(O[P])O[P]>>O=P(O)(O)O[P]:1.0, SC[CH2]:1.0, SC[CH2]>>[C]C(=O)SC[CH2]:1.0, S[CH2]:1.0, S[CH2]>>[C]S[CH2]:1.0, [CH]:2.0, [C]:2.0, [C]C(=O)O:2.0, [C]C(=O)O>>[C]C(=O)[S]:1.0, [C]C(=O)O>>[O]P(=O)(O)O:1.0, [C]C(=O)SC[CH2]:1.0, [C]C(=O)[S]:1.0, [C]C([C])O:2.0, [C]C([C])O>>[C]C([C])O:1.0, [C]O:1.0, [C]O>>[P]O:1.0, [C]S[CH2]:1.0, [NH]CCS>>O=C(O)C(=O)SCC[NH]:1.0, [OH]:3.0, [O]:1.0, [O]CC(C)(C)C(O)C(=O)N[CH2]>>[O]CC(C)(C)C(O)C(=O)N[CH2]:1.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)OP(=O)(O)OP(=O)(O)O>>O=P(O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [O]P([O])(=O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0, [SH]:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[C]:2.0, [OH]:2.0, [O]:1.0, [P]:2.0, [S]:1.0]


ID=Reaction Center at Level: 1 (8)
[[C]C(=O)O:1.0, [C]C(=O)[S]:1.0, [C]O:1.0, [C]S[CH2]:1.0, [O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (8)
[O=C(O)C(=O)O:1.0, O=C(O)C(=O)S[CH2]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[P]:1.0, [C]C(=O)O:1.0, [C]C(=O)SC[CH2]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (1)
[[C]C([C])O:2.0]


ID=Reaction Center at Level: 2 (1)
[O=C([NH])C(O)C([CH2])(C)C:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C([C])O>>[C]C([C])O
2: [C]C(=O)O>>[C]C(=O)[S]
3: [P]O[P]>>[P]O
4: [O]P([O])(=O)O>>[O]P(=O)(O)O
5: [C]O>>[P]O
6: S[CH2]>>[C]S[CH2]

MMP Level 2
1: O=C([NH])C(O)C([CH2])(C)C>>O=C([NH])C(O)C([CH2])(C)C
2: O=C(O)C(=O)O>>O=C(O)C(=O)S[CH2]
3: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
4: O=P(O)(O[P])O[P]>>O=P(O)(O)O[P]
5: [C]C(=O)O>>[O]P(=O)(O)O
6: SC[CH2]>>[C]C(=O)SC[CH2]

MMP Level 3
1: [O]CC(C)(C)C(O)C(=O)N[CH2]>>[O]CC(C)(C)C(O)C(=O)N[CH2]
2: O=C(O)C(=O)O>>O=C(O)C(=O)SC[CH2]
3: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]
4: [O]P(=O)(O)OP(=O)(O)OP(=O)(O)O>>O=P(O)(O)OP(=O)(O)O
5: O=C(O)C(=O)O>>O=P(O)(O)O[P]
6: [NH]CCS>>O=C(O)C(=O)SCC[NH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)O]
2: R:M00001, P:M00005	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
3: R:M00002, P:M00006	[O=C(NCCS)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O>>O=C(O)C(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O, O=C(NCCS)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O>>O=C(O)C(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O]
4: R:M00003, P:M00004	[O=C(O)C(=O)O>>O=P(O)(O)OP(=O)(O)O]
5: R:M00003, P:M00006	[O=C(O)C(=O)O>>O=C(O)C(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:80]=[C:81]([OH:82])[C:83](=[O:84])[OH:85].[O:1]=[C:2]([NH:3][CH2:4][CH2:5][SH:6])[CH2:7][CH2:8][NH:9][C:10](=[O:11])[CH:12]([OH:13])[C:14]([CH3:15])([CH3:16])[CH2:17][O:18][P:19](=[O:20])([OH:21])[O:22][P:23](=[O:24])([OH:25])[O:26][CH2:27][CH:28]1[O:29][CH:30]([N:31]:2:[CH:32]:[N:33]:[C:34]:3:[C:35](:[N:36]:[CH:37]:[N:38]:[C:39]32)[NH2:40])[CH:41]([OH:42])[CH:43]1[O:44][P:45](=[O:46])([OH:47])[OH:48].[O:49]=[P:50]([OH:51])([OH:52])[O:53][P:54](=[O:55])([OH:56])[O:57][P:58](=[O:59])([OH:60])[O:61][CH2:62][CH:63]1[O:64][CH:65]([N:66]:2:[CH:67]:[N:68]:[C:69]:3:[C:70](:[N:71]:[CH:72]:[N:73]:[C:74]32)[NH2:75])[CH:76]([OH:77])[CH:78]1[OH:79]>>[O:80]=[C:81]([OH:82])[C:83](=[O:84])[S:6][CH2:5][CH2:4][NH:3][C:2](=[O:1])[CH2:7][CH2:8][NH:9][C:10](=[O:11])[CH:12]([OH:13])[C:14]([CH3:15])([CH3:16])[CH2:17][O:18][P:19](=[O:20])([OH:21])[O:22][P:23](=[O:24])([OH:25])[O:26][CH2:27][CH:28]1[O:29][CH:30]([N:31]:2:[CH:32]:[N:33]:[C:34]:3:[C:35](:[N:36]:[CH:37]:[N:38]:[C:39]32)[NH2:40])[CH:41]([OH:42])[CH:43]1[O:44][P:45](=[O:46])([OH:47])[OH:48].[O:59]=[P:58]([OH:57])([OH:60])[O:61][CH2:62][CH:63]1[O:64][CH:65]([N:66]:2:[CH:67]:[N:68]:[C:69]:3:[C:70](:[N:71]:[CH:72]:[N:73]:[C:74]32)[NH2:75])[CH:76]([OH:77])[CH:78]1[OH:79].[O:49]=[P:50]([OH:51])([OH:52])[O:53][P:54](=[O:55])([OH:56])[OH:85]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=72, 2=73, 3=74, 4=69, 5=70, 6=71, 7=75, 8=68, 9=67, 10=66, 11=65, 12=76, 13=78, 14=63, 15=64, 16=62, 17=61, 18=58, 19=59, 20=60, 21=57, 22=54, 23=55, 24=56, 25=53, 26=50, 27=49, 28=51, 29=52, 30=79, 31=77, 32=15, 33=14, 34=16, 35=17, 36=18, 37=19, 38=20, 39=21, 40=22, 41=23, 42=24, 43=25, 44=26, 45=27, 46=28, 47=43, 48=41, 49=30, 50=29, 51=31, 52=32, 53=33, 54=34, 55=39, 56=38, 57=37, 58=36, 59=35, 60=40, 61=42, 62=44, 63=45, 64=46, 65=47, 66=48, 67=12, 68=10, 69=11, 70=9, 71=8, 72=7, 73=2, 74=1, 75=3, 76=4, 77=5, 78=6, 79=13, 80=81, 81=80, 82=83, 83=84, 84=85, 85=82}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=79, 2=78, 3=77, 4=80, 5=81, 6=82, 7=83, 8=84, 9=85, 10=69, 11=70, 12=71, 13=66, 14=67, 15=68, 16=72, 17=65, 18=64, 19=63, 20=62, 21=73, 22=75, 23=60, 24=61, 25=59, 26=58, 27=55, 28=54, 29=56, 30=57, 31=76, 32=74, 33=20, 34=19, 35=21, 36=22, 37=23, 38=24, 39=25, 40=26, 41=27, 42=28, 43=29, 44=30, 45=31, 46=32, 47=33, 48=48, 49=46, 50=35, 51=34, 52=36, 53=37, 54=38, 55=39, 56=44, 57=43, 58=42, 59=41, 60=40, 61=45, 62=47, 63=49, 64=50, 65=51, 66=52, 67=53, 68=17, 69=15, 70=16, 71=14, 72=13, 73=12, 74=10, 75=11, 76=9, 77=8, 78=7, 79=6, 80=4, 81=5, 82=2, 83=1, 84=3, 85=18}

