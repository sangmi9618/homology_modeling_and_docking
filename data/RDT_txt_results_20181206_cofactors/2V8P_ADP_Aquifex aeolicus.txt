
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)OC([CH])([CH2])C:1.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>[C]OP(=O)(O)O:1.0, OCC(O)(C)C(O)[CH2]>>O=P(O)(O)OC(C)(CO)C(O)[CH2]:1.0, [CH]C(O)([CH2])C:1.0, [CH]C(O)([CH2])C>>O=P(O)(O)OC([CH])([CH2])C:1.0, [C]O:1.0, [C]O>>[C]O[P]:1.0, [C]OP(=O)(O)O:1.0, [C]O[P]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC([CH])([CH2])C:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[C]O[P]:1.0, [O]P(=O)(O)O:2.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)OC([CH])([CH2])C:1.0, O=P(O)(O)O[P]:1.0, [C]OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[P]>>[P]O
2: [C]O>>[C]O[P]
3: [O]P(=O)(O)O>>[O]P(=O)(O)O

MMP Level 2
1: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
2: [CH]C(O)([CH2])C>>O=P(O)(O)OC([CH])([CH2])C
3: O=P(O)(O)O[P]>>[C]OP(=O)(O)O

MMP Level 3
1: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
2: OCC(O)(C)C(O)[CH2]>>O=P(O)(O)OC(C)(CO)C(O)[CH2]
3: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC([CH])([CH2])C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)OCC(O)C(O)(C)CO)C(O)C2O>>O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)OCC(O)C(OP(=O)(O)O)(C)CO)C(O)C2O]
2: R:M00002, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)OCC(O)C(OP(=O)(O)O)(C)CO)C(O)C2O]
3: R:M00002, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[CH:6]=[CH:7][N:8]1[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][CH2:22][CH:23]([OH:24])[C:25]([OH:26])([CH3:27])[CH2:28][OH:29])[CH:30]([OH:31])[CH:32]2[OH:33].[O:34]=[P:35]([OH:36])([OH:37])[O:38][P:39](=[O:40])([OH:41])[O:42][P:43](=[O:44])([OH:45])[O:46][CH2:47][CH:48]1[O:49][CH:50]([N:51]:2:[CH:52]:[N:53]:[C:54]:3:[C:55](:[N:56]:[CH:57]:[N:58]:[C:59]32)[NH2:60])[CH:61]([OH:62])[CH:63]1[OH:64]>>[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[CH:6]=[CH:7][N:8]1[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][CH2:22][CH:23]([OH:24])[C:25]([O:26][P:35](=[O:34])([OH:36])[OH:37])([CH3:27])[CH2:28][OH:29])[CH:30]([OH:31])[CH:32]2[OH:33].[O:40]=[P:39]([OH:38])([OH:41])[O:42][P:43](=[O:44])([OH:45])[O:46][CH2:47][CH:48]1[O:49][CH:50]([N:51]:2:[CH:52]:[N:53]:[C:54]:3:[C:55](:[N:56]:[CH:57]:[N:58]:[C:59]32)[NH2:60])[CH:61]([OH:62])[CH:63]1[OH:64]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=27, 2=25, 3=28, 4=29, 5=23, 6=22, 7=21, 8=18, 9=19, 10=20, 11=17, 12=14, 13=15, 14=16, 15=13, 16=12, 17=11, 18=30, 19=32, 20=9, 21=10, 22=8, 23=7, 24=6, 25=4, 26=3, 27=2, 28=1, 29=5, 30=33, 31=31, 32=24, 33=26, 34=57, 35=58, 36=59, 37=54, 38=55, 39=56, 40=60, 41=53, 42=52, 43=51, 44=50, 45=61, 46=63, 47=48, 48=49, 49=47, 50=46, 51=43, 52=44, 53=45, 54=42, 55=39, 56=40, 57=41, 58=38, 59=35, 60=34, 61=36, 62=37, 63=64, 64=62}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=31, 2=25, 3=32, 4=33, 5=23, 6=22, 7=21, 8=18, 9=19, 10=20, 11=17, 12=14, 13=15, 14=16, 15=13, 16=12, 17=11, 18=34, 19=36, 20=9, 21=10, 22=8, 23=7, 24=6, 25=4, 26=3, 27=2, 28=1, 29=5, 30=37, 31=35, 32=24, 33=26, 34=27, 35=28, 36=29, 37=30, 38=57, 39=58, 40=59, 41=54, 42=55, 43=56, 44=60, 45=53, 46=52, 47=51, 48=50, 49=61, 50=63, 51=48, 52=49, 53=47, 54=46, 55=43, 56=44, 57=45, 58=42, 59=39, 60=38, 61=40, 62=41, 63=64, 64=62}

