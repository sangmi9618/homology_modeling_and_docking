
//
FINGERPRINTS BC

ORDER_CHANGED
[C%C*C%C:1.0, C%N*C%N:2.0, C-C*C=C:1.0]

//
FINGERPRINTS RC
[O=C(S[CH2])CCC>>O=C(S[CH2])C=CC:1.0, O=C([S])C=CC:1.0, O=C([S])CCC:1.0, O=C([S])CCC>>O=C([S])C=CC:2.0, O=C1N=C2C(=N[C]=C([CH])N2C[CH])C(=O)N1>>O=c1[nH]c(=O)c2N[C]=C([CH])N(c2[nH]1)C[CH]:1.0, O=C1N[C]N=C2C1=NC([CH])=[C]N2[CH2]>>O=C1N[C]NC2=C1NC([CH])=[C]N2[CH2]:1.0, [CH2]:2.0, [CH2]CC:1.0, [CH2]CC>>[CH]=CC:1.0, [CH]:2.0, [CH]=CC:1.0, [C]:4.0, [C]=C([N])NC(=O)[NH]:1.0, [C]=C([N])[NH]:1.0, [C]=CC=1N=C(C(=[N])[N]C1[CH])C(=O)[NH]>>[C]=CC=1NC(=C([NH])[N]C1[CH])C(=O)[NH]:1.0, [C]C(=[C])NC(=[C])[CH]:1.0, [C]C(=[C])[NH]:1.0, [C]C([C])=NC(=[C])[CH]:1.0, [C]C([C])=NC(=[C])[CH]>>[C]C(=[C])NC(=[C])[CH]:1.0, [C]C([C])=[N]:1.0, [C]C([C])=[N]>>[C]C(=[C])[NH]:1.0, [C]C([N])=NC(=O)[NH]:1.0, [C]C([N])=NC(=O)[NH]>>[C]=C([N])NC(=O)[NH]:1.0, [C]C([N])=[N]:1.0, [C]C([N])=[N]>>[C]=C([N])[NH]:1.0, [C]C=CC:1.0, [C]C=[CH]:1.0, [C]CCC:1.0, [C]CCC>>[C]C=CC:1.0, [C]C[CH2]:1.0, [C]C[CH2]>>[C]C=[CH]:1.0, [C]N([CH2])C1=NC(=O)N[C]C1=[N]>>[C]N([CH2])C=1NC(=O)N[C]C1[NH]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]>>[C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]>>[C]NC(=C([N])[NH])C(=O)[NH]:1.0, [C]N=[C]:2.0, [C]N=[C]>>[C]N[C]:2.0, [C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]NC(=C([N])[NH])C(=O)[NH]:1.0, [C]N[C]:2.0, [NH]:2.0, [N]:2.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (5)
[[CH2]:2.0, [CH]:2.0, [C]:4.0, [NH]:2.0, [N]:2.0]


ID=Reaction Center at Level: 1 (10)
[[CH2]CC:1.0, [CH]=CC:1.0, [C]=C([N])[NH]:1.0, [C]C(=[C])[NH]:1.0, [C]C([C])=[N]:1.0, [C]C([N])=[N]:1.0, [C]C=[CH]:1.0, [C]C[CH2]:1.0, [C]N=[C]:2.0, [C]N[C]:2.0]


ID=Reaction Center at Level: 2 (12)
[O=C([S])C=CC:1.0, O=C([S])CCC:1.0, [C]=C([N])NC(=O)[NH]:1.0, [C]C(=[C])NC(=[C])[CH]:1.0, [C]C([C])=NC(=[C])[CH]:1.0, [C]C([N])=NC(=O)[NH]:1.0, [C]C=CC:1.0, [C]CCC:1.0, [C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]:1.0, [C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]NC(=C([N])[NH])C(=O)[NH]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH2]CC>>[CH]=CC
2: [C]N=[C]>>[C]N[C]
3: [C]N=[C]>>[C]N[C]
4: [C]C[CH2]>>[C]C=[CH]
5: [C]C([N])=[N]>>[C]=C([N])[NH]
6: [C]C([C])=[N]>>[C]C(=[C])[NH]

MMP Level 2
1: [C]CCC>>[C]C=CC
2: [C]C([C])=NC(=[C])[CH]>>[C]C(=[C])NC(=[C])[CH]
3: [C]C([N])=NC(=O)[NH]>>[C]=C([N])NC(=O)[NH]
4: O=C([S])CCC>>O=C([S])C=CC
5: [C]N=C(C([C])=[N])N([C])[CH2]>>[C]NC(=C([C])[NH])N([C])[CH2]
6: [C]N=C(C([N])=[N])C(=O)[NH]>>[C]NC(=C([N])[NH])C(=O)[NH]

MMP Level 3
1: O=C([S])CCC>>O=C([S])C=CC
2: [C]=CC=1N=C(C(=[N])[N]C1[CH])C(=O)[NH]>>[C]=CC=1NC(=C([NH])[N]C1[CH])C(=O)[NH]
3: [C]N([CH2])C1=NC(=O)N[C]C1=[N]>>[C]N([CH2])C=1NC(=O)N[C]C1[NH]
4: O=C(S[CH2])CCC>>O=C(S[CH2])C=CC
5: O=C1N=C2C(=N[C]=C([CH])N2C[CH])C(=O)N1>>O=c1[nH]c(=O)c2N[C]=C([CH])N(c2[nH]1)C[CH]
6: O=C1N[C]N=C2C1=NC([CH])=[C]N2[CH2]>>O=C1N[C]NC2=C1NC([CH])=[C]N2[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)CCC>>O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C=CC, O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)CCC>>O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C=CC]
2: R:M00002, P:M00004	[O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C, O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C, O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C, O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C]


//
SELECTED AAM MAPPING
[O:54]=[C:55]1[N:56]=[C:57]2[C:58](=[N:59][C:60]:3:[CH:61]:[C:62](:[C:63](:[CH:64]:[C:65]3[N:66]2[CH2:67][CH:68]([OH:69])[CH:70]([OH:71])[CH:72]([OH:73])[CH2:74][O:75][P:76](=[O:77])([OH:78])[O:79][P:80](=[O:81])([OH:82])[O:83][CH2:84][CH:85]4[O:86][CH:87]([N:88]:5:[CH:89]:[N:90]:[C:91]:6:[C:92](:[N:93]:[CH:94]:[N:95]:[C:96]65)[NH2:97])[CH:98]([OH:99])[CH:100]4[OH:101])[CH3:102])[CH3:103])[C:104](=[O:105])[NH:106]1.[O:1]=[C:2]([S:3][CH2:4][CH2:5][NH:6][C:7](=[O:8])[CH2:9][CH2:10][NH:11][C:12](=[O:13])[CH:14]([OH:15])[C:16]([CH3:17])([CH3:18])[CH2:19][O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[O:28][CH2:29][CH:30]1[O:31][CH:32]([N:33]:2:[CH:34]:[N:35]:[C:36]:3:[C:37](:[N:38]:[CH:39]:[N:40]:[C:41]32)[NH2:42])[CH:43]([OH:44])[CH:45]1[O:46][P:47](=[O:48])([OH:50])[OH:49])[CH2:51][CH2:52][CH3:53]>>[O:1]=[C:2]([S:3][CH2:4][CH2:5][NH:6][C:7](=[O:8])[CH2:9][CH2:10][NH:11][C:12](=[O:13])[CH:14]([OH:15])[C:16]([CH3:17])([CH3:18])[CH2:19][O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[O:28][CH2:29][CH:30]1[O:31][CH:32]([N:33]:2:[CH:34]:[N:35]:[C:36]:3:[C:37](:[N:38]:[CH:39]:[N:40]:[C:41]32)[NH2:42])[CH:43]([OH:44])[CH:45]1[O:46][P:47](=[O:48])([OH:49])[OH:50])[CH:51]=[CH:52][CH3:53].[O:54]=[C:55]1[NH:106][C:104](=[O:105])[C:58]=2[NH:59][C:60]:3:[CH:61]:[C:62](:[C:63](:[CH:64]:[C:65]3[N:66]([C:57]2[NH:56]1)[CH2:67][CH:68]([OH:69])[CH:70]([OH:71])[CH:72]([OH:73])[CH2:74][O:75][P:76](=[O:77])([OH:78])[O:79][P:80](=[O:81])([OH:82])[O:83][CH2:84][CH:85]4[O:86][CH:87]([N:88]:5:[CH:89]:[N:90]:[C:91]:6:[C:92](:[N:93]:[CH:94]:[N:95]:[C:96]65)[NH2:97])[CH:98]([OH:99])[CH:100]4[OH:101])[CH3:102])[CH3:103]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=53, 2=52, 3=51, 4=2, 5=1, 6=3, 7=4, 8=5, 9=6, 10=7, 11=8, 12=9, 13=10, 14=11, 15=12, 16=13, 17=14, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=25, 28=26, 29=27, 30=28, 31=29, 32=30, 33=45, 34=43, 35=32, 36=31, 37=33, 38=34, 39=35, 40=36, 41=41, 42=40, 43=39, 44=38, 45=37, 46=42, 47=44, 48=46, 49=47, 50=48, 51=49, 52=50, 53=15, 54=103, 55=62, 56=61, 57=60, 58=65, 59=64, 60=63, 61=102, 62=66, 63=57, 64=56, 65=55, 66=54, 67=106, 68=104, 69=105, 70=58, 71=59, 72=67, 73=68, 74=70, 75=72, 76=74, 77=75, 78=76, 79=77, 80=78, 81=79, 82=80, 83=81, 84=82, 85=83, 86=84, 87=85, 88=100, 89=98, 90=87, 91=86, 92=88, 93=89, 94=90, 95=91, 96=96, 97=95, 98=94, 99=93, 100=92, 101=97, 102=99, 103=101, 104=73, 105=71, 106=69}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=53, 2=52, 3=51, 4=2, 5=1, 6=3, 7=4, 8=5, 9=6, 10=7, 11=8, 12=9, 13=10, 14=11, 15=12, 16=13, 17=14, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=25, 28=26, 29=27, 30=28, 31=29, 32=30, 33=45, 34=43, 35=32, 36=31, 37=33, 38=34, 39=35, 40=36, 41=41, 42=40, 43=39, 44=38, 45=37, 46=42, 47=44, 48=46, 49=47, 50=48, 51=49, 52=50, 53=15, 54=106, 55=63, 56=62, 57=61, 58=66, 59=65, 60=64, 61=105, 62=67, 63=68, 64=59, 65=60, 66=57, 67=58, 68=56, 69=55, 70=54, 71=69, 72=70, 73=71, 74=73, 75=75, 76=77, 77=78, 78=79, 79=80, 80=81, 81=82, 82=83, 83=84, 84=85, 85=86, 86=87, 87=88, 88=103, 89=101, 90=90, 91=89, 92=91, 93=92, 94=93, 95=94, 96=99, 97=98, 98=97, 99=96, 100=95, 101=100, 102=102, 103=104, 104=76, 105=74, 106=72}

