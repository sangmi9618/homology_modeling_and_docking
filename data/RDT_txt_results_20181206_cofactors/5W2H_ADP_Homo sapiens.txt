
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O=P(O)(O)OC([CH])[CH]:1.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[CH]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O[CH]:1.0, [CH]:4.0, [CH]C([CH])O:3.0, [CH]C([CH])O>>O=P(O)(O)OC([CH])[CH]:1.0, [CH]C([CH])O>>[CH]C([CH])O:1.0, [CH]O:1.0, [CH]O>>[P]O[CH]:1.0, [OH]:2.0, [O]:2.0, [O]C([CH])C(O)C([CH])O:1.0, [O]C([CH])C(O)C([CH])O>>[O]C([CH])C(O)C([O])[CH]:1.0, [O]C([CH])C(O)C([CH])O>>[O]C([CH])C(OP(=O)(O)O)C([CH])O:1.0, [O]C([CH])C(O)C([O])[CH]:1.0, [O]C([CH])C(O[P])C([O])[CH]:2.0, [O]C([CH])C(O[P])C([O])[CH]>>[O]C([CH])C(O[P])C([O])[CH]:1.0, [O]C([CH])[CH]:2.0, [O]C([CH])[CH]>>[O]C([CH])[CH]:1.0, [O]C1[CH]C(O)C(O[P])C(OP(=O)(O)O)C1O[P]>>[O]C1[CH]C([O])C(O[P])C(OP(=O)(O)O)C1O[P]:1.0, [O]C1[CH]C([O])C(O[P])C(O)C1O>>[O]C1[CH]C([O])C(O[P])C(O)C1O[P]:1.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC([CH])[CH]:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[O]P(=O)(O)O:2.0, [P]O[CH]:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)OC([CH])[CH]:1.0, O=P(O)(O)O[CH]:1.0, O=P(O)(O)O[P]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:4.0]


ID=Reaction Center at Level: 1 (2)
[[CH]C([CH])O:2.0, [O]C([CH])[CH]:2.0]


ID=Reaction Center at Level: 2 (3)
[[O]C([CH])C(O)C([CH])O:1.0, [O]C([CH])C(O)C([O])[CH]:1.0, [O]C([CH])C(O[P])C([O])[CH]:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]C([CH])O>>[CH]C([CH])O
2: [O]P(=O)(O)O>>[O]P(=O)(O)O
3: [P]O[P]>>[P]O
4: [O]C([CH])[CH]>>[O]C([CH])[CH]
5: [CH]O>>[P]O[CH]

MMP Level 2
1: [O]C([CH])C(O)C([CH])O>>[O]C([CH])C(O)C([O])[CH]
2: O=P(O)(O)O[P]>>O=P(O)(O)O[CH]
3: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
4: [O]C([CH])C(O[P])C([O])[CH]>>[O]C([CH])C(O[P])C([O])[CH]
5: [CH]C([CH])O>>O=P(O)(O)OC([CH])[CH]

MMP Level 3
1: [O]C1[CH]C([O])C(O[P])C(O)C1O>>[O]C1[CH]C([O])C(O[P])C(O)C1O[P]
2: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC([CH])[CH]
3: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
4: [O]C1[CH]C(O)C(O[P])C(OP(=O)(O)O)C1O[P]>>[O]C1[CH]C([O])C(O[P])C(OP(=O)(O)O)C1O[P]
5: [O]C([CH])C(O)C([CH])O>>[O]C([CH])C(OP(=O)(O)O)C([CH])O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OC1C(O)C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)O)C1OP(=O)(O)O]
3: R:M00002, P:M00004	[O=P(O)(O)OC1C(O)C(O)C(OP(=O)(O)O)C(OP(=O)(O)O)C1OP(=O)(O)O>>O=P(O)(O)OC1C(O)C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)O)C1OP(=O)(O)O, O=P(O)(O)OC1C(O)C(O)C(OP(=O)(O)O)C(OP(=O)(O)O)C1OP(=O)(O)O>>O=P(O)(O)OC1C(O)C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)O)C1OP(=O)(O)O, O=P(O)(O)OC1C(O)C(O)C(OP(=O)(O)O)C(OP(=O)(O)O)C1OP(=O)(O)O>>O=P(O)(O)OC1C(O)C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)O)C1OP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:32]=[P:33]([OH:34])([OH:35])[O:36][CH:37]1[CH:38]([OH:39])[CH:40]([OH:41])[CH:42]([O:43][P:44](=[O:45])([OH:46])[OH:47])[CH:48]([O:49][P:50](=[O:51])([OH:52])[OH:53])[CH:54]1[O:55][P:56](=[O:57])([OH:58])[OH:59].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:1]=[P:2]([OH:3])([OH:4])[O:39][CH:38]1[CH:40]([OH:41])[CH:42]([O:43][P:44](=[O:45])([OH:46])[OH:47])[CH:48]([O:49][P:50](=[O:51])([OH:52])[OH:53])[CH:54]([O:55][P:56](=[O:57])([OH:58])[OH:59])[CH:37]1[O:36][P:33](=[O:32])([OH:35])[OH:34].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=38, 33=40, 34=42, 35=48, 36=54, 37=37, 38=36, 39=33, 40=32, 41=34, 42=35, 43=55, 44=56, 45=57, 46=58, 47=59, 48=49, 49=50, 50=51, 51=52, 52=53, 53=43, 54=44, 55=45, 56=46, 57=47, 58=41, 59=39}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=52, 2=53, 3=54, 4=49, 5=50, 6=51, 7=55, 8=48, 9=47, 10=46, 11=45, 12=56, 13=58, 14=43, 15=44, 16=42, 17=41, 18=38, 19=39, 20=40, 21=37, 22=34, 23=33, 24=35, 25=36, 26=59, 27=57, 28=9, 29=15, 30=21, 31=27, 32=6, 33=7, 34=8, 35=5, 36=2, 37=1, 38=3, 39=4, 40=28, 41=29, 42=30, 43=31, 44=32, 45=22, 46=23, 47=24, 48=25, 49=26, 50=16, 51=17, 52=18, 53=19, 54=20, 55=10, 56=11, 57=12, 58=13, 59=14}

