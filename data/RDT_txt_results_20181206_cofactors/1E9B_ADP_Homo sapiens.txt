
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:1.0]

//
FINGERPRINTS RC
[O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, [OH]:1.0, [O]:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:1.0, [P]O:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:1.0, [P]:1.0]


ID=Reaction Center at Level: 1 (2)
[[O]P(=O)(O)O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (2)
[O=P(O)(O)O[P]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[P]>>[P]O

MMP Level 2
1: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O

MMP Level 3
1: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]


//
REACTION MMP (RPAIR)
1: R:M00002, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]


//
SELECTED AAM MAPPING
[O:32]=[C:33]1[NH:34][C:35](=[O:36])[N:37]([CH:38]=[C:39]1[CH3:40])[CH:41]2[O:42][CH:43]([CH2:44][O:45][P:46](=[O:47])([OH:48])[OH:49])[CH:50]([OH:51])[CH2:52]2.[O:28]=[P:29]([OH:30])([OH:31])[O:1][P:2](=[O:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][CH2:10][CH:11]1[O:12][CH:13]([N:14]:2:[CH:15]:[N:16]:[C:17]:3:[C:18](:[N:19]:[CH:20]:[N:21]:[C:22]32)[NH2:23])[CH:24]([OH:25])[CH:26]1[OH:27]>>[O:3]=[P:2]([OH:1])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][CH2:10][CH:11]1[O:12][CH:13]([N:14]:2:[CH:15]:[N:16]:[C:17]:3:[C:18](:[N:19]:[CH:20]:[N:21]:[C:22]32)[NH2:23])[CH:24]([OH:25])[CH:26]1[OH:27]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=40, 2=39, 3=38, 4=37, 5=35, 6=36, 7=34, 8=33, 9=32, 10=41, 11=52, 12=50, 13=43, 14=42, 15=44, 16=45, 17=46, 18=47, 19=48, 20=49, 21=51, 22=24, 23=25, 24=26, 25=21, 26=22, 27=23, 28=27, 29=20, 30=19, 31=18, 32=17, 33=28, 34=30, 35=15, 36=16, 37=14, 38=13, 39=10, 40=11, 41=12, 42=9, 43=6, 44=7, 45=8, 46=5, 47=2, 48=1, 49=3, 50=4, 51=31, 52=29}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=17, 5=18, 6=19, 7=23, 8=16, 9=15, 10=14, 11=13, 12=24, 13=26, 14=11, 15=12, 16=10, 17=9, 18=6, 19=7, 20=8, 21=5, 22=2, 23=1, 24=3, 25=4, 26=27, 27=25}

