
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-O:1.0, S-S:1.0]

//
FINGERPRINTS RC
[O:3.0, S[CH2]:1.0, S[CH2]>>[S]S[CH2]:1.0, [CH]C(=[CH])C(OO)(C)C>>[CH]C(=[CH])C(O)(C)C:1.0, [CH]CS:1.0, [CH]CS>>[CH]CSS[CH2]:1.0, [CH]CSS[CH2]:1.0, [C]C(O)(C)C:1.0, [C]C(OO)(C)C:1.0, [C]C(OO)(C)C>>O:1.0, [C]C(OO)(C)C>>[C]C(O)(C)C:1.0, [C]C([NH])CS>>[C]C([NH])CSSC[CH]:1.0, [C]O:1.0, [C]OO:2.0, [C]OO>>O:1.0, [C]OO>>[C]O:1.0, [OH]:2.0, [O]:1.0, [O]O:1.0, [O]O>>O:1.0, [SH]:1.0, [S]:1.0, [S]S[CH2]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [OH]:1.0, [O]:1.0, [S]:2.0]


ID=Reaction Center at Level: 1 (4)
[O:1.0, [C]OO:1.0, [O]O:1.0, [S]S[CH2]:2.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, [CH]CSS[CH2]:2.0, [C]C(OO)(C)C:1.0, [C]OO:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]OO>>[C]O
2: [O]O>>O
3: S[CH2]>>[S]S[CH2]

MMP Level 2
1: [C]C(OO)(C)C>>[C]C(O)(C)C
2: [C]OO>>O
3: [CH]CS>>[CH]CSS[CH2]

MMP Level 3
1: [CH]C(=[CH])C(OO)(C)C>>[CH]C(=[CH])C(O)(C)C
2: [C]C(OO)(C)C>>O
3: [C]C([NH])CS>>[C]C([NH])CSSC[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[OOC(c1ccccc1)(C)C>>OC(c1ccccc1)(C)C]
2: R:M00001, P:M00005	[OOC(c1ccccc1)(C)C>>O]
3: R:M00002, P:M00004	[O=C(O)CNC(=O)C(NC(=O)CCC(N)C(=O)O)CS>>O=C(O)CNC(=O)C(NC(=O)CCC(N)C(=O)O)CSSCC(NC(=O)CCC(N)C(=O)O)C(=O)NCC(=O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH2:4][NH:5][C:6](=[O:7])[CH:8]([NH:9][C:10](=[O:11])[CH2:12][CH2:13][CH:14]([NH2:15])[C:16](=[O:17])[OH:18])[CH2:19][SH:20].[OH:21][O:22][C:23]([C:24]:1:[CH:25]:[CH:26]:[CH:27]:[CH:28]:[CH:29]1)([CH3:30])[CH3:31]>>[O:50]=[C:49]([OH:51])[CH2:48][NH:47][C:45](=[O:46])[CH:34]([NH:35][C:36](=[O:37])[CH2:38][CH2:39][CH:40]([NH2:41])[C:42](=[O:43])[OH:44])[CH2:33][S:32][S:20][CH2:19][CH:8]([NH:9][C:10](=[O:11])[CH2:12][CH2:13][CH:14]([NH2:15])[C:16](=[O:17])[OH:18])[C:6](=[O:7])[NH:5][CH2:4][C:2](=[O:1])[OH:3].[OH2:21].[OH:22][C:23]([C:24]:1:[CH:25]:[CH:26]:[CH:27]:[CH:28]:[CH:29]1)([CH3:30])[CH3:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=30, 2=23, 3=31, 4=24, 5=25, 6=26, 7=27, 8=28, 9=29, 10=22, 11=21, 12=13, 13=12, 14=10, 15=11, 16=9, 17=8, 18=19, 19=20, 20=6, 21=7, 22=5, 23=4, 24=2, 25=1, 26=3, 27=14, 28=16, 29=17, 30=18, 31=15}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=49, 2=42, 3=50, 4=43, 5=44, 6=45, 7=46, 8=47, 9=48, 10=41, 11=28, 12=27, 13=25, 14=26, 15=24, 16=23, 17=22, 18=21, 19=20, 20=19, 21=8, 22=6, 23=7, 24=5, 25=4, 26=2, 27=1, 28=3, 29=9, 30=10, 31=11, 32=12, 33=13, 34=14, 35=16, 36=17, 37=18, 38=15, 39=34, 40=35, 41=36, 42=37, 43=38, 44=39, 45=40, 46=29, 47=31, 48=32, 49=33, 50=30, 51=51}

