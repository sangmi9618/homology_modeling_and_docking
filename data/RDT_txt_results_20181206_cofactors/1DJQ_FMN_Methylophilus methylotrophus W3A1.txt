
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C=O:1.0]

ORDER_CHANGED
[C%C*C%C:1.0, C%N*C%N:2.0]

//
FINGERPRINTS RC
[N(C)(C)C:3.0, N(C)(C)C>>N(C)C:3.0, N(C)(C)C>>O=C:2.0, N(C)C:2.0, O:3.0, O=C:4.0, O=C1N=C2C(=N[C]=C([CH])N2C[CH])C(=O)N1>>O=c1[nH]c(=O)c2N[C]=C([CH])N(c2[nH]1)C[CH]:1.0, O=C1N[C]N=C2C1=NC([CH])=[C]N2[CH2]>>O=C1N[C]NC2=C1NC([CH])=[C]N2[CH2]:1.0, O>>O=C:3.0, [CH2]:1.0, [CH3]:1.0, [C]:4.0, [C]=C([N])NC(=O)[NH]:1.0, [C]=C([N])[NH]:1.0, [C]=CC=1N=C(C(=[N])[N]C1[CH])C(=O)[NH]>>[C]=CC=1NC(=C([NH])[N]C1[CH])C(=O)[NH]:1.0, [C]C(=[C])NC(=[C])[CH]:1.0, [C]C(=[C])[NH]:1.0, [C]C([C])=NC(=[C])[CH]:1.0, [C]C([C])=NC(=[C])[CH]>>[C]C(=[C])NC(=[C])[CH]:1.0, [C]C([C])=[N]:1.0, [C]C([C])=[N]>>[C]C(=[C])[NH]:1.0, [C]C([N])=NC(=O)[NH]:1.0, [C]C([N])=NC(=O)[NH]>>[C]=C([N])NC(=O)[NH]:1.0, [C]C([N])=[N]:1.0, [C]C([N])=[N]>>[C]=C([N])[NH]:1.0, [C]N([CH2])C1=NC(=O)N[C]C1=[N]>>[C]N([CH2])C=1NC(=O)N[C]C1[NH]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]>>[C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]>>[C]NC(=C([N])[NH])C(=O)[NH]:1.0, [C]N=[C]:2.0, [C]N=[C]>>[C]N[C]:2.0, [C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]NC(=C([N])[NH])C(=O)[NH]:1.0, [C]N[C]:2.0, [NH]:3.0, [N]:3.0, [N]C:1.0, [N]C>>O=C:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[O:1.0, [CH2]:1.0, [CH3]:1.0, [N]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[N(C)(C)C:1.0, O:1.0, O=C:2.0, [N]C:1.0]


ID=Reaction Center at Level: 2 (3)
[N(C)(C)C:2.0, O:1.0, O=C:2.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:4.0, [NH]:2.0, [N]:2.0]


ID=Reaction Center at Level: 1 (6)
[[C]=C([N])[NH]:1.0, [C]C(=[C])[NH]:1.0, [C]C([C])=[N]:1.0, [C]C([N])=[N]:1.0, [C]N=[C]:2.0, [C]N[C]:2.0]


ID=Reaction Center at Level: 2 (8)
[[C]=C([N])NC(=O)[NH]:1.0, [C]C(=[C])NC(=[C])[CH]:1.0, [C]C([C])=NC(=[C])[CH]:1.0, [C]C([N])=NC(=O)[NH]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]:1.0, [C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]NC(=C([N])[NH])C(=O)[NH]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: N(C)(C)C>>N(C)C
2: [C]C([N])=[N]>>[C]=C([N])[NH]
3: [C]C([C])=[N]>>[C]C(=[C])[NH]
4: [C]N=[C]>>[C]N[C]
5: [C]N=[C]>>[C]N[C]
6: O>>O=C
7: [N]C>>O=C

MMP Level 2
1: N(C)(C)C>>N(C)C
2: [C]N=C(C([C])=[N])N([C])[CH2]>>[C]NC(=C([C])[NH])N([C])[CH2]
3: [C]N=C(C([N])=[N])C(=O)[NH]>>[C]NC(=C([N])[NH])C(=O)[NH]
4: [C]C([C])=NC(=[C])[CH]>>[C]C(=[C])NC(=[C])[CH]
5: [C]C([N])=NC(=O)[NH]>>[C]=C([N])NC(=O)[NH]
6: O>>O=C
7: N(C)(C)C>>O=C

MMP Level 3
1: N(C)(C)C>>N(C)C
2: O=C1N=C2C(=N[C]=C([CH])N2C[CH])C(=O)N1>>O=c1[nH]c(=O)c2N[C]=C([CH])N(c2[nH]1)C[CH]
3: O=C1N[C]N=C2C1=NC([CH])=[C]N2[CH2]>>O=C1N[C]NC2=C1NC([CH])=[C]N2[CH2]
4: [C]=CC=1N=C(C(=[N])[N]C1[CH])C(=O)[NH]>>[C]=CC=1NC(=C([NH])[N]C1[CH])C(=O)[NH]
5: [C]N([CH2])C1=NC(=O)N[C]C1=[N]>>[C]N([CH2])C=1NC(=O)N[C]C1[NH]
6: O>>O=C
7: N(C)(C)C>>O=C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[N(C)(C)C>>N(C)C]
2: R:M00001, P:M00005	[N(C)(C)C>>O=C]
3: R:M00002, P:M00005	[O>>O=C]
4: R:M00003, P:M00006	[O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)O)C)C, O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)O)C)C, O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)O)C)C, O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)O)C)C]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[C:4]2[C:5](=[N:6][C:7]:3:[CH:8]:[C:9](:[C:10](:[CH:11]:[C:12]3[N:13]2[CH2:14][CH:15]([OH:16])[CH:17]([OH:18])[CH:19]([OH:20])[CH2:21][O:22][P:23](=[O:24])([OH:25])[OH:26])[CH3:27])[CH3:28])[C:29](=[O:30])[NH:31]1.[OH2:36].[N:32]([CH3:33])([CH3:34])[CH3:35]>>[O:36]=[CH2:35].[O:1]=[C:2]1[NH:31][C:29](=[O:30])[C:5]=2[NH:6][C:7]:3:[CH:8]:[C:9](:[C:10](:[CH:11]:[C:12]3[N:13]([C:4]2[NH:3]1)[CH2:14][CH:15]([OH:16])[CH:17]([OH:18])[CH:19]([OH:20])[CH2:21][O:22][P:23](=[O:24])([OH:26])[OH:25])[CH3:27])[CH3:28].[NH:32]([CH3:33])[CH3:34]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=33, 2=32, 3=34, 4=35, 5=36, 6=28, 7=9, 8=8, 9=7, 10=12, 11=11, 12=10, 13=27, 14=13, 15=4, 16=3, 17=2, 18=1, 19=31, 20=29, 21=30, 22=5, 23=6, 24=14, 25=15, 26=17, 27=19, 28=21, 29=22, 30=23, 31=24, 32=25, 33=26, 34=20, 35=18, 36=16}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=33, 2=32, 3=34, 4=36, 5=35, 6=31, 7=10, 8=9, 9=8, 10=13, 11=12, 12=11, 13=30, 14=14, 15=15, 16=6, 17=7, 18=4, 19=5, 20=3, 21=2, 22=1, 23=16, 24=17, 25=18, 26=20, 27=22, 28=24, 29=25, 30=26, 31=27, 32=28, 33=29, 34=23, 35=21, 36=19}

