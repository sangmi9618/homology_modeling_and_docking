
//
FINGERPRINTS BC

ORDER_CHANGED
[C-C*C=C:1.0]

//
FINGERPRINTS RC
[O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1:1.0, O=C(S[CH2])C=CC[CH2]>>O=C(S[CH2])CCC[CH2]:1.0, O=C([S])C=CCC[CH2]>>O=C([S])CCCC[CH2]:1.0, O=C([S])C=C[CH2]:1.0, O=C([S])C=C[CH2]>>O=C([S])CC[CH2]:1.0, O=C([S])CC[CH2]:1.0, [CH2]:3.0, [CH2]C[CH2]:1.0, [CH]:3.0, [CH]=C[CH2]:1.0, [CH]=C[CH2]>>[CH2]C[CH2]:1.0, [CH]N([CH])[CH]:1.0, [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]:1.0, [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O:1.0, [C]C=CC[CH2]:1.0, [C]C=CC[CH2]>>[C]CCC[CH2]:1.0, [C]C=[CH]:2.0, [C]C=[CH]>>[C]C[CH2]:1.0, [C]CCC[CH2]:1.0, [C]C[CH2]:1.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C=[CH]:1.0, [N+]:1.0, [N]:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (2)
[[CH2]:2.0, [CH]:2.0]


ID=Reaction Center at Level: 1 (4)
[[CH2]C[CH2]:1.0, [CH]=C[CH2]:1.0, [C]C=[CH]:1.0, [C]C[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C([S])C=C[CH2]:1.0, O=C([S])CC[CH2]:1.0, [C]C=CC[CH2]:1.0, [C]CCC[CH2]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C=[CH]>>[C]C[CH2]
2: [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]
3: [C]C[CH]>>[C]C=[CH]
4: [CH]=C[CH2]>>[CH2]C[CH2]

MMP Level 2
1: O=C([S])C=C[CH2]>>O=C([S])CC[CH2]
2: [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]
3: [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]
4: [C]C=CC[CH2]>>[C]CCC[CH2]

MMP Level 3
1: O=C(S[CH2])C=CC[CH2]>>O=C(S[CH2])CCC[CH2]
2: [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O
3: O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1
4: O=C([S])C=CCC[CH2]>>O=C([S])CCCC[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C(SCCNC(=O)C)C=CCCCCC>>O=C(SCCNC(=O)C)CCCCCCC, O=C(SCCNC(=O)C)C=CCCCCC>>O=C(SCCNC(=O)C)CCCCCCC]
2: R:M00002, P:M00005	[O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O]


//
SELECTED AAM MAPPING
[H+:65].[O:49]=[C:50]([S:51][CH2:52][CH2:53][NH:54][C:55](=[O:56])[CH3:57])[CH:58]=[CH:59][CH2:60][CH2:61][CH2:62][CH2:63][CH3:64].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:5][N:6]([CH:7]=[CH:8][CH2:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48]>>[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:9]:[CH:8]:[CH:7]:[N+:6](:[CH:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48].[O:49]=[C:50]([S:51][CH2:52][CH2:53][NH:54][C:55](=[O:56])[CH3:57])[CH2:58][CH2:59][CH2:60][CH2:61][CH2:62][CH2:63][CH3:64]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=64, 2=63, 3=62, 4=61, 5=60, 6=59, 7=58, 8=50, 9=49, 10=51, 11=52, 12=53, 13=54, 14=55, 15=56, 16=57, 17=9, 18=8, 19=7, 20=6, 21=5, 22=4, 23=2, 24=1, 25=3, 26=10, 27=47, 28=45, 29=12, 30=11, 31=13, 32=14, 33=15, 34=16, 35=17, 36=18, 37=19, 38=20, 39=21, 40=22, 41=23, 42=24, 43=43, 44=37, 45=26, 46=25, 47=27, 48=28, 49=29, 50=30, 51=35, 52=34, 53=33, 54=32, 55=31, 56=36, 57=38, 58=39, 59=40, 60=41, 61=42, 62=44, 63=46, 64=48, 65=65}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=64, 2=63, 3=62, 4=61, 5=60, 6=59, 7=58, 8=50, 9=49, 10=51, 11=52, 12=53, 13=54, 14=55, 15=56, 16=57, 17=6, 18=5, 19=4, 20=9, 21=8, 22=7, 23=10, 24=47, 25=45, 26=12, 27=11, 28=13, 29=14, 30=15, 31=16, 32=17, 33=18, 34=19, 35=20, 36=21, 37=22, 38=23, 39=24, 40=43, 41=37, 42=26, 43=25, 44=27, 45=28, 46=29, 47=30, 48=35, 49=34, 50=33, 51=32, 52=31, 53=36, 54=38, 55=39, 56=40, 57=41, 58=42, 59=44, 60=46, 61=48, 62=2, 63=1, 64=3}

