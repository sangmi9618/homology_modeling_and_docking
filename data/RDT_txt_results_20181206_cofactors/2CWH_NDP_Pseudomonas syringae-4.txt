
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[NC:2.0, NC>>[CH]NC:1.0, NC>>[C]C([CH2])NC:1.0, NC>>[C]CC(NC)C(=O)O:1.0, O:3.0, O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1:1.0, O=C(O)C(=O)CC([CH])=[CH]>>O=C(O)C(NC)CC([CH])=[CH]:1.0, [CH2]:1.0, [CH]:2.0, [CH]N([CH])[CH]:1.0, [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]:1.0, [CH]NC:1.0, [CH][N+]([CH])=[CH]:1.0, [C]:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=O:1.0, [C]=O>>O:1.0, [C]C(=O)[CH2]:2.0, [C]C(=O)[CH2]>>O:1.0, [C]C(=O)[CH2]>>[C]C([NH])[CH2]:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]:1.0, [C]C([CH2])NC:1.0, [C]C([NH])[CH2]:1.0, [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O:1.0, [C]C=[CH]:1.0, [C]CC(=O)C(=O)O:1.0, [C]CC(=O)C(=O)O>>O:1.0, [C]CC(=O)C(=O)O>>[C]CC(NC)C(=O)O:1.0, [C]CC(NC)C(=O)O:1.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C=[CH]:1.0, [N+]:1.0, [NH2]:1.0, [NH]:1.0, [N]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[O:1.0, [CH]:1.0, [C]:1.0, [NH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, [CH]NC:1.0, [C]=O:1.0, [C]C(=O)[CH2]:1.0, [C]C([NH])[CH2]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, [C]C(=O)[CH2]:1.0, [C]C([CH2])NC:1.0, [C]CC(=O)C(=O)O:1.0, [C]CC(NC)C(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(=O)[CH2]:1.0, [C]C([NH])[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[[C]CC(=O)C(=O)O:1.0, [C]CC(NC)C(=O)O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C(=O)[CH2]>>[C]C([NH])[CH2]
2: [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]
3: NC>>[CH]NC
4: [C]C[CH]>>[C]C=[CH]
5: [C]=O>>O

MMP Level 2
1: [C]CC(=O)C(=O)O>>[C]CC(NC)C(=O)O
2: [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]
3: NC>>[C]C([CH2])NC
4: [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]
5: [C]C(=O)[CH2]>>O

MMP Level 3
1: O=C(O)C(=O)CC([CH])=[CH]>>O=C(O)C(NC)CC([CH])=[CH]
2: [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O
3: NC>>[C]CC(NC)C(=O)O
4: O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1
5: [C]CC(=O)C(=O)O>>O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00006	[O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O]
2: R:M00002, P:M00004	[O=C(O)C(=O)Cc1ccccc1>>O]
3: R:M00002, P:M00005	[O=C(O)C(=O)Cc1ccccc1>>O=C(O)C(NC)Cc1ccccc1]
4: R:M00003, P:M00005	[NC>>O=C(O)C(NC)Cc1ccccc1]


//
SELECTED AAM MAPPING
[O:49]=[C:50]([OH:51])[C:52](=[O:53])[CH2:54][C:55]:1:[CH:60]:[CH:59]:[CH:58]:[CH:57]:[CH:56]1.[O:1]=[C:2]([NH2:3])[C:4]1=[CH:5][N:6]([CH:7]=[CH:8][CH2:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48].[NH2:61][CH3:62]>>[O:49]=[C:50]([OH:51])[CH:52]([NH:61][CH3:62])[CH2:54][C:55]:1:[CH:56]:[CH:57]:[CH:58]:[CH:59]:[CH:60]1.[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:9]:[CH:8]:[CH:7]:[N+:6](:[CH:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48].[OH2:53]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=5, 6=4, 7=2, 8=1, 9=3, 10=10, 11=47, 12=45, 13=12, 14=11, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=43, 28=37, 29=26, 30=25, 31=27, 32=28, 33=29, 34=30, 35=35, 36=34, 37=33, 38=32, 39=31, 40=36, 41=38, 42=39, 43=40, 44=41, 45=42, 46=44, 47=46, 48=48, 49=58, 50=57, 51=56, 52=55, 53=60, 54=59, 55=54, 56=52, 57=53, 58=50, 59=49, 60=51, 61=62, 62=61}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=62, 2=54, 3=53, 4=52, 5=55, 6=56, 7=57, 8=58, 9=59, 10=60, 11=61, 12=50, 13=49, 14=51, 15=6, 16=5, 17=4, 18=9, 19=8, 20=7, 21=10, 22=47, 23=45, 24=12, 25=11, 26=13, 27=14, 28=15, 29=16, 30=17, 31=18, 32=19, 33=20, 34=21, 35=22, 36=23, 37=24, 38=43, 39=37, 40=26, 41=25, 42=27, 43=28, 44=29, 45=30, 46=35, 47=34, 48=33, 49=32, 50=31, 51=36, 52=38, 53=39, 54=40, 55=41, 56=42, 57=44, 58=46, 59=48, 60=2, 61=1, 62=3}

