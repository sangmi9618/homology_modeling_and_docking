
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-S:2.0]

ORDER_CHANGED
[C-S*C=S:1.0]

//
FINGERPRINTS RC
[S([CH2])[CH2]:1.0, S=c1nc[nH]c2nc[nH]c12>>n1cnc(SC)c2[nH]cnc12:1.0, [CH2][S+]([CH2])C:2.0, [CH2][S+]([CH2])C>>S([CH2])[CH2]:1.0, [CH2][S+]([CH2])C>>[C]SC:1.0, [CH3]:2.0, [CH]CSC[CH2]:1.0, [CH]C[S+](C)C[CH2]:1.0, [CH]C[S+](C)C[CH2]>>[CH]CSC[CH2]:1.0, [CH]C[S+](C)C[CH2]>>[C]C(=[N])SC:1.0, [C]:2.0, [C]1N=CNc2nc[nH]c12>>[C]1=NC=Nc2nc[nH]c12:1.0, [C]=C([NH])C(=N[CH])SC:1.0, [C]=C([NH])C(=S)N=[CH]:1.0, [C]=C([NH])C(=S)N=[CH]>>[C]=C([NH])C(=N[CH])SC:2.0, [C]=C([N])N=C[N]:1.0, [C]=C([N])NC=[N]:1.0, [C]=C([N])NC=[N]>>[C]=C([N])N=C[N]:1.0, [C]=S:1.0, [C]=S>>[C]SC:1.0, [C]C(=[N])SC:1.0, [C]C(=[N])[S]:1.0, [C]C([N])=S:2.0, [C]C([N])=S>>[C]C(=[N])SC:1.0, [C]C([N])=S>>[C]C(=[N])[S]:1.0, [C]N=[CH]:1.0, [C]N[CH]:1.0, [C]N[CH]>>[C]N=[CH]:1.0, [C]SC:2.0, [NH]:1.0, [N]:1.0, [O]C([CH])C[S+](C)CC[CH]>>[O]C([CH])CSCC[CH]:1.0, [S+]:1.0, [S+]C:1.0, [S+]C>>[S]C:1.0, [S]:3.0, [S]C:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH3]:2.0, [S+]:1.0, [S]:1.0]


ID=Reaction Center at Level: 1 (4)
[[CH2][S+]([CH2])C:1.0, [C]SC:1.0, [S+]C:1.0, [S]C:1.0]


ID=Reaction Center at Level: 2 (4)
[[CH2][S+]([CH2])C:1.0, [CH]C[S+](C)C[CH2]:1.0, [C]C(=[N])SC:1.0, [C]SC:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (2)
[[C]:2.0, [S]:2.0]


ID=Reaction Center at Level: 1 (4)
[[C]=S:1.0, [C]C(=[N])[S]:1.0, [C]C([N])=S:1.0, [C]SC:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]=C([NH])C(=N[CH])SC:1.0, [C]=C([NH])C(=S)N=[CH]:1.0, [C]C(=[N])SC:1.0, [C]C([N])=S:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]=S>>[C]SC
2: [S+]C>>[S]C
3: [C]N[CH]>>[C]N=[CH]
4: [C]C([N])=S>>[C]C(=[N])[S]
5: [CH2][S+]([CH2])C>>S([CH2])[CH2]

MMP Level 2
1: [C]C([N])=S>>[C]C(=[N])SC
2: [CH2][S+]([CH2])C>>[C]SC
3: [C]=C([N])NC=[N]>>[C]=C([N])N=C[N]
4: [C]=C([NH])C(=S)N=[CH]>>[C]=C([NH])C(=N[CH])SC
5: [CH]C[S+](C)C[CH2]>>[CH]CSC[CH2]

MMP Level 3
1: [C]=C([NH])C(=S)N=[CH]>>[C]=C([NH])C(=N[CH])SC
2: [CH]C[S+](C)C[CH2]>>[C]C(=[N])SC
3: [C]1N=CNc2nc[nH]c12>>[C]1=NC=Nc2nc[nH]c12
4: S=c1nc[nH]c2nc[nH]c12>>n1cnc(SC)c2[nH]cnc12
5: [O]C([CH])C[S+](C)CC[CH]>>[O]C([CH])CSCC[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(O)C(N)CCSCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>n1cnc(SC)c2[nH]cnc12]
3: R:M00002, P:M00004	[S=c1nc[nH]c2nc[nH]c12>>n1cnc(SC)c2[nH]cnc12, S=c1nc[nH]c2nc[nH]c12>>n1cnc(SC)c2[nH]cnc12, S=c1nc[nH]c2nc[nH]c12>>n1cnc(SC)c2[nH]cnc12]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][S+:8]([CH3:9])[CH2:10][CH:11]1[O:12][CH:13]([N:14]:2:[CH:15]:[N:16]:[C:17]:3:[C:18](:[N:19]:[CH:20]:[N:21]:[C:22]32)[NH2:23])[CH:24]([OH:25])[CH:26]1[OH:27].[S:28]=[C:29]1[N:30]=[CH:31][NH:32][C:33]:2:[N:34]:[CH:35]:[NH:36]:[C:37]12>>[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][S:8][CH2:10][CH:11]1[O:12][CH:13]([N:14]:2:[CH:15]:[N:16]:[C:17]:3:[C:18](:[N:19]:[CH:20]:[N:21]:[C:22]32)[NH2:23])[CH:24]([OH:25])[CH:26]1[OH:27].[N:32]:1:[CH:31]:[N:30]:[C:29]([S:28][CH3:9]):[C:37]:2:[NH:36]:[CH:35]:[N:34]:[C:33]12


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=4, 6=2, 7=1, 8=3, 9=5, 10=10, 11=11, 12=26, 13=24, 14=13, 15=12, 16=14, 17=15, 18=16, 19=17, 20=22, 21=21, 22=20, 23=19, 24=18, 25=23, 26=25, 27=27, 28=35, 29=34, 30=33, 31=37, 32=36, 33=29, 34=28, 35=30, 36=31, 37=32}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=19, 2=20, 3=21, 4=16, 5=17, 6=18, 7=22, 8=15, 9=14, 10=13, 11=12, 12=23, 13=25, 14=10, 15=11, 16=9, 17=8, 18=7, 19=6, 20=4, 21=2, 22=1, 23=3, 24=5, 25=26, 26=24, 27=32, 28=31, 29=30, 30=29, 31=28, 32=27, 33=37, 34=33, 35=34, 36=35, 37=36}

