
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0, O-P:2.0]

//
FINGERPRINTS RC
[O=C(O)C(O)C([CH2])(C)C>>O=C(NC[CH2])C(O)C([CH2])(C)C:1.0, O=C([CH])NC[CH2]:1.0, O=C([CH])O:2.0, O=C([CH])O>>O=C([CH])[NH]:1.0, O=C([CH])O>>[O]P(=O)(O)O:1.0, O=C([CH])[NH]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]:1.0, O=P(O)(O[P])O[P]:1.0, O=P(O)(O[P])O[P]>>O=P(O)(O)O[P]:1.0, [CH2]CN:1.0, [CH2]CN>>O=C([CH])NC[CH2]:1.0, [CH2]N:1.0, [CH2]N>>[C]N[CH2]:1.0, [C]:2.0, [C]C(O)C(=O)N[CH2]:1.0, [C]C(O)C(=O)O:1.0, [C]C(O)C(=O)O>>O=P(O)(O)O[P]:1.0, [C]C(O)C(=O)O>>[C]C(O)C(=O)N[CH2]:1.0, [C]CCN>>[C]CCNC(=O)C([C])O:1.0, [C]N[CH2]:1.0, [C]O:1.0, [C]O>>[P]O:1.0, [NH2]:1.0, [NH]:1.0, [OH]:3.0, [O]:1.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)OP(=O)(O)OP(=O)(O)O>>O=P(O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [O]P([O])(=O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[C]:2.0, [NH]:1.0, [OH]:2.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (8)
[O=C([CH])O:1.0, O=C([CH])[NH]:1.0, [C]N[CH2]:1.0, [C]O:1.0, [O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (8)
[O=C([CH])NC[CH2]:1.0, O=C([CH])O:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[P]:1.0, [C]C(O)C(=O)N[CH2]:1.0, [C]C(O)C(=O)O:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=C([CH])O>>O=C([CH])[NH]
2: [O]P([O])(=O)O>>[O]P(=O)(O)O
3: [P]O[P]>>[P]O
4: [C]O>>[P]O
5: [CH2]N>>[C]N[CH2]

MMP Level 2
1: [C]C(O)C(=O)O>>[C]C(O)C(=O)N[CH2]
2: O=P(O)(O[P])O[P]>>O=P(O)(O)O[P]
3: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
4: O=C([CH])O>>[O]P(=O)(O)O
5: [CH2]CN>>O=C([CH])NC[CH2]

MMP Level 3
1: O=C(O)C(O)C([CH2])(C)C>>O=C(NC[CH2])C(O)C([CH2])(C)C
2: [O]P(=O)(O)OP(=O)(O)OP(=O)(O)O>>O=P(O)(O)OP(=O)(O)O
3: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]
4: [C]C(O)C(=O)O>>O=P(O)(O)O[P]
5: [C]CCN>>[C]CCNC(=O)C([C])O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00005	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)O]
3: R:M00002, P:M00005	[O=C(O)C(O)C(C)(C)CO>>O=P(O)(O)OP(=O)(O)O]
4: R:M00002, P:M00006	[O=C(O)C(O)C(C)(C)CO>>O=C(O)CCNC(=O)C(O)C(C)(C)CO]
5: R:M00003, P:M00006	[O=C(O)CCN>>O=C(O)CCNC(=O)C(O)C(C)(C)CO]


//
SELECTED AAM MAPPING
[O:42]=[C:43]([OH:44])[CH2:45][CH2:46][NH2:47].[O:32]=[C:33]([OH:34])[CH:35]([OH:36])[C:37]([CH3:38])([CH3:39])[CH2:40][OH:41].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:42]=[C:43]([OH:44])[CH2:45][CH2:46][NH:47][C:33](=[O:32])[CH:35]([OH:36])[C:37]([CH3:38])([CH3:39])[CH2:40][OH:41].[O:11]=[P:10]([OH:9])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[OH:34]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=38, 33=37, 34=39, 35=40, 36=41, 37=35, 38=33, 39=32, 40=34, 41=36, 42=45, 43=46, 44=47, 45=43, 46=42, 47=44}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=16, 2=17, 3=18, 4=13, 5=14, 6=15, 7=19, 8=12, 9=11, 10=10, 11=9, 12=20, 13=22, 14=7, 15=8, 16=6, 17=5, 18=2, 19=1, 20=3, 21=4, 22=23, 23=21, 24=41, 25=40, 26=39, 27=42, 28=43, 29=44, 30=45, 31=46, 32=47, 33=35, 34=34, 35=36, 36=37, 37=38, 38=32, 39=30, 40=31, 41=29, 42=28, 43=27, 44=25, 45=24, 46=26, 47=33}

