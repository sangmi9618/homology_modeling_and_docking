
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%N:1.0, C=O:1.0, O-P:2.0]

ORDER_CHANGED
[C%N*C-N:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=P(O)(O)O:3.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O:1.0, O=[CH]:1.0, O=[CH]>>[P]O:1.0, [CH]:4.0, [CH]C(=[CH])N1C=[N+][CH]C1:1.0, [CH]C(=[CH])NC[CH]:1.0, [CH]C(=[CH])NC[CH]>>[CH]C(=[CH])N1C=[N+][CH]C1:1.0, [C]C(=[C])N(C=O)C([CH2])[CH2]:1.0, [C]C(=[C])N(C=O)C([CH2])[CH2]>>[C]C(=[C])[N+]1=CN(C([CH])=[CH])CC1[CH2]:1.0, [C]C(=[C])N(C=O)C([CH2])[CH2]>>[C]C(=[C])[N+]1=C[N]CC1[CH2]:1.0, [C]C(=[C])[N+]1=C[N]CC1[CH2]:1.0, [C]N([CH])C(C[NH])C[NH]:1.0, [C]N([CH])C(C[NH])C[NH]>>[C][N+]1=C[N]CC1C[NH]:1.0, [C]N([CH])C=O:1.0, [C]N([CH])C=O>>O=P(O)(O)O:1.0, [C]N([CH])C=O>>[C]N1C=[N+]([C])[CH]C1:1.0, [C]N([CH])[CH2]:1.0, [C]N([CH])[CH]:1.0, [C]N([CH])[CH]>>[C][N+]([CH])=[CH]:1.0, [C]N1C=[N+]([C])[CH]C1:1.0, [C]NCC1N(C=O)C([C])=[C]NC1>>[C]C1=[C]NCC2[N+]1=CN([C])C2:1.0, [C]N[CH2]:1.0, [C]N[CH2]>>[C]N([CH])[CH2]:1.0, [C][N+]([CH])=[CH]:1.0, [C][N+]1=C[N]CC1C[NH]:1.0, [N+]:1.0, [N+]C([CH2])[CH2]:1.0, [NH]:1.0, [N]:2.0, [N]C(=O)C1=C([NH])NCC(N1C=O)C[NH]>>[C]N1C=[N+]2C(=C([NH])NCC2C1)C([N])=O:1.0, [N]C([CH2])CNC(=C[CH])C=[CH]>>[C][N+]1=CN(C(=C[CH])C=[CH])CC1[CH2]:1.0, [N]C([CH2])[CH2]:1.0, [N]C([CH2])[CH2]>>[N+]C([CH2])[CH2]:1.0, [N]C=O:2.0, [N]C=O>>O=P(O)(O)O:1.0, [N]C=O>>[N]C=[N+]:1.0, [N]C=[N+]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[CH]:2.0, [N]:1.0, [OH]:1.0, [O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (8)
[O=P(O)(O)O:1.0, O=[CH]:1.0, [C]N([CH])[CH2]:1.0, [N]C=O:1.0, [N]C=[N+]:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (7)
[O=P(O)(O)O:2.0, O=P(O)(O)O[P]:1.0, [CH]C(=[CH])N1C=[N+][CH]C1:1.0, [C]N([CH])C=O:1.0, [C]N1C=[N+]([C])[CH]C1:1.0, [N]C=O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [N+]:1.0, [N]:1.0]


ID=Reaction Center at Level: 1 (4)
[[C]N([CH])[CH]:1.0, [C][N+]([CH])=[CH]:1.0, [N]C=O:1.0, [N]C=[N+]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]C(=[C])N(C=O)C([CH2])[CH2]:1.0, [C]C(=[C])[N+]1=C[N]CC1[CH2]:1.0, [C]N([CH])C=O:1.0, [C]N1C=[N+]([C])[CH]C1:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (2)
[[N+]C([CH2])[CH2]:1.0, [N]C([CH2])[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[[C]N([CH])C(C[NH])C[NH]:1.0, [C][N+]1=C[N]CC1C[NH]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]N([CH])[CH]>>[C][N+]([CH])=[CH]
2: [C]N[CH2]>>[C]N([CH])[CH2]
3: O=[CH]>>[P]O
4: [P]O[P]>>[P]O
5: [O]P(=O)(O)O>>O=P(O)(O)O
6: [N]C([CH2])[CH2]>>[N+]C([CH2])[CH2]
7: [N]C=O>>[N]C=[N+]

MMP Level 2
1: [C]C(=[C])N(C=O)C([CH2])[CH2]>>[C]C(=[C])[N+]1=C[N]CC1[CH2]
2: [CH]C(=[CH])NC[CH]>>[CH]C(=[CH])N1C=[N+][CH]C1
3: [N]C=O>>O=P(O)(O)O
4: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
5: O=P(O)(O)O[P]>>O=P(O)(O)O
6: [C]N([CH])C(C[NH])C[NH]>>[C][N+]1=C[N]CC1C[NH]
7: [C]N([CH])C=O>>[C]N1C=[N+]([C])[CH]C1

MMP Level 3
1: [N]C(=O)C1=C([NH])NCC(N1C=O)C[NH]>>[C]N1C=[N+]2C(=C([NH])NCC2C1)C([N])=O
2: [N]C([CH2])CNC(=C[CH])C=[CH]>>[C][N+]1=CN(C(=C[CH])C=[CH])CC1[CH2]
3: [C]N([CH])C=O>>O=P(O)(O)O
4: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
5: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O
6: [C]NCC1N(C=O)C([C])=[C]NC1>>[C]C1=[C]NCC2[N+]1=CN([C])C2
7: [C]C(=[C])N(C=O)C([CH2])[CH2]>>[C]C(=[C])[N+]1=CN(C([CH])=[CH])CC1[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00005	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)O]
3: R:M00002, P:M00004	[O=CN1c2c(=O)nc(N)[nH]c2NCC1CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)O>>O=c1nc(N)[nH]c2NCC3[N+](=CN(c4ccc(cc4)C(=O)NC(C(=O)O)CCC(=O)O)C3)c12, O=CN1c2c(=O)nc(N)[nH]c2NCC1CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)O>>O=c1nc(N)[nH]c2NCC3[N+](=CN(c4ccc(cc4)C(=O)NC(C(=O)O)CCC(=O)O)C3)c12, O=CN1c2c(=O)nc(N)[nH]c2NCC1CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)O>>O=c1nc(N)[nH]c2NCC3[N+](=CN(c4ccc(cc4)C(=O)NC(C(=O)O)CCC(=O)O)C3)c12, O=CN1c2c(=O)nc(N)[nH]c2NCC1CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)O>>O=c1nc(N)[nH]c2NCC3[N+](=CN(c4ccc(cc4)C(=O)NC(C(=O)O)CCC(=O)O)C3)c12]
4: R:M00002, P:M00005	[O=CN1c2c(=O)nc(N)[nH]c2NCC1CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[CH:2][N:3]1[C:4]=2[C:5](=[O:6])[N:7]=[C:8]([NH2:9])[NH:10][C:11]2[NH:12][CH2:13][CH:14]1[CH2:15][NH:16][C:17]:3:[CH:18]:[CH:19]:[C:20](:[CH:21]:[CH:22]3)[C:23](=[O:24])[NH:25][CH:26]([C:27](=[O:28])[OH:29])[CH2:30][CH2:31][C:32](=[O:33])[OH:34].[O:35]=[P:36]([OH:37])([OH:38])[O:39][P:40](=[O:41])([OH:42])[O:43][P:44](=[O:45])([OH:46])[O:47][CH2:48][CH:49]1[O:50][CH:51]([N:52]:2:[CH:53]:[N:54]:[C:55]:3:[C:56](:[N:57]:[CH:58]:[N:59]:[C:60]32)[NH2:61])[CH:62]([OH:63])[CH:64]1[OH:65]>>[O:33]=[C:32]([OH:34])[CH2:31][CH2:30][CH:26]([NH:25][C:23](=[O:24])[C:20]:1:[CH:19]:[CH:18]:[C:17](:[CH:22]:[CH:21]1)[N:16]2[CH:2]=[N+:3]3[C:4]=4[C:5](=[O:6])[N:7]=[C:8]([NH2:9])[NH:10][C:11]4[NH:12][CH2:13][CH:14]3[CH2:15]2)[C:27](=[O:28])[OH:29].[O:35]=[P:36]([OH:37])([OH:38])[OH:1].[O:41]=[P:40]([OH:39])([OH:42])[O:43][P:44](=[O:45])([OH:46])[O:47][CH2:48][CH:49]1[O:50][CH:51]([N:52]:2:[CH:53]:[N:54]:[C:55]:3:[C:56](:[N:57]:[CH:58]:[N:59]:[C:60]32)[NH2:61])[CH:62]([OH:63])[CH:64]1[OH:65]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=58, 2=59, 3=60, 4=55, 5=56, 6=57, 7=61, 8=54, 9=53, 10=52, 11=51, 12=62, 13=64, 14=49, 15=50, 16=48, 17=47, 18=44, 19=45, 20=46, 21=43, 22=40, 23=41, 24=42, 25=39, 26=36, 27=35, 28=37, 29=38, 30=65, 31=63, 32=13, 33=14, 34=3, 35=4, 36=11, 37=12, 38=10, 39=8, 40=7, 41=5, 42=6, 43=9, 44=2, 45=1, 46=15, 47=16, 48=17, 49=18, 50=19, 51=20, 52=21, 53=22, 54=23, 55=24, 56=25, 57=26, 58=30, 59=31, 60=32, 61=33, 62=34, 63=27, 64=28, 65=29}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=53, 2=54, 3=55, 4=50, 5=51, 6=52, 7=56, 8=49, 9=48, 10=47, 11=46, 12=57, 13=59, 14=44, 15=45, 16=43, 17=42, 18=39, 19=40, 20=41, 21=38, 22=35, 23=34, 24=36, 25=37, 26=60, 27=58, 28=9, 29=10, 30=32, 31=13, 32=12, 33=11, 34=33, 35=7, 36=8, 37=6, 38=4, 39=3, 40=2, 41=1, 42=5, 43=14, 44=15, 45=16, 46=17, 47=18, 48=19, 49=20, 50=21, 51=22, 52=23, 53=27, 54=28, 55=29, 56=30, 57=31, 58=24, 59=25, 60=26, 61=63, 62=62, 63=61, 64=64, 65=65}

