
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(N)C=1C=[N+]C=CC1>>O=C(N)C1=C[N]C=CC1:1.0, O=C(O)C[CH2]:1.0, O=C(O)[CH2]:2.0, O=CCC[CH2]>>O=C(O)CC[CH2]:1.0, O=CC[CH2]:1.0, O=CC[CH2]>>O=C(O)C[CH2]:1.0, O=C[CH2]:1.0, O=C[CH2]>>O=C(O)[CH2]:1.0, O>>O=C(O)C[CH2]:1.0, O>>O=C(O)[CH2]:1.0, O>>[C]O:1.0, [CH2]:1.0, [CH]:2.0, [CH]N([CH])[CH]:1.0, [CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]>>[CH]N([CH])[CH]:1.0, [C]:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN(C=[CH])C([O])[CH]:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])C=C[CH]>>[C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C=[CH]:1.0, [C]C=[CH]>>[C]C[CH]:1.0, [C]C[CH]:1.0, [C]O:1.0, [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]C1=CN(C=CC1)C2O[CH][CH]C2O:1.0, [N+]:1.0, [N]:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[O:1.0, [C]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (3)
[O:1.0, O=C(O)[CH2]:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (3)
[O:1.0, O=C(O)C[CH2]:1.0, O=C(O)[CH2]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C=[CH]>>[C]C[CH]
2: O>>[C]O
3: [CH][N+]([CH])=[CH]>>[CH]N([CH])[CH]
4: O=C[CH2]>>O=C(O)[CH2]

MMP Level 2
1: [C]C(=[CH])C=C[CH]>>[C]C(=[CH])CC=[CH]
2: O>>O=C(O)[CH2]
3: [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN(C=[CH])C([O])[CH]
4: O=CC[CH2]>>O=C(O)C[CH2]

MMP Level 3
1: O=C(N)C=1C=[N+]C=CC1>>O=C(N)C1=C[N]C=CC1
2: O>>O=C(O)C[CH2]
3: [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]C1=CN(C=CC1)C2O[CH][CH]C2O
4: O=CCC[CH2]>>O=C(O)CC[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=CCCCC(N)C(=O)O>>O=C(O)CCCC(N)C(=O)O]
2: R:M00002, P:M00005	[O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O, O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O]
3: R:M00003, P:M00004	[O>>O=C(O)CCCC(N)C(=O)O]


//
SELECTED AAM MAPPING
[O:45]=[CH:46][CH2:47][CH2:48][CH2:49][CH:50]([NH2:51])[C:52](=[O:53])[OH:54].[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:5]:[CH:6]:[CH:7]:[N+:8](:[CH:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44].[OH2:55]>>[H+:56].[O:45]=[C:46]([OH:55])[CH2:47][CH2:48][CH2:49][CH:50]([NH2:51])[C:52](=[O:53])[OH:54].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:9][N:8]([CH:7]=[CH:6][CH2:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=48, 2=47, 3=46, 4=45, 5=49, 6=50, 7=52, 8=53, 9=54, 10=51, 11=6, 12=5, 13=4, 14=9, 15=8, 16=7, 17=10, 18=43, 19=41, 20=12, 21=11, 22=13, 23=14, 24=15, 25=16, 26=17, 27=18, 28=19, 29=20, 30=21, 31=22, 32=23, 33=24, 34=39, 35=37, 36=26, 37=25, 38=27, 39=28, 40=29, 41=30, 42=35, 43=34, 44=33, 45=32, 46=31, 47=36, 48=38, 49=40, 50=42, 51=44, 52=2, 53=1, 54=3, 55=55}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=49, 2=50, 3=51, 4=53, 5=54, 6=55, 7=52, 8=48, 9=46, 10=45, 11=47, 12=9, 13=8, 14=7, 15=6, 16=5, 17=4, 18=2, 19=1, 20=3, 21=10, 22=43, 23=41, 24=12, 25=11, 26=13, 27=14, 28=15, 29=16, 30=17, 31=18, 32=19, 33=20, 34=21, 35=22, 36=23, 37=24, 38=39, 39=37, 40=26, 41=25, 42=27, 43=28, 44=29, 45=30, 46=35, 47=34, 48=33, 49=32, 50=31, 51=36, 52=38, 53=40, 54=42, 55=44, 56=56}

