
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0]

ORDER_CHANGED
[C%C*C%C:1.0, C%N*C%N:2.0, C-C*C=C:1.0, C-O*C=O:1.0]

//
FINGERPRINTS RC
[O=C(O)CC[CH2]:1.0, O=C(O)CC[CH2]>>O=C=O:1.0, O=C(O)CC[CH2]>>[CH]=CC:1.0, O=C(O)C[CH2]:1.0, O=C(O)C[CH2]>>O=C=O:2.0, O=C(O)[CH2]:2.0, O=C(O)[CH2]>>O=C=O:2.0, O=C([S])C=CC:1.0, O=C([S])CCCC(=O)O>>O=C([S])C=CC:1.0, O=C([S])CC[CH2]:1.0, O=C([S])CC[CH2]>>O=C([S])C=CC:1.0, O=C1N=C2C(=N[C]=C([CH])N2C[CH])C(=O)N1>>O=c1[nH]c(=O)c2N[C]=C([CH])N(c2[nH]1)C[CH]:1.0, O=C1N[C]N=C2C1=NC([CH])=[C]N2[CH2]>>O=C1N[C]NC2=C1NC([CH])=[C]N2[CH2]:1.0, O=C=O:3.0, [CH2]:3.0, [CH2]C[CH2]:1.0, [CH2]C[CH2]>>[CH]=CC:1.0, [CH3]:1.0, [CH]:2.0, [CH]=CC:2.0, [CH]C:1.0, [C]:6.0, [C]=C([N])NC(=O)[NH]:1.0, [C]=C([N])[NH]:1.0, [C]=CC=1N=C(C(=[N])[N]C1[CH])C(=O)[NH]>>[C]=CC=1NC(=C([NH])[N]C1[CH])C(=O)[NH]:1.0, [C]=O:1.0, [C]C(=[C])NC(=[C])[CH]:1.0, [C]C(=[C])[NH]:1.0, [C]C([C])=NC(=[C])[CH]:1.0, [C]C([C])=NC(=[C])[CH]>>[C]C(=[C])NC(=[C])[CH]:1.0, [C]C([C])=[N]:1.0, [C]C([C])=[N]>>[C]C(=[C])[NH]:1.0, [C]C([N])=NC(=O)[NH]:1.0, [C]C([N])=NC(=O)[NH]>>[C]=C([N])NC(=O)[NH]:1.0, [C]C([N])=[N]:1.0, [C]C([N])=[N]>>[C]=C([N])[NH]:1.0, [C]C=CC:1.0, [C]C=[CH]:1.0, [C]CCCC(=O)O>>[C]C=CC:1.0, [C]CCCC(=O)S[CH2]>>O=C(S[CH2])C=CC:1.0, [C]CCC[C]:1.0, [C]CCC[C]>>[C]C=CC:1.0, [C]C[CH2]:2.0, [C]C[CH2]>>[CH]C:1.0, [C]C[CH2]>>[C]C=[CH]:1.0, [C]N([CH2])C1=NC(=O)N[C]C1=[N]>>[C]N([CH2])C=1NC(=O)N[C]C1[NH]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]>>[C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]>>[C]NC(=C([N])[NH])C(=O)[NH]:1.0, [C]N=[C]:2.0, [C]N=[C]>>[C]N[C]:2.0, [C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]NC(=C([N])[NH])C(=O)[NH]:1.0, [C]N[C]:2.0, [C]O:1.0, [C]O>>[C]=O:1.0, [NH]:2.0, [N]:2.0, [OH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH2]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[O=C(O)[CH2]:1.0, [C]C[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)CC[CH2]:1.0, O=C(O)C[CH2]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (7)
[[CH2]:2.0, [CH]:2.0, [C]:6.0, [NH]:2.0, [N]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (14)
[O=C(O)[CH2]:1.0, O=C=O:1.0, [CH2]C[CH2]:1.0, [CH]=CC:1.0, [C]=C([N])[NH]:1.0, [C]=O:1.0, [C]C(=[C])[NH]:1.0, [C]C([C])=[N]:1.0, [C]C([N])=[N]:1.0, [C]C=[CH]:1.0, [C]C[CH2]:1.0, [C]N=[C]:2.0, [C]N[C]:2.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (15)
[O=C(O)C[CH2]:1.0, O=C(O)[CH2]:1.0, O=C([S])C=CC:1.0, O=C([S])CC[CH2]:1.0, O=C=O:2.0, [C]=C([N])NC(=O)[NH]:1.0, [C]C(=[C])NC(=[C])[CH]:1.0, [C]C([C])=NC(=[C])[CH]:1.0, [C]C([N])=NC(=O)[NH]:1.0, [C]C=CC:1.0, [C]CCC[C]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]:1.0, [C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]NC(=C([N])[NH])C(=O)[NH]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C[CH2]>>[C]C=[CH]
2: [C]N=[C]>>[C]N[C]
3: O=C(O)[CH2]>>O=C=O
4: [C]C[CH2]>>[CH]C
5: [C]N=[C]>>[C]N[C]
6: [C]O>>[C]=O
7: [C]C([N])=[N]>>[C]=C([N])[NH]
8: [CH2]C[CH2]>>[CH]=CC
9: [C]C([C])=[N]>>[C]C(=[C])[NH]

MMP Level 2
1: O=C([S])CC[CH2]>>O=C([S])C=CC
2: [C]C([N])=NC(=O)[NH]>>[C]=C([N])NC(=O)[NH]
3: O=C(O)C[CH2]>>O=C=O
4: O=C(O)CC[CH2]>>[CH]=CC
5: [C]C([C])=NC(=[C])[CH]>>[C]C(=[C])NC(=[C])[CH]
6: O=C(O)[CH2]>>O=C=O
7: [C]N=C(C([C])=[N])N([C])[CH2]>>[C]NC(=C([C])[NH])N([C])[CH2]
8: [C]CCC[C]>>[C]C=CC
9: [C]N=C(C([N])=[N])C(=O)[NH]>>[C]NC(=C([N])[NH])C(=O)[NH]

MMP Level 3
1: [C]CCCC(=O)S[CH2]>>O=C(S[CH2])C=CC
2: [C]N([CH2])C1=NC(=O)N[C]C1=[N]>>[C]N([CH2])C=1NC(=O)N[C]C1[NH]
3: O=C(O)CC[CH2]>>O=C=O
4: [C]CCCC(=O)O>>[C]C=CC
5: [C]=CC=1N=C(C(=[N])[N]C1[CH])C(=O)[NH]>>[C]=CC=1NC(=C([NH])[N]C1[CH])C(=O)[NH]
6: O=C(O)C[CH2]>>O=C=O
7: O=C1N=C2C(=N[C]=C([CH])N2C[CH])C(=O)N1>>O=c1[nH]c(=O)c2N[C]=C([CH])N(c2[nH]1)C[CH]
8: O=C([S])CCCC(=O)O>>O=C([S])C=CC
9: O=C1N[C]N=C2C1=NC([CH])=[C]N2[CH2]>>O=C1N[C]NC2=C1NC([CH])=[C]N2[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)CCCC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O>>O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C=CC, O=C(O)CCCC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O>>O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C=CC, O=C(O)CCCC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O>>O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C=CC]
2: R:M00001, P:M00004	[O=C(O)CCCC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O>>O=C=O, O=C(O)CCCC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O>>O=C=O]
3: R:M00002, P:M00005	[O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C, O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C, O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C, O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH2:4][CH2:5][CH2:6][C:7](=[O:8])[S:9][CH2:10][CH2:11][NH:12][C:13](=[O:14])[CH2:15][CH2:16][NH:17][C:18](=[O:19])[CH:20]([OH:21])[C:22]([CH3:23])([CH3:24])[CH2:25][O:26][P:27](=[O:28])([OH:29])[O:30][P:31](=[O:32])([OH:33])[O:34][CH2:35][CH:36]1[O:37][CH:38]([N:39]:2:[CH:40]:[N:41]:[C:42]:3:[C:43](:[N:44]:[CH:45]:[N:46]:[C:47]32)[NH2:48])[CH:49]([OH:50])[CH:51]1[O:52][P:53](=[O:54])([OH:55])[OH:56].[O:57]=[C:58]1[N:59]=[C:60]2[C:61](=[N:62][C:63]:3:[CH:64]:[C:65](:[C:66](:[CH:67]:[C:68]3[N:69]2[CH2:70][CH:71]([OH:72])[CH:73]([OH:74])[CH:75]([OH:76])[CH2:77][O:78][P:79](=[O:80])([OH:81])[O:82][P:83](=[O:84])([OH:85])[O:86][CH2:87][CH:88]4[O:89][CH:90]([N:91]:5:[CH:92]:[N:93]:[C:94]:6:[C:95](:[N:96]:[CH:97]:[N:98]:[C:99]65)[NH2:100])[CH:101]([OH:102])[CH:103]4[OH:104])[CH3:105])[CH3:106])[C:107](=[O:108])[NH:109]1>>[O:1]=[C:2]=[O:3].[O:8]=[C:7]([S:9][CH2:10][CH2:11][NH:12][C:13](=[O:14])[CH2:15][CH2:16][NH:17][C:18](=[O:19])[CH:20]([OH:21])[C:22]([CH3:23])([CH3:24])[CH2:25][O:26][P:27](=[O:28])([OH:29])[O:30][P:31](=[O:32])([OH:33])[O:34][CH2:35][CH:36]1[O:37][CH:38]([N:39]:2:[CH:40]:[N:41]:[C:42]:3:[C:43](:[N:44]:[CH:45]:[N:46]:[C:47]32)[NH2:48])[CH:49]([OH:50])[CH:51]1[O:52][P:53](=[O:54])([OH:55])[OH:56])[CH:6]=[CH:5][CH3:4].[O:57]=[C:58]1[NH:109][C:107](=[O:108])[C:61]=2[NH:62][C:63]:3:[CH:64]:[C:65](:[C:66](:[CH:67]:[C:68]3[N:69]([C:60]2[NH:59]1)[CH2:70][CH:71]([OH:72])[CH:73]([OH:74])[CH:75]([OH:76])[CH2:77][O:78][P:79](=[O:80])([OH:81])[O:82][P:83](=[O:84])([OH:85])[O:86][CH2:87][CH:88]4[O:89][CH:90]([N:91]:5:[CH:92]:[N:93]:[C:94]:6:[C:95](:[N:96]:[CH:97]:[N:98]:[C:99]65)[NH2:100])[CH:101]([OH:102])[CH:103]4[OH:104])[CH3:105])[CH3:106]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=23, 2=22, 3=24, 4=25, 5=26, 6=27, 7=28, 8=29, 9=30, 10=31, 11=32, 12=33, 13=34, 14=35, 15=36, 16=51, 17=49, 18=38, 19=37, 20=39, 21=40, 22=41, 23=42, 24=47, 25=46, 26=45, 27=44, 28=43, 29=48, 30=50, 31=52, 32=53, 33=54, 34=55, 35=56, 36=20, 37=18, 38=19, 39=17, 40=16, 41=15, 42=13, 43=14, 44=12, 45=11, 46=10, 47=9, 48=7, 49=8, 50=6, 51=5, 52=4, 53=2, 54=1, 55=3, 56=21, 57=106, 58=65, 59=64, 60=63, 61=68, 62=67, 63=66, 64=105, 65=69, 66=60, 67=59, 68=58, 69=57, 70=109, 71=107, 72=108, 73=61, 74=62, 75=70, 76=71, 77=73, 78=75, 79=77, 80=78, 81=79, 82=80, 83=81, 84=82, 85=83, 86=84, 87=85, 88=86, 89=87, 90=88, 91=103, 92=101, 93=90, 94=89, 95=91, 96=92, 97=93, 98=94, 99=99, 100=98, 101=97, 102=96, 103=95, 104=100, 105=102, 106=104, 107=76, 108=74, 109=72}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=53, 2=52, 3=51, 4=2, 5=1, 6=3, 7=4, 8=5, 9=6, 10=7, 11=8, 12=9, 13=10, 14=11, 15=12, 16=13, 17=14, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=25, 28=26, 29=27, 30=28, 31=29, 32=30, 33=45, 34=43, 35=32, 36=31, 37=33, 38=34, 39=35, 40=36, 41=41, 42=40, 43=39, 44=38, 45=37, 46=42, 47=44, 48=46, 49=47, 50=48, 51=49, 52=50, 53=15, 54=108, 55=107, 56=109, 57=106, 58=63, 59=62, 60=61, 61=66, 62=65, 63=64, 64=105, 65=67, 66=68, 67=59, 68=60, 69=57, 70=58, 71=56, 72=55, 73=54, 74=69, 75=70, 76=71, 77=73, 78=75, 79=77, 80=78, 81=79, 82=80, 83=81, 84=82, 85=83, 86=84, 87=85, 88=86, 89=87, 90=88, 91=103, 92=101, 93=90, 94=89, 95=91, 96=92, 97=93, 98=94, 99=99, 100=98, 101=97, 102=96, 103=95, 104=100, 105=102, 106=104, 107=76, 108=74, 109=72}

