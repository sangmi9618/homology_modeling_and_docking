
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C=O:1.0]

ORDER_CHANGED
[C%C*C%C:1.0, C%N*C%N:2.0]

//
FINGERPRINTS RC
[O=C1N=C2C(=N[C]=C([CH])N2C[CH])C(=O)N1>>O=c1[nH]c(=O)c2N[C]=C([CH])N(c2[nH]1)C[CH]:1.0, O=C1N[C]N=C2C1=NC([CH])=[C]N2[CH2]>>O=C1N[C]NC2=C1NC([CH])=[C]N2[CH2]:1.0, [CH2]:1.0, [CH]:1.0, [C]:4.0, [C]=C([NH])C(=N[CH])NCC([O])=[CH]>>[C]=C([NH])C(=N[CH])N:1.0, [C]=C([N])NC(=O)[NH]:1.0, [C]=C([N])[NH]:1.0, [C]=CC=1N=C(C(=[N])[N]C1[CH])C(=O)[NH]>>[C]=CC=1NC(=C([NH])[N]C1[CH])C(=O)[NH]:1.0, [C]C(=[C])NC(=[C])[CH]:1.0, [C]C(=[C])[NH]:1.0, [C]C(=[N])N:1.0, [C]C(=[N])NC[C]:1.0, [C]C(=[N])NC[C]>>[C]C(=[N])N:1.0, [C]C(=[N])NCc1occc1>>O=Cc1occc1:1.0, [C]C([C])=NC(=[C])[CH]:1.0, [C]C([C])=NC(=[C])[CH]>>[C]C(=[C])NC(=[C])[CH]:1.0, [C]C([C])=[N]:1.0, [C]C([C])=[N]>>[C]C(=[C])[NH]:1.0, [C]C([N])=NC(=O)[NH]:1.0, [C]C([N])=NC(=O)[NH]>>[C]=C([N])NC(=O)[NH]:1.0, [C]C([N])=[N]:1.0, [C]C([N])=[N]>>[C]=C([N])[NH]:1.0, [C]C=O:1.0, [C]C[NH]:1.0, [C]C[NH]>>[C]C=O:1.0, [C]N:1.0, [C]N([CH2])C1=NC(=O)N[C]C1=[N]>>[C]N([CH2])C=1NC(=O)N[C]C1[NH]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]>>[C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]>>[C]NC(=C([N])[NH])C(=O)[NH]:1.0, [C]N=[C]:2.0, [C]N=[C]>>[C]N[C]:2.0, [C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]NC(=C([N])[NH])C(=O)[NH]:1.0, [C]NCC([O])=[CH]:1.0, [C]NCC([O])=[CH]>>[O]C(=[CH])C=O:1.0, [C]N[CH2]:1.0, [C]N[CH2]>>[C]N:1.0, [C]N[C]:2.0, [NH2]:1.0, [NH]:3.0, [N]:2.0, [O]C(=[CH])C=O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[CH2]:1.0, [CH]:1.0, [NH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=[CH]:1.0, [C]C=O:1.0, [C]C[NH]:1.0, [C]N[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]C(=[N])NC[C]:1.0, [C]C=O:1.0, [C]NCC([O])=[CH]:1.0, [O]C(=[CH])C=O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:4.0, [NH]:2.0, [N]:2.0]


ID=Reaction Center at Level: 1 (6)
[[C]=C([N])[NH]:1.0, [C]C(=[C])[NH]:1.0, [C]C([C])=[N]:1.0, [C]C([N])=[N]:1.0, [C]N=[C]:2.0, [C]N[C]:2.0]


ID=Reaction Center at Level: 2 (8)
[[C]=C([N])NC(=O)[NH]:1.0, [C]C(=[C])NC(=[C])[CH]:1.0, [C]C([C])=NC(=[C])[CH]:1.0, [C]C([N])=NC(=O)[NH]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]:1.0, [C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]NC(=C([N])[NH])C(=O)[NH]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]N=[C]>>[C]N[C]
2: [C]C[NH]>>[C]C=O
3: [C]C([C])=[N]>>[C]C(=[C])[NH]
4: [C]N=[C]>>[C]N[C]
5: [C]C([N])=[N]>>[C]=C([N])[NH]
6: [C]N[CH2]>>[C]N

MMP Level 2
1: [C]C([C])=NC(=[C])[CH]>>[C]C(=[C])NC(=[C])[CH]
2: [C]NCC([O])=[CH]>>[O]C(=[CH])C=O
3: [C]N=C(C([N])=[N])C(=O)[NH]>>[C]NC(=C([N])[NH])C(=O)[NH]
4: [C]C([N])=NC(=O)[NH]>>[C]=C([N])NC(=O)[NH]
5: [C]N=C(C([C])=[N])N([C])[CH2]>>[C]NC(=C([C])[NH])N([C])[CH2]
6: [C]C(=[N])NC[C]>>[C]C(=[N])N

MMP Level 3
1: [C]=CC=1N=C(C(=[N])[N]C1[CH])C(=O)[NH]>>[C]=CC=1NC(=C([NH])[N]C1[CH])C(=O)[NH]
2: [C]C(=[N])NCc1occc1>>O=Cc1occc1
3: O=C1N[C]N=C2C1=NC([CH])=[C]N2[CH2]>>O=C1N[C]NC2=C1NC([CH])=[C]N2[CH2]
4: [C]N([CH2])C1=NC(=O)N[C]C1=[N]>>[C]N([CH2])C=1NC(=O)N[C]C1[NH]
5: O=C1N=C2C(=N[C]=C([CH])N2C[CH])C(=O)N1>>O=c1[nH]c(=O)c2N[C]=C([CH])N(c2[nH]1)C[CH]
6: [C]=C([NH])C(=N[CH])NCC([O])=[CH]>>[C]=C([NH])C(=N[CH])N


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[n1cnc(NCc2occc2)c3[nH]cnc13>>O=Cc1occc1]
2: R:M00001, P:M00004	[n1cnc(NCc2occc2)c3[nH]cnc13>>n1cnc(N)c2[nH]cnc12]
3: R:M00002, P:M00005	[O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C, O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C, O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C, O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[C:4]2[C:5](=[N:6][C:7]:3:[CH:8]:[C:9](:[C:10](:[CH:11]:[C:12]3[N:13]2[CH2:14][CH:15]([OH:16])[CH:17]([OH:18])[CH:19]([OH:20])[CH2:21][O:22][P:23](=[O:24])([OH:25])[O:26][P:27](=[O:28])([OH:29])[O:30][CH2:31][CH:32]4[O:33][CH:34]([N:35]:5:[CH:36]:[N:37]:[C:38]:6:[C:39](:[N:40]:[CH:41]:[N:42]:[C:43]65)[NH2:44])[CH:45]([OH:46])[CH:47]4[OH:48])[CH3:49])[CH3:50])[C:51](=[O:52])[NH:53]1.[N:54]:1:[CH:55]:[N:56]:[C:57]([NH:58][CH2:59][C:60]:2:[O:61]:[CH:62]:[CH:63]:[CH:64]2):[C:65]:3:[NH:66]:[CH:67]:[N:68]:[C:69]13>>[O:70]=[CH:59][C:60]:1:[O:61]:[CH:62]:[CH:63]:[CH:64]1.[O:1]=[C:2]1[NH:53][C:51](=[O:52])[C:5]=2[NH:6][C:7]:3:[CH:8]:[C:9](:[C:10](:[CH:11]:[C:12]3[N:13]([C:4]2[NH:3]1)[CH2:14][CH:15]([OH:16])[CH:17]([OH:18])[CH:19]([OH:20])[CH2:21][O:22][P:23](=[O:24])([OH:25])[O:26][P:27](=[O:28])([OH:29])[O:30][CH2:31][CH:32]4[O:33][CH:34]([N:35]:5:[CH:36]:[N:37]:[C:38]:6:[C:39](:[N:40]:[CH:41]:[N:42]:[C:43]65)[NH2:44])[CH:45]([OH:46])[CH:47]4[OH:48])[CH3:49])[CH3:50].[N:54]:1:[CH:55]:[N:56]:[C:57]([NH2:58]):[C:65]:2:[NH:66]:[CH:67]:[N:68]:[C:69]12


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=63, 2=62, 3=61, 4=60, 5=64, 6=59, 7=58, 8=57, 9=56, 10=55, 11=54, 12=69, 13=65, 14=66, 15=67, 16=68, 17=50, 18=9, 19=8, 20=7, 21=12, 22=11, 23=10, 24=49, 25=13, 26=4, 27=3, 28=2, 29=1, 30=53, 31=51, 32=52, 33=5, 34=6, 35=14, 36=15, 37=17, 38=19, 39=21, 40=22, 41=23, 42=24, 43=25, 44=26, 45=27, 46=28, 47=29, 48=30, 49=31, 50=32, 51=47, 52=45, 53=34, 54=33, 55=35, 56=36, 57=37, 58=38, 59=43, 60=42, 61=41, 62=40, 63=39, 64=44, 65=46, 66=48, 67=20, 68=18, 69=16}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=69, 2=68, 3=67, 4=66, 5=70, 6=65, 7=64, 8=61, 9=62, 10=63, 11=59, 12=60, 13=57, 14=56, 15=55, 16=54, 17=58, 18=53, 19=10, 20=9, 21=8, 22=13, 23=12, 24=11, 25=52, 26=14, 27=15, 28=6, 29=7, 30=4, 31=5, 32=3, 33=2, 34=1, 35=16, 36=17, 37=18, 38=20, 39=22, 40=24, 41=25, 42=26, 43=27, 44=28, 45=29, 46=30, 47=31, 48=32, 49=33, 50=34, 51=35, 52=50, 53=48, 54=37, 55=36, 56=38, 57=39, 58=40, 59=41, 60=46, 61=45, 62=44, 63=43, 64=42, 65=47, 66=49, 67=51, 68=23, 69=21, 70=19}

