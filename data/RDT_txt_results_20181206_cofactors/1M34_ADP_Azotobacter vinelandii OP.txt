
//
FINGERPRINTS BC

FORMED_CLEAVED
[N#N:1.0, O-P:2.0]

//
FINGERPRINTS RC
[N:3.0, N#N:4.0, N#N>>N:3.0, O:3.0, O=P(O)(O)O:3.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O:1.0, O>>O=P(O)(O)O:2.0, O>>[P]O:1.0, [N]:2.0, [OH]:2.0, [O]:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (6)
[N:1.0, O:1.0, [N]:2.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (7)
[N:1.0, N#N:2.0, O:1.0, O=P(O)(O)O:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (6)
[N:1.0, N#N:2.0, O:1.0, O=P(O)(O)O:2.0, O=P(O)(O)O[P]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O>>[P]O
2: N#N>>N
3: [P]O[P]>>[P]O
4: [O]P(=O)(O)O>>O=P(O)(O)O

MMP Level 2
1: O>>O=P(O)(O)O
2: N#N>>N
3: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
4: O=P(O)(O)O[P]>>O=P(O)(O)O

MMP Level 3
1: O>>O=P(O)(O)O
2: N#N>>N
3: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
4: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00006	[N#N>>N]
2: R:M00003, P:M00007	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
3: R:M00003, P:M00008	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)O]
4: R:M00004, P:M00008	[O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[H+:34].[N:32]#[N:35].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31].[OH2:33]>>[HH:34].[O:1]=[P:2]([OH:4])([OH:33])[OH:3].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31].[NH3:32]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=32, 2=33, 3=35, 4=24, 5=25, 6=26, 7=21, 8=22, 9=23, 10=27, 11=20, 12=19, 13=18, 14=17, 15=28, 16=30, 17=15, 18=16, 19=14, 20=13, 21=10, 22=11, 23=12, 24=9, 25=6, 26=7, 27=8, 28=5, 29=2, 30=1, 31=3, 32=4, 33=31, 34=29, 35=34}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=34, 2=33, 3=20, 4=21, 5=22, 6=17, 7=18, 8=19, 9=23, 10=16, 11=15, 12=14, 13=13, 14=24, 15=26, 16=11, 17=12, 18=10, 19=9, 20=6, 21=7, 22=8, 23=5, 24=2, 25=1, 26=3, 27=4, 28=27, 29=25, 30=30, 31=29, 32=28, 33=31, 34=32}

