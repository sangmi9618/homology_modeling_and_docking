
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0, C-S:1.0]

//
FINGERPRINTS RC
[S([CH2])[CH2]:1.0, [CH2][S+]([CH2])C:2.0, [CH2][S+]([CH2])C>>S([CH2])[CH2]:1.0, [CH2][S+]([CH2])C>>[C]OC:1.0, [CH3]:2.0, [CH]C(=[CH])O:1.0, [CH]C(=[CH])O>>[CH]C(=[CH])OC:1.0, [CH]C(=[CH])OC:1.0, [CH]CSC[CH2]:1.0, [CH]C[S+](C)C[CH2]:1.0, [CH]C[S+](C)C[CH2]>>[CH]C(=[CH])OC:1.0, [CH]C[S+](C)C[CH2]>>[CH]CSC[CH2]:1.0, [C]C=C(O)C=[CH]>>[C]C=C(OC)C=[CH]:1.0, [C]O:1.0, [C]O>>[C]OC:1.0, [C]OC:2.0, [OH]:1.0, [O]:1.0, [O]C:1.0, [O]C([CH])C[S+](C)CC[CH]>>[O]C([CH])CSCC[CH]:1.0, [S+]:1.0, [S+]C:1.0, [S+]C>>[O]C:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH3]:2.0, [O]:1.0, [S+]:1.0]


ID=Reaction Center at Level: 1 (4)
[[CH2][S+]([CH2])C:1.0, [C]OC:1.0, [O]C:1.0, [S+]C:1.0]


ID=Reaction Center at Level: 2 (4)
[[CH2][S+]([CH2])C:1.0, [CH]C(=[CH])OC:1.0, [CH]C[S+](C)C[CH2]:1.0, [C]OC:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [S+]C>>[O]C
2: [C]O>>[C]OC
3: [CH2][S+]([CH2])C>>S([CH2])[CH2]

MMP Level 2
1: [CH2][S+]([CH2])C>>[C]OC
2: [CH]C(=[CH])O>>[CH]C(=[CH])OC
3: [CH]C[S+](C)C[CH2]>>[CH]CSC[CH2]

MMP Level 3
1: [CH]C[S+](C)C[CH2]>>[CH]C(=[CH])OC
2: [C]C=C(O)C=[CH]>>[C]C=C(OC)C=[CH]
3: [O]C([CH])C[S+](C)CC[CH]>>[O]C([CH])CSCC[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(O)C(N)CCSCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=c1c(coc2cc(OC)ccc21)-c3ccc(O)cc3]
3: R:M00002, P:M00004	[O=c1c(coc2cc(O)ccc12)-c3ccc(O)cc3>>O=c1c(coc2cc(OC)ccc21)-c3ccc(O)cc3]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][S+:8]([CH3:9])[CH2:10][CH:11]1[O:12][CH:13]([N:14]:2:[CH:15]:[N:16]:[C:17]:3:[C:18](:[N:19]:[CH:20]:[N:21]:[C:22]32)[NH2:23])[CH:24]([OH:25])[CH:26]1[OH:27].[O:28]=[C:29]1[C:30](=[CH:31][O:32][C:33]:2:[CH:34]:[C:35]([OH:36]):[CH:37]:[CH:38]:[C:39]12)[C:40]:3:[CH:41]:[CH:42]:[C:43]([OH:44]):[CH:45]:[CH:46]3>>[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][S:8][CH2:10][CH:11]1[O:12][CH:13]([N:14]:2:[CH:15]:[N:16]:[C:17]:3:[C:18](:[N:19]:[CH:20]:[N:21]:[C:22]32)[NH2:23])[CH:24]([OH:25])[CH:26]1[OH:27].[O:28]=[C:29]1[C:39]:2:[CH:38]:[CH:37]:[C:35]([O:36][CH3:9]):[CH:34]:[C:33]2[O:32][CH:31]=[C:30]1[C:40]:3:[CH:41]:[CH:42]:[C:43]([OH:44]):[CH:45]:[CH:46]3


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=4, 6=2, 7=1, 8=3, 9=5, 10=10, 11=11, 12=26, 13=24, 14=13, 15=12, 16=14, 17=15, 18=16, 19=17, 20=22, 21=21, 22=20, 23=19, 24=18, 25=23, 26=25, 27=27, 28=41, 29=42, 30=43, 31=45, 32=46, 33=40, 34=30, 35=31, 36=32, 37=33, 38=39, 39=29, 40=28, 41=38, 42=37, 43=35, 44=34, 45=36, 46=44}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=19, 2=20, 3=21, 4=16, 5=17, 6=18, 7=22, 8=15, 9=14, 10=13, 11=12, 12=23, 13=25, 14=10, 15=11, 16=9, 17=8, 18=7, 19=6, 20=4, 21=2, 22=1, 23=3, 24=5, 25=26, 26=24, 27=36, 28=35, 29=34, 30=33, 31=32, 32=39, 33=38, 34=37, 35=28, 36=27, 37=29, 38=30, 39=31, 40=40, 41=41, 42=42, 43=43, 44=45, 45=46, 46=44}

