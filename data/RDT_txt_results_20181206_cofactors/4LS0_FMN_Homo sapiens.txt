
//
FINGERPRINTS BC

ORDER_CHANGED
[C%N*C%N:1.0]

//
FINGERPRINTS RC
[O=C1N=C2C(=N[C]=C([CH])N2C[CH])C(=O)N1>>O=C1NC(=O)C2=N[C]=C([CH])N(C[CH])C2N1:1.0, [CH]:1.0, [C]:1.0, [C]C([N])=NC(=O)[NH]:1.0, [C]C([N])=NC(=O)[NH]>>[C]C([N])NC(=O)[NH]:1.0, [C]C([N])=[N]:1.0, [C]C([N])=[N]>>[C]C([N])[NH]:1.0, [C]C([N])NC(=O)[NH]:1.0, [C]C([N])[NH]:1.0, [C]N([CH2])C1=NC(=O)N[C]C1=[N]>>[C]N([CH2])C1NC(=O)N[C]C1=[N]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]>>[C]NC(C([C])=[N])N([C])[CH2]:1.0, [C]N=[C]:1.0, [C]N=[C]>>[C]N[CH]:1.0, [C]NC(C([C])=[N])N([C])[CH2]:1.0, [C]N[CH]:1.0, [NH]:1.0, [N]:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH]:1.0, [C]:1.0, [NH]:1.0, [N]:1.0]


ID=Reaction Center at Level: 1 (4)
[[C]C([N])=[N]:1.0, [C]C([N])[NH]:1.0, [C]N=[C]:1.0, [C]N[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]C([N])=NC(=O)[NH]:1.0, [C]C([N])NC(=O)[NH]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]NC(C([C])=[N])N([C])[CH2]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]N=[C]>>[C]N[CH]
2: [C]C([N])=[N]>>[C]C([N])[NH]

MMP Level 2
1: [C]C([N])=NC(=O)[NH]>>[C]C([N])NC(=O)[NH]
2: [C]N=C(C([C])=[N])N([C])[CH2]>>[C]NC(C([C])=[N])N([C])[CH2]

MMP Level 3
1: [C]N([CH2])C1=NC(=O)N[C]C1=[N]>>[C]N([CH2])C1NC(=O)N[C]C1=[N]
2: O=C1N=C2C(=N[C]=C([CH])N2C[CH])C(=O)N1>>O=C1NC(=O)C2=N[C]=C([CH])N(C[CH])C2N1


//
REACTION MMP (RPAIR)
1: R:M00002, P:M00003	[O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)O)C)C)c(=O)[nH]1>>O=C1NC(=O)C2=Nc3cc(c(cc3N(CC(O)C(O)C(O)COP(=O)(O)O)C2N1)C)C, O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)O)C)C)c(=O)[nH]1>>O=C1NC(=O)C2=Nc3cc(c(cc3N(CC(O)C(O)C(O)COP(=O)(O)O)C2N1)C)C]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[C:4]2[C:5](=[N:6][C:7]:3:[CH:8]:[C:9](:[C:10](:[CH:11]:[C:12]3[N:13]2[CH2:14][CH:15]([OH:16])[CH:17]([OH:18])[CH:19]([OH:20])[CH2:21][O:22][P:23](=[O:24])([OH:25])[OH:26])[CH3:27])[CH3:28])[C:29](=[O:30])[NH:31]1.[O:41]=[C:40]([OH:42])[CH:38]1[NH:39][C:33](=[O:32])[NH:34][C:35](=[O:36])[CH2:37]1>>[O:1]=[C:2]1[NH:31][C:29](=[O:30])[C:5]2=[N:6][C:7]:3:[CH:8]:[C:9](:[C:10](:[CH:11]:[C:12]3[N:13]([CH2:14][CH:15]([OH:16])[CH:17]([OH:18])[CH:19]([OH:20])[CH2:21][O:22][P:23](=[O:24])([OH:25])[OH:26])[CH:4]2[NH:3]1)[CH3:27])[CH3:28]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=37, 2=38, 3=39, 4=33, 5=32, 6=34, 7=35, 8=36, 9=40, 10=41, 11=42, 12=28, 13=9, 14=8, 15=7, 16=12, 17=11, 18=10, 19=27, 20=13, 21=4, 22=3, 23=2, 24=1, 25=31, 26=29, 27=30, 28=5, 29=6, 30=14, 31=15, 32=17, 33=19, 34=21, 35=22, 36=23, 37=24, 38=25, 39=26, 40=20, 41=18, 42=16}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=31, 2=10, 3=9, 4=8, 5=13, 6=12, 7=11, 8=30, 9=14, 10=28, 11=6, 12=7, 13=4, 14=5, 15=3, 16=2, 17=1, 18=29, 19=15, 20=16, 21=18, 22=20, 23=22, 24=23, 25=24, 26=25, 27=26, 28=27, 29=21, 30=19, 31=17}

