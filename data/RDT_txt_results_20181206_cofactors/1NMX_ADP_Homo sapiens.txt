
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:1.0]

//
FINGERPRINTS RC
[O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, [OH]:1.0, [O]:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:1.0, [P]O:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:1.0, [P]:1.0]


ID=Reaction Center at Level: 1 (2)
[[O]P(=O)(O)O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (2)
[O=P(O)(O)O[P]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[P]>>[P]O

MMP Level 2
1: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O

MMP Level 3
1: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]


//
SELECTED AAM MAPPING
[O:32]=[C:33]1[NH:34][C:35](=[O:36])[N:37]([CH:38]=[C:39]1[CH3:40])[CH:41]2[O:42][CH:43]([CH2:44][O:45][P:46](=[O:47])([OH:48])[OH:49])[CH:50]([F:51])[CH2:52]2.[O:28]=[P:29]([OH:30])([OH:31])[O:1][P:2](=[O:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][CH2:10][CH:11]1[O:12][CH:13]([N:14]:2:[CH:15]:[N:16]:[C:17]:3:[C:18](:[N:19]:[CH:20]:[N:21]:[C:22]32)[NH2:23])[CH:24]([OH:25])[CH:26]1[OH:27]>>[O:3]=[P:2]([OH:1])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][CH2:10][CH:11]1[O:12][CH:13]([N:14]:2:[CH:15]:[N:16]:[C:17]:3:[C:18](:[N:19]:[CH:20]:[N:21]:[C:22]32)[NH2:23])[CH:24]([OH:25])[CH:26]1[OH:27]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=40, 33=39, 34=38, 35=37, 36=35, 37=36, 38=34, 39=33, 40=32, 41=41, 42=52, 43=50, 44=43, 45=42, 46=44, 47=45, 48=46, 49=47, 50=48, 51=49, 52=51}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=17, 5=18, 6=19, 7=23, 8=16, 9=15, 10=14, 11=13, 12=24, 13=26, 14=11, 15=12, 16=10, 17=9, 18=6, 19=7, 20=8, 21=5, 22=2, 23=1, 24=3, 25=4, 26=27, 27=25}

