
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O>>[CH]O:1.0, O>>[C]C(=[C])C(O)C([CH])O:1.0, O>>[C]C([CH])O:1.0, [CH2]:1.0, [CH]:1.0, [CH]O:1.0, [C]C(=[C])C(O)C([CH])O:1.0, [C]C(=[C])CC([CH])O:1.0, [C]C(=[C])CC([CH])O>>[C]C(=[C])C(O)C([CH])O:1.0, [C]C([CH])O:2.0, [C]C1OC(=[CH])C(=C([CH])O)CC1O>>[C]C1OC(=[CH])C(=C([CH])O)C(O)C1O:1.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C([CH])O:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[O:1.0, [CH]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (3)
[O:1.0, [CH]O:1.0, [C]C([CH])O:1.0]


ID=Reaction Center at Level: 2 (3)
[O:1.0, [C]C(=[C])C(O)C([CH])O:1.0, [C]C([CH])O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH2]:1.0, [CH]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]C([CH])O:1.0, [C]C[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[C]C(=[C])C(O)C([CH])O:1.0, [C]C(=[C])CC([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: O>>[CH]O
2: [C]C[CH]>>[C]C([CH])O

MMP Level 2
1: O>>[C]C([CH])O
2: [C]C(=[C])CC([CH])O>>[C]C(=[C])C(O)C([CH])O

MMP Level 3
1: O>>[C]C(=[C])C(O)C([CH])O
2: [C]C1OC(=[CH])C(=C([CH])O)CC1O>>[C]C1OC(=[CH])C(=C([CH])O)C(O)C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00005	[O>>Oc1cc(O)c2c(OC(c3ccc(O)c(O)c3)C(O)C2O)c1]
2: R:M00002, P:M00005	[Oc1cc(O)c2c(OC(c3ccc(O)c(O)c3)C(O)C2)c1>>Oc1cc(O)c2c(OC(c3ccc(O)c(O)c3)C(O)C2O)c1]


//
SELECTED AAM MAPPING
[OH2:22].[OH:1][C:2]:1:[CH:3]:[C:4]([OH:5]):[C:6]2:[C:7]([O:8][CH:9]([C:10]:3:[CH:11]:[CH:12]:[C:13]([OH:14]):[C:15]([OH:16]):[CH:17]3)[CH:18]([OH:19])[CH2:20]2):[CH:21]1>>[H+:71].[O:23]=[C:24]([NH2:25])[C:26]1=[CH:27][N:28]([CH:29]=[CH:30][CH2:31]1)[CH:32]2[O:33][CH:34]([CH2:35][O:36][P:37](=[O:38])([OH:39])[O:40][P:41](=[O:42])([OH:43])[O:44][CH2:45][CH:46]3[O:47][CH:48]([N:49]:4:[CH:50]:[N:51]:[C:52]:5:[C:53](:[N:54]:[CH:55]:[N:56]:[C:57]54)[NH2:58])[CH:59]([O:60][P:61](=[O:62])([OH:63])[OH:64])[CH:65]3[OH:66])[CH:67]([OH:68])[CH:69]2[OH:70].[OH:1][C:2]:1:[CH:3]:[C:4]([OH:5]):[C:6]2:[C:7]([O:8][CH:9]([C:10]:3:[CH:11]:[CH:12]:[C:13]([OH:14]):[C:15]([OH:16]):[CH:17]3)[CH:18]([OH:19])[CH:20]2[OH:22]):[CH:21]1


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=22, 2=20, 3=18, 4=9, 5=8, 6=7, 7=21, 8=2, 9=3, 10=4, 11=6, 12=5, 13=1, 14=10, 15=17, 16=15, 17=13, 18=12, 19=11, 20=14, 21=16, 22=19}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=5, 6=4, 7=2, 8=1, 9=3, 10=10, 11=47, 12=45, 13=12, 14=11, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=43, 28=37, 29=26, 30=25, 31=27, 32=28, 33=29, 34=30, 35=35, 36=34, 37=33, 38=32, 39=31, 40=36, 41=38, 42=39, 43=40, 44=41, 45=42, 46=44, 47=46, 48=48, 49=71, 50=59, 51=60, 52=61, 53=63, 54=65, 55=58, 56=57, 57=66, 58=68, 59=54, 60=52, 61=51, 62=50, 63=70, 64=55, 65=56, 66=49, 67=53, 68=69, 69=67, 70=64, 71=62}

