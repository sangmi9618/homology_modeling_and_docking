
//
FINGERPRINTS BC

FORMED_CLEAVED
[Br-C:1.0, C-S:1.0]

//
FINGERPRINTS RC
[Br:3.0, Br[CH]:1.0, Br[CH]>>Br:1.0, O=C(O)C(Br)CC([CH])=[CH]>>O=C(O)C(SC[CH])CC([CH])=[CH]:1.0, S[CH2]:1.0, S[CH2]>>[CH]S[CH2]:1.0, [Br]:1.0, [CH]:2.0, [CH]CS:1.0, [CH]CS>>[C]C(SC[CH])[CH2]:1.0, [CH]S[CH2]:1.0, [C]C(Br)[CH2]:2.0, [C]C(Br)[CH2]>>Br:1.0, [C]C(Br)[CH2]>>[C]C([S])[CH2]:1.0, [C]C(SC[CH])[CH2]:1.0, [C]C([NH])CS>>[C]CC(SCC([C])[NH])C(=O)O:1.0, [C]C([S])[CH2]:1.0, [C]CC(Br)C(=O)O:1.0, [C]CC(Br)C(=O)O>>Br:1.0, [C]CC(Br)C(=O)O>>[C]CC(S[CH2])C(=O)O:1.0, [C]CC(S[CH2])C(=O)O:1.0, [SH]:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[Br:1.0, [Br]:1.0, [CH]:2.0, [S]:1.0]


ID=Reaction Center at Level: 1 (5)
[Br:1.0, Br[CH]:1.0, [CH]S[CH2]:1.0, [C]C(Br)[CH2]:1.0, [C]C([S])[CH2]:1.0]


ID=Reaction Center at Level: 2 (5)
[Br:1.0, [C]C(Br)[CH2]:1.0, [C]C(SC[CH])[CH2]:1.0, [C]CC(Br)C(=O)O:1.0, [C]CC(S[CH2])C(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: Br[CH]>>Br
2: S[CH2]>>[CH]S[CH2]
3: [C]C(Br)[CH2]>>[C]C([S])[CH2]

MMP Level 2
1: [C]C(Br)[CH2]>>Br
2: [CH]CS>>[C]C(SC[CH])[CH2]
3: [C]CC(Br)C(=O)O>>[C]CC(S[CH2])C(=O)O

MMP Level 3
1: [C]CC(Br)C(=O)O>>Br
2: [C]C([NH])CS>>[C]CC(SCC([C])[NH])C(=O)O
3: O=C(O)C(Br)CC([CH])=[CH]>>O=C(O)C(SC[CH])CC([CH])=[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[[O-][N+](=O)c1ccc(cc1)CC(Br)C(=O)O>>[O-][N+](=O)c1ccc(cc1)CC(SCC(NC(=O)CCC(N)C(=O)O)C(=O)NCC(=O)O)C(=O)O]
2: R:M00001, P:M00004	[[O-][N+](=O)c1ccc(cc1)CC(Br)C(=O)O>>Br]
3: R:M00002, P:M00003	[O=C(O)CNC(=O)C(NC(=O)CCC(N)C(=O)O)CS>>[O-][N+](=O)c1ccc(cc1)CC(SCC(NC(=O)CCC(N)C(=O)O)C(=O)NCC(=O)O)C(=O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH2:4][NH:5][C:6](=[O:7])[CH:8]([NH:9][C:10](=[O:11])[CH2:12][CH2:13][CH:14]([NH2:15])[C:16](=[O:17])[OH:18])[CH2:19][SH:20].[O:21]=[C:22]([OH:23])[CH:24]([Br:25])[CH2:26][C:27]:1:[CH:28]:[CH:29]:[C:30](:[CH:31]:[CH:32]1)[N+:33](=[O:34])[O-:35]>>[BrH:25].[O:1]=[C:2]([OH:3])[CH2:4][NH:5][C:6](=[O:7])[CH:8]([NH:9][C:10](=[O:11])[CH2:12][CH2:13][CH:14]([NH2:15])[C:16](=[O:17])[OH:18])[CH2:19][S:20][CH:24]([C:22](=[O:21])[OH:23])[CH2:26][C:27]:1:[CH:28]:[CH:29]:[C:30](:[CH:31]:[CH:32]1)[N+:33](=[O:34])[O-:35]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=28, 2=29, 3=30, 4=31, 5=32, 6=27, 7=26, 8=24, 9=22, 10=21, 11=23, 12=25, 13=33, 14=34, 15=35, 16=13, 17=12, 18=10, 19=11, 20=9, 21=8, 22=19, 23=20, 24=6, 25=7, 26=5, 27=4, 28=2, 29=1, 30=3, 31=14, 32=16, 33=17, 34=18, 35=15}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=27, 2=28, 3=29, 4=30, 5=31, 6=26, 7=25, 8=21, 9=22, 10=23, 11=24, 12=20, 13=19, 14=8, 15=6, 16=7, 17=5, 18=4, 19=2, 20=1, 21=3, 22=9, 23=10, 24=11, 25=12, 26=13, 27=14, 28=16, 29=17, 30=18, 31=15, 32=32, 33=33, 34=34, 35=35}

