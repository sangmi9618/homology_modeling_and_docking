
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[CH2]>>O=P(O)(O)OP(=O)(O)O[CH2]:1.0, O=P(O)(O)O[P]:2.0, O=P(O)(O)O[P]>>O=P(O)(O)O[P]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:4.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:2.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O>>[P]O[P]:1.0, [P]O[P]:2.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]P(=O)(O)O:2.0, [P]O[P]:2.0]


ID=Reaction Center at Level: 2 (2)
[O=P(O)(O)O[P]:2.0, [O]P(=O)(O)OP(=O)(O)O:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[P]>>[P]O
2: [O]P(=O)(O)O>>[O]P(=O)(O)O
3: [P]O>>[P]O[P]

MMP Level 2
1: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
2: O=P(O)(O)O[P]>>O=P(O)(O)O[P]
3: [O]P(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O

MMP Level 3
1: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
2: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O
3: O=P(O)(O)O[CH2]>>O=P(O)(O)OP(=O)(O)O[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OCC=C(C)C>>O=P(O)(O)OP(=O)(O)OCC=C(C)C]
2: R:M00002, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC=C(C)C]
3: R:M00002, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]


//
SELECTED AAM MAPPING
[O:32]=[P:33]([OH:34])([OH:35])[O:36][CH2:37][CH:38]=[C:39]([CH3:40])[CH3:41].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:1]=[P:2]([OH:3])([OH:4])[O:34][P:33](=[O:32])([OH:35])[O:36][CH2:37][CH:38]=[C:39]([CH3:40])[CH3:41].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=40, 2=39, 3=38, 4=37, 5=36, 6=33, 7=32, 8=34, 9=35, 10=41, 11=24, 12=25, 13=26, 14=21, 15=22, 16=23, 17=27, 18=20, 19=19, 20=18, 21=17, 22=28, 23=30, 24=15, 25=16, 26=14, 27=13, 28=10, 29=11, 30=12, 31=9, 32=6, 33=7, 34=8, 35=5, 36=2, 37=1, 38=3, 39=4, 40=31, 41=29}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=40, 2=39, 3=38, 4=37, 5=36, 6=33, 7=34, 8=35, 9=32, 10=29, 11=28, 12=30, 13=31, 14=41, 15=20, 16=21, 17=22, 18=17, 19=18, 20=19, 21=23, 22=16, 23=15, 24=14, 25=13, 26=24, 27=26, 28=11, 29=12, 30=10, 31=9, 32=6, 33=7, 34=8, 35=5, 36=2, 37=1, 38=3, 39=4, 40=27, 41=25}

