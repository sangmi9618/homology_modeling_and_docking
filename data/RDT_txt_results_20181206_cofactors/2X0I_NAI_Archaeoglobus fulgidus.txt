
//
FINGERPRINTS BC

ORDER_CHANGED
[C-O*C=O:1.0]

//
FINGERPRINTS RC
[O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1:1.0, O=C(O)C(=O)C(=O)O:1.0, O=C(O)C(=O)C(=O)O>>O=C(O)C(O)C(=O)O:3.0, O=C(O)C(O)C(=O)O:1.0, [CH2]:1.0, [CH]:2.0, [CH]N([CH])[CH]:1.0, [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]:1.0, [CH]O:1.0, [CH][N+]([CH])=[CH]:1.0, [C]:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=O:1.0, [C]=O>>[CH]O:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]:1.0, [C]C([C])=O:2.0, [C]C([C])=O>>[C]C([C])O:2.0, [C]C([C])O:2.0, [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O:1.0, [C]C=[CH]:1.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C=[CH]:1.0, [N+]:1.0, [N]:1.0, [OH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH]:1.0, [C]:1.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[CH]O:1.0, [C]=O:1.0, [C]C([C])=O:1.0, [C]C([C])O:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(O)C(=O)C(=O)O:1.0, O=C(O)C(O)C(=O)O:1.0, [C]C([C])=O:1.0, [C]C([C])O:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C[CH]>>[C]C=[CH]
2: [C]C([C])=O>>[C]C([C])O
3: [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]
4: [C]=O>>[CH]O

MMP Level 2
1: [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]
2: O=C(O)C(=O)C(=O)O>>O=C(O)C(O)C(=O)O
3: [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]
4: [C]C([C])=O>>[C]C([C])O

MMP Level 3
1: O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1
2: O=C(O)C(=O)C(=O)O>>O=C(O)C(O)C(=O)O
3: [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O
4: O=C(O)C(=O)C(=O)O>>O=C(O)C(O)C(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(=O)C(=O)O>>O=C(O)C(O)C(=O)O, O=C(O)C(=O)C(=O)O>>O=C(O)C(O)C(=O)O]
2: R:M00002, P:M00004	[O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O]


//
SELECTED AAM MAPPING
[O:45]=[C:46]([OH:47])[C:48](=[O:49])[C:50](=[O:51])[OH:52].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:5][N:6]([CH:7]=[CH:8][CH2:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44]>>[O:45]=[C:46]([OH:47])[CH:48]([OH:49])[C:50](=[O:51])[OH:52].[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:9]:[CH:8]:[CH:7]:[N+:6](:[CH:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=48, 2=49, 3=46, 4=45, 5=47, 6=50, 7=51, 8=52, 9=9, 10=8, 11=7, 12=6, 13=5, 14=4, 15=2, 16=1, 17=3, 18=10, 19=43, 20=41, 21=12, 22=11, 23=13, 24=14, 25=15, 26=16, 27=17, 28=18, 29=19, 30=20, 31=21, 32=22, 33=23, 34=24, 35=39, 36=37, 37=26, 38=25, 39=27, 40=28, 41=29, 42=30, 43=35, 44=34, 45=33, 46=32, 47=31, 48=36, 49=38, 50=40, 51=42, 52=44}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=48, 2=46, 3=45, 4=47, 5=50, 6=51, 7=52, 8=49, 9=6, 10=5, 11=4, 12=9, 13=8, 14=7, 15=10, 16=43, 17=41, 18=12, 19=11, 20=13, 21=14, 22=15, 23=16, 24=17, 25=18, 26=19, 27=20, 28=21, 29=22, 30=23, 31=24, 32=39, 33=37, 34=26, 35=25, 36=27, 37=28, 38=29, 39=30, 40=35, 41=34, 42=33, 43=32, 44=31, 45=36, 46=38, 47=40, 48=42, 49=44, 50=2, 51=1, 52=3}

