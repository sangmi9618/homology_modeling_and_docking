
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=C(O)C[CH2]>>[O-]C(=O)C[CH2]:1.0, O=C(O)[CH2]:1.0, O=C(O)[CH2]>>[O-]C(=O)[CH2]:1.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O[CH2]:1.0, O[CH2]:1.0, O[CH2]>>[P]O[CH2]:1.0, [CH]C(C)(C)CO>>O=P(O)(O)OCC([CH])(C)C:1.0, [C]CO:1.0, [C]CO>>[C]COP(=O)(O)O:1.0, [C]COP(=O)(O)O:1.0, [C]O:1.0, [C]O>>[C][O-]:1.0, [C][O-]:1.0, [O-]:1.0, [O-]C(=O)[CH2]:1.0, [OH]:3.0, [O]:2.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[C]COP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[O]P(=O)(O)O:2.0, [P]O[CH2]:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[P]:1.0, [C]COP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O[CH2]>>[P]O[CH2]
2: [C]O>>[C][O-]
3: [P]O[P]>>[P]O
4: [O]P(=O)(O)O>>[O]P(=O)(O)O

MMP Level 2
1: [C]CO>>[C]COP(=O)(O)O
2: O=C(O)[CH2]>>[O-]C(=O)[CH2]
3: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
4: O=P(O)(O)O[P]>>O=P(O)(O)O[CH2]

MMP Level 3
1: [CH]C(C)(C)CO>>O=P(O)(O)OCC([CH])(C)C
2: O=C(O)C[CH2]>>[O-]C(=O)C[CH2]
3: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
4: [O]P(=O)(O)OP(=O)(O)O>>[C]COP(=O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>[O-]C(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)O]
3: R:M00002, P:M00004	[O=C(O)CCNC(=O)C(O)C(C)(C)CO>>[O-]C(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)O, O=C(O)CCNC(=O)C(O)C(C)(C)CO>>[O-]C(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:32]=[C:33]([OH:34])[CH2:35][CH2:36][NH:37][C:38](=[O:39])[CH:40]([OH:41])[C:42]([CH3:43])([CH3:44])[CH2:45][OH:46].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:32]=[C:33]([O-:34])[CH2:35][CH2:36][NH:37][C:38](=[O:39])[CH:40]([OH:41])[C:42]([CH3:43])([CH3:44])[CH2:45][O:46][P:2](=[O:1])([OH:3])[OH:4].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=43, 33=42, 34=44, 35=45, 36=46, 37=40, 38=38, 39=39, 40=37, 41=36, 42=35, 43=33, 44=32, 45=34, 46=41}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=17, 5=18, 6=19, 7=23, 8=16, 9=15, 10=14, 11=13, 12=24, 13=26, 14=11, 15=12, 16=10, 17=9, 18=6, 19=7, 20=8, 21=5, 22=2, 23=1, 24=3, 25=4, 26=27, 27=25, 28=39, 29=38, 30=40, 31=41, 32=42, 33=43, 34=44, 35=45, 36=46, 37=36, 38=34, 39=35, 40=33, 41=32, 42=31, 43=29, 44=28, 45=30, 46=37}

