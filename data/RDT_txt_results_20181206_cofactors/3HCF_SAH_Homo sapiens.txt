
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-S:1.0]

//
FINGERPRINTS RC
[S([CH2])[CH2]:1.0, [CH2]N:1.0, [CH2]N>>[CH2]NC:1.0, [CH2]NC:2.0, [CH2][S+]([CH2])C:2.0, [CH2][S+]([CH2])C>>S([CH2])[CH2]:1.0, [CH2][S+]([CH2])C>>[CH2]NC:1.0, [CH3]:2.0, [CH]CN:1.0, [CH]CN>>[CH]CNC:1.0, [CH]CNC:1.0, [CH]CSC[CH2]:1.0, [CH]C[S+](C)C[CH2]:1.0, [CH]C[S+](C)C[CH2]>>[CH]CNC:1.0, [CH]C[S+](C)C[CH2]>>[CH]CSC[CH2]:1.0, [C]C(O)CN>>[C]C(O)CNC:1.0, [NH2]:1.0, [NH]:1.0, [NH]C:1.0, [O]C([CH])C[S+](C)CC[CH]>>[O]C([CH])CSCC[CH]:1.0, [S+]:1.0, [S+]C:1.0, [S+]C>>[NH]C:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH3]:2.0, [NH]:1.0, [S+]:1.0]


ID=Reaction Center at Level: 1 (4)
[[CH2]NC:1.0, [CH2][S+]([CH2])C:1.0, [NH]C:1.0, [S+]C:1.0]


ID=Reaction Center at Level: 2 (4)
[[CH2]NC:1.0, [CH2][S+]([CH2])C:1.0, [CH]CNC:1.0, [CH]C[S+](C)C[CH2]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [S+]C>>[NH]C
2: [CH2]N>>[CH2]NC
3: [CH2][S+]([CH2])C>>S([CH2])[CH2]

MMP Level 2
1: [CH2][S+]([CH2])C>>[CH2]NC
2: [CH]CN>>[CH]CNC
3: [CH]C[S+](C)C[CH2]>>[CH]CSC[CH2]

MMP Level 3
1: [CH]C[S+](C)C[CH2]>>[CH]CNC
2: [C]C(O)CN>>[C]C(O)CNC
3: [O]C([CH])C[S+](C)CC[CH]>>[O]C([CH])CSCC[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(O)C(N)CCSCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>OC(c1ccccc1)CNC]
3: R:M00002, P:M00004	[OC(c1ccccc1)CN>>OC(c1ccccc1)CNC]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][S+:8]([CH3:9])[CH2:10][CH:11]1[O:12][CH:13]([N:14]:2:[CH:15]:[N:16]:[C:17]:3:[C:18](:[N:19]:[CH:20]:[N:21]:[C:22]32)[NH2:23])[CH:24]([OH:25])[CH:26]1[OH:27].[OH:28][CH:29]([C:30]:1:[CH:31]:[CH:32]:[CH:33]:[CH:34]:[CH:35]1)[CH2:36][NH2:37]>>[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][S:8][CH2:10][CH:11]1[O:12][CH:13]([N:14]:2:[CH:15]:[N:16]:[C:17]:3:[C:18](:[N:19]:[CH:20]:[N:21]:[C:22]32)[NH2:23])[CH:24]([OH:25])[CH:26]1[OH:27].[OH:28][CH:29]([C:30]:1:[CH:31]:[CH:32]:[CH:33]:[CH:34]:[CH:35]1)[CH2:36][NH:37][CH3:9]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=4, 6=2, 7=1, 8=3, 9=5, 10=10, 11=11, 12=26, 13=24, 14=13, 15=12, 16=14, 17=15, 18=16, 19=17, 20=22, 21=21, 22=20, 23=19, 24=18, 25=23, 26=25, 27=27, 28=33, 29=32, 30=31, 31=30, 32=35, 33=34, 34=29, 35=36, 36=37, 37=28}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=19, 2=20, 3=21, 4=16, 5=17, 6=18, 7=22, 8=15, 9=14, 10=13, 11=12, 12=23, 13=25, 14=10, 15=11, 16=9, 17=8, 18=7, 19=6, 20=4, 21=2, 22=1, 23=3, 24=5, 25=26, 26=24, 27=37, 28=36, 29=35, 30=28, 31=29, 32=30, 33=31, 34=32, 35=33, 36=34, 37=27}

