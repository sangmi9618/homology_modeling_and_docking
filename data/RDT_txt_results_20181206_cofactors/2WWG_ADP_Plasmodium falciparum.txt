
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[CH2]>>O=P(O)(O)OP(=O)(O)O[CH2]:1.0, O=P(O)(O)O[P]:2.0, O=P(O)(O)O[P]>>O=P(O)(O)O[P]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:4.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:2.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O>>[P]O[P]:1.0, [P]O[P]:2.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]P(=O)(O)O:2.0, [P]O[P]:2.0]


ID=Reaction Center at Level: 2 (2)
[O=P(O)(O)O[P]:2.0, [O]P(=O)(O)OP(=O)(O)O:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O>>[P]O[P]
2: [P]O[P]>>[P]O
3: [O]P(=O)(O)O>>[O]P(=O)(O)O

MMP Level 2
1: [O]P(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O
2: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
3: O=P(O)(O)O[P]>>O=P(O)(O)O[P]

MMP Level 3
1: O=P(O)(O)O[CH2]>>O=P(O)(O)OP(=O)(O)O[CH2]
2: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
3: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)O)C(O)C3]
3: R:M00002, P:M00004	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)O)C(O)C3>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)O)C(O)C3]


//
SELECTED AAM MAPPING
[O:32]=[C:33]1[N:34]=[C:35]([NH2:36])[NH:37][C:38]:2:[C:39]1:[N:40]:[CH:41]:[N:42]2[CH:43]3[O:44][CH:45]([CH2:46][O:47][P:48](=[O:49])([OH:50])[OH:51])[CH:52]([OH:53])[CH2:54]3.[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:32]=[C:33]1[N:34]=[C:35]([NH2:36])[NH:37][C:38]:2:[C:39]1:[N:40]:[CH:41]:[N:42]2[CH:43]3[O:44][CH:45]([CH2:46][O:47][P:48](=[O:49])([OH:50])[O:51][P:2](=[O:1])([OH:3])[OH:4])[CH:52]([OH:53])[CH2:54]3.[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=54, 33=52, 34=45, 35=44, 36=43, 37=42, 38=41, 39=40, 40=39, 41=38, 42=37, 43=35, 44=34, 45=33, 46=32, 47=36, 48=46, 49=47, 50=48, 51=49, 52=50, 53=51, 54=53}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=47, 2=48, 3=49, 4=44, 5=45, 6=46, 7=50, 8=43, 9=42, 10=41, 11=40, 12=51, 13=53, 14=38, 15=39, 16=37, 17=36, 18=33, 19=34, 20=35, 21=32, 22=29, 23=28, 24=30, 25=31, 26=54, 27=52, 28=27, 29=25, 30=14, 31=13, 32=12, 33=11, 34=10, 35=9, 36=8, 37=7, 38=6, 39=4, 40=3, 41=2, 42=1, 43=5, 44=15, 45=16, 46=17, 47=18, 48=19, 49=20, 50=21, 51=22, 52=23, 53=24, 54=26}

