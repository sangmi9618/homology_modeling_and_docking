
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0, O-P:2.0]

//
FINGERPRINTS RC
[O=C(O)C=1N=C[N]C1N>>[C]C([CH2])NC(=O)C=1N=C[N]C1N:1.0, O=P(O)(O)O:3.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O:1.0, [CH]N:1.0, [CH]N>>[C]N[CH]:1.0, [C]:2.0, [C]=C([N])C(=O)N[CH]:1.0, [C]=C([N])C(=O)O:1.0, [C]=C([N])C(=O)O>>O=P(O)(O)O:1.0, [C]=C([N])C(=O)O>>[C]=C([N])C(=O)N[CH]:1.0, [C]C(=O)NC([C])[CH2]:1.0, [C]C(=O)O:2.0, [C]C(=O)O>>O=P(O)(O)O:1.0, [C]C(=O)O>>[C]C(=O)[NH]:1.0, [C]C(=O)[NH]:1.0, [C]C([CH2])N:1.0, [C]C([CH2])N>>[C]C(=O)NC([C])[CH2]:1.0, [C]CC(N)C(=O)O>>[C]CC(NC(=O)C(=[C])[N])C(=O)O:1.0, [C]N[CH]:1.0, [C]O:1.0, [C]O>>[P]O:1.0, [NH2]:1.0, [NH]:1.0, [OH]:3.0, [O]:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[C]:2.0, [NH]:1.0, [OH]:2.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (8)
[O=P(O)(O)O:1.0, [C]C(=O)O:1.0, [C]C(=O)[NH]:1.0, [C]N[CH]:1.0, [C]O:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (7)
[O=P(O)(O)O:2.0, O=P(O)(O)O[P]:1.0, [C]=C([N])C(=O)N[CH]:1.0, [C]=C([N])C(=O)O:1.0, [C]C(=O)NC([C])[CH2]:1.0, [C]C(=O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH]N>>[C]N[CH]
2: [P]O[P]>>[P]O
3: [C]O>>[P]O
4: [O]P(=O)(O)O>>O=P(O)(O)O
5: [C]C(=O)O>>[C]C(=O)[NH]

MMP Level 2
1: [C]C([CH2])N>>[C]C(=O)NC([C])[CH2]
2: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
3: [C]C(=O)O>>O=P(O)(O)O
4: O=P(O)(O)O[P]>>O=P(O)(O)O
5: [C]=C([N])C(=O)O>>[C]=C([N])C(=O)N[CH]

MMP Level 3
1: [C]CC(N)C(=O)O>>[C]CC(NC(=O)C(=[C])[N])C(=O)O
2: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
3: [C]=C([N])C(=O)O>>O=P(O)(O)O
4: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O
5: O=C(O)C=1N=C[N]C1N>>[C]C([CH2])NC(=O)C=1N=C[N]C1N


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00005	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)O]
3: R:M00002, P:M00005	[O=C(O)c1ncn(c1N)C2OC(COP(=O)(O)O)C(O)C2O>>O=P(O)(O)O]
4: R:M00002, P:M00006	[O=C(O)c1ncn(c1N)C2OC(COP(=O)(O)O)C(O)C2O>>O=C(O)CC(NC(=O)c1ncn(c1N)C2OC(COP(=O)(O)O)C(O)C2O)C(=O)O]
5: R:M00003, P:M00006	[O=C(O)CC(N)C(=O)O>>O=C(O)CC(NC(=O)c1ncn(c1N)C2OC(COP(=O)(O)O)C(O)C2O)C(=O)O]


//
SELECTED AAM MAPPING
[O:32]=[C:33]([OH:34])[C:35]:1:[N:36]:[CH:37]:[N:38](:[C:39]1[NH2:40])[CH:41]2[O:42][CH:43]([CH2:44][O:45][P:46](=[O:47])([OH:48])[OH:49])[CH:50]([OH:51])[CH:52]2[OH:53].[O:54]=[C:55]([OH:56])[CH2:57][CH:58]([NH2:59])[C:60](=[O:61])[OH:62].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:54]=[C:55]([OH:56])[CH2:57][CH:58]([NH:59][C:33](=[O:32])[C:35]:1:[N:36]:[CH:37]:[N:38](:[C:39]1[NH2:40])[CH:41]2[O:42][CH:43]([CH2:44][O:45][P:46](=[O:47])([OH:48])[OH:49])[CH:50]([OH:51])[CH:52]2[OH:53])[C:60](=[O:61])[OH:62].[O:1]=[P:2]([OH:3])([OH:4])[OH:34].[O:7]=[P:6]([OH:8])([OH:5])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=37, 33=36, 34=35, 35=39, 36=38, 37=41, 38=52, 39=50, 40=43, 41=42, 42=44, 43=45, 44=46, 45=47, 46=48, 47=49, 48=51, 49=53, 50=40, 51=33, 52=32, 53=34, 54=57, 55=58, 56=60, 57=61, 58=62, 59=59, 60=55, 61=54, 62=56}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=50, 2=51, 3=52, 4=47, 5=48, 6=49, 7=53, 8=46, 9=45, 10=44, 11=43, 12=54, 13=56, 14=41, 15=42, 16=40, 17=39, 18=36, 19=37, 20=38, 21=35, 22=32, 23=31, 24=33, 25=34, 26=57, 27=55, 28=60, 29=59, 30=58, 31=61, 32=62, 33=11, 34=10, 35=9, 36=13, 37=12, 38=15, 39=26, 40=24, 41=17, 42=16, 43=18, 44=19, 45=20, 46=21, 47=22, 48=23, 49=25, 50=27, 51=14, 52=7, 53=8, 54=6, 55=5, 56=4, 57=2, 58=1, 59=3, 60=28, 61=29, 62=30}

