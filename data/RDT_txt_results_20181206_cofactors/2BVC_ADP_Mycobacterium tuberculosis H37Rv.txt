
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0, O-P:2.0]

//
FINGERPRINTS RC
[N:3.0, N>>O=C(N)C[CH2]:1.0, N>>O=C([CH2])N:1.0, N>>[C]N:1.0, O=C(N)C[CH2]:1.0, O=C(O)CC[CH]>>O=C(N)CC[CH]:1.0, O=C(O)C[CH2]:1.0, O=C(O)C[CH2]>>O=C(N)C[CH2]:1.0, O=C(O)C[CH2]>>O=P(O)(O)O:1.0, O=C(O)[CH2]:2.0, O=C(O)[CH2]>>O=C([CH2])N:1.0, O=C(O)[CH2]>>O=P(O)(O)O:1.0, O=C([CH2])N:2.0, O=P(O)(O)O:3.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O:1.0, [C]:2.0, [C]N:1.0, [C]O:1.0, [C]O>>[P]O:1.0, [NH2]:1.0, [OH]:3.0, [O]:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (6)
[N:1.0, [C]:2.0, [NH2]:1.0, [OH]:2.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (9)
[N:1.0, O=C(O)[CH2]:1.0, O=C([CH2])N:1.0, O=P(O)(O)O:1.0, [C]N:1.0, [C]O:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (8)
[N:1.0, O=C(N)C[CH2]:1.0, O=C(O)C[CH2]:1.0, O=C(O)[CH2]:1.0, O=C([CH2])N:1.0, O=P(O)(O)O:2.0, O=P(O)(O)O[P]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=C(O)[CH2]>>O=C([CH2])N
2: [O]P(=O)(O)O>>O=P(O)(O)O
3: N>>[C]N
4: [P]O[P]>>[P]O
5: [C]O>>[P]O

MMP Level 2
1: O=C(O)C[CH2]>>O=C(N)C[CH2]
2: O=P(O)(O)O[P]>>O=P(O)(O)O
3: N>>O=C([CH2])N
4: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
5: O=C(O)[CH2]>>O=P(O)(O)O

MMP Level 3
1: O=C(O)CC[CH]>>O=C(N)CC[CH]
2: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O
3: N>>O=C(N)C[CH2]
4: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
5: O=C(O)C[CH2]>>O=P(O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00005	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)O]
3: R:M00002, P:M00005	[O=C(O)CCC(N)C(=O)O>>O=P(O)(O)O]
4: R:M00002, P:M00006	[O=C(O)CCC(N)C(=O)O>>O=C(O)C(N)CCC(=O)N]
5: R:M00003, P:M00006	[N>>O=C(O)C(N)CCC(=O)N]


//
SELECTED AAM MAPPING
[O:32]=[C:33]([OH:34])[CH2:35][CH2:36][CH:37]([NH2:38])[C:39](=[O:40])[OH:41].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31].[NH3:42]>>[O:40]=[C:39]([OH:41])[CH:37]([NH2:38])[CH2:36][CH2:35][C:33](=[O:32])[NH2:42].[O:1]=[P:2]([OH:3])([OH:4])[OH:34].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=36, 33=35, 34=33, 35=32, 36=34, 37=37, 38=39, 39=40, 40=41, 41=38, 42=42}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=17, 5=18, 6=19, 7=23, 8=16, 9=15, 10=14, 11=13, 12=24, 13=26, 14=11, 15=12, 16=10, 17=9, 18=6, 19=7, 20=8, 21=5, 22=2, 23=1, 24=3, 25=4, 26=27, 27=25, 28=40, 29=39, 30=38, 31=41, 32=42, 33=33, 34=34, 35=35, 36=36, 37=37, 38=31, 39=29, 40=28, 41=30, 42=32}

