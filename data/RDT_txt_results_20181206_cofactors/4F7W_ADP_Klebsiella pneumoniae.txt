
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

ORDER_CHANGED
[C-N*C=N:1.0, C-O*C=O:1.0]

//
FINGERPRINTS RC
[O=C(NC[CH2])C(O)C([CH2])(C)C>>[O-]C(=NC[CH2])C(O)C([CH2])(C)C:1.0, O=C(O)C[CH2]>>[O-]C(=O)C[CH2]:1.0, O=C(O)[CH2]:1.0, O=C(O)[CH2]>>[O-]C(=O)[CH2]:1.0, O=C([CH])NC[CH2]:1.0, O=C([CH])NC[CH2]>>[O-]C([CH])=NC[CH2]:1.0, O=C([CH])[NH]:2.0, O=C([CH])[NH]>>[N]=C([O-])[CH]:2.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>[O-]P(=O)(O)O[CH2]:2.0, O[CH2]:1.0, O[CH2]>>[P]O[CH2]:1.0, [CH]C(C)(C)CO>>[O-]P(=O)(O)OCC([CH])(C)C:1.0, [C]:2.0, [C]=N[CH2]:1.0, [C]=O:1.0, [C]=O>>[C][O-]:1.0, [C]C(O)C(=O)N[CH2]:1.0, [C]C(O)C(=O)N[CH2]>>[C]C(O)C([O-])=N[CH2]:2.0, [C]C(O)C([O-])=N[CH2]:1.0, [C]CCNC(=O)C([C])O>>[C]CCN=C([O-])C([C])O:1.0, [C]CO:1.0, [C]CO>>[C]COP([O-])(=O)O:1.0, [C]COP([O-])(=O)O:1.0, [C]N[CH2]:1.0, [C]N[CH2]>>[C]=N[CH2]:1.0, [C]O:1.0, [C]O>>[C][O-]:1.0, [C][O-]:2.0, [NH]:1.0, [N]:1.0, [N]=C([O-])[CH]:2.0, [O-]:3.0, [O-]C(=O)[CH2]:1.0, [O-]C([CH])=NC[CH2]:1.0, [O-]P(=O)(O)O[CH2]:1.0, [O-][P]:1.0, [OH]:4.0, [O]:3.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P([O-])(=O)O:2.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[C]COP([O-])(=O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P([O-])(=O)O:2.0, [P]:2.0, [P]O:2.0, [P]O>>[O-][P]:1.0, [P]O[CH2]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (4)
[[O]P(=O)(O)O:1.0, [O]P([O-])(=O)O:1.0, [P]O[CH2]:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)O[P]:1.0, [C]COP([O-])(=O)O:1.0, [O-]P(=O)(O)O[CH2]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (5)
[[C]:2.0, [NH]:1.0, [N]:1.0, [O-]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (6)
[O=C([CH])[NH]:1.0, [C]=N[CH2]:1.0, [C]=O:1.0, [C]N[CH2]:1.0, [C][O-]:1.0, [N]=C([O-])[CH]:1.0]


ID=Reaction Center at Level: 2 (6)
[O=C([CH])NC[CH2]:1.0, O=C([CH])[NH]:1.0, [C]C(O)C(=O)N[CH2]:1.0, [C]C(O)C([O-])=N[CH2]:1.0, [N]=C([O-])[CH]:1.0, [O-]C([CH])=NC[CH2]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O[CH2]>>[P]O[CH2]
2: [O]P(=O)(O)O>>[O]P([O-])(=O)O
3: [C]=O>>[C][O-]
4: [P]O[P]>>[P]O
5: [C]N[CH2]>>[C]=N[CH2]
6: [P]O>>[O-][P]
7: [C]O>>[C][O-]
8: O=C([CH])[NH]>>[N]=C([O-])[CH]

MMP Level 2
1: [C]CO>>[C]COP([O-])(=O)O
2: O=P(O)(O)O[P]>>[O-]P(=O)(O)O[CH2]
3: O=C([CH])[NH]>>[N]=C([O-])[CH]
4: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
5: O=C([CH])NC[CH2]>>[O-]C([CH])=NC[CH2]
6: [O]P(=O)(O)O>>[O]P([O-])(=O)O
7: O=C(O)[CH2]>>[O-]C(=O)[CH2]
8: [C]C(O)C(=O)N[CH2]>>[C]C(O)C([O-])=N[CH2]

MMP Level 3
1: [CH]C(C)(C)CO>>[O-]P(=O)(O)OCC([CH])(C)C
2: [O]P(=O)(O)OP(=O)(O)O>>[C]COP([O-])(=O)O
3: [C]C(O)C(=O)N[CH2]>>[C]C(O)C([O-])=N[CH2]
4: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
5: [C]CCNC(=O)C([C])O>>[C]CCN=C([O-])C([C])O
6: O=P(O)(O)O[P]>>[O-]P(=O)(O)O[CH2]
7: O=C(O)C[CH2]>>[O-]C(=O)C[CH2]
8: O=C(NC[CH2])C(O)C([CH2])(C)C>>[O-]C(=NC[CH2])C(O)C([CH2])(C)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>[O-]C(=O)CCN=C([O-])C(O)C(C)(C)COP([O-])(=O)O, O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>[O-]C(=O)CCN=C([O-])C(O)C(C)(C)COP([O-])(=O)O]
3: R:M00002, P:M00004	[O=C(O)CCNC(=O)C(O)C(C)(C)CO>>[O-]C(=O)CCN=C([O-])C(O)C(C)(C)COP([O-])(=O)O, O=C(O)CCNC(=O)C(O)C(C)(C)CO>>[O-]C(=O)CCN=C([O-])C(O)C(C)(C)COP([O-])(=O)O, O=C(O)CCNC(=O)C(O)C(C)(C)CO>>[O-]C(=O)CCN=C([O-])C(O)C(C)(C)COP([O-])(=O)O, O=C(O)CCNC(=O)C(O)C(C)(C)CO>>[O-]C(=O)CCN=C([O-])C(O)C(C)(C)COP([O-])(=O)O, O=C(O)CCNC(=O)C(O)C(C)(C)CO>>[O-]C(=O)CCN=C([O-])C(O)C(C)(C)COP([O-])(=O)O]


//
SELECTED AAM MAPPING
[O:32]=[C:33]([OH:34])[CH2:35][CH2:36][NH:37][C:38](=[O:39])[CH:40]([OH:41])[C:42]([CH3:43])([CH3:44])[CH2:45][OH:46].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:32]=[C:33]([O-:34])[CH2:35][CH2:36][N:37]=[C:38]([O-:39])[CH:40]([OH:41])[C:42]([CH3:43])([CH3:44])[CH2:45][O:46][P:2](=[O:1])([O-:3])[OH:4].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=43, 33=42, 34=44, 35=45, 36=46, 37=40, 38=38, 39=39, 40=37, 41=36, 42=35, 43=33, 44=32, 45=34, 46=41}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=17, 5=18, 6=19, 7=23, 8=16, 9=15, 10=14, 11=13, 12=24, 13=26, 14=11, 15=12, 16=10, 17=9, 18=6, 19=7, 20=8, 21=5, 22=2, 23=1, 24=3, 25=4, 26=27, 27=25, 28=39, 29=38, 30=40, 31=41, 32=42, 33=43, 34=44, 35=46, 36=45, 37=36, 38=34, 39=33, 40=32, 41=31, 42=29, 43=28, 44=30, 45=35, 46=37}

