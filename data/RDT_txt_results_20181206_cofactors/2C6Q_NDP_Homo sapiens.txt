
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0]

//
FINGERPRINTS RC
[N:3.0, O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1:1.0, [CH2]:1.0, [CH]:2.0, [CH]N([CH])[CH]:1.0, [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]:1.0, [C]:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]:1.0, [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O:1.0, [C]C=[CH]:1.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C=[CH]:1.0, [C]N:1.0, [C]N=C(N)N[C]:1.0, [C]N=C(N)N[C]>>N:1.0, [C]N=C(N)N[C]>>[C]N=CN[C]:1.0, [C]N=CN[C]:1.0, [C]N>>N:1.0, [N+]:1.0, [NH2]:1.0, [N]:1.0, [N]=C([NH])N:2.0, [N]=C([NH])N>>N:1.0, [N]=C([NH])N>>[N]=C[NH]:1.0, [N]=C[NH]:1.0, [N]C1=[C]C(=O)N=C(N)N1>>[N]C1=[C]C(=O)N=CN1:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[N:1.0, [C]:1.0, [NH2]:1.0]


ID=Reaction Center at Level: 1 (3)
[N:1.0, [C]N:1.0, [N]=C([NH])N:1.0]


ID=Reaction Center at Level: 2 (3)
[N:1.0, [C]N=C(N)N[C]:1.0, [N]=C([NH])N:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]
2: [C]C[CH]>>[C]C=[CH]
3: [C]N>>N
4: [N]=C([NH])N>>[N]=C[NH]

MMP Level 2
1: [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]
2: [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]
3: [N]=C([NH])N>>N
4: [C]N=C(N)N[C]>>[C]N=CN[C]

MMP Level 3
1: [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O
2: O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1
3: [C]N=C(N)N[C]>>N
4: [N]C1=[C]C(=O)N=C(N)N1>>[N]C1=[C]C(=O)N=CN1


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00006	[O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O]
2: R:M00002, P:M00004	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O>>N]
3: R:M00002, P:M00005	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O>>O=c1nc[nH]c2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O]


//
SELECTED AAM MAPPING
[H+:73].[O:49]=[C:50]1[N:51]=[C:52]([NH2:53])[NH:54][C:55]:2:[C:56]1:[N:57]:[CH:58]:[N:59]2[CH:60]3[O:61][CH:62]([CH2:63][O:64][P:65](=[O:66])([OH:67])[OH:68])[CH:69]([OH:70])[CH:71]3[OH:72].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:5][N:6]([CH:7]=[CH:8][CH2:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48]>>[O:49]=[C:50]1[N:51]=[CH:52][NH:54][C:55]:2:[C:56]1:[N:57]:[CH:58]:[N:59]2[CH:60]3[O:61][CH:62]([CH2:63][O:64][P:65](=[O:66])([OH:67])[OH:68])[CH:69]([OH:70])[CH:71]3[OH:72].[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:9]:[CH:8]:[CH:7]:[N+:6](:[CH:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48].[NH3:53]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=5, 6=4, 7=2, 8=1, 9=3, 10=10, 11=47, 12=45, 13=12, 14=11, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=43, 28=37, 29=26, 30=25, 31=27, 32=28, 33=29, 34=30, 35=35, 36=34, 37=33, 38=32, 39=31, 40=36, 41=38, 42=39, 43=40, 44=41, 45=42, 46=44, 47=46, 48=48, 49=58, 50=57, 51=56, 52=55, 53=59, 54=60, 55=71, 56=69, 57=62, 58=61, 59=63, 60=64, 61=65, 62=66, 63=67, 64=68, 65=70, 66=72, 67=54, 68=52, 69=51, 70=50, 71=49, 72=53, 73=73}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=72, 2=52, 3=51, 4=50, 5=49, 6=55, 7=54, 8=53, 9=58, 10=57, 11=56, 12=59, 13=70, 14=68, 15=61, 16=60, 17=62, 18=63, 19=64, 20=65, 21=66, 22=67, 23=69, 24=71, 25=6, 26=5, 27=4, 28=9, 29=8, 30=7, 31=10, 32=47, 33=45, 34=12, 35=11, 36=13, 37=14, 38=15, 39=16, 40=17, 41=18, 42=19, 43=20, 44=21, 45=22, 46=23, 47=24, 48=43, 49=37, 50=26, 51=25, 52=27, 53=28, 54=29, 55=30, 56=35, 57=34, 58=33, 59=32, 60=31, 61=36, 62=38, 63=39, 64=40, 65=41, 66=42, 67=44, 68=46, 69=48, 70=2, 71=1, 72=3}

