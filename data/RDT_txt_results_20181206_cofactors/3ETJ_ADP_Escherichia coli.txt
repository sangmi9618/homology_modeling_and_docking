
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0, O-P:2.0]

//
FINGERPRINTS RC
[O=C([NH])O:1.0, O=P(O)(O)O:3.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O:1.0, [CH]n1cncc1N>>O=C(O)Nc1cncn1[CH]:1.0, [C]:2.0, [C]N:1.0, [C]N>>[C]N[C]:1.0, [C]NC(=O)O:1.0, [C]N[C]:1.0, [C][O-]:1.0, [C][O-]>>[P]O:1.0, [NH2]:1.0, [NH]:1.0, [N]C(=[CH])N:1.0, [N]C(=[CH])N>>[N]C(=[CH])NC(=O)O:1.0, [N]C(=[CH])NC(=O)O:1.0, [O-]:1.0, [O-]C(=O)O:3.0, [O-]C(=O)O>>O=C([NH])O:1.0, [O-]C(=O)O>>O=P(O)(O)O:2.0, [O-]C(=O)O>>[C]NC(=O)O:1.0, [O-]C(=O)O>>[N]C(=[CH])NC(=O)O:1.0, [OH]:2.0, [O]:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (6)
[[C]:2.0, [NH]:1.0, [O-]:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (8)
[O=C([NH])O:1.0, O=P(O)(O)O:1.0, [C]N[C]:1.0, [C][O-]:1.0, [O-]C(=O)O:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (6)
[O=P(O)(O)O:2.0, O=P(O)(O)O[P]:1.0, [C]NC(=O)O:1.0, [N]C(=[CH])NC(=O)O:1.0, [O-]C(=O)O:2.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]P(=O)(O)O>>O=P(O)(O)O
2: [C]N>>[C]N[C]
3: [O-]C(=O)O>>O=C([NH])O
4: [P]O[P]>>[P]O
5: [C][O-]>>[P]O

MMP Level 2
1: O=P(O)(O)O[P]>>O=P(O)(O)O
2: [N]C(=[CH])N>>[N]C(=[CH])NC(=O)O
3: [O-]C(=O)O>>[C]NC(=O)O
4: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
5: [O-]C(=O)O>>O=P(O)(O)O

MMP Level 3
1: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O
2: [CH]n1cncc1N>>O=C(O)Nc1cncn1[CH]
3: [O-]C(=O)O>>[N]C(=[CH])NC(=O)O
4: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
5: [O-]C(=O)O>>O=P(O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00006	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)O]
3: R:M00002, P:M00005	[O=P(O)(O)OCC1OC(n2cncc2N)C(O)C1O>>O=C(O)Nc1cncn1C2OC(COP(=O)(O)O)C(O)C2O]
4: R:M00003, P:M00005	[[O-]C(=O)O>>O=C(O)Nc1cncn1C2OC(COP(=O)(O)O)C(O)C2O]
5: R:M00003, P:M00006	[[O-]C(=O)O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[O:51]=[C:52]([O-:53])[OH:54].[O:32]=[P:33]([OH:34])([OH:35])[O:36][CH2:37][CH:38]1[O:39][CH:40]([N:41]:2:[CH:42]:[N:43]:[CH:44]:[C:45]2[NH2:46])[CH:47]([OH:48])[CH:49]1[OH:50].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:51]=[C:52]([OH:54])[NH:46][C:45]:1:[CH:44]:[N:43]:[CH:42]:[N:41]1[CH:40]2[O:39][CH:38]([CH2:37][O:36][P:33](=[O:32])([OH:35])[OH:34])[CH:49]([OH:50])[CH:47]2[OH:48].[O:1]=[P:2]([OH:3])([OH:4])[OH:53].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=44, 33=45, 34=41, 35=42, 36=43, 37=40, 38=47, 39=49, 40=38, 41=39, 42=37, 43=36, 44=33, 45=32, 46=34, 47=35, 48=50, 49=48, 50=46, 51=52, 52=51, 53=54, 54=53}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=17, 5=18, 6=19, 7=23, 8=16, 9=15, 10=14, 11=13, 12=24, 13=26, 14=11, 15=12, 16=10, 17=9, 18=6, 19=7, 20=8, 21=5, 22=2, 23=1, 24=3, 25=4, 26=27, 27=25, 28=33, 29=32, 30=36, 31=35, 32=34, 33=37, 34=48, 35=46, 36=39, 37=38, 38=40, 39=41, 40=42, 41=43, 42=44, 43=45, 44=47, 45=49, 46=31, 47=29, 48=28, 49=30, 50=52, 51=51, 52=50, 53=53, 54=54}

