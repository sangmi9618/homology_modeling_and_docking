
//
FINGERPRINTS BC

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1:1.0, O=C1OCC(C)(C)C1O:1.0, O=C1OCC(C1=O)(C)C:1.0, O=C1OCC(C1=O)(C)C>>O=C1OCC(C)(C)C1O:3.0, [CH2]:1.0, [CH]:2.0, [CH]N([CH])[CH]:1.0, [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]:1.0, [CH]O:1.0, [CH][N+]([CH])=[CH]:1.0, [C]:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=O:1.0, [C]=O>>[CH]O:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]:1.0, [C]C([C])=O:2.0, [C]C([C])=O>>[C]C([C])O:2.0, [C]C([C])O:2.0, [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O:1.0, [C]C=[CH]:1.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C=[CH]:1.0, [N+]:1.0, [N]:1.0, [OH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH]:1.0, [C]:1.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[CH]O:1.0, [C]=O:1.0, [C]C([C])=O:1.0, [C]C([C])O:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C1OCC(C)(C)C1O:1.0, O=C1OCC(C1=O)(C)C:1.0, [C]C([C])=O:1.0, [C]C([C])O:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]C([C])=O:1.0, [C]C([C])O:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C1OCC(C)(C)C1O:1.0, O=C1OCC(C1=O)(C)C:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]
2: [C]=O>>[CH]O
3: [C]C[CH]>>[C]C=[CH]
4: [C]C([C])=O>>[C]C([C])O

MMP Level 2
1: [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]
2: [C]C([C])=O>>[C]C([C])O
3: [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]
4: O=C1OCC(C1=O)(C)C>>O=C1OCC(C)(C)C1O

MMP Level 3
1: [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O
2: O=C1OCC(C1=O)(C)C>>O=C1OCC(C)(C)C1O
3: O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1
4: O=C1OCC(C1=O)(C)C>>O=C1OCC(C)(C)C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C1OCC(C1=O)(C)C>>O=C1OCC(C)(C)C1O, O=C1OCC(C1=O)(C)C>>O=C1OCC(C)(C)C1O]
2: R:M00002, P:M00005	[O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O]


//
SELECTED AAM MAPPING
[H+:58].[O:49]=[C:50]1[O:51][CH2:52][C:53]([C:54]1=[O:55])([CH3:56])[CH3:57].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:5][N:6]([CH:7]=[CH:8][CH2:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48]>>[O:49]=[C:50]1[O:51][CH2:52][C:53]([CH3:56])([CH3:57])[CH:54]1[OH:55].[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:9]:[CH:8]:[CH:7]:[N+:6](:[CH:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=56, 2=53, 3=52, 4=51, 5=50, 6=49, 7=54, 8=55, 9=57, 10=9, 11=8, 12=7, 13=6, 14=5, 15=4, 16=2, 17=1, 18=3, 19=10, 20=47, 21=45, 22=12, 23=11, 24=13, 25=14, 26=15, 27=16, 28=17, 29=18, 30=19, 31=20, 32=21, 33=22, 34=23, 35=24, 36=43, 37=37, 38=26, 39=25, 40=27, 41=28, 42=29, 43=30, 44=35, 45=34, 46=33, 47=32, 48=31, 49=36, 50=38, 51=39, 52=40, 53=41, 54=42, 55=44, 56=46, 57=48, 58=58}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=54, 2=53, 3=52, 4=51, 5=50, 6=49, 7=56, 8=57, 9=55, 10=6, 11=5, 12=4, 13=9, 14=8, 15=7, 16=10, 17=47, 18=45, 19=12, 20=11, 21=13, 22=14, 23=15, 24=16, 25=17, 26=18, 27=19, 28=20, 29=21, 30=22, 31=23, 32=24, 33=43, 34=37, 35=26, 36=25, 37=27, 38=28, 39=29, 40=30, 41=35, 42=34, 43=33, 44=32, 45=31, 46=36, 47=38, 48=39, 49=40, 50=41, 51=42, 52=44, 53=46, 54=48, 55=2, 56=1, 57=3}

