
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)O[CH2]>>O=P(O)(O[CH2])OP(=O)(O)O[CH2]:1.0, O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[CH2]:2.0, O=P(O)(O[P])O[CH2]>>O=P(O)(O[P])O[CH2]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>[O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP(=O)(O)OC[CH]>>[O]P(=O)(O)OP(=O)(O)OC[CH]:1.0, [O]P(=O)(O)OP([O])(=O)O:2.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:2.0, [O]P([O])(=O)O>>[O]P([O])(=O)O:1.0, [P]:2.0, [P]O:2.0, [P]O>>[P]O[P]:1.0, [P]O[P]:2.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]P([O])(=O)O:2.0, [P]O[P]:2.0]


ID=Reaction Center at Level: 2 (2)
[O=P(O)(O[P])O[CH2]:2.0, [O]P(=O)(O)OP([O])(=O)O:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[P]>>[P]O
2: [O]P([O])(=O)O>>[O]P([O])(=O)O
3: [P]O>>[P]O[P]

MMP Level 2
1: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
2: O=P(O)(O[P])O[CH2]>>O=P(O)(O[P])O[CH2]
3: [O]P(=O)(O)O>>[O]P(=O)(O)OP([O])(=O)O

MMP Level 3
1: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]
2: [O]P(=O)(O)OP(=O)(O)OC[CH]>>[O]P(=O)(O)OP(=O)(O)OC[CH]
3: O=P(O)(O)O[CH2]>>O=P(O)(O[CH2])OP(=O)(O)O[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(NCCS)CCNC(=O)C(O)C(C)(C)COP(=O)(O)O>>O=C(NCCS)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00002, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(NCCS)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
3: R:M00002, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:32]=[C:33]([NH:34][CH2:35][CH2:36][SH:37])[CH2:38][CH2:39][NH:40][C:41](=[O:42])[CH:43]([OH:44])[C:45]([CH3:46])([CH3:47])[CH2:48][O:49][P:50](=[O:51])([OH:52])[OH:53].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:32]=[C:33]([NH:34][CH2:35][CH2:36][SH:37])[CH2:38][CH2:39][NH:40][C:41](=[O:42])[CH:43]([OH:44])[C:45]([CH3:46])([CH3:47])[CH2:48][O:49][P:50](=[O:51])([OH:52])[O:53][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[OH:9]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=46, 2=45, 3=47, 4=48, 5=49, 6=50, 7=51, 8=52, 9=53, 10=43, 11=41, 12=42, 13=40, 14=39, 15=38, 16=33, 17=32, 18=34, 19=35, 20=36, 21=37, 22=44, 23=24, 24=25, 25=26, 26=21, 27=22, 28=23, 29=27, 30=20, 31=19, 32=18, 33=17, 34=28, 35=30, 36=15, 37=16, 38=14, 39=13, 40=10, 41=11, 42=12, 43=9, 44=6, 45=7, 46=8, 47=5, 48=2, 49=1, 50=3, 51=4, 52=31, 53=29}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=15, 2=14, 3=16, 4=17, 5=18, 6=19, 7=20, 8=21, 9=22, 10=23, 11=24, 12=25, 13=26, 14=27, 15=28, 16=43, 17=41, 18=30, 19=29, 20=31, 21=32, 22=33, 23=34, 24=39, 25=38, 26=37, 27=36, 28=35, 29=40, 30=42, 31=44, 32=12, 33=10, 34=11, 35=9, 36=8, 37=7, 38=2, 39=1, 40=3, 41=4, 42=5, 43=6, 44=13, 45=47, 46=46, 47=45, 48=48, 49=49, 50=50, 51=51, 52=52, 53=53}

