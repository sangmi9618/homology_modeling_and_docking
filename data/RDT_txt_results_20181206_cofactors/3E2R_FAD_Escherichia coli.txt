
//
FINGERPRINTS BC

ORDER_CHANGED
[C%C*C%C:1.0, C%N*C%N:3.0]

//
FINGERPRINTS RC
[N=1[CH]CCC1:1.0, O=C(O)C1NCCC1>>O=C(O)C1N=CCC1:1.0, O=C1N=C2C(=N[C]=C([CH])N2C[CH])C(=O)N1>>O=c1[nH]c(=O)c2N[C]=C([CH])N(c2[nH]1)C[CH]:1.0, O=C1N[C]N=C2C1=NC([CH])=[C]N2[CH2]>>O=C1N[C]NC2=C1NC([CH])=[C]N2[CH2]:1.0, [CH2]:1.0, [CH]:1.0, [CH]1NCCC1:1.0, [CH]1NCCC1>>N=1[CH]CCC1:1.0, [CH]N=[CH]:1.0, [CH]N[CH2]:1.0, [CH]N[CH2]>>[CH]N=[CH]:1.0, [C]:4.0, [C]=C([N])NC(=O)[NH]:1.0, [C]=C([N])[NH]:1.0, [C]=CC=1N=C(C(=[N])[N]C1[CH])C(=O)[NH]>>[C]=CC=1NC(=C([NH])[N]C1[CH])C(=O)[NH]:1.0, [C]C(=[C])NC(=[C])[CH]:1.0, [C]C(=[C])[NH]:1.0, [C]C([C])=NC(=[C])[CH]:1.0, [C]C([C])=NC(=[C])[CH]>>[C]C(=[C])NC(=[C])[CH]:1.0, [C]C([C])=[N]:1.0, [C]C([C])=[N]>>[C]C(=[C])[NH]:1.0, [C]C([N])=NC(=O)[NH]:1.0, [C]C([N])=NC(=O)[NH]>>[C]=C([N])NC(=O)[NH]:1.0, [C]C([N])=[N]:1.0, [C]C([N])=[N]>>[C]=C([N])[NH]:1.0, [C]C1N=CCC1:1.0, [C]C1NCCC1:1.0, [C]C1NCCC1>>[C]C1N=CCC1:2.0, [C]N([CH2])C1=NC(=O)N[C]C1=[N]>>[C]N([CH2])C=1NC(=O)N[C]C1[NH]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]>>[C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]>>[C]NC(=C([N])[NH])C(=O)[NH]:1.0, [C]N=[C]:2.0, [C]N=[C]>>[C]N[C]:2.0, [C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]NC(=C([N])[NH])C(=O)[NH]:1.0, [C]N[C]:2.0, [NH]:3.0, [NH]C[CH2]:1.0, [NH]C[CH2]>>[N]=C[CH2]:1.0, [N]:3.0, [N]=C[CH2]:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (5)
[[CH2]:1.0, [CH]:1.0, [C]:4.0, [NH]:3.0, [N]:3.0]


ID=Reaction Center at Level: 1 (10)
[[CH]N=[CH]:1.0, [CH]N[CH2]:1.0, [C]=C([N])[NH]:1.0, [C]C(=[C])[NH]:1.0, [C]C([C])=[N]:1.0, [C]C([N])=[N]:1.0, [C]N=[C]:2.0, [C]N[C]:2.0, [NH]C[CH2]:1.0, [N]=C[CH2]:1.0]


ID=Reaction Center at Level: 2 (12)
[N=1[CH]CCC1:1.0, [CH]1NCCC1:1.0, [C]=C([N])NC(=O)[NH]:1.0, [C]C(=[C])NC(=[C])[CH]:1.0, [C]C([C])=NC(=[C])[CH]:1.0, [C]C([N])=NC(=O)[NH]:1.0, [C]C1N=CCC1:1.0, [C]C1NCCC1:1.0, [C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]:1.0, [C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]NC(=C([N])[NH])C(=O)[NH]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C([C])=[N]>>[C]C(=[C])[NH]
2: [C]N=[C]>>[C]N[C]
3: [C]N=[C]>>[C]N[C]
4: [C]C([N])=[N]>>[C]=C([N])[NH]
5: [CH]N[CH2]>>[CH]N=[CH]
6: [NH]C[CH2]>>[N]=C[CH2]

MMP Level 2
1: [C]N=C(C([N])=[N])C(=O)[NH]>>[C]NC(=C([N])[NH])C(=O)[NH]
2: [C]C([C])=NC(=[C])[CH]>>[C]C(=[C])NC(=[C])[CH]
3: [C]C([N])=NC(=O)[NH]>>[C]=C([N])NC(=O)[NH]
4: [C]N=C(C([C])=[N])N([C])[CH2]>>[C]NC(=C([C])[NH])N([C])[CH2]
5: [C]C1NCCC1>>[C]C1N=CCC1
6: [CH]1NCCC1>>N=1[CH]CCC1

MMP Level 3
1: O=C1N[C]N=C2C1=NC([CH])=[C]N2[CH2]>>O=C1N[C]NC2=C1NC([CH])=[C]N2[CH2]
2: [C]=CC=1N=C(C(=[N])[N]C1[CH])C(=O)[NH]>>[C]=CC=1NC(=C([NH])[N]C1[CH])C(=O)[NH]
3: [C]N([CH2])C1=NC(=O)N[C]C1=[N]>>[C]N([CH2])C=1NC(=O)N[C]C1[NH]
4: O=C1N=C2C(=N[C]=C([CH])N2C[CH])C(=O)N1>>O=c1[nH]c(=O)c2N[C]=C([CH])N(c2[nH]1)C[CH]
5: O=C(O)C1NCCC1>>O=C(O)C1N=CCC1
6: [C]C1NCCC1>>[C]C1N=CCC1


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C(O)C1NCCC1>>O=C(O)C1N=CCC1, O=C(O)C1NCCC1>>O=C(O)C1N=CCC1]
2: R:M00002, P:M00005	[O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C, O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C, O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C, O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C]


//
SELECTED AAM MAPPING
[O:54]=[C:55]([OH:56])[CH:57]1[NH:58][CH2:59][CH2:60][CH2:61]1.[O:1]=[C:2]1[N:3]=[C:4]2[C:5](=[N:6][C:7]:3:[CH:8]:[C:9](:[C:10](:[CH:11]:[C:12]3[N:13]2[CH2:14][CH:15]([OH:16])[CH:17]([OH:18])[CH:19]([OH:20])[CH2:21][O:22][P:23](=[O:24])([OH:25])[O:26][P:27](=[O:28])([OH:29])[O:30][CH2:31][CH:32]4[O:33][CH:34]([N:35]:5:[CH:36]:[N:37]:[C:38]:6:[C:39](:[N:40]:[CH:41]:[N:42]:[C:43]65)[NH2:44])[CH:45]([OH:46])[CH:47]4[OH:48])[CH3:49])[CH3:50])[C:51](=[O:52])[NH:53]1.[OH2:62]>>[O:54]=[C:55]([OH:56])[CH:57]1[N:58]=[CH:59][CH2:60][CH2:61]1.[O:1]=[C:2]1[NH:53][C:51](=[O:52])[C:5]=2[NH:6][C:7]:3:[CH:8]:[C:9](:[C:10](:[CH:11]:[C:12]3[N:13]([C:4]2[NH:3]1)[CH2:14][CH:15]([OH:16])[CH:17]([OH:18])[CH:19]([OH:20])[CH2:21][O:22][P:23](=[O:24])([OH:25])[O:26][P:27](=[O:28])([OH:29])[O:30][CH2:31][CH:32]4[O:33][CH:34]([N:35]:5:[CH:36]:[N:37]:[C:38]:6:[C:39](:[N:40]:[CH:41]:[N:42]:[C:43]65)[NH2:44])[CH:45]([OH:46])[CH:47]4[OH:48])[CH3:49])[CH3:50]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=60, 2=61, 3=57, 4=58, 5=59, 6=55, 7=54, 8=56, 9=50, 10=9, 11=8, 12=7, 13=12, 14=11, 15=10, 16=49, 17=13, 18=4, 19=3, 20=2, 21=1, 22=53, 23=51, 24=52, 25=5, 26=6, 27=14, 28=15, 29=17, 30=19, 31=21, 32=22, 33=23, 34=24, 35=25, 36=26, 37=27, 38=28, 39=29, 40=30, 41=31, 42=32, 43=47, 44=45, 45=34, 46=33, 47=35, 48=36, 49=37, 50=38, 51=43, 52=42, 53=41, 54=40, 55=39, 56=44, 57=46, 58=48, 59=20, 60=18, 61=16, 62=62}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=60, 2=61, 3=57, 4=58, 5=59, 6=55, 7=54, 8=56, 9=53, 10=10, 11=9, 12=8, 13=13, 14=12, 15=11, 16=52, 17=14, 18=15, 19=6, 20=7, 21=4, 22=5, 23=3, 24=2, 25=1, 26=16, 27=17, 28=18, 29=20, 30=22, 31=24, 32=25, 33=26, 34=27, 35=28, 36=29, 37=30, 38=31, 39=32, 40=33, 41=34, 42=35, 43=50, 44=48, 45=37, 46=36, 47=38, 48=39, 49=40, 50=41, 51=46, 52=45, 53=44, 54=43, 55=42, 56=47, 57=49, 58=51, 59=23, 60=21, 61=19}

