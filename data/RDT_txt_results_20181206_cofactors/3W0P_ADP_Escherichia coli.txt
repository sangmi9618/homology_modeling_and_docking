
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)OC([CH])[CH]:1.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[CH]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O[CH]:1.0, [CH]C([CH])O:1.0, [CH]C([CH])O>>O=P(O)(O)OC([CH])[CH]:1.0, [CH]O:1.0, [CH]O>>[P]O[CH]:1.0, [OH]:2.0, [O]:2.0, [O]C([CH])C(O)C([CH2])N>>[O]C([CH])C(OP(=O)(O)O)C([CH2])N:1.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC([CH])[CH]:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[O]P(=O)(O)O:2.0, [P]O[CH]:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)OC([CH])[CH]:1.0, O=P(O)(O)O[CH]:1.0, O=P(O)(O)O[P]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH]O>>[P]O[CH]
2: [P]O[P]>>[P]O
3: [O]P(=O)(O)O>>[O]P(=O)(O)O

MMP Level 2
1: [CH]C([CH])O>>O=P(O)(O)OC([CH])[CH]
2: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
3: O=P(O)(O)O[P]>>O=P(O)(O)O[CH]

MMP Level 3
1: [O]C([CH])C(O)C([CH2])N>>[O]C([CH])C(OP(=O)(O)O)C([CH2])N
2: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
3: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC([CH])[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OC1C(N)CC(NC)C(O)C1OC2OC(CO)C(O)C3OC4(OC23)OC(C(O)C(O)C4O)C(N)CO]
3: R:M00002, P:M00004	[OCC1OC(OC2C(O)C(N)CC(NC)C2O)C3OC4(OC3C1O)OC(C(O)C(O)C4O)C(N)CO>>O=P(O)(O)OC1C(N)CC(NC)C(O)C1OC2OC(CO)C(O)C3OC4(OC23)OC(C(O)C(O)C4O)C(N)CO]


//
SELECTED AAM MAPPING
[O:37]=[P:38]([OH:39])([OH:40])[O:41][P:42](=[O:43])([OH:44])[O:45][P:46](=[O:47])([OH:48])[O:49][CH2:50][CH:51]1[O:52][CH:53]([N:54]:2:[CH:55]:[N:56]:[C:57]:3:[C:58](:[N:59]:[CH:60]:[N:61]:[C:62]32)[NH2:63])[CH:64]([OH:65])[CH:66]1[OH:67].[OH:1][CH2:2][CH:3]1[O:4][CH:5]([O:6][CH:7]2[CH:8]([OH:9])[CH:10]([NH2:11])[CH2:12][CH:13]([NH:14][CH3:15])[CH:16]2[OH:17])[CH:18]3[O:19][C:20]4([O:21][CH:22]3[CH:23]1[OH:24])[O:25][CH:26]([CH:27]([OH:28])[CH:29]([OH:30])[CH:31]4[OH:32])[CH:33]([NH2:34])[CH2:35][OH:36]>>[O:37]=[P:38]([OH:40])([OH:39])[O:9][CH:8]1[CH:7]([O:6][CH:5]2[O:4][CH:3]([CH2:2][OH:1])[CH:23]([OH:24])[CH:22]3[O:21][C:20]4([O:25][CH:26]([CH:27]([OH:28])[CH:29]([OH:30])[CH:31]4[OH:32])[CH:33]([NH2:34])[CH2:35][OH:36])[O:19][CH:18]23)[CH:16]([OH:17])[CH:13]([NH:14][CH3:15])[CH2:12][CH:10]1[NH2:11].[O:43]=[P:42]([OH:41])([OH:44])[O:45][P:46](=[O:47])([OH:48])[O:49][CH2:50][CH:51]1[O:52][CH:53]([N:54]:2:[CH:55]:[N:56]:[C:57]:3:[C:58](:[N:59]:[CH:60]:[N:61]:[C:62]32)[NH2:63])[CH:64]([OH:65])[CH:66]1[OH:67]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=60, 2=61, 3=62, 4=57, 5=58, 6=59, 7=63, 8=56, 9=55, 10=54, 11=53, 12=64, 13=66, 14=51, 15=52, 16=50, 17=49, 18=46, 19=47, 20=48, 21=45, 22=42, 23=43, 24=44, 25=41, 26=38, 27=37, 28=39, 29=40, 30=67, 31=65, 32=15, 33=14, 34=13, 35=12, 36=10, 37=8, 38=7, 39=16, 40=17, 41=6, 42=5, 43=18, 44=22, 45=23, 46=3, 47=4, 48=2, 49=1, 50=24, 51=21, 52=20, 53=19, 54=31, 55=29, 56=27, 57=26, 58=25, 59=33, 60=35, 61=36, 62=34, 63=28, 64=30, 65=32, 66=9, 67=11}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=60, 2=61, 3=62, 4=57, 5=58, 6=59, 7=63, 8=56, 9=55, 10=54, 11=53, 12=64, 13=66, 14=51, 15=52, 16=50, 17=49, 18=46, 19=47, 20=48, 21=45, 22=42, 23=41, 24=43, 25=44, 26=67, 27=65, 28=37, 29=36, 30=35, 31=38, 32=39, 33=6, 34=7, 35=33, 36=34, 37=8, 38=9, 39=20, 40=16, 41=14, 42=11, 43=10, 44=12, 45=13, 46=15, 47=17, 48=18, 49=19, 50=27, 51=25, 52=23, 53=22, 54=21, 55=29, 56=31, 57=32, 58=30, 59=24, 60=26, 61=28, 62=5, 63=2, 64=1, 65=3, 66=4, 67=40}

