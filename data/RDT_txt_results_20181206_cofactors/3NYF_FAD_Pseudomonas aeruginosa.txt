
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C=O:1.0]

ORDER_CHANGED
[C%C*C%C:1.0, C%N*C%N:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[N:3.0, O:3.0, O=C(O)C(=O)C:1.0, O=C(O)C(N)C:1.0, O=C(O)C(N)C>>N:1.0, O=C(O)C(N)C>>O=C(O)C(=O)C:2.0, O=C1N=C2C(=N[C]=C([CH])N2C[CH])C(=O)N1>>O=c1[nH]c(=O)c2N[C]=C([CH])N(c2[nH]1)C[CH]:1.0, O=C1N[C]N=C2C1=NC([CH])=[C]N2[CH2]>>O=C1N[C]NC2=C1NC([CH])=[C]N2[CH2]:1.0, O>>O=C(O)C(=O)C:1.0, O>>[C]=O:1.0, O>>[C]C(=O)C:1.0, [CH]:1.0, [CH]N:1.0, [CH]N>>N:1.0, [C]:5.0, [C]=C([N])NC(=O)[NH]:1.0, [C]=C([N])[NH]:1.0, [C]=CC=1N=C(C(=[N])[N]C1[CH])C(=O)[NH]>>[C]=CC=1NC(=C([NH])[N]C1[CH])C(=O)[NH]:1.0, [C]=O:1.0, [C]C(=O)C:2.0, [C]C(=[C])NC(=[C])[CH]:1.0, [C]C(=[C])[NH]:1.0, [C]C(N)C:2.0, [C]C(N)C>>N:1.0, [C]C(N)C>>[C]C(=O)C:1.0, [C]C([C])=NC(=[C])[CH]:1.0, [C]C([C])=NC(=[C])[CH]>>[C]C(=[C])NC(=[C])[CH]:1.0, [C]C([C])=[N]:1.0, [C]C([C])=[N]>>[C]C(=[C])[NH]:1.0, [C]C([N])=NC(=O)[NH]:1.0, [C]C([N])=NC(=O)[NH]>>[C]=C([N])NC(=O)[NH]:1.0, [C]C([N])=[N]:1.0, [C]C([N])=[N]>>[C]=C([N])[NH]:1.0, [C]N([CH2])C1=NC(=O)N[C]C1=[N]>>[C]N([CH2])C=1NC(=O)N[C]C1[NH]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]>>[C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]>>[C]NC(=C([N])[NH])C(=O)[NH]:1.0, [C]N=[C]:2.0, [C]N=[C]>>[C]N[C]:2.0, [C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]NC(=C([N])[NH])C(=O)[NH]:1.0, [C]N[C]:2.0, [NH2]:1.0, [NH]:2.0, [N]:2.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (6)
[N:1.0, O:1.0, [CH]:1.0, [C]:1.0, [NH2]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (6)
[N:1.0, O:1.0, [CH]N:1.0, [C]=O:1.0, [C]C(=O)C:1.0, [C]C(N)C:1.0]


ID=Reaction Center at Level: 2 (6)
[N:1.0, O:1.0, O=C(O)C(=O)C:1.0, O=C(O)C(N)C:1.0, [C]C(=O)C:1.0, [C]C(N)C:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:4.0, [NH]:2.0, [N]:2.0]


ID=Reaction Center at Level: 1 (6)
[[C]=C([N])[NH]:1.0, [C]C(=[C])[NH]:1.0, [C]C([C])=[N]:1.0, [C]C([N])=[N]:1.0, [C]N=[C]:2.0, [C]N[C]:2.0]


ID=Reaction Center at Level: 2 (8)
[[C]=C([N])NC(=O)[NH]:1.0, [C]C(=[C])NC(=[C])[CH]:1.0, [C]C([C])=NC(=[C])[CH]:1.0, [C]C([N])=NC(=O)[NH]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]:1.0, [C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]NC(=C([N])[NH])C(=O)[NH]:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(=O)C:1.0, [C]C(N)C:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C(=O)C:1.0, O=C(O)C(N)C:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: O>>[C]=O
2: [C]N=[C]>>[C]N[C]
3: [CH]N>>N
4: [C]C([N])=[N]>>[C]=C([N])[NH]
5: [C]N=[C]>>[C]N[C]
6: [C]C([C])=[N]>>[C]C(=[C])[NH]
7: [C]C(N)C>>[C]C(=O)C

MMP Level 2
1: O>>[C]C(=O)C
2: [C]C([N])=NC(=O)[NH]>>[C]=C([N])NC(=O)[NH]
3: [C]C(N)C>>N
4: [C]N=C(C([C])=[N])N([C])[CH2]>>[C]NC(=C([C])[NH])N([C])[CH2]
5: [C]C([C])=NC(=[C])[CH]>>[C]C(=[C])NC(=[C])[CH]
6: [C]N=C(C([N])=[N])C(=O)[NH]>>[C]NC(=C([N])[NH])C(=O)[NH]
7: O=C(O)C(N)C>>O=C(O)C(=O)C

MMP Level 3
1: O>>O=C(O)C(=O)C
2: [C]N([CH2])C1=NC(=O)N[C]C1=[N]>>[C]N([CH2])C=1NC(=O)N[C]C1[NH]
3: O=C(O)C(N)C>>N
4: O=C1N=C2C(=N[C]=C([CH])N2C[CH])C(=O)N1>>O=c1[nH]c(=O)c2N[C]=C([CH])N(c2[nH]1)C[CH]
5: [C]=CC=1N=C(C(=[N])[N]C1[CH])C(=O)[NH]>>[C]=CC=1NC(=C([NH])[N]C1[CH])C(=O)[NH]
6: O=C1N[C]N=C2C1=NC([CH])=[C]N2[CH2]>>O=C1N[C]NC2=C1NC([CH])=[C]N2[CH2]
7: O=C(O)C(N)C>>O=C(O)C(=O)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C(O)C(N)C>>O=C(O)C(=O)C]
2: R:M00001, P:M00005	[O=C(O)C(N)C>>N]
3: R:M00002, P:M00004	[O>>O=C(O)C(=O)C]
4: R:M00003, P:M00006	[O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C, O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C, O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C, O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1>>O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C]


//
SELECTED AAM MAPPING
[O:54]=[C:55]([OH:56])[CH:57]([NH2:58])[CH3:59].[O:1]=[C:2]1[N:3]=[C:4]2[C:5](=[N:6][C:7]:3:[CH:8]:[C:9](:[C:10](:[CH:11]:[C:12]3[N:13]2[CH2:14][CH:15]([OH:16])[CH:17]([OH:18])[CH:19]([OH:20])[CH2:21][O:22][P:23](=[O:24])([OH:25])[O:26][P:27](=[O:28])([OH:29])[O:30][CH2:31][CH:32]4[O:33][CH:34]([N:35]:5:[CH:36]:[N:37]:[C:38]:6:[C:39](:[N:40]:[CH:41]:[N:42]:[C:43]65)[NH2:44])[CH:45]([OH:46])[CH:47]4[OH:48])[CH3:49])[CH3:50])[C:51](=[O:52])[NH:53]1.[OH2:60]>>[O:54]=[C:55]([OH:56])[C:57](=[O:60])[CH3:59].[O:1]=[C:2]1[NH:53][C:51](=[O:52])[C:5]=2[NH:6][C:7]:3:[CH:8]:[C:9](:[C:10](:[CH:11]:[C:12]3[N:13]([C:4]2[NH:3]1)[CH2:14][CH:15]([OH:16])[CH:17]([OH:18])[CH:19]([OH:20])[CH2:21][O:22][P:23](=[O:24])([OH:25])[O:26][P:27](=[O:28])([OH:29])[O:30][CH2:31][CH:32]4[O:33][CH:34]([N:35]:5:[CH:36]:[N:37]:[C:38]:6:[C:39](:[N:40]:[CH:41]:[N:42]:[C:43]65)[NH2:44])[CH:45]([OH:46])[CH:47]4[OH:48])[CH3:49])[CH3:50].[NH3:58]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=59, 2=57, 3=55, 4=54, 5=56, 6=58, 7=60, 8=50, 9=9, 10=8, 11=7, 12=12, 13=11, 14=10, 15=49, 16=13, 17=4, 18=3, 19=2, 20=1, 21=53, 22=51, 23=52, 24=5, 25=6, 26=14, 27=15, 28=17, 29=19, 30=21, 31=22, 32=23, 33=24, 34=25, 35=26, 36=27, 37=28, 38=29, 39=30, 40=31, 41=32, 42=47, 43=45, 44=34, 45=33, 46=35, 47=36, 48=37, 49=38, 50=43, 51=42, 52=41, 53=40, 54=39, 55=44, 56=46, 57=48, 58=20, 59=18, 60=16}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=59, 2=57, 3=58, 4=55, 5=54, 6=56, 7=60, 8=53, 9=10, 10=9, 11=8, 12=13, 13=12, 14=11, 15=52, 16=14, 17=15, 18=6, 19=7, 20=4, 21=5, 22=3, 23=2, 24=1, 25=16, 26=17, 27=18, 28=20, 29=22, 30=24, 31=25, 32=26, 33=27, 34=28, 35=29, 36=30, 37=31, 38=32, 39=33, 40=34, 41=35, 42=50, 43=48, 44=37, 45=36, 46=38, 47=39, 48=40, 49=41, 50=46, 51=45, 52=44, 53=43, 54=42, 55=47, 56=49, 57=51, 58=23, 59=21, 60=19}

