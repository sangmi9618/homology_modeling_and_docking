
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:2.0, O-P:2.0]

//
FINGERPRINTS RC
[O=C(O)C(N)C:1.0, O=C(O)C(N)C>>O=C(O[CH])C(N)C:1.0, O=C(O)C(N)C>>O=P(O)(O)O:1.0, O=C(O)C(N)C>>[C]C(OC(=O)C(N)C)C:1.0, O=C(O)C(O)C>>O=C(O)C(OC(=O)C(N)C)C:1.0, O=C(O[CH])C(N)C:1.0, O=C([CH])O:2.0, O=C([CH])O>>O=P(O)(O)O:1.0, O=C([CH])O>>[O]C(=O)[CH]:1.0, O=P(O)(O)O:3.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O:1.0, [CH]O:1.0, [CH]O>>[C]O[CH]:1.0, [C]:2.0, [C]C(O)C:1.0, [C]C(O)C>>[C]C(OC(=O)[CH])C:1.0, [C]C(OC(=O)[CH])C:1.0, [C]O:1.0, [C]O>>[P]O:1.0, [C]O[CH]:1.0, [OH]:4.0, [O]:2.0, [O]C(=O)[CH]:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[C]:2.0, [OH]:2.0, [O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (8)
[O=C([CH])O:1.0, O=P(O)(O)O:1.0, [C]O:1.0, [C]O[CH]:1.0, [O]C(=O)[CH]:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (7)
[O=C(O)C(N)C:1.0, O=C(O[CH])C(N)C:1.0, O=C([CH])O:1.0, O=P(O)(O)O:2.0, O=P(O)(O)O[P]:1.0, [C]C(OC(=O)[CH])C:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH]O>>[C]O[CH]
2: [O]P(=O)(O)O>>O=P(O)(O)O
3: [C]O>>[P]O
4: O=C([CH])O>>[O]C(=O)[CH]
5: [P]O[P]>>[P]O

MMP Level 2
1: [C]C(O)C>>[C]C(OC(=O)[CH])C
2: O=P(O)(O)O[P]>>O=P(O)(O)O
3: O=C([CH])O>>O=P(O)(O)O
4: O=C(O)C(N)C>>O=C(O[CH])C(N)C
5: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O

MMP Level 3
1: O=C(O)C(O)C>>O=C(O)C(OC(=O)C(N)C)C
2: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O
3: O=C(O)C(N)C>>O=P(O)(O)O
4: O=C(O)C(N)C>>[C]C(OC(=O)C(N)C)C
5: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C(O)C(N)C>>O=C(O)C(OC(=O)C(N)C)C]
2: R:M00001, P:M00006	[O=C(O)C(N)C>>O=P(O)(O)O]
3: R:M00002, P:M00004	[O=C(O)C(O)C>>O=C(O)C(OC(=O)C(N)C)C]
4: R:M00003, P:M00005	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
5: R:M00003, P:M00006	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[O:32]=[C:33]([OH:34])[CH:35]([OH:36])[CH3:37].[O:38]=[C:39]([OH:40])[CH:41]([NH2:42])[CH3:43].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:32]=[C:33]([OH:34])[CH:35]([O:36][C:39](=[O:38])[CH:41]([NH2:42])[CH3:43])[CH3:37].[O:1]=[P:2]([OH:3])([OH:4])[OH:40].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=43, 2=41, 3=39, 4=38, 5=40, 6=42, 7=37, 8=35, 9=33, 10=32, 11=34, 12=36, 13=24, 14=25, 15=26, 16=21, 17=22, 18=23, 19=27, 20=20, 21=19, 22=18, 23=17, 24=28, 25=30, 26=15, 27=16, 28=14, 29=13, 30=10, 31=11, 32=12, 33=9, 34=6, 35=7, 36=8, 37=5, 38=2, 39=1, 40=3, 41=4, 42=31, 43=29}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=37, 2=35, 3=33, 4=34, 5=32, 6=31, 7=38, 8=29, 9=28, 10=30, 11=36, 12=20, 13=21, 14=22, 15=17, 16=18, 17=19, 18=23, 19=16, 20=15, 21=14, 22=13, 23=24, 24=26, 25=11, 26=12, 27=10, 28=9, 29=6, 30=7, 31=8, 32=5, 33=2, 34=1, 35=3, 36=4, 37=27, 38=25, 39=41, 40=40, 41=39, 42=42, 43=43}

