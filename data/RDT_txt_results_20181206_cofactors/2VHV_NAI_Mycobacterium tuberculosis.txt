
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[N:3.0, O:3.0, O=C(N)C=1C=[N+]C=CC1>>O=C(N)C1=C[N]C=CC1:1.0, O=C(O)C(=O)C:1.0, O=C(O)C(N)C:1.0, O=C(O)C(N)C>>N:1.0, O=C(O)C(N)C>>O=C(O)C(=O)C:2.0, O>>O=C(O)C(=O)C:1.0, O>>[C]=O:1.0, O>>[C]C(=O)C:1.0, [CH2]:1.0, [CH]:2.0, [CH]N:1.0, [CH]N([CH])[CH]:1.0, [CH]N>>N:1.0, [CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]>>[CH]N([CH])[CH]:1.0, [C]:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN(C=[CH])C([O])[CH]:1.0, [C]=O:1.0, [C]C(=O)C:2.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])C=C[CH]>>[C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C(N)C:2.0, [C]C(N)C>>N:1.0, [C]C(N)C>>[C]C(=O)C:1.0, [C]C=[CH]:1.0, [C]C=[CH]>>[C]C[CH]:1.0, [C]C[CH]:1.0, [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]C1=CN(C=CC1)C2O[CH][CH]C2O:1.0, [N+]:1.0, [NH2]:1.0, [N]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (6)
[N:1.0, O:1.0, [CH]:1.0, [C]:1.0, [NH2]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (6)
[N:1.0, O:1.0, [CH]N:1.0, [C]=O:1.0, [C]C(=O)C:1.0, [C]C(N)C:1.0]


ID=Reaction Center at Level: 2 (6)
[N:1.0, O:1.0, O=C(O)C(=O)C:1.0, O=C(O)C(N)C:1.0, [C]C(=O)C:1.0, [C]C(N)C:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(=O)C:1.0, [C]C(N)C:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C(=O)C:1.0, O=C(O)C(N)C:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]N>>N
2: [CH][N+]([CH])=[CH]>>[CH]N([CH])[CH]
3: [C]C(N)C>>[C]C(=O)C
4: [C]C=[CH]>>[C]C[CH]
5: O>>[C]=O

MMP Level 2
1: [C]C(N)C>>N
2: [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN(C=[CH])C([O])[CH]
3: O=C(O)C(N)C>>O=C(O)C(=O)C
4: [C]C(=[CH])C=C[CH]>>[C]C(=[CH])CC=[CH]
5: O>>[C]C(=O)C

MMP Level 3
1: O=C(O)C(N)C>>N
2: [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]C1=CN(C=CC1)C2O[CH][CH]C2O
3: O=C(O)C(N)C>>O=C(O)C(=O)C
4: O=C(N)C=1C=[N+]C=CC1>>O=C(N)C1=C[N]C=CC1
5: O>>O=C(O)C(=O)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C(O)C(N)C>>O=C(O)C(=O)C]
2: R:M00001, P:M00005	[O=C(O)C(N)C>>N]
3: R:M00002, P:M00004	[O>>O=C(O)C(=O)C]
4: R:M00003, P:M00006	[O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O, O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O]


//
SELECTED AAM MAPPING
[O:45]=[C:46]([OH:47])[CH:48]([NH2:49])[CH3:50].[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:5]:[CH:6]:[CH:7]:[N+:8](:[CH:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44].[OH2:51]>>[H+:52].[O:45]=[C:46]([OH:47])[C:48](=[O:51])[CH3:50].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:9][N:8]([CH:7]=[CH:6][CH2:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44].[NH3:49]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=50, 2=48, 3=46, 4=45, 5=47, 6=49, 7=51, 8=6, 9=5, 10=4, 11=9, 12=8, 13=7, 14=10, 15=43, 16=41, 17=12, 18=11, 19=13, 20=14, 21=15, 22=16, 23=17, 24=18, 25=19, 26=20, 27=21, 28=22, 29=23, 30=24, 31=39, 32=37, 33=26, 34=25, 35=27, 36=28, 37=29, 38=30, 39=35, 40=34, 41=33, 42=32, 43=31, 44=36, 45=38, 46=40, 47=42, 48=44, 49=2, 50=1, 51=3}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=50, 2=48, 3=49, 4=46, 5=45, 6=47, 7=51, 8=9, 9=8, 10=7, 11=6, 12=5, 13=4, 14=2, 15=1, 16=3, 17=10, 18=43, 19=41, 20=12, 21=11, 22=13, 23=14, 24=15, 25=16, 26=17, 27=18, 28=19, 29=20, 30=21, 31=22, 32=23, 33=24, 34=39, 35=37, 36=26, 37=25, 38=27, 39=28, 40=29, 41=30, 42=35, 43=34, 44=33, 45=32, 46=31, 47=36, 48=38, 49=40, 50=42, 51=44, 52=52}

