
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%O:1.0, O-P:2.0]

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O=C([CH])[CH2]:2.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O[CH2]:1.0, OC1(OC([CH][CH]1)C)[CH2]:1.0, OC1(OC([CH][CH]1)C)[CH2]>>[CH]C(O)C:1.0, OC1C(O)C(O)(OC1C)[CH2]>>O=C([CH2])C(O)C(O)C(O)C:1.0, OC1[C]OC(C)C1O:1.0, OC1[C]OC(C)C1O>>[C]C(O)C(O)C(O)C:1.0, OCC1(O)OC(C)C(O)C1O>>O=C(CO[P])C(O)C([CH])O:1.0, OCC1(O)OC(C)C(O)C1O>>[CH]C(O)C(O)C:1.0, OCC1(O)O[CH][CH]C1O:1.0, OCC1(O)O[CH][CH]C1O>>[O]CC(=O)C([CH])O:2.0, O[CH2]:1.0, O[CH2]>>[P]O[CH2]:1.0, [CH]:2.0, [CH]C(O)C:1.0, [CH]C([CH])O:2.0, [CH]C([CH])O>>[CH]C([CH])O:1.0, [CH]O:1.0, [C]:2.0, [C]=O:1.0, [C]C(O)C(O)C(O)C:1.0, [C]CO:1.0, [C]CO>>[C]COP(=O)(O)O:1.0, [C]COP(=O)(O)O:1.0, [C]O:1.0, [C]O>>[C]=O:1.0, [C]O[CH]:1.0, [C]O[CH]>>[CH]O:1.0, [OH]:4.0, [O]:4.0, [O]C([CH])(O)CO>>O=C([CH])COP(=O)(O)O:1.0, [O]C([CH])(O)[CH2]:2.0, [O]C([CH])(O)[CH2]>>O=C([CH])[CH2]:2.0, [O]CC(=O)C([CH])O:1.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[C]COP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[C]:1.0, [O]:3.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[[C]O[CH]:1.0, [O]C([CH])(O)[CH2]:1.0, [O]P(=O)(O)O:2.0, [P]O[CH2]:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (6)
[O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[P]:1.0, OC1(OC([CH][CH]1)C)[CH2]:1.0, OCC1(O)O[CH][CH]C1O:1.0, [C]COP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([CH])[CH2]:1.0, [C]=O:1.0, [C]O:1.0, [O]C([CH])(O)[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C([CH])[CH2]:1.0, OCC1(O)O[CH][CH]C1O:1.0, [O]C([CH])(O)[CH2]:1.0, [O]CC(=O)C([CH])O:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:2.0, [C]:2.0]


ID=Reaction Center at Level: 1 (3)
[O=C([CH])[CH2]:1.0, [CH]C([CH])O:2.0, [O]C([CH])(O)[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[OC1[C]OC(C)C1O:1.0, OCC1(O)O[CH][CH]C1O:1.0, [C]C(O)C(O)C(O)C:1.0, [O]CC(=O)C([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [O]P(=O)(O)O>>[O]P(=O)(O)O
2: [C]O>>[C]=O
3: [CH]C([CH])O>>[CH]C([CH])O
4: O[CH2]>>[P]O[CH2]
5: [C]O[CH]>>[CH]O
6: [O]C([CH])(O)[CH2]>>O=C([CH])[CH2]
7: [P]O[P]>>[P]O

MMP Level 2
1: O=P(O)(O)O[P]>>O=P(O)(O)O[CH2]
2: [O]C([CH])(O)[CH2]>>O=C([CH])[CH2]
3: OC1[C]OC(C)C1O>>[C]C(O)C(O)C(O)C
4: [C]CO>>[C]COP(=O)(O)O
5: OC1(OC([CH][CH]1)C)[CH2]>>[CH]C(O)C
6: OCC1(O)O[CH][CH]C1O>>[O]CC(=O)C([CH])O
7: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O

MMP Level 3
1: [O]P(=O)(O)OP(=O)(O)O>>[C]COP(=O)(O)O
2: OCC1(O)O[CH][CH]C1O>>[O]CC(=O)C([CH])O
3: OC1C(O)C(O)(OC1C)[CH2]>>O=C([CH2])C(O)C(O)C(O)C
4: [O]C([CH])(O)CO>>O=C([CH])COP(=O)(O)O
5: OCC1(O)OC(C)C(O)C1O>>[CH]C(O)C(O)C
6: OCC1(O)OC(C)C(O)C1O>>O=C(CO[P])C(O)C([CH])O
7: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(COP(=O)(O)O)C(O)C(O)C(O)C]
3: R:M00002, P:M00004	[OCC1(O)OC(C)C(O)C1O>>O=C(COP(=O)(O)O)C(O)C(O)C(O)C, OCC1(O)OC(C)C(O)C1O>>O=C(COP(=O)(O)O)C(O)C(O)C(O)C, OCC1(O)OC(C)C(O)C1O>>O=C(COP(=O)(O)O)C(O)C(O)C(O)C, OCC1(O)OC(C)C(O)C1O>>O=C(COP(=O)(O)O)C(O)C(O)C(O)C, OCC1(O)OC(C)C(O)C1O>>O=C(COP(=O)(O)O)C(O)C(O)C(O)C]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31].[OH:32][CH2:33][C:34]1([OH:35])[O:36][CH:37]([CH3:38])[CH:39]([OH:40])[CH:41]1[OH:42]>>[O:35]=[C:34]([CH2:33][O:32][P:2](=[O:1])([OH:4])[OH:3])[CH:41]([OH:42])[CH:39]([OH:40])[CH:37]([OH:36])[CH3:38].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=38, 33=37, 34=39, 35=41, 36=34, 37=36, 38=33, 39=32, 40=35, 41=42, 42=40}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=17, 5=18, 6=19, 7=23, 8=16, 9=15, 10=14, 11=13, 12=24, 13=26, 14=11, 15=12, 16=10, 17=9, 18=6, 19=7, 20=8, 21=5, 22=2, 23=1, 24=3, 25=4, 26=27, 27=25, 28=42, 29=40, 30=38, 31=36, 32=29, 33=28, 34=30, 35=31, 36=32, 37=33, 38=34, 39=35, 40=37, 41=39, 42=41}

