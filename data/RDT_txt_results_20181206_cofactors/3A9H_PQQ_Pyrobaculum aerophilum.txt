
//
FINGERPRINTS BC

ORDER_CHANGED
[C%C*C%C:1.0, C-O*C=O:2.0]

//
FINGERPRINTS RC
[O=C(O[CH])C([CH])O:1.0, OC1OC([CH2])[CH]C(O)C1O>>O=C1OC([CH2])[CH]C(O)C1O:1.0, [CH]:1.0, [CH]O:1.0, [CH]O>>[C]=O:1.0, [CH]OC(O)C([CH])O:1.0, [CH]OC(O)C([CH])O>>O=C(O[CH])C([CH])O:2.0, [C]:5.0, [C]=O:2.0, [C]=O>>[C]O:1.0, [C]C(=O)C(=O)C(=[C])[N]:1.0, [C]C(=O)C(=O)C(=[C])[N]>>[C]C(=O)C(O)=C([C])[NH]:2.0, [C]C(=O)C(O)=C([C])[NH]:1.0, [C]C(=[C])N=C([C])[CH]:1.0, [C]C(=[C])N=C([C])[CH]>>[C]C(=[C])NC([C])=[CH]:1.0, [C]C(=[C])NC([C])=[CH]:1.0, [C]C(=[C])O:2.0, [C]C(=[C])[NH]:1.0, [C]C(=[C])[N]:1.0, [C]C(=[C])[N]>>[C]C(=[C])[NH]:1.0, [C]C([C])=C(N=[C])C([C])=O:1.0, [C]C([C])=C(N=[C])C([C])=O>>[C]NC(C([C])=[C])=C([C])O:1.0, [C]C([C])=O:2.0, [C]C([C])=O>>[C]C(=[C])O:2.0, [C]C1=[C]C=C(N=C1C([C])=O)C(=O)O>>[C]C1=[C]C=C(NC1=C([C])O)C(=O)O:1.0, [C]C=1[C]=C([CH])C(=O)C(=O)C1N=[C]>>[C]NC=1C(=[C])[C]=C([CH])C(=O)C1O:1.0, [C]N=[C]:1.0, [C]N=[C]>>[C]N[C]:1.0, [C]NC(C([C])=[C])=C([C])O:1.0, [C]N[C]:1.0, [C]O:1.0, [C]c1nc2C(=O)C(=O)[C]=C([NH])c2c([C])c1>>[C]C1=CC([C])=C2C([NH])=[C]C(=O)C(O)=C2N1:1.0, [NH]:1.0, [N]:1.0, [OH]:2.0, [O]:2.0, [O]C(=O)[CH]:2.0, [O]C([CH])O:2.0, [O]C([CH])O>>[O]C(=O)[CH]:2.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH]:1.0, [C]:5.0, [OH]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (9)
[[CH]O:1.0, [C]=O:2.0, [C]C(=[C])O:1.0, [C]C(=[C])[NH]:1.0, [C]C(=[C])[N]:1.0, [C]C([C])=O:1.0, [C]O:1.0, [O]C(=O)[CH]:1.0, [O]C([CH])O:1.0]


ID=Reaction Center at Level: 2 (10)
[O=C(O[CH])C([CH])O:1.0, [CH]OC(O)C([CH])O:1.0, [C]C(=O)C(=O)C(=[C])[N]:1.0, [C]C(=O)C(O)=C([C])[NH]:1.0, [C]C(=[C])O:1.0, [C]C([C])=C(N=[C])C([C])=O:1.0, [C]C([C])=O:1.0, [C]NC(C([C])=[C])=C([C])O:1.0, [O]C(=O)[CH]:1.0, [O]C([CH])O:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C(=[C])[N]>>[C]C(=[C])[NH]
2: [C]=O>>[C]O
3: [C]C([C])=O>>[C]C(=[C])O
4: [CH]O>>[C]=O
5: [O]C([CH])O>>[O]C(=O)[CH]
6: [C]N=[C]>>[C]N[C]

MMP Level 2
1: [C]C([C])=C(N=[C])C([C])=O>>[C]NC(C([C])=[C])=C([C])O
2: [C]C([C])=O>>[C]C(=[C])O
3: [C]C(=O)C(=O)C(=[C])[N]>>[C]C(=O)C(O)=C([C])[NH]
4: [O]C([CH])O>>[O]C(=O)[CH]
5: [CH]OC(O)C([CH])O>>O=C(O[CH])C([CH])O
6: [C]C(=[C])N=C([C])[CH]>>[C]C(=[C])NC([C])=[CH]

MMP Level 3
1: [C]c1nc2C(=O)C(=O)[C]=C([NH])c2c([C])c1>>[C]C1=CC([C])=C2C([NH])=[C]C(=O)C(O)=C2N1
2: [C]C(=O)C(=O)C(=[C])[N]>>[C]C(=O)C(O)=C([C])[NH]
3: [C]C=1[C]=C([CH])C(=O)C(=O)C1N=[C]>>[C]NC=1C(=[C])[C]=C([CH])C(=O)C1O
4: [CH]OC(O)C([CH])O>>O=C(O[CH])C([CH])O
5: OC1OC([CH2])[CH]C(O)C1O>>O=C1OC([CH2])[CH]C(O)C1O
6: [C]C1=[C]C=C(N=C1C([C])=O)C(=O)O>>[C]C1=[C]C=C(NC1=C([C])O)C(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[OCC1OC(O)C(O)C(O)C1O>>O=C1OC(CO)C(O)C(O)C1O, OCC1OC(O)C(O)C(O)C1O>>O=C1OC(CO)C(O)C(O)C1O]
2: R:M00002, P:M00004	[O=C1C(=O)c2cc([nH]c2-c3c1nc(cc3C(=O)O)C(=O)O)C(=O)O>>O=c1c(O)c-2[nH]c(cc(c2c3[nH]c(cc13)C(=O)O)C(=O)O)C(=O)O, O=C1C(=O)c2cc([nH]c2-c3c1nc(cc3C(=O)O)C(=O)O)C(=O)O>>O=c1c(O)c-2[nH]c(cc(c2c3[nH]c(cc13)C(=O)O)C(=O)O)C(=O)O, O=C1C(=O)c2cc([nH]c2-c3c1nc(cc3C(=O)O)C(=O)O)C(=O)O>>O=c1c(O)c-2[nH]c(cc(c2c3[nH]c(cc13)C(=O)O)C(=O)O)C(=O)O, O=C1C(=O)c2cc([nH]c2-c3c1nc(cc3C(=O)O)C(=O)O)C(=O)O>>O=c1c(O)c-2[nH]c(cc(c2c3[nH]c(cc13)C(=O)O)C(=O)O)C(=O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[C:4]:1:[N:5]:[C:6]:2[C:7](=[O:8])[C:9](=[O:10])[C:11]:3:[CH:12]:[C:13](:[NH:14]:[C:15]3[C:16]2:[C:17](:[CH:18]1)[C:19](=[O:20])[OH:21])[C:22](=[O:23])[OH:24].[OH:25][CH2:26][CH:27]1[O:28][CH:29]([OH:30])[CH:31]([OH:32])[CH:33]([OH:34])[CH:35]1[OH:36]>>[O:1]=[C:2]([OH:3])[C:4]1=[CH:18][C:17]([C:19](=[O:20])[OH:21])=[C:16]2[C:6]([NH:5]1)=[C:7]([OH:8])[C:9](=[O:10])[C:11]:3:[CH:12]:[C:13](:[NH:14]:[C:15]32)[C:22](=[O:23])[OH:24].[O:30]=[C:29]1[O:28][CH:27]([CH2:26][OH:25])[CH:35]([OH:36])[CH:33]([OH:34])[CH:31]1[OH:32]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=26, 2=27, 3=35, 4=33, 5=31, 6=29, 7=28, 8=30, 9=32, 10=34, 11=36, 12=25, 13=18, 14=17, 15=16, 16=6, 17=7, 18=8, 19=9, 20=10, 21=11, 22=15, 23=14, 24=13, 25=12, 26=22, 27=23, 28=24, 29=5, 30=4, 31=2, 32=1, 33=3, 34=19, 35=20, 36=21}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=29, 2=28, 3=31, 4=33, 5=35, 6=26, 7=25, 8=27, 9=36, 10=34, 11=32, 12=30, 13=18, 14=19, 15=20, 16=21, 17=17, 18=15, 19=16, 20=13, 21=11, 22=10, 23=6, 24=5, 25=4, 26=12, 27=2, 28=1, 29=3, 30=7, 31=8, 32=9, 33=14, 34=22, 35=23, 36=24}

