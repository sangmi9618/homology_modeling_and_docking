
//
FINGERPRINTS BC

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(C[CH])C([CH])[CH]:1.0, O=C(N)C=1C=[N+]C=CC1>>O=C(N)C1=C[N]C=CC1:1.0, O=C([CH])[CH2]:2.0, [CH2]:1.0, [CH]:2.0, [CH]C(O)[CH2]:2.0, [CH]C(O)[CH2]>>O=C([CH])[CH2]:2.0, [CH]CC(O)C([CH])[CH]:1.0, [CH]CC(O)C([CH])[CH]>>O=C(C[CH])C([CH])[CH]:2.0, [CH]N([CH])[CH]:1.0, [CH]O:1.0, [CH]O>>[C]=O:1.0, [CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]>>[CH]N([CH])[CH]:1.0, [C]:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN(C=[CH])C([O])[CH]:1.0, [C]=O:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])C=C[CH]>>[C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C([CH2])C1C([C]C([CH2])CC1O)[CH2]>>[C]C([CH2])C1C(=O)CC([C]C1[CH2])[CH2]:1.0, [C]C=[CH]:1.0, [C]C=[CH]>>[C]C[CH]:1.0, [C]C[CH]:1.0, [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]C1=CN(C=CC1)C2O[CH][CH]C2O:1.0, [N+]:1.0, [N]:1.0, [OH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH]:1.0, [C]:1.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([CH])[CH2]:1.0, [CH]C(O)[CH2]:1.0, [CH]O:1.0, [C]=O:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(C[CH])C([CH])[CH]:1.0, O=C([CH])[CH2]:1.0, [CH]C(O)[CH2]:1.0, [CH]CC(O)C([CH])[CH]:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[O=C([CH])[CH2]:1.0, [CH]C(O)[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(C[CH])C([CH])[CH]:1.0, [CH]CC(O)C([CH])[CH]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C=[CH]>>[C]C[CH]
2: [CH]C(O)[CH2]>>O=C([CH])[CH2]
3: [CH]O>>[C]=O
4: [CH][N+]([CH])=[CH]>>[CH]N([CH])[CH]

MMP Level 2
1: [C]C(=[CH])C=C[CH]>>[C]C(=[CH])CC=[CH]
2: [CH]CC(O)C([CH])[CH]>>O=C(C[CH])C([CH])[CH]
3: [CH]C(O)[CH2]>>O=C([CH])[CH2]
4: [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN(C=[CH])C([O])[CH]

MMP Level 3
1: O=C(N)C=1C=[N+]C=CC1>>O=C(N)C1=C[N]C=CC1
2: [C]C([CH2])C1C([C]C([CH2])CC1O)[CH2]>>[C]C([CH2])C1C(=O)CC([C]C1[CH2])[CH2]
3: [CH]CC(O)C([CH])[CH]>>O=C(C[CH])C([CH])[CH]
4: [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]C1=CN(C=CC1)C2O[CH][CH]C2O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)CCC(C)C1CCC2C3C(O)CC4CC(O)CCC4(C)C3CC(O)C21C>>O=C1CC2CC(O)CCC2(C)C3CC(O)C4(C)C(CCC4C(C)CCC(=O)O)C13, O=C(O)CCC(C)C1CCC2C3C(O)CC4CC(O)CCC4(C)C3CC(O)C21C>>O=C1CC2CC(O)CCC2(C)C3CC(O)C4(C)C(CCC4C(C)CCC(=O)O)C13]
2: R:M00002, P:M00004	[O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O, O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O]


//
SELECTED AAM MAPPING
[O:45]=[C:46]([OH:47])[CH2:48][CH2:49][CH:50]([CH3:51])[CH:52]1[CH2:53][CH2:54][CH:55]2[CH:56]3[CH:57]([OH:58])[CH2:59][CH:60]4[CH2:61][CH:62]([OH:63])[CH2:64][CH2:65][C:66]4([CH3:67])[CH:68]3[CH2:69][CH:70]([OH:71])[C:72]12[CH3:73].[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:5]:[CH:6]:[CH:7]:[N+:8](:[CH:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44]>>[H+:74].[O:45]=[C:46]([OH:47])[CH2:48][CH2:49][CH:50]([CH3:51])[CH:52]1[CH2:53][CH2:54][CH:55]2[CH:56]3[C:57](=[O:58])[CH2:59][CH:60]4[CH2:61][CH:62]([OH:63])[CH2:64][CH2:65][C:66]4([CH3:67])[CH:68]3[CH2:69][CH:70]([OH:71])[C:72]21[CH3:73].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:9][N:8]([CH:7]=[CH:6][CH2:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=51, 2=50, 3=49, 4=48, 5=46, 6=45, 7=47, 8=52, 9=53, 10=54, 11=55, 12=72, 13=70, 14=69, 15=68, 16=56, 17=57, 18=59, 19=60, 20=66, 21=65, 22=64, 23=62, 24=61, 25=63, 26=67, 27=58, 28=71, 29=73, 30=6, 31=5, 32=4, 33=9, 34=8, 35=7, 36=10, 37=43, 38=41, 39=12, 40=11, 41=13, 42=14, 43=15, 44=16, 45=17, 46=18, 47=19, 48=20, 49=21, 50=22, 51=23, 52=24, 53=39, 54=37, 55=26, 56=25, 57=27, 58=28, 59=29, 60=30, 61=35, 62=34, 63=33, 64=32, 65=31, 66=36, 67=38, 68=40, 69=42, 70=44, 71=2, 72=1, 73=3}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=51, 2=50, 3=49, 4=48, 5=46, 6=45, 7=47, 8=52, 9=53, 10=54, 11=55, 12=72, 13=70, 14=69, 15=68, 16=56, 17=57, 18=58, 19=59, 20=60, 21=66, 22=65, 23=64, 24=62, 25=61, 26=63, 27=67, 28=71, 29=73, 30=9, 31=8, 32=7, 33=6, 34=5, 35=4, 36=2, 37=1, 38=3, 39=10, 40=43, 41=41, 42=12, 43=11, 44=13, 45=14, 46=15, 47=16, 48=17, 49=18, 50=19, 51=20, 52=21, 53=22, 54=23, 55=24, 56=39, 57=37, 58=26, 59=25, 60=27, 61=28, 62=29, 63=30, 64=35, 65=34, 66=33, 67=32, 68=31, 69=36, 70=38, 71=40, 72=42, 73=44, 74=74}

