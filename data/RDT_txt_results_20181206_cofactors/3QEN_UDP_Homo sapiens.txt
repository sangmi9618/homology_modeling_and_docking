
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=P(O)(O)OC[CH]:1.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O[CH2]:1.0, OC1[CH]OC([CH2])C1O:2.0, OC1[CH]OC([CH2])C1O>>OC1[CH]OC([CH2])C1O:1.0, O[CH2]:1.0, O[CH2]>>[P]O[CH2]:1.0, [CH]:2.0, [CH]C([CH])O:2.0, [CH]C([CH])O>>[CH]C([CH])O:1.0, [CH]CO:1.0, [CH]CO>>O=P(O)(O)OC[CH]:1.0, [N]C1OC(CO)C(O)C1O>>[N]C1OC(C[O])C(O)C1O:1.0, [OH]:2.0, [O]:2.0, [O]C([CH])CO>>[O]C([CH])COP(=O)(O)O:1.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC[CH]:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[O]P(=O)(O)O:2.0, [P]O[CH2]:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)OC[CH]:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[P]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (1)
[[CH]C([CH])O:2.0]


ID=Reaction Center at Level: 2 (1)
[OC1[CH]OC([CH2])C1O:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: [O]P(=O)(O)O>>[O]P(=O)(O)O
2: O[CH2]>>[P]O[CH2]
3: [CH]C([CH])O>>[CH]C([CH])O
4: [P]O[P]>>[P]O

MMP Level 2
1: O=P(O)(O)O[P]>>O=P(O)(O)O[CH2]
2: [CH]CO>>O=P(O)(O)OC[CH]
3: OC1[CH]OC([CH2])C1O>>OC1[CH]OC([CH2])C1O
4: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O

MMP Level 3
1: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC[CH]
2: [O]C([CH])CO>>[O]C([CH])COP(=O)(O)O
3: [N]C1OC(CO)C(O)C1O>>[N]C1OC(C[O])C(O)C1O
4: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)O)C(O)C2O]
2: R:M00001, P:M00004	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2O>>O=P(O)(O)OCC1OC(n2cnc3c(nc(F)nc32)N)C(O)C1O]
3: R:M00002, P:M00004	[Fc1nc(N)c2ncn(c2n1)C3OC(CO)C(O)C3O>>O=P(O)(O)OCC1OC(n2cnc3c(nc(F)nc32)N)C(O)C1O, Fc1nc(N)c2ncn(c2n1)C3OC(CO)C(O)C3O>>O=P(O)(O)OCC1OC(n2cnc3c(nc(F)nc32)N)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][P:22](=[O:23])([OH:25])[OH:24])[CH:26]([OH:27])[CH:28]2[OH:29].[F:30][C:31]:1:[N:32]:[C:33]([NH2:34]):[C:35]:2:[N:36]:[CH:37]:[N:38](:[C:39]2:[N:40]1)[CH:41]3[O:42][CH:43]([CH2:44][OH:45])[CH:46]([OH:47])[CH:48]3[OH:49]>>[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[OH:21])[CH:26]([OH:27])[CH:28]2[OH:29].[O:23]=[P:22]([OH:24])([OH:25])[O:45][CH2:44][CH:43]1[O:42][CH:41]([N:38]:2:[CH:37]:[N:36]:[C:35]:3:[C:33](:[N:32]:[C:31]([F:30]):[N:40]:[C:39]32)[NH2:34])[CH:48]([OH:49])[CH:46]1[OH:47]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=3, 2=4, 3=5, 4=6, 5=7, 6=8, 7=2, 8=1, 9=9, 10=28, 11=26, 12=11, 13=10, 14=12, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=25, 28=27, 29=29, 30=37, 31=36, 32=35, 33=39, 34=38, 35=41, 36=48, 37=46, 38=43, 39=42, 40=44, 41=45, 42=47, 43=49, 44=40, 45=31, 46=32, 47=33, 48=34, 49=30}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=3, 2=4, 3=5, 4=6, 5=7, 6=8, 7=2, 8=1, 9=9, 10=24, 11=22, 12=11, 13=10, 14=12, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=23, 25=25, 26=36, 27=37, 28=38, 29=44, 30=35, 31=34, 32=46, 33=48, 34=32, 35=33, 36=31, 37=30, 38=27, 39=26, 40=28, 41=29, 42=49, 43=47, 44=43, 45=41, 46=40, 47=39, 48=45, 49=42}

