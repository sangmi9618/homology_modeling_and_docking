
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0, C-S:1.0]

//
FINGERPRINTS RC
[O=C(OP(=O)(O)O)C:1.0, O=C(O[P])C:1.0, O=C(SCC[NH])C>>[NH]CCS:1.0, O=C(SC[CH2])C:1.0, O=C(SC[CH2])C>>O=C(OP(=O)(O)O)C:1.0, O=C(SC[CH2])C>>SC[CH2]:1.0, O=C(S[CH2])C:1.0, O=C(S[CH2])C>>O=C(O[P])C:1.0, O=C([S])C:1.0, O=C([S])C>>[O]C(=O)C:1.0, O=P(O)(O)O:1.0, O=P(O)(O)O>>O=C(OP(=O)(O)O)C:2.0, SC[CH2]:1.0, S[CH2]:1.0, [C]:2.0, [C]O[P]:1.0, [C]S[CH2]:1.0, [C]S[CH2]>>S[CH2]:1.0, [OH]:1.0, [O]:1.0, [O]C(=O)C:1.0, [P]O:1.0, [P]O>>[C]O[P]:1.0, [SH]:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [O]:1.0, [S]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([S])C:1.0, [C]O[P]:1.0, [C]S[CH2]:1.0, [O]C(=O)C:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(OP(=O)(O)O)C:1.0, O=C(O[P])C:1.0, O=C(SC[CH2])C:1.0, O=C(S[CH2])C:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=C([S])C>>[O]C(=O)C
2: [C]S[CH2]>>S[CH2]
3: [P]O>>[C]O[P]

MMP Level 2
1: O=C(S[CH2])C>>O=C(O[P])C
2: O=C(SC[CH2])C>>SC[CH2]
3: O=P(O)(O)O>>O=C(OP(=O)(O)O)C

MMP Level 3
1: O=C(SC[CH2])C>>O=C(OP(=O)(O)O)C
2: O=C(SCC[NH])C>>[NH]CCS
3: O=P(O)(O)O>>O=C(OP(=O)(O)O)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=P(O)(O)O>>O=C(OP(=O)(O)O)C]
2: R:M00002, P:M00003	[O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C>>O=C(NCCS)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O]
3: R:M00002, P:M00004	[O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C>>O=C(OP(=O)(O)O)C]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([S:3][CH2:4][CH2:5][NH:6][C:7](=[O:8])[CH2:9][CH2:10][NH:11][C:12](=[O:13])[CH:14]([OH:15])[C:16]([CH3:17])([CH3:18])[CH2:19][O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[O:28][CH2:29][CH:30]1[O:31][CH:32]([N:33]:2:[CH:34]:[N:35]:[C:36]:3:[C:37](:[N:38]:[CH:39]:[N:40]:[C:41]32)[NH2:42])[CH:43]([OH:44])[CH:45]1[O:46][P:47](=[O:48])([OH:49])[OH:50])[CH3:51].[O:52]=[P:53]([OH:54])([OH:55])[OH:56]>>[O:1]=[C:2]([O:54][P:53](=[O:52])([OH:55])[OH:56])[CH3:51].[O:8]=[C:7]([NH:6][CH2:5][CH2:4][SH:3])[CH2:9][CH2:10][NH:11][C:12](=[O:13])[CH:14]([OH:15])[C:16]([CH3:17])([CH3:18])[CH2:19][O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[O:28][CH2:29][CH:30]1[O:31][CH:32]([N:33]:2:[CH:34]:[N:35]:[C:36]:3:[C:37](:[N:38]:[CH:39]:[N:40]:[C:41]32)[NH2:42])[CH:43]([OH:44])[CH:45]1[O:46][P:47](=[O:48])([OH:49])[OH:50]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=54, 2=53, 3=52, 4=55, 5=56, 6=51, 7=2, 8=1, 9=3, 10=4, 11=5, 12=6, 13=7, 14=8, 15=9, 16=10, 17=11, 18=12, 19=13, 20=14, 21=16, 22=17, 23=18, 24=19, 25=20, 26=21, 27=22, 28=23, 29=24, 30=25, 31=26, 32=27, 33=28, 34=29, 35=30, 36=45, 37=43, 38=32, 39=31, 40=33, 41=34, 42=35, 43=36, 44=41, 45=40, 46=39, 47=38, 48=37, 49=42, 50=44, 51=46, 52=47, 53=48, 54=49, 55=50, 56=15}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=15, 2=14, 3=16, 4=17, 5=18, 6=19, 7=20, 8=21, 9=22, 10=23, 11=24, 12=25, 13=26, 14=27, 15=28, 16=43, 17=41, 18=30, 19=29, 20=31, 21=32, 22=33, 23=34, 24=39, 25=38, 26=37, 27=36, 28=35, 29=40, 30=42, 31=44, 32=45, 33=46, 34=47, 35=48, 36=12, 37=10, 38=11, 39=9, 40=8, 41=7, 42=2, 43=1, 44=3, 45=4, 46=5, 47=6, 48=13, 49=56, 50=50, 51=49, 52=51, 53=52, 54=53, 55=54, 56=55}

