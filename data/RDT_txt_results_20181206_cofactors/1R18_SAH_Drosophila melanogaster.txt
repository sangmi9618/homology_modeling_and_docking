
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:3.0, C-N:4.0, C-O:3.0, C-S:1.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O=C(N[CH])C([NH])[CH2]:1.0, O=C(N[CH])C([NH])[CH2]>>O=C(OC)C([NH])[CH2]:1.0, O=C(N[CH])C[CH]:1.0, O=C(O)C(NC(=O)C([NH])[CH2])C>>O=C(O)C(NC(=O)C([NH])[CH2])C:1.0, O=C(O)C([NH])C:1.0, O=C(O)C([NH])C>>O=C(N[CH])C[CH]:1.0, O=C(O)C([NH])C>>O=C(OC)C([NH])[CH2]:1.0, O=C(O)CC[CH]:1.0, O=C(O)CC[CH]>>[C]NC(C(=O)O)C:1.0, O=C(OC)C([NH])[CH2]:1.0, O=C([CH])NC(C(=O)O)C>>[C]NC(C([O])=O)CC(=O)N[CH]:1.0, O=C([CH])O:2.0, O=C([CH])O>>O=C([CH])OC:1.0, O=C([CH])O>>O=C([NH])[CH2]:1.0, O=C([CH])OC:1.0, O=C([CH])[NH]:1.0, O=C([CH])[NH]>>[O]C(=O)[CH]:1.0, O=C([NH])[CH2]:1.0, S([CH2])[CH2]:1.0, [CH2]:3.0, [CH2][S+]([CH2])C:2.0, [CH2][S+]([CH2])C>>S([CH2])[CH2]:1.0, [CH3]:4.0, [CH]:4.0, [CH]C:2.0, [CH]C>>[O]C:1.0, [CH]CSC[CH2]:1.0, [CH]C[CH2]:1.0, [CH]C[CH2]>>[CH]C:1.0, [CH]C[S+](C)C[CH2]:1.0, [CH]C[S+](C)C[CH2]>>[CH]CSC[CH2]:1.0, [C]:4.0, [C]C(NC(=O)[CH])C:2.0, [C]C(NC(=O)[CH])C>>[C]C(NC(=O)[CH])C:1.0, [C]C([CH2])NC(=O)[CH2]:1.0, [C]C([CH2])NC(=O)[CH]:1.0, [C]C([CH2])NC(=O)[CH]>>[C]C([CH2])NC(=O)[CH2]:1.0, [C]C([NH])C:4.0, [C]C([NH])C>>[C]C[CH]:1.0, [C]C([NH])C>>[C]OC:1.0, [C]C([NH])CC(=O)[NH]:1.0, [C]C([NH])CCC(=O)O>>O=C([CH])NC(C(=O)O)C:1.0, [C]C([NH])[CH2]:2.0, [C]C([NH])[CH2]>>[C]C([NH])[CH2]:1.0, [C]CC(NC(=O)C([NH])[CH2])C(=O)[NH]>>[C]CC(NC(=O)C[CH])C(=O)[NH]:1.0, [C]CCC(NC(=O)[CH])C(=O)N[CH]>>O=C([CH])NC(C(=O)OC)CC(=O)[NH]:1.0, [C]CCC([C])[NH]:1.0, [C]CCC([C])[NH]>>[C]C([NH])C:1.0, [C]C[CH2]:1.0, [C]C[CH2]>>[C]C([NH])C:1.0, [C]C[CH]:1.0, [C]NC(C(=O)NC([C])[CH2])C[CH2]>>[C]NC(C(=O)OC)C[C]:1.0, [C]NC(C(=O)O)C:2.0, [C]NC(C(=O)O)C>>O=C([CH])OC:1.0, [C]NC(C(=O)O)C>>[C]C([CH2])NC(=O)CC([C])[NH]:1.0, [C]NC(C(=O)O)C>>[C]C([NH])CC(=O)[NH]:1.0, [C]NC(C(=O)[NH])CCC(=O)O>>[C]NC(C(=O)O)C:1.0, [C]NC(C(=O)[NH])C[CH2]:1.0, [C]NC(C(=O)[NH])C[CH2]>>[C]NC(C([O])=O)C[C]:1.0, [C]NC(C([O])=O)C[C]:1.0, [C]N[CH]:4.0, [C]N[CH]>>[C]N[CH]:2.0, [C]O:1.0, [C]O>>[C]OC:1.0, [C]OC:2.0, [NH]:4.0, [OH]:1.0, [O]:1.0, [O]C:1.0, [O]C(=O)[CH]:1.0, [O]C([CH])C[S+](C)CC[CH]>>[O]C([CH])CSCC[CH]:1.0, [S+]:1.0, [S+]C:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (8)
[[CH2]:2.0, [CH3]:3.0, [CH]:5.0, [C]:4.0, [NH]:4.0, [OH]:1.0, [O]:2.0, [S+]:1.0]


ID=Reaction Center at Level: 1 (15)
[O=C([CH])O:1.0, O=C([CH])[NH]:1.0, O=C([NH])[CH2]:1.0, [CH2][S+]([CH2])C:1.0, [CH]C:1.0, [CH]C[CH2]:1.0, [C]C([NH])C:3.0, [C]C([NH])[CH2]:2.0, [C]C[CH]:1.0, [C]N[CH]:4.0, [C]O:1.0, [C]OC:2.0, [O]C:1.0, [O]C(=O)[CH]:1.0, [S+]C:1.0]


ID=Reaction Center at Level: 2 (18)
[O=C(N[CH])C([NH])[CH2]:1.0, O=C(N[CH])C[CH]:1.0, O=C(O)C([NH])C:1.0, O=C(OC)C([NH])[CH2]:1.0, O=C([CH])O:1.0, O=C([CH])OC:2.0, [CH2][S+]([CH2])C:1.0, [CH]C[S+](C)C[CH2]:1.0, [C]C(NC(=O)[CH])C:2.0, [C]C([CH2])NC(=O)[CH2]:1.0, [C]C([CH2])NC(=O)[CH]:1.0, [C]C([NH])C:1.0, [C]C([NH])CC(=O)[NH]:1.0, [C]CCC([C])[NH]:1.0, [C]NC(C(=O)O)C:3.0, [C]NC(C(=O)[NH])C[CH2]:1.0, [C]NC(C([O])=O)C[C]:1.0, [C]OC:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH2]:2.0, [CH]:2.0]


ID=Reaction Center at Level: 1 (3)
[[C]C([NH])C:2.0, [C]C[CH2]:1.0, [C]C[CH]:1.0]


ID=Reaction Center at Level: 2 (3)
[O=C(O)CC[CH]:1.0, [C]C([NH])CC(=O)[NH]:1.0, [C]NC(C(=O)O)C:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]N[CH]>>[C]N[CH]
2: [CH]C[CH2]>>[CH]C
3: [CH2][S+]([CH2])C>>S([CH2])[CH2]
4: O=C([CH])[NH]>>[O]C(=O)[CH]
5: [C]C([NH])[CH2]>>[C]C([NH])[CH2]
6: [C]N[CH]>>[C]N[CH]
7: [CH]C>>[O]C
8: [C]C[CH2]>>[C]C([NH])C
9: O=C([CH])O>>O=C([NH])[CH2]
10: [C]C([NH])C>>[C]C[CH]
11: [C]O>>[C]OC

MMP Level 2
1: [C]C([CH2])NC(=O)[CH]>>[C]C([CH2])NC(=O)[CH2]
2: [C]CCC([C])[NH]>>[C]C([NH])C
3: [CH]C[S+](C)C[CH2]>>[CH]CSC[CH2]
4: O=C(N[CH])C([NH])[CH2]>>O=C(OC)C([NH])[CH2]
5: [C]NC(C(=O)[NH])C[CH2]>>[C]NC(C([O])=O)C[C]
6: [C]C(NC(=O)[CH])C>>[C]C(NC(=O)[CH])C
7: [C]C([NH])C>>[C]OC
8: O=C(O)CC[CH]>>[C]NC(C(=O)O)C
9: O=C(O)C([NH])C>>O=C(N[CH])C[CH]
10: [C]NC(C(=O)O)C>>[C]C([NH])CC(=O)[NH]
11: O=C([CH])O>>O=C([CH])OC

MMP Level 3
1: [C]CC(NC(=O)C([NH])[CH2])C(=O)[NH]>>[C]CC(NC(=O)C[CH])C(=O)[NH]
2: [C]NC(C(=O)[NH])CCC(=O)O>>[C]NC(C(=O)O)C
3: [O]C([CH])C[S+](C)CC[CH]>>[O]C([CH])CSCC[CH]
4: [C]NC(C(=O)NC([C])[CH2])C[CH2]>>[C]NC(C(=O)OC)C[C]
5: [C]CCC(NC(=O)[CH])C(=O)N[CH]>>O=C([CH])NC(C(=O)OC)CC(=O)[NH]
6: O=C(O)C(NC(=O)C([NH])[CH2])C>>O=C(O)C(NC(=O)C([NH])[CH2])C
7: [C]NC(C(=O)O)C>>O=C([CH])OC
8: [C]C([NH])CCC(=O)O>>O=C([CH])NC(C(=O)O)C
9: [C]NC(C(=O)O)C>>[C]C([CH2])NC(=O)CC([C])[NH]
10: O=C([CH])NC(C(=O)O)C>>[C]NC(C([O])=O)CC(=O)N[CH]
11: O=C(O)C([NH])C>>O=C(OC)C([NH])[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(O)C(N)CCSCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00002, P:M00004	[O=C(O)CCC(NC(=O)C1N(C(=O)C(NC(=O)C(N)C(C)C)Cc2ccc(O)cc2)CCC1)C(=O)NC(C(=O)NC(C(=O)O)C)Cc3cnc[nH]3>>O=C(O)C(NC(=O)C(NC(=O)CC(NC(=O)C1N(C(=O)C(NC(=O)C(N)C(C)C)Cc2ccc(O)cc2)CCC1)C(=O)OC)Cc3cnc[nH]3)C, O=C(O)CCC(NC(=O)C1N(C(=O)C(NC(=O)C(N)C(C)C)Cc2ccc(O)cc2)CCC1)C(=O)NC(C(=O)NC(C(=O)O)C)Cc3cnc[nH]3>>O=C(O)C(NC(=O)C(NC(=O)CC(NC(=O)C1N(C(=O)C(NC(=O)C(N)C(C)C)Cc2ccc(O)cc2)CCC1)C(=O)OC)Cc3cnc[nH]3)C, O=C(O)CCC(NC(=O)C1N(C(=O)C(NC(=O)C(N)C(C)C)Cc2ccc(O)cc2)CCC1)C(=O)NC(C(=O)NC(C(=O)O)C)Cc3cnc[nH]3>>O=C(O)C(NC(=O)C(NC(=O)CC(NC(=O)C1N(C(=O)C(NC(=O)C(N)C(C)C)Cc2ccc(O)cc2)CCC1)C(=O)OC)Cc3cnc[nH]3)C, O=C(O)CCC(NC(=O)C1N(C(=O)C(NC(=O)C(N)C(C)C)Cc2ccc(O)cc2)CCC1)C(=O)NC(C(=O)NC(C(=O)O)C)Cc3cnc[nH]3>>O=C(O)C(NC(=O)C(NC(=O)CC(NC(=O)C1N(C(=O)C(NC(=O)C(N)C(C)C)Cc2ccc(O)cc2)CCC1)C(=O)OC)Cc3cnc[nH]3)C, O=C(O)CCC(NC(=O)C1N(C(=O)C(NC(=O)C(N)C(C)C)Cc2ccc(O)cc2)CCC1)C(=O)NC(C(=O)NC(C(=O)O)C)Cc3cnc[nH]3>>O=C(O)C(NC(=O)C(NC(=O)CC(NC(=O)C1N(C(=O)C(NC(=O)C(N)C(C)C)Cc2ccc(O)cc2)CCC1)C(=O)OC)Cc3cnc[nH]3)C, O=C(O)CCC(NC(=O)C1N(C(=O)C(NC(=O)C(N)C(C)C)Cc2ccc(O)cc2)CCC1)C(=O)NC(C(=O)NC(C(=O)O)C)Cc3cnc[nH]3>>O=C(O)C(NC(=O)C(NC(=O)CC(NC(=O)C1N(C(=O)C(NC(=O)C(N)C(C)C)Cc2ccc(O)cc2)CCC1)C(=O)OC)Cc3cnc[nH]3)C, O=C(O)CCC(NC(=O)C1N(C(=O)C(NC(=O)C(N)C(C)C)Cc2ccc(O)cc2)CCC1)C(=O)NC(C(=O)NC(C(=O)O)C)Cc3cnc[nH]3>>O=C(O)C(NC(=O)C(NC(=O)CC(NC(=O)C1N(C(=O)C(NC(=O)C(N)C(C)C)Cc2ccc(O)cc2)CCC1)C(=O)OC)Cc3cnc[nH]3)C, O=C(O)CCC(NC(=O)C1N(C(=O)C(NC(=O)C(N)C(C)C)Cc2ccc(O)cc2)CCC1)C(=O)NC(C(=O)NC(C(=O)O)C)Cc3cnc[nH]3>>O=C(O)C(NC(=O)C(NC(=O)CC(NC(=O)C1N(C(=O)C(NC(=O)C(N)C(C)C)Cc2ccc(O)cc2)CCC1)C(=O)OC)Cc3cnc[nH]3)C, O=C(O)CCC(NC(=O)C1N(C(=O)C(NC(=O)C(N)C(C)C)Cc2ccc(O)cc2)CCC1)C(=O)NC(C(=O)NC(C(=O)O)C)Cc3cnc[nH]3>>O=C(O)C(NC(=O)C(NC(=O)CC(NC(=O)C1N(C(=O)C(NC(=O)C(N)C(C)C)Cc2ccc(O)cc2)CCC1)C(=O)OC)Cc3cnc[nH]3)C, O=C(O)CCC(NC(=O)C1N(C(=O)C(NC(=O)C(N)C(C)C)Cc2ccc(O)cc2)CCC1)C(=O)NC(C(=O)NC(C(=O)O)C)Cc3cnc[nH]3>>O=C(O)C(NC(=O)C(NC(=O)CC(NC(=O)C1N(C(=O)C(NC(=O)C(N)C(C)C)Cc2ccc(O)cc2)CCC1)C(=O)OC)Cc3cnc[nH]3)C]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH2:4][CH2:5][CH:6]([NH:7][C:8](=[O:9])[CH:10]1[N:11]([C:12](=[O:13])[CH:14]([NH:15][C:16](=[O:17])[CH:18]([NH2:19])[CH:20]([CH3:21])[CH3:22])[CH2:23][C:24]:2:[CH:25]:[CH:26]:[C:27]([OH:28]):[CH:29]:[CH:30]2)[CH2:31][CH2:32][CH2:33]1)[C:34](=[O:35])[NH:36][CH:37]([C:38](=[O:39])[NH:40][CH:41]([C:42](=[O:43])[OH:44])[CH3:45])[CH2:46][C:47]:3:[CH:48]:[N:49]:[CH:50]:[NH:51]3.[O:52]=[C:53]([OH:54])[CH:55]([NH2:56])[CH2:57][CH2:58][S+:59]([CH3:78])[CH2:60][CH:61]1[O:62][CH:63]([N:64]:2:[CH:65]:[N:66]:[C:67]:3:[C:68](:[N:69]:[CH:70]:[N:71]:[C:72]32)[NH2:73])[CH:74]([OH:75])[CH:76]1[OH:77]>>[O:52]=[C:53]([OH:54])[CH:55]([NH2:56])[CH2:57][CH2:58][S:59][CH2:60][CH:61]1[O:62][CH:63]([N:64]:2:[CH:65]:[N:66]:[C:67]:3:[C:68](:[N:69]:[CH:70]:[N:71]:[C:72]32)[NH2:73])[CH:74]([OH:75])[CH:76]1[OH:77].[O:1]=[C:2]([OH:3])[CH:4]([NH:40][C:38](=[O:39])[CH:37]([NH:36][C:42](=[O:43])[CH2:41][CH:6]([NH:7][C:8](=[O:9])[CH:10]1[N:11]([C:12](=[O:13])[CH:14]([NH:15][C:16](=[O:17])[CH:18]([NH2:19])[CH:20]([CH3:22])[CH3:21])[CH2:23][C:24]:2:[CH:25]:[CH:26]:[C:27]([OH:28]):[CH:29]:[CH:30]2)[CH2:31][CH2:32][CH2:33]1)[C:34](=[O:35])[O:44][CH3:45])[CH2:46][C:47]:3:[CH:48]:[N:49]:[CH:50]:[NH:51]3)[CH3:5]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=60, 2=59, 3=58, 4=57, 5=55, 6=53, 7=52, 8=54, 9=56, 10=61, 11=62, 12=77, 13=75, 14=64, 15=63, 16=65, 17=66, 18=67, 19=68, 20=73, 21=72, 22=71, 23=70, 24=69, 25=74, 26=76, 27=78, 28=45, 29=41, 30=42, 31=43, 32=44, 33=40, 34=38, 35=39, 36=37, 37=46, 38=47, 39=48, 40=49, 41=50, 42=51, 43=36, 44=34, 45=35, 46=6, 47=5, 48=4, 49=2, 50=1, 51=3, 52=7, 53=8, 54=9, 55=10, 56=33, 57=32, 58=31, 59=11, 60=12, 61=13, 62=14, 63=23, 64=24, 65=25, 66=26, 67=27, 68=29, 69=30, 70=28, 71=15, 72=16, 73=17, 74=18, 75=20, 76=21, 77=22, 78=19}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=70, 2=71, 3=72, 4=67, 5=68, 6=69, 7=73, 8=66, 9=65, 10=64, 11=63, 12=74, 13=76, 14=61, 15=62, 16=60, 17=59, 18=58, 19=57, 20=55, 21=53, 22=52, 23=54, 24=56, 25=77, 26=75, 27=51, 28=4, 29=2, 30=1, 31=3, 32=5, 33=6, 34=7, 35=8, 36=45, 37=46, 38=47, 39=48, 40=49, 41=50, 42=9, 43=10, 44=11, 45=12, 46=13, 47=41, 48=42, 49=43, 50=44, 51=14, 52=15, 53=16, 54=17, 55=40, 56=39, 57=38, 58=18, 59=19, 60=20, 61=21, 62=30, 63=31, 64=32, 65=33, 66=34, 67=36, 68=37, 69=35, 70=22, 71=23, 72=24, 73=25, 74=27, 75=28, 76=29, 77=26}

