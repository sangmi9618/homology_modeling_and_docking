
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-S:1.0]

ORDER_CHANGED
[C-C*C=C:1.0]

//
FINGERPRINTS RC
[O=C(C=C)N([CH2])[CH2]>>O=C(N([CH2])[CH2])CCS[CH2]:1.0, S([CH2])[CH2]:1.0, S[CH2]:1.0, S[CH2]>>S([CH2])[CH2]:1.0, [CH2]:3.0, [CH]:1.0, [CH]=C:1.0, [CH]=C>>[S]C[CH2]:1.0, [CH]CS:1.0, [CH]CS>>[CH]CSC[CH2]:1.0, [CH]CSC[CH2]:1.0, [C]C([NH])CS>>[C]CCSCC([C])[NH]:1.0, [C]C=C:2.0, [C]C=C>>[C]CCS[CH2]:1.0, [C]C=C>>[C]C[CH2]:1.0, [C]CCS[CH2]:1.0, [C]C[CH2]:1.0, [N]C(=O)C=C:1.0, [N]C(=O)C=C>>[N]C(=O)CCSC[CH]:1.0, [N]C(=O)C=C>>[N]C(=O)CC[S]:1.0, [N]C(=O)CC[S]:1.0, [SH]:1.0, [S]:1.0, [S]C[CH2]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH2]:1.0, [S]:1.0]


ID=Reaction Center at Level: 1 (2)
[S([CH2])[CH2]:1.0, [S]C[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[[CH]CSC[CH2]:1.0, [C]CCS[CH2]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (2)
[[CH2]:3.0, [CH]:1.0]


ID=Reaction Center at Level: 1 (4)
[[CH]=C:1.0, [C]C=C:1.0, [C]C[CH2]:1.0, [S]C[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]C=C:1.0, [C]CCS[CH2]:1.0, [N]C(=O)C=C:1.0, [N]C(=O)CC[S]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH]=C>>[S]C[CH2]
2: S[CH2]>>S([CH2])[CH2]
3: [C]C=C>>[C]C[CH2]

MMP Level 2
1: [C]C=C>>[C]CCS[CH2]
2: [CH]CS>>[CH]CSC[CH2]
3: [N]C(=O)C=C>>[N]C(=O)CC[S]

MMP Level 3
1: [N]C(=O)C=C>>[N]C(=O)CCSC[CH]
2: [C]C([NH])CS>>[C]CCSCC([C])[NH]
3: O=C(C=C)N([CH2])[CH2]>>O=C(N([CH2])[CH2])CCS[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(C=C)N1CN(C(=O)C=C)CN(C(=O)C=C)C1>>O=C(O)CNC(=O)C(NC(=O)CCC(N)C(=O)O)CSCCC(=O)N1CN(C(=O)C=C)CN(C(=O)C=C)C1, O=C(C=C)N1CN(C(=O)C=C)CN(C(=O)C=C)C1>>O=C(O)CNC(=O)C(NC(=O)CCC(N)C(=O)O)CSCCC(=O)N1CN(C(=O)C=C)CN(C(=O)C=C)C1]
2: R:M00002, P:M00003	[O=C(O)CNC(=O)C(NC(=O)CCC(N)C(=O)O)CS>>O=C(O)CNC(=O)C(NC(=O)CCC(N)C(=O)O)CSCCC(=O)N1CN(C(=O)C=C)CN(C(=O)C=C)C1]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH2:4][NH:5][C:6](=[O:7])[CH:8]([NH:9][C:10](=[O:11])[CH2:12][CH2:13][CH:14]([NH2:15])[C:16](=[O:17])[OH:18])[CH2:19][SH:20].[O:21]=[C:22]([CH:23]=[CH2:24])[N:25]1[CH2:26][N:27]([C:28](=[O:29])[CH:30]=[CH2:31])[CH2:32][N:33]([C:34](=[O:35])[CH:36]=[CH2:37])[CH2:38]1>>[O:1]=[C:2]([OH:3])[CH2:4][NH:5][C:6](=[O:7])[CH:8]([NH:9][C:10](=[O:11])[CH2:12][CH2:13][CH:14]([NH2:15])[C:16](=[O:17])[OH:18])[CH2:19][S:20][CH2:37][CH2:36][C:34](=[O:35])[N:33]1[CH2:32][N:27]([C:28](=[O:29])[CH:30]=[CH2:31])[CH2:26][N:25]([C:22](=[O:21])[CH:23]=[CH2:24])[CH2:38]1


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=23, 3=22, 4=21, 5=25, 6=38, 7=33, 8=32, 9=27, 10=26, 11=28, 12=29, 13=30, 14=31, 15=34, 16=35, 17=36, 18=37, 19=13, 20=12, 21=10, 22=11, 23=9, 24=8, 25=19, 26=20, 27=6, 28=7, 29=5, 30=4, 31=2, 32=1, 33=3, 34=14, 35=16, 36=17, 37=18, 38=15}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=31, 2=30, 3=28, 4=29, 5=27, 6=26, 7=25, 8=38, 9=33, 10=32, 11=34, 12=35, 13=36, 14=37, 15=23, 16=24, 17=22, 18=21, 19=20, 20=19, 21=8, 22=6, 23=7, 24=5, 25=4, 26=2, 27=1, 28=3, 29=9, 30=10, 31=11, 32=12, 33=13, 34=14, 35=16, 36=17, 37=18, 38=15}

