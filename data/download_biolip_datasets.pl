#!/usr/bin/perl -w
use strict;

`mkdir -p BioLiP_updated_set`;
chdir "BioLiP_updated_set";

system("wget -o log -c https://zhanglab.ccmb.med.umich.edu/BioLiP/download/receptor_2013-03-6.tar.bz2") == 0 or die "System call failed: $!";
system("tar -xvf receptor_2013-03-6.tar.bz2>log") == 0 or die "System call failed: $!";
system("wget -o log -c https://zhanglab.ccmb.med.umich.edu/BioLiP/download/ligand_2013-03-6.tar.bz2") == 0 or die "System call failed: $!";
system("tar -xvf ligand_2013-03-6.tar.bz2>log") == 0 or die "System call failed: $!";
system("wget -o log -c https://zhanglab.ccmb.med.umich.edu/BioLiP/download/BioLiP.tar.bz2") == 0 or die "System call failed: $!";
system("tar -xvf BioLiP.tar.bz2>log") == 0 or die "System call failed: $!";

my $head= "http://zhanglab.ccmb.med.umich.edu/BioLiP/weekly";
my $address="http://zhanglab.ccmb.med.umich.edu/BioLiP/weekly.html";
system("wget -o log -c $address") == 0 or die "System call failed: $!";

my @rst=`cat weekly.html`;
my @all=();
foreach my $r(@rst)
{
    if($r =~ /\<tr\>\<td\>(\S+)\<\/td\>/)
    {
	push(@all, $1);
    }
}
my $tot=@all;
print "\n====================================================\n";
print "In total, there are $tot weeks to update.\n\n";

my $annotation="BioLiP_UP.txt";
open(OUT, ">$annotation");
close(OUT);

`cat BioLiP_2013-03-6.txt >> $annotation`;

foreach my $r(@all)
{        
    my $rec="receptor_$r.tar.bz2";
    #my $rec1="receptor1_$r.tar.bz2";
    my $lig="ligand_$r.tar.bz2";
    my $ano="BioLiP_$r.txt";

    if(-s $ano)
    {
	print "The week $r was updated before, skip this one\n";	
    }
    else
    {
	print "Dowload redundant set for the week $r...\n";    
	system("wget -o log -c $head/$rec") == 0 or die "System call failed: $!";
	system("tar -xvf $rec >log")== 0 or die "System call failed: $!";
	system("wget -o log -c $head/$lig") == 0 or die "System call failed: $!";
	system("tar -xvf $lig >log")== 0 or die "System call failed: $!";
	system("wget -o log -c $head/$ano") == 0 or die "System call failed: $!"; 	
    }

    `cat $ano >> $annotation`;

    #last;
}

`rm *.tar.bz2`;
`mv BioLiP_UP.txt BioLiP_UP.out`;
`rm *.txt`;
`rm log`;

print "Cheers! All updates are done.\n";
print "====================================================\n\n";

print "Please download the old sets manually at 
http://zhanglab.ccmb.med.umich.edu/BioLiP/download.html

Please read http://zhanglab.ccmb.med.umich.edu/BioLiP/download/readme.txt 
about explanation of the annotation file.
";

print "Please feel free to contact me (Jianyi, yangji\@umich.edu) if you have any problems with BioLiP.\n
Thanks for using the BioLiP database!

---------------------------------------------
Please cite the following paper if you use BioLiP in your projects:

Jianyi Yang, Ambrish Roy, Yang Zhang, BioLiP: a semi-manually curated database for biologically 
relevant ligand-protein interactions, Nucleic Acids Research, 41:D1096-D1103, 2013. 

";


