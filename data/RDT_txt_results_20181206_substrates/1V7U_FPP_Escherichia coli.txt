
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:2.0, C-O:1.0]

ORDER_CHANGED
[C-C*C=C:2.0]

//
FINGERPRINTS RC
[[CH2]:5.0, [CH2]C(=C)C:2.0, [CH2]C(=C)C>>[CH]=C([CH2])C:1.0, [CH2]C(=C)C>>[CH]CCC(=[CH])C:1.0, [CH2]C=C(C)C[CH2]:1.0, [CH2]CC(=C)C:1.0, [CH2]CC(=C)C>>[CH2]C=C(C)C[CH2]:1.0, [CH2]CC(=C)C>>[C]=CCCC(=C[CH2])C:1.0, [CH]:1.0, [CH]=C([CH2])C:1.0, [CH]CCC(=[CH])C:1.0, [CH]C[CH2]:1.0, [C]:2.0, [C]=C:1.0, [C]=C>>[C]C[CH2]:1.0, [C]=CCOP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, [C]=CCO[P]:1.0, [C]=CCO[P]>>[C]CCC=[C]:1.0, [C]=C[CH2]:1.0, [C]CCC=[C]:1.0, [C]C[CH2]:2.0, [C]C[CH2]>>[C]=C[CH2]:1.0, [OH]:1.0, [O]:1.0, [O]CC=C([CH2])C:1.0, [O]CCC(=C)C:1.0, [O]CCC(=C)C>>[O]CC=C(C)CC[CH]:1.0, [O]CCC(=C)C>>[O]CC=C([CH2])C:1.0, [O]C[CH]:1.0, [O]C[CH]>>[CH]C[CH2]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OCC=C([CH2])C>>[CH]=C(C)CCC=C([CH2])C:1.0, [O]P(=O)(O)OC[CH]:1.0, [O]P(=O)(O)OC[CH]>>[O]P(=O)(O)O:1.0, [P]O:1.0, [P]OCCC(=C)C>>[P]OCC=C(C)C[CH2]:1.0, [P]O[CH2]:1.0, [P]O[CH2]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH2]:5.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[CH]C[CH2]:2.0, [C]C[CH2]:2.0, [O]C[CH]:1.0, [P]O[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[[CH]CCC(=[CH])C:2.0, [C]=CCO[P]:1.0, [C]CCC=[C]:2.0, [O]P(=O)(O)OC[CH]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[CH2]:3.0, [CH]:1.0, [C]:2.0]


ID=Reaction Center at Level: 1 (5)
[[CH2]C(=C)C:1.0, [CH]=C([CH2])C:1.0, [C]=C:1.0, [C]=C[CH2]:1.0, [C]C[CH2]:2.0]


ID=Reaction Center at Level: 2 (6)
[[CH2]C(=C)C:1.0, [CH2]C=C(C)C[CH2]:1.0, [CH2]CC(=C)C:1.0, [CH]CCC(=[CH])C:1.0, [O]CC=C([CH2])C:1.0, [O]CCC(=C)C:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]C[CH]>>[CH]C[CH2]
2: [C]C[CH2]>>[C]=C[CH2]
3: [P]O[CH2]>>[P]O
4: [CH2]C(=C)C>>[CH]=C([CH2])C
5: [C]=C>>[C]C[CH2]

MMP Level 2
1: [C]=CCO[P]>>[C]CCC=[C]
2: [O]CCC(=C)C>>[O]CC=C([CH2])C
3: [O]P(=O)(O)OC[CH]>>[O]P(=O)(O)O
4: [CH2]CC(=C)C>>[CH2]C=C(C)C[CH2]
5: [CH2]C(=C)C>>[CH]CCC(=[CH])C

MMP Level 3
1: [O]P(=O)(O)OCC=C([CH2])C>>[CH]=C(C)CCC=C([CH2])C
2: [P]OCCC(=C)C>>[P]OCC=C(C)C[CH2]
3: [C]=CCOP(=O)(O)O[P]>>O=P(O)(O)O[P]
4: [O]CCC(=C)C>>[O]CC=C(C)CC[CH]
5: [CH2]CC(=C)C>>[C]=CCCC(=C[CH2])C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)C>>O=P(O)(O)OP(=O)(O)O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)C>>O=P(O)(O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)C]
3: R:M00002, P:M00004	[O=P(O)(O)OP(=O)(O)OCCC(=C)C>>O=P(O)(O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)C, O=P(O)(O)OP(=O)(O)OCCC(=C)C>>O=P(O)(O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)C, O=P(O)(O)OP(=O)(O)OCCC(=C)C>>O=P(O)(O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)CCC=C(C)C]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][CH2:10][CH:11]=[C:12]([CH3:13])[CH2:14][CH2:15][CH:16]=[C:17]([CH3:18])[CH2:19][CH2:20][CH:21]=[C:22]([CH3:23])[CH3:24].[O:25]=[P:26]([OH:27])([OH:28])[O:29][P:30](=[O:31])([OH:32])[O:33][CH2:34][CH2:35][C:36](=[CH2:37])[CH3:38]>>[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[OH:9].[O:25]=[P:26]([OH:27])([OH:28])[O:29][P:30](=[O:31])([OH:32])[O:33][CH2:34][CH:35]=[C:36]([CH3:38])[CH2:37][CH2:39][CH:40]=[C:41]([CH3:42])[CH2:43][CH2:44][CH:45]=[C:46]([CH3:47])[CH2:48][CH2:49][CH:50]=[C:51]([CH3:52])[CH2:53][CH2:54][CH:55]=[C:56]([CH3:57])[CH2:58][CH2:59][CH:60]=[C:61]([CH3:62])[CH2:63][CH2:64][CH:65]=[C:66]([CH3:67])[CH2:68][CH2:69][CH:70]=[C:71]([CH3:72])[CH2:73][CH2:10][CH:11]=[C:12]([CH3:13])[CH2:14][CH2:15][CH:16]=[C:17]([CH3:18])[CH2:19][CH2:20][CH:21]=[C:22]([CH3:23])[CH3:24]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=23, 2=22, 3=21, 4=20, 5=19, 6=17, 7=16, 8=15, 9=14, 10=12, 11=11, 12=10, 13=9, 14=6, 15=7, 16=8, 17=5, 18=2, 19=1, 20=3, 21=4, 22=13, 23=18, 24=24, 25=38, 26=36, 27=37, 28=35, 29=34, 30=33, 31=30, 32=31, 33=32, 34=29, 35=26, 36=25, 37=27, 38=28}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=67, 2=66, 3=65, 4=68, 5=69, 6=70, 7=71, 8=72, 9=73, 10=64, 11=62, 12=61, 13=60, 14=59, 15=57, 16=56, 17=55, 18=54, 19=52, 20=51, 21=50, 22=49, 23=47, 24=46, 25=45, 26=44, 27=42, 28=41, 29=40, 30=39, 31=37, 32=36, 33=35, 34=34, 35=32, 36=31, 37=30, 38=29, 39=27, 40=26, 41=25, 42=24, 43=22, 44=21, 45=20, 46=19, 47=17, 48=16, 49=15, 50=14, 51=12, 52=11, 53=10, 54=9, 55=6, 56=7, 57=8, 58=5, 59=2, 60=1, 61=3, 62=4, 63=13, 64=18, 65=23, 66=28, 67=33, 68=38, 69=43, 70=48, 71=53, 72=58, 73=63}

