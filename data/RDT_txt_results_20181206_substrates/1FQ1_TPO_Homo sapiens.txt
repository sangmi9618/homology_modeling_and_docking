
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O:3.0, O=P(O)(O)O:3.0, O=P(O)(O)OC([CH])C:1.0, O=P(O)(O)OC([CH])C>>O=P(O)(O)O:1.0, O=P(O)(O)OC([CH])C>>[CH]C(O)C:1.0, O=P(O)(O)O[CH]:1.0, O=P(O)(O)O[CH]>>O=P(O)(O)O:1.0, O>>O=P(O)(O)O:2.0, O>>[P]O:1.0, [CH]C(O)C:1.0, [CH]O:1.0, [C]C(N)C(OP(=O)(O)O)C>>[C]C(N)C(O)C:1.0, [OH]:2.0, [O]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH]:1.0, [P]O[CH]>>[CH]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=P(O)(O)O:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=P(O)(O)O:2.0, O=P(O)(O)OC([CH])C:1.0, O=P(O)(O)O[CH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O>>[P]O
2: [P]O[CH]>>[CH]O
3: [O]P(=O)(O)O>>O=P(O)(O)O

MMP Level 2
1: O>>O=P(O)(O)O
2: O=P(O)(O)OC([CH])C>>[CH]C(O)C
3: O=P(O)(O)O[CH]>>O=P(O)(O)O

MMP Level 3
1: O>>O=P(O)(O)O
2: [C]C(N)C(OP(=O)(O)O)C>>[C]C(N)C(O)C
3: O=P(O)(O)OC([CH])C>>O=P(O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)C(OP(=O)(O)O)C>>O=C(O)C(N)C(O)C]
2: R:M00001, P:M00004	[O=C(O)C(N)C(OP(=O)(O)O)C>>O=P(O)(O)O]
3: R:M00002, P:M00004	[O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH:6]([O:7][P:8](=[O:9])([OH:10])[OH:11])[CH3:12].[OH2:13]>>[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH:6]([OH:7])[CH3:12].[O:9]=[P:8]([OH:11])([OH:13])[OH:10]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=12, 2=6, 3=4, 4=2, 5=1, 6=3, 7=5, 8=7, 9=8, 10=9, 11=10, 12=11, 13=13}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=8, 2=6, 3=4, 4=2, 5=1, 6=3, 7=5, 8=7, 9=11, 10=10, 11=9, 12=12, 13=13}

