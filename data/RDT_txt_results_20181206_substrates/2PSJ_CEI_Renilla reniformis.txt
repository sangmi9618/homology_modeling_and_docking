
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%C:1.0, C%N:1.0, C=O:2.0, O=O:1.0]

ORDER_CHANGED
[C%C*C%C:1.0, C%N*C%N:1.0, C%N*C-N:1.0, C-C*C=C:1.0, C-O*C=O:1.0]

//
FINGERPRINTS RC
[O=C(C=[CH])C=[CH]:1.0, O=C([CH])[CH]:2.0, O=C([NH])[CH2]:2.0, O=C1C(=N[C]N1[CH])[CH2]:1.0, O=C1C(=N[C]N1[CH])[CH2]>>O=C=O:1.0, O=C1[C]=NC2=[C]NC(=CN12)C([CH])=[CH]>>[CH]C([CH])=C1C=NC([NH])=[C]N1:1.0, O=C=O:3.0, O=O:4.0, O=O>>O=C([NH])[CH2]:1.0, O=O>>O=C=O:2.0, O=O>>[C]=O:2.0, O=O>>[C]NC(=O)C[C]:1.0, OC=1C=C[C]=CC1>>O=C1C=C[C]C=C1:1.0, [CH]:2.0, [CH]C(=[CH])O:2.0, [CH]C(=[CH])O>>O=C([CH])[CH]:2.0, [CH]C([NH])=C(C=[CH])C=[CH]:1.0, [CH]C=C(C=[CH])C(=[CH])[NH]:1.0, [CH]C=C(C=[CH])C(=[CH])[NH]>>[CH]C([NH])=C(C=[CH])C=[CH]:1.0, [CH]C=C(O)C=[CH]:1.0, [CH]C=C(O)C=[CH]>>O=C(C=[CH])C=[CH]:2.0, [C]:10.0, [C]=C([CH])[CH]:1.0, [C]=C([CH])[NH]:1.0, [C]=C([N])NC(=O)[CH2]:1.0, [C]=C1N=C(C(=O)N1[CH])CC([CH])=[CH]>>[C]=C([N])NC(=O)CC([CH])=[CH]:1.0, [C]=C1[N][C]C(=N1)[CH2]:1.0, [C]=C1[N][C]C(=N1)[CH2]>>[C]=C([N])NC(=O)[CH2]:1.0, [C]=CN1C(=[C])N=[C]C1=O:1.0, [C]=CN1C(=[C])N=[C]C1=O>>[C]C=NC(=[C])[NH]:1.0, [C]=C[N]:1.0, [C]=C[N]>>[C]C=[N]:1.0, [C]=O:3.0, [C]C(=[CH])[NH]:1.0, [C]C(=[CH])[NH]>>[C]=C([CH])[NH]:1.0, [C]C(=[N])[CH2]:1.0, [C]C(=[N])[CH2]>>O=C([NH])[CH2]:1.0, [C]C([CH])=[CH]:1.0, [C]C([CH])=[CH]>>[C]=C([CH])[CH]:1.0, [C]C([NH])=CN([C])[C]:1.0, [C]C([NH])=CN([C])[C]>>[C]N=CC(=[C])[NH]:1.0, [C]C([N])=O:1.0, [C]C([N])=O>>O=C=O:1.0, [C]C=NC(=[C])[NH]:1.0, [C]C=[N]:1.0, [C]CC1=NC(=C([NH])[CH2])N([CH])C1=O>>[C]CC(=O)NC(N=[CH])=C([NH])[CH2]:1.0, [C]CC1=NC(=[C])N(C=[C])C1=O>>O=C=O:1.0, [C]CC1=N[C][N]C1=O:1.0, [C]CC1=N[C][N]C1=O>>[C]NC(=O)C[C]:1.0, [C]N([C])[CH]:1.0, [C]N([C])[CH]>>[C]N=[CH]:1.0, [C]N1[C]=C([CH2])NC(=C1)C(=C[CH])C=[CH]>>[CH]=CC(C=[CH])=C1C=N[C]=C([CH2])N1:1.0, [C]N=CC(=[C])[NH]:1.0, [C]N=[CH]:1.0, [C]N=[C]:1.0, [C]N=[C]>>[C]N[C]:1.0, [C]NC(=C[N])C([CH])=[CH]:1.0, [C]NC(=C[N])C([CH])=[CH]>>[C]NC(C=[N])=C([CH])[CH]:1.0, [C]NC(=C[N])C=1C=C[C]=CC1>>[C]NC(C=[N])=C1C=C[C]C=C1:1.0, [C]NC(=O)C[C]:1.0, [C]NC(C=[N])=C([CH])[CH]:1.0, [C]N[C]:1.0, [C]O:1.0, [C]O>>[C]=O:1.0, [C]c1cn-2c(=O)c(nc2c([CH2])[nH]1)[CH2]>>[C]NC=1N=CC(=[C])NC1[CH2]:1.0, [NH]:1.0, [N]:3.0, [OH]:1.0, [O]:5.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[C]:5.0, [N]:1.0, [O]:4.0]


ID=Reaction Center at Level: 1 (7)
[O=C([NH])[CH2]:1.0, O=C=O:1.0, O=O:2.0, [C]=O:2.0, [C]C(=[N])[CH2]:1.0, [C]C([N])=O:2.0, [C]N([C])[CH]:1.0]


ID=Reaction Center at Level: 2 (7)
[O=C([NH])[CH2]:1.0, O=C1C(=N[C]N1[CH])[CH2]:2.0, O=C=O:2.0, O=O:2.0, [C]=CN1C(=[C])N=[C]C1=O:1.0, [C]CC1=N[C][N]C1=O:1.0, [C]NC(=O)C[C]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (6)
[[CH]:2.0, [C]:8.0, [NH]:1.0, [N]:3.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (16)
[O=C([CH])[CH]:1.0, O=C([NH])[CH2]:1.0, [CH]C(=[CH])O:1.0, [C]=C([CH])[CH]:1.0, [C]=C([CH])[NH]:1.0, [C]=C[N]:1.0, [C]=O:1.0, [C]C(=[CH])[NH]:1.0, [C]C(=[N])[CH2]:1.0, [C]C([CH])=[CH]:1.0, [C]C=[N]:1.0, [C]N([C])[CH]:1.0, [C]N=[CH]:1.0, [C]N=[C]:1.0, [C]N[C]:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (16)
[O=C(C=[CH])C=[CH]:1.0, O=C([CH])[CH]:1.0, [CH]C(=[CH])O:1.0, [CH]C([NH])=C(C=[CH])C=[CH]:1.0, [CH]C=C(C=[CH])C(=[CH])[NH]:1.0, [CH]C=C(O)C=[CH]:1.0, [C]=C([N])NC(=O)[CH2]:1.0, [C]=C1[N][C]C(=N1)[CH2]:1.0, [C]=CN1C(=[C])N=[C]C1=O:1.0, [C]C([NH])=CN([C])[C]:1.0, [C]C=NC(=[C])[NH]:1.0, [C]CC1=N[C][N]C1=O:1.0, [C]N=CC(=[C])[NH]:1.0, [C]NC(=C[N])C([CH])=[CH]:1.0, [C]NC(=O)C[C]:1.0, [C]NC(C=[N])=C([CH])[CH]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C(=[N])[CH2]>>O=C([NH])[CH2]
2: [C]=C[N]>>[C]C=[N]
3: [C]C([N])=O>>O=C=O
4: [C]O>>[C]=O
5: [C]C(=[CH])[NH]>>[C]=C([CH])[NH]
6: O=O>>[C]=O
7: [C]C([CH])=[CH]>>[C]=C([CH])[CH]
8: [C]N([C])[CH]>>[C]N=[CH]
9: O=O>>[C]=O
10: [CH]C(=[CH])O>>O=C([CH])[CH]
11: [C]N=[C]>>[C]N[C]

MMP Level 2
1: [C]CC1=N[C][N]C1=O>>[C]NC(=O)C[C]
2: [C]C([NH])=CN([C])[C]>>[C]N=CC(=[C])[NH]
3: O=C1C(=N[C]N1[CH])[CH2]>>O=C=O
4: [CH]C(=[CH])O>>O=C([CH])[CH]
5: [C]NC(=C[N])C([CH])=[CH]>>[C]NC(C=[N])=C([CH])[CH]
6: O=O>>O=C=O
7: [CH]C=C(C=[CH])C(=[CH])[NH]>>[CH]C([NH])=C(C=[CH])C=[CH]
8: [C]=CN1C(=[C])N=[C]C1=O>>[C]C=NC(=[C])[NH]
9: O=O>>O=C([NH])[CH2]
10: [CH]C=C(O)C=[CH]>>O=C(C=[CH])C=[CH]
11: [C]=C1[N][C]C(=N1)[CH2]>>[C]=C([N])NC(=O)[CH2]

MMP Level 3
1: [C]=C1N=C(C(=O)N1[CH])CC([CH])=[CH]>>[C]=C([N])NC(=O)CC([CH])=[CH]
2: O=C1[C]=NC2=[C]NC(=CN12)C([CH])=[CH]>>[CH]C([CH])=C1C=NC([NH])=[C]N1
3: [C]CC1=NC(=[C])N(C=[C])C1=O>>O=C=O
4: [CH]C=C(O)C=[CH]>>O=C(C=[CH])C=[CH]
5: [C]N1[C]=C([CH2])NC(=C1)C(=C[CH])C=[CH]>>[CH]=CC(C=[CH])=C1C=N[C]=C([CH2])N1
6: O=O>>O=C=O
7: [C]NC(=C[N])C=1C=C[C]=CC1>>[C]NC(C=[N])=C1C=C[C]C=C1
8: [C]c1cn-2c(=O)c(nc2c([CH2])[nH]1)[CH2]>>[C]NC=1N=CC(=[C])NC1[CH2]
9: O=O>>[C]NC(=O)C[C]
10: OC=1C=C[C]=CC1>>O=C1C=C[C]C=C1
11: [C]CC1=NC(=C([NH])[CH2])N([CH])C1=O>>[C]CC(=O)NC(N=[CH])=C([NH])[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1c(nc-2c([nH]c(cn12)-c3ccc(O)cc3)Cc4ccccc4)Cc5ccc(O)cc5>>O=C1C=CC(C=C1)=C2C=NC(NC(=O)Cc3ccc(O)cc3)=C(N2)Cc4ccccc4, O=c1c(nc-2c([nH]c(cn12)-c3ccc(O)cc3)Cc4ccccc4)Cc5ccc(O)cc5>>O=C1C=CC(C=C1)=C2C=NC(NC(=O)Cc3ccc(O)cc3)=C(N2)Cc4ccccc4, O=c1c(nc-2c([nH]c(cn12)-c3ccc(O)cc3)Cc4ccccc4)Cc5ccc(O)cc5>>O=C1C=CC(C=C1)=C2C=NC(NC(=O)Cc3ccc(O)cc3)=C(N2)Cc4ccccc4, O=c1c(nc-2c([nH]c(cn12)-c3ccc(O)cc3)Cc4ccccc4)Cc5ccc(O)cc5>>O=C1C=CC(C=C1)=C2C=NC(NC(=O)Cc3ccc(O)cc3)=C(N2)Cc4ccccc4, O=c1c(nc-2c([nH]c(cn12)-c3ccc(O)cc3)Cc4ccccc4)Cc5ccc(O)cc5>>O=C1C=CC(C=C1)=C2C=NC(NC(=O)Cc3ccc(O)cc3)=C(N2)Cc4ccccc4, O=c1c(nc-2c([nH]c(cn12)-c3ccc(O)cc3)Cc4ccccc4)Cc5ccc(O)cc5>>O=C1C=CC(C=C1)=C2C=NC(NC(=O)Cc3ccc(O)cc3)=C(N2)Cc4ccccc4, O=c1c(nc-2c([nH]c(cn12)-c3ccc(O)cc3)Cc4ccccc4)Cc5ccc(O)cc5>>O=C1C=CC(C=C1)=C2C=NC(NC(=O)Cc3ccc(O)cc3)=C(N2)Cc4ccccc4, O=c1c(nc-2c([nH]c(cn12)-c3ccc(O)cc3)Cc4ccccc4)Cc5ccc(O)cc5>>O=C1C=CC(C=C1)=C2C=NC(NC(=O)Cc3ccc(O)cc3)=C(N2)Cc4ccccc4]
2: R:M00001, P:M00004	[O=c1c(nc-2c([nH]c(cn12)-c3ccc(O)cc3)Cc4ccccc4)Cc5ccc(O)cc5>>O=C=O]
3: R:M00002, P:M00003	[O=O>>O=C1C=CC(C=C1)=C2C=NC(NC(=O)Cc3ccc(O)cc3)=C(N2)Cc4ccccc4]
4: R:M00002, P:M00004	[O=O>>O=C=O]


//
SELECTED AAM MAPPING
[O:33]=[O:34].[O:1]=[C:2]1[C:3](=[N:4][C:5]2=[C:6]([NH:7][C:8](=[CH:9][N:10]12)[C:11]:3:[CH:12]:[CH:13]:[C:14]([OH:15]):[CH:16]:[CH:17]3)[CH2:18][C:19]:4:[CH:20]:[CH:21]:[CH:22]:[CH:23]:[CH:24]4)[CH2:25][C:26]:5:[CH:27]:[CH:28]:[C:29]([OH:30]):[CH:31]:[CH:32]5>>[O:1]=[C:2]=[O:34].[O:15]=[C:14]1[CH:13]=[CH:12][C:11]([CH:17]=[CH:16]1)=[C:8]2[CH:9]=[N:10][C:5]([NH:4][C:3](=[O:33])[CH2:25][C:26]:3:[CH:27]:[CH:28]:[C:29]([OH:30]):[CH:31]:[CH:32]3)=[C:6]([NH:7]2)[CH2:18][C:19]:4:[CH:20]:[CH:21]:[CH:22]:[CH:23]:[CH:24]4


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=22, 2=21, 3=20, 4=19, 5=24, 6=23, 7=18, 8=6, 9=5, 10=4, 11=3, 12=2, 13=1, 14=10, 15=9, 16=8, 17=7, 18=11, 19=12, 20=13, 21=14, 22=16, 23=17, 24=15, 25=25, 26=26, 27=27, 28=28, 29=29, 30=31, 31=32, 32=30, 33=33, 34=34}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=29, 2=28, 3=27, 4=26, 5=31, 6=30, 7=25, 8=23, 9=11, 10=10, 11=9, 12=8, 13=5, 14=4, 15=3, 16=2, 17=1, 18=7, 19=6, 20=24, 21=12, 22=13, 23=14, 24=15, 25=16, 26=17, 27=18, 28=19, 29=21, 30=22, 31=20, 32=33, 33=32, 34=34}

