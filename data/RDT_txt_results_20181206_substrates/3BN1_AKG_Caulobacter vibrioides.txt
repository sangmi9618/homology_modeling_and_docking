
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:2.0, C=O:2.0]

STEREO_CHANGED
[C(R/S):3.0]

//
FINGERPRINTS RC
[O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(N)C[CH2]:1.0, O=C(O)C(N)C[CH2]>>O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(N)C[CH2]>>[O]C(C)C(N)C([CH])O:1.0, O=C([CH])C(O)C([CH])O:1.0, O=C([CH])C(O)C([CH])O>>[CH]C(O)C(O)C([CH])N:1.0, O=C([CH])[CH]:2.0, O=C([CH])[CH]>>[CH]C([CH])N:1.0, O=C([CH])[CH]>>[C]C(=O)[CH2]:1.0, O=C1C(O)C(O)[CH]OC1C>>OC1[CH]OC(C)C(N)C1O:1.0, [CH]:4.0, [CH]C(O)C(O)C([CH])N:1.0, [CH]C([CH])N:2.0, [CH]C([CH])O:1.0, [CH]N:2.0, [CH]N>>[CH]N:1.0, [C]:2.0, [C]=O:2.0, [C]=O>>[C]=O:1.0, [C]C(=O)[CH2]:2.0, [C]C([CH2])N:2.0, [C]C([CH2])N>>[CH]C([CH])N:1.0, [C]C([CH2])N>>[C]C(=O)[CH2]:1.0, [C]C([CH])O:1.0, [C]C([CH])O>>[CH]C([CH])O:1.0, [C]CCC(N)C(=O)O>>[C]CCC(=O)C(=O)O:1.0, [NH2]:2.0, [O]:2.0, [O]C(C(=O)C([CH])O)C:1.0, [O]C(C(=O)C([CH])O)C>>O=C(O)C(=O)C[CH2]:1.0, [O]C(C(=O)C([CH])O)C>>[O]C(C)C(N)C([CH])O:1.0, [O]C(C)C(N)C([CH])O:1.0, [O]C1OC(C(=O)C(O)C1O)C>>[O]C1OC(C)C(N)C(O)C1O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[CH]:2.0, [C]:2.0, [NH2]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (6)
[O=C([CH])[CH]:1.0, [CH]C([CH])N:1.0, [CH]N:2.0, [C]=O:2.0, [C]C(=O)[CH2]:1.0, [C]C([CH2])N:1.0]


ID=Reaction Center at Level: 2 (8)
[O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(N)C[CH2]:1.0, O=C([CH])[CH]:1.0, [CH]C([CH])N:1.0, [C]C(=O)[CH2]:1.0, [C]C([CH2])N:1.0, [O]C(C(=O)C([CH])O)C:1.0, [O]C(C)C(N)C([CH])O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:4.0, [C]:2.0]


ID=Reaction Center at Level: 1 (6)
[O=C([CH])[CH]:1.0, [CH]C([CH])N:1.0, [CH]C([CH])O:1.0, [C]C(=O)[CH2]:1.0, [C]C([CH2])N:1.0, [C]C([CH])O:1.0]


ID=Reaction Center at Level: 2 (6)
[O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(N)C[CH2]:1.0, O=C([CH])C(O)C([CH])O:1.0, [CH]C(O)C(O)C([CH])N:1.0, [O]C(C(=O)C([CH])O)C:1.0, [O]C(C)C(N)C([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: O=C([CH])[CH]>>[CH]C([CH])N
2: [C]C([CH])O>>[CH]C([CH])O
3: [CH]N>>[CH]N
4: [C]C([CH2])N>>[C]C(=O)[CH2]
5: [C]=O>>[C]=O

MMP Level 2
1: [O]C(C(=O)C([CH])O)C>>[O]C(C)C(N)C([CH])O
2: O=C([CH])C(O)C([CH])O>>[CH]C(O)C(O)C([CH])N
3: [C]C([CH2])N>>[CH]C([CH])N
4: O=C(O)C(N)C[CH2]>>O=C(O)C(=O)C[CH2]
5: O=C([CH])[CH]>>[C]C(=O)[CH2]

MMP Level 3
1: O=C1C(O)C(O)[CH]OC1C>>OC1[CH]OC(C)C(N)C1O
2: [O]C1OC(C(=O)C(O)C1O)C>>[O]C1OC(C)C(N)C(O)C1O
3: O=C(O)C(N)C[CH2]>>[O]C(C)C(N)C([CH])O
4: [C]CCC(N)C(=O)O>>[C]CCC(=O)C(=O)O
5: [O]C(C(=O)C([CH])O)C>>O=C(O)C(=O)C[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OC4OC(C(=O)C(O)C4O)C)C(O)C3O>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OC4OC(C)C(N)C(O)C4O)C(O)C3O, O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OC4OC(C(=O)C(O)C4O)C)C(O)C3O>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OC4OC(C)C(N)C(O)C4O)C(O)C3O]
2: R:M00001, P:M00004	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OC4OC(C(=O)C(O)C4O)C)C(O)C3O>>O=C(O)C(=O)CCC(=O)O]
3: R:M00002, P:M00003	[O=C(O)CCC(N)C(=O)O>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OC4OC(C)C(N)C(O)C4O)C(O)C3O]
4: R:M00002, P:M00004	[O=C(O)CCC(N)C(=O)O>>O=C(O)C(=O)CCC(=O)O]


//
SELECTED AAM MAPPING
[O:39]=[C:40]([OH:41])[CH2:42][CH2:43][CH:44]([NH2:45])[C:46](=[O:47])[OH:48].[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[C:8]1:[N:9]:[CH:10]:[N:11]2[CH:12]3[O:13][CH:14]([CH2:15][O:16][P:17](=[O:18])([OH:19])[O:20][P:21](=[O:22])([OH:23])[O:24][CH:25]4[O:26][CH:27]([C:28](=[O:29])[CH:30]([OH:31])[CH:32]4[OH:33])[CH3:34])[CH:35]([OH:36])[CH:37]3[OH:38]>>[O:47]=[C:46]([OH:48])[C:44](=[O:29])[CH2:43][CH2:42][C:40](=[O:39])[OH:41].[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[C:8]1:[N:9]:[CH:10]:[N:11]2[CH:12]3[O:13][CH:14]([CH2:15][O:16][P:17](=[O:18])([OH:19])[O:20][P:21](=[O:22])([OH:23])[O:24][CH:25]4[O:26][CH:27]([CH3:34])[CH:28]([NH2:45])[CH:30]([OH:31])[CH:32]4[OH:33])[CH:35]([OH:36])[CH:37]3[OH:38]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=34, 2=27, 3=28, 4=29, 5=30, 6=32, 7=25, 8=26, 9=24, 10=21, 11=22, 12=23, 13=20, 14=17, 15=18, 16=19, 17=16, 18=15, 19=14, 20=35, 21=37, 22=12, 23=13, 24=11, 25=10, 26=9, 27=8, 28=7, 29=6, 30=4, 31=3, 32=2, 33=1, 34=5, 35=38, 36=36, 37=33, 38=31, 39=43, 40=42, 41=40, 42=39, 43=41, 44=44, 45=46, 46=47, 47=48, 48=45}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=28, 2=27, 3=29, 4=31, 5=33, 6=25, 7=26, 8=24, 9=21, 10=22, 11=23, 12=20, 13=17, 14=18, 15=19, 16=16, 17=15, 18=14, 19=35, 20=37, 21=12, 22=13, 23=11, 24=10, 25=9, 26=8, 27=7, 28=6, 29=4, 30=3, 31=2, 32=1, 33=5, 34=38, 35=36, 36=34, 37=32, 38=30, 39=44, 40=45, 41=46, 42=47, 43=48, 44=42, 45=43, 46=40, 47=39, 48=41}

