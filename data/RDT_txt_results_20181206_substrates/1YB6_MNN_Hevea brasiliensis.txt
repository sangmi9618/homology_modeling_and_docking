
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[N#C:2.0, N#CC(O)C(=C[CH])C=[CH]>>O=CC(=C[CH])C=[CH]:1.0, N#CC(O)C([CH])=[CH]:1.0, N#CC(O)C([CH])=[CH]>>N#C:1.0, N#CC(O)C([CH])=[CH]>>O=CC([CH])=[CH]:2.0, N#C[CH]:1.0, N#C[CH]>>N#C:1.0, O=CC([CH])=[CH]:1.0, O=[CH]:1.0, [CH]:3.0, [CH]O:1.0, [CH]O>>O=[CH]:1.0, [C]:1.0, [C]C(O)C#N:1.0, [C]C(O)C#N>>N#C:1.0, [C]C([C])O:2.0, [C]C([C])O>>[C]C=O:2.0, [C]C=O:2.0, [OH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[N#C[CH]:1.0, [C]C([C])O:1.0]


ID=Reaction Center at Level: 2 (2)
[N#CC(O)C([CH])=[CH]:1.0, [C]C(O)C#N:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=[CH]:1.0, [CH]O:1.0, [C]C([C])O:1.0, [C]C=O:1.0]


ID=Reaction Center at Level: 2 (4)
[N#CC(O)C([CH])=[CH]:1.0, O=CC([CH])=[CH]:1.0, [C]C([C])O:1.0, [C]C=O:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (2)
[[C]C([C])O:1.0, [C]C=O:1.0]


ID=Reaction Center at Level: 2 (2)
[N#CC(O)C([CH])=[CH]:1.0, O=CC([CH])=[CH]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: N#C[CH]>>N#C
2: [CH]O>>O=[CH]
3: [C]C([C])O>>[C]C=O

MMP Level 2
1: [C]C(O)C#N>>N#C
2: [C]C([C])O>>[C]C=O
3: N#CC(O)C([CH])=[CH]>>O=CC([CH])=[CH]

MMP Level 3
1: N#CC(O)C([CH])=[CH]>>N#C
2: N#CC(O)C([CH])=[CH]>>O=CC([CH])=[CH]
3: N#CC(O)C(=C[CH])C=[CH]>>O=CC(=C[CH])C=[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[N#CC(O)c1ccccc1>>O=Cc1ccccc1, N#CC(O)c1ccccc1>>O=Cc1ccccc1]
2: R:M00001, P:M00003	[N#CC(O)c1ccccc1>>N#C]


//
SELECTED AAM MAPPING
[N:1]#[C:2][CH:3]([OH:4])[C:5]:1:[CH:6]:[CH:7]:[CH:8]:[CH:9]:[CH:10]1>>[N:1]#[CH:2].[O:4]=[CH:3][C:5]:1:[CH:6]:[CH:7]:[CH:8]:[CH:9]:[CH:10]1


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=8, 2=7, 3=6, 4=5, 5=10, 6=9, 7=3, 8=2, 9=1, 10=4}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=5, 3=4, 4=3, 5=8, 6=7, 7=2, 8=1, 9=10, 10=9}

