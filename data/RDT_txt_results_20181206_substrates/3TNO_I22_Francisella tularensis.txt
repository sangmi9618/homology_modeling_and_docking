
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%O:1.0, C-C:2.0]

ORDER_CHANGED
[C-O*C=O:3.0]

STEREO_CHANGED
[C(R/S):4.0]

//
FINGERPRINTS RC
[O=C(CO)C(O)C(O)C([CH])O>>OCC1(O)OC([CH2])C(O)C1O:1.0, O=C(CO)C(O)C([CH])O>>OCC1(O)O[CH][CH]C1O:1.0, O=C(CO)C([CH])O:1.0, O=C(CO)C([CH])O>>O=CC(O)C(O)[CH2]:1.0, O=C(CO)C([CH])O>>[O]C([CH])(O)CO:2.0, O=C([CH2])C(O)C(O)C(O)C(O)[CH2]>>OCC1(O)OC([CH2])C(O)C1O:1.0, O=C([CH2])C(O)C([CH])O:1.0, O=C([CH2])C(O)C([CH])O>>OCC1(O)O[CH][CH]C1O:1.0, O=C([CH])CO:1.0, O=C([CH])CO>>O=CC([CH])O:2.0, O=C([CH])[CH2]:2.0, O=C([CH])[CH2]>>[C]CO:2.0, O=CC(O)C(O)[CH2]:1.0, O=CC(O)[CH2]:1.0, O=CC(O)[CH2]>>O=CC(O)C(O)[CH2]:2.0, O=CC([CH])O:1.0, O=C[CH]:4.0, O=C[CH]>>[CH]C([CH])O:2.0, O=[CH]:2.0, O=[CH]>>[CH]O:1.0, OC1(OC([CH2])[CH][CH]1)[CH2]:1.0, OC1[CH]OC(O)([CH2])C1O:1.0, OC1[C]OC([CH2])C1O:1.0, OCC1(O)O[CH][CH]C1O:1.0, O[CH2]:2.0, O[CH2]>>O=[CH]:1.0, [CH2]:2.0, [CH]:8.0, [CH]C(O)C(O)C(O)[CH2]:1.0, [CH]C(O)C(O)C(O)[CH2]>>OC1[C]OC([CH2])C1O:1.0, [CH]C(O)[CH2]:1.0, [CH]C(O)[CH2]>>OC1(OC([CH2])[CH][CH]1)[CH2]:1.0, [CH]C([CH])O:5.0, [CH]C([CH])O>>[CH]C([CH])O:1.0, [CH]C([CH])O>>[C]C([CH])O:1.0, [CH]O:2.0, [CH]O>>[C]O[CH]:1.0, [C]:2.0, [C]=O:1.0, [C]=O>>O[CH2]:1.0, [C]C(O)C(O)C(O)C(O)C[O]>>[O]CC1OC(O)([CH2])C(O)C1O:1.0, [C]C(O)C(O)C([CH])O:1.0, [C]C(O)C(O)C([CH])O>>OC1[CH]OC(O)([CH2])C1O:1.0, [C]C([CH])O:2.0, [C]C([CH])O>>[O]C([CH])(O)[CH2]:1.0, [C]CO:4.0, [C]CO>>O=C[CH]:2.0, [C]O[CH]:1.0, [OH]:4.0, [O]:4.0, [O]C([CH])(O)CO:1.0, [O]C([CH])(O)[CH2]:1.0, [O]CC(O)C([CH])O>>[O]CC1OC(O)(CO)C(O)C1O:1.0, [O]CC(O)C=O>>[O]CC(O)C(O)C=O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[CH2]:1.0, [CH]:2.0, [C]:2.0, [O]:1.0]


ID=Reaction Center at Level: 1 (6)
[O=C([CH])[CH2]:1.0, O=C[CH]:1.0, [CH]C([CH])O:1.0, [C]CO:1.0, [C]O[CH]:1.0, [O]C([CH])(O)[CH2]:1.0]


ID=Reaction Center at Level: 2 (6)
[O=C(CO)C([CH])O:1.0, O=C([CH])CO:1.0, O=CC(O)C(O)[CH2]:1.0, O=CC([CH])O:1.0, OC1(OC([CH2])[CH][CH]1)[CH2]:1.0, OCC1(O)O[CH][CH]C1O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (5)
[[CH2]:2.0, [CH]:3.0, [C]:1.0, [OH]:3.0, [O]:3.0]


ID=Reaction Center at Level: 1 (8)
[O=C([CH])[CH2]:1.0, O=C[CH]:2.0, O=[CH]:2.0, O[CH2]:2.0, [CH]C([CH])O:1.0, [CH]O:1.0, [C]=O:1.0, [C]CO:2.0]


ID=Reaction Center at Level: 2 (10)
[O=C(CO)C([CH])O:1.0, O=C([CH])CO:1.0, O=C([CH])[CH2]:1.0, O=CC(O)C(O)[CH2]:1.0, O=CC(O)[CH2]:1.0, O=CC([CH])O:1.0, O=C[CH]:2.0, [CH]C([CH])O:1.0, [C]CO:2.0, [O]C([CH])(O)CO:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:7.0, [C]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C[CH]:1.0, [CH]C([CH])O:4.0, [C]C([CH])O:2.0, [O]C([CH])(O)[CH2]:1.0]


ID=Reaction Center at Level: 2 (8)
[O=C([CH2])C(O)C([CH])O:1.0, O=CC(O)C(O)[CH2]:1.0, O=CC(O)[CH2]:1.0, OC1[CH]OC(O)([CH2])C1O:1.0, OC1[C]OC([CH2])C1O:1.0, OCC1(O)O[CH][CH]C1O:1.0, [CH]C(O)C(O)C(O)[CH2]:1.0, [C]C(O)C(O)C([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: O=[CH]>>[CH]O
2: [C]CO>>O=C[CH]
3: O=C([CH])[CH2]>>[C]CO
4: [CH]C([CH])O>>[CH]C([CH])O
5: [CH]C([CH])O>>[C]C([CH])O
6: O=C[CH]>>[CH]C([CH])O
7: [CH]O>>[C]O[CH]
8: [C]C([CH])O>>[O]C([CH])(O)[CH2]
9: [C]=O>>O[CH2]
10: O[CH2]>>O=[CH]

MMP Level 2
1: O=C[CH]>>[CH]C([CH])O
2: O=C([CH])CO>>O=CC([CH])O
3: O=C(CO)C([CH])O>>[O]C([CH])(O)CO
4: [CH]C(O)C(O)C(O)[CH2]>>OC1[C]OC([CH2])C1O
5: [C]C(O)C(O)C([CH])O>>OC1[CH]OC(O)([CH2])C1O
6: O=CC(O)[CH2]>>O=CC(O)C(O)[CH2]
7: [CH]C(O)[CH2]>>OC1(OC([CH2])[CH][CH]1)[CH2]
8: O=C([CH2])C(O)C([CH])O>>OCC1(O)O[CH][CH]C1O
9: O=C([CH])[CH2]>>[C]CO
10: [C]CO>>O=C[CH]

MMP Level 3
1: O=CC(O)[CH2]>>O=CC(O)C(O)[CH2]
2: O=C(CO)C([CH])O>>O=CC(O)C(O)[CH2]
3: O=C(CO)C(O)C([CH])O>>OCC1(O)O[CH][CH]C1O
4: [C]C(O)C(O)C(O)C(O)C[O]>>[O]CC1OC(O)([CH2])C(O)C1O
5: O=C([CH2])C(O)C(O)C(O)C(O)[CH2]>>OCC1(O)OC([CH2])C(O)C1O
6: [O]CC(O)C=O>>[O]CC(O)C(O)C=O
7: [O]CC(O)C([CH])O>>[O]CC1OC(O)(CO)C(O)C1O
8: O=C(CO)C(O)C(O)C([CH])O>>OCC1(O)OC([CH2])C(O)C1O
9: O=C(CO)C([CH])O>>[O]C([CH])(O)CO
10: O=C([CH])CO>>O=CC([CH])O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(CO)C(O)C(O)C(O)C(O)COP(=O)(O)O>>O=CC(O)C(O)COP(=O)(O)O, O=C(CO)C(O)C(O)C(O)C(O)COP(=O)(O)O>>O=CC(O)C(O)COP(=O)(O)O]
2: R:M00001, P:M00004	[O=C(CO)C(O)C(O)C(O)C(O)COP(=O)(O)O>>O=P(O)(O)OCC1OC(O)(CO)C(O)C1O, O=C(CO)C(O)C(O)C(O)C(O)COP(=O)(O)O>>O=P(O)(O)OCC1OC(O)(CO)C(O)C1O, O=C(CO)C(O)C(O)C(O)C(O)COP(=O)(O)O>>O=P(O)(O)OCC1OC(O)(CO)C(O)C1O, O=C(CO)C(O)C(O)C(O)C(O)COP(=O)(O)O>>O=P(O)(O)OCC1OC(O)(CO)C(O)C1O, O=C(CO)C(O)C(O)C(O)C(O)COP(=O)(O)O>>O=P(O)(O)OCC1OC(O)(CO)C(O)C1O, O=C(CO)C(O)C(O)C(O)C(O)COP(=O)(O)O>>O=P(O)(O)OCC1OC(O)(CO)C(O)C1O]
3: R:M00002, P:M00003	[O=CC(O)COP(=O)(O)O>>O=CC(O)C(O)COP(=O)(O)O, O=CC(O)COP(=O)(O)O>>O=CC(O)C(O)COP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:19]=[CH:20][CH:21]([OH:22])[CH2:23][O:24][P:25](=[O:26])([OH:27])[OH:28].[O:1]=[C:2]([CH2:3][OH:4])[CH:5]([OH:6])[CH:7]([OH:8])[CH:9]([OH:10])[CH:11]([OH:12])[CH2:13][O:14][P:15](=[O:16])([OH:17])[OH:18]>>[O:4]=[CH:3][CH:20]([OH:19])[CH:21]([OH:22])[CH2:23][O:24][P:25](=[O:26])([OH:27])[OH:28].[O:16]=[P:15]([OH:18])([OH:17])[O:14][CH2:13][CH:11]1[O:12][C:5]([OH:6])([CH2:2][OH:1])[CH:7]([OH:8])[CH:9]1[OH:10]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=13, 2=11, 3=9, 4=7, 5=5, 6=2, 7=1, 8=3, 9=4, 10=6, 11=8, 12=10, 13=12, 14=14, 15=15, 16=16, 17=17, 18=18, 19=23, 20=21, 21=20, 22=19, 23=22, 24=24, 25=25, 26=26, 27=27, 28=28}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=23, 2=21, 3=19, 4=18, 5=17, 6=20, 7=22, 8=24, 9=25, 10=26, 11=27, 12=28, 13=6, 14=7, 15=15, 16=13, 17=9, 18=8, 19=11, 20=12, 21=10, 22=14, 23=16, 24=5, 25=2, 26=1, 27=3, 28=4}

