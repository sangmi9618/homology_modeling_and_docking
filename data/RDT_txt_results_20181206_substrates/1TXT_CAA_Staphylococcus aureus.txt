
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0, C-O:1.0, C-S:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)CC(O)([CH2])C:1.0, O=C(O)[CH2]:2.0, O=C(SCC[NH])C>>[NH]CCS:1.0, O=C(SC[CH2])C:1.0, O=C(SC[CH2])C>>O=C(O)CC(O)([CH2])C:1.0, O=C(SC[CH2])C>>SC[CH2]:1.0, O=C(S[CH2])C:1.0, O=C(S[CH2])C>>[C]CC(=O)O:1.0, O=C(S[CH2])C>>[C]CC(O)(C)CC(=O)O:1.0, O=C([CH2])C:2.0, O=C([CH2])C>>OC([CH2])([CH2])C:2.0, O=C([S])C:2.0, O=C([S])C>>O=C(O)CC(O)([CH2])C:1.0, O=C([S])C>>O=C(O)[CH2]:1.0, O=C([S])CC(=O)C>>O=C([S])CC(O)(C)CC(=O)O:1.0, O>>O=C(O)[CH2]:1.0, O>>[C]CC(=O)O:1.0, O>>[C]O:1.0, OC([CH2])([CH2])C:2.0, SC[CH2]:1.0, S[CH2]:1.0, [CH2]:1.0, [CH3]:1.0, [C]:4.0, [C]=O:1.0, [C]=O>>[C]O:1.0, [C]C:1.0, [C]C>>[C]C[C]:1.0, [C]CC(=O)C:1.0, [C]CC(=O)C>>[C]CC(O)(C)C[C]:2.0, [C]CC(=O)O:1.0, [C]CC(O)(C)C[C]:1.0, [C]C[C]:1.0, [C]O:2.0, [C]S[CH2]:1.0, [C]S[CH2]>>S[CH2]:1.0, [OH]:2.0, [O]:1.0, [SH]:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[O:1.0, [CH2]:1.0, [C]:3.0, [OH]:1.0, [S]:1.0]


ID=Reaction Center at Level: 1 (7)
[O:1.0, O=C(O)[CH2]:1.0, O=C([S])C:1.0, OC([CH2])([CH2])C:1.0, [C]C[C]:1.0, [C]O:1.0, [C]S[CH2]:1.0]


ID=Reaction Center at Level: 2 (7)
[O:1.0, O=C(O)CC(O)([CH2])C:1.0, O=C(O)[CH2]:1.0, O=C(SC[CH2])C:1.0, O=C(S[CH2])C:1.0, [C]CC(=O)O:1.0, [C]CC(O)(C)C[C]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([CH2])C:1.0, OC([CH2])([CH2])C:1.0, [C]=O:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C([CH2])C:1.0, OC([CH2])([CH2])C:1.0, [C]CC(=O)C:1.0, [C]CC(O)(C)C[C]:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[C]:2.0]


ID=Reaction Center at Level: 1 (2)
[O=C([CH2])C:1.0, OC([CH2])([CH2])C:1.0]


ID=Reaction Center at Level: 2 (2)
[[C]CC(=O)C:1.0, [C]CC(O)(C)C[C]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: O=C([S])C>>O=C(O)[CH2]
2: [C]C>>[C]C[C]
3: [C]=O>>[C]O
4: O>>[C]O
5: O=C([CH2])C>>OC([CH2])([CH2])C
6: [C]S[CH2]>>S[CH2]

MMP Level 2
1: O=C(S[CH2])C>>[C]CC(=O)O
2: O=C([S])C>>O=C(O)CC(O)([CH2])C
3: O=C([CH2])C>>OC([CH2])([CH2])C
4: O>>O=C(O)[CH2]
5: [C]CC(=O)C>>[C]CC(O)(C)C[C]
6: O=C(SC[CH2])C>>SC[CH2]

MMP Level 3
1: O=C(SC[CH2])C>>O=C(O)CC(O)([CH2])C
2: O=C(S[CH2])C>>[C]CC(O)(C)CC(=O)O
3: [C]CC(=O)C>>[C]CC(O)(C)C[C]
4: O>>[C]CC(=O)O
5: O=C([S])CC(=O)C>>O=C([S])CC(O)(C)CC(=O)O
6: O=C(SCC[NH])C>>[NH]CCS


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00005	[O>>O=C(O)CC(O)(C)CC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O]
2: R:M00002, P:M00004	[O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C>>O=C(NCCS)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O]
3: R:M00002, P:M00005	[O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C>>O=C(O)CC(O)(C)CC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O, O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C>>O=C(O)CC(O)(C)CC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O]
4: R:M00003, P:M00005	[O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)CC(=O)C>>O=C(O)CC(O)(C)CC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O, O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)CC(=O)C>>O=C(O)CC(O)(C)CC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:55]=[C:56]([S:57][CH2:58][CH2:59][NH:60][C:61](=[O:62])[CH2:63][CH2:64][NH:65][C:66](=[O:67])[CH:68]([OH:69])[C:70]([CH3:71])([CH3:72])[CH2:73][O:74][P:75](=[O:76])([OH:77])[O:78][P:79](=[O:80])([OH:81])[O:82][CH2:83][CH:84]1[O:85][CH:86]([N:87]:2:[CH:88]:[N:89]:[C:90]:3:[C:91](:[N:92]:[CH:93]:[N:94]:[C:95]32)[NH2:96])[CH:97]([OH:98])[CH:99]1[O:100][P:101](=[O:102])([OH:103])[OH:104])[CH3:105].[O:1]=[C:2]([S:3][CH2:4][CH2:5][NH:6][C:7](=[O:8])[CH2:9][CH2:10][NH:11][C:12](=[O:13])[CH:14]([OH:15])[C:16]([CH3:17])([CH3:18])[CH2:19][O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[O:28][CH2:29][CH:30]1[O:31][CH:32]([N:33]:2:[CH:34]:[N:35]:[C:36]:3:[C:37](:[N:38]:[CH:39]:[N:40]:[C:41]32)[NH2:42])[CH:43]([OH:44])[CH:45]1[O:46][P:47](=[O:48])([OH:49])[OH:50])[CH2:51][C:52](=[O:53])[CH3:54].[OH2:106]>>[O:55]=[C:56]([OH:106])[CH2:105][C:52]([OH:53])([CH3:54])[CH2:51][C:2](=[O:1])[S:3][CH2:4][CH2:5][NH:6][C:7](=[O:8])[CH2:9][CH2:10][NH:11][C:12](=[O:13])[CH:14]([OH:15])[C:16]([CH3:17])([CH3:18])[CH2:19][O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[O:28][CH2:29][CH:30]1[O:31][CH:32]([N:33]:2:[CH:34]:[N:35]:[C:36]:3:[C:37](:[N:38]:[CH:39]:[N:40]:[C:41]32)[NH2:42])[CH:43]([OH:44])[CH:45]1[O:46][P:47](=[O:48])([OH:50])[OH:49].[O:62]=[C:61]([NH:60][CH2:59][CH2:58][SH:57])[CH2:63][CH2:64][NH:65][C:66](=[O:67])[CH:68]([OH:69])[C:70]([CH3:71])([CH3:72])[CH2:73][O:74][P:75](=[O:76])([OH:77])[O:78][P:79](=[O:80])([OH:81])[O:82][CH2:83][CH:84]1[O:85][CH:86]([N:87]:2:[CH:88]:[N:89]:[C:90]:3:[C:91](:[N:92]:[CH:93]:[N:94]:[C:95]32)[NH2:96])[CH:97]([OH:98])[CH:99]1[O:100][P:101](=[O:102])([OH:103])[OH:104]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=106, 2=105, 3=56, 4=55, 5=57, 6=58, 7=59, 8=60, 9=61, 10=62, 11=63, 12=64, 13=65, 14=66, 15=67, 16=68, 17=70, 18=71, 19=72, 20=73, 21=74, 22=75, 23=76, 24=77, 25=78, 26=79, 27=80, 28=81, 29=82, 30=83, 31=84, 32=99, 33=97, 34=86, 35=85, 36=87, 37=88, 38=89, 39=90, 40=95, 41=94, 42=93, 43=92, 44=91, 45=96, 46=98, 47=100, 48=101, 49=102, 50=103, 51=104, 52=69, 53=54, 54=52, 55=53, 56=51, 57=2, 58=1, 59=3, 60=4, 61=5, 62=6, 63=7, 64=8, 65=9, 66=10, 67=11, 68=12, 69=13, 70=14, 71=16, 72=17, 73=18, 74=19, 75=20, 76=21, 77=22, 78=23, 79=24, 80=25, 81=26, 82=27, 83=28, 84=29, 85=30, 86=45, 87=43, 88=32, 89=31, 90=33, 91=34, 92=35, 93=36, 94=41, 95=40, 96=39, 97=38, 98=37, 99=42, 100=44, 101=46, 102=47, 103=48, 104=49, 105=50, 106=15}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=73, 2=72, 3=74, 4=75, 5=76, 6=77, 7=78, 8=79, 9=80, 10=81, 11=82, 12=83, 13=84, 14=85, 15=86, 16=101, 17=99, 18=88, 19=87, 20=89, 21=90, 22=91, 23=92, 24=97, 25=96, 26=95, 27=94, 28=93, 29=98, 30=100, 31=102, 32=103, 33=104, 34=105, 35=106, 36=70, 37=68, 38=69, 39=67, 40=66, 41=65, 42=60, 43=59, 44=61, 45=62, 46=63, 47=64, 48=71, 49=7, 50=5, 51=4, 52=2, 53=1, 54=3, 55=8, 56=9, 57=10, 58=11, 59=12, 60=13, 61=14, 62=15, 63=16, 64=17, 65=18, 66=19, 67=20, 68=21, 69=22, 70=24, 71=25, 72=26, 73=27, 74=28, 75=29, 76=30, 77=31, 78=32, 79=33, 80=34, 81=35, 82=36, 83=37, 84=38, 85=53, 86=51, 87=40, 88=39, 89=41, 90=42, 91=43, 92=44, 93=49, 94=48, 95=47, 96=46, 97=45, 98=50, 99=52, 100=54, 101=55, 102=56, 103=57, 104=58, 105=23, 106=6}

