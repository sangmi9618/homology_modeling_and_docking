
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O=P(O)(O[P])OC1O[CH][CH]C1O>>O=P(O)(O)O[P]:1.0, [CH]:2.0, [C]C(=[CH])C(N)=C[CH]>>[C]C(=[CH])C(=C[CH])NC1O[CH][CH]C1O:1.0, [C]C(=[CH])N:1.0, [C]C(=[CH])N>>[C]C(=[CH])NC([O])[CH]:1.0, [C]C(=[CH])NC([O])[CH]:1.0, [C]N:1.0, [C]N>>[C]N[CH]:1.0, [C]NC1O[CH][CH]C1O:1.0, [C]N[CH]:1.0, [NH2]:1.0, [NH]:1.0, [OH]:1.0, [O]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]C([CH])[NH]:1.0, [O]C([O])[CH]:1.0, [O]C([O])[CH]>>[O]C([CH])[NH]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OC1OC([CH2])C(O)C1O>>[C]C(=[CH])NC1OC([CH2])C(O)C1O:1.0, [P]O:1.0, [P]OC1O[CH][CH]C1O:1.0, [P]OC1O[CH][CH]C1O>>[C]NC1O[CH][CH]C1O:1.0, [P]O[CH]:1.0, [P]O[CH]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [NH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[C]N[CH]:1.0, [O]C([CH])[NH]:1.0, [O]C([O])[CH]:1.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]C(=[CH])NC([O])[CH]:1.0, [C]NC1O[CH][CH]C1O:1.0, [O]C([CH])OP([O])(=O)O:1.0, [P]OC1O[CH][CH]C1O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[CH]>>[P]O
2: [O]C([O])[CH]>>[O]C([CH])[NH]
3: [C]N>>[C]N[CH]

MMP Level 2
1: [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O
2: [P]OC1O[CH][CH]C1O>>[C]NC1O[CH][CH]C1O
3: [C]C(=[CH])N>>[C]C(=[CH])NC([O])[CH]

MMP Level 3
1: O=P(O)(O[P])OC1O[CH][CH]C1O>>O=P(O)(O)O[P]
2: [O]P(=O)(O)OC1OC([CH2])C(O)C1O>>[C]C(=[CH])NC1OC([CH2])C(O)C1O
3: [C]C(=[CH])C(N)=C[CH]>>[C]C(=[CH])C(=C[CH])NC1O[CH][CH]C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)c1ccccc1N>>O=C(O)c1ccccc1NC2OC(COP(=O)(O)O)C(O)C2O]
2: R:M00002, P:M00003	[O=P(O)(O)OCC1OC(OP(=O)(O)OP(=O)(O)O)C(O)C1O>>O=C(O)c1ccccc1NC2OC(COP(=O)(O)O)C(O)C2O]
3: R:M00002, P:M00004	[O=P(O)(O)OCC1OC(OP(=O)(O)OP(=O)(O)O)C(O)C1O>>O=P(O)(O)OP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:23]=[C:24]([OH:25])[C:26]:1:[CH:27]:[CH:28]:[CH:29]:[CH:30]:[C:31]1[NH2:32].[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH2:6][CH:7]1[O:8][CH:9]([O:10][P:11](=[O:12])([OH:13])[O:14][P:15](=[O:16])([OH:17])[OH:18])[CH:19]([OH:20])[CH:21]1[OH:22]>>[O:23]=[C:24]([OH:25])[C:26]:1:[CH:27]:[CH:28]:[CH:29]:[CH:30]:[C:31]1[NH:32][CH:9]2[O:8][CH:7]([CH2:6][O:5][P:2](=[O:1])([OH:3])[OH:4])[CH:21]([OH:22])[CH:19]2[OH:20].[O:12]=[P:11]([OH:10])([OH:13])[O:14][P:15](=[O:16])([OH:17])[OH:18]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=28, 2=29, 3=30, 4=31, 5=26, 6=27, 7=24, 8=23, 9=25, 10=32, 11=6, 12=7, 13=21, 14=19, 15=9, 16=8, 17=10, 18=11, 19=12, 20=13, 21=14, 22=15, 23=16, 24=17, 25=18, 26=20, 27=22, 28=5, 29=2, 30=1, 31=3, 32=4}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=8, 4=9, 5=4, 6=5, 7=2, 8=1, 9=3, 10=10, 11=11, 12=22, 13=20, 14=13, 15=12, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=21, 23=23, 24=26, 25=25, 26=24, 27=27, 28=28, 29=29, 30=30, 31=31, 32=32}

