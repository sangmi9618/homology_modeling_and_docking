
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-S:1.0]

//
FINGERPRINTS RC
[S([CH2])[CH2]:1.0, [CH2][S+]([CH2])C:2.0, [CH2][S+]([CH2])C>>S([CH2])[CH2]:1.0, [CH2][S+]([CH2])C>>[CH][N+](=[CH])C:1.0, [CH3]:2.0, [CH]CSC[CH2]:1.0, [CH]C[S+](C)C[CH2]:1.0, [CH]C[S+](C)C[CH2]>>[CH]CSC[CH2]:1.0, [CH]C[S+](C)C[CH2]>>[C]=C[N+](=C[CH])C:1.0, [CH]N=[CH]:1.0, [CH]N=[CH]>>[CH][N+](=[CH])C:1.0, [CH][N+](=[CH])C:2.0, [C]=CN=C[CH]:1.0, [C]=CN=C[CH]>>[C]=C[N+](=C[CH])C:1.0, [C]=C[N+](=C[CH])C:1.0, [C]c1cnccc1>>[C]c1ccc[n+](c1)C:1.0, [N+]:1.0, [N+]C:1.0, [N]:1.0, [O]C([CH])C[S+](C)CC[CH]>>[O]C([CH])CSCC[CH]:1.0, [S+]:1.0, [S+]C:1.0, [S+]C>>[N+]C:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH3]:2.0, [N+]:1.0, [S+]:1.0]


ID=Reaction Center at Level: 1 (4)
[[CH2][S+]([CH2])C:1.0, [CH][N+](=[CH])C:1.0, [N+]C:1.0, [S+]C:1.0]


ID=Reaction Center at Level: 2 (4)
[[CH2][S+]([CH2])C:1.0, [CH]C[S+](C)C[CH2]:1.0, [CH][N+](=[CH])C:1.0, [C]=C[N+](=C[CH])C:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH]N=[CH]>>[CH][N+](=[CH])C
2: [S+]C>>[N+]C
3: [CH2][S+]([CH2])C>>S([CH2])[CH2]

MMP Level 2
1: [C]=CN=C[CH]>>[C]=C[N+](=C[CH])C
2: [CH2][S+]([CH2])C>>[CH][N+](=[CH])C
3: [CH]C[S+](C)C[CH2]>>[CH]CSC[CH2]

MMP Level 3
1: [C]c1cnccc1>>[C]c1ccc[n+](c1)C
2: [CH]C[S+](C)C[CH2]>>[C]=C[N+](=C[CH])C
3: [O]C([CH])C[S+](C)CC[CH]>>[O]C([CH])CSCC[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(O)C(N)CCSCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(N)c1ccc[n+](c1)C]
3: R:M00002, P:M00004	[O=C(N)c1cnccc1>>O=C(N)c1ccc[n+](c1)C]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][S+:8]([CH3:9])[CH2:10][CH:11]1[O:12][CH:13]([N:14]:2:[CH:15]:[N:16]:[C:17]:3:[C:18](:[N:19]:[CH:20]:[N:21]:[C:22]32)[NH2:23])[CH:24]([OH:25])[CH:26]1[OH:27].[O:28]=[C:29]([NH2:30])[C:31]:1:[CH:32]:[N:33]:[CH:34]:[CH:35]:[CH:36]1>>[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][S:8][CH2:10][CH:11]1[O:12][CH:13]([N:14]:2:[CH:15]:[N:16]:[C:17]:3:[C:18](:[N:19]:[CH:20]:[N:21]:[C:22]32)[NH2:23])[CH:24]([OH:25])[CH:26]1[OH:27].[O:28]=[C:29]([NH2:30])[C:31]:1:[CH:36]:[CH:35]:[CH:34]:[N+:33](:[CH:32]1)[CH3:9]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=4, 6=2, 7=1, 8=3, 9=5, 10=10, 11=11, 12=26, 13=24, 14=13, 15=12, 16=14, 17=15, 18=16, 19=17, 20=22, 21=21, 22=20, 23=19, 24=18, 25=23, 26=25, 27=27, 28=35, 29=36, 30=31, 31=32, 32=33, 33=34, 34=29, 35=28, 36=30}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=19, 2=20, 3=21, 4=16, 5=17, 6=18, 7=22, 8=15, 9=14, 10=13, 11=12, 12=23, 13=25, 14=10, 15=11, 16=9, 17=8, 18=7, 19=6, 20=4, 21=2, 22=1, 23=3, 24=5, 25=26, 26=24, 27=36, 28=34, 29=33, 30=32, 31=31, 32=30, 33=35, 34=28, 35=27, 36=29}

