
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:2.0, C=O:1.0, C@C:1.0, C@N:1.0]

ORDER_CHANGED
[C-C*C@C:2.0, C=N*C@N:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(CO)COP(=O)(O)O>>[C]C1=[C]C=CC=N1:1.0, O=C(CO)CO[P]>>[C]C1=[C]N=CC=C1:1.0, O=C(O)C(=N)CC(=O)O>>O=C(O)c1ncccc1C(=O)O:2.0, O=C([CH2])CO:1.0, O=C([CH2])CO>>O:1.0, O=C([CH2])CO>>[C]C(=[C])C=C[CH]:1.0, O=C([CH2])COP(=O)(O)O>>O=P(O)(O)O:1.0, O=C([CH2])CO[P]:1.0, O=C([CH2])CO[P]>>[C]N=CC=[CH]:1.0, O=C([CH2])[CH2]:2.0, O=C([CH2])[CH2]>>[CH]C=[CH]:1.0, O=P(O)(O)O:1.0, O[CH2]:1.0, O[CH2]>>O:1.0, [CH2]:3.0, [CH]:3.0, [CH]C=[CH]:1.0, [C]:4.0, [C]=N:1.0, [C]=N>>[C]N=[CH]:1.0, [C]=O:1.0, [C]C(=N)CC(=O)O:1.0, [C]C(=N)CC(=O)O>>[C]C([N])=C(C=[CH])C(=O)O:1.0, [C]C(=N)[CH2]:2.0, [C]C(=N)[CH2]>>[C]C(=[C])N=C[CH]:1.0, [C]C(=N)[CH2]>>[C]C(=[C])[N]:1.0, [C]C(=[C])C=C[CH]:1.0, [C]C(=[C])N=C[CH]:1.0, [C]C(=[C])[CH]:1.0, [C]C(=[C])[N]:1.0, [C]C([CH])=C(N=[CH])C(=O)O:1.0, [C]C([N])=C(C=[CH])C(=O)O:1.0, [C]C=CC=[N]:1.0, [C]C=[CH]:1.0, [C]CC(=N)C(=O)O:1.0, [C]CC(=N)C(=O)O>>[C]C([CH])=C(N=[CH])C(=O)O:1.0, [C]CC(=N)C(=O)O>>[C]c1cccnc1C(=O)O:1.0, [C]CO:2.0, [C]CO>>O:1.0, [C]CO>>[C]C=[CH]:1.0, [C]COP(=O)(O)O:1.0, [C]COP(=O)(O)O>>O=P(O)(O)O:1.0, [C]C[C]:1.0, [C]C[C]>>[C]C(=[C])[CH]:1.0, [C]C[O]:1.0, [C]C[O]>>[N]=C[CH]:1.0, [C]N=CC=[CH]:1.0, [C]N=[CH]:1.0, [NH]:1.0, [N]:1.0, [N]=C[CH]:1.0, [OH]:2.0, [O]:2.0, [O]CC(=O)CO:1.0, [O]CC(=O)CO>>[C]C=CC=[N]:1.0, [O]CC(=O)CO>>[C]c1ncccc1C(=O)O:1.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[CH2]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (7)
[O:1.0, [CH2]:2.0, [CH]:2.0, [C]:2.0, [N]:1.0, [OH]:1.0, [O]:2.0]


ID=Reaction Center at Level: 1 (11)
[O:1.0, O=C([CH2])[CH2]:1.0, O[CH2]:1.0, [C]=O:1.0, [C]C(=[C])[CH]:1.0, [C]C=[CH]:1.0, [C]CO:1.0, [C]C[O]:1.0, [C]N=[CH]:1.0, [N]=C[CH]:1.0, [P]O[CH2]:1.0]


ID=Reaction Center at Level: 2 (11)
[O:1.0, O=C([CH2])CO:1.0, O=C([CH2])CO[P]:1.0, O=C([CH2])[CH2]:1.0, [C]C(=[C])C=C[CH]:1.0, [C]C(=[C])N=C[CH]:1.0, [C]C([N])=C(C=[CH])C(=O)O:1.0, [C]CO:1.0, [C]COP(=O)(O)O:1.0, [C]N=CC=[CH]:1.0, [O]CC(=O)CO:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (5)
[[CH2]:2.0, [CH]:2.0, [C]:4.0, [NH]:1.0, [N]:1.0]


ID=Reaction Center at Level: 1 (10)
[O=C([CH2])[CH2]:1.0, [CH]C=[CH]:1.0, [C]=N:1.0, [C]C(=N)[CH2]:1.0, [C]C(=[C])[CH]:1.0, [C]C(=[C])[N]:1.0, [C]C=[CH]:1.0, [C]CO:1.0, [C]C[C]:1.0, [C]N=[CH]:1.0]


ID=Reaction Center at Level: 2 (10)
[O=C([CH2])CO:1.0, [C]C(=N)CC(=O)O:1.0, [C]C(=N)[CH2]:1.0, [C]C(=[C])C=C[CH]:1.0, [C]C(=[C])N=C[CH]:1.0, [C]C([CH])=C(N=[CH])C(=O)O:1.0, [C]C([N])=C(C=[CH])C(=O)O:1.0, [C]C=CC=[N]:1.0, [C]CC(=N)C(=O)O:1.0, [O]CC(=O)CO:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[CH2]>>[P]O
2: O[CH2]>>O
3: [C]C[O]>>[N]=C[CH]
4: [C]C(=N)[CH2]>>[C]C(=[C])[N]
5: [C]=N>>[C]N=[CH]
6: O=C([CH2])[CH2]>>[CH]C=[CH]
7: [C]CO>>[C]C=[CH]
8: [C]C[C]>>[C]C(=[C])[CH]

MMP Level 2
1: [C]COP(=O)(O)O>>O=P(O)(O)O
2: [C]CO>>O
3: O=C([CH2])CO[P]>>[C]N=CC=[CH]
4: [C]CC(=N)C(=O)O>>[C]C([CH])=C(N=[CH])C(=O)O
5: [C]C(=N)[CH2]>>[C]C(=[C])N=C[CH]
6: [O]CC(=O)CO>>[C]C=CC=[N]
7: O=C([CH2])CO>>[C]C(=[C])C=C[CH]
8: [C]C(=N)CC(=O)O>>[C]C([N])=C(C=[CH])C(=O)O

MMP Level 3
1: O=C([CH2])COP(=O)(O)O>>O=P(O)(O)O
2: O=C([CH2])CO>>O
3: O=C(CO)COP(=O)(O)O>>[C]C1=[C]C=CC=N1
4: O=C(O)C(=N)CC(=O)O>>O=C(O)c1ncccc1C(=O)O
5: [C]CC(=N)C(=O)O>>[C]c1cccnc1C(=O)O
6: O=C(CO)CO[P]>>[C]C1=[C]N=CC=C1
7: [O]CC(=O)CO>>[C]c1ncccc1C(=O)O
8: O=C(O)C(=N)CC(=O)O>>O=C(O)c1ncccc1C(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(CO)COP(=O)(O)O>>O=C(O)c1ncccc1C(=O)O, O=C(CO)COP(=O)(O)O>>O=C(O)c1ncccc1C(=O)O, O=C(CO)COP(=O)(O)O>>O=C(O)c1ncccc1C(=O)O]
2: R:M00001, P:M00004	[O=C(CO)COP(=O)(O)O>>O]
3: R:M00001, P:M00005	[O=C(CO)COP(=O)(O)O>>O=P(O)(O)O]
4: R:M00002, P:M00003	[O=C(O)C(=N)CC(=O)O>>O=C(O)c1ncccc1C(=O)O, O=C(O)C(=N)CC(=O)O>>O=C(O)c1ncccc1C(=O)O, O=C(O)C(=N)CC(=O)O>>O=C(O)c1ncccc1C(=O)O]


//
SELECTED AAM MAPPING
[O:10]=[C:11]([OH:12])[C:13](=[NH:14])[CH2:15][C:16](=[O:17])[OH:18].[O:19]=[C:1]([CH2:2][OH:3])[CH2:4][O:5][P:6](=[O:7])([OH:8])[OH:9]>>[O:10]=[C:11]([OH:12])[C:13]:1:[N:14]:[CH:4]:[CH:1]:[CH:2]:[C:15]1[C:16](=[O:17])[OH:18].[O:7]=[P:6]([OH:5])([OH:8])[OH:9].[OH2:3]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=3, 2=2, 3=1, 4=5, 5=6, 6=7, 7=8, 8=9, 9=10, 10=4, 11=16, 12=14, 13=15, 14=12, 15=11, 16=13, 17=17, 18=18, 19=19}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=7, 2=8, 3=9, 4=4, 5=5, 6=6, 7=2, 8=1, 9=3, 10=10, 11=11, 12=12, 13=18, 14=15, 15=14, 16=13, 17=16, 18=17}

