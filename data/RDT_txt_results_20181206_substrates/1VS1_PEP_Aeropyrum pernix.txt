
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0, O-P:2.0]

ORDER_CHANGED
[C-C*C=C:1.0, C-O*C=O:2.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)C(=O)C[CH]:1.0, O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(=O)CC([CH])O:1.0, O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(=O)C[CH]:1.0, O=C(O)C(O[P])=C:1.0, O=C(O)C(O[P])=C>>O=C(O)C(=O)CC(O)C([CH])O:1.0, O=C(O)C(O[P])=C>>O=C(O)C(=O)C[CH]:1.0, O=CC(O)C(O)[CH2]:1.0, O=CC(O)C(O)[CH2]>>OC([CH2])C(O)C(O)[CH2]:1.0, O=CC(O)C(O)[CH2]>>[C]C(=O)CC(O)C(O)C(O)[CH2]:1.0, O=CC([CH])O:1.0, O=CC([CH])O>>[C]CC(O)C([CH])O:2.0, O=C[CH]:2.0, O=C[CH]>>[CH]C(O)[CH2]:2.0, O=P(O)(O)O:3.0, O=[CH]:1.0, O=[CH]>>[CH]O:1.0, O>>O=P(O)(O)O:2.0, O>>[P]O:1.0, OC([CH2])C(O)C(O)[CH2]:1.0, [CH2]:2.0, [CH]:4.0, [CH]C(O)[CH2]:2.0, [CH]C([CH])O:2.0, [CH]C([CH])O>>[CH]C([CH])O:1.0, [CH]O:1.0, [C]:2.0, [C]=C:1.0, [C]=C>>[C]C[CH]:1.0, [C]=O:1.0, [C]C(=O)CC([CH])O:1.0, [C]C(=O)[CH2]:2.0, [C]C(OP(=O)(O)O)=C:1.0, [C]C(OP(=O)(O)O)=C>>O=P(O)(O)O:1.0, [C]C(OP(=O)(O)O)=C>>[C]C(=O)[CH2]:1.0, [C]C([O])=C:2.0, [C]C([O])=C>>[C]C(=O)CC([CH])O:1.0, [C]C([O])=C>>[C]C(=O)[CH2]:1.0, [C]CC(O)C([CH])O:1.0, [C]C[CH]:1.0, [C]OP(=O)(O)O:1.0, [C]OP(=O)(O)O>>O=P(O)(O)O:1.0, [C]O[P]:1.0, [C]O[P]>>[C]=O:1.0, [OH]:2.0, [O]:3.0, [O]CC(O)C(O)C=O>>[C]CC(O)C(O)C(O)C[O]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [P]:2.0, [P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (6)
[O:1.0, [CH2]:1.0, [CH]:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (7)
[O:1.0, O=P(O)(O)O:1.0, [CH]C(O)[CH2]:1.0, [C]C[CH]:1.0, [C]O[P]:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0]


ID=Reaction Center at Level: 2 (6)
[O:1.0, O=P(O)(O)O:2.0, [C]C(=O)CC([CH])O:1.0, [C]C(OP(=O)(O)O)=C:1.0, [C]CC(O)C([CH])O:1.0, [C]OP(=O)(O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (5)
[[CH2]:2.0, [CH]:2.0, [C]:2.0, [OH]:1.0, [O]:3.0]


ID=Reaction Center at Level: 1 (10)
[O=C[CH]:1.0, O=[CH]:1.0, [CH]C(O)[CH2]:1.0, [CH]O:1.0, [C]=C:1.0, [C]=O:1.0, [C]C(=O)[CH2]:1.0, [C]C([O])=C:1.0, [C]C[CH]:1.0, [C]O[P]:1.0]


ID=Reaction Center at Level: 2 (10)
[O=C(O)C(=O)C[CH]:1.0, O=C(O)C(O[P])=C:1.0, O=CC([CH])O:1.0, O=C[CH]:1.0, [CH]C(O)[CH2]:1.0, [C]C(=O)CC([CH])O:1.0, [C]C(=O)[CH2]:1.0, [C]C(OP(=O)(O)O)=C:1.0, [C]C([O])=C:1.0, [C]CC(O)C([CH])O:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:4.0]


ID=Reaction Center at Level: 1 (3)
[O=C[CH]:1.0, [CH]C(O)[CH2]:1.0, [CH]C([CH])O:2.0]


ID=Reaction Center at Level: 2 (4)
[O=CC(O)C(O)[CH2]:1.0, O=CC([CH])O:1.0, OC([CH2])C(O)C(O)[CH2]:1.0, [C]CC(O)C([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: O=C[CH]>>[CH]C(O)[CH2]
2: [C]=C>>[C]C[CH]
3: [C]O[P]>>[C]=O
4: O=[CH]>>[CH]O
5: [C]C([O])=C>>[C]C(=O)[CH2]
6: [CH]C([CH])O>>[CH]C([CH])O
7: [O]P(=O)(O)O>>O=P(O)(O)O
8: O>>[P]O

MMP Level 2
1: O=CC([CH])O>>[C]CC(O)C([CH])O
2: [C]C([O])=C>>[C]C(=O)CC([CH])O
3: [C]C(OP(=O)(O)O)=C>>[C]C(=O)[CH2]
4: O=C[CH]>>[CH]C(O)[CH2]
5: O=C(O)C(O[P])=C>>O=C(O)C(=O)C[CH]
6: O=CC(O)C(O)[CH2]>>OC([CH2])C(O)C(O)[CH2]
7: [C]OP(=O)(O)O>>O=P(O)(O)O
8: O>>O=P(O)(O)O

MMP Level 3
1: O=CC(O)C(O)[CH2]>>[C]C(=O)CC(O)C(O)C(O)[CH2]
2: O=C(O)C(O[P])=C>>O=C(O)C(=O)CC(O)C([CH])O
3: O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(=O)C[CH]
4: O=CC([CH])O>>[C]CC(O)C([CH])O
5: O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(=O)CC([CH])O
6: [O]CC(O)C(O)C=O>>[C]CC(O)C(O)C(O)C[O]
7: [C]C(OP(=O)(O)O)=C>>O=P(O)(O)O
8: O>>O=P(O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(=O)CC(O)C(O)C(O)COP(=O)(O)O, O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(=O)CC(O)C(O)C(O)COP(=O)(O)O, O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(=O)CC(O)C(O)C(O)COP(=O)(O)O]
2: R:M00001, P:M00005	[O=C(O)C(OP(=O)(O)O)=C>>O=P(O)(O)O]
3: R:M00002, P:M00004	[O=CC(O)C(O)COP(=O)(O)O>>O=C(O)C(=O)CC(O)C(O)C(O)COP(=O)(O)O, O=CC(O)C(O)COP(=O)(O)O>>O=C(O)C(=O)CC(O)C(O)C(O)COP(=O)(O)O, O=CC(O)C(O)COP(=O)(O)O>>O=C(O)C(=O)CC(O)C(O)C(O)COP(=O)(O)O]
4: R:M00003, P:M00005	[O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[CH:2][CH:3]([OH:4])[CH:5]([OH:6])[CH2:7][O:8][P:9](=[O:10])([OH:11])[OH:12].[O:13]=[C:14]([OH:15])[C:16]([O:17][P:18](=[O:19])([OH:20])[OH:21])=[CH2:22].[OH2:23]>>[O:13]=[C:14]([OH:15])[C:16](=[O:17])[CH2:22][CH:2]([OH:1])[CH:3]([OH:4])[CH:5]([OH:6])[CH2:7][O:8][P:9](=[O:10])([OH:11])[OH:12].[O:19]=[P:18]([OH:20])([OH:21])[OH:23]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=22, 2=16, 3=14, 4=13, 5=15, 6=17, 7=18, 8=19, 9=20, 10=21, 11=7, 12=5, 13=3, 14=2, 15=1, 16=4, 17=6, 18=8, 19=9, 20=10, 21=11, 22=12, 23=23}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=9, 4=11, 5=13, 6=14, 7=15, 8=16, 9=17, 10=18, 11=12, 12=10, 13=8, 14=4, 15=5, 16=2, 17=1, 18=3, 19=21, 20=20, 21=19, 22=22, 23=23}

