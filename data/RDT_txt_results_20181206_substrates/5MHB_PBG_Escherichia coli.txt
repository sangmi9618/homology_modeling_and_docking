
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:2.0, C-N:2.0, C=O:2.0, C@C:1.0]

ORDER_CHANGED
[C-C*C@C:2.0]

//
FINGERPRINTS RC
[O:3.0, O=C(CN)C([NH])[CH2]:1.0, O=C(CN)C([NH])[CH2]>>O=C(O)Cc1c([CH2])c[nH]c1[CH2]:1.0, O=C(CN)C([NH])[CH2]>>[C]Cc1c([CH2])c[nH]c1[CH2]:1.0, O=C(C[NH])C[CH2]:1.0, O=C(C[NH])C[CH2]>>O:1.0, O=C(C[NH])C[CH2]>>[C]=1NC=C(C1[CH2])C[CH2]:1.0, O=C(O)CC(N[CH2])C(=O)[CH2]>>[C]1=CNC(=C1[CH2])CN:1.0, O=C(O)C[CH]:1.0, O=C(O)C[CH]>>[C]CC(=O)O:1.0, O=C(O)[CH2]:2.0, O=C(O)[CH2]>>O=C(O)[CH2]:1.0, O=C([CH2])CN[CH]:1.0, O=C([CH2])CN[CH]>>[C]1=[C]C([CH2])=CN1:1.0, O=C([CH2])[CH2]:2.0, O=C([CH2])[CH2]>>O:1.0, O=C([CH2])[CH2]>>[C]C(=[CH])[CH2]:1.0, O=C([CH])CN:1.0, O=C([CH])CN>>[C]=C([NH])CN:1.0, O=C([CH])CN>>[C]C(=[C])CC(=O)O:1.0, O=C([CH])[CH2]:2.0, O=C([CH])[CH2]>>[C]C(=[C])[CH2]:1.0, [CH2]:5.0, [CH2]N:2.0, [CH2]N>>[CH2]N:1.0, [CH]:2.0, [C]:7.0, [C]1=CNC(=C1[CH2])CN:1.0, [C]1=[C]C([CH2])=CN1:1.0, [C]=1NC=C(C1[CH2])C[CH2]:1.0, [C]=C([NH])CN:1.0, [C]=C([NH])[CH2]:1.0, [C]=C[NH]:1.0, [C]=O:2.0, [C]=O>>O:1.0, [C]C(=[CH])[CH2]:1.0, [C]C(=[C])CC(=O)O:1.0, [C]C(=[C])[CH2]:1.0, [C]C([CH2])NCC(=O)C[CH2]>>[CH2]c1[nH]cc(c1[CH2])C[CH2]:1.0, [C]C([NH])CC(=O)O:1.0, [C]C([NH])CC(=O)O>>[C]=C([NH])CN:1.0, [C]C([NH])CC(=O)O>>[C]C(=[C])CC(=O)O:1.0, [C]C([NH])[CH2]:1.0, [C]C([NH])[CH2]>>[C]=C([NH])[CH2]:1.0, [C]CC(=O)O:1.0, [C]CC(N[CH2])C(=O)CN>>O=C(O)Cc1c(c[nH]c1CN)C[CH2]:1.0, [C]CC(N[CH2])C(=O)[CH2]:1.0, [C]CC(N[CH2])C(=O)[CH2]>>[C]1=CNC(=C1[CH2])CN:1.0, [C]CCC(=O)CN[CH]>>[C]Cc1c([CH2])[nH]cc1CC[C]:1.0, [C]CN:4.0, [C]CN>>[C]CN:1.0, [C]CN>>[C]C[C]:1.0, [C]CNC(C(=O)CN)CC(=O)O>>[C]Cc1c([CH2])c[nH]c1CN:1.0, [C]C[CH]:1.0, [C]C[CH]>>[C]CN:1.0, [C]C[C]:1.0, [C]C[NH]:1.0, [C]C[NH]>>[C]=C[NH]:1.0, [C]Cc1c([CH2])c[nH]c1[CH2]:1.0, [NH2]:2.0, [O]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[O:1.0, [CH2]:4.0, [C]:6.0, [NH2]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (11)
[O:1.0, O=C(O)[CH2]:2.0, O=C([CH2])[CH2]:1.0, O=C([CH])[CH2]:1.0, [CH2]N:2.0, [C]=O:2.0, [C]C(=[CH])[CH2]:1.0, [C]C(=[C])[CH2]:1.0, [C]CN:2.0, [C]C[CH]:1.0, [C]C[C]:1.0]


ID=Reaction Center at Level: 2 (14)
[O:1.0, O=C(CN)C([NH])[CH2]:1.0, O=C(C[NH])C[CH2]:1.0, O=C(O)C[CH]:1.0, O=C([CH2])[CH2]:1.0, O=C([CH])CN:1.0, O=C([CH])[CH2]:1.0, [C]=1NC=C(C1[CH2])C[CH2]:1.0, [C]=C([NH])CN:1.0, [C]C(=[C])CC(=O)O:1.0, [C]C([NH])CC(=O)O:1.0, [C]CC(=O)O:1.0, [C]CN:2.0, [C]Cc1c([CH2])c[nH]c1[CH2]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[CH2]:1.0, [CH]:2.0, [C]:5.0]


ID=Reaction Center at Level: 1 (8)
[O=C([CH2])[CH2]:1.0, O=C([CH])[CH2]:1.0, [C]=C([NH])[CH2]:1.0, [C]=C[NH]:1.0, [C]C(=[CH])[CH2]:1.0, [C]C(=[C])[CH2]:1.0, [C]C([NH])[CH2]:1.0, [C]C[NH]:1.0]


ID=Reaction Center at Level: 2 (8)
[O=C(CN)C([NH])[CH2]:1.0, O=C(C[NH])C[CH2]:1.0, O=C([CH2])CN[CH]:1.0, [C]1=CNC(=C1[CH2])CN:1.0, [C]1=[C]C([CH2])=CN1:1.0, [C]=1NC=C(C1[CH2])C[CH2]:1.0, [C]CC(N[CH2])C(=O)[CH2]:1.0, [C]Cc1c([CH2])c[nH]c1[CH2]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=C([CH2])[CH2]>>[C]C(=[CH])[CH2]
2: O=C([CH])[CH2]>>[C]C(=[C])[CH2]
3: [C]C[CH]>>[C]CN
4: [C]C[NH]>>[C]=C[NH]
5: O=C(O)[CH2]>>O=C(O)[CH2]
6: [C]=O>>O
7: [C]C([NH])[CH2]>>[C]=C([NH])[CH2]
8: [C]CN>>[C]C[C]
9: [CH2]N>>[CH2]N

MMP Level 2
1: O=C(C[NH])C[CH2]>>[C]=1NC=C(C1[CH2])C[CH2]
2: O=C(CN)C([NH])[CH2]>>[C]Cc1c([CH2])c[nH]c1[CH2]
3: [C]C([NH])CC(=O)O>>[C]=C([NH])CN
4: O=C([CH2])CN[CH]>>[C]1=[C]C([CH2])=CN1
5: O=C(O)C[CH]>>[C]CC(=O)O
6: O=C([CH2])[CH2]>>O
7: [C]CC(N[CH2])C(=O)[CH2]>>[C]1=CNC(=C1[CH2])CN
8: O=C([CH])CN>>[C]C(=[C])CC(=O)O
9: [C]CN>>[C]CN

MMP Level 3
1: [C]CCC(=O)CN[CH]>>[C]Cc1c([CH2])[nH]cc1CC[C]
2: [C]CC(N[CH2])C(=O)CN>>O=C(O)Cc1c(c[nH]c1CN)C[CH2]
3: O=C(O)CC(N[CH2])C(=O)[CH2]>>[C]1=CNC(=C1[CH2])CN
4: [C]C([CH2])NCC(=O)C[CH2]>>[CH2]c1[nH]cc(c1[CH2])C[CH2]
5: [C]C([NH])CC(=O)O>>[C]C(=[C])CC(=O)O
6: O=C(C[NH])C[CH2]>>O
7: [C]CNC(C(=O)CN)CC(=O)O>>[C]Cc1c([CH2])c[nH]c1CN
8: O=C(CN)C([NH])[CH2]>>O=C(O)Cc1c([CH2])c[nH]c1[CH2]
9: O=C([CH])CN>>[C]=C([NH])CN


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)CCC(=O)CNC(C(=O)CN)CC(=O)O>>O=C(O)Cc1c(c[nH]c1CN)CCC(=O)O, O=C(O)CCC(=O)CNC(C(=O)CN)CC(=O)O>>O=C(O)Cc1c(c[nH]c1CN)CCC(=O)O, O=C(O)CCC(=O)CNC(C(=O)CN)CC(=O)O>>O=C(O)Cc1c(c[nH]c1CN)CCC(=O)O, O=C(O)CCC(=O)CNC(C(=O)CN)CC(=O)O>>O=C(O)Cc1c(c[nH]c1CN)CCC(=O)O, O=C(O)CCC(=O)CNC(C(=O)CN)CC(=O)O>>O=C(O)Cc1c(c[nH]c1CN)CCC(=O)O, O=C(O)CCC(=O)CNC(C(=O)CN)CC(=O)O>>O=C(O)Cc1c(c[nH]c1CN)CCC(=O)O, O=C(O)CCC(=O)CNC(C(=O)CN)CC(=O)O>>O=C(O)Cc1c(c[nH]c1CN)CCC(=O)O, O=C(O)CCC(=O)CNC(C(=O)CN)CC(=O)O>>O=C(O)Cc1c(c[nH]c1CN)CCC(=O)O]
2: R:M00001, P:M00003	[O=C(O)CCC(=O)CNC(C(=O)CN)CC(=O)O>>O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH2:4][CH2:5][C:6](=[O:7])[CH2:8][NH:9][CH:10]([C:11](=[O:18])[CH2:12][NH2:13])[CH2:14][C:15](=[O:16])[OH:17]>>[O:16]=[C:15]([OH:17])[CH2:12][C:11]:1:[C:6](:[CH:8]:[NH:9]:[C:10]1[CH2:14][NH2:13])[CH2:5][CH2:4][C:2](=[O:1])[OH:3].[OH2:7]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=5, 2=4, 3=2, 4=1, 5=3, 6=6, 7=7, 8=8, 9=9, 10=10, 11=15, 12=16, 13=17, 14=18, 15=11, 16=12, 17=13, 18=14}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=7, 2=6, 3=5, 4=9, 5=8, 6=10, 7=11, 8=4, 9=2, 10=1, 11=3, 12=12, 13=13, 14=14, 15=15, 16=16, 17=17}

