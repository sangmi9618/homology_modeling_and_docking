
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(N[CH2])C([CH])N:1.0, O=C(N[CH2])C([CH])N>>O=C(O)C([CH])N:1.0, O=C(O)C([CH])N:1.0, O=C(O)CNC(=O)C([CH])N>>O=C(O)CN:1.0, O=C([CH])O:2.0, O=C([CH])[NH]:1.0, O=C([CH])[NH]>>O=C([CH])O:1.0, O>>O=C(O)C([CH])N:1.0, O>>O=C([CH])O:1.0, O>>[C]O:1.0, [CH2]N:1.0, [C]:2.0, [C]CN:1.0, [C]CNC(=O)C(N)C(C)C>>O=C(O)C(N)C(C)C:1.0, [C]CNC(=O)[CH]:1.0, [C]CNC(=O)[CH]>>[C]CN:1.0, [C]N[CH2]:1.0, [C]N[CH2]>>[CH2]N:1.0, [C]O:1.0, [NH2]:1.0, [NH]:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [C]:2.0, [NH]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=C([CH])O:1.0, O=C([CH])[NH]:1.0, [C]N[CH2]:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=C(N[CH2])C([CH])N:1.0, O=C(O)C([CH])N:1.0, O=C([CH])O:1.0, [C]CNC(=O)[CH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O>>[C]O
2: [C]N[CH2]>>[CH2]N
3: O=C([CH])[NH]>>O=C([CH])O

MMP Level 2
1: O>>O=C([CH])O
2: [C]CNC(=O)[CH]>>[C]CN
3: O=C(N[CH2])C([CH])N>>O=C(O)C([CH])N

MMP Level 3
1: O>>O=C(O)C([CH])N
2: O=C(O)CNC(=O)C([CH])N>>O=C(O)CN
3: [C]CNC(=O)C(N)C(C)C>>O=C(O)C(N)C(C)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)CNC(=O)C(N)C(C)C>>O=C(O)C(N)C(C)C]
2: R:M00001, P:M00004	[O=C(O)CNC(=O)C(N)C(C)C>>O=C(O)CN]
3: R:M00002, P:M00003	[O>>O=C(O)C(N)C(C)C]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH2:4][NH:5][C:6](=[O:7])[CH:8]([NH2:9])[CH:10]([CH3:11])[CH3:12].[OH2:13]>>[O:1]=[C:2]([OH:3])[CH2:4][NH2:5].[O:7]=[C:6]([OH:13])[CH:8]([NH2:9])[CH:10]([CH3:12])[CH3:11]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=11, 2=10, 3=12, 4=8, 5=6, 6=7, 7=5, 8=4, 9=2, 10=1, 11=3, 12=9, 13=13}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=7, 2=6, 3=8, 4=4, 5=2, 6=1, 7=3, 8=5, 9=12, 10=10, 11=9, 12=11, 13=13}

