
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)OC[CH]:1.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O[CH2]:1.0, O[CH2]:1.0, O[CH2]>>[P]O[CH2]:1.0, [CH]CO:1.0, [CH]CO>>O=P(O)(O)OC[CH]:1.0, [OH]:2.0, [O]:2.0, [O]C([CH])CO>>[O]C([CH])COP(=O)(O)O:1.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC[CH]:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[O]P(=O)(O)O:2.0, [P]O[CH2]:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)OC[CH]:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[P]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O[CH2]>>[P]O[CH2]
2: [P]O[P]>>[P]O
3: [O]P(=O)(O)O>>[O]P(=O)(O)O

MMP Level 2
1: [CH]CO>>O=P(O)(O)OC[CH]
2: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
3: O=P(O)(O)O[P]>>O=P(O)(O)O[CH2]

MMP Level 3
1: [O]C([CH])CO>>[O]C([CH])COP(=O)(O)O
2: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
3: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
3: R:M00002, P:M00004	[OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31].[OH:32][CH2:33][CH:34]1[O:35][CH:36]([N:37]:2:[CH:38]:[N:39]:[C:40]:3:[C:41](:[N:42]:[CH:43]:[N:44]:[C:45]32)[NH2:46])[CH:47]([OH:48])[CH:49]1[OH:50]>>[O:1]=[P:2]([OH:3])([OH:4])[O:32][CH2:33][CH:34]1[O:35][CH:36]([N:37]:2:[CH:38]:[N:39]:[C:40]:3:[C:41](:[N:42]:[CH:43]:[N:44]:[C:45]32)[NH2:46])[CH:47]([OH:48])[CH:49]1[OH:50].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=43, 33=44, 34=45, 35=40, 36=41, 37=42, 38=46, 39=39, 40=38, 41=37, 42=36, 43=47, 44=49, 45=34, 46=35, 47=33, 48=32, 49=50, 50=48}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=17, 5=18, 6=19, 7=23, 8=16, 9=15, 10=14, 11=13, 12=24, 13=26, 14=11, 15=12, 16=10, 17=9, 18=6, 19=7, 20=8, 21=5, 22=2, 23=1, 24=3, 25=4, 26=27, 27=25, 28=43, 29=44, 30=45, 31=40, 32=41, 33=42, 34=46, 35=39, 36=38, 37=37, 38=36, 39=47, 40=49, 41=34, 42=35, 43=33, 44=32, 45=29, 46=28, 47=30, 48=31, 49=50, 50=48}

