
//
FINGERPRINTS BC

ORDER_CHANGED
[C-O*C=O:1.0, O-O*O=O:1.0]

//
FINGERPRINTS RC
[O=O:4.0, O=O>>OO:6.0, O=[CH]:1.0, OO:4.0, O[CH2]:1.0, O[CH2]>>O=[CH]:1.0, [CH2]:1.0, [CH]:1.0, [C]C(=[C])C=O:1.0, [C]C(=[C])CO:1.0, [C]C(=[C])CO>>[C]C(=[C])C=O:2.0, [C]C(O)=C(C(=[CH])[CH2])CO>>[C]C(O)=C(C=O)C(=[CH])[CH2]:1.0, [C]C=O:2.0, [C]CO:2.0, [C]CO>>[C]C=O:2.0, [OH]:3.0, [O]:3.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH2]:1.0, [CH]:1.0, [OH]:3.0, [O]:3.0]


ID=Reaction Center at Level: 1 (6)
[O=O:2.0, O=[CH]:1.0, OO:2.0, O[CH2]:1.0, [C]C=O:1.0, [C]CO:1.0]


ID=Reaction Center at Level: 2 (6)
[O=O:2.0, OO:2.0, [C]C(=[C])C=O:1.0, [C]C(=[C])CO:1.0, [C]C=O:1.0, [C]CO:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=O>>OO
2: [C]CO>>[C]C=O
3: O[CH2]>>O=[CH]

MMP Level 2
1: O=O>>OO
2: [C]C(=[C])CO>>[C]C(=[C])C=O
3: [C]CO>>[C]C=O

MMP Level 3
1: O=O>>OO
2: [C]C(O)=C(C(=[CH])[CH2])CO>>[C]C(O)=C(C=O)C(=[CH])[CH2]
3: [C]C(=[C])CO>>[C]C(=[C])C=O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OCc1cnc(c(O)c1CO)C>>O=Cc1c(O)c(ncc1COP(=O)(O)O)C, O=P(O)(O)OCc1cnc(c(O)c1CO)C>>O=Cc1c(O)c(ncc1COP(=O)(O)O)C]
2: R:M00002, P:M00004	[O=O>>OO]


//
SELECTED AAM MAPPING
[O:17]=[O:18].[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH2:6][C:7]:1:[CH:8]:[N:9]:[C:10](:[C:11]([OH:12]):[C:13]1[CH2:14][OH:15])[CH3:16]>>[O:15]=[CH:14][C:13]:1:[C:11]([OH:12]):[C:10](:[N:9]:[CH:8]:[C:7]1[CH2:6][O:5][P:2](=[O:1])([OH:3])[OH:4])[CH3:16].[OH:17][OH:18]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=16, 2=10, 3=9, 4=8, 5=7, 6=13, 7=11, 8=12, 9=14, 10=15, 11=6, 12=5, 13=2, 14=1, 15=3, 16=4, 17=17, 18=18}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=16, 2=6, 3=7, 4=8, 5=9, 6=3, 7=4, 8=5, 9=2, 10=1, 11=10, 12=11, 13=12, 14=13, 15=14, 16=15, 17=17, 18=18}

