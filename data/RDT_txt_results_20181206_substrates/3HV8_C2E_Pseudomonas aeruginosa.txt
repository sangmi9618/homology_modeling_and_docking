
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%O:1.0, C-O:1.0]

ORDER_CHANGED
[O-P*O=P:2.0]

//
FINGERPRINTS RC
[O:3.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(OC[CH])OC([CH])[CH]>>O=P(O)(O)OC[CH]:1.0, O=P(O)(O[CH2])OC1C(O)[CH]OC1[CH2]>>O=P(O)(O)O[CH2]:1.0, O=P(O)(O[CH])O[CH2]:1.0, O=P(O)(O[CH])O[CH2]>>O=P(O)(O)O[CH2]:3.0, O=[P]:2.0, O=[P]>>[P]O:1.0, O>>OC1[CH]OC([CH2])C1O:1.0, O>>[CH]C([CH])O:1.0, O>>[CH]O:1.0, OC1[CH]OC([CH2])C1O:1.0, [CH]:2.0, [CH]C([CH])O:2.0, [CH]O:1.0, [N]C1OC(C[O])C(OP([O])(=O)O)C1O>>[N]C1OC(C[O])C(O)C1O:1.0, [OH]:4.0, [O]:3.0, [O]C([CH])[CH]:1.0, [O]C([CH])[CH]>>[CH]C([CH])O:1.0, [O]P(=O)(O)O:4.0, [O]P(=O)(O)OC([CH])[CH]:1.0, [O]P(=O)(O)OC([CH])[CH]>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:3.0, [O]P([O])(=O)O>>[O]P(=O)(O)O:3.0, [P]:2.0, [P]O:3.0, [P]O>>O=[P]:1.0, [P]OC1C(O)[CH]OC1[CH2]:1.0, [P]OC1C(O)[CH]OC1[CH2]>>OC1[CH]OC([CH2])C1O:1.0, [P]O[CH]:1.0, [P]O[CH]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [CH]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, [CH]C([CH])O:1.0, [CH]O:1.0, [O]C([CH])[CH]:1.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, OC1[CH]OC([CH2])C1O:1.0, [CH]C([CH])O:1.0, [O]P(=O)(O)OC([CH])[CH]:1.0, [P]OC1C(O)[CH]OC1[CH2]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[OH]:2.0, [O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (4)
[O=[P]:2.0, [O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [P]O:2.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)O[CH2]:1.0, O=P(O)(O[CH])O[CH2]:1.0, [O]P(=O)(O)O:2.0, [O]P([O])(=O)O:2.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O>>O=[P]
2: [O]P([O])(=O)O>>[O]P(=O)(O)O
3: O=[P]>>[P]O
4: O>>[CH]O
5: [O]C([CH])[CH]>>[CH]C([CH])O
6: [P]O[CH]>>[P]O

MMP Level 2
1: [O]P([O])(=O)O>>[O]P(=O)(O)O
2: O=P(O)(O[CH])O[CH2]>>O=P(O)(O)O[CH2]
3: [O]P([O])(=O)O>>[O]P(=O)(O)O
4: O>>[CH]C([CH])O
5: [P]OC1C(O)[CH]OC1[CH2]>>OC1[CH]OC([CH2])C1O
6: [O]P(=O)(O)OC([CH])[CH]>>[O]P(=O)(O)O

MMP Level 3
1: O=P(O)(O[CH])O[CH2]>>O=P(O)(O)O[CH2]
2: O=P(O)(OC[CH])OC([CH])[CH]>>O=P(O)(O)OC[CH]
3: O=P(O)(O[CH])O[CH2]>>O=P(O)(O)O[CH2]
4: O>>OC1[CH]OC([CH2])C1O
5: [N]C1OC(C[O])C(OP([O])(=O)O)C1O>>[N]C1OC(C[O])C(O)C1O
6: O=P(O)(O[CH2])OC1C(O)[CH]OC1[CH2]>>O=P(O)(O)O[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1nc(N)[nH]c2c1ncn2C3OC4COP(=O)(O)OC5C(O)C(OC5COP(=O)(O)OC4C3O)n6cnc7c(=O)nc(N)[nH]c76>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OC4C(O)C(OC4COP(=O)(O)O)n5cnc6c(=O)nc(N)[nH]c65)C(O)C3O, O=c1nc(N)[nH]c2c1ncn2C3OC4COP(=O)(O)OC5C(O)C(OC5COP(=O)(O)OC4C3O)n6cnc7c(=O)nc(N)[nH]c76>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OC4C(O)C(OC4COP(=O)(O)O)n5cnc6c(=O)nc(N)[nH]c65)C(O)C3O, O=c1nc(N)[nH]c2c1ncn2C3OC4COP(=O)(O)OC5C(O)C(OC5COP(=O)(O)OC4C3O)n6cnc7c(=O)nc(N)[nH]c76>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OC4C(O)C(OC4COP(=O)(O)O)n5cnc6c(=O)nc(N)[nH]c65)C(O)C3O, O=c1nc(N)[nH]c2c1ncn2C3OC4COP(=O)(O)OC5C(O)C(OC5COP(=O)(O)OC4C3O)n6cnc7c(=O)nc(N)[nH]c76>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OC4C(O)C(OC4COP(=O)(O)O)n5cnc6c(=O)nc(N)[nH]c65)C(O)C3O, O=c1nc(N)[nH]c2c1ncn2C3OC4COP(=O)(O)OC5C(O)C(OC5COP(=O)(O)OC4C3O)n6cnc7c(=O)nc(N)[nH]c76>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OC4C(O)C(OC4COP(=O)(O)O)n5cnc6c(=O)nc(N)[nH]c65)C(O)C3O]
2: R:M00002, P:M00003	[O>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OC4C(O)C(OC4COP(=O)(O)O)n5cnc6c(=O)nc(N)[nH]c65)C(O)C3O]


//
SELECTED AAM MAPPING
[O:41]=[C:40]1[N:42]=[C:43]([NH2:44])[NH:45][C:46]:2:[C:39]1:[N:38]:[CH:37]:[N:36]2[CH:24]3[O:25][CH:26]4[CH2:27][O:28][P:29](=[O:30])([OH:31])[O:32][CH:33]5[CH:34]([OH:35])[CH:12]([O:13][CH:14]5[CH2:15][O:16][P:17](=[O:18])([OH:19])[O:20][CH:21]4[CH:22]3[OH:23])[N:11]:6:[CH:10]:[N:9]:[C:8]:7[C:2](=[O:1])[N:3]=[C:4]([NH2:5])[NH:6][C:7]76.[OH2:47]>>[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[C:8]1:[N:9]:[CH:10]:[N:11]2[CH:12]3[O:13][CH:14]([CH2:15][O:16][P:17](=[O:18])([OH:19])[O:20][CH:21]4[CH:22]([OH:23])[CH:24]([O:25][CH:26]4[CH2:27][O:28][P:29](=[O:31])([OH:32])[OH:30])[N:36]:5:[CH:37]:[N:38]:[C:39]:6[C:40](=[O:41])[N:42]=[C:43]([NH2:44])[NH:45][C:46]65)[CH:33]([OH:47])[CH:34]3[OH:35]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=27, 2=26, 3=21, 4=22, 5=24, 6=25, 7=36, 8=37, 9=38, 10=39, 11=46, 12=45, 13=43, 14=42, 15=40, 16=41, 17=44, 18=23, 19=20, 20=17, 21=18, 22=16, 23=15, 24=14, 25=33, 26=34, 27=12, 28=13, 29=11, 30=10, 31=9, 32=8, 33=7, 34=6, 35=4, 36=3, 37=2, 38=1, 39=5, 40=35, 41=32, 42=29, 43=30, 44=28, 45=31, 46=19, 47=47}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=10, 2=9, 3=8, 4=7, 5=11, 6=12, 7=46, 8=44, 9=14, 10=13, 11=15, 12=16, 13=17, 14=18, 15=19, 16=20, 17=21, 18=26, 19=25, 20=24, 21=22, 22=23, 23=33, 24=34, 25=35, 26=36, 27=43, 28=42, 29=40, 30=39, 31=37, 32=38, 33=41, 34=27, 35=28, 36=29, 37=30, 38=31, 39=32, 40=45, 41=47, 42=6, 43=4, 44=3, 45=2, 46=1, 47=5}

