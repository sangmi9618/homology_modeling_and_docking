
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(N)C=1C=[N+]C=CC1>>O=C(N)C1=C[N]C=CC1:1.0, O=C([CH])O:2.0, O=CC=C([CH])C>>O=C(O)C=C([CH])C:1.0, O=C[CH]:1.0, O=C[CH]>>O=C([CH])O:1.0, O>>O=C([CH])O:1.0, O>>[C]=CC(=O)O:1.0, O>>[C]O:1.0, [CH2]:1.0, [CH]:2.0, [CH]N([CH])[CH]:1.0, [CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]>>[CH]N([CH])[CH]:1.0, [C]:1.0, [C]=CC(=O)O:1.0, [C]=CC=O:1.0, [C]=CC=O>>[C]=CC(=O)O:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN(C=[CH])C([O])[CH]:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])C=C[CH]>>[C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C=[CH]:1.0, [C]C=[CH]>>[C]C[CH]:1.0, [C]C[CH]:1.0, [C]O:1.0, [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]C1=CN(C=CC1)C2O[CH][CH]C2O:1.0, [N+]:1.0, [N]:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[O:1.0, [C]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (3)
[O:1.0, O=C([CH])O:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (3)
[O:1.0, O=C([CH])O:1.0, [C]=CC(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O>>[C]O
2: [C]C=[CH]>>[C]C[CH]
3: [CH][N+]([CH])=[CH]>>[CH]N([CH])[CH]
4: O=C[CH]>>O=C([CH])O

MMP Level 2
1: O>>O=C([CH])O
2: [C]C(=[CH])C=C[CH]>>[C]C(=[CH])CC=[CH]
3: [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN(C=[CH])C([O])[CH]
4: [C]=CC=O>>[C]=CC(=O)O

MMP Level 3
1: O>>[C]=CC(=O)O
2: O=C(N)C=1C=[N+]C=CC1>>O=C(N)C1=C[N]C=CC1
3: [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]C1=CN(C=CC1)C2O[CH][CH]C2O
4: O=CC=C([CH])C>>O=C(O)C=C([CH])C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=CC=C(C=CC=C(C=CC1=C(C)CCCC1(C)C)C)C>>O=C(O)C=C(C=CC=C(C=CC1=C(C)CCCC1(C)C)C)C]
2: R:M00002, P:M00005	[O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O, O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O]
3: R:M00003, P:M00004	[O>>O=C(O)C=C(C=CC=C(C=CC1=C(C)CCCC1(C)C)C)C]


//
SELECTED AAM MAPPING
[O:45]=[CH:46][CH:47]=[C:48]([CH:49]=[CH:50][CH:51]=[C:52]([CH:53]=[CH:54][C:55]1=[C:56]([CH3:57])[CH2:58][CH2:59][CH2:60][C:61]1([CH3:62])[CH3:63])[CH3:64])[CH3:65].[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:5]:[CH:6]:[CH:7]:[N+:8](:[CH:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44].[OH2:66]>>[H+:67].[O:45]=[C:46]([OH:66])[CH:47]=[C:48]([CH:49]=[CH:50][CH:51]=[C:52]([CH:53]=[CH:54][C:55]1=[C:56]([CH3:57])[CH2:58][CH2:59][CH2:60][C:61]1([CH3:62])[CH3:63])[CH3:64])[CH3:65].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:9][N:8]([CH:7]=[CH:6][CH2:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=57, 2=56, 3=55, 4=61, 5=60, 6=59, 7=58, 8=62, 9=63, 10=54, 11=53, 12=52, 13=51, 14=50, 15=49, 16=48, 17=47, 18=46, 19=45, 20=65, 21=64, 22=6, 23=5, 24=4, 25=9, 26=8, 27=7, 28=10, 29=43, 30=41, 31=12, 32=11, 33=13, 34=14, 35=15, 36=16, 37=17, 38=18, 39=19, 40=20, 41=21, 42=22, 43=23, 44=24, 45=39, 46=37, 47=26, 48=25, 49=27, 50=28, 51=29, 52=30, 53=35, 54=34, 55=33, 56=32, 57=31, 58=36, 59=38, 60=40, 61=42, 62=44, 63=2, 64=1, 65=3, 66=66}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=58, 2=57, 3=56, 4=62, 5=61, 6=60, 7=59, 8=63, 9=64, 10=55, 11=54, 12=53, 13=52, 14=51, 15=50, 16=49, 17=48, 18=46, 19=45, 20=47, 21=66, 22=65, 23=9, 24=8, 25=7, 26=6, 27=5, 28=4, 29=2, 30=1, 31=3, 32=10, 33=43, 34=41, 35=12, 36=11, 37=13, 38=14, 39=15, 40=16, 41=17, 42=18, 43=19, 44=20, 45=21, 46=22, 47=23, 48=24, 49=39, 50=37, 51=26, 52=25, 53=27, 54=28, 55=29, 56=30, 57=35, 58=34, 59=33, 60=32, 61=31, 62=36, 63=38, 64=40, 65=42, 66=44, 67=67}

