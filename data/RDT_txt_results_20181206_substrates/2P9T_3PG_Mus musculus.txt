
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=C(O)C(O)[CH2]>>O=C(OP(=O)(O)O)C(O)[CH2]:1.0, O=C([CH])O:1.0, O=C([CH])O>>O=C([CH])OP(=O)(O)O:1.0, O=C([CH])OP(=O)(O)O:1.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>[C]OP(=O)(O)O:1.0, [C]O:1.0, [C]O>>[C]O[P]:1.0, [C]OP(=O)(O)O:1.0, [C]O[P]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=C([CH])OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[C]O[P]:1.0, [O]P(=O)(O)O:2.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C([CH])OP(=O)(O)O:1.0, O=P(O)(O)O[P]:1.0, [C]OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]P(=O)(O)O>>[O]P(=O)(O)O
2: [P]O[P]>>[P]O
3: [C]O>>[C]O[P]

MMP Level 2
1: O=P(O)(O)O[P]>>[C]OP(=O)(O)O
2: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
3: O=C([CH])O>>O=C([CH])OP(=O)(O)O

MMP Level 3
1: [O]P(=O)(O)OP(=O)(O)O>>O=C([CH])OP(=O)(O)O
2: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
3: O=C(O)C(O)[CH2]>>O=C(OP(=O)(O)O)C(O)[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(O)COP(=O)(O)O>>O=C(OP(=O)(O)O)C(O)COP(=O)(O)O]
2: R:M00002, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(OP(=O)(O)O)C(O)COP(=O)(O)O]
3: R:M00002, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]


//
SELECTED AAM MAPPING
[O:32]=[C:33]([OH:34])[CH:35]([OH:36])[CH2:37][O:38][P:39](=[O:40])([OH:41])[OH:42].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:32]=[C:33]([O:34][P:2](=[O:1])([OH:3])[OH:4])[CH:35]([OH:36])[CH2:37][O:38][P:39](=[O:40])([OH:41])[OH:42].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=37, 2=35, 3=33, 4=32, 5=34, 6=36, 7=38, 8=39, 9=40, 10=41, 11=42, 12=24, 13=25, 14=26, 15=21, 16=22, 17=23, 18=27, 19=20, 20=19, 21=18, 22=17, 23=28, 24=30, 25=15, 26=16, 27=14, 28=13, 29=10, 30=11, 31=12, 32=9, 33=6, 34=7, 35=8, 36=5, 37=2, 38=1, 39=3, 40=4, 41=31, 42=29}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=37, 2=35, 3=29, 4=28, 5=30, 6=31, 7=32, 8=33, 9=34, 10=36, 11=38, 12=39, 13=40, 14=41, 15=42, 16=20, 17=21, 18=22, 19=17, 20=18, 21=19, 22=23, 23=16, 24=15, 25=14, 26=13, 27=24, 28=26, 29=11, 30=12, 31=10, 32=9, 33=6, 34=7, 35=8, 36=5, 37=2, 38=1, 39=3, 40=4, 41=27, 42=25}

