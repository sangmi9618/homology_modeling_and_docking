
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O>>[CH]C(=[CH])O:1.0, O>>[CH]C=C(O)C=[CH]:1.0, O>>[C]O:1.0, [CH]:2.0, [CH]C(=[CH])O:2.0, [CH]C=C(O)C=[CH]:1.0, [CH]O:1.0, [CH]OC(=C[CH])C=[CH]:1.0, [CH]OC(=C[CH])C=[CH]>>[CH]C=C(O)C=[CH]:1.0, [CH]OC(O)C([CH])[NH]:1.0, [CH]OC(OC(=C[CH])C=[CH])C([CH])[NH]>>[CH]OC(O)C([CH])[NH]:1.0, [C]:2.0, [C]NC1C(OC([CH2])[CH]C1O)OC([CH])=[CH]>>[C]NC1C(O)OC([CH2])[CH]C1O:1.0, [C]O:1.0, [C]OC(O[CH])C([CH])[NH]:1.0, [C]OC(O[CH])C([CH])[NH]>>[CH]OC(O)C([CH])[NH]:1.0, [C]O[CH]:1.0, [C]O[CH]>>[CH]O:1.0, [OH]:2.0, [O]:1.0, [O]C([CH])=[CH]:1.0, [O]C([CH])=[CH]>>[CH]C(=[CH])O:1.0, [O]C([CH])O:2.0, [O]C([CH])OC([CH])=[CH]:1.0, [O]C([CH])OC([CH])=[CH]>>[O]C([CH])O:1.0, [O]C([CH])OC=1C=C[C]=CC1>>OC=1C=C[C]=CC1:1.0, [O]C([O])[CH]:1.0, [O]C([O])[CH]>>[O]C([CH])O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [C]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, [CH]C(=[CH])O:1.0, [C]O:1.0, [C]O[CH]:1.0, [O]C([CH])=[CH]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, [CH]C(=[CH])O:1.0, [CH]C=C(O)C=[CH]:1.0, [CH]OC(=C[CH])C=[CH]:1.0, [O]C([CH])OC([CH])=[CH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]C([CH])O:1.0, [O]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[CH]OC(O)C([CH])[NH]:1.0, [C]OC(O[CH])C([CH])[NH]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]O[CH]>>[CH]O
2: [O]C([O])[CH]>>[O]C([CH])O
3: [O]C([CH])=[CH]>>[CH]C(=[CH])O
4: O>>[C]O

MMP Level 2
1: [O]C([CH])OC([CH])=[CH]>>[O]C([CH])O
2: [C]OC(O[CH])C([CH])[NH]>>[CH]OC(O)C([CH])[NH]
3: [CH]OC(=C[CH])C=[CH]>>[CH]C=C(O)C=[CH]
4: O>>[CH]C(=[CH])O

MMP Level 3
1: [CH]OC(OC(=C[CH])C=[CH])C([CH])[NH]>>[CH]OC(O)C([CH])[NH]
2: [C]NC1C(OC([CH2])[CH]C1O)OC([CH])=[CH]>>[C]NC1C(O)OC([CH2])[CH]C1O
3: [O]C([CH])OC=1C=C[C]=CC1>>OC=1C=C[C]=CC1
4: O>>[CH]C=C(O)C=[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[[O-][N+](=O)c1ccc(OC2OC(CO)C(O)C(O)C2NC(=O)C)cc1>>[O-][N+](=O)c1ccc(O)cc1]
2: R:M00001, P:M00004	[[O-][N+](=O)c1ccc(OC2OC(CO)C(O)C(O)C2NC(=O)C)cc1>>O=C(NC1C(O)OC(CO)C(O)C1O)C, [O-][N+](=O)c1ccc(OC2OC(CO)C(O)C(O)C2NC(=O)C)cc1>>O=C(NC1C(O)OC(CO)C(O)C1O)C]
3: R:M00002, P:M00003	[O>>[O-][N+](=O)c1ccc(O)cc1]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([NH:3][CH:4]1[CH:5]([O:6][C:7]:2:[CH:8]:[CH:9]:[C:10](:[CH:11]:[CH:12]2)[N+:13](=[O:14])[O-:15])[O:16][CH:17]([CH2:18][OH:19])[CH:20]([OH:21])[CH:22]1[OH:23])[CH3:24].[OH2:25]>>[O:1]=[C:2]([NH:3][CH:4]1[CH:5]([OH:6])[O:16][CH:17]([CH2:18][OH:19])[CH:20]([OH:21])[CH:22]1[OH:23])[CH3:24].[O:14]=[N+:13]([O-:15])[C:10]:1:[CH:9]:[CH:8]:[C:7]([OH:25]):[CH:12]:[CH:11]1


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=2, 3=1, 4=3, 5=4, 6=22, 7=20, 8=17, 9=16, 10=5, 11=6, 12=7, 13=8, 14=9, 15=10, 16=11, 17=12, 18=13, 19=14, 20=15, 21=18, 22=19, 23=21, 24=23, 25=25}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=24, 5=25, 6=19, 7=17, 8=16, 9=18, 10=23, 11=15, 12=2, 13=1, 14=3, 15=4, 16=13, 17=11, 18=8, 19=7, 20=5, 21=6, 22=9, 23=10, 24=12, 25=14}

