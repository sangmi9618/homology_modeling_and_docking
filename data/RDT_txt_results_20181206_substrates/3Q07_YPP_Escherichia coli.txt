
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)C([CH])[NH]:1.0, O=C([CH])O:2.0, O=C1N([CH])[CH]C1[NH]:1.0, O=C1N([CH])[CH]C1[NH]>>O=C(O)C([CH])[NH]:1.0, O=C1N2C(SC(C)(C)C2C(=O)O)C1[NH]>>[C]C([NH])C1SC(C)(C)C(N1)C(=O)O:1.0, O>>O=C(O)C([CH])[NH]:1.0, O>>O=C([CH])O:1.0, O>>[C]O:1.0, [CH]N[CH]:1.0, [C]:2.0, [C]C1[C]SC([CH])N1:1.0, [C]C1[C]SC2[CH]C(=O)N12:1.0, [C]C1[C]SC2[CH]C(=O)N12>>[C]C1[C]SC([CH])N1:1.0, [C]N([CH])[CH]:1.0, [C]N([CH])[CH]>>[CH]N[CH]:1.0, [C]NC1C(=O)N2C([C])[C]SC21>>[C]NC(C(=O)O)C([S])[NH]:1.0, [C]O:1.0, [NH]:1.0, [N]:1.0, [N]C(=O)[CH]:1.0, [N]C(=O)[CH]>>O=C([CH])O:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [C]:2.0, [N]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=C([CH])O:1.0, [C]N([CH])[CH]:1.0, [C]O:1.0, [N]C(=O)[CH]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=C(O)C([CH])[NH]:1.0, O=C([CH])O:1.0, O=C1N([CH])[CH]C1[NH]:1.0, [C]C1[C]SC2[CH]C(=O)N12:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O>>[C]O
2: [C]N([CH])[CH]>>[CH]N[CH]
3: [N]C(=O)[CH]>>O=C([CH])O

MMP Level 2
1: O>>O=C([CH])O
2: [C]C1[C]SC2[CH]C(=O)N12>>[C]C1[C]SC([CH])N1
3: O=C1N([CH])[CH]C1[NH]>>O=C(O)C([CH])[NH]

MMP Level 3
1: O>>O=C(O)C([CH])[NH]
2: O=C1N2C(SC(C)(C)C2C(=O)O)C1[NH]>>[C]C([NH])C1SC(C)(C)C(N1)C(=O)O
3: [C]NC1C(=O)N2C([C])[C]SC21>>[C]NC(C(=O)O)C([S])[NH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C1C(=O)N(CCN1C(=O)NC(c2ccccc2)C(=O)NC3C(=O)N4C3SC(C)(C)C4C(=O)O)CC>>O=C1C(=O)N(CCN1C(=O)NC(c2ccccc2)C(=O)NC(C(=O)O)C3SC(C)(C)C(N3)C(=O)O)CC, O=C1C(=O)N(CCN1C(=O)NC(c2ccccc2)C(=O)NC3C(=O)N4C3SC(C)(C)C4C(=O)O)CC>>O=C1C(=O)N(CCN1C(=O)NC(c2ccccc2)C(=O)NC(C(=O)O)C3SC(C)(C)C(N3)C(=O)O)CC]
2: R:M00002, P:M00003	[O>>O=C1C(=O)N(CCN1C(=O)NC(c2ccccc2)C(=O)NC(C(=O)O)C3SC(C)(C)C(N3)C(=O)O)CC]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]1[N:5]2[C:6](=[O:7])[CH:8]([NH:9][C:10](=[O:11])[CH:12]([NH:13][C:14](=[O:15])[N:16]3[C:17](=[O:18])[C:19](=[O:20])[N:21]([CH2:22][CH3:23])[CH2:24][CH2:25]3)[C:26]:4:[CH:27]:[CH:28]:[CH:29]:[CH:30]:[CH:31]4)[CH:32]2[S:33][C:34]1([CH3:35])[CH3:36].[OH2:37]>>[O:7]=[C:6]([OH:37])[CH:8]([NH:9][C:10](=[O:11])[CH:12]([NH:13][C:14](=[O:15])[N:16]1[C:17](=[O:18])[C:19](=[O:20])[N:21]([CH2:22][CH3:23])[CH2:24][CH2:25]1)[C:26]:2:[CH:27]:[CH:28]:[CH:29]:[CH:30]:[CH:31]2)[CH:32]3[S:33][C:34]([CH3:35])([CH3:36])[CH:4]([NH:5]3)[C:2](=[O:1])[OH:3]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=23, 2=22, 3=21, 4=24, 5=25, 6=16, 7=17, 8=18, 9=19, 10=20, 11=14, 12=15, 13=13, 14=12, 15=26, 16=27, 17=28, 18=29, 19=30, 20=31, 21=10, 22=11, 23=9, 24=8, 25=32, 26=5, 27=6, 28=7, 29=4, 30=34, 31=33, 32=35, 33=36, 34=2, 35=1, 36=3, 37=37}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=19, 2=18, 3=17, 4=20, 5=21, 6=12, 7=13, 8=14, 9=15, 10=16, 11=10, 12=11, 13=9, 14=8, 15=22, 16=23, 17=24, 18=25, 19=26, 20=27, 21=6, 22=7, 23=5, 24=4, 25=28, 26=34, 27=33, 28=30, 29=29, 30=31, 31=32, 32=35, 33=36, 34=37, 35=2, 36=1, 37=3}

