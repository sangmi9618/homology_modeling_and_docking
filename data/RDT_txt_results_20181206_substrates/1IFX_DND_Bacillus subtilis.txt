
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0, O-P:4.0]

//
FINGERPRINTS RC
[N:3.0, N>>O=C(N)C([CH])=[CH]:1.0, N>>[C]C(=O)N:1.0, N>>[C]N:1.0, O=C(N)C([CH])=[CH]:1.0, O=C(O)C([CH])=[CH]:1.0, O=C(O)C([CH])=[CH]>>O=C(N)C([CH])=[CH]:1.0, O=C(O)C([CH])=[CH]>>O=P(O)(O[CH2])OP(=O)(O)O[CH2]:1.0, O=P(O)(O[CH2])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]:1.0, O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[CH2]:4.0, O=P(O)(O[P])O[CH2]>>O=P(O)(O[P])O[CH2]:2.0, [C]:2.0, [C]C(=O)N:2.0, [C]C(=O)O:2.0, [C]C(=O)O>>[C]C(=O)N:1.0, [C]C(=O)O>>[O]P(=O)(O)OP([O])(=O)O:1.0, [C]N:1.0, [C]O:1.0, [C]O>>[P]O[P]:1.0, [N+]C=C(C=[CH])C(=O)O>>[N+]C=C(C=[CH])C(=O)N:1.0, [NH2]:1.0, [OH]:3.0, [O]:3.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)OP(=O)(O)OC[CH]>>[O]P(=O)(O)OP(=O)(O)OC[CH]:2.0, [O]P(=O)(O)OP([O])(=O)O:3.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:2.0, [O]P([O])(=O)O:4.0, [O]P([O])(=O)O>>[O]P([O])(=O)O:2.0, [P]:4.0, [P]O:2.0, [P]O[P]:3.0, [P]O[P]>>[P]O:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (6)
[N:1.0, [C]:2.0, [NH2]:1.0, [OH]:1.0, [O]:4.0, [P]:4.0]


ID=Reaction Center at Level: 1 (7)
[N:1.0, [C]C(=O)N:1.0, [C]C(=O)O:1.0, [C]N:1.0, [C]O:1.0, [O]P([O])(=O)O:4.0, [P]O[P]:4.0]


ID=Reaction Center at Level: 2 (7)
[N:1.0, O=C(N)C([CH])=[CH]:1.0, O=C(O)C([CH])=[CH]:1.0, O=P(O)(O[P])O[CH2]:4.0, [C]C(=O)N:1.0, [C]C(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:4.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]P([O])(=O)O>>[O]P([O])(=O)O
2: [O]P([O])(=O)O>>[O]P([O])(=O)O
3: N>>[C]N
4: [C]C(=O)O>>[C]C(=O)N
5: [P]O[P]>>[P]O
6: [C]O>>[P]O[P]
7: [P]O[P]>>[P]O

MMP Level 2
1: O=P(O)(O[P])O[CH2]>>O=P(O)(O[P])O[CH2]
2: O=P(O)(O[P])O[CH2]>>O=P(O)(O[P])O[CH2]
3: N>>[C]C(=O)N
4: O=C(O)C([CH])=[CH]>>O=C(N)C([CH])=[CH]
5: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
6: [C]C(=O)O>>[O]P(=O)(O)OP([O])(=O)O
7: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O

MMP Level 3
1: [O]P(=O)(O)OP(=O)(O)OC[CH]>>[O]P(=O)(O)OP(=O)(O)OC[CH]
2: [O]P(=O)(O)OP(=O)(O)OC[CH]>>[O]P(=O)(O)OP(=O)(O)OC[CH]
3: N>>O=C(N)C([CH])=[CH]
4: [N+]C=C(C=[CH])C(=O)O>>[N+]C=C(C=[CH])C(=O)N
5: O=P(O)(O[CH2])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]
6: O=C(O)C([CH])=[CH]>>O=P(O)(O[CH2])OP(=O)(O)O[CH2]
7: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C(O)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=P(O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00006	[O=C(O)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O, O=C(O)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O, O=C(O)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O]
3: R:M00002, P:M00005	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)O]
4: R:M00002, P:M00006	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O]
5: R:M00003, P:M00006	[N>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[C:4]:1:[CH:5]:[CH:6]:[CH:7]:[N+:8](:[CH:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44].[O:45]=[P:46]([OH:47])([OH:48])[O:49][P:50](=[O:51])([OH:52])[O:53][P:54](=[O:55])([OH:56])[O:57][CH2:58][CH:59]1[O:60][CH:61]([N:62]:2:[CH:63]:[N:64]:[C:65]:3:[C:66](:[N:67]:[CH:68]:[N:69]:[C:70]32)[NH2:71])[CH:72]([OH:73])[CH:74]1[OH:75].[NH3:76]>>[O:1]=[C:2]([NH2:76])[C:4]:1:[CH:5]:[CH:6]:[CH:7]:[N+:8](:[CH:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:3][P:54](=[O:55])([OH:56])[O:57][CH2:58][CH:59]3[O:60][CH:61]([N:62]:4:[CH:63]:[N:64]:[C:65]:5:[C:66](:[N:67]:[CH:68]:[N:69]:[C:70]54)[NH2:71])[CH:72]([OH:73])[CH:74]3[OH:75])[CH:41]([OH:42])[CH:43]2[OH:44].[O:20]=[P:19]([OH:18])([OH:21])[O:22][CH2:23][CH:24]1[O:25][CH:26]([N:27]:2:[CH:28]:[N:29]:[C:30]:3:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]32)[NH2:36])[CH:37]([OH:38])[CH:39]1[OH:40].[O:45]=[P:46]([OH:47])([OH:48])[O:49][P:50](=[O:51])([OH:52])[OH:53]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=5, 3=4, 4=9, 5=8, 6=7, 7=10, 8=43, 9=41, 10=12, 11=11, 12=13, 13=14, 14=15, 15=16, 16=17, 17=18, 18=19, 19=20, 20=21, 21=22, 22=23, 23=24, 24=39, 25=37, 26=26, 27=25, 28=27, 29=28, 30=29, 31=30, 32=35, 33=34, 34=33, 35=32, 36=31, 37=36, 38=38, 39=40, 40=42, 41=44, 42=2, 43=1, 44=3, 45=68, 46=69, 47=70, 48=65, 49=66, 50=67, 51=71, 52=64, 53=63, 54=62, 55=61, 56=72, 57=74, 58=59, 59=60, 60=58, 61=57, 62=54, 63=55, 64=56, 65=53, 66=50, 67=51, 68=52, 69=49, 70=46, 71=45, 72=47, 73=48, 74=75, 75=73, 76=76}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=60, 2=61, 3=62, 4=57, 5=58, 6=59, 7=63, 8=56, 9=55, 10=54, 11=53, 12=64, 13=66, 14=51, 15=52, 16=50, 17=49, 18=46, 19=45, 20=47, 21=48, 22=67, 23=65, 24=70, 25=69, 26=68, 27=71, 28=72, 29=73, 30=74, 31=75, 32=76, 33=6, 34=5, 35=4, 36=9, 37=8, 38=7, 39=10, 40=43, 41=41, 42=12, 43=11, 44=13, 45=14, 46=15, 47=16, 48=17, 49=18, 50=19, 51=20, 52=21, 53=22, 54=23, 55=24, 56=39, 57=37, 58=26, 59=25, 60=27, 61=28, 62=29, 63=30, 64=35, 65=34, 66=33, 67=32, 68=31, 69=36, 70=38, 71=40, 72=42, 73=44, 74=2, 75=1, 76=3}

