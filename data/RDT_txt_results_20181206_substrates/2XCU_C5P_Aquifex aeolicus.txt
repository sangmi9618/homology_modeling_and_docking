
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%O:1.0, O-P:2.0]

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(O)C(=O)C[CH]:1.0, O=C(O)C(O[CH])(OP(=O)(O)O[CH2])C[CH]>>O=C(O)C(=O)C[CH]:1.0, O=C(O)C(O[P])(O[CH])C[CH]:1.0, O=C(O)C(O[P])(O[CH])C[CH]>>O=C(O)C(=O)C[CH]:1.0, O=C(O)C1(OC(C(O)[CH]C1)C(O)[CH2])O[P]>>[CH]C(O)C(O)C(O)[CH2]:1.0, O=P(O)(O)O[CH2]:1.0, [CH]C([CH])O:1.0, [CH]O:1.0, [C]:2.0, [C]=O:1.0, [C]C(=O)[CH2]:2.0, [C]C([O])(OC([CH])[CH])[CH2]:1.0, [C]C([O])(OC([CH])[CH])[CH2]>>[CH]C([CH])O:1.0, [C]C([O])(OP(=O)(O)OC[CH])[CH2]>>O=P(O)(O)OC[CH]:1.0, [C]C([O])(OP([O])(=O)O)[CH2]:1.0, [C]C([O])(OP([O])(=O)O)[CH2]>>[C]C(=O)[CH2]:1.0, [C]C([O])([O])[CH2]:1.0, [C]C([O])([O])[CH2]>>[C]C(=O)[CH2]:1.0, [C]OP(=O)(O)O[CH2]:1.0, [C]OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]:1.0, [C]O[CH]:1.0, [C]O[CH]>>[CH]O:1.0, [C]O[P]:1.0, [C]O[P]>>[C]=O:1.0, [OH]:1.0, [O]:3.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OC1(OC([CH])[CH]C(O)C1)C(=O)O>>O=C(O)C(=O)CC([CH])O:1.0, [O]P([O])(=O)O:1.0, [O]P([O])(=O)O>>[O]P(=O)(O)O:1.0, [P]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[C]:1.0, [OH]:1.0, [O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (6)
[[C]C([O])([O])[CH2]:1.0, [C]O[CH]:1.0, [C]O[P]:1.0, [O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [P]O:1.0]


ID=Reaction Center at Level: 2 (6)
[O=C(O)C(O[P])(O[CH])C[CH]:1.0, O=P(O)(O)O[CH2]:1.0, [C]C([O])(OC([CH])[CH])[CH2]:1.0, [C]C([O])(OP([O])(=O)O)[CH2]:1.0, [C]OP(=O)(O)O[CH2]:1.0, [O]P(=O)(O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (2)
[[C]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (4)
[[C]=O:1.0, [C]C(=O)[CH2]:1.0, [C]C([O])([O])[CH2]:1.0, [C]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(O)C(=O)C[CH]:1.0, O=C(O)C(O[P])(O[CH])C[CH]:1.0, [C]C(=O)[CH2]:1.0, [C]C([O])(OP([O])(=O)O)[CH2]:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[C]:2.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(=O)[CH2]:1.0, [C]C([O])([O])[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C(=O)C[CH]:1.0, O=C(O)C(O[P])(O[CH])C[CH]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [O]P([O])(=O)O>>[O]P(=O)(O)O
2: [C]O[CH]>>[CH]O
3: [C]O[P]>>[C]=O
4: [C]C([O])([O])[CH2]>>[C]C(=O)[CH2]

MMP Level 2
1: [C]OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]
2: [C]C([O])(OC([CH])[CH])[CH2]>>[CH]C([CH])O
3: [C]C([O])(OP([O])(=O)O)[CH2]>>[C]C(=O)[CH2]
4: O=C(O)C(O[P])(O[CH])C[CH]>>O=C(O)C(=O)C[CH]

MMP Level 3
1: [C]C([O])(OP(=O)(O)OC[CH])[CH2]>>O=P(O)(O)OC[CH]
2: O=C(O)C1(OC(C(O)[CH]C1)C(O)[CH2])O[P]>>[CH]C(O)C(O)C(O)[CH2]
3: O=C(O)C(O[CH])(OP(=O)(O)O[CH2])C[CH]>>O=C(O)C(=O)C[CH]
4: [O]P(=O)(O)OC1(OC([CH])[CH]C(O)C1)C(=O)O>>O=C(O)C(=O)CC([CH])O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=c1nc(N)ccn1C2OC(COP(=O)(O)OC3(OC(C(O)C(O)C3)C(O)CO)C(=O)O)C(O)C2O>>O=c1nc(N)ccn1C2OC(COP(=O)(O)O)C(O)C2O]
2: R:M00001, P:M00003	[O=c1nc(N)ccn1C2OC(COP(=O)(O)OC3(OC(C(O)C(O)C3)C(O)CO)C(=O)O)C(O)C2O>>O=C(O)C(=O)CC(O)C(O)C(O)C(O)CO, O=c1nc(N)ccn1C2OC(COP(=O)(O)OC3(OC(C(O)C(O)C3)C(O)CO)C(=O)O)C(O)C2O>>O=C(O)C(=O)CC(O)C(O)C(O)C(O)CO, O=c1nc(N)ccn1C2OC(COP(=O)(O)OC3(OC(C(O)C(O)C3)C(O)CO)C(=O)O)C(O)C2O>>O=C(O)C(=O)CC(O)C(O)C(O)C(O)CO]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[C:4]1([O:5][CH:6]([CH:7]([OH:8])[CH2:9][OH:10])[CH:11]([OH:12])[CH:13]([OH:14])[CH2:15]1)[O:16][P:17](=[O:18])([OH:19])[O:20][CH2:21][CH:22]2[O:23][CH:24]([N:25]3[CH:26]=[CH:27][C:28](=[N:29][C:30]3=[O:31])[NH2:32])[CH:33]([OH:34])[CH:35]2[OH:36]>>[O:1]=[C:2]([OH:3])[C:4](=[O:16])[CH2:15][CH:13]([OH:14])[CH:11]([OH:12])[CH:6]([OH:5])[CH:7]([OH:8])[CH2:9][OH:10].[O:31]=[C:30]1[N:29]=[C:28]([NH2:32])[CH:27]=[CH:26][N:25]1[CH:24]2[O:23][CH:22]([CH2:21][O:20][P:17](=[O:18])([OH:19])[OH:37])[CH:35]([OH:36])[CH:33]2[OH:34]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=15, 2=13, 3=11, 4=6, 5=5, 6=4, 7=2, 8=1, 9=3, 10=16, 11=17, 12=18, 13=19, 14=20, 15=21, 16=22, 17=35, 18=33, 19=24, 20=23, 21=25, 22=26, 23=27, 24=28, 25=29, 26=30, 27=31, 28=32, 29=34, 30=36, 31=7, 32=9, 33=10, 34=8, 35=12, 36=14}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=8, 4=2, 5=1, 6=3, 7=4, 8=5, 9=9, 10=20, 11=18, 12=11, 13=10, 14=12, 15=13, 16=14, 17=15, 18=16, 19=17, 20=19, 21=21, 22=27, 23=28, 24=30, 25=32, 26=34, 27=36, 28=37, 29=35, 30=33, 31=31, 32=29, 33=25, 34=26, 35=23, 36=22, 37=24}

