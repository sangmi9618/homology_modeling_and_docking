
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C=O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=CC:3.0, O=[CH]:1.0, O>>O=CC:2.0, O>>O=[CH]:1.0, [CH2]:1.0, [CH]:1.0, [N+]:1.0, [N+]CC:1.0, [N+]CC>>O=CC:1.0, [N]:1.0, [O-]N=O:2.0, [O-][N+](=O)CC:2.0, [O-][N+](=O)CC>>O=CC:2.0, [O-][N+](=O)CC>>[O-]N=O:2.0, [O-][N+](=O)[CH2]:1.0, [O-][N+](=O)[CH2]>>[O-]N=O:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[O:1.0, [CH2]:1.0, [CH]:1.0, [N+]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=CC:1.0, O=[CH]:1.0, [N+]CC:1.0, [O-][N+](=O)[CH2]:1.0]


ID=Reaction Center at Level: 2 (3)
[O:1.0, O=CC:2.0, [O-][N+](=O)CC:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [N+]CC>>O=CC
2: O>>O=[CH]
3: [O-][N+](=O)[CH2]>>[O-]N=O

MMP Level 2
1: [O-][N+](=O)CC>>O=CC
2: O>>O=CC
3: [O-][N+](=O)CC>>[O-]N=O

MMP Level 3
1: [O-][N+](=O)CC>>O=CC
2: O>>O=CC
3: [O-][N+](=O)CC>>[O-]N=O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[[O-][N+](=O)CC>>O=CC]
2: R:M00001, P:M00004	[[O-][N+](=O)CC>>[O-]N=O]
3: R:M00002, P:M00003	[O>>O=CC]


//
SELECTED AAM MAPPING
[O:1]=[N+:2]([O-:3])[CH2:4][CH3:5].[OH2:6]>>[O:1]=[N:2][O-:3].[O:6]=[CH:4][CH3:5].[OH:7][OH:8]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=5, 2=4, 3=2, 4=1, 5=3, 6=6}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=5, 3=4, 4=2, 5=1, 6=3, 7=7, 8=8}

