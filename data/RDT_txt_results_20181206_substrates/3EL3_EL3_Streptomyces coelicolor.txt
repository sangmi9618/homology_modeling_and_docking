
//
FINGERPRINTS BC

FORMED_CLEAVED
[C=O:1.0, O=O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1:1.0, O=O:4.0, O=O>>O:3.0, O=O>>[C]=C1[C][CH]CC1=O:1.0, O=O>>[C]=O:1.0, O=O>>[C]C(=O)[CH2]:1.0, [CH2]:2.0, [CH]:1.0, [CH]N([CH])[CH]:1.0, [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]:1.0, [C]:1.0, [C]=C1[C][CH]CC1:1.0, [C]=C1[C][CH]CC1=O:1.0, [C]=C1[C][CH]CC1>>[C]=C1[C][CH]CC1=O:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=O:1.0, [C]C(=C1CCC(C)C1([CH2])[CH2])C>>[C]C(=C1C(=O)CC(C)C1([CH2])[CH2])C:1.0, [C]C(=O)[CH2]:2.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]:1.0, [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O:1.0, [C]C=[CH]:1.0, [C]C[CH2]:1.0, [C]C[CH2]>>[C]C(=O)[CH2]:1.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C=[CH]:1.0, [N+]:1.0, [N]:1.0, [O]:3.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[O:1.0, [C]:1.0, [O]:3.0]


ID=Reaction Center at Level: 1 (4)
[O:1.0, O=O:2.0, [C]=O:1.0, [C]C(=O)[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=O:2.0, [C]=C1[C][CH]CC1=O:1.0, [C]C(=O)[CH2]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=O>>[C]=O
2: [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]
3: [C]C[CH]>>[C]C=[CH]
4: [C]C[CH2]>>[C]C(=O)[CH2]
5: O=O>>O

MMP Level 2
1: O=O>>[C]C(=O)[CH2]
2: [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]
3: [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]
4: [C]=C1[C][CH]CC1>>[C]=C1[C][CH]CC1=O
5: O=O>>O

MMP Level 3
1: O=O>>[C]=C1[C][CH]CC1=O
2: [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O
3: O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1
4: [C]C(=C1CCC(C)C1([CH2])[CH2])C>>[C]C(=C1C(=O)CC(C)C1([CH2])[CH2])C
5: O=O>>O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00005	[C1(=C2CCC(C)C23CCC(C3)C1(C)C)C>>O=C1C2=C(C)C(C)(C)C3CCC2(C3)C(C)C1]
2: R:M00002, P:M00006	[O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O]
3: R:M00004, P:M00005	[O=O>>O=C1C2=C(C)C(C)(C)C3CCC2(C3)C(C)C1]
4: R:M00004, P:M00007	[O=O>>O]


//
SELECTED AAM MAPPING
[H+:66].[O:64]=[O:65].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:5][N:6]([CH:7]=[CH:8][CH2:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48].[C:49]1(=[C:50]2[CH2:51][CH2:52][CH:53]([CH3:54])[C:55]23[CH2:56][CH2:57][CH:58]([CH2:59]3)[C:60]1([CH3:61])[CH3:62])[CH3:63]>>[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:9]:[CH:8]:[CH:7]:[N+:6](:[CH:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48].[O:64]=[C:51]1[C:50]2=[C:49]([CH3:63])[C:60]([CH3:62])([CH3:61])[CH:58]3[CH2:57][CH2:56][C:55]2([CH2:59]3)[CH:53]([CH3:54])[CH2:52]1.[OH2:65]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=54, 2=53, 3=52, 4=51, 5=50, 6=49, 7=60, 8=58, 9=57, 10=56, 11=55, 12=59, 13=61, 14=62, 15=63, 16=9, 17=8, 18=7, 19=6, 20=5, 21=4, 22=2, 23=1, 24=3, 25=10, 26=47, 27=45, 28=12, 29=11, 30=13, 31=14, 32=15, 33=16, 34=17, 35=18, 36=19, 37=20, 38=21, 39=22, 40=23, 41=24, 42=43, 43=37, 44=26, 45=25, 46=27, 47=28, 48=29, 49=30, 50=35, 51=34, 52=33, 53=32, 54=31, 55=36, 56=38, 57=39, 58=40, 59=41, 60=42, 61=44, 62=46, 63=48, 64=66, 65=64, 66=65}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=63, 2=62, 3=64, 4=50, 5=49, 6=51, 7=52, 8=54, 9=57, 10=58, 11=59, 12=60, 13=61, 14=55, 15=56, 16=53, 17=6, 18=5, 19=4, 20=9, 21=8, 22=7, 23=10, 24=47, 25=45, 26=12, 27=11, 28=13, 29=14, 30=15, 31=16, 32=17, 33=18, 34=19, 35=20, 36=21, 37=22, 38=23, 39=24, 40=43, 41=37, 42=26, 43=25, 44=27, 45=28, 46=29, 47=30, 48=35, 49=34, 50=33, 51=32, 52=31, 53=36, 54=38, 55=39, 56=40, 57=41, 58=42, 59=44, 60=46, 61=48, 62=2, 63=1, 64=3, 65=65}

