
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%C:1.0, C%O:2.0]

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):4.0]

//
FINGERPRINTS RC
[O=C(NC1C(O)C[C]OC1C([CH])O)C>>O=C(NC1C(O)O[CH]C(O)C1O)C:1.0, O=C(O)C(=O)C:1.0, O=C(O)C(O)(O[CH])C[CH]:1.0, O=C(O)C(O)(O[CH])C[CH]>>O=C(O)C(=O)C:2.0, O=C(O)C1(O)OC(C([NH])[CH]C1)C([CH])O>>[CH]C([NH])C(O)C([CH])O:1.0, O=C(O)C1(O)OC([CH])[CH]C(O)C1>>O=C(O)C(=O)C:1.0, O=C(O)C1(O)O[CH]C([NH])C(O)C1>>O=C(O)C(=O)C:1.0, [CH2]:1.0, [CH3]:1.0, [CH]:6.0, [CH]C(O)C(O)CO>>[NH]C1[CH]C(O)C(OC1O)CO:1.0, [CH]C(O)OC([CH])[CH2]:1.0, [CH]C(O)[CH2]:2.0, [CH]C(O)[CH2]>>[CH]C(O)OC([CH])[CH2]:1.0, [CH]C(O)[CH2]>>[O]C([CH])O:1.0, [CH]C([CH])O:3.0, [CH]C([CH])O>>[CH]C([CH])O:1.0, [CH]C([CH])[NH]:2.0, [CH]C([CH])[NH]>>[CH]C([CH])[NH]:1.0, [CH]O:2.0, [CH]O>>[CH]O[CH]:1.0, [CH]OC(O)C([CH])[NH]:1.0, [CH]O[CH]:1.0, [C]:2.0, [C]=O:1.0, [C]C:1.0, [C]C(=O)C:3.0, [C]C(O)(OC([CH])[CH])[CH2]:1.0, [C]C(O)(OC([CH])[CH])[CH2]>>[CH]C([CH])O:1.0, [C]C([O])(O)CC([CH])O:1.0, [C]C([O])(O)CC([CH])O>>[C]C(=O)C:1.0, [C]C([O])(O)[CH2]:2.0, [C]C([O])(O)[CH2]>>[C]C(=O)C:2.0, [C]CC(O)C([CH])[NH]:1.0, [C]CC(O)C([CH])[NH]>>[CH]OC(O)C([CH])[NH]:1.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C:1.0, [C]NC(C([O])O)C([CH])O:1.0, [C]NC(C([O])[CH])C(O)[CH2]:1.0, [C]NC(C([O])[CH])C(O)[CH2]>>[C]NC(C([O])O)C([CH])O:1.0, [C]NC1C([CH])OC([C])(O)CC1O>>[C]NC1C(O)OC([CH2])[CH]C1O:1.0, [C]O:1.0, [C]O>>[C]=O:1.0, [C]OC(C([CH])[NH])C(O)C(O)CO>>[NH]C1[CH]OC(CO)C(O)C1O:1.0, [C]O[CH]:1.0, [C]O[CH]>>[CH]O:1.0, [OH]:3.0, [O]:3.0, [O]C([CH2])C(O)C([CH])O:1.0, [O]C([CH])C(O)C(O)[CH2]:1.0, [O]C([CH])C(O)C(O)[CH2]>>[O]C([CH2])C(O)C([CH])O:1.0, [O]C([CH])O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[CH2]:1.0, [CH]:2.0, [C]:1.0, [O]:2.0]


ID=Reaction Center at Level: 1 (6)
[[CH]C(O)[CH2]:1.0, [CH]O[CH]:1.0, [C]C([O])(O)[CH2]:1.0, [C]C[CH]:1.0, [C]O[CH]:1.0, [O]C([CH])O:1.0]


ID=Reaction Center at Level: 2 (6)
[O=C(O)C(O)(O[CH])C[CH]:1.0, [CH]C(O)OC([CH])[CH2]:1.0, [CH]OC(O)C([CH])[NH]:1.0, [C]C(O)(OC([CH])[CH])[CH2]:1.0, [C]C([O])(O)CC([CH])O:1.0, [C]CC(O)C([CH])[NH]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[C]=O:1.0, [C]C(=O)C:1.0, [C]C([O])(O)[CH2]:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(O)C(=O)C:1.0, O=C(O)C(O)(O[CH])C[CH]:1.0, [C]C(=O)C:1.0, [C]C([O])(O)[CH2]:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:6.0, [C]:2.0]


ID=Reaction Center at Level: 1 (6)
[[CH]C(O)[CH2]:1.0, [CH]C([CH])O:2.0, [CH]C([CH])[NH]:2.0, [C]C(=O)C:1.0, [C]C([O])(O)[CH2]:1.0, [O]C([CH])O:1.0]


ID=Reaction Center at Level: 2 (8)
[O=C(O)C(=O)C:1.0, O=C(O)C(O)(O[CH])C[CH]:1.0, [CH]OC(O)C([CH])[NH]:1.0, [C]CC(O)C([CH])[NH]:1.0, [C]NC(C([O])O)C([CH])O:1.0, [C]NC(C([O])[CH])C(O)[CH2]:1.0, [O]C([CH2])C(O)C([CH])O:1.0, [O]C([CH])C(O)C(O)[CH2]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]C([CH])[NH]>>[CH]C([CH])[NH]
2: [CH]C(O)[CH2]>>[O]C([CH])O
3: [C]O[CH]>>[CH]O
4: [C]C([O])(O)[CH2]>>[C]C(=O)C
5: [C]C[CH]>>[C]C
6: [CH]O>>[CH]O[CH]
7: [CH]C([CH])O>>[CH]C([CH])O
8: [C]O>>[C]=O

MMP Level 2
1: [C]NC(C([O])[CH])C(O)[CH2]>>[C]NC(C([O])O)C([CH])O
2: [C]CC(O)C([CH])[NH]>>[CH]OC(O)C([CH])[NH]
3: [C]C(O)(OC([CH])[CH])[CH2]>>[CH]C([CH])O
4: O=C(O)C(O)(O[CH])C[CH]>>O=C(O)C(=O)C
5: [C]C([O])(O)CC([CH])O>>[C]C(=O)C
6: [CH]C(O)[CH2]>>[CH]C(O)OC([CH])[CH2]
7: [O]C([CH])C(O)C(O)[CH2]>>[O]C([CH2])C(O)C([CH])O
8: [C]C([O])(O)[CH2]>>[C]C(=O)C

MMP Level 3
1: O=C(NC1C(O)C[C]OC1C([CH])O)C>>O=C(NC1C(O)O[CH]C(O)C1O)C
2: [C]NC1C([CH])OC([C])(O)CC1O>>[C]NC1C(O)OC([CH2])[CH]C1O
3: O=C(O)C1(O)OC(C([NH])[CH]C1)C([CH])O>>[CH]C([NH])C(O)C([CH])O
4: O=C(O)C1(O)OC([CH])[CH]C(O)C1>>O=C(O)C(=O)C
5: O=C(O)C1(O)O[CH]C([NH])C(O)C1>>O=C(O)C(=O)C
6: [CH]C(O)C(O)CO>>[NH]C1[CH]C(O)C(OC1O)CO
7: [C]OC(C([CH])[NH])C(O)C(O)CO>>[NH]C1[CH]OC(CO)C(O)C1O
8: O=C(O)C(O)(O[CH])C[CH]>>O=C(O)C(=O)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)C1(O)OC(C(NC(=O)C)C(O)C1)C(O)C(O)CO>>O=C(NC1C(O)OC(CO)C(O)C1O)C, O=C(O)C1(O)OC(C(NC(=O)C)C(O)C1)C(O)C(O)CO>>O=C(NC1C(O)OC(CO)C(O)C1O)C, O=C(O)C1(O)OC(C(NC(=O)C)C(O)C1)C(O)C(O)CO>>O=C(NC1C(O)OC(CO)C(O)C1O)C, O=C(O)C1(O)OC(C(NC(=O)C)C(O)C1)C(O)C(O)CO>>O=C(NC1C(O)OC(CO)C(O)C1O)C, O=C(O)C1(O)OC(C(NC(=O)C)C(O)C1)C(O)C(O)CO>>O=C(NC1C(O)OC(CO)C(O)C1O)C]
2: R:M00001, P:M00003	[O=C(O)C1(O)OC(C(NC(=O)C)C(O)C1)C(O)C(O)CO>>O=C(O)C(=O)C, O=C(O)C1(O)OC(C(NC(=O)C)C(O)C1)C(O)C(O)CO>>O=C(O)C(=O)C, O=C(O)C1(O)OC(C(NC(=O)C)C(O)C1)C(O)C(O)CO>>O=C(O)C(=O)C]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[C:4]1([OH:5])[O:6][CH:7]([CH:8]([OH:9])[CH:10]([OH:11])[CH2:12][OH:13])[CH:14]([NH:15][C:16](=[O:17])[CH3:18])[CH:19]([OH:20])[CH2:21]1>>[O:1]=[C:2]([OH:3])[C:4](=[O:5])[CH3:21].[O:17]=[C:16]([NH:15][CH:14]1[CH:19]([OH:20])[O:11][CH:10]([CH2:12][OH:13])[CH:8]([OH:9])[CH:7]1[OH:6])[CH3:18]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=18, 2=16, 3=17, 4=15, 5=14, 6=19, 7=21, 8=4, 9=6, 10=7, 11=8, 12=10, 13=12, 14=13, 15=11, 16=9, 17=2, 18=1, 19=3, 20=5, 21=20}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=15, 2=2, 3=1, 4=3, 5=4, 6=13, 7=11, 8=8, 9=7, 10=5, 11=6, 12=9, 13=10, 14=12, 15=14, 16=21, 17=19, 18=20, 19=17, 20=16, 21=18}

