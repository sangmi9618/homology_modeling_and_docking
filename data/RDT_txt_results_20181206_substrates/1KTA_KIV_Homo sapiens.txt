
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:2.0, C=O:2.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O=C(O)C(=O)C(C)C:1.0, O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(=O)C[CH2]>>O=C(O)C(=O)C(C)C:1.0, O=C(O)C(=O)C[CH2]>>O=C(O)C(N)C[CH2]:1.0, O=C(O)C(N)C(C)C:1.0, O=C(O)C(N)C(C)C>>O=C(O)C(=O)C(C)C:2.0, O=C(O)C(N)C(C)C>>O=C(O)C(N)C[CH2]:1.0, O=C(O)C(N)C[CH2]:1.0, [CH]:2.0, [CH]N:2.0, [CH]N>>[CH]N:1.0, [C]:2.0, [C]=O:2.0, [C]=O>>[C]=O:1.0, [C]C(=O)[CH2]:2.0, [C]C(=O)[CH2]>>[C]C(=O)[CH]:1.0, [C]C(=O)[CH2]>>[C]C([CH2])N:1.0, [C]C(=O)[CH]:2.0, [C]C([CH2])N:2.0, [C]C([CH])N:2.0, [C]C([CH])N>>[C]C(=O)[CH]:1.0, [C]C([CH])N>>[C]C([CH2])N:1.0, [C]CCC(=O)C(=O)O>>[C]CCC(N)C(=O)O:1.0, [NH2]:2.0, [O]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[CH]:2.0, [C]:2.0, [NH2]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (6)
[[CH]N:2.0, [C]=O:2.0, [C]C(=O)[CH2]:1.0, [C]C(=O)[CH]:1.0, [C]C([CH2])N:1.0, [C]C([CH])N:1.0]


ID=Reaction Center at Level: 2 (8)
[O=C(O)C(=O)C(C)C:1.0, O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(N)C(C)C:1.0, O=C(O)C(N)C[CH2]:1.0, [C]C(=O)[CH2]:1.0, [C]C(=O)[CH]:1.0, [C]C([CH2])N:1.0, [C]C([CH])N:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:2.0, [C]:2.0]


ID=Reaction Center at Level: 1 (4)
[[C]C(=O)[CH2]:1.0, [C]C(=O)[CH]:1.0, [C]C([CH2])N:1.0, [C]C([CH])N:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(O)C(=O)C(C)C:1.0, O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(N)C(C)C:1.0, O=C(O)C(N)C[CH2]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]N>>[CH]N
2: [C]C([CH])N>>[C]C(=O)[CH]
3: [C]=O>>[C]=O
4: [C]C(=O)[CH2]>>[C]C([CH2])N

MMP Level 2
1: [C]C([CH])N>>[C]C([CH2])N
2: O=C(O)C(N)C(C)C>>O=C(O)C(=O)C(C)C
3: [C]C(=O)[CH2]>>[C]C(=O)[CH]
4: O=C(O)C(=O)C[CH2]>>O=C(O)C(N)C[CH2]

MMP Level 3
1: O=C(O)C(N)C(C)C>>O=C(O)C(N)C[CH2]
2: O=C(O)C(N)C(C)C>>O=C(O)C(=O)C(C)C
3: O=C(O)C(=O)C[CH2]>>O=C(O)C(=O)C(C)C
4: [C]CCC(=O)C(=O)O>>[C]CCC(N)C(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)C(C)C>>O=C(O)C(=O)C(C)C]
2: R:M00001, P:M00004	[O=C(O)C(N)C(C)C>>O=C(O)CCC(N)C(=O)O]
3: R:M00002, P:M00003	[O=C(O)C(=O)CCC(=O)O>>O=C(O)C(=O)C(C)C]
4: R:M00002, P:M00004	[O=C(O)C(=O)CCC(=O)O>>O=C(O)CCC(N)C(=O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[C:4](=[O:5])[CH2:6][CH2:7][C:8](=[O:9])[OH:10].[O:11]=[C:12]([OH:13])[CH:14]([NH2:15])[CH:16]([CH3:17])[CH3:18]>>[O:11]=[C:12]([OH:13])[C:14](=[O:5])[CH:16]([CH3:17])[CH3:18].[O:9]=[C:8]([OH:10])[CH2:7][CH2:6][CH:4]([NH2:15])[C:2](=[O:1])[OH:3]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=17, 2=16, 3=18, 4=14, 5=12, 6=11, 7=13, 8=15, 9=6, 10=7, 11=8, 12=9, 13=10, 14=4, 15=5, 16=2, 17=1, 18=3}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=17, 2=16, 3=18, 4=14, 5=15, 6=12, 7=11, 8=13, 9=5, 10=4, 11=2, 12=1, 13=3, 14=6, 15=8, 16=9, 17=10, 18=7}

