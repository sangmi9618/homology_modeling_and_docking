
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:2.0]

STEREO_CHANGED
[C(R/S):3.0]

//
FINGERPRINTS RC
[O:3.0, O>>[CH]C([CH])O:1.0, O>>[CH]O:1.0, O>>[O]C([CH2])C(O)C([CH])O:1.0, OC1OC([CH2])[CH]C(O)C1O>>OC1OC([CH2])[CH]C(O)C1O:1.0, [CH]:8.0, [CH]C(O)C(O)C([CH])O:1.0, [CH]C([CH])O:4.0, [CH]C([CH])O>>[CH]C([CH])O:1.0, [CH]C([CH])OC1OC([CH2])[CH]C(O)C1O>>OC1OC([CH2])[CH]C(O)C1O:1.0, [CH]O:2.0, [CH]OC(O)C([CH])O:3.0, [CH]OC(O)C([CH])O>>[CH]OC(O)C([CH])O:1.0, [CH]OC(O[CH])C([CH])O:1.0, [CH]OC(O[CH])C([CH])O>>[CH]OC(O)C([CH])O:1.0, [CH]OC1C(O)C(O)C(O)OC1[CH2]>>OC1OC([CH2])C(O)C(O)C1O:1.0, [CH]O[CH]:1.0, [CH]O[CH]>>[CH]O:1.0, [OH]:2.0, [O]:1.0, [O]C([CH2])C(O)C([CH])O:1.0, [O]C([CH2])C(OC(O[CH])C([CH])O)C([CH])O>>[CH]OC(O)C([CH])O:1.0, [O]C([CH2])C(O[CH])C([CH])O:1.0, [O]C([CH2])C(O[CH])C([CH])O>>[O]C([CH2])C(O)C([CH])O:1.0, [O]C([CH])C(O)C([CH])O:1.0, [O]C([CH])C(O)C([CH])O>>[CH]C(O)C(O)C([CH])O:1.0, [O]C([CH])O:4.0, [O]C([CH])O>>[O]C([CH])O:1.0, [O]C([CH])OC([CH])[CH]:1.0, [O]C([CH])OC([CH])[CH]>>[O]C([CH])O:1.0, [O]C([CH])OC1C(O)C(O)[CH]OC1CO>>OCC1O[CH]C(O)C(O)C1O:1.0, [O]C([CH])[CH]:1.0, [O]C([CH])[CH]>>[CH]C([CH])O:1.0, [O]C([O])[CH]:1.0, [O]C([O])[CH]>>[O]C([CH])O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [CH]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, [CH]C([CH])O:1.0, [CH]O:1.0, [CH]O[CH]:1.0, [O]C([CH])[CH]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, [CH]C([CH])O:1.0, [O]C([CH2])C(O)C([CH])O:1.0, [O]C([CH2])C(O[CH])C([CH])O:1.0, [O]C([CH])OC([CH])[CH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:6.0]


ID=Reaction Center at Level: 1 (3)
[[CH]C([CH])O:2.0, [O]C([CH])O:3.0, [O]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[CH]C(O)C(O)C([CH])O:1.0, [CH]OC(O)C([CH])O:3.0, [CH]OC(O[CH])C([CH])O:1.0, [O]C([CH])C(O)C([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [O]C([O])[CH]>>[O]C([CH])O
2: [CH]O[CH]>>[CH]O
3: [CH]C([CH])O>>[CH]C([CH])O
4: [O]C([CH])[CH]>>[CH]C([CH])O
5: [O]C([CH])O>>[O]C([CH])O
6: O>>[CH]O

MMP Level 2
1: [CH]OC(O[CH])C([CH])O>>[CH]OC(O)C([CH])O
2: [O]C([CH])OC([CH])[CH]>>[O]C([CH])O
3: [O]C([CH])C(O)C([CH])O>>[CH]C(O)C(O)C([CH])O
4: [O]C([CH2])C(O[CH])C([CH])O>>[O]C([CH2])C(O)C([CH])O
5: [CH]OC(O)C([CH])O>>[CH]OC(O)C([CH])O
6: O>>[CH]C([CH])O

MMP Level 3
1: [CH]C([CH])OC1OC([CH2])[CH]C(O)C1O>>OC1OC([CH2])[CH]C(O)C1O
2: [O]C([CH2])C(OC(O[CH])C([CH])O)C([CH])O>>[CH]OC(O)C([CH])O
3: [CH]OC1C(O)C(O)C(O)OC1[CH2]>>OC1OC([CH2])C(O)C(O)C1O
4: [O]C([CH])OC1C(O)C(O)[CH]OC1CO>>OCC1O[CH]C(O)C(O)C1O
5: OC1OC([CH2])[CH]C(O)C1O>>OC1OC([CH2])[CH]C(O)C1O
6: O>>[O]C([CH2])C(O)C([CH])O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O>>OCC1OC(O)C(O)C(O)C1O]
2: R:M00002, P:M00003	[O=P(O)(O)OCC1OC(OC2C(O)C(O)C(O)OC2CO)C(O)C(O)C1O>>OCC1OC(O)C(O)C(O)C1O, O=P(O)(O)OCC1OC(OC2C(O)C(O)C(O)OC2CO)C(O)C(O)C1O>>OCC1OC(O)C(O)C(O)C1O, O=P(O)(O)OCC1OC(OC2C(O)C(O)C(O)OC2CO)C(O)C(O)C1O>>OCC1OC(O)C(O)C(O)C1O]
3: R:M00002, P:M00004	[O=P(O)(O)OCC1OC(OC2C(O)C(O)C(O)OC2CO)C(O)C(O)C1O>>O=P(O)(O)OCC1OC(O)C(O)C(O)C1O, O=P(O)(O)OCC1OC(OC2C(O)C(O)C(O)OC2CO)C(O)C(O)C1O>>O=P(O)(O)OCC1OC(O)C(O)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH2:6][CH:7]1[O:8][CH:9]([O:10][CH:11]2[CH:12]([OH:13])[CH:14]([OH:15])[CH:16]([OH:17])[O:18][CH:19]2[CH2:20][OH:21])[CH:22]([OH:23])[CH:24]([OH:25])[CH:26]1[OH:27].[OH2:28]>>[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH2:6][CH:7]1[O:8][CH:9]([OH:10])[CH:22]([OH:23])[CH:24]([OH:25])[CH:26]1[OH:27].[OH:21][CH2:20][CH:19]1[O:18][CH:16]([OH:17])[CH:14]([OH:15])[CH:12]([OH:13])[CH:11]1[OH:28]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=28, 2=20, 3=19, 4=11, 5=12, 6=14, 7=16, 8=18, 9=17, 10=15, 11=13, 12=10, 13=9, 14=22, 15=24, 16=26, 17=7, 18=8, 19=6, 20=5, 21=2, 22=1, 23=3, 24=4, 25=27, 26=25, 27=23, 28=21}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=18, 2=19, 3=27, 4=25, 5=23, 6=21, 7=20, 8=22, 9=24, 10=26, 11=28, 12=17, 13=6, 14=7, 15=15, 16=13, 17=11, 18=9, 19=8, 20=10, 21=12, 22=14, 23=16, 24=5, 25=2, 26=1, 27=3, 28=4}

