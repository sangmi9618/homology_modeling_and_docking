
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O>>OC1O[CH][CH]C1O:1.0, O>>[CH]O:1.0, O>>[O]C([CH])O:1.0, OC1O[CH][CH]C1O:1.0, [CH]:2.0, [CH]O:1.0, [C]C1=[C]N=CN1:1.0, [C]C1=[C][N]C=N1:1.0, [C]C1=[C][N]C=N1>>[C]C1=[C]N=CN1:1.0, [C]N([CH])C1O[CH][CH]C1O:1.0, [C]N([CH])C1O[CH][CH]C1O>>OC1O[CH][CH]C1O:1.0, [C]N([CH])[CH]:1.0, [C]N([CH])[CH]>>[C]N=[CH]:1.0, [C]N=[CH]:2.0, [C]N=[CH]>>[C]N[CH]:1.0, [C]N[CH]:1.0, [C]c1ncn(c1N=[CH])C2O[CH][CH]C2O>>[C]c1[nH]cnc1N=[CH]:1.0, [NH]:1.0, [N]:3.0, [N]C([O])[CH]:1.0, [N]C([O])[CH]>>[O]C([CH])O:1.0, [N]C1=[C]N=CN1C([O])[CH]:1.0, [N]C1=[C]N=CN1C([O])[CH]>>[N]C1=[C]NC=N1:1.0, [N]C1=[C]N=CN1C2OC([CH2])C(O)C2O>>OC1OC([CH2])C(O)C1O:1.0, [N]C1=[C]NC=N1:1.0, [N]c1c(ncn1[CH])C(=[N])N>>[N]c1nc[nH]c1C(=[N])N:1.0, [OH]:1.0, [O]C([CH])O:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [CH]:2.0, [N]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, [CH]O:1.0, [C]N([CH])[CH]:1.0, [N]C([O])[CH]:1.0, [O]C([CH])O:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, OC1O[CH][CH]C1O:1.0, [C]N([CH])C1O[CH][CH]C1O:1.0, [N]C1=[C]N=CN1C([O])[CH]:1.0, [O]C([CH])O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (2)
[[N]C([O])[CH]:1.0, [O]C([CH])O:1.0]


ID=Reaction Center at Level: 2 (2)
[OC1O[CH][CH]C1O:1.0, [C]N([CH])C1O[CH][CH]C1O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [N]C([O])[CH]>>[O]C([CH])O
2: [C]N=[CH]>>[C]N[CH]
3: [C]N([CH])[CH]>>[C]N=[CH]
4: O>>[CH]O

MMP Level 2
1: [C]N([CH])C1O[CH][CH]C1O>>OC1O[CH][CH]C1O
2: [C]C1=[C][N]C=N1>>[C]C1=[C]N=CN1
3: [N]C1=[C]N=CN1C([O])[CH]>>[N]C1=[C]NC=N1
4: O>>[O]C([CH])O

MMP Level 3
1: [N]C1=[C]N=CN1C2OC([CH2])C(O)C2O>>OC1OC([CH2])C(O)C1O
2: [N]c1c(ncn1[CH])C(=[N])N>>[N]c1nc[nH]c1C(=[N])N
3: [C]c1ncn(c1N=[CH])C2O[CH][CH]C2O>>[C]c1[nH]cnc1N=[CH]
4: O>>OC1O[CH][CH]C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[OC1C(O)C(OC1n2cnc3c(ncnc32)N)CSC>>OC1OC(CSC)C(O)C1O]
2: R:M00001, P:M00004	[OC1C(O)C(OC1n2cnc3c(ncnc32)N)CSC>>n1cnc(N)c2[nH]cnc12, OC1C(O)C(OC1n2cnc3c(ncnc32)N)CSC>>n1cnc(N)c2[nH]cnc12]
3: R:M00002, P:M00003	[O>>OC1OC(CSC)C(O)C1O]


//
SELECTED AAM MAPPING
[OH2:21].[OH:1][CH:2]1[CH:3]([OH:4])[CH:5]([O:6][CH:7]1[N:8]:2:[CH:9]:[N:10]:[C:11]:3:[C:12](:[N:13]:[CH:14]:[N:15]:[C:16]32)[NH2:17])[CH2:18][S:19][CH3:20]>>[OH:21][CH:7]1[O:6][CH:5]([CH2:18][S:19][CH3:20])[CH:3]([OH:4])[CH:2]1[OH:1].[N:15]:1:[CH:14]:[N:13]:[C:12]([NH2:17]):[C:11]:2:[NH:10]:[CH:9]:[N:8]:[C:16]12


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=20, 2=19, 3=18, 4=5, 5=3, 6=2, 7=7, 8=6, 9=8, 10=9, 11=10, 12=11, 13=16, 14=15, 15=14, 16=13, 17=12, 18=17, 19=1, 20=4, 21=21}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=7, 2=6, 3=5, 4=4, 5=8, 6=10, 7=2, 8=3, 9=1, 10=11, 11=9, 12=19, 13=20, 14=21, 15=17, 16=18, 17=15, 18=14, 19=13, 20=12, 21=16}

