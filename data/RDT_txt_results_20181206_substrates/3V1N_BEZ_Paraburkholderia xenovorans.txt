
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0, C-O:1.0]

ORDER_CHANGED
[C-C*C=C:2.0]

//
FINGERPRINTS RC
[O:3.0, O=C(C(=C[CH])C=[CH])CC=[CH]>>O=C(O)C(=C[CH])C=[CH]:1.0, O=C(C([CH])=[CH])C[CH]:1.0, O=C(C([CH])=[CH])C[CH]>>O=C(O)C([CH])=[CH]:1.0, O=C(O)C([CH])=[CH]:1.0, O>>O=C(O)C([CH])=[CH]:1.0, O>>[C]C(=O)O:1.0, O>>[C]O:1.0, [CH2]:3.0, [CH2]C=C:2.0, [CH]:3.0, [CH]=C:1.0, [CH]=C[CH2]:1.0, [CH]=C[CH2]>>[CH2]C=C:1.0, [C]:2.0, [C]C(=O)C=CCC([C])=O>>[C]C(=O)CC=C:1.0, [C]C(=O)C=C[CH2]:1.0, [C]C(=O)C=C[CH2]>>[C]C(=O)CC=C:1.0, [C]C(=O)CC=C:1.0, [C]C(=O)CC=[CH]:1.0, [C]C(=O)CC=[CH]>>[CH2]C=C:1.0, [C]C(=O)O:2.0, [C]C(=O)[CH2]:1.0, [C]C(=O)[CH2]>>[C]C(=O)O:1.0, [C]C=CCC(=O)C([CH])=[CH]>>[C]CC=C:1.0, [C]C=CC[C]:1.0, [C]C=CC[C]>>[C]CC=C:1.0, [C]C=[CH]:1.0, [C]C=[CH]>>[C]C[CH]:1.0, [C]CC=C:1.0, [C]CC=CC(=O)C(=O)O>>O=C(O)C(=O)CC=C:1.0, [C]C[CH]:2.0, [C]C[CH]>>[CH]=C:1.0, [C]O:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [CH2]:1.0, [C]:2.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, [C]C(=O)O:1.0, [C]C(=O)[CH2]:1.0, [C]C[CH]:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=C(C([CH])=[CH])C[CH]:1.0, O=C(O)C([CH])=[CH]:1.0, [C]C(=O)CC=[CH]:1.0, [C]C(=O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (2)
[[CH2]:3.0, [CH]:3.0]


ID=Reaction Center at Level: 1 (5)
[[CH2]C=C:1.0, [CH]=C:1.0, [CH]=C[CH2]:1.0, [C]C=[CH]:1.0, [C]C[CH]:2.0]


ID=Reaction Center at Level: 2 (6)
[[CH2]C=C:1.0, [C]C(=O)C=C[CH2]:1.0, [C]C(=O)CC=C:1.0, [C]C(=O)CC=[CH]:1.0, [C]C=CC[C]:1.0, [C]CC=C:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C=[CH]>>[C]C[CH]
2: [C]C(=O)[CH2]>>[C]C(=O)O
3: [C]C[CH]>>[CH]=C
4: [CH]=C[CH2]>>[CH2]C=C
5: O>>[C]O

MMP Level 2
1: [C]C(=O)C=C[CH2]>>[C]C(=O)CC=C
2: O=C(C([CH])=[CH])C[CH]>>O=C(O)C([CH])=[CH]
3: [C]C(=O)CC=[CH]>>[CH2]C=C
4: [C]C=CC[C]>>[C]CC=C
5: O>>[C]C(=O)O

MMP Level 3
1: [C]CC=CC(=O)C(=O)O>>O=C(O)C(=O)CC=C
2: O=C(C(=C[CH])C=[CH])CC=[CH]>>O=C(O)C(=C[CH])C=[CH]
3: [C]C=CCC(=O)C([CH])=[CH]>>[C]CC=C
4: [C]C(=O)C=CCC([C])=O>>[C]C(=O)CC=C
5: O>>O=C(O)C([CH])=[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(=O)C=CCC(=O)c1ccccc1>>O=C(O)c1ccccc1]
2: R:M00001, P:M00004	[O=C(O)C(=O)C=CCC(=O)c1ccccc1>>O=C(O)C(=O)CC=C, O=C(O)C(=O)C=CCC(=O)c1ccccc1>>O=C(O)C(=O)CC=C, O=C(O)C(=O)C=CCC(=O)c1ccccc1>>O=C(O)C(=O)CC=C]
3: R:M00002, P:M00003	[O>>O=C(O)c1ccccc1]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[C:4](=[O:5])[CH:6]=[CH:7][CH2:8][C:9](=[O:10])[C:11]:1:[CH:12]:[CH:13]:[CH:14]:[CH:15]:[CH:16]1.[OH2:17]>>[O:1]=[C:2]([OH:3])[C:4](=[O:5])[CH2:6][CH:7]=[CH2:8].[O:10]=[C:9]([OH:17])[C:11]:1:[CH:12]:[CH:13]:[CH:14]:[CH:15]:[CH:16]1


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=14, 2=13, 3=12, 4=11, 5=16, 6=15, 7=9, 8=10, 9=8, 10=7, 11=6, 12=4, 13=5, 14=2, 15=1, 16=3, 17=17}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=7, 2=6, 3=5, 4=4, 5=9, 6=8, 7=2, 8=1, 9=3, 10=17, 11=16, 12=15, 13=13, 14=14, 15=11, 16=10, 17=12}

