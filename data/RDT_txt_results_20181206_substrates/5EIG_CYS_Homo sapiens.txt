
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-S:1.0, C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[N:3.0, O:3.0, O=C(O)C(=O)C:1.0, O=C(O)C(N)CS:1.0, O=C(O)C(N)CS>>N:1.0, O=C(O)C(N)CS>>O=C(O)C(=O)C:3.0, O>>O=C(O)C(=O)C:1.0, O>>[C]=O:1.0, O>>[C]C(=O)C:1.0, S:3.0, S[CH2]:1.0, S[CH2]>>S:1.0, [CH2]:1.0, [CH3]:1.0, [CH]:1.0, [CH]CS:2.0, [CH]CS>>S:1.0, [CH]CS>>[C]C:1.0, [CH]N:1.0, [CH]N>>N:1.0, [C]:1.0, [C]=O:1.0, [C]C:1.0, [C]C(=O)C:3.0, [C]C(N)CS:1.0, [C]C(N)CS>>S:1.0, [C]C(N)CS>>[C]C(=O)C:1.0, [C]C([CH2])N:2.0, [C]C([CH2])N>>N:1.0, [C]C([CH2])N>>[C]C(=O)C:1.0, [NH2]:1.0, [O]:1.0, [SH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (9)
[N:1.0, O:1.0, S:1.0, [CH2]:1.0, [CH]:1.0, [C]:1.0, [NH2]:1.0, [O]:1.0, [SH]:1.0]


ID=Reaction Center at Level: 1 (9)
[N:1.0, O:1.0, S:1.0, S[CH2]:1.0, [CH]CS:1.0, [CH]N:1.0, [C]=O:1.0, [C]C(=O)C:1.0, [C]C([CH2])N:1.0]


ID=Reaction Center at Level: 2 (9)
[N:1.0, O:1.0, O=C(O)C(=O)C:1.0, O=C(O)C(N)CS:1.0, S:1.0, [CH]CS:1.0, [C]C(=O)C:1.0, [C]C(N)CS:1.0, [C]C([CH2])N:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(=O)C:1.0, [C]C([CH2])N:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C(=O)C:1.0, O=C(O)C(N)CS:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C([CH2])N>>[C]C(=O)C
2: O>>[C]=O
3: S[CH2]>>S
4: [CH]CS>>[C]C
5: [CH]N>>N

MMP Level 2
1: O=C(O)C(N)CS>>O=C(O)C(=O)C
2: O>>[C]C(=O)C
3: [CH]CS>>S
4: [C]C(N)CS>>[C]C(=O)C
5: [C]C([CH2])N>>N

MMP Level 3
1: O=C(O)C(N)CS>>O=C(O)C(=O)C
2: O>>O=C(O)C(=O)C
3: [C]C(N)CS>>S
4: O=C(O)C(N)CS>>O=C(O)C(=O)C
5: O=C(O)C(N)CS>>N


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)CS>>S]
2: R:M00001, P:M00004	[O=C(O)C(N)CS>>O=C(O)C(=O)C, O=C(O)C(N)CS>>O=C(O)C(=O)C]
3: R:M00001, P:M00005	[O=C(O)C(N)CS>>N]
4: R:M00002, P:M00004	[O>>O=C(O)C(=O)C]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][SH:7].[OH2:8]>>[O:1]=[C:2]([OH:3])[C:4](=[O:8])[CH3:6].[SH2:7].[NH3:5]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=4, 3=2, 4=1, 5=3, 6=5, 7=7, 8=8}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=7, 2=6, 3=4, 4=5, 5=2, 6=1, 7=3, 8=8}

