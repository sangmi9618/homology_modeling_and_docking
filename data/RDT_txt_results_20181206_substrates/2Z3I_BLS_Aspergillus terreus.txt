
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C=O:1.0]

ORDER_CHANGED
[C%N*C%N:1.0]

//
FINGERPRINTS RC
[N:3.0, O:3.0, O=C([CH])[NH]:2.0, O=C1[N]C=CC(=N1)N>>O=C1[N]C=CC(=O)N1:1.0, O=c1nc(N)ccn1[CH]>>O=c1ccn([CH])c(=O)[nH]1:1.0, O>>O=C([CH])[NH]:1.0, O>>[C]=O:1.0, O>>[C]NC(=O)C=[CH]:1.0, [C]:2.0, [C]=O:1.0, [C]N:1.0, [C]N=C(N)C=[CH]:1.0, [C]N=C(N)C=[CH]>>N:1.0, [C]N=C(N)C=[CH]>>[C]NC(=O)C=[CH]:1.0, [C]N=[C]:1.0, [C]N=[C]>>[C]N[C]:1.0, [C]N>>N:1.0, [C]NC(=O)C=[CH]:1.0, [C]N[C]:1.0, [NH2]:1.0, [NH]:1.0, [N]:1.0, [N]=C([CH])N:2.0, [N]=C([CH])N>>N:1.0, [N]=C([CH])N>>O=C([CH])[NH]:1.0, [N]C(=O)N=C([CH])N:1.0, [N]C(=O)N=C([CH])N>>[N]C(=O)NC(=O)[CH]:1.0, [N]C(=O)NC(=O)[CH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[N:1.0, O:1.0, [C]:2.0, [NH2]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (6)
[N:1.0, O:1.0, O=C([CH])[NH]:1.0, [C]=O:1.0, [C]N:1.0, [N]=C([CH])N:1.0]


ID=Reaction Center at Level: 2 (6)
[N:1.0, O:1.0, O=C([CH])[NH]:1.0, [C]N=C(N)C=[CH]:1.0, [C]NC(=O)C=[CH]:1.0, [N]=C([CH])N:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [NH]:1.0, [N]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([CH])[NH]:1.0, [C]N=[C]:1.0, [C]N[C]:1.0, [N]=C([CH])N:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]N=C(N)C=[CH]:1.0, [C]NC(=O)C=[CH]:1.0, [N]C(=O)N=C([CH])N:1.0, [N]C(=O)NC(=O)[CH]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [N]=C([CH])N>>O=C([CH])[NH]
2: [C]N=[C]>>[C]N[C]
3: O>>[C]=O
4: [C]N>>N

MMP Level 2
1: [C]N=C(N)C=[CH]>>[C]NC(=O)C=[CH]
2: [N]C(=O)N=C([CH])N>>[N]C(=O)NC(=O)[CH]
3: O>>O=C([CH])[NH]
4: [N]=C([CH])N>>N

MMP Level 3
1: O=C1[N]C=CC(=N1)N>>O=C1[N]C=CC(=O)N1
2: O=c1nc(N)ccn1[CH]>>O=c1ccn([CH])c(=O)[nH]1
3: O>>[C]NC(=O)C=[CH]
4: [C]N=C(N)C=[CH]>>N


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1nc(N)ccn1C2OC(C(=O)O)C(C=C2)NC(=O)CC(N)CCN(C(=N)N)C>>O=c1ccn(c(=O)[nH]1)C2OC(C(=O)O)C(C=C2)NC(=O)CC(N)CCN(C(=N)N)C, O=c1nc(N)ccn1C2OC(C(=O)O)C(C=C2)NC(=O)CC(N)CCN(C(=N)N)C>>O=c1ccn(c(=O)[nH]1)C2OC(C(=O)O)C(C=C2)NC(=O)CC(N)CCN(C(=N)N)C]
2: R:M00001, P:M00004	[O=c1nc(N)ccn1C2OC(C(=O)O)C(C=C2)NC(=O)CC(N)CCN(C(=N)N)C>>N]
3: R:M00002, P:M00003	[O>>O=c1ccn(c(=O)[nH]1)C2OC(C(=O)O)C(C=C2)NC(=O)CC(N)CCN(C(=N)N)C]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[CH:6]=[CH:7][N:8]1[CH:9]2[O:10][CH:11]([C:12](=[O:13])[OH:14])[CH:15]([CH:16]=[CH:17]2)[NH:18][C:19](=[O:20])[CH2:21][CH:22]([NH2:23])[CH2:24][CH2:25][N:26]([C:27](=[NH:28])[NH2:29])[CH3:30].[OH2:31]>>[O:13]=[C:12]([OH:14])[CH:11]1[O:10][CH:9]([CH:17]=[CH:16][CH:15]1[NH:18][C:19](=[O:20])[CH2:21][CH:22]([NH2:23])[CH2:24][CH2:25][N:26]([C:27](=[NH:28])[NH2:29])[CH3:30])[N:8]2[CH:7]=[CH:6][C:4](=[O:31])[NH:3][C:2]2=[O:1].[NH3:5]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=30, 2=26, 3=25, 4=24, 5=22, 6=21, 7=19, 8=20, 9=18, 10=15, 11=16, 12=17, 13=9, 14=10, 15=11, 16=12, 17=13, 18=14, 19=8, 20=7, 21=6, 22=4, 23=3, 24=2, 25=1, 26=5, 27=23, 28=27, 29=28, 30=29, 31=31}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=30, 2=26, 3=25, 4=24, 5=22, 6=21, 7=19, 8=20, 9=18, 10=15, 11=16, 12=17, 13=9, 14=10, 15=11, 16=12, 17=13, 18=14, 19=5, 20=4, 21=3, 22=2, 23=1, 24=8, 25=6, 26=7, 27=23, 28=27, 29=28, 30=29, 31=31}

