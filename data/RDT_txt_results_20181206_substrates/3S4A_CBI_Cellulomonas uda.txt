
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:2.0]

STEREO_CHANGED
[C(R/S):3.0]

//
FINGERPRINTS RC
[O=P(O)(O)O:1.0, O=P(O)(O)O>>O=P(O)(O)OC(O[CH])C([CH])O:1.0, O=P(O)(O)O>>[O]C([CH])OP(=O)(O)O:1.0, OC1OC([CH2])[CH]C(O)C1O>>OC1OC([CH2])[CH]C(O)C1O:1.0, [CH]:6.0, [CH]C(O)C(O)C([CH])O:1.0, [CH]C([CH])O:3.0, [CH]C([CH])O>>[CH]C([CH])O:1.0, [CH]C([CH])OC1OC([CH2])[CH]C(O)C1O>>O=P(O)(O)OC1OC([CH2])[CH]C(O)C1O:1.0, [CH]O:1.0, [CH]OC(O)C([CH])O:2.0, [CH]OC(O)C([CH])O>>[CH]OC(O)C([CH])O:1.0, [CH]OC(O[CH])C([CH])O:1.0, [CH]OC(O[CH])C([CH])O>>[P]OC(O[CH])C([CH])O:1.0, [CH]OC1C(O)C(O)C(O)OC1[CH2]>>OC1OC([CH2])C(O)C(O)C1O:1.0, [CH]O[CH]:1.0, [CH]O[CH]>>[CH]O:1.0, [OH]:2.0, [O]:2.0, [O]C([CH2])C(OC(O[CH])C([CH])O)C([CH])O>>[O]C([CH2])C(O)C([CH])O:1.0, [O]C([CH])C(O)C([CH])O:1.0, [O]C([CH])C(O)C([CH])O>>[CH]C(O)C(O)C([CH])O:1.0, [O]C([CH])O:2.0, [O]C([CH])O>>[O]C([CH])O:1.0, [O]C([CH])OC([CH])[CH]:1.0, [O]C([CH])OC([CH])[CH]>>[CH]C([CH])O:1.0, [O]C([CH])OP(=O)(O)O:1.0, [O]C([O])[CH]:2.0, [O]C([O])[CH]>>[O]C([O])[CH]:1.0, [P]O:1.0, [P]O>>[P]O[CH]:1.0, [P]OC(O[CH])C([CH])O:1.0, [P]O[CH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (3)
[[CH]O[CH]:1.0, [O]C([O])[CH]:2.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[CH]OC(O[CH])C([CH])O:1.0, [O]C([CH])OC([CH])[CH]:1.0, [O]C([CH])OP(=O)(O)O:1.0, [P]OC(O[CH])C([CH])O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:6.0]


ID=Reaction Center at Level: 1 (3)
[[CH]C([CH])O:2.0, [O]C([CH])O:2.0, [O]C([O])[CH]:2.0]


ID=Reaction Center at Level: 2 (5)
[[CH]C(O)C(O)C([CH])O:1.0, [CH]OC(O)C([CH])O:2.0, [CH]OC(O[CH])C([CH])O:1.0, [O]C([CH])C(O)C([CH])O:1.0, [P]OC(O[CH])C([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [O]C([O])[CH]>>[O]C([O])[CH]
2: [CH]O[CH]>>[CH]O
3: [O]C([CH])O>>[O]C([CH])O
4: [P]O>>[P]O[CH]
5: [CH]C([CH])O>>[CH]C([CH])O

MMP Level 2
1: [CH]OC(O[CH])C([CH])O>>[P]OC(O[CH])C([CH])O
2: [O]C([CH])OC([CH])[CH]>>[CH]C([CH])O
3: [CH]OC(O)C([CH])O>>[CH]OC(O)C([CH])O
4: O=P(O)(O)O>>[O]C([CH])OP(=O)(O)O
5: [O]C([CH])C(O)C([CH])O>>[CH]C(O)C(O)C([CH])O

MMP Level 3
1: [CH]C([CH])OC1OC([CH2])[CH]C(O)C1O>>O=P(O)(O)OC1OC([CH2])[CH]C(O)C1O
2: [O]C([CH2])C(OC(O[CH])C([CH])O)C([CH])O>>[O]C([CH2])C(O)C([CH])O
3: OC1OC([CH2])[CH]C(O)C1O>>OC1OC([CH2])[CH]C(O)C1O
4: O=P(O)(O)O>>O=P(O)(O)OC(O[CH])C([CH])O
5: [CH]OC1C(O)C(O)C(O)OC1[CH2]>>OC1OC([CH2])C(O)C(O)C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[OCC1OC(OC2C(O)C(O)C(O)OC2CO)C(O)C(O)C1O>>O=P(O)(O)OC1OC(CO)C(O)C(O)C1O]
2: R:M00001, P:M00004	[OCC1OC(OC2C(O)C(O)C(O)OC2CO)C(O)C(O)C1O>>OCC1OC(O)C(O)C(O)C1O, OCC1OC(OC2C(O)C(O)C(O)OC2CO)C(O)C(O)C1O>>OCC1OC(O)C(O)C(O)C1O, OCC1OC(OC2C(O)C(O)C(O)OC2CO)C(O)C(O)C1O>>OCC1OC(O)C(O)C(O)C1O]
3: R:M00002, P:M00003	[O=P(O)(O)O>>O=P(O)(O)OC1OC(CO)C(O)C(O)C1O]


//
SELECTED AAM MAPPING
[O:24]=[P:25]([OH:26])([OH:27])[OH:28].[OH:1][CH2:2][CH:3]1[O:4][CH:5]([O:6][CH:7]2[CH:8]([OH:9])[CH:10]([OH:11])[CH:12]([OH:13])[O:14][CH:15]2[CH2:16][OH:17])[CH:18]([OH:19])[CH:20]([OH:21])[CH:22]1[OH:23]>>[O:24]=[P:25]([OH:27])([OH:26])[O:28][CH:5]1[O:4][CH:3]([CH2:2][OH:1])[CH:22]([OH:23])[CH:20]([OH:21])[CH:18]1[OH:19].[OH:17][CH2:16][CH:15]1[O:14][CH:12]([OH:13])[CH:10]([OH:11])[CH:8]([OH:9])[CH:7]1[OH:6]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=2, 2=3, 3=22, 4=20, 5=18, 6=5, 7=4, 8=6, 9=7, 10=15, 11=14, 12=12, 13=10, 14=8, 15=9, 16=11, 17=13, 18=16, 19=17, 20=19, 21=21, 22=23, 23=1, 24=26, 25=25, 26=24, 27=27, 28=28}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=11, 4=13, 5=15, 6=6, 7=7, 8=5, 9=2, 10=1, 11=3, 12=4, 13=16, 14=14, 15=12, 16=10, 17=18, 18=19, 19=27, 20=25, 21=23, 22=21, 23=20, 24=22, 25=24, 26=26, 27=28, 28=17}

