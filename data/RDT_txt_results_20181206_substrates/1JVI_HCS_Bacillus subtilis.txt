
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%O:1.0, C-S:1.0]

ORDER_CHANGED
[C%O*C=O:1.0, C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):4.0]

//
FINGERPRINTS RC
[O=C(C(=O)C(O)[CH2])C:1.0, O=C([CH])C(=O)C:1.0, OC1OC(CS[CH2])C(O)C1O>>O=C(C(=O)C(O)[CH2])C:1.0, OC1OC([CH2])C(O)C1O>>O=C(C(=O)C(O)CO)C:1.0, OC1OC([CH2])C(O)C1O>>[C]C(=O)C(O)CO:1.0, OC1OC([CH2])[CH][CH]1:1.0, OC1OC([CH2])[CH][CH]1>>[C]C(=O)C:1.0, OC1O[CH]C(O)C1O:1.0, OC1O[CH]C(O)C1O>>[C]C(=O)C(O)CO:1.0, OC1O[CH][CH]C1O:1.0, OC1O[CH][CH]C1O>>[C]C(O)CO:1.0, OC1[CH]OC([CH2])C1O:1.0, OC1[CH]OC([CH2])C1O>>O=C(C(=O)C(O)[CH2])C:2.0, OC1[CH][CH]OC1CSC[CH2]>>O=C([CH])C(=O)C:1.0, S([CH2])[CH2]:1.0, S([CH2])[CH2]>>S[CH2]:1.0, SC[CH2]:1.0, S[CH2]:1.0, [CH2]:2.0, [CH3]:1.0, [CH]:5.0, [CH]C([CH])O:3.0, [CH]C([CH])O>>[C]C(=O)[CH]:2.0, [CH]C([CH])O>>[C]C(O)[CH2]:1.0, [CH]CO:1.0, [CH]CSC[CH2]:1.0, [CH]CSC[CH2]>>SC[CH2]:1.0, [CH]O:1.0, [CH]O>>[C]=O:1.0, [CH]O[CH]:1.0, [CH]O[CH]>>[C]=O:1.0, [C]:2.0, [C]=O:2.0, [C]C:1.0, [C]C(=O)C:3.0, [C]C(=O)C(O)CO:1.0, [C]C(=O)[CH]:2.0, [C]C(O)CO:1.0, [C]C(O)[CH2]:1.0, [OH]:1.0, [O]:3.0, [O]C([CH])CSCC[CH]>>[CH]CCS:1.0, [O]C([CH])CS[CH2]:1.0, [O]C([CH])CS[CH2]>>[C]C(=O)C:1.0, [O]C([CH])O:1.0, [O]C([CH])O>>[CH]CO:1.0, [O]C([CH])[CH2]:1.0, [O]C([CH])[CH2]>>[C]C(=O)C:1.0, [SH]:1.0, [S]:1.0, [S]CC1OC(O)C(O)C1O>>O=C(C(=O)C(O)CO)C:1.0, [S]CC1OC(O)C(O)C1O>>O=C([CH])C(=O)C:1.0, [S]CC1O[CH][CH]C1O:1.0, [S]CC1O[CH][CH]C1O>>O=C([CH])C(=O)C:1.0, [S]C[CH]:1.0, [S]C[CH]>>[C]C:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[CH2]:1.0, [CH]:1.0, [O]:1.0, [S]:1.0]


ID=Reaction Center at Level: 1 (4)
[S([CH2])[CH2]:1.0, [CH]O[CH]:1.0, [O]C([CH])O:1.0, [S]C[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[OC1OC([CH2])[CH][CH]1:1.0, OC1O[CH][CH]C1O:1.0, [CH]CSC[CH2]:1.0, [O]C([CH])CS[CH2]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH]:2.0, [C]:2.0, [OH]:1.0, [O]:3.0]


ID=Reaction Center at Level: 1 (7)
[[CH]C([CH])O:1.0, [CH]O:1.0, [CH]O[CH]:1.0, [C]=O:2.0, [C]C(=O)C:1.0, [C]C(=O)[CH]:1.0, [O]C([CH])[CH2]:1.0]


ID=Reaction Center at Level: 2 (8)
[O=C(C(=O)C(O)[CH2])C:1.0, O=C([CH])C(=O)C:1.0, OC1OC([CH2])[CH][CH]1:1.0, OC1[CH]OC([CH2])C1O:1.0, [CH]C([CH])O:1.0, [C]C(=O)C:1.0, [C]C(=O)[CH]:1.0, [S]CC1O[CH][CH]C1O:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (3)
[[CH2]:1.0, [CH]:5.0, [C]:2.0]


ID=Reaction Center at Level: 1 (7)
[[CH]C([CH])O:2.0, [CH]CO:1.0, [C]C(=O)C:1.0, [C]C(=O)[CH]:1.0, [C]C(O)[CH2]:1.0, [O]C([CH])O:1.0, [O]C([CH])[CH2]:1.0]


ID=Reaction Center at Level: 2 (8)
[O=C(C(=O)C(O)[CH2])C:1.0, O=C([CH])C(=O)C:1.0, OC1O[CH]C(O)C1O:1.0, OC1O[CH][CH]C1O:1.0, OC1[CH]OC([CH2])C1O:1.0, [C]C(=O)C(O)CO:1.0, [C]C(O)CO:1.0, [S]CC1O[CH][CH]C1O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]C([CH])O>>[C]C(=O)[CH]
2: [CH]O>>[C]=O
3: [O]C([CH])O>>[CH]CO
4: [O]C([CH])[CH2]>>[C]C(=O)C
5: [CH]C([CH])O>>[C]C(O)[CH2]
6: [CH]O[CH]>>[C]=O
7: S([CH2])[CH2]>>S[CH2]
8: [S]C[CH]>>[C]C

MMP Level 2
1: OC1[CH]OC([CH2])C1O>>O=C(C(=O)C(O)[CH2])C
2: [CH]C([CH])O>>[C]C(=O)[CH]
3: OC1O[CH][CH]C1O>>[C]C(O)CO
4: [S]CC1O[CH][CH]C1O>>O=C([CH])C(=O)C
5: OC1O[CH]C(O)C1O>>[C]C(=O)C(O)CO
6: OC1OC([CH2])[CH][CH]1>>[C]C(=O)C
7: [CH]CSC[CH2]>>SC[CH2]
8: [O]C([CH])CS[CH2]>>[C]C(=O)C

MMP Level 3
1: [S]CC1OC(O)C(O)C1O>>O=C(C(=O)C(O)CO)C
2: OC1[CH]OC([CH2])C1O>>O=C(C(=O)C(O)[CH2])C
3: OC1OC([CH2])C(O)C1O>>[C]C(=O)C(O)CO
4: OC1OC(CS[CH2])C(O)C1O>>O=C(C(=O)C(O)[CH2])C
5: OC1OC([CH2])C(O)C1O>>O=C(C(=O)C(O)CO)C
6: [S]CC1OC(O)C(O)C1O>>O=C([CH])C(=O)C
7: [O]C([CH])CSCC[CH]>>[CH]CCS
8: OC1[CH][CH]OC1CSC[CH2]>>O=C([CH])C(=O)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)C(N)CCSCC1OC(O)C(O)C1O>>O=C(O)C(N)CCS]
2: R:M00001, P:M00003	[O=C(O)C(N)CCSCC1OC(O)C(O)C1O>>O=C(C(=O)C(O)CO)C, O=C(O)C(N)CCSCC1OC(O)C(O)C1O>>O=C(C(=O)C(O)CO)C, O=C(O)C(N)CCSCC1OC(O)C(O)C1O>>O=C(C(=O)C(O)CO)C, O=C(O)C(N)CCSCC1OC(O)C(O)C1O>>O=C(C(=O)C(O)CO)C, O=C(O)C(N)CCSCC1OC(O)C(O)C1O>>O=C(C(=O)C(O)CO)C, O=C(O)C(N)CCSCC1OC(O)C(O)C1O>>O=C(C(=O)C(O)CO)C, O=C(O)C(N)CCSCC1OC(O)C(O)C1O>>O=C(C(=O)C(O)CO)C]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][S:8][CH2:9][CH:10]1[O:11][CH:12]([OH:13])[CH:14]([OH:15])[CH:16]1[OH:17]>>[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][SH:8].[O:11]=[C:10]([C:16](=[O:17])[CH:14]([OH:15])[CH2:12][OH:13])[CH3:9]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=8, 4=9, 5=10, 6=16, 7=14, 8=12, 9=11, 10=13, 11=15, 12=17, 13=4, 14=2, 15=1, 16=3, 17=5}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=15, 2=16, 3=17, 4=13, 5=11, 6=10, 7=12, 8=14, 9=9, 10=2, 11=1, 12=3, 13=4, 14=5, 15=7, 16=8, 17=6}

