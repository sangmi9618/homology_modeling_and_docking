
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0, C-N:1.0, C-O:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

//
FINGERPRINTS RC
[O=C(O)c1ncccc1C(=O)O>>[O]C([CH])[n+]1cccc(c1)C(=O)O:1.0, O=C=O:3.0, O=P(O)(O[P])OC1O[CH][CH]C1O>>O=P(O)(O)O[P]:1.0, [CH]:3.0, [CH][N+](=[CH])C1O[CH][CH]C1O:1.0, [CH][N+]([CH])=[CH]:1.0, [C]:3.0, [C]=C([N])C(=O)O:1.0, [C]=C([N])C(=O)O>>O=C=O:2.0, [C]=C[N+]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=O:1.0, [C]C(=O)O:2.0, [C]C(=O)O>>O=C=O:2.0, [C]C(=[C])N=C[CH]:1.0, [C]C(=[C])N=C[CH]>>[C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]C(=[C])[N]:1.0, [C]C(=[C])[N]>>[C]=C[N+]:1.0, [C]C([CH])=C(N=[CH])C(=O)O:1.0, [C]C([CH])=C(N=[CH])C(=O)O>>O=C=O:1.0, [C]C([CH])=C(N=[CH])C(=O)O>>[C]C([CH])=C[N+]([CH])=[CH]:1.0, [C]C([CH])=C[N+]([CH])=[CH]:1.0, [C]N=[CH]:1.0, [C]N=[CH]>>[CH][N+]([CH])=[CH]:1.0, [C]O:1.0, [C]O>>[C]=O:1.0, [C]c1cccnc1C(=O)O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O:1.0, [N+]:1.0, [N+]C([O])[CH]:1.0, [N]:1.0, [OH]:2.0, [O]:2.0, [O]C([CH])OP([O])(=O)O:1.0, [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]C([O])[CH]:1.0, [O]C([O])[CH]>>[N+]C([O])[CH]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OC1OC([CH2])C(O)C1O>>[C]=C[N+](=C[CH])C1OC([CH2])C(O)C1O:1.0, [P]O:1.0, [P]OC1O[CH][CH]C1O:1.0, [P]OC1O[CH][CH]C1O>>[CH][N+](=[CH])C1O[CH][CH]C1O:1.0, [P]O[CH]:1.0, [P]O[CH]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[CH]:2.0, [C]:2.0, [N+]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (6)
[[CH][N+]([CH])=[CH]:1.0, [C]C(=O)O:1.0, [C]C(=[C])[N]:1.0, [N+]C([O])[CH]:1.0, [O]C([O])[CH]:1.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (6)
[[CH][N+](=[CH])C1O[CH][CH]C1O:1.0, [C]=C([N])C(=O)O:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]C([CH])=C(N=[CH])C(=O)O:1.0, [O]C([CH])OP([O])(=O)O:1.0, [P]OC1O[CH][CH]C1O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C=O:1.0, [C]=O:1.0, [C]C(=O)O:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (3)
[O=C=O:2.0, [C]=C([N])C(=O)O:1.0, [C]C(=O)O:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]C([O])[CH]>>[N+]C([O])[CH]
2: [C]O>>[C]=O
3: [C]N=[CH]>>[CH][N+]([CH])=[CH]
4: [C]C(=O)O>>O=C=O
5: [C]C(=[C])[N]>>[C]=C[N+]
6: [P]O[CH]>>[P]O

MMP Level 2
1: [P]OC1O[CH][CH]C1O>>[CH][N+](=[CH])C1O[CH][CH]C1O
2: [C]C(=O)O>>O=C=O
3: [C]C(=[C])N=C[CH]>>[C]=C[N+](=C[CH])C([O])[CH]
4: [C]=C([N])C(=O)O>>O=C=O
5: [C]C([CH])=C(N=[CH])C(=O)O>>[C]C([CH])=C[N+]([CH])=[CH]
6: [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O

MMP Level 3
1: [O]P(=O)(O)OC1OC([CH2])C(O)C1O>>[C]=C[N+](=C[CH])C1OC([CH2])C(O)C1O
2: [C]=C([N])C(=O)O>>O=C=O
3: [C]c1cccnc1C(=O)O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O
4: [C]C([CH])=C(N=[CH])C(=O)O>>O=C=O
5: O=C(O)c1ncccc1C(=O)O>>[O]C([CH])[n+]1cccc(c1)C(=O)O
6: O=P(O)(O[P])OC1O[CH][CH]C1O>>O=P(O)(O)O[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)c1ncccc1C(=O)O>>O=C(O)c1ccc[n+](c1)C2OC(COP(=O)(O)O)C(O)C2O, O=C(O)c1ncccc1C(=O)O>>O=C(O)c1ccc[n+](c1)C2OC(COP(=O)(O)O)C(O)C2O]
2: R:M00001, P:M00005	[O=C(O)c1ncccc1C(=O)O>>O=C=O, O=C(O)c1ncccc1C(=O)O>>O=C=O]
3: R:M00002, P:M00003	[O=P(O)(O)OCC1OC(OP(=O)(O)OP(=O)(O)O)C(O)C1O>>O=C(O)c1ccc[n+](c1)C2OC(COP(=O)(O)O)C(O)C2O]
4: R:M00002, P:M00004	[O=P(O)(O)OCC1OC(OP(=O)(O)OP(=O)(O)O)C(O)C1O>>O=P(O)(O)OP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:23]=[C:24]([OH:25])[C:26]:1:[N:27]:[CH:28]:[CH:29]:[CH:30]:[C:31]1[C:32](=[O:33])[OH:34].[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH2:6][CH:7]1[O:8][CH:9]([O:10][P:11](=[O:12])([OH:13])[O:14][P:15](=[O:16])([OH:17])[OH:18])[CH:19]([OH:20])[CH:21]1[OH:22]>>[O:23]=[C:24]=[O:25].[O:33]=[C:32]([OH:34])[C:31]:1:[CH:30]:[CH:29]:[CH:28]:[N+:27](:[CH:26]1)[CH:9]2[O:8][CH:7]([CH2:6][O:5][P:2](=[O:1])([OH:4])[OH:3])[CH:21]([OH:22])[CH:19]2[OH:20].[O:12]=[P:11]([OH:10])([OH:13])[O:14][P:15](=[O:16])([OH:17])[OH:18]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=29, 2=30, 3=31, 4=26, 5=27, 6=28, 7=24, 8=23, 9=25, 10=32, 11=33, 12=34, 13=6, 14=7, 15=21, 16=19, 17=9, 18=8, 19=10, 20=11, 21=12, 22=13, 23=14, 24=15, 25=16, 26=17, 27=18, 28=20, 29=22, 30=5, 31=2, 32=1, 33=3, 34=4}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=5, 3=4, 4=9, 5=8, 6=7, 7=10, 8=21, 9=19, 10=12, 11=11, 12=13, 13=14, 14=15, 15=16, 16=17, 17=18, 18=20, 19=22, 20=2, 21=1, 22=3, 23=25, 24=24, 25=23, 26=26, 27=27, 28=28, 29=29, 30=30, 31=31, 32=33, 33=32, 34=34}

