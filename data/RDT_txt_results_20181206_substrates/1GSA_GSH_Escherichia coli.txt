
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0, O-P:2.0]

//
FINGERPRINTS RC
[O=C(N[CH2])C([NH])[CH2]:1.0, O=C(O)C([NH])[CH2]:1.0, O=C(O)C([NH])[CH2]>>O=C(N[CH2])C([NH])[CH2]:1.0, O=C(O)C([NH])[CH2]>>O=P(O)(O)O:1.0, O=C(O)CN>>O=C(O)CNC(=O)C([NH])[CH2]:1.0, O=C([CH])O:2.0, O=C([CH])O>>O=C([CH])[NH]:1.0, O=C([CH])O>>O=P(O)(O)O:1.0, O=C([CH])[NH]:1.0, O=P(O)(O)O:3.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O:1.0, [CH2]N:1.0, [CH2]N>>[C]N[CH2]:1.0, [C]:2.0, [C]CN:1.0, [C]CN>>[C]CNC(=O)[CH]:1.0, [C]CNC(=O)[CH]:1.0, [C]NC(C(=O)O)CS>>[C]NC(C(=O)NC[C])CS:1.0, [C]N[CH2]:1.0, [C]O:1.0, [C]O>>[P]O:1.0, [NH2]:1.0, [NH]:1.0, [OH]:3.0, [O]:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[C]:2.0, [NH]:1.0, [OH]:2.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (8)
[O=C([CH])O:1.0, O=C([CH])[NH]:1.0, O=P(O)(O)O:1.0, [C]N[CH2]:1.0, [C]O:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (7)
[O=C(N[CH2])C([NH])[CH2]:1.0, O=C(O)C([NH])[CH2]:1.0, O=C([CH])O:1.0, O=P(O)(O)O:2.0, O=P(O)(O)O[P]:1.0, [C]CNC(=O)[CH]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]P(=O)(O)O>>O=P(O)(O)O
2: [P]O[P]>>[P]O
3: O=C([CH])O>>O=C([CH])[NH]
4: [CH2]N>>[C]N[CH2]
5: [C]O>>[P]O

MMP Level 2
1: O=P(O)(O)O[P]>>O=P(O)(O)O
2: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
3: O=C(O)C([NH])[CH2]>>O=C(N[CH2])C([NH])[CH2]
4: [C]CN>>[C]CNC(=O)[CH]
5: O=C([CH])O>>O=P(O)(O)O

MMP Level 3
1: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O
2: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
3: [C]NC(C(=O)O)CS>>[C]NC(C(=O)NC[C])CS
4: O=C(O)CN>>O=C(O)CNC(=O)C([NH])[CH2]
5: O=C(O)C([NH])[CH2]>>O=P(O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00005	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)O]
3: R:M00002, P:M00006	[O=C(O)CN>>O=C(O)CNC(=O)C(NC(=O)CCC(N)C(=O)O)CS]
4: R:M00003, P:M00005	[O=C(O)C(N)CCC(=O)NC(C(=O)O)CS>>O=P(O)(O)O]
5: R:M00003, P:M00006	[O=C(O)C(N)CCC(=O)NC(C(=O)O)CS>>O=C(O)CNC(=O)C(NC(=O)CCC(N)C(=O)O)CS]


//
SELECTED AAM MAPPING
[O:48]=[C:49]([OH:50])[CH2:51][NH2:52].[O:32]=[C:33]([OH:34])[CH:35]([NH2:36])[CH2:37][CH2:38][C:39](=[O:40])[NH:41][CH:42]([C:43](=[O:44])[OH:45])[CH2:46][SH:47].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:48]=[C:49]([OH:50])[CH2:51][NH:52][C:43](=[O:44])[CH:42]([NH:41][C:39](=[O:40])[CH2:38][CH2:37][CH:35]([NH2:36])[C:33](=[O:32])[OH:34])[CH2:46][SH:47].[O:1]=[P:2]([OH:3])([OH:4])[OH:45].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=51, 33=49, 34=48, 35=50, 36=52, 37=37, 38=38, 39=39, 40=40, 41=41, 42=42, 43=46, 44=47, 45=43, 46=44, 47=45, 48=35, 49=33, 50=32, 51=34, 52=36}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=17, 5=18, 6=19, 7=23, 8=16, 9=15, 10=14, 11=13, 12=24, 13=26, 14=11, 15=12, 16=10, 17=9, 18=6, 19=7, 20=8, 21=5, 22=2, 23=1, 24=3, 25=4, 26=27, 27=25, 28=50, 29=49, 30=48, 31=51, 32=52, 33=40, 34=39, 35=37, 36=38, 37=36, 38=35, 39=46, 40=47, 41=33, 42=34, 43=32, 44=31, 45=29, 46=28, 47=30, 48=41, 49=43, 50=44, 51=45, 52=42}

