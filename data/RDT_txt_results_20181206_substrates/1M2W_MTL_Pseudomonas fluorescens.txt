
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%O:1.0]

STEREO_CHANGED
[C(R/S):3.0]

//
FINGERPRINTS RC
[O=C(N)C=1C=[N+]C=CC1>>O=C(N)C1=C[N]C=CC1:1.0, OC1(OC([CH2])[CH][CH]1)[CH2]:1.0, OC1[CH]OC(O)([CH2])C1O:1.0, OC1[C]OC([CH2])C1O:1.0, OCC(O)C(O)C(O)C(O)[CH2]>>OCC1(O)OC([CH2])C(O)C1O:1.0, OCC(O)C(O)C(O)C(O)[CH2]>>OCC1OC(O)([CH2])C(O)C1O:1.0, OCC1(O)O[CH][CH]C1O:1.0, [CH2]:1.0, [CH]:6.0, [CH]C(O)C(O)C(O)CO>>OCC1(O)OC([CH2])C(O)C1O:1.0, [CH]C(O)C(O)C(O)[CH2]:2.0, [CH]C(O)C(O)C(O)[CH2]>>OC1[CH]OC(O)([CH2])C1O:1.0, [CH]C(O)C(O)C(O)[CH2]>>OC1[C]OC([CH2])C1O:1.0, [CH]C(O)C(O)CO:1.0, [CH]C(O)C(O)CO>>OCC1(O)O[CH][CH]C1O:1.0, [CH]C(O)C(O)CO>>OCC1OC(O)(CO)C(O)C1O:1.0, [CH]C(O)[CH2]:2.0, [CH]C(O)[CH2]>>OC1(OC([CH2])[CH][CH]1)[CH2]:1.0, [CH]C(O)[CH2]>>[O]C([CH])(O)[CH2]:1.0, [CH]C([CH])O:3.0, [CH]C([CH])O>>[CH]C([CH])O:1.0, [CH]C([CH])O>>[C]C([CH])O:1.0, [CH]N([CH])[CH]:1.0, [CH]O:1.0, [CH]O>>[C]O[CH]:1.0, [CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]>>[CH]N([CH])[CH]:1.0, [C]:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN(C=[CH])C([O])[CH]:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])C=C[CH]>>[C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C([CH])O:1.0, [C]C=[CH]:1.0, [C]C=[CH]>>[C]C[CH]:1.0, [C]C[CH]:1.0, [C]O[CH]:1.0, [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]C1=CN(C=CC1)C2O[CH][CH]C2O:1.0, [N+]:1.0, [N]:1.0, [OH]:1.0, [O]:1.0, [O]C([CH])(O)[CH2]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[C]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]O[CH]:1.0, [O]C([CH])(O)[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[OC1(OC([CH2])[CH][CH]1)[CH2]:1.0, OCC1(O)O[CH][CH]C1O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:5.0, [C]:1.0]


ID=Reaction Center at Level: 1 (4)
[[CH]C(O)[CH2]:1.0, [CH]C([CH])O:3.0, [C]C([CH])O:1.0, [O]C([CH])(O)[CH2]:1.0]


ID=Reaction Center at Level: 2 (5)
[OC1[CH]OC(O)([CH2])C1O:1.0, OC1[C]OC([CH2])C1O:1.0, OCC1(O)O[CH][CH]C1O:1.0, [CH]C(O)C(O)C(O)[CH2]:2.0, [CH]C(O)C(O)CO:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]C(O)[CH2]>>[O]C([CH])(O)[CH2]
2: [CH]C([CH])O>>[C]C([CH])O
3: [CH]O>>[C]O[CH]
4: [C]C=[CH]>>[C]C[CH]
5: [CH][N+]([CH])=[CH]>>[CH]N([CH])[CH]
6: [CH]C([CH])O>>[CH]C([CH])O

MMP Level 2
1: [CH]C(O)C(O)CO>>OCC1(O)O[CH][CH]C1O
2: [CH]C(O)C(O)C(O)[CH2]>>OC1[CH]OC(O)([CH2])C1O
3: [CH]C(O)[CH2]>>OC1(OC([CH2])[CH][CH]1)[CH2]
4: [C]C(=[CH])C=C[CH]>>[C]C(=[CH])CC=[CH]
5: [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN(C=[CH])C([O])[CH]
6: [CH]C(O)C(O)C(O)[CH2]>>OC1[C]OC([CH2])C1O

MMP Level 3
1: [CH]C(O)C(O)C(O)CO>>OCC1(O)OC([CH2])C(O)C1O
2: OCC(O)C(O)C(O)C(O)[CH2]>>OCC1(O)OC([CH2])C(O)C1O
3: [CH]C(O)C(O)CO>>OCC1OC(O)(CO)C(O)C1O
4: O=C(N)C=1C=[N+]C=CC1>>O=C(N)C1=C[N]C=CC1
5: [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]C1=CN(C=CC1)C2O[CH][CH]C2O
6: OCC(O)C(O)C(O)C(O)[CH2]>>OCC1OC(O)([CH2])C(O)C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[OCC(O)C(O)C(O)C(O)CO>>OCC1OC(O)(CO)C(O)C1O, OCC(O)C(O)C(O)C(O)CO>>OCC1OC(O)(CO)C(O)C1O, OCC(O)C(O)C(O)C(O)CO>>OCC1OC(O)(CO)C(O)C1O, OCC(O)C(O)C(O)C(O)CO>>OCC1OC(O)(CO)C(O)C1O]
2: R:M00002, P:M00004	[O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O, O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:5]:[CH:6]:[CH:7]:[N+:8](:[CH:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44].[OH:45][CH2:46][CH:47]([OH:48])[CH:49]([OH:50])[CH:51]([OH:52])[CH:53]([OH:54])[CH2:55][OH:56]>>[H+:57].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:9][N:8]([CH:7]=[CH:6][CH2:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44].[OH:45][CH2:46][CH:47]1[O:48][C:53]([OH:54])([CH2:55][OH:56])[CH:51]([OH:52])[CH:49]1[OH:50]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=55, 2=53, 3=51, 4=49, 5=47, 6=46, 7=45, 8=48, 9=50, 10=52, 11=54, 12=56, 13=6, 14=5, 15=4, 16=9, 17=8, 18=7, 19=10, 20=43, 21=41, 22=12, 23=11, 24=13, 25=14, 26=15, 27=16, 28=17, 29=18, 30=19, 31=20, 32=21, 33=22, 34=23, 35=24, 36=39, 37=37, 38=26, 39=25, 40=27, 41=28, 42=29, 43=30, 44=35, 45=34, 46=33, 47=32, 48=31, 49=36, 50=38, 51=40, 52=42, 53=44, 54=2, 55=1, 56=3}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=46, 2=47, 3=55, 4=53, 5=49, 6=48, 7=51, 8=52, 9=50, 10=54, 11=56, 12=45, 13=9, 14=8, 15=7, 16=6, 17=5, 18=4, 19=2, 20=1, 21=3, 22=10, 23=43, 24=41, 25=12, 26=11, 27=13, 28=14, 29=15, 30=16, 31=17, 32=18, 33=19, 34=20, 35=21, 36=22, 37=23, 38=24, 39=39, 40=37, 41=26, 42=25, 43=27, 44=28, 45=29, 46=30, 47=35, 48=34, 49=33, 50=32, 51=31, 52=36, 53=38, 54=40, 55=42, 56=44, 57=57}

