
//
FINGERPRINTS BC

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(O)C(N)C[CH2]:2.0, O=C(O)C(N)C[CH2]>>O=C(O)C(N)C[CH2]:1.0, [CH]:2.0, [C]C([CH2])N:2.0, [C]C([CH2])N>>[C]C([CH2])N:1.0, [C]CCC(N)C(=O)O>>[C]CCC(N)C(=O)O:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (1)
[[C]C([CH2])N:2.0]


ID=Reaction Center at Level: 2 (1)
[O=C(O)C(N)C[CH2]:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C([CH2])N>>[C]C([CH2])N

MMP Level 2
1: O=C(O)C(N)C[CH2]>>O=C(O)C(N)C[CH2]

MMP Level 3
1: [C]CCC(N)C(=O)O>>[C]CCC(N)C(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)CCC(N)C(=O)O>>O=C(O)CCC(N)C(=O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH2:4][CH2:5][CH:6]([NH2:7])[C:8](=[O:9])[OH:10]>>[O:1]=[C:2]([OH:3])[CH2:4][CH2:5][CH:6]([NH2:7])[C:8](=[O:9])[OH:10]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=5, 2=4, 3=2, 4=1, 5=3, 6=6, 7=8, 8=9, 9=10, 10=7}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=5, 2=4, 3=2, 4=1, 5=3, 6=6, 7=8, 8=9, 9=10, 10=7}

