
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[CH2]:1.0, O=P(O)(O[P])O[CH2]>>O=P(O)(O[S])O[CH2]:1.0, O=P(O)(O[S])O[CH2]:1.0, [O-]:2.0, [O-]S([O-])(=O)=O:2.0, [O-]S([O-])(=O)=O>>O=P(O)(O[CH2])OS(=O)(=O)O:1.0, [O-]S([O-])(=O)=O>>O=S(=O)(O)O[P]:1.0, [O-]S([O-])(=O)=O>>[O]P(=O)(O)OS(=O)(=O)O:1.0, [O-]S([O-])(=O)=O>>[O]S(=O)(=O)O:1.0, [O-][S]:2.0, [O-][S]>>[P]O[S]:1.0, [O-][S]>>[S]O:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)OC[CH]>>O=P(O)(OC[CH])OS(=O)(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OS(=O)(=O)O:1.0, [O]P([O])(=O)O:2.0, [O]P([O])(=O)O>>[O]P([O])(=O)O:1.0, [O]S(=O)(=O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0, [P]O[S]:1.0, [S]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[O]P([O])(=O)O:2.0, [P]O[P]:1.0, [P]O[S]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O[P])O[CH2]:1.0, O=P(O)(O[S])O[CH2]:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OS(=O)(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[P]>>[P]O
2: [O]P([O])(=O)O>>[O]P([O])(=O)O
3: [O-][S]>>[P]O[S]
4: [O-][S]>>[S]O

MMP Level 2
1: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
2: O=P(O)(O[P])O[CH2]>>O=P(O)(O[S])O[CH2]
3: [O-]S([O-])(=O)=O>>[O]P(=O)(O)OS(=O)(=O)O
4: [O-]S([O-])(=O)=O>>[O]S(=O)(=O)O

MMP Level 3
1: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]
2: [O]P(=O)(O)OP(=O)(O)OC[CH]>>O=P(O)(OC[CH])OS(=O)(=O)O
3: [O-]S([O-])(=O)=O>>O=P(O)(O[CH2])OS(=O)(=O)O
4: [O-]S([O-])(=O)=O>>O=S(=O)(O)O[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O)OS(=O)(=O)O]
3: R:M00002, P:M00004	[[O-]S([O-])(=O)=O>>O=P(O)(OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O)OS(=O)(=O)O, [O-]S([O-])(=O)=O>>O=P(O)(OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O)OS(=O)(=O)O]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31].[O:32]=[S:33](=[O:34])([O-:35])[O-:36]>>[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[OH:9].[O:11]=[P:10]([OH:12])([O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31])[O:35][S:33](=[O:34])(=[O:32])[OH:36]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=35, 33=33, 34=32, 35=34, 36=36}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=30, 2=29, 3=28, 4=31, 5=32, 6=33, 7=34, 8=35, 9=36, 10=15, 11=16, 12=17, 13=12, 14=13, 15=14, 16=18, 17=11, 18=10, 19=9, 20=8, 21=19, 22=21, 23=6, 24=7, 25=5, 26=4, 27=2, 28=1, 29=3, 30=23, 31=24, 32=25, 33=26, 34=27, 35=22, 36=20}

