
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)O[CH2]:1.0, O=P(O)(O[CH2])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]:1.0, O=P(O)(O[P])O[CH2]:1.0, O=P(O)(O[P])O[CH2]>>O=P(O)(O)O[CH2]:1.0, [C]COP(=O)(O)OP([O])(=O)O>>[C]COP(=O)(O)O:1.0, [OH]:1.0, [O]:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [O]P([O])(=O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (4)
[[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)O[CH2]:1.0, O=P(O)(O[P])O[CH2]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]P([O])(=O)O>>[O]P(=O)(O)O
2: [P]O[P]>>[P]O

MMP Level 2
1: O=P(O)(O[P])O[CH2]>>O=P(O)(O)O[CH2]
2: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O

MMP Level 3
1: [C]COP(=O)(O)OP([O])(=O)O>>[C]COP(=O)(O)O
2: O=P(O)(O[CH2])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(NCCS)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O>>O=P(O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O]
2: R:M00001, P:M00003	[O=C(NCCS)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O>>O=C(NCCS)CCNC(=O)C(O)C(C)(C)COP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([NH:3][CH2:4][CH2:5][SH:6])[CH2:7][CH2:8][NH:9][C:10](=[O:11])[CH:12]([OH:13])[C:14]([CH3:15])([CH3:16])[CH2:17][O:18][P:19](=[O:20])([OH:21])[O:22][P:23](=[O:24])([OH:25])[O:26][CH2:27][CH:28]1[O:29][CH:30]([N:31]:2:[CH:32]:[N:33]:[C:34]:3:[C:35](:[N:36]:[CH:37]:[N:38]:[C:39]32)[NH2:40])[CH:41]([OH:42])[CH:43]1[O:44][P:45](=[O:46])([OH:47])[OH:48]>>[O:1]=[C:2]([NH:3][CH2:4][CH2:5][SH:6])[CH2:7][CH2:8][NH:9][C:10](=[O:11])[CH:12]([OH:13])[C:14]([CH3:15])([CH3:16])[CH2:17][O:18][P:19](=[O:20])([OH:21])[OH:49].[O:24]=[P:23]([OH:22])([OH:25])[O:26][CH2:27][CH:28]1[O:29][CH:30]([N:31]:2:[CH:32]:[N:33]:[C:34]:3:[C:35](:[N:36]:[CH:37]:[N:38]:[C:39]32)[NH2:40])[CH:41]([OH:42])[CH:43]1[O:44][P:45](=[O:46])([OH:47])[OH:48]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=15, 2=14, 3=16, 4=17, 5=18, 6=19, 7=20, 8=21, 9=22, 10=23, 11=24, 12=25, 13=26, 14=27, 15=28, 16=43, 17=41, 18=30, 19=29, 20=31, 21=32, 22=33, 23=34, 24=39, 25=38, 26=37, 27=36, 28=35, 29=40, 30=42, 31=44, 32=45, 33=46, 34=47, 35=48, 36=12, 37=10, 38=11, 39=9, 40=8, 41=7, 42=2, 43=1, 44=3, 45=4, 46=5, 47=6, 48=13}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=16, 2=17, 3=18, 4=13, 5=14, 6=15, 7=19, 8=12, 9=11, 10=10, 11=9, 12=20, 13=22, 14=7, 15=8, 16=6, 17=5, 18=2, 19=1, 20=3, 21=4, 22=23, 23=24, 24=25, 25=26, 26=27, 27=21, 28=42, 29=41, 30=43, 31=44, 32=45, 33=46, 34=47, 35=48, 36=49, 37=39, 38=37, 39=38, 40=36, 41=35, 42=34, 43=29, 44=28, 45=30, 46=31, 47=32, 48=33, 49=40}

