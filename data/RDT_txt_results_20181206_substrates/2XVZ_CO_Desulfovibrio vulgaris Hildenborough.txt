
//
FINGERPRINTS BC

//
FINGERPRINTS RC
[[CH]=C1[C]=[C]C(=[CH])N1:1.0, [CH]=C1[C]=[C]C(=[CH])N1>>[CH]C=1[C]=[C]C(=[CH])N1:1.0, [CH]=C1[C][CH]C(=[CH])[N-]1:1.0, [CH]C1=NC(=[CH])[C][CH]1:1.0, [CH]C1=NC(=[CH])[C][CH]1>>[CH]=C1[C][CH]C(=[CH])[N-]1:1.0, [CH]C1=[C][C]=C([CH])N1:1.0, [CH]C1=[C][C]=C([CH])N1>>[CH]C1=[C][C]=C([CH])[N-]1:1.0, [CH]C1=[C][C]=C([CH])[N-]1:1.0, [CH]C=1[C]=[C]C(=[CH])N1:1.0, [C]=Cc1[nH]c(C=[C])c([CH2])c1[CH2]>>[C]=Cc1[n-]c(C=[C])c([CH2])c1[CH2]:1.0, [C]C=C1N=C(C=[C])C([CH2])C1([CH2])C>>[C]C=C1[N-]C(=C[C])C([CH2])(C)C1[CH2]:1.0, [C]C=c1[nH]c(=C[C])c([CH2])c1[CH2]>>[C]C=C1N=C(C=[C])C([CH2])=C1[CH2]:1.0, [C]N=[C]:2.0, [C]N=[C]>>[C][N-][C]:1.0, [C]N[C]:2.0, [C]N[C]>>[C]N=[C]:1.0, [C]N[C]>>[C][N-][C]:1.0, [C][N-][C]:2.0, [N-]:2.0, [NH]:2.0, [N]:2.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]N[C]>>[C][N-][C]
2: [C]N[C]>>[C]N=[C]
3: [C]N=[C]>>[C][N-][C]

MMP Level 2
1: [CH]C1=[C][C]=C([CH])N1>>[CH]C1=[C][C]=C([CH])[N-]1
2: [CH]=C1[C]=[C]C(=[CH])N1>>[CH]C=1[C]=[C]C(=[CH])N1
3: [CH]C1=NC(=[CH])[C][CH]1>>[CH]=C1[C][CH]C(=[CH])[N-]1

MMP Level 3
1: [C]=Cc1[nH]c(C=[C])c([CH2])c1[CH2]>>[C]=Cc1[n-]c(C=[C])c([CH2])c1[CH2]
2: [C]C=c1[nH]c(=C[C])c([CH2])c1[CH2]>>[C]C=C1N=C(C=[C])C([CH2])=C1[CH2]
3: [C]C=C1N=C(C=[C])C([CH2])C1([CH2])C>>[C]C=C1[N-]C(=C[C])C([CH2])(C)C1[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)Cc1c2cc3nc(cc4nc(cc5[nH]c(cc([nH]2)c1CCC(=O)O)c(c5CC(=O)O)CCC(=O)O)C(C)(CC(=O)O)C4CCC(=O)O)C(C)(CC(=O)O)C3CCC(=O)O>>O=C(O)Cc1c2[n-]c(cc3nc(cc4[n-]c(cc5nc(c2)C(C)(CC(=O)O)C5CCC(=O)O)C(C)(CC(=O)O)C4CCC(=O)O)C(=C3CCC(=O)O)CC(=O)O)c1CCC(=O)O, O=C(O)Cc1c2cc3nc(cc4nc(cc5[nH]c(cc([nH]2)c1CCC(=O)O)c(c5CC(=O)O)CCC(=O)O)C(C)(CC(=O)O)C4CCC(=O)O)C(C)(CC(=O)O)C3CCC(=O)O>>O=C(O)Cc1c2[n-]c(cc3nc(cc4[n-]c(cc5nc(c2)C(C)(CC(=O)O)C5CCC(=O)O)C(C)(CC(=O)O)C4CCC(=O)O)C(=C3CCC(=O)O)CC(=O)O)c1CCC(=O)O, O=C(O)Cc1c2cc3nc(cc4nc(cc5[nH]c(cc([nH]2)c1CCC(=O)O)c(c5CC(=O)O)CCC(=O)O)C(C)(CC(=O)O)C4CCC(=O)O)C(C)(CC(=O)O)C3CCC(=O)O>>O=C(O)Cc1c2[n-]c(cc3nc(cc4[n-]c(cc5nc(c2)C(C)(CC(=O)O)C5CCC(=O)O)C(C)(CC(=O)O)C4CCC(=O)O)C(=C3CCC(=O)O)CC(=O)O)c1CCC(=O)O]


//
SELECTED AAM MAPPING
[Co+2:63].[O:1]=[C:2]([OH:3])[CH2:4][C:5]:1:[C:6]:2:[CH:7]:[C:8]3:[N:9]:[C:10](:[CH:11]:[C:12]4:[N:13]:[C:14](:[CH:15]:[C:16]:5:[NH:17]:[C:18](:[CH:19]:[C:20](:[NH:21]2):[C:22]1[CH2:23][CH2:24][C:25](=[O:26])[OH:27]):[C:28](:[C:29]5[CH2:30][C:31](=[O:32])[OH:33])[CH2:34][CH2:35][C:36](=[O:37])[OH:38])[C:39]([CH3:40])([CH2:41][C:42](=[O:43])[OH:44])[CH:45]4[CH2:46][CH2:47][C:48](=[O:49])[OH:50])[C:51]([CH3:52])([CH2:53][C:54](=[O:55])[OH:56])[CH:57]3[CH2:58][CH2:59][C:60](=[O:61])[OH:62]>>[H+:64].[H+:65].[Co+2:63].[O:1]=[C:2]([OH:3])[CH2:4][C:5]=1[C:6]:2:[N:21]:[C:20](:[CH:19]:[C:18]:3:[N-:17]:[C:16](:[CH:15]:[C:14]4:[N:13]:[C:12](:[CH:11]:[C:10]5:[N-:9]:[C:8](:[CH:7]2)[CH:57]([CH2:58][CH2:59][C:60](=[O:61])[OH:62])[C:51]5([CH3:52])[CH2:53][C:54](=[O:55])[OH:56])[CH:45]([CH2:46][CH2:47][C:48](=[O:49])[OH:50])[C:39]4([CH3:40])[CH2:41][C:42](=[O:43])[OH:44]):[C:29](:[C:28]3[CH2:34][CH2:35][C:36](=[O:37])[OH:38])[CH2:30][C:31](=[O:32])[OH:33])[C:22]1[CH2:23][CH2:24][C:25](=[O:26])[OH:27]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=52, 2=51, 3=57, 4=8, 5=9, 6=10, 7=11, 8=12, 9=13, 10=14, 11=15, 12=16, 13=29, 14=28, 15=18, 16=17, 17=19, 18=20, 19=22, 20=5, 21=6, 22=7, 23=21, 24=4, 25=2, 26=1, 27=3, 28=23, 29=24, 30=25, 31=26, 32=27, 33=34, 34=35, 35=36, 36=37, 37=38, 38=30, 39=31, 40=32, 41=33, 42=39, 43=45, 44=46, 45=47, 46=48, 47=49, 48=50, 49=40, 50=41, 51=42, 52=43, 53=44, 54=58, 55=59, 56=60, 57=61, 58=62, 59=53, 60=54, 61=55, 62=56, 63=63}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=29, 2=28, 3=22, 4=20, 5=21, 6=6, 7=7, 8=8, 9=9, 10=10, 11=47, 12=46, 13=12, 14=11, 15=13, 16=14, 17=40, 18=34, 19=16, 20=15, 21=17, 22=18, 23=19, 24=35, 25=36, 26=37, 27=38, 28=39, 29=41, 30=42, 31=43, 32=44, 33=45, 34=53, 35=54, 36=55, 37=56, 38=48, 39=49, 40=50, 41=51, 42=52, 43=57, 44=5, 45=4, 46=2, 47=1, 48=3, 49=58, 50=59, 51=60, 52=61, 53=62, 54=23, 55=24, 56=25, 57=26, 58=27, 59=30, 60=31, 61=32, 62=33, 63=63, 64=65}

