
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0]

ORDER_CHANGED
[C%N*C%N:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C1C=CN([CH])[C]N1>>O=C1N[C]N([CH])C=C1C:1.0, [CH]:2.0, [C]:2.0, [C]C(=[CH])C:1.0, [C]C(=[C])N=C([CH2])[CH2]:1.0, [C]C(=[C])NC([CH2])[CH2]:1.0, [C]C(=[C])NC([CH2])[CH2]>>[C]C(=[C])N=C([CH2])[CH2]:1.0, [C]C=[CH]:1.0, [C]C=[CH]>>[C]C(=[CH])C:1.0, [C]N=C(C[NH])C[NH]:1.0, [C]N=[C]:1.0, [C]NC(C[NH])C[NH]:1.0, [C]NC(C[NH])C[NH]>>[C]N=C(C[NH])C[NH]:1.0, [C]NCC1NC([C])=[C]NC1>>[C]NCC1=NC([C])=[C]NC1:1.0, [C]N[CH]:1.0, [C]N[CH]>>[C]N=[C]:1.0, [NH]:1.0, [NH]C([CH2])[CH2]:1.0, [NH]C([CH2])[CH2]>>[N]=C([CH2])[CH2]:1.0, [N]:1.0, [N]=C([CH2])[CH2]:1.0, [N]C(=O)C=1NC(CNC1[NH])C[NH]>>[N]C(=O)C=1N=C(CNC1[NH])C[NH]:1.0, [N]C=C(C(=O)[NH])C:1.0, [N]C=CC(=O)[NH]:1.0, [N]C=CC(=O)[NH]>>[N]C=C(C(=O)[NH])C:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH3]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]C:1.0, [C]C(=[CH])C:1.0]


ID=Reaction Center at Level: 2 (2)
[[C]C(=[CH])C:1.0, [N]C=C(C(=O)[NH])C:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH]:1.0, [C]:1.0, [NH]:1.0, [N]:1.0]


ID=Reaction Center at Level: 1 (4)
[[C]N=[C]:1.0, [C]N[CH]:1.0, [NH]C([CH2])[CH2]:1.0, [N]=C([CH2])[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]C(=[C])N=C([CH2])[CH2]:1.0, [C]C(=[C])NC([CH2])[CH2]:1.0, [C]N=C(C[NH])C[NH]:1.0, [C]NC(C[NH])C[NH]:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[NH]C([CH2])[CH2]:1.0, [N]=C([CH2])[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[[C]N=C(C[NH])C[NH]:1.0, [C]NC(C[NH])C[NH]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C=[CH]>>[C]C(=[CH])C
2: [C]N[CH]>>[C]N=[C]
3: [NH]C([CH2])[CH2]>>[N]=C([CH2])[CH2]

MMP Level 2
1: [N]C=CC(=O)[NH]>>[N]C=C(C(=O)[NH])C
2: [C]C(=[C])NC([CH2])[CH2]>>[C]C(=[C])N=C([CH2])[CH2]
3: [C]NC(C[NH])C[NH]>>[C]N=C(C[NH])C[NH]

MMP Level 3
1: O=C1C=CN([CH])[C]N1>>O=C1N[C]N([CH])C=C1C
2: [N]C(=O)C=1NC(CNC1[NH])C[NH]>>[N]C(=O)C=1N=C(CNC1[NH])C[NH]
3: [C]NCC1NC([C])=[C]NC1>>[C]NCC1=NC([C])=[C]NC1


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1nc(N)[nH]c2NCC(Nc12)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)O>>O=c1nc(N)[nH]c2NCC(=Nc12)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)O, O=c1nc(N)[nH]c2NCC(Nc12)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)O>>O=c1nc(N)[nH]c2NCC(=Nc12)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)O]
2: R:M00002, P:M00004	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)O)C(O)C2>>O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)O)C(O)C2]


//
SELECTED AAM MAPPING
[O:31]=[C:30]([OH:32])[CH2:29][CH2:28][CH:24]([NH:23][C:21](=[O:22])[C:18]:1:[CH:17]:[CH:16]:[C:15](:[CH:20]:[CH:19]1)[NH:14][CH2:13][CH:10]2[NH:11][C:12]=3[C:2](=[O:1])[N:3]=[C:4]([NH2:5])[NH:6][C:7]3[NH:8][CH2:9]2)[C:25](=[O:26])[OH:27].[O:33]=[C:34]1[CH:35]=[CH:36][N:37]([C:38](=[O:39])[NH:40]1)[CH:41]2[O:42][CH:43]([CH2:44][O:45][P:46](=[O:47])([OH:48])[OH:49])[CH:50]([OH:51])[CH2:52]2>>[O:31]=[C:30]([OH:32])[CH2:29][CH2:28][CH:24]([NH:23][C:21](=[O:22])[C:18]:1:[CH:17]:[CH:16]:[C:15](:[CH:20]:[CH:19]1)[NH:14][CH2:13][C:10]2=[N:11][C:12]=3[C:2](=[O:1])[N:3]=[C:4]([NH2:5])[NH:6][C:7]3[NH:8][CH2:9]2)[C:25](=[O:26])[OH:27].[O:33]=[C:34]1[NH:40][C:38](=[O:39])[N:37]([CH:36]=[C:35]1[CH3:53])[CH:41]2[O:42][CH:43]([CH2:44][O:45][P:46](=[O:47])([OH:48])[OH:49])[CH:50]([OH:51])[CH2:52]2


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=10, 3=11, 4=12, 5=7, 6=8, 7=6, 8=4, 9=3, 10=2, 11=1, 12=5, 13=13, 14=14, 15=15, 16=16, 17=17, 18=18, 19=19, 20=20, 21=21, 22=22, 23=23, 24=24, 25=28, 26=29, 27=30, 28=31, 29=32, 30=25, 31=26, 32=27, 33=52, 34=50, 35=43, 36=42, 37=41, 38=37, 39=36, 40=35, 41=34, 42=33, 43=40, 44=38, 45=39, 46=44, 47=45, 48=46, 49=47, 50=48, 51=49, 52=51}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=9, 2=10, 3=11, 4=12, 5=7, 6=8, 7=6, 8=4, 9=3, 10=2, 11=1, 12=5, 13=13, 14=14, 15=15, 16=16, 17=17, 18=18, 19=19, 20=20, 21=21, 22=22, 23=23, 24=24, 25=28, 26=29, 27=30, 28=31, 29=32, 30=25, 31=26, 32=27, 33=41, 34=40, 35=39, 36=38, 37=36, 38=37, 39=35, 40=34, 41=33, 42=42, 43=53, 44=51, 45=44, 46=43, 47=45, 48=46, 49=47, 50=48, 51=49, 52=50, 53=52}

