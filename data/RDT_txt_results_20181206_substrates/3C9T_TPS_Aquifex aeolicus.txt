
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[CH2]>>O=P(O)(O)OP(=O)(O)O[CH2]:1.0, O=P(O)(O)O[P]:2.0, O=P(O)(O)O[P]>>O=P(O)(O)O[P]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:4.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:2.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O>>[P]O[P]:1.0, [P]O[P]:2.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]P(=O)(O)O:2.0, [P]O[P]:2.0]


ID=Reaction Center at Level: 2 (2)
[O=P(O)(O)O[P]:2.0, [O]P(=O)(O)OP(=O)(O)O:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O>>[P]O[P]
2: [P]O[P]>>[P]O
3: [O]P(=O)(O)O>>[O]P(=O)(O)O

MMP Level 2
1: [O]P(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O
2: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
3: O=P(O)(O)O[P]>>O=P(O)(O)O[P]

MMP Level 3
1: O=P(O)(O)O[CH2]>>O=P(O)(O)OP(=O)(O)O[CH2]
2: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
3: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCCc1sc[n+](c1C)Cc2cnc(nc2N)C]
3: R:M00002, P:M00004	[O=P(O)(O)OCCc1sc[n+](c1C)Cc2cnc(nc2N)C>>O=P(O)(O)OP(=O)(O)OCCc1sc[n+](c1C)Cc2cnc(nc2N)C]


//
SELECTED AAM MAPPING
[O:32]=[P:33]([OH:34])([OH:35])[O:36][CH2:37][CH2:38][C:39]:1:[S:40]:[CH:41]:[N+:42](:[C:43]1[CH3:44])[CH2:45][C:46]:2:[CH:47]:[N:48]:[C:49](:[N:50]:[C:51]2[NH2:52])[CH3:53].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:1]=[P:2]([OH:3])([OH:4])[O:34][P:33](=[O:32])([OH:35])[O:36][CH2:37][CH2:38][C:39]:1:[S:40]:[CH:41]:[N+:42](:[C:43]1[CH3:44])[CH2:45][C:46]:2:[CH:47]:[N:48]:[C:49](:[N:50]:[C:51]2[NH2:52])[CH3:53].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=44, 33=43, 34=39, 35=40, 36=41, 37=42, 38=45, 39=46, 40=47, 41=48, 42=49, 43=50, 44=51, 45=52, 46=53, 47=38, 48=37, 49=36, 50=33, 51=32, 52=34, 53=35}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=17, 5=18, 6=19, 7=23, 8=16, 9=15, 10=14, 11=13, 12=24, 13=26, 14=11, 15=12, 16=10, 17=9, 18=6, 19=7, 20=8, 21=5, 22=2, 23=1, 24=3, 25=4, 26=27, 27=25, 28=44, 29=43, 30=39, 31=40, 32=41, 33=42, 34=45, 35=46, 36=47, 37=48, 38=49, 39=50, 40=51, 41=52, 42=53, 43=38, 44=37, 45=36, 46=33, 47=34, 48=35, 49=32, 50=29, 51=28, 52=30, 53=31}

