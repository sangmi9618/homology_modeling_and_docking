
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0, C=O:1.0, C@C:1.0, O=O:1.0]

ORDER_CHANGED
[C-C*C@C:1.0, C-O*C=O:1.0]

//
FINGERPRINTS RC
[O=C(O)C(=O)C=[CH]:1.0, O=C([CH])C(=O)O:1.0, O=O:4.0, O=O>>O=C([CH])C(=O)O:1.0, O=O>>[CH]C([CH2])=CO:1.0, O=O>>[CH]O:1.0, O=O>>[C]=CO:1.0, O=O>>[C]=O:1.0, O=O>>[C]C(=O)O:1.0, OC=1C=[C]C=CC1O>>[C]C=CC(=O)C(=O)O:1.0, Oc1ccc([CH2])cc1O>>O=C(O)C(=O)C=[CH]:1.0, [CH]:2.0, [CH]C(O)=C(O)C=[CH]:1.0, [CH]C(O)=C(O)C=[CH]>>O=C(O)C(=O)C=[CH]:2.0, [CH]C([CH2])=CO:1.0, [CH]O:1.0, [C]:4.0, [C]=C(O)C=C([CH])[CH2]:1.0, [C]=C(O)C=C([CH])[CH2]>>[CH]C([CH2])=CO:1.0, [C]=C([CH])O:3.0, [C]=C([CH])O>>[C]C(=O)O:1.0, [C]=C([CH])O>>[C]C(=O)[CH]:2.0, [C]=CC(O)=C([CH])O:1.0, [C]=CC(O)=C([CH])O>>O=C([CH])C(=O)O:1.0, [C]=CO:2.0, [C]=O:2.0, [C]C(=O)O:2.0, [C]C(=O)[CH]:2.0, [C]C=[C]:1.0, [C]C=[C]>>[C]=CO:1.0, [C]Cc1ccc(O)c(O)c1>>[C]CC(C=[CH])=CO:1.0, [C]O:1.0, [C]O>>[C]=O:1.0, [OH]:2.0, [O]:4.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[CH]:2.0, [C]:2.0, [OH]:1.0, [O]:3.0]


ID=Reaction Center at Level: 1 (7)
[O=O:2.0, [CH]O:1.0, [C]=C([CH])O:1.0, [C]=CO:1.0, [C]=O:1.0, [C]C(=O)O:1.0, [C]C=[C]:1.0]


ID=Reaction Center at Level: 2 (7)
[O=C([CH])C(=O)O:1.0, O=O:2.0, [CH]C([CH2])=CO:1.0, [C]=C(O)C=C([CH])[CH2]:1.0, [C]=CC(O)=C([CH])O:1.0, [C]=CO:1.0, [C]C(=O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:4.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (5)
[[C]=C([CH])O:2.0, [C]=O:1.0, [C]C(=O)O:1.0, [C]C(=O)[CH]:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (6)
[O=C(O)C(=O)C=[CH]:1.0, O=C([CH])C(=O)O:1.0, [CH]C(O)=C(O)C=[CH]:1.0, [C]=C([CH])O:1.0, [C]=CC(O)=C([CH])O:1.0, [C]C(=O)[CH]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]=C([CH])O>>[C]C(=O)O
2: O=O>>[CH]O
3: [C]C=[C]>>[C]=CO
4: [C]O>>[C]=O
5: [C]=C([CH])O>>[C]C(=O)[CH]
6: O=O>>[C]=O

MMP Level 2
1: [C]=CC(O)=C([CH])O>>O=C([CH])C(=O)O
2: O=O>>[C]=CO
3: [C]=C(O)C=C([CH])[CH2]>>[CH]C([CH2])=CO
4: [C]=C([CH])O>>[C]C(=O)[CH]
5: [CH]C(O)=C(O)C=[CH]>>O=C(O)C(=O)C=[CH]
6: O=O>>[C]C(=O)O

MMP Level 3
1: Oc1ccc([CH2])cc1O>>O=C(O)C(=O)C=[CH]
2: O=O>>[CH]C([CH2])=CO
3: [C]Cc1ccc(O)c(O)c1>>[C]CC(C=[CH])=CO
4: [CH]C(O)=C(O)C=[CH]>>O=C(O)C(=O)C=[CH]
5: OC=1C=[C]C=CC1O>>[C]C=CC(=O)C(=O)O
6: O=O>>O=C([CH])C(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)Cc1ccc(O)c(O)c1>>O=C(O)C(=O)C=CC(=CO)CC(=O)O, O=C(O)Cc1ccc(O)c(O)c1>>O=C(O)C(=O)C=CC(=CO)CC(=O)O, O=C(O)Cc1ccc(O)c(O)c1>>O=C(O)C(=O)C=CC(=CO)CC(=O)O, O=C(O)Cc1ccc(O)c(O)c1>>O=C(O)C(=O)C=CC(=CO)CC(=O)O]
2: R:M00002, P:M00003	[O=O>>O=C(O)C(=O)C=CC(=CO)CC(=O)O, O=O>>O=C(O)C(=O)C=CC(=CO)CC(=O)O]


//
SELECTED AAM MAPPING
[O:13]=[O:14].[O:1]=[C:2]([OH:3])[CH2:4][C:5]:1:[CH:6]:[CH:7]:[C:8]([OH:9]):[C:10]([OH:11]):[CH:12]1>>[O:13]=[C:10]([OH:11])[C:8](=[O:9])[CH:7]=[CH:6][C:5](=[CH:12][OH:14])[CH2:4][C:2](=[O:1])[OH:3]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=8, 4=10, 5=12, 6=5, 7=4, 8=2, 9=1, 10=3, 11=11, 12=9, 13=13, 14=14}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=11, 2=8, 3=9, 4=10, 5=7, 6=6, 7=4, 8=5, 9=2, 10=1, 11=3, 12=12, 13=13, 14=14}

