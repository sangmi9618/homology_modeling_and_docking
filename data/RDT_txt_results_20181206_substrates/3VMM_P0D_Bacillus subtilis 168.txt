
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O=C(O)C(CC([CH])=[CH])CP(=O)([CH])O>>[O]P(=O)([CH])CC(C(=O)O)CC([CH])=[CH]:1.0, O=P(O)(C[CH])C(N)C>>O=P(O)(O)OP(=O)(C[CH])C(N)C:1.0, O=P(O)(C[CH])C(N)C>>O=P(O[P])(C[CH])C(N)C:1.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)OP(=O)([CH])[CH2]:1.0, O=P(O)(O)O[P]:2.0, O=P(O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)([CH2])C(N)C:1.0, O=P(O)([CH2])C(N)C>>[O]P(=O)([CH2])C(N)C:1.0, O=P([CH])(O)[CH2]:1.0, O=P([CH])(O)[CH2]>>O=P(O)(O)OP(=O)([CH])[CH2]:1.0, [CH]:4.0, [C]C([CH2])[CH2]:2.0, [C]C([CH2])[CH2]>>[C]C([CH2])[CH2]:1.0, [C]CC(C(=O)O)C[P]:2.0, [C]CC(C(=O)O)C[P]>>[C]CC(C(=O)O)C[P]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OP(=O)([CH])[CH2]:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)([CH2])C(N)C:1.0, [P]:2.0, [P]C(N)C:2.0, [P]C(N)C>>[P]C(N)C:1.0, [P]O:2.0, [P]O>>[P]O[P]:1.0, [P]O[P]:2.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]P(=O)(O)O:2.0, [P]O[P]:2.0]


ID=Reaction Center at Level: 2 (3)
[O=P(O)(O)OP(=O)([CH])[CH2]:1.0, O=P(O)(O)O[P]:2.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:4.0]


ID=Reaction Center at Level: 1 (2)
[[C]C([CH2])[CH2]:2.0, [P]C(N)C:2.0]


ID=Reaction Center at Level: 2 (3)
[O=P(O)([CH2])C(N)C:1.0, [C]CC(C(=O)O)C[P]:2.0, [O]P(=O)([CH2])C(N)C:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [P]C(N)C>>[P]C(N)C
2: [O]P(=O)(O)O>>[O]P(=O)(O)O
3: [C]C([CH2])[CH2]>>[C]C([CH2])[CH2]
4: [P]O[P]>>[P]O
5: [P]O>>[P]O[P]

MMP Level 2
1: O=P(O)([CH2])C(N)C>>[O]P(=O)([CH2])C(N)C
2: O=P(O)(O)O[P]>>O=P(O)(O)O[P]
3: [C]CC(C(=O)O)C[P]>>[C]CC(C(=O)O)C[P]
4: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
5: O=P([CH])(O)[CH2]>>O=P(O)(O)OP(=O)([CH])[CH2]

MMP Level 3
1: O=P(O)(C[CH])C(N)C>>O=P(O[P])(C[CH])C(N)C
2: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OP(=O)([CH])[CH2]
3: O=C(O)C(CC([CH])=[CH])CP(=O)([CH])O>>[O]P(=O)([CH])CC(C(=O)O)CC([CH])=[CH]
4: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
5: O=P(O)(C[CH])C(N)C>>O=P(O)(O)OP(=O)(C[CH])C(N)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(Cc1ccccc1)CP(=O)(O)C(N)C>>O=C(O)C(Cc1ccccc1)CP(=O)(OP(=O)(O)O)C(N)C, O=C(O)C(Cc1ccccc1)CP(=O)(O)C(N)C>>O=C(O)C(Cc1ccccc1)CP(=O)(OP(=O)(O)O)C(N)C, O=C(O)C(Cc1ccccc1)CP(=O)(O)C(N)C>>O=C(O)C(Cc1ccccc1)CP(=O)(OP(=O)(O)O)C(N)C]
2: R:M00002, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(O)C(Cc1ccccc1)CP(=O)(OP(=O)(O)O)C(N)C]
3: R:M00002, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]


//
SELECTED AAM MAPPING
[O:32]=[C:33]([OH:34])[CH:35]([CH2:36][C:37]:1:[CH:38]:[CH:39]:[CH:40]:[CH:41]:[CH:42]1)[CH2:43][P:44](=[O:45])([OH:46])[CH:47]([NH2:48])[CH3:49].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:32]=[C:33]([OH:34])[CH:35]([CH2:36][C:37]:1:[CH:38]:[CH:39]:[CH:40]:[CH:41]:[CH:42]1)[CH2:43][P:44](=[O:45])([O:46][P:2](=[O:1])([OH:3])[OH:4])[CH:47]([NH2:48])[CH3:49].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=49, 2=47, 3=48, 4=44, 5=45, 6=43, 7=35, 8=36, 9=37, 10=38, 11=39, 12=40, 13=41, 14=42, 15=33, 16=32, 17=34, 18=46, 19=24, 20=25, 21=26, 22=21, 23=22, 24=23, 25=27, 26=20, 27=19, 28=18, 29=17, 30=28, 31=30, 32=15, 33=16, 34=14, 35=13, 36=10, 37=11, 38=12, 39=9, 40=6, 41=7, 42=8, 43=5, 44=2, 45=1, 46=3, 47=4, 48=31, 49=29}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=49, 2=47, 3=48, 4=40, 5=41, 6=39, 7=31, 8=32, 9=33, 10=34, 11=35, 12=36, 13=37, 14=38, 15=29, 16=28, 17=30, 18=42, 19=43, 20=44, 21=45, 22=46, 23=20, 24=21, 25=22, 26=17, 27=18, 28=19, 29=23, 30=16, 31=15, 32=14, 33=13, 34=24, 35=26, 36=11, 37=12, 38=10, 39=9, 40=6, 41=7, 42=8, 43=5, 44=2, 45=1, 46=3, 47=4, 48=27, 49=25}

