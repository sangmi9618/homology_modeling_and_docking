
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-Cl:1.0, C-O:1.0]

//
FINGERPRINTS RC
[N#CC=1C(Cl)=[C]C(Cl)=C(Cl)C1Cl>>N#CC=1C(Cl)=[C]C(Cl)=C(Cl)C1O:1.0, O:3.0, O>>[C]C(=[C])O:1.0, O>>[C]C([C])=C(O)C(=[C])Cl:1.0, O>>[C]O:1.0, [C]:2.0, [C]C(=[C])Cl:2.0, [C]C(=[C])Cl>>[C]C(=[C])O:1.0, [C]C(=[C])Cl>>[Cl-]:1.0, [C]C(=[C])O:2.0, [C]C([C])=C(Cl)C(=[C])Cl:1.0, [C]C([C])=C(Cl)C(=[C])Cl>>[C]C([C])=C(O)C(=[C])Cl:1.0, [C]C([C])=C(Cl)C(=[C])Cl>>[Cl-]:1.0, [C]C([C])=C(O)C(=[C])Cl:1.0, [C]Cl:1.0, [C]Cl>>[Cl-]:1.0, [C]O:1.0, [Cl-]:3.0, [Cl]:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[O:1.0, [C]:2.0, [Cl-]:1.0, [Cl]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (6)
[O:1.0, [C]C(=[C])Cl:1.0, [C]C(=[C])O:1.0, [C]Cl:1.0, [C]O:1.0, [Cl-]:1.0]


ID=Reaction Center at Level: 2 (6)
[O:1.0, [C]C(=[C])Cl:1.0, [C]C(=[C])O:1.0, [C]C([C])=C(Cl)C(=[C])Cl:1.0, [C]C([C])=C(O)C(=[C])Cl:1.0, [Cl-]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C(=[C])Cl>>[C]C(=[C])O
2: O>>[C]O
3: [C]Cl>>[Cl-]

MMP Level 2
1: [C]C([C])=C(Cl)C(=[C])Cl>>[C]C([C])=C(O)C(=[C])Cl
2: O>>[C]C(=[C])O
3: [C]C(=[C])Cl>>[Cl-]

MMP Level 3
1: N#CC=1C(Cl)=[C]C(Cl)=C(Cl)C1Cl>>N#CC=1C(Cl)=[C]C(Cl)=C(Cl)C1O
2: O>>[C]C([C])=C(O)C(=[C])Cl
3: [C]C([C])=C(Cl)C(=[C])Cl>>[Cl-]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O>>N#Cc1c(Cl)c(Cl)c(O)c(C#N)c1Cl]
2: R:M00002, P:M00003	[N#Cc1c(Cl)c(Cl)c(Cl)c(C#N)c1Cl>>[Cl-]]
3: R:M00002, P:M00004	[N#Cc1c(Cl)c(Cl)c(Cl)c(C#N)c1Cl>>N#Cc1c(Cl)c(Cl)c(O)c(C#N)c1Cl]


//
SELECTED AAM MAPPING
[N:1]#[C:2][C:3]:1:[C:4]([Cl:5]):[C:6]([Cl:7]):[C:8]([Cl:9]):[C:10]([C:11]#[N:12]):[C:13]1[Cl:14].[OH2:15]>>[Cl-:9].[N:1]#[C:2][C:3]:1:[C:4]([Cl:5]):[C:6]([Cl:7]):[C:8]([OH:15]):[C:10]([C:11]#[N:12]):[C:13]1[Cl:14]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=15, 2=2, 3=1, 4=3, 5=13, 6=10, 7=8, 8=6, 9=4, 10=5, 11=7, 12=9, 13=11, 14=12, 15=14}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=15, 2=11, 3=12, 4=10, 5=8, 6=6, 7=4, 8=3, 9=13, 10=14, 11=2, 12=1, 13=5, 14=7, 15=9}

