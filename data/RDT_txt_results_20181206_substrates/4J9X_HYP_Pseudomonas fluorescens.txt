
//
FINGERPRINTS BC

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(O)C1NCC(O)C1>>O=C(O)C1NCC(O)C1:1.0, O=C(O)C1NC[CH]C1:2.0, O=C(O)C1NC[CH]C1>>O=C(O)C1NC[CH]C1:1.0, [CH]:2.0, [C]C([NH])[CH2]:2.0, [C]C([NH])[CH2]>>[C]C([NH])[CH2]:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (1)
[[C]C([NH])[CH2]:2.0]


ID=Reaction Center at Level: 2 (1)
[O=C(O)C1NC[CH]C1:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C([NH])[CH2]>>[C]C([NH])[CH2]

MMP Level 2
1: O=C(O)C1NC[CH]C1>>O=C(O)C1NC[CH]C1

MMP Level 3
1: O=C(O)C1NCC(O)C1>>O=C(O)C1NCC(O)C1


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)C1NCC(O)C1>>O=C(O)C1NCC(O)C1]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]1[NH:5][CH2:6][CH:7]([OH:8])[CH2:9]1>>[O:1]=[C:2]([OH:3])[CH:4]1[NH:5][CH2:6][CH:7]([OH:8])[CH2:9]1


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=7, 3=6, 4=5, 5=4, 6=2, 7=1, 8=3, 9=8}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=9, 2=7, 3=6, 4=5, 5=4, 6=2, 7=1, 8=3, 9=8}

