
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O=P(O)(O[P])OC1O[CH][CH]C1O>>O=P(O)(O)O[P]:1.0, [CH]:2.0, [C]C1=[C]N=CN1:1.0, [C]C1=[C]N=CN1>>[C]C1=[C][N]C=N1:1.0, [C]C1=[C][N]C=N1:1.0, [C]N([CH])C1O[CH][CH]C1O:1.0, [C]N=[CH]:1.0, [C]N[CH]:1.0, [C]N[CH]>>[C]N=[CH]:1.0, [NH]:1.0, [N]:1.0, [N]C(=O)c1[nH]cnc1[NH]>>[N]C(=O)c1ncn([CH])c1[NH]:1.0, [N]C([O])[CH]:1.0, [OH]:1.0, [O]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]C([O])[CH]:1.0, [O]C([O])[CH]>>[N]C([O])[CH]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OC1OC([CH2])C(O)C1O>>[NH]C1=[C]N=CN1C2OC([CH2])C(O)C2O:1.0, [P]O:1.0, [P]OC1O[CH][CH]C1O:1.0, [P]OC1O[CH][CH]C1O>>[C]N([CH])C1O[CH][CH]C1O:1.0, [P]O[CH]:1.0, [P]O[CH]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [N]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[C]N([CH])[CH]:1.0, [N]C([O])[CH]:1.0, [O]C([O])[CH]:1.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]N([CH])C1O[CH][CH]C1O:1.0, [O]C([CH])N1C=N[C]=C1[NH]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [P]OC1O[CH][CH]C1O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]C([O])[CH]>>[N]C([O])[CH]
2: [P]O[CH]>>[P]O
3: [C]N[CH]>>[C]N=[CH]

MMP Level 2
1: [P]OC1O[CH][CH]C1O>>[C]N([CH])C1O[CH][CH]C1O
2: [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O
3: [C]C1=[C]N=CN1>>[C]C1=[C][N]C=N1

MMP Level 3
1: [O]P(=O)(O)OC1OC([CH2])C(O)C1O>>[NH]C1=[C]N=CN1C2OC([CH2])C(O)C2O
2: O=P(O)(O[P])OC1O[CH][CH]C1O>>O=P(O)(O)O[P]
3: [N]C(=O)c1[nH]cnc1[NH]>>[N]C(=O)c1ncn([CH])c1[NH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1nc(N)[nH]c2nc[nH]c12>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O]
2: R:M00002, P:M00003	[O=P(O)(O)OCC1OC(OP(=O)(O)OP(=O)(O)O)C(O)C1O>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O]
3: R:M00002, P:M00004	[O=P(O)(O)OCC1OC(OP(=O)(O)OP(=O)(O)O)C(O)C1O>>O=P(O)(O)OP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:23]=[C:24]1[N:25]=[C:26]([NH2:27])[NH:28][C:29]:2:[N:30]:[CH:31]:[NH:32]:[C:33]12.[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH2:6][CH:7]1[O:8][CH:9]([O:10][P:11](=[O:12])([OH:13])[O:14][P:15](=[O:16])([OH:17])[OH:18])[CH:19]([OH:20])[CH:21]1[OH:22]>>[O:23]=[C:24]1[N:25]=[C:26]([NH2:27])[NH:28][C:29]:2:[C:33]1:[N:32]:[CH:31]:[N:30]2[CH:9]3[O:8][CH:7]([CH2:6][O:5][P:2](=[O:1])([OH:3])[OH:4])[CH:21]([OH:22])[CH:19]3[OH:20].[O:12]=[P:11]([OH:10])([OH:13])[O:14][P:15](=[O:16])([OH:17])[OH:18]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=31, 2=30, 3=29, 4=33, 5=32, 6=24, 7=23, 8=25, 9=26, 10=28, 11=27, 12=6, 13=7, 14=21, 15=19, 16=9, 17=8, 18=10, 19=11, 20=12, 21=13, 22=14, 23=15, 24=16, 25=17, 26=18, 27=20, 28=22, 29=5, 30=2, 31=1, 32=3, 33=4}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=10, 2=9, 3=8, 4=7, 5=11, 6=12, 7=23, 8=21, 9=14, 10=13, 11=15, 12=16, 13=17, 14=18, 15=19, 16=20, 17=22, 18=24, 19=6, 20=4, 21=3, 22=2, 23=1, 24=5, 25=27, 26=26, 27=25, 28=28, 29=29, 30=30, 31=31, 32=32, 33=33}

