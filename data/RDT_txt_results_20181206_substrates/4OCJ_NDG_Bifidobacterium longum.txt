
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[CH]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O[CH]:1.0, [CH]:2.0, [CH]O:1.0, [CH]O>>[P]O[CH]:1.0, [CH]OC(O)C([CH])[NH]:1.0, [CH]OC(O)C([CH])[NH]>>O=P(O)(O)OC(O[CH])C([CH])[NH]:1.0, [CH]OC(O)C([CH])[NH]>>[P]OC(O[CH])C([CH])[NH]:1.0, [C]NC1C(O)OC([CH2])[CH]C1O>>[C]NC1C(O)[CH]C(OC1OP(=O)(O)O)[CH2]:1.0, [OH]:2.0, [O]:2.0, [O]C([CH])O:2.0, [O]C([CH])O>>[O]C([CH])OP(=O)(O)O:1.0, [O]C([CH])O>>[O]C([O])[CH]:1.0, [O]C([CH])OP(=O)(O)O:1.0, [O]C([O])[CH]:1.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]C([CH])OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]OC(O[CH])C([CH])[NH]:1.0, [P]O[CH]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[O]P(=O)(O)O:2.0, [P]O[CH]:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)O[CH]:1.0, O=P(O)(O)O[P]:1.0, [O]C([CH])OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]C([CH])O:1.0, [O]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[CH]OC(O)C([CH])[NH]:1.0, [P]OC(O[CH])C([CH])[NH]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]O>>[P]O[CH]
2: [O]C([CH])O>>[O]C([O])[CH]
3: [O]P(=O)(O)O>>[O]P(=O)(O)O
4: [P]O[P]>>[P]O

MMP Level 2
1: [O]C([CH])O>>[O]C([CH])OP(=O)(O)O
2: [CH]OC(O)C([CH])[NH]>>[P]OC(O[CH])C([CH])[NH]
3: O=P(O)(O)O[P]>>O=P(O)(O)O[CH]
4: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O

MMP Level 3
1: [CH]OC(O)C([CH])[NH]>>O=P(O)(O)OC(O[CH])C([CH])[NH]
2: [C]NC1C(O)OC([CH2])[CH]C1O>>[C]NC1C(O)[CH]C(OC1OP(=O)(O)O)[CH2]
3: [O]P(=O)(O)OP(=O)(O)O>>[O]C([CH])OP(=O)(O)O
4: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(NC1C(O)C(O)C(OC1OP(=O)(O)O)CO)C]
3: R:M00002, P:M00004	[O=C(NC1C(O)OC(CO)C(O)C1O)C>>O=C(NC1C(O)C(O)C(OC1OP(=O)(O)O)CO)C, O=C(NC1C(O)OC(CO)C(O)C1O)C>>O=C(NC1C(O)C(O)C(OC1OP(=O)(O)O)CO)C]


//
SELECTED AAM MAPPING
[O:32]=[C:33]([NH:34][CH:35]1[CH:36]([OH:37])[O:38][CH:39]([CH2:40][OH:41])[CH:42]([OH:43])[CH:44]1[OH:45])[CH3:46].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:32]=[C:33]([NH:34][CH:35]1[CH:44]([OH:45])[CH:42]([OH:43])[CH:39]([O:38][CH:36]1[O:37][P:2](=[O:1])([OH:3])[OH:4])[CH2:40][OH:41])[CH3:46].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=46, 33=33, 34=32, 35=34, 36=35, 37=44, 38=42, 39=39, 40=38, 41=36, 42=37, 43=40, 44=41, 45=43, 46=45}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=17, 5=18, 6=19, 7=23, 8=16, 9=15, 10=14, 11=13, 12=24, 13=26, 14=11, 15=12, 16=10, 17=9, 18=6, 19=7, 20=8, 21=5, 22=2, 23=1, 24=3, 25=4, 26=27, 27=25, 28=46, 29=29, 30=28, 31=30, 32=31, 33=32, 34=34, 35=36, 36=37, 37=38, 38=39, 39=40, 40=41, 41=42, 42=43, 43=44, 44=45, 45=35, 46=33}

