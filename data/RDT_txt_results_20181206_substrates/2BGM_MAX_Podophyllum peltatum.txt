
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%O:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

//
FINGERPRINTS RC
[O=C(N)C=1C=[N+]C=CC1>>O=C(N)C1=C[N]C=CC1:1.0, O=C1OC[CH]C1[CH2]:1.0, O=C1OC[CH][CH]1:1.0, O[CH2]:2.0, O[CH2]>>[C]=O:1.0, O[CH2]>>[C]O[CH2]:1.0, [CH2]:2.0, [CH]:1.0, [CH]C([CH2])CO:1.0, [CH]C([CH2])CO>>O=C1OCC([CH2])C1[CH2]:1.0, [CH]C([CH2])CO>>O=C1OC[CH]C1[CH2]:2.0, [CH]CO:3.0, [CH]CO>>O=C1OC[CH][CH]1:1.0, [CH]CO>>[O]C(=O)[CH]:2.0, [CH]N([CH])[CH]:1.0, [CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]>>[CH]N([CH])[CH]:1.0, [C]:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN(C=[CH])C([O])[CH]:1.0, [C]=O:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])C=C[CH]>>[C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C=[CH]:1.0, [C]C=[CH]>>[C]C[CH]:1.0, [C]CC(CO)C([CH2])[CH2]>>[C]CC1C(=O)OCC1[CH2]:1.0, [C]C[CH]:1.0, [C]O[CH2]:1.0, [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]C1=CN(C=CC1)C2O[CH][CH]C2O:1.0, [N+]:1.0, [N]:1.0, [OH]:2.0, [O]:2.0, [O]C(=O)[CH]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[C]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]O[CH2]:1.0, [O]C(=O)[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C1OC[CH]C1[CH2]:1.0, O=C1OC[CH][CH]1:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH2]:1.0, [C]:1.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O[CH2]:1.0, [CH]CO:1.0, [C]=O:1.0, [O]C(=O)[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C1OC[CH]C1[CH2]:1.0, [CH]C([CH2])CO:1.0, [CH]CO:1.0, [O]C(=O)[CH]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH][N+]([CH])=[CH]>>[CH]N([CH])[CH]
2: O[CH2]>>[C]O[CH2]
3: [C]C=[CH]>>[C]C[CH]
4: [CH]CO>>[O]C(=O)[CH]
5: O[CH2]>>[C]=O

MMP Level 2
1: [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN(C=[CH])C([O])[CH]
2: [CH]CO>>O=C1OC[CH][CH]1
3: [C]C(=[CH])C=C[CH]>>[C]C(=[CH])CC=[CH]
4: [CH]C([CH2])CO>>O=C1OC[CH]C1[CH2]
5: [CH]CO>>[O]C(=O)[CH]

MMP Level 3
1: [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]C1=CN(C=CC1)C2O[CH][CH]C2O
2: [CH]C([CH2])CO>>O=C1OCC([CH2])C1[CH2]
3: O=C(N)C=1C=[N+]C=CC1>>O=C(N)C1=C[N]C=CC1
4: [C]CC(CO)C([CH2])[CH2]>>[C]CC1C(=O)OCC1[CH2]
5: [CH]C([CH2])CO>>O=C1OC[CH]C1[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00005	[Oc1ccc(cc1OC)CC(CO)C(CO)Cc2ccc(O)c(OC)c2>>O=C1OCC(Cc2ccc(O)c(OC)c2)C1Cc3ccc(O)c(OC)c3, Oc1ccc(cc1OC)CC(CO)C(CO)Cc2ccc(O)c(OC)c2>>O=C1OCC(Cc2ccc(O)c(OC)c2)C1Cc3ccc(O)c(OC)c3, Oc1ccc(cc1OC)CC(CO)C(CO)Cc2ccc(O)c(OC)c2>>O=C1OCC(Cc2ccc(O)c(OC)c2)C1Cc3ccc(O)c(OC)c3]
2: R:M00002, P:M00003	[O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O, O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:5]:[CH:6]:[CH:7]:[N+:8](:[CH:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44].[OH:45][C:46]:1:[CH:47]:[CH:48]:[C:49](:[CH:50]:[C:51]1[O:52][CH3:53])[CH2:54][CH:55]([CH2:56][OH:57])[CH:58]([CH2:59][OH:60])[CH2:61][C:62]:2:[CH:63]:[CH:64]:[C:65]([OH:66]):[C:67]([O:68][CH3:69]):[CH:70]2>>[H+:71].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:9][N:8]([CH:7]=[CH:6][CH2:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44].[O:60]=[C:59]1[O:57][CH2:56][CH:55]([CH2:54][C:49]:2:[CH:48]:[CH:47]:[C:46]([OH:45]):[C:51]([O:52][CH3:53]):[CH:50]2)[CH:58]1[CH2:61][C:62]:3:[CH:63]:[CH:64]:[C:65]([OH:66]):[C:67]([O:68][CH3:69]):[CH:70]3


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=69, 2=68, 3=67, 4=65, 5=64, 6=63, 7=62, 8=70, 9=61, 10=58, 11=59, 12=60, 13=55, 14=54, 15=49, 16=50, 17=51, 18=46, 19=47, 20=48, 21=45, 22=52, 23=53, 24=56, 25=57, 26=66, 27=6, 28=5, 29=4, 30=9, 31=8, 32=7, 33=10, 34=43, 35=41, 36=12, 37=11, 38=13, 39=14, 40=15, 41=16, 42=17, 43=18, 44=19, 45=20, 46=21, 47=22, 48=23, 49=24, 50=39, 51=37, 52=26, 53=25, 54=27, 55=28, 56=29, 57=30, 58=35, 59=34, 60=33, 61=32, 62=31, 63=36, 64=38, 65=40, 66=42, 67=44, 68=2, 69=1, 70=3}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=5, 6=4, 7=2, 8=1, 9=3, 10=10, 11=43, 12=41, 13=12, 14=11, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=39, 28=37, 29=26, 30=25, 31=27, 32=28, 33=29, 34=30, 35=35, 36=34, 37=33, 38=32, 39=31, 40=36, 41=38, 42=40, 43=42, 44=44, 45=71, 46=58, 47=57, 48=56, 49=54, 50=53, 51=52, 52=51, 53=59, 54=50, 55=49, 56=48, 57=47, 58=46, 59=45, 60=60, 61=61, 62=62, 63=70, 64=67, 65=65, 66=64, 67=63, 68=66, 69=68, 70=69, 71=55}

