
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(O)C(=O)CC(O)C(O)[CH2]>>O=C(O)C(=O)C:1.0, O=CC(O)[CH2]:1.0, O=C[CH]:2.0, O=[CH]:1.0, [CH2]:1.0, [CH3]:1.0, [CH]:2.0, [CH]C(O)[CH2]:2.0, [CH]C(O)[CH2]>>O=C[CH]:2.0, [CH]O:1.0, [CH]O>>O=[CH]:1.0, [C]C:1.0, [C]C(=O)C:1.0, [C]C(=O)CC(O)C(O)C[O]>>[O]CC(O)C=O:1.0, [C]C(=O)CC([CH])O:1.0, [C]C(=O)CC([CH])O>>[C]C(=O)C:1.0, [C]CC(O)C(O)[CH2]:1.0, [C]CC(O)C(O)[CH2]>>O=CC(O)[CH2]:2.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C:1.0, [OH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH2]:1.0, [CH]:1.0]


ID=Reaction Center at Level: 1 (2)
[[CH]C(O)[CH2]:1.0, [C]C[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[C]C(=O)CC([CH])O:1.0, [C]CC(O)C(O)[CH2]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C[CH]:1.0, O=[CH]:1.0, [CH]C(O)[CH2]:1.0, [CH]O:1.0]


ID=Reaction Center at Level: 2 (4)
[O=CC(O)[CH2]:1.0, O=C[CH]:1.0, [CH]C(O)[CH2]:1.0, [C]CC(O)C(O)[CH2]:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (2)
[O=C[CH]:1.0, [CH]C(O)[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[O=CC(O)[CH2]:1.0, [C]CC(O)C(O)[CH2]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C[CH]>>[C]C
2: [CH]O>>O=[CH]
3: [CH]C(O)[CH2]>>O=C[CH]

MMP Level 2
1: [C]C(=O)CC([CH])O>>[C]C(=O)C
2: [CH]C(O)[CH2]>>O=C[CH]
3: [C]CC(O)C(O)[CH2]>>O=CC(O)[CH2]

MMP Level 3
1: O=C(O)C(=O)CC(O)C(O)[CH2]>>O=C(O)C(=O)C
2: [C]CC(O)C(O)[CH2]>>O=CC(O)[CH2]
3: [C]C(=O)CC(O)C(O)C[O]>>[O]CC(O)C=O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)C(=O)CC(O)C(O)COP(=O)(O)O>>O=C(O)C(=O)C]
2: R:M00001, P:M00003	[O=C(O)C(=O)CC(O)C(O)COP(=O)(O)O>>O=CC(O)COP(=O)(O)O, O=C(O)C(=O)CC(O)C(O)COP(=O)(O)O>>O=CC(O)COP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[C:4](=[O:5])[CH2:6][CH:7]([OH:8])[CH:9]([OH:10])[CH2:11][O:12][P:13](=[O:14])([OH:15])[OH:16]>>[O:8]=[CH:7][CH:9]([OH:10])[CH2:11][O:12][P:13](=[O:14])([OH:15])[OH:16].[O:1]=[C:2]([OH:3])[C:4](=[O:5])[CH3:6]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=9, 4=11, 5=12, 6=13, 7=14, 8=15, 9=16, 10=10, 11=8, 12=4, 13=5, 14=2, 15=1, 16=3}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=16, 2=14, 3=15, 4=12, 5=11, 6=13, 7=5, 8=3, 9=2, 10=1, 11=4, 12=6, 13=7, 14=8, 15=9, 16=10}

