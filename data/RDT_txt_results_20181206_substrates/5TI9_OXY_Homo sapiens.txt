
//
FINGERPRINTS BC

FORMED_CLEAVED
[C=O:2.0, C@C:1.0, O=O:1.0]

//
FINGERPRINTS RC
[O=C[NH]:2.0, O=O:4.0, O=O>>O=C[NH]:1.0, O=O>>O=[CH]:1.0, O=O>>[C]=O:1.0, O=O>>[C]C(=O)[CH2]:1.0, O=O>>[C]C(=[CH])C(=O)C[CH]:1.0, O=O>>[C]NC=O:1.0, O=[CH]:1.0, [CH]:2.0, [CH]CC1=CN[C]C1=[CH]:1.0, [CH]CC1=CN[C]C1=[CH]>>[C]C(=[CH])C(=O)C[CH]:1.0, [CH]Cc1c[nH]c(=[CH])c1=[CH]>>[C]C(=[CH])NC=O:1.0, [C]:2.0, [C]1[C]C([CH2])=CN1:1.0, [C]1[C]C([CH2])=CN1>>[C]NC=O:1.0, [C]=C[NH]:1.0, [C]=C[NH]>>O=C[NH]:1.0, [C]=O:1.0, [C]C(=O)[CH2]:2.0, [C]C(=[CH])C(=O)C[CH]:1.0, [C]C(=[CH])[CH2]:1.0, [C]C(=[CH])[CH2]>>[C]C(=O)[CH2]:1.0, [C]C(N)Cc1c[nH]c(=[CH])c1=C[CH]>>[C]C(N)CC(=O)C(=C[CH])C(=[CH])[NH]:1.0, [C]NC=O:1.0, [O]:4.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [C]:2.0, [O]:4.0]


ID=Reaction Center at Level: 1 (7)
[O=C[NH]:1.0, O=O:2.0, O=[CH]:1.0, [C]=C[NH]:1.0, [C]=O:1.0, [C]C(=O)[CH2]:1.0, [C]C(=[CH])[CH2]:1.0]


ID=Reaction Center at Level: 2 (7)
[O=C[NH]:1.0, O=O:2.0, [CH]CC1=CN[C]C1=[CH]:1.0, [C]1[C]C([CH2])=CN1:1.0, [C]C(=O)[CH2]:1.0, [C]C(=[CH])C(=O)C[CH]:1.0, [C]NC=O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]=C[NH]>>O=C[NH]
2: [C]C(=[CH])[CH2]>>[C]C(=O)[CH2]
3: O=O>>O=[CH]
4: O=O>>[C]=O

MMP Level 2
1: [C]1[C]C([CH2])=CN1>>[C]NC=O
2: [CH]CC1=CN[C]C1=[CH]>>[C]C(=[CH])C(=O)C[CH]
3: O=O>>O=C[NH]
4: O=O>>[C]C(=O)[CH2]

MMP Level 3
1: [CH]Cc1c[nH]c(=[CH])c1=[CH]>>[C]C(=[CH])NC=O
2: [C]C(N)Cc1c[nH]c(=[CH])c1=C[CH]>>[C]C(N)CC(=O)C(=C[CH])C(=[CH])[NH]
3: O=O>>[C]NC=O
4: O=O>>[C]C(=[CH])C(=O)C[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)Cc1c[nH]c2ccccc21>>O=CNc1ccccc1C(=O)CC(N)C(=O)O, O=C(O)C(N)Cc1c[nH]c2ccccc21>>O=CNc1ccccc1C(=O)CC(N)C(=O)O]
2: R:M00002, P:M00003	[O=O>>O=CNc1ccccc1C(=O)CC(N)C(=O)O, O=O>>O=CNc1ccccc1C(=O)CC(N)C(=O)O]


//
SELECTED AAM MAPPING
[O:16]=[O:17].[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][C:7]:1:[CH:8]:[NH:9]:[C:10]:2:[CH:11]:[CH:12]:[CH:13]:[CH:14]:[C:15]21>>[O:16]=[CH:8][NH:9][C:10]:1:[CH:11]:[CH:12]:[CH:13]:[CH:14]:[C:15]1[C:7](=[O:17])[CH2:6][CH:4]([NH2:5])[C:2](=[O:1])[OH:3]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=13, 2=12, 3=11, 4=10, 5=15, 6=14, 7=7, 8=8, 9=9, 10=6, 11=4, 12=2, 13=1, 14=3, 15=5, 16=16, 17=17}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=7, 2=6, 3=5, 4=4, 5=9, 6=8, 7=10, 8=11, 9=12, 10=13, 11=15, 12=16, 13=17, 14=14, 15=3, 16=2, 17=1}

