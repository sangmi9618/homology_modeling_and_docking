
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-S:1.0, O-S:1.0]

//
FINGERPRINTS RC
[[CH]:1.0, [C]:1.0, [C]C(=[CH])C(=C[CH])S(=O)O:1.0, [C]C(=[CH])C(=C[CH])S(=O)O>>[C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])C(=C[CH])S(=O)O>>[O-]S([O-])=O:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])S(=O)O:1.0, [C]C(=[CH])S(=O)O>>[O-]S([O-])=O:2.0, [C]C(=[CH])c1ccccc1S(=O)O>>[C]C(=[CH])c1ccccc1:1.0, [C]C([S])=[CH]:1.0, [C]C([S])=[CH]>>[C]C=[CH]:1.0, [C]C=[CH]:1.0, [C]S(=O)O:2.0, [C]S(=O)O>>[O-]S([O-])=O:2.0, [O-]:1.0, [O-]S([O-])=O:3.0, [O-][S]:1.0, [OH]:1.0, [S]:2.0, [S]O:1.0, [S]O>>[O-][S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[C]:1.0, [O-]:1.0, [S]:2.0]


ID=Reaction Center at Level: 1 (4)
[[C]C([S])=[CH]:1.0, [C]S(=O)O:1.0, [O-]S([O-])=O:1.0, [O-][S]:1.0]


ID=Reaction Center at Level: 2 (3)
[[C]C(=[CH])C(=C[CH])S(=O)O:1.0, [C]C(=[CH])S(=O)O:1.0, [O-]S([O-])=O:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [S]O>>[O-][S]
2: [C]C([S])=[CH]>>[C]C=[CH]
3: [C]S(=O)O>>[O-]S([O-])=O

MMP Level 2
1: [C]S(=O)O>>[O-]S([O-])=O
2: [C]C(=[CH])C(=C[CH])S(=O)O>>[C]C(=[CH])C=C[CH]
3: [C]C(=[CH])S(=O)O>>[O-]S([O-])=O

MMP Level 3
1: [C]C(=[CH])S(=O)O>>[O-]S([O-])=O
2: [C]C(=[CH])c1ccccc1S(=O)O>>[C]C(=[CH])c1ccccc1
3: [C]C(=[CH])C(=C[CH])S(=O)O>>[O-]S([O-])=O


//
REACTION MMP (RPAIR)
1: R:M00002, P:M00003	[O=S(O)c1ccccc1-c2ccccc2O>>Oc1ccccc1-c2ccccc2]
2: R:M00002, P:M00004	[O=S(O)c1ccccc1-c2ccccc2O>>[O-]S([O-])=O, O=S(O)c1ccccc1-c2ccccc2O>>[O-]S([O-])=O]


//
SELECTED AAM MAPPING
[O:1]=[S:2]([OH:3])[C:4]:1:[CH:5]:[CH:6]:[CH:7]:[CH:8]:[C:9]1[C:10]:2:[CH:11]:[CH:12]:[CH:13]:[CH:14]:[C:15]2[OH:16].[OH2:17]>>[O:1]=[S:2]([O-:3])[O-:17].[OH:16][C:15]:1:[CH:14]:[CH:13]:[CH:12]:[CH:11]:[C:10]1[C:9]:2:[CH:4]:[CH:5]:[CH:6]:[CH:7]:[CH:8]2


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=17, 2=12, 3=13, 4=14, 5=15, 6=10, 7=11, 8=9, 9=8, 10=7, 11=6, 12=5, 13=4, 14=2, 15=1, 16=3, 17=16}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=11, 2=10, 3=9, 4=8, 5=13, 6=12, 7=7, 8=6, 9=5, 10=4, 11=3, 12=2, 13=1, 14=16, 15=15, 16=14, 17=17}

