
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C=O:1.0, O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)O:3.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O:1.0, O=c1nc[nH]c2[N]C=Nc12>>[C]C([CH2])Nc1ncnc2[N]C=Nc21:1.0, [CH]N:1.0, [CH]N>>[C]N[CH]:1.0, [CH]n1cnc2[C]N=CNc21>>[CH]n1cnc2[C]=NC=Nc21:1.0, [C]:2.0, [C]=C([N])C(=N[CH])N[CH]:1.0, [C]=C([N])C(=O)N=[CH]:1.0, [C]=C([N])C(=O)N=[CH]>>O=P(O)(O)O:1.0, [C]=C([N])C(=O)N=[CH]>>[C]=C([N])C(=N[CH])N[CH]:1.0, [C]=C([N])N=C[N]:1.0, [C]=C([N])NC=[N]:1.0, [C]=C([N])NC=[N]>>[C]=C([N])N=C[N]:1.0, [C]=O:1.0, [C]=O>>[P]O:1.0, [C]C(=[N])NC([C])[CH2]:1.0, [C]C(=[N])[NH]:1.0, [C]C([CH2])N:1.0, [C]C([CH2])N>>[C]C(=[N])NC([C])[CH2]:1.0, [C]C([N])=O:2.0, [C]C([N])=O>>O=P(O)(O)O:1.0, [C]C([N])=O>>[C]C(=[N])[NH]:1.0, [C]CC(N)C(=O)O>>[C]CC(NC(=N[CH])C(=[C])[N])C(=O)O:1.0, [C]N=[CH]:1.0, [C]N[CH]:2.0, [C]N[CH]>>[C]N=[CH]:1.0, [NH2]:1.0, [NH]:2.0, [N]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[C]:2.0, [NH]:1.0, [OH]:1.0, [O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (8)
[O=P(O)(O)O:1.0, [C]=O:1.0, [C]C(=[N])[NH]:1.0, [C]C([N])=O:1.0, [C]N[CH]:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (7)
[O=P(O)(O)O:2.0, O=P(O)(O)O[P]:1.0, [C]=C([N])C(=N[CH])N[CH]:1.0, [C]=C([N])C(=O)N=[CH]:1.0, [C]C(=[N])NC([C])[CH2]:1.0, [C]C([N])=O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH]N>>[C]N[CH]
2: [P]O[P]>>[P]O
3: [O]P(=O)(O)O>>O=P(O)(O)O
4: [C]=O>>[P]O
5: [C]N[CH]>>[C]N=[CH]
6: [C]C([N])=O>>[C]C(=[N])[NH]

MMP Level 2
1: [C]C([CH2])N>>[C]C(=[N])NC([C])[CH2]
2: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
3: O=P(O)(O)O[P]>>O=P(O)(O)O
4: [C]C([N])=O>>O=P(O)(O)O
5: [C]=C([N])NC=[N]>>[C]=C([N])N=C[N]
6: [C]=C([N])C(=O)N=[CH]>>[C]=C([N])C(=N[CH])N[CH]

MMP Level 3
1: [C]CC(N)C(=O)O>>[C]CC(NC(=N[CH])C(=[C])[N])C(=O)O
2: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
3: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O
4: [C]=C([N])C(=O)N=[CH]>>O=P(O)(O)O
5: [CH]n1cnc2[C]N=CNc21>>[CH]n1cnc2[C]=NC=Nc21
6: O=c1nc[nH]c2[N]C=Nc12>>[C]C([CH2])Nc1ncnc2[N]C=Nc21


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C3O>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)O)C(O)C3O]
2: R:M00001, P:M00005	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C3O>>O=P(O)(O)O]
3: R:M00002, P:M00005	[O=c1nc[nH]c2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O>>O=P(O)(O)O]
4: R:M00002, P:M00006	[O=c1nc[nH]c2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O>>O=C(O)CC(Nc1ncnc2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O)C(=O)O, O=c1nc[nH]c2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O>>O=C(O)CC(Nc1ncnc2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O)C(=O)O]
5: R:M00003, P:M00006	[O=C(O)CC(N)C(=O)O>>O=C(O)CC(Nc1ncnc2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O)C(=O)O]


//
SELECTED AAM MAPPING
[O:56]=[C:57]([OH:58])[CH2:59][CH:60]([NH2:61])[C:62](=[O:63])[OH:64].[O:33]=[C:34]1[N:35]=[CH:36][NH:37][C:38]:2:[C:39]1:[N:40]:[CH:41]:[N:42]2[CH:43]3[O:44][CH:45]([CH2:46][O:47][P:48](=[O:49])([OH:50])[OH:51])[CH:52]([OH:53])[CH:54]3[OH:55].[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[C:8]1:[N:9]:[CH:10]:[N:11]2[CH:12]3[O:13][CH:14]([CH2:15][O:16][P:17](=[O:18])([OH:19])[O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[OH:28])[CH:29]([OH:30])[CH:31]3[OH:32]>>[O:56]=[C:57]([OH:58])[CH2:59][CH:60]([NH:61][C:34]:1:[N:35]:[CH:36]:[N:37]:[C:38]:2:[C:39]1:[N:40]:[CH:41]:[N:42]2[CH:43]3[O:44][CH:45]([CH2:46][O:47][P:48](=[O:49])([OH:50])[OH:51])[CH:52]([OH:53])[CH:54]3[OH:55])[C:62](=[O:63])[OH:64].[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[C:8]1:[N:9]:[CH:10]:[N:11]2[CH:12]3[O:13][CH:14]([CH2:15][O:16][P:17](=[O:18])([OH:19])[O:20][P:21](=[O:22])([OH:23])[OH:24])[CH:29]([OH:30])[CH:31]3[OH:32].[O:26]=[P:25]([OH:27])([OH:28])[OH:33]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=10, 2=9, 3=8, 4=7, 5=11, 6=12, 7=31, 8=29, 9=14, 10=13, 11=15, 12=16, 13=17, 14=18, 15=19, 16=20, 17=21, 18=22, 19=23, 20=24, 21=25, 22=26, 23=27, 24=28, 25=30, 26=32, 27=6, 28=4, 29=3, 30=2, 31=1, 32=5, 33=36, 34=35, 35=34, 36=33, 37=39, 38=38, 39=37, 40=42, 41=41, 42=40, 43=43, 44=54, 45=52, 46=45, 47=44, 48=46, 49=47, 50=48, 51=49, 52=50, 53=51, 54=53, 55=55, 56=59, 57=60, 58=62, 59=63, 60=64, 61=61, 62=57, 63=56, 64=58}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=41, 2=40, 3=39, 4=38, 5=42, 6=43, 7=58, 8=56, 9=45, 10=44, 11=46, 12=47, 13=48, 14=49, 15=50, 16=51, 17=52, 18=53, 19=54, 20=55, 21=57, 22=59, 23=37, 24=35, 25=34, 26=33, 27=32, 28=36, 29=62, 30=61, 31=60, 32=63, 33=64, 34=9, 35=10, 36=11, 37=12, 38=7, 39=8, 40=6, 41=5, 42=4, 43=2, 44=1, 45=3, 46=29, 47=30, 48=31, 49=13, 50=14, 51=15, 52=16, 53=27, 54=25, 55=18, 56=17, 57=19, 58=20, 59=21, 60=22, 61=23, 62=24, 63=26, 64=28}

