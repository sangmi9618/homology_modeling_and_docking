
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-S:2.0]

//
FINGERPRINTS RC
[O=P(O)(O[CH2])OS(=O)(=O)O>>O=P(O)(O)O[CH2]:1.0, O=S(=O)(O)OC([CH])=[CH]:1.0, O=S(=O)(O)O[P]:1.0, O=S(=O)(O)O[P]>>[C]OS(=O)(=O)O:1.0, [CH]C(=[CH])O:1.0, [CH]C(=[CH])O>>O=S(=O)(O)OC([CH])=[CH]:1.0, [C]C=C(O)C=[CH]>>[C]C=C(OS(=O)(=O)O)C=[CH]:1.0, [C]O:1.0, [C]O>>[C]O[S]:1.0, [C]OS(=O)(=O)O:1.0, [C]O[S]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OS(=O)(=O)O:1.0, [O]P(=O)(O)OS(=O)(=O)O>>O=S(=O)(O)OC([CH])=[CH]:1.0, [O]P(=O)(O)OS(=O)(=O)O>>[O]P(=O)(O)O:1.0, [O]S(=O)(=O)O:2.0, [O]S(=O)(=O)O>>[O]S(=O)(=O)O:1.0, [P]O:1.0, [P]O[S]:1.0, [P]O[S]>>[P]O:1.0, [S]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [S]:2.0]


ID=Reaction Center at Level: 1 (3)
[[C]O[S]:1.0, [O]S(=O)(=O)O:2.0, [P]O[S]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=S(=O)(O)OC([CH])=[CH]:1.0, O=S(=O)(O)O[P]:1.0, [C]OS(=O)(=O)O:1.0, [O]P(=O)(O)OS(=O)(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[S]>>[P]O
2: [O]S(=O)(=O)O>>[O]S(=O)(=O)O
3: [C]O>>[C]O[S]

MMP Level 2
1: [O]P(=O)(O)OS(=O)(=O)O>>[O]P(=O)(O)O
2: O=S(=O)(O)O[P]>>[C]OS(=O)(=O)O
3: [CH]C(=[CH])O>>O=S(=O)(O)OC([CH])=[CH]

MMP Level 3
1: O=P(O)(O[CH2])OS(=O)(=O)O>>O=P(O)(O)O[CH2]
2: [O]P(=O)(O)OS(=O)(=O)O>>O=S(=O)(O)OC([CH])=[CH]
3: [C]C=C(O)C=[CH]>>[C]C=C(OS(=O)(=O)O)C=[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OC1C(O)C(OC1COP(=O)(O)OS(=O)(=O)O)n2cnc3c(ncnc32)N>>O=P(O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O]
2: R:M00001, P:M00004	[O=P(O)(O)OC1C(O)C(OC1COP(=O)(O)OS(=O)(=O)O)n2cnc3c(ncnc32)N>>O=S(=O)(O)Oc1ccc2c(c1)CCC3C2CCC4(C)C(O)CCC34]
3: R:M00002, P:M00004	[Oc1ccc2c(c1)CCC3C2CCC4(C)C(O)CCC34>>O=S(=O)(O)Oc1ccc2c(c1)CCC3C2CCC4(C)C(O)CCC34]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH:6]1[CH:7]([OH:8])[CH:9]([O:10][CH:11]1[CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][S:18](=[O:19])(=[O:20])[OH:21])[N:22]:2:[CH:23]:[N:24]:[C:25]:3:[C:26](:[N:27]:[CH:28]:[N:29]:[C:30]32)[NH2:31].[OH:32][C:33]:1:[CH:34]:[CH:35]:[C:36]2:[C:37](:[CH:38]1)[CH2:39][CH2:40][CH:41]3[CH:42]2[CH2:43][CH2:44][C:45]4([CH3:46])[CH:47]([OH:48])[CH2:49][CH2:50][CH:51]34>>[O:15]=[P:14]([OH:16])([OH:17])[O:13][CH2:12][CH:11]1[O:10][CH:9]([N:22]:2:[CH:23]:[N:24]:[C:25]:3:[C:26](:[N:27]:[CH:28]:[N:29]:[C:30]32)[NH2:31])[CH:7]([OH:8])[CH:6]1[O:5][P:2](=[O:1])([OH:3])[OH:4].[O:19]=[S:18](=[O:20])([OH:21])[O:32][C:33]:1:[CH:34]:[CH:35]:[C:36]2:[C:37](:[CH:38]1)[CH2:39][CH2:40][CH:41]3[CH:42]2[CH2:43][CH2:44][C:45]4([CH3:46])[CH:47]([OH:48])[CH2:49][CH2:50][CH:51]34


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=28, 2=29, 3=30, 4=25, 5=26, 6=27, 7=31, 8=24, 9=23, 10=22, 11=9, 12=7, 13=6, 14=11, 15=10, 16=12, 17=13, 18=14, 19=15, 20=16, 21=17, 22=18, 23=19, 24=20, 25=21, 26=5, 27=2, 28=1, 29=3, 30=4, 31=8, 32=46, 33=45, 34=44, 35=43, 36=42, 37=41, 38=51, 39=50, 40=49, 41=47, 42=48, 43=40, 44=39, 45=37, 46=36, 47=35, 48=34, 49=33, 50=38, 51=32}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=16, 2=17, 3=18, 4=13, 5=14, 6=15, 7=19, 8=12, 9=11, 10=10, 11=9, 12=20, 13=22, 14=7, 15=8, 16=6, 17=5, 18=2, 19=1, 20=3, 21=4, 22=23, 23=24, 24=25, 25=26, 26=27, 27=21, 28=46, 29=45, 30=44, 31=43, 32=42, 33=41, 34=51, 35=50, 36=49, 37=47, 38=48, 39=40, 40=39, 41=37, 42=36, 43=35, 44=34, 45=33, 46=38, 47=32, 48=29, 49=28, 50=30, 51=31}

