
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

STEREO_CHANGED
[C(R/S):4.0]

//
FINGERPRINTS RC
[O=P(O)(O)OC([CH])[CH]:1.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[CH]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O[CH]:1.0, [CH]:8.0, [CH]C([CH])O:1.0, [CH]C([CH])O>>O=P(O)(O)OC([CH])[CH]:1.0, [CH]O:1.0, [CH]O>>[P]O[CH]:1.0, [OH]:2.0, [O]:2.0, [O]C([CH])C(O)C([O])[CH]>>[O]C([CH])C(OP(=O)(O)O)C([O])[CH]:1.0, [O]C([CH])C(O[P])C([CH])O:2.0, [O]C([CH])C(O[P])C([CH])O>>[O]C([CH])C(O[P])C([O])[CH]:2.0, [O]C([CH])C(O[P])C([O])[CH]:6.0, [O]C([CH])C(O[P])C([O])[CH]>>[O]C([CH])C(O[P])C([O])[CH]:2.0, [O]C([CH])[CH]:8.0, [O]C([CH])[CH]>>[O]C([CH])[CH]:4.0, [O]C1[CH]C(O)C(O[P])C(OP(=O)(O)O)C1O[P]>>[O]C1[CH]C([O])C(O[P])C(OP(=O)(O)O)C1O[P]:2.0, [O]C1[CH]C([O])C(O[P])C(OP(=O)(O)O)C1O>>[O]C1[CH]C([O])C(O[P])C(OP(=O)(O)O)C1O[P]:2.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC([CH])[CH]:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[O]P(=O)(O)O:2.0, [P]O[CH]:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)OC([CH])[CH]:1.0, O=P(O)(O)O[CH]:1.0, O=P(O)(O)O[P]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:8.0]


ID=Reaction Center at Level: 1 (1)
[[O]C([CH])[CH]:8.0]


ID=Reaction Center at Level: 2 (2)
[[O]C([CH])C(O[P])C([CH])O:2.0, [O]C([CH])C(O[P])C([O])[CH]:6.0]



//
TRANSFORMATIONS
MMP Level 1
1: [O]C([CH])[CH]>>[O]C([CH])[CH]
2: [CH]O>>[P]O[CH]
3: [P]O[P]>>[P]O
4: [O]P(=O)(O)O>>[O]P(=O)(O)O
5: [O]C([CH])[CH]>>[O]C([CH])[CH]

MMP Level 2
1: [O]C([CH])C(O[P])C([CH])O>>[O]C([CH])C(O[P])C([O])[CH]
2: [CH]C([CH])O>>O=P(O)(O)OC([CH])[CH]
3: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
4: O=P(O)(O)O[P]>>O=P(O)(O)O[CH]
5: [O]C([CH])C(O[P])C([O])[CH]>>[O]C([CH])C(O[P])C([O])[CH]

MMP Level 3
1: [O]C1[CH]C([O])C(O[P])C(OP(=O)(O)O)C1O>>[O]C1[CH]C([O])C(O[P])C(OP(=O)(O)O)C1O[P]
2: [O]C([CH])C(O)C([O])[CH]>>[O]C([CH])C(OP(=O)(O)O)C([O])[CH]
3: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
4: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC([CH])[CH]
5: [O]C1[CH]C(O)C(O[P])C(OP(=O)(O)O)C1O[P]>>[O]C1[CH]C([O])C(O[P])C(OP(=O)(O)O)C1O[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OC1C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)O)C1OP(=O)(O)O]
3: R:M00002, P:M00004	[O=P(O)(O)OC1C(O)C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)O)C1OP(=O)(O)O>>O=P(O)(O)OC1C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)O)C1OP(=O)(O)O, O=P(O)(O)OC1C(O)C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)O)C1OP(=O)(O)O>>O=P(O)(O)OC1C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)O)C1OP(=O)(O)O, O=P(O)(O)OC1C(O)C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)O)C1OP(=O)(O)O>>O=P(O)(O)OC1C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)O)C(OP(=O)(O)O)C1OP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH:6]1[CH:7]([OH:8])[CH:9]([O:10][P:11](=[O:12])([OH:13])[OH:14])[CH:15]([O:16][P:17](=[O:18])([OH:19])[OH:20])[CH:21]([O:22][P:23](=[O:24])([OH:25])[OH:26])[CH:27]1[O:28][P:29](=[O:30])([OH:32])[OH:31].[O:33]=[P:34]([OH:35])([OH:36])[O:37][P:38](=[O:39])([OH:40])[O:41][P:42](=[O:43])([OH:44])[O:45][CH2:46][CH:47]1[O:48][CH:49]([N:50]:2:[CH:51]:[N:52]:[C:53]:3:[C:54](:[N:55]:[CH:56]:[N:57]:[C:58]32)[NH2:59])[CH:60]([OH:61])[CH:62]1[OH:63]>>[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH:6]1[CH:7]([O:8][P:34](=[O:33])([OH:35])[OH:36])[CH:9]([O:10][P:11](=[O:12])([OH:13])[OH:14])[CH:15]([O:16][P:17](=[O:18])([OH:19])[OH:20])[CH:21]([O:22][P:23](=[O:24])([OH:25])[OH:26])[CH:27]1[O:28][P:29](=[O:30])([OH:32])[OH:31].[O:39]=[P:38]([OH:37])([OH:40])[O:41][P:42](=[O:43])([OH:44])[O:45][CH2:46][CH:47]1[O:48][CH:49]([N:50]:2:[CH:51]:[N:52]:[C:53]:3:[C:54](:[N:55]:[CH:56]:[N:57]:[C:58]32)[NH2:59])[CH:60]([OH:61])[CH:62]1[OH:63]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=56, 2=57, 3=58, 4=53, 5=54, 6=55, 7=59, 8=52, 9=51, 10=50, 11=49, 12=60, 13=62, 14=47, 15=48, 16=46, 17=45, 18=42, 19=43, 20=44, 21=41, 22=38, 23=39, 24=40, 25=37, 26=34, 27=33, 28=35, 29=36, 30=63, 31=61, 32=9, 33=15, 34=21, 35=27, 36=6, 37=7, 38=8, 39=5, 40=2, 41=1, 42=3, 43=4, 44=28, 45=29, 46=30, 47=31, 48=32, 49=22, 50=23, 51=24, 52=25, 53=26, 54=16, 55=17, 56=18, 57=19, 58=20, 59=10, 60=11, 61=12, 62=13, 63=14}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=56, 2=57, 3=58, 4=53, 5=54, 6=55, 7=59, 8=52, 9=51, 10=50, 11=49, 12=60, 13=62, 14=47, 15=48, 16=46, 17=45, 18=42, 19=43, 20=44, 21=41, 22=38, 23=37, 24=39, 25=40, 26=63, 27=61, 28=31, 29=25, 30=19, 31=13, 32=7, 33=6, 34=5, 35=2, 36=1, 37=3, 38=4, 39=8, 40=9, 41=10, 42=11, 43=12, 44=14, 45=15, 46=16, 47=17, 48=18, 49=20, 50=21, 51=22, 52=23, 53=24, 54=26, 55=27, 56=28, 57=29, 58=30, 59=32, 60=33, 61=34, 62=35, 63=36}

