
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0]

ORDER_CHANGED
[C-C*C=C:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)CC(O)C(=O)O>>O=C(O)C=CC(=O)O:2.0, [CH2]:1.0, [CH]:3.0, [CH]O:1.0, [CH]O>>O:1.0, [C]C(O)CC(=O)O:1.0, [C]C(O)CC(=O)O>>[C]C=CC(=O)O:1.0, [C]C(O)[CH2]:2.0, [C]C(O)[CH2]>>O:1.0, [C]C(O)[CH2]>>[C]C=[CH]:1.0, [C]C=CC(=O)O:2.0, [C]C=[CH]:2.0, [C]CC(O)C(=O)O:1.0, [C]CC(O)C(=O)O>>O:1.0, [C]CC(O)C(=O)O>>[C]C=CC(=O)O:1.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C=[CH]:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[O:1.0, [CH]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (3)
[O:1.0, [CH]O:1.0, [C]C(O)[CH2]:1.0]


ID=Reaction Center at Level: 2 (3)
[O:1.0, [C]C(O)[CH2]:1.0, [C]CC(O)C(=O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (2)
[[CH2]:1.0, [CH]:3.0]


ID=Reaction Center at Level: 1 (3)
[[C]C(O)[CH2]:1.0, [C]C=[CH]:2.0, [C]C[CH]:1.0]


ID=Reaction Center at Level: 2 (3)
[[C]C(O)CC(=O)O:1.0, [C]C=CC(=O)O:2.0, [C]CC(O)C(=O)O:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH]O>>O
2: [C]C[CH]>>[C]C=[CH]
3: [C]C(O)[CH2]>>[C]C=[CH]

MMP Level 2
1: [C]C(O)[CH2]>>O
2: [C]C(O)CC(=O)O>>[C]C=CC(=O)O
3: [C]CC(O)C(=O)O>>[C]C=CC(=O)O

MMP Level 3
1: [C]CC(O)C(=O)O>>O
2: O=C(O)CC(O)C(=O)O>>O=C(O)C=CC(=O)O
3: O=C(O)CC(O)C(=O)O>>O=C(O)C=CC(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)CC(O)C(=O)O>>O=C(O)C=CC(=O)O, O=C(O)CC(O)C(=O)O>>O=C(O)C=CC(=O)O]
2: R:M00001, P:M00003	[O=C(O)CC(O)C(=O)O>>O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH2:4][CH:5]([OH:6])[C:7](=[O:8])[OH:9]>>[O:1]=[C:2]([OH:3])[CH:4]=[CH:5][C:7](=[O:8])[OH:9].[OH2:6]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=4, 2=5, 3=7, 4=8, 5=9, 6=6, 7=2, 8=1, 9=3}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=5, 2=4, 3=2, 4=1, 5=3, 6=6, 7=7, 8=8, 9=9}

