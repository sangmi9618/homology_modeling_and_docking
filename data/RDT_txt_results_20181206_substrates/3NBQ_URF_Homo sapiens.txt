
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O=C1N[C]C(F)=CN1>>O=C1N[C]C(F)=CN1C2O[CH][CH]C2O:1.0, O=P(O)(O)O:1.0, O=P(O)(O)OC1OC([CH2])C(O)C1O>>[C]=CN(C(=O)[NH])C1OC([CH2])C(O)C1O:1.0, O=P(O)(O)OC1O[CH][CH]C1O>>O=P(O)(O)O:1.0, [CH]:2.0, [C]=CN(C(=O)[NH])C([O])[CH]:1.0, [C]=CNC(=O)[NH]:1.0, [C]=CNC(=O)[NH]>>[C]=CN(C(=O)[NH])C([O])[CH]:1.0, [C]N([CH])C1O[CH][CH]C1O:1.0, [C]N([CH])[CH]:1.0, [C]N[CH]:1.0, [C]N[CH]>>[C]N([CH])[CH]:1.0, [NH]:1.0, [N]:1.0, [N]C([O])[CH]:1.0, [OH]:1.0, [O]:1.0, [O]C([CH])OP(=O)(O)O:1.0, [O]C([CH])OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]C([O])[CH]:1.0, [O]C([O])[CH]>>[N]C([O])[CH]:1.0, [P]O:1.0, [P]OC1O[CH][CH]C1O:1.0, [P]OC1O[CH][CH]C1O>>[C]N([CH])C1O[CH][CH]C1O:1.0, [P]O[CH]:1.0, [P]O[CH]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [N]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[C]N([CH])[CH]:1.0, [N]C([O])[CH]:1.0, [O]C([O])[CH]:1.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]=CN(C(=O)[NH])C([O])[CH]:1.0, [C]N([CH])C1O[CH][CH]C1O:1.0, [O]C([CH])OP(=O)(O)O:1.0, [P]OC1O[CH][CH]C1O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]C([O])[CH]>>[N]C([O])[CH]
2: [C]N[CH]>>[C]N([CH])[CH]
3: [P]O[CH]>>[P]O

MMP Level 2
1: [P]OC1O[CH][CH]C1O>>[C]N([CH])C1O[CH][CH]C1O
2: [C]=CNC(=O)[NH]>>[C]=CN(C(=O)[NH])C([O])[CH]
3: [O]C([CH])OP(=O)(O)O>>O=P(O)(O)O

MMP Level 3
1: O=P(O)(O)OC1OC([CH2])C(O)C1O>>[C]=CN(C(=O)[NH])C1OC([CH2])C(O)C1O
2: O=C1N[C]C(F)=CN1>>O=C1N[C]C(F)=CN1C2O[CH][CH]C2O
3: O=P(O)(O)OC1O[CH][CH]C1O>>O=P(O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1[nH]cc(F)c(=O)[nH]1>>O=c1[nH]c(=O)n(cc1F)C2OC(CO)C(O)C2O]
2: R:M00002, P:M00003	[O=P(O)(O)OC1OC(CO)C(O)C1O>>O=c1[nH]c(=O)n(cc1F)C2OC(CO)C(O)C2O]
3: R:M00002, P:M00004	[O=P(O)(O)OC1OC(CO)C(O)C1O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[O:15]=[C:16]1[NH:17][CH:18]=[C:19]([F:20])[C:21](=[O:22])[NH:23]1.[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH:6]1[O:7][CH:8]([CH2:9][OH:10])[CH:11]([OH:12])[CH:13]1[OH:14]>>[O:22]=[C:21]1[NH:23][C:16](=[O:15])[N:17]([CH:18]=[C:19]1[F:20])[CH:6]2[O:7][CH:8]([CH2:9][OH:10])[CH:11]([OH:12])[CH:13]2[OH:14].[O:1]=[P:2]([OH:3])([OH:4])[OH:5]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=18, 2=19, 3=21, 4=22, 5=23, 6=16, 7=15, 8=17, 9=20, 10=9, 11=8, 12=11, 13=13, 14=6, 15=7, 16=5, 17=2, 18=1, 19=3, 20=4, 21=14, 22=12, 23=10}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=7, 2=8, 3=2, 4=1, 5=3, 6=4, 7=5, 8=6, 9=10, 10=17, 11=15, 12=12, 13=11, 14=13, 15=14, 16=16, 17=18, 18=9, 19=21, 20=20, 21=19, 22=22, 23=23}

