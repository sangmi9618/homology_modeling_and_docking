
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0, C-O:1.0]

ORDER_CHANGED
[C-C*C=C:2.0]

//
FINGERPRINTS RC
[[CH2]:5.0, [CH2]C(=C)C:2.0, [CH2]C(=C)C>>[CH]=C(C)C:2.0, [CH2]C=C(C)C:1.0, [CH2]C=C(C)C>>[C]=CCCC(=C[CH2])C:1.0, [CH2]CC(=C)C:1.0, [CH2]CC(=C)C>>[CH2]C=C(C)C:2.0, [CH2]CC=C(C)C:1.0, [CH3]:2.0, [CH]:1.0, [CH]=C(C)C:3.0, [CH]=C(C)C>>[CH]CCC(=[CH])C:1.0, [CH]CCC(=[CH])C:1.0, [CH]C[CH2]:1.0, [C]:2.0, [C]=C:1.0, [C]=C>>[C]C:1.0, [C]=C[CH2]:1.0, [C]C:2.0, [C]C>>[C]C[CH2]:1.0, [C]CCC=[C]:1.0, [C]CCOP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, [C]CCO[P]:1.0, [C]CCO[P]>>[C]CCC=[C]:1.0, [C]C[CH2]:2.0, [C]C[CH2]>>[C]=C[CH2]:1.0, [OH]:1.0, [O]:1.0, [O]CCC(=C)C:1.0, [O]CCC(=C)C>>[CH2]CC=C(C)C:2.0, [O]C[CH2]:1.0, [O]C[CH2]>>[CH]C[CH2]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OCCC(=C)C>>[CH]=C(C)CCC=C(C)C:1.0, [O]P(=O)(O)OC[CH2]:1.0, [O]P(=O)(O)OC[CH2]>>[O]P(=O)(O)O:1.0, [P]O:1.0, [P]OCCC(=C)C>>[C]CCC=C(C)C:1.0, [P]O[CH2]:1.0, [P]O[CH2]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH2]:3.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[CH]C[CH2]:1.0, [C]C[CH2]:1.0, [O]C[CH2]:1.0, [P]O[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[[CH]CCC(=[CH])C:1.0, [C]CCC=[C]:1.0, [C]CCO[P]:1.0, [O]P(=O)(O)OC[CH2]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH2]:2.0, [CH3]:1.0, [CH]:1.0, [C]:2.0]


ID=Reaction Center at Level: 1 (6)
[[CH2]C(=C)C:1.0, [CH]=C(C)C:1.0, [C]=C:1.0, [C]=C[CH2]:1.0, [C]C:1.0, [C]C[CH2]:1.0]


ID=Reaction Center at Level: 2 (6)
[[CH2]C(=C)C:1.0, [CH2]C=C(C)C:1.0, [CH2]CC(=C)C:1.0, [CH2]CC=C(C)C:1.0, [CH]=C(C)C:1.0, [O]CCC(=C)C:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[CH2]>>[P]O
2: [O]C[CH2]>>[CH]C[CH2]
3: [C]C[CH2]>>[C]=C[CH2]
4: [C]=C>>[C]C
5: [C]C>>[C]C[CH2]
6: [CH2]C(=C)C>>[CH]=C(C)C

MMP Level 2
1: [O]P(=O)(O)OC[CH2]>>[O]P(=O)(O)O
2: [C]CCO[P]>>[C]CCC=[C]
3: [O]CCC(=C)C>>[CH2]CC=C(C)C
4: [CH2]C(=C)C>>[CH]=C(C)C
5: [CH]=C(C)C>>[CH]CCC(=[CH])C
6: [CH2]CC(=C)C>>[CH2]C=C(C)C

MMP Level 3
1: [C]CCOP(=O)(O)O[P]>>O=P(O)(O)O[P]
2: [O]P(=O)(O)OCCC(=C)C>>[CH]=C(C)CCC=C(C)C
3: [P]OCCC(=C)C>>[C]CCC=C(C)C
4: [CH2]CC(=C)C>>[CH2]C=C(C)C
5: [CH2]C=C(C)C>>[C]=CCCC(=C[CH2])C
6: [O]CCC(=C)C>>[CH2]CC=C(C)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OCC=C(C)CCC=C(C)C>>O=P(O)(O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)C]
2: R:M00002, P:M00003	[O=P(O)(O)OP(=O)(O)OCCC(=C)C>>O=P(O)(O)OP(=O)(O)O]
3: R:M00002, P:M00004	[O=P(O)(O)OP(=O)(O)OCCC(=C)C>>O=P(O)(O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)C, O=P(O)(O)OP(=O)(O)OCCC(=C)C>>O=P(O)(O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)C, O=P(O)(O)OP(=O)(O)OCCC(=C)C>>O=P(O)(O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)C, O=P(O)(O)OP(=O)(O)OCCC(=C)C>>O=P(O)(O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)C]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][CH2:10][CH:11]=[C:12]([CH3:13])[CH2:14][CH2:15][CH:16]=[C:17]([CH3:18])[CH3:19].[O:20]=[P:21]([OH:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[O:28][CH2:29][CH2:30][C:31](=[CH2:32])[CH3:33]>>[O:20]=[P:21]([OH:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[OH:28].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][CH2:10][CH:11]=[C:12]([CH3:13])[CH2:14][CH2:15][CH:16]=[C:17]([CH3:18])[CH2:19][CH2:29][CH:30]=[C:31]([CH3:32])[CH3:33]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=18, 2=17, 3=16, 4=15, 5=14, 6=12, 7=11, 8=10, 9=9, 10=6, 11=7, 12=8, 13=5, 14=2, 15=1, 16=3, 17=4, 18=13, 19=19, 20=33, 21=31, 22=32, 23=30, 24=29, 25=28, 26=25, 27=26, 28=27, 29=24, 30=21, 31=20, 32=22, 33=23}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=27, 2=26, 3=25, 4=28, 5=29, 6=30, 7=31, 8=32, 9=33, 10=23, 11=22, 12=21, 13=20, 14=19, 15=17, 16=16, 17=15, 18=14, 19=12, 20=11, 21=10, 22=9, 23=6, 24=7, 25=8, 26=5, 27=2, 28=1, 29=3, 30=4, 31=13, 32=18, 33=24}

