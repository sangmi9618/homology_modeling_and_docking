
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O:3.0, O=P(O)(O)O:3.0, O=P(O)(O)OC(=C[CH])C=[CH]>>[CH]C=C(O)C=[CH]:1.0, O=P(O)(O)OC([CH])=[CH]:1.0, O=P(O)(O)OC([CH])=[CH]>>O=P(O)(O)O:1.0, O=P(O)(O)OC([CH])=[CH]>>[CH]C(=[CH])O:1.0, O>>O=P(O)(O)O:2.0, O>>[P]O:1.0, [CH]C(=[CH])O:1.0, [C]O:1.0, [C]OP(=O)(O)O:1.0, [C]OP(=O)(O)O>>O=P(O)(O)O:1.0, [C]O[P]:1.0, [C]O[P]>>[C]O:1.0, [OH]:2.0, [O]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [P]:2.0, [P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=P(O)(O)O:1.0, [C]O[P]:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=P(O)(O)O:2.0, O=P(O)(O)OC([CH])=[CH]:1.0, [C]OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O>>[P]O
2: [C]O[P]>>[C]O
3: [O]P(=O)(O)O>>O=P(O)(O)O

MMP Level 2
1: O>>O=P(O)(O)O
2: O=P(O)(O)OC([CH])=[CH]>>[CH]C(=[CH])O
3: [C]OP(=O)(O)O>>O=P(O)(O)O

MMP Level 3
1: O>>O=P(O)(O)O
2: O=P(O)(O)OC(=C[CH])C=[CH]>>[CH]C=C(O)C=[CH]
3: O=P(O)(O)OC([CH])=[CH]>>O=P(O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)Cc1ccc(OP(=O)(O)O)cc1>>O=C(O)C(N)Cc1ccc(O)cc1]
2: R:M00001, P:M00004	[O=C(O)C(N)Cc1ccc(OP(=O)(O)O)cc1>>O=P(O)(O)O]
3: R:M00002, P:M00004	[O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][C:7]:1:[CH:8]:[CH:9]:[C:10]([O:11][P:12](=[O:13])([OH:14])[OH:15]):[CH:16]:[CH:17]1.[OH2:18]>>[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][C:7]:1:[CH:8]:[CH:9]:[C:10]([OH:11]):[CH:16]:[CH:17]1.[O:13]=[P:12]([OH:15])([OH:18])[OH:14]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=8, 2=9, 3=10, 4=16, 5=17, 6=7, 7=6, 8=4, 9=2, 10=1, 11=3, 12=5, 13=11, 14=12, 15=13, 16=14, 17=15, 18=18}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=8, 2=9, 3=10, 4=12, 5=13, 6=7, 7=6, 8=4, 9=2, 10=1, 11=3, 12=5, 13=11, 14=16, 15=15, 16=14, 17=17, 18=18}

