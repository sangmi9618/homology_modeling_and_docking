
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0, O-P:2.0]

//
FINGERPRINTS RC
[O=C(O)C[CH2]:1.0, O=C(O)C[CH2]>>O=P(O)(O)O[P]:1.0, O=C(O)[CH2]:2.0, O=C(O)[CH2]>>[O]P(=O)(O)O:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]:1.0, O=P(O)(O[P])O[P]:1.0, O=P(O)(O[P])O[P]>>O=P(O)(O)O[P]:1.0, [C]:1.0, [C]O:1.0, [C]O>>[P]O:1.0, [OH]:3.0, [O]:1.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)OP(=O)(O)OP(=O)(O)O>>O=P(O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [O]P([O])(=O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[C]:1.0, [OH]:2.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (6)
[O=C(O)[CH2]:1.0, [C]O:1.0, [O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (6)
[O=C(O)C[CH2]:1.0, O=C(O)[CH2]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[P]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]O>>[P]O
2: [O]P([O])(=O)O>>[O]P(=O)(O)O
3: [P]O[P]>>[P]O

MMP Level 2
1: O=C(O)[CH2]>>[O]P(=O)(O)O
2: O=P(O)(O[P])O[P]>>O=P(O)(O)O[P]
3: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O

MMP Level 3
1: O=C(O)C[CH2]>>O=P(O)(O)O[P]
2: [O]P(=O)(O)OP(=O)(O)OP(=O)(O)O>>O=P(O)(O)OP(=O)(O)O
3: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)O]
3: R:M00002, P:M00004	[O=C1NC2CSC(CCCCC(=O)O)C2N1>>O=P(O)(O)OP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:45]=[C:44]([OH:32])[CH2:43][CH2:42][CH2:41][CH2:40][CH:39]1[S:38][CH2:37][CH:36]2[NH:35][C:34](=[O:33])[NH:47][CH:46]12.[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:11]=[P:10]([OH:9])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[OH:32]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=36, 33=35, 34=46, 35=38, 36=37, 37=39, 38=40, 39=41, 40=42, 41=43, 42=44, 43=45, 44=47, 45=33, 46=32, 47=34}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=16, 2=17, 3=18, 4=13, 5=14, 6=15, 7=19, 8=12, 9=11, 10=10, 11=9, 12=20, 13=22, 14=7, 15=8, 16=6, 17=5, 18=2, 19=1, 20=3, 21=4, 22=23, 23=21, 24=26, 25=25, 26=24, 27=27, 28=28, 29=29, 30=30, 31=31, 32=32}

