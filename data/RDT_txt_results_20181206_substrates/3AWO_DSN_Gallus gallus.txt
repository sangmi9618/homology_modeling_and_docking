
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0, C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[N:3.0, O=C(O)C(=O)C:1.0, O=C(O)C(N)CO:1.0, O=C(O)C(N)CO>>N:1.0, O=C(O)C(N)CO>>O=C(O)C(=O)C:3.0, O[CH2]:1.0, O[CH2]>>[C]=O:1.0, [CH2]:1.0, [CH3]:1.0, [CH]:1.0, [CH]CO:2.0, [CH]CO>>[C]C:1.0, [CH]CO>>[C]C(=O)C:1.0, [CH]N:1.0, [CH]N>>N:1.0, [C]:1.0, [C]=O:1.0, [C]C:1.0, [C]C(=O)C:3.0, [C]C(N)CO:1.0, [C]C(N)CO>>O=C(O)C(=O)C:1.0, [C]C(N)CO>>[C]C(=O)C:1.0, [C]C([CH2])N:2.0, [C]C([CH2])N>>N:1.0, [C]C([CH2])N>>[C]C(=O)C:1.0, [NH2]:1.0, [OH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (7)
[N:1.0, [CH2]:1.0, [CH]:1.0, [C]:1.0, [NH2]:1.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (7)
[N:1.0, O[CH2]:1.0, [CH]CO:1.0, [CH]N:1.0, [C]=O:1.0, [C]C(=O)C:1.0, [C]C([CH2])N:1.0]


ID=Reaction Center at Level: 2 (7)
[N:1.0, O=C(O)C(=O)C:1.0, O=C(O)C(N)CO:1.0, [CH]CO:1.0, [C]C(=O)C:1.0, [C]C(N)CO:1.0, [C]C([CH2])N:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(=O)C:1.0, [C]C([CH2])N:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C(=O)C:1.0, O=C(O)C(N)CO:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]CO>>[C]C
2: [CH]N>>N
3: O[CH2]>>[C]=O
4: [C]C([CH2])N>>[C]C(=O)C

MMP Level 2
1: [C]C(N)CO>>[C]C(=O)C
2: [C]C([CH2])N>>N
3: [CH]CO>>[C]C(=O)C
4: O=C(O)C(N)CO>>O=C(O)C(=O)C

MMP Level 3
1: O=C(O)C(N)CO>>O=C(O)C(=O)C
2: O=C(O)C(N)CO>>N
3: [C]C(N)CO>>O=C(O)C(=O)C
4: O=C(O)C(N)CO>>O=C(O)C(=O)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)C(N)CO>>O=C(O)C(=O)C, O=C(O)C(N)CO>>O=C(O)C(=O)C, O=C(O)C(N)CO>>O=C(O)C(=O)C]
2: R:M00001, P:M00003	[O=C(O)C(N)CO>>N]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][OH:7]>>[O:1]=[C:2]([OH:3])[C:4](=[O:7])[CH3:6].[NH3:5]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=4, 3=2, 4=1, 5=3, 6=5, 7=7}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=4, 3=5, 4=2, 5=1, 6=3, 7=7}

