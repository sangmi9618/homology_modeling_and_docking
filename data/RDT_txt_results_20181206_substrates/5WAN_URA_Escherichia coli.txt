
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%N:1.0, C-O:1.0]

ORDER_CHANGED
[C%N*C%N:1.0, O-O*O=O:1.0]

//
FINGERPRINTS RC
[O=C(OO)C=[CH]:1.0, O=C([CH])NC(=O)[NH]:1.0, O=C([CH])NC(=O)[NH]>>O=C([NH])N:1.0, O=C([CH])OO:1.0, O=C([CH])[NH]:1.0, O=C([CH])[NH]>>[O]C(=O)[CH]:1.0, O=C([NH])N:1.0, O=C1NC(=O)C2=N[C]=C([CH])N(C[CH])C2N1>>O=C1N=C2C(=N[C]=C([CH])N2C[CH])C(=O)N1:1.0, O=O:4.0, O=O>>O=C(OO)C=[CH]:1.0, O=O>>O=C([CH])OO:2.0, O=O>>[C]OO:2.0, O=O>>[O]O:1.0, O=c1cc[nH]c(=O)[nH]1>>O=C(N)N[CH]:1.0, O=c1cc[nH]c(=O)[nH]1>>O=C(OO)C=C[NH]:1.0, [CH]:1.0, [C]:3.0, [C]C([N])=NC(=O)[NH]:1.0, [C]C([N])=[N]:1.0, [C]C([N])NC(=O)[NH]:1.0, [C]C([N])NC(=O)[NH]>>[C]C([N])=NC(=O)[NH]:1.0, [C]C([N])[NH]:1.0, [C]C([N])[NH]>>[C]C([N])=[N]:1.0, [C]N:1.0, [C]N([CH2])C1NC(=O)N[C]C1=[N]>>[C]N([CH2])C1=NC(=O)N[C]C1=[N]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]N=[C]:1.0, [C]NC(=O)C=[CH]:1.0, [C]NC(=O)C=[CH]>>O=C(OO)C=[CH]:1.0, [C]NC(C([C])=[N])N([C])[CH2]:1.0, [C]NC(C([C])=[N])N([C])[CH2]>>[C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]N[CH]:1.0, [C]N[CH]>>[C]N=[C]:1.0, [C]N[C]:1.0, [C]N[C]>>[C]N:1.0, [C]OO:2.0, [NH2]:1.0, [NH]:2.0, [N]:1.0, [OH]:1.0, [O]:3.0, [O]C(=O)[CH]:1.0, [O]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [NH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([CH])[NH]:1.0, [C]N[C]:1.0, [C]OO:1.0, [O]C(=O)[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(OO)C=[CH]:1.0, O=C([CH])NC(=O)[NH]:1.0, O=C([CH])OO:1.0, [C]NC(=O)C=[CH]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (6)
[[CH]:1.0, [C]:1.0, [NH]:1.0, [N]:1.0, [OH]:1.0, [O]:3.0]


ID=Reaction Center at Level: 1 (7)
[O=O:2.0, [C]C([N])=[N]:1.0, [C]C([N])[NH]:1.0, [C]N=[C]:1.0, [C]N[CH]:1.0, [C]OO:1.0, [O]O:1.0]


ID=Reaction Center at Level: 2 (7)
[O=C([CH])OO:1.0, O=O:2.0, [C]C([N])=NC(=O)[NH]:1.0, [C]C([N])NC(=O)[NH]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]NC(C([C])=[N])N([C])[CH2]:1.0, [C]OO:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C([N])[NH]>>[C]C([N])=[N]
2: O=O>>[C]OO
3: O=O>>[O]O
4: [C]N[CH]>>[C]N=[C]
5: [C]N[C]>>[C]N
6: O=C([CH])[NH]>>[O]C(=O)[CH]

MMP Level 2
1: [C]NC(C([C])=[N])N([C])[CH2]>>[C]N=C(C([C])=[N])N([C])[CH2]
2: O=O>>O=C([CH])OO
3: O=O>>[C]OO
4: [C]C([N])NC(=O)[NH]>>[C]C([N])=NC(=O)[NH]
5: O=C([CH])NC(=O)[NH]>>O=C([NH])N
6: [C]NC(=O)C=[CH]>>O=C(OO)C=[CH]

MMP Level 3
1: O=C1NC(=O)C2=N[C]=C([CH])N(C[CH])C2N1>>O=C1N=C2C(=N[C]=C([CH])N2C[CH])C(=O)N1
2: O=O>>O=C(OO)C=[CH]
3: O=O>>O=C([CH])OO
4: [C]N([CH2])C1NC(=O)N[C]C1=[N]>>[C]N([CH2])C1=NC(=O)N[C]C1=[N]
5: O=c1cc[nH]c(=O)[nH]1>>O=C(N)N[CH]
6: O=c1cc[nH]c(=O)[nH]1>>O=C(OO)C=C[NH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=c1cc[nH]c(=O)[nH]1>>O=C(OO)C=CNC(=O)N, O=c1cc[nH]c(=O)[nH]1>>O=C(OO)C=CNC(=O)N]
2: R:M00002, P:M00005	[O=C1NC(=O)C2=Nc3cc(c(cc3N(CC(O)C(O)C(O)COP(=O)(O)O)C2N1)C)C>>O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)O)C)C)c(=O)[nH]1, O=C1NC(=O)C2=Nc3cc(c(cc3N(CC(O)C(O)C(O)COP(=O)(O)O)C2N1)C)C>>O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)O)C)C)c(=O)[nH]1]
3: R:M00003, P:M00004	[O=O>>O=C(OO)C=CNC(=O)N, O=O>>O=C(OO)C=CNC(=O)N]


//
SELECTED AAM MAPPING
[O:40]=[O:41].[O:32]=[C:33]1[CH:34]=[CH:35][NH:36][C:37](=[O:38])[NH:39]1.[O:1]=[C:2]1[NH:3][C:4](=[O:5])[C:6]2=[N:7][C:8]:3:[CH:9]:[C:10](:[C:11](:[CH:12]:[C:13]3[N:14]([CH2:15][CH:16]([OH:17])[CH:18]([OH:19])[CH:20]([OH:21])[CH2:22][O:23][P:24](=[O:25])([OH:26])[OH:27])[CH:28]2[NH:29]1)[CH3:30])[CH3:31]>>[O:32]=[C:33]([O:40][OH:41])[CH:34]=[CH:35][NH:36][C:37](=[O:38])[NH2:39].[O:1]=[C:2]1[N:29]=[C:28]2[C:6](=[N:7][C:8]:3:[CH:9]:[C:10](:[C:11](:[CH:12]:[C:13]3[N:14]2[CH2:15][CH:16]([OH:17])[CH:18]([OH:19])[CH:20]([OH:21])[CH2:22][O:23][P:24](=[O:25])([OH:26])[OH:27])[CH3:30])[CH3:31])[C:4](=[O:5])[NH:3]1


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=34, 2=35, 3=36, 4=37, 5=38, 6=39, 7=33, 8=32, 9=31, 10=10, 11=9, 12=8, 13=13, 14=12, 15=11, 16=30, 17=14, 18=28, 19=6, 20=7, 21=4, 22=5, 23=3, 24=2, 25=1, 26=29, 27=15, 28=16, 29=18, 30=20, 31=22, 32=23, 33=24, 34=25, 35=26, 36=27, 37=21, 38=19, 39=17, 40=40, 41=41}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=36, 2=37, 3=38, 4=39, 5=40, 6=41, 7=33, 8=32, 9=34, 10=35, 11=28, 12=9, 13=8, 14=7, 15=12, 16=11, 17=10, 18=27, 19=13, 20=4, 21=3, 22=2, 23=1, 24=31, 25=29, 26=30, 27=5, 28=6, 29=14, 30=15, 31=17, 32=19, 33=21, 34=22, 35=23, 36=24, 37=25, 38=26, 39=20, 40=18, 41=16}

