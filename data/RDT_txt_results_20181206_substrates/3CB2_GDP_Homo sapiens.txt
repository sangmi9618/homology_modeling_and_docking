
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O:3.0, O=P(O)(O)O:3.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O:1.0, O>>O=P(O)(O)O:2.0, O>>[P]O:1.0, [OH]:2.0, [O]:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=P(O)(O)O:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=P(O)(O)O:2.0, O=P(O)(O)O[P]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[P]>>[P]O
2: O>>[P]O
3: [O]P(=O)(O)O>>O=P(O)(O)O

MMP Level 2
1: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
2: O>>O=P(O)(O)O
3: O=P(O)(O)O[P]>>O=P(O)(O)O

MMP Level 3
1: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
2: O>>O=P(O)(O)O
3: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O>>O=P(O)(O)O]
2: R:M00002, P:M00003	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C3O>>O=P(O)(O)O]
3: R:M00002, P:M00004	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C3O>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)O)C(O)C3O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[C:8]1:[N:9]:[CH:10]:[N:11]2[CH:12]3[O:13][CH:14]([CH2:15][O:16][P:17](=[O:18])([OH:19])[O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[OH:28])[CH:29]([OH:30])[CH:31]3[OH:32].[OH2:33]>>[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[C:8]1:[N:9]:[CH:10]:[N:11]2[CH:12]3[O:13][CH:14]([CH2:15][O:16][P:17](=[O:18])([OH:19])[O:20][P:21](=[O:22])([OH:23])[OH:24])[CH:29]([OH:30])[CH:31]3[OH:32].[O:26]=[P:25]([OH:28])([OH:33])[OH:27]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=33, 2=10, 3=9, 4=8, 5=7, 6=11, 7=12, 8=31, 9=29, 10=14, 11=13, 12=15, 13=16, 14=17, 15=18, 16=19, 17=20, 18=21, 19=22, 20=23, 21=24, 22=25, 23=26, 24=27, 25=28, 26=30, 27=32, 28=6, 29=4, 30=3, 31=2, 32=1, 33=5}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=31, 2=30, 3=29, 4=32, 5=33, 6=10, 7=9, 8=8, 9=7, 10=11, 11=12, 12=27, 13=25, 14=14, 15=13, 16=15, 17=16, 18=17, 19=18, 20=19, 21=20, 22=21, 23=22, 24=23, 25=24, 26=26, 27=28, 28=6, 29=4, 30=3, 31=2, 32=1, 33=5}

