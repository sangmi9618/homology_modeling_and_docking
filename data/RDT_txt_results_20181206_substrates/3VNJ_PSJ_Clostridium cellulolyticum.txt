
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%O:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O=C(CO)C(O)C(O)C(O)[CH2]>>OCC1(O)OC([CH2])C(O)C1O:1.0, O=C(CO)C(O)C([CH])O>>OCC1(O)OC([CH2])C(O)C1O:1.0, O=C(CO)C([CH])O:1.0, O=C(CO)C([CH])O>>OCC1(O)O[CH][CH]C1O:2.0, O=C([CH2])C(O)C(O)C(O)CO>>OCC1OC(O)([CH2])C(O)C1O:1.0, O=C([CH2])C(O)C([CH])O:1.0, O=C([CH2])C(O)C([CH])O>>OC1[CH]OC(O)([CH2])C1O:1.0, O=C([CH])[CH2]:2.0, O=C([CH])[CH2]>>[O]C([CH])(O)[CH2]:2.0, OC1(OC([CH2])[CH][CH]1)[CH2]:1.0, OC1[CH]OC(O)([CH2])C1O:1.0, OC1[C]OC([CH2])C1O:1.0, OCC1(O)O[CH][CH]C1O:1.0, [CH]:4.0, [CH]C(O)C(O)CO>>OCC1OC(O)(CO)C(O)C1O:1.0, [CH]C(O)[CH2]:1.0, [CH]C(O)[CH2]>>OC1(OC([CH2])[CH][CH]1)[CH2]:1.0, [CH]C([CH])O:2.0, [CH]C([CH])O>>[CH]C([CH])O:1.0, [CH]O:1.0, [CH]O>>[C]O[CH]:1.0, [C]:2.0, [C]=O:1.0, [C]=O>>[C]O:1.0, [C]C(O)C(O)C(O)[CH2]:1.0, [C]C(O)C(O)C(O)[CH2]>>OC1[C]OC([CH2])C1O:1.0, [C]C([CH])O:2.0, [C]C([CH])O>>[C]C([CH])O:1.0, [C]O:1.0, [C]O[CH]:1.0, [OH]:2.0, [O]:2.0, [O]C([CH])(O)[CH2]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[C]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]O[CH]:1.0, [O]C([CH])(O)[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[OC1(OC([CH2])[CH][CH]1)[CH2]:1.0, OCC1(O)O[CH][CH]C1O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([CH])[CH2]:1.0, [C]=O:1.0, [C]O:1.0, [O]C([CH])(O)[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(CO)C([CH])O:1.0, O=C([CH])[CH2]:1.0, OCC1(O)O[CH][CH]C1O:1.0, [O]C([CH])(O)[CH2]:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:4.0]


ID=Reaction Center at Level: 1 (2)
[[CH]C([CH])O:2.0, [C]C([CH])O:2.0]


ID=Reaction Center at Level: 2 (4)
[O=C([CH2])C(O)C([CH])O:1.0, OC1[CH]OC(O)([CH2])C1O:1.0, OC1[C]OC([CH2])C1O:1.0, [C]C(O)C(O)C(O)[CH2]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]C([CH])O>>[CH]C([CH])O
2: [C]=O>>[C]O
3: O=C([CH])[CH2]>>[O]C([CH])(O)[CH2]
4: [CH]O>>[C]O[CH]
5: [C]C([CH])O>>[C]C([CH])O

MMP Level 2
1: [C]C(O)C(O)C(O)[CH2]>>OC1[C]OC([CH2])C1O
2: O=C([CH])[CH2]>>[O]C([CH])(O)[CH2]
3: O=C(CO)C([CH])O>>OCC1(O)O[CH][CH]C1O
4: [CH]C(O)[CH2]>>OC1(OC([CH2])[CH][CH]1)[CH2]
5: O=C([CH2])C(O)C([CH])O>>OC1[CH]OC(O)([CH2])C1O

MMP Level 3
1: O=C([CH2])C(O)C(O)C(O)CO>>OCC1OC(O)([CH2])C(O)C1O
2: O=C(CO)C([CH])O>>OCC1(O)O[CH][CH]C1O
3: O=C(CO)C(O)C([CH])O>>OCC1(O)OC([CH2])C(O)C1O
4: [CH]C(O)C(O)CO>>OCC1OC(O)(CO)C(O)C1O
5: O=C(CO)C(O)C(O)C(O)[CH2]>>OCC1(O)OC([CH2])C(O)C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(CO)C(O)C(O)C(O)CO>>OCC1OC(O)(CO)C(O)C1O, O=C(CO)C(O)C(O)C(O)CO>>OCC1OC(O)(CO)C(O)C1O, O=C(CO)C(O)C(O)C(O)CO>>OCC1OC(O)(CO)C(O)C1O, O=C(CO)C(O)C(O)C(O)CO>>OCC1OC(O)(CO)C(O)C1O, O=C(CO)C(O)C(O)C(O)CO>>OCC1OC(O)(CO)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([CH2:3][OH:4])[CH:5]([OH:6])[CH:7]([OH:8])[CH:9]([OH:10])[CH2:11][OH:12]>>[OH:12][CH2:11][CH:9]1[O:10][C:2]([OH:1])([CH2:3][OH:4])[CH:5]([OH:6])[CH:7]1[OH:8]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=11, 2=9, 3=7, 4=5, 5=2, 6=1, 7=3, 8=4, 9=6, 10=8, 11=10, 12=12}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=2, 2=3, 3=11, 4=9, 5=5, 6=4, 7=7, 8=8, 9=6, 10=10, 11=12, 12=1}

