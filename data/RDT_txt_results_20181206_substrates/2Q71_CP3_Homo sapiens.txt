
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:4.0]

ORDER_CHANGED
[C-O*C=O:1.0]

//
FINGERPRINTS RC
[O=C(O)CC=1C(=[C]NC1[CH2])[CH2]>>[C]=1NC([CH2])=C(C1[CH2])C:4.0, O=C(O)[CH2]:5.0, O=C(O)[CH2]>>O=C=O:2.0, O=C=O:3.0, [CH2]:4.0, [CH3]:4.0, [C]:5.0, [C]=O:1.0, [C]C:4.0, [C]C(=[C])C:4.0, [C]C(=[C])CC(=O)O:4.0, [C]C(=[C])CC(=O)O>>O=C=O:1.0, [C]C(=[C])CC(=O)O>>[C]C(=[C])C:4.0, [C]CC(=O)O:4.0, [C]CC(=O)O>>O=C=O:2.0, [C]C[C]:4.0, [C]C[C]>>[C]C:4.0, [C]O:1.0, [C]O>>[C]=O:1.0, [OH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH2]:4.0, [C]:4.0]


ID=Reaction Center at Level: 1 (2)
[O=C(O)[CH2]:4.0, [C]C[C]:4.0]


ID=Reaction Center at Level: 2 (2)
[[C]C(=[C])CC(=O)O:4.0, [C]CC(=O)O:4.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C(O)[CH2]:1.0, O=C=O:1.0, [C]=O:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (3)
[O=C(O)[CH2]:1.0, O=C=O:2.0, [C]CC(=O)O:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C[C]>>[C]C
2: [C]C[C]>>[C]C
3: [C]C[C]>>[C]C
4: [C]O>>[C]=O
5: [C]C[C]>>[C]C
6: O=C(O)[CH2]>>O=C=O

MMP Level 2
1: [C]C(=[C])CC(=O)O>>[C]C(=[C])C
2: [C]C(=[C])CC(=O)O>>[C]C(=[C])C
3: [C]C(=[C])CC(=O)O>>[C]C(=[C])C
4: O=C(O)[CH2]>>O=C=O
5: [C]C(=[C])CC(=O)O>>[C]C(=[C])C
6: [C]CC(=O)O>>O=C=O

MMP Level 3
1: O=C(O)CC=1C(=[C]NC1[CH2])[CH2]>>[C]=1NC([CH2])=C(C1[CH2])C
2: O=C(O)CC=1C(=[C]NC1[CH2])[CH2]>>[C]=1NC([CH2])=C(C1[CH2])C
3: O=C(O)CC=1C(=[C]NC1[CH2])[CH2]>>[C]=1NC([CH2])=C(C1[CH2])C
4: [C]CC(=O)O>>O=C=O
5: O=C(O)CC=1C(=[C]NC1[CH2])[CH2]>>[C]=1NC([CH2])=C(C1[CH2])C
6: [C]C(=[C])CC(=O)O>>O=C=O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)Cc1c2[nH]c(c1CCC(=O)O)Cc3[nH]c(c(c3CC(=O)O)CCC(=O)O)Cc4[nH]c(c(c4CC(=O)O)CCC(=O)O)Cc5[nH]c(c(c5CCC(=O)O)CC(=O)O)C2>>O=C(O)CCc1c2[nH]c(c1C)Cc3[nH]c(c(c3C)CCC(=O)O)Cc4[nH]c(c(c4CCC(=O)O)C)Cc5[nH]c(c(c5CCC(=O)O)C)C2, O=C(O)Cc1c2[nH]c(c1CCC(=O)O)Cc3[nH]c(c(c3CC(=O)O)CCC(=O)O)Cc4[nH]c(c(c4CC(=O)O)CCC(=O)O)Cc5[nH]c(c(c5CCC(=O)O)CC(=O)O)C2>>O=C(O)CCc1c2[nH]c(c1C)Cc3[nH]c(c(c3C)CCC(=O)O)Cc4[nH]c(c(c4CCC(=O)O)C)Cc5[nH]c(c(c5CCC(=O)O)C)C2, O=C(O)Cc1c2[nH]c(c1CCC(=O)O)Cc3[nH]c(c(c3CC(=O)O)CCC(=O)O)Cc4[nH]c(c(c4CC(=O)O)CCC(=O)O)Cc5[nH]c(c(c5CCC(=O)O)CC(=O)O)C2>>O=C(O)CCc1c2[nH]c(c1C)Cc3[nH]c(c(c3C)CCC(=O)O)Cc4[nH]c(c(c4CCC(=O)O)C)Cc5[nH]c(c(c5CCC(=O)O)C)C2, O=C(O)Cc1c2[nH]c(c1CCC(=O)O)Cc3[nH]c(c(c3CC(=O)O)CCC(=O)O)Cc4[nH]c(c(c4CC(=O)O)CCC(=O)O)Cc5[nH]c(c(c5CCC(=O)O)CC(=O)O)C2>>O=C(O)CCc1c2[nH]c(c1C)Cc3[nH]c(c(c3C)CCC(=O)O)Cc4[nH]c(c(c4CCC(=O)O)C)Cc5[nH]c(c(c5CCC(=O)O)C)C2]
2: R:M00001, P:M00003	[O=C(O)Cc1c2[nH]c(c1CCC(=O)O)Cc3[nH]c(c(c3CC(=O)O)CCC(=O)O)Cc4[nH]c(c(c4CC(=O)O)CCC(=O)O)Cc5[nH]c(c(c5CCC(=O)O)CC(=O)O)C2>>O=C=O, O=C(O)Cc1c2[nH]c(c1CCC(=O)O)Cc3[nH]c(c(c3CC(=O)O)CCC(=O)O)Cc4[nH]c(c(c4CC(=O)O)CCC(=O)O)Cc5[nH]c(c(c5CCC(=O)O)CC(=O)O)C2>>O=C=O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH2:4][C:5]:1:[C:6]2:[NH:7]:[C:8](:[C:9]1[CH2:10][CH2:11][C:12](=[O:13])[OH:14])[CH2:15][C:16]:3:[NH:17]:[C:18](:[C:19](:[C:20]3[CH2:21][C:52](=[O:53])[OH:54])[CH2:22][CH2:23][C:24](=[O:25])[OH:26])[CH2:27][C:28]:4:[NH:29]:[C:30](:[C:31](:[C:32]4[CH2:33][C:55](=[O:56])[OH:57])[CH2:34][CH2:35][C:36](=[O:37])[OH:38])[CH2:39][C:40]:5:[NH:41]:[C:42](:[C:43](:[C:44]5[CH2:45][CH2:46][C:47](=[O:48])[OH:49])[CH2:50][C:58](=[O:59])[OH:60])[CH2:51]2>>[O:1]=[C:2]=[O:3].[O:13]=[C:12]([OH:14])[CH2:11][CH2:10][C:9]:1:[C:8]2:[NH:7]:[C:6](:[C:5]1[CH3:4])[CH2:51][C:42]:3:[NH:41]:[C:40](:[C:44](:[C:43]3[CH3:50])[CH2:45][CH2:46][C:47](=[O:48])[OH:49])[CH2:39][C:30]:4:[NH:29]:[C:28](:[C:32](:[C:31]4[CH2:34][CH2:35][C:36](=[O:37])[OH:38])[CH3:33])[CH2:27][C:18]:5:[NH:17]:[C:16](:[C:20](:[C:19]5[CH2:22][CH2:23][C:24](=[O:25])[OH:26])[CH3:21])[CH2:15]2


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=45, 2=33, 3=34, 4=35, 5=31, 6=32, 7=30, 8=18, 9=19, 10=20, 11=16, 12=17, 13=15, 14=8, 15=9, 16=5, 17=6, 18=7, 19=60, 20=48, 21=49, 22=50, 23=46, 24=47, 25=51, 26=52, 27=53, 28=54, 29=55, 30=56, 31=57, 32=58, 33=59, 34=4, 35=2, 36=1, 37=3, 38=10, 39=11, 40=12, 41=13, 42=14, 43=21, 44=22, 45=23, 46=24, 47=25, 48=26, 49=27, 50=28, 51=29, 52=36, 53=37, 54=38, 55=39, 56=40, 57=41, 58=42, 59=43, 60=44}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=11, 2=10, 3=9, 4=12, 5=13, 6=17, 7=16, 8=15, 9=14, 10=24, 11=25, 12=29, 13=28, 14=27, 15=26, 16=36, 17=37, 18=41, 19=40, 20=39, 21=38, 22=48, 23=7, 24=6, 25=5, 26=4, 27=2, 28=1, 29=3, 30=8, 31=47, 32=42, 33=43, 34=44, 35=45, 36=46, 37=35, 38=30, 39=31, 40=32, 41=33, 42=34, 43=19, 44=20, 45=21, 46=22, 47=23, 48=18, 49=50, 50=49, 51=51}

