
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)C([CH])[NH]:1.0, O=C([CH])O:2.0, O=C1N2C(=C([CH2])CSC2C1[NH])C(=O)O>>[C]C([NH])C1SCC([CH2])=C(N1)C(=O)O:1.0, O>>O=C(O)C([CH])[NH]:1.0, O>>O=C([CH])O:1.0, O>>[C]O:1.0, [C]:2.0, [C]C(=[C])N1C(=O)[CH]C1[S]:1.0, [C]C(=[C])N1C(=O)[CH]C1[S]>>[C]C(=[C])NC([S])[CH]:1.0, [C]C(=[C])NC([S])[CH]:1.0, [C]N([C])[CH]:1.0, [C]N([C])[CH]>>[C]N[CH]:1.0, [C]N1[CH]C([NH])C1=O:1.0, [C]N1[CH]C([NH])C1=O>>O=C(O)C([CH])[NH]:1.0, [C]NC1C(=O)N(C([C])=[C])C1[S]>>[C]NC(C(=O)O)C([S])[NH]:1.0, [C]N[CH]:1.0, [C]O:1.0, [NH]:1.0, [N]:1.0, [N]C(=O)[CH]:1.0, [N]C(=O)[CH]>>O=C([CH])O:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [C]:2.0, [N]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=C([CH])O:1.0, [C]N([C])[CH]:1.0, [C]O:1.0, [N]C(=O)[CH]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=C(O)C([CH])[NH]:1.0, O=C([CH])O:1.0, [C]C(=[C])N1C(=O)[CH]C1[S]:1.0, [C]N1[CH]C([NH])C1=O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [N]C(=O)[CH]>>O=C([CH])O
2: O>>[C]O
3: [C]N([C])[CH]>>[C]N[CH]

MMP Level 2
1: [C]N1[CH]C([NH])C1=O>>O=C(O)C([CH])[NH]
2: O>>O=C([CH])O
3: [C]C(=[C])N1C(=O)[CH]C1[S]>>[C]C(=[C])NC([S])[CH]

MMP Level 3
1: [C]NC1C(=O)N(C([C])=[C])C1[S]>>[C]NC(C(=O)O)C([S])[NH]
2: O>>O=C(O)C([CH])[NH]
3: O=C1N2C(=C([CH2])CSC2C1[NH])C(=O)O>>[C]C([NH])C1SCC([CH2])=C(N1)C(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C1N2C(=C(CSC2C1NC(=O)Cc3sccc3)COC(=O)C)C(=O)O>>O=C(O)C=1NC(SCC1COC(=O)C)C(NC(=O)Cc2sccc2)C(=O)O, O=C1N2C(=C(CSC2C1NC(=O)Cc3sccc3)COC(=O)C)C(=O)O>>O=C(O)C=1NC(SCC1COC(=O)C)C(NC(=O)Cc2sccc2)C(=O)O]
2: R:M00002, P:M00003	[O>>O=C(O)C=1NC(SCC1COC(=O)C)C(NC(=O)Cc2sccc2)C(=O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[C:4]1=[C:5]([CH2:6][O:7][C:8](=[O:9])[CH3:10])[CH2:11][S:12][CH:13]2[N:14]1[C:15](=[O:16])[CH:17]2[NH:18][C:19](=[O:20])[CH2:21][C:22]:3:[S:23]:[CH:24]:[CH:25]:[CH:26]3.[OH2:27]>>[O:1]=[C:2]([OH:3])[C:4]=1[NH:14][CH:13]([S:12][CH2:11][C:5]1[CH2:6][O:7][C:8](=[O:9])[CH3:10])[CH:17]([NH:18][C:19](=[O:20])[CH2:21][C:22]:2:[S:23]:[CH:24]:[CH:25]:[CH:26]2)[C:15](=[O:16])[OH:27]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=10, 2=8, 3=9, 4=7, 5=6, 6=5, 7=4, 8=14, 9=13, 10=17, 11=15, 12=16, 13=18, 14=19, 15=20, 16=21, 17=22, 18=26, 19=25, 20=24, 21=23, 22=12, 23=11, 24=2, 25=1, 26=3, 27=27}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=14, 2=12, 3=13, 4=11, 5=10, 6=9, 7=4, 8=5, 9=6, 10=7, 11=8, 12=15, 13=25, 14=26, 15=27, 16=16, 17=17, 18=18, 19=19, 20=20, 21=24, 22=23, 23=22, 24=21, 25=2, 26=1, 27=3}

