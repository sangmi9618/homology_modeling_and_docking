
//
FINGERPRINTS BC

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[OC1OC([CH2])[CH]C(O)C1O>>OC1OC([CH2])[CH]C(O)C1O:1.0, [CH]:2.0, [CH]OC(O)C([CH])O:2.0, [CH]OC(O)C([CH])O>>[CH]OC(O)C([CH])O:1.0, [O]C([CH])O:2.0, [O]C([CH])O>>[O]C([CH])O:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (1)
[[O]C([CH])O:2.0]


ID=Reaction Center at Level: 2 (1)
[[CH]OC(O)C([CH])O:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: [O]C([CH])O>>[O]C([CH])O

MMP Level 2
1: [CH]OC(O)C([CH])O>>[CH]OC(O)C([CH])O

MMP Level 3
1: OC1OC([CH2])[CH]C(O)C1O>>OC1OC([CH2])[CH]C(O)C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=P(O)(O)OCC1OC(O)C(O)C(O)C1O>>O=P(O)(O)OCC1OC(O)C(O)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH2:6][CH:7]1[O:8][CH:9]([OH:10])[CH:11]([OH:12])[CH:13]([OH:14])[CH:15]1[OH:16]>>[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH2:6][CH:7]1[O:8][CH:9]([OH:10])[CH:11]([OH:12])[CH:13]([OH:14])[CH:15]1[OH:16]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=15, 4=13, 5=11, 6=9, 7=8, 8=10, 9=12, 10=14, 11=16, 12=5, 13=2, 14=1, 15=3, 16=4}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=15, 4=13, 5=11, 6=9, 7=8, 8=10, 9=12, 10=14, 11=16, 12=5, 13=2, 14=1, 15=3, 16=4}

