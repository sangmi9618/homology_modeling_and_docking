
//
FINGERPRINTS BC

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(O)C(O)C(=C[CH])C=[CH]>>[O-]C(=O)C(O)C(=C[CH])C=[CH]:1.0, O=C(O)C(O)C([CH])=[CH]:1.0, O=C(O)C(O)C([CH])=[CH]>>[O-]C(=O)C(O)C([CH])=[CH]:1.0, O=C([CH])O:1.0, O=C([CH])O>>[O-]C(=O)[CH]:1.0, [CH]:2.0, [C]C(O)C(=O)O>>[C]C(O)C([O-])=O:1.0, [C]C([C])O:2.0, [C]C([C])O>>[C]C([C])O:1.0, [C]O:1.0, [C]O>>[C][O-]:1.0, [C][O-]:1.0, [O-]:1.0, [O-]C(=O)C(O)C([CH])=[CH]:1.0, [O-]C(=O)[CH]:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (1)
[[C]C([C])O:2.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C(O)C([CH])=[CH]:1.0, [O-]C(=O)C(O)C([CH])=[CH]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C([C])O>>[C]C([C])O
2: [C]O>>[C][O-]

MMP Level 2
1: O=C(O)C(O)C([CH])=[CH]>>[O-]C(=O)C(O)C([CH])=[CH]
2: O=C([CH])O>>[O-]C(=O)[CH]

MMP Level 3
1: O=C(O)C(O)C(=C[CH])C=[CH]>>[O-]C(=O)C(O)C(=C[CH])C=[CH]
2: [C]C(O)C(=O)O>>[C]C(O)C([O-])=O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)C(O)c1ccccc1>>[O-]C(=O)C(O)c1ccccc1, O=C(O)C(O)c1ccccc1>>[O-]C(=O)C(O)c1ccccc1]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([OH:5])[C:6]:1:[CH:7]:[CH:8]:[CH:9]:[CH:10]:[CH:11]1>>[O:1]=[C:2]([O-:3])[CH:4]([OH:5])[C:6]:1:[CH:7]:[CH:8]:[CH:9]:[CH:10]:[CH:11]1


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=11, 6=10, 7=4, 8=2, 9=1, 10=3, 11=5}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=11, 6=10, 7=4, 8=2, 9=1, 10=3, 11=5}

