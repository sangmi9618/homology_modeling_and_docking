
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-Se:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(O)C(N)C:1.0, O=C(O)C(N)C[SeH]:1.0, O=C(O)C(N)C[SeH]>>O=C(O)C(N)C:3.0, [CH2]:1.0, [CH3]:1.0, [CH]:2.0, [CH]C:1.0, [CH]C[SeH]:2.0, [CH]C[SeH]>>[CH]C:1.0, [CH]C[SeH]>>[SeH2]:1.0, [C]C(N)C:2.0, [C]C(N)C[SeH]:1.0, [C]C(N)C[SeH]>>[C]C(N)C:1.0, [C]C(N)C[SeH]>>[SeH2]:1.0, [C]C([CH2])N:1.0, [C]C([CH2])N>>[C]C(N)C:1.0, [SeH2]:3.0, [SeH]:1.0, [SeH][CH2]:1.0, [SeH][CH2]>>[SeH2]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH2]:1.0, [SeH2]:1.0, [SeH]:1.0]


ID=Reaction Center at Level: 1 (3)
[[CH]C[SeH]:1.0, [SeH2]:1.0, [SeH][CH2]:1.0]


ID=Reaction Center at Level: 2 (3)
[[CH]C[SeH]:1.0, [C]C(N)C[SeH]:1.0, [SeH2]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(N)C:1.0, [C]C([CH2])N:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C(N)C:1.0, O=C(O)C(N)C[SeH]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]C[SeH]>>[CH]C
2: [SeH][CH2]>>[SeH2]
3: [C]C([CH2])N>>[C]C(N)C

MMP Level 2
1: [C]C(N)C[SeH]>>[C]C(N)C
2: [CH]C[SeH]>>[SeH2]
3: O=C(O)C(N)C[SeH]>>O=C(O)C(N)C

MMP Level 3
1: O=C(O)C(N)C[SeH]>>O=C(O)C(N)C
2: [C]C(N)C[SeH]>>[SeH2]
3: O=C(O)C(N)C[SeH]>>O=C(O)C(N)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)C(N)C[SeH]>>O=C(O)C(N)C, O=C(O)C(N)C[SeH]>>O=C(O)C(N)C]
2: R:M00001, P:M00003	[O=C(O)C(N)C[SeH]>>[SeH2]]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][SeH:7]>>[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH3:6].[SeH2:7]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=4, 3=2, 4=1, 5=3, 6=5, 7=7}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=4, 3=2, 4=1, 5=3, 6=5, 7=7}

