
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%S:2.0, C-C:1.0, C-O:1.0, O=O:1.0]

ORDER_CHANGED
[C%C*C%C:1.0, C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(=O)C[CH2]>>O=C(O)C[CH2]:1.0, O=C(O)C(=O)C[CH2]>>O=C=O:1.0, O=C(O)C(=O)[CH2]:1.0, O=C(O)C(=O)[CH2]>>O=C=O:2.0, O=C(O)C1N2[C][CH]C2SC1(C)C>>[C]N1[CH]SCC(=C1C(=O)O)C:1.0, O=C(O)C[CH2]:1.0, O=C(O)[CH2]:2.0, O=C1[CH]C2SC(C)(C)C(C(=O)O)N12>>O=C1[CH]C2SCC(=C(C(=O)O)N12)C:1.0, O=C=O:3.0, O=O:4.0, O=O>>O:3.0, O=O>>O=C(O)C[CH2]:1.0, O=O>>O=C(O)[CH2]:1.0, O=O>>[C]O:1.0, [CH2]:1.0, [CH3]:1.0, [CH]:3.0, [CH]C1[N][CH]C(S1)(C)C:1.0, [CH]C1[N][CH]C(S1)(C)C>>[C]CSC([N])[CH]:1.0, [CH]S[CH2]:1.0, [C]:7.0, [C]=C(C)CS[CH]:1.0, [C]=C([CH2])C:1.0, [C]=O:1.0, [C]C:1.0, [C]C(=O)O:2.0, [C]C(=O)O>>O=C=O:2.0, [C]C(=O)[CH2]:1.0, [C]C(=O)[CH2]>>O=C(O)[CH2]:1.0, [C]C(=[C])[N]:1.0, [C]C([C])[N]:1.0, [C]C([C])[N]>>[C]C(=[C])[N]:1.0, [C]C([N])=C(C)C[S]:1.0, [C]C1N2[C]C([NH])C2SC1(C)C>>[NH]C1[C]N2[C]=C(C)CSC21:1.0, [C]C1[N][CH]SC1(C)C:1.0, [C]C1[N][CH]SC1(C)C>>[C]C([N])=C(C)C[S]:1.0, [C]C1[N][CH]SC1(C)C>>[C]C=1[N]C([CH])SCC1C:1.0, [C]C>>[C]C[S]:1.0, [C]CCC(=O)C(=O)O>>[C]CCC(=O)O:1.0, [C]CSC([N])[CH]:1.0, [C]C[S]:1.0, [C]N([CH])C(C(=O)O)=C([CH2])C:1.0, [C]N1[CH]SC(C)(C)C1C(=O)O:1.0, [C]N1[CH]SC(C)(C)C1C(=O)O>>[C]N([CH])C(C(=O)O)=C([CH2])C:1.0, [C]N1[C]C([NH])C1S[CH2]:1.0, [C]NC1C(=O)N2C([C])C(SC21)(C)C>>[C]NC1C(=O)N2C([C])=[C]CSC21:1.0, [C]O:2.0, [C]O>>[C]=O:1.0, [C]S[CH]:1.0, [C]S[CH]>>[CH]S[CH2]:1.0, [NH]C1[C]N2[CH][C]SC21:1.0, [NH]C1[C]N2[CH][C]SC21>>[C]N1[C]C([NH])C1S[CH2]:1.0, [N]C([S])[CH]:2.0, [N]C([S])[CH]>>[N]C([S])[CH]:1.0, [OH]:2.0, [O]:3.0, [S]:2.0, [S]C([CH])(C)C:2.0, [S]C([CH])(C)C>>[C]=C(C)CS[CH]:1.0, [S]C([CH])(C)C>>[C]=C([CH2])C:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (6)
[O:1.0, [CH2]:1.0, [C]:4.0, [OH]:1.0, [O]:2.0, [S]:2.0]


ID=Reaction Center at Level: 1 (10)
[O:1.0, O=C(O)[CH2]:1.0, O=O:2.0, [CH]S[CH2]:1.0, [C]C(=O)O:1.0, [C]C(=O)[CH2]:1.0, [C]C[S]:1.0, [C]O:1.0, [C]S[CH]:1.0, [S]C([CH])(C)C:1.0]


ID=Reaction Center at Level: 2 (10)
[O:1.0, O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(=O)[CH2]:1.0, O=C(O)C[CH2]:1.0, O=C(O)[CH2]:1.0, O=O:2.0, [CH]C1[N][CH]C(S1)(C)C:1.0, [C]=C(C)CS[CH]:1.0, [C]C1[N][CH]SC1(C)C:1.0, [C]CSC([N])[CH]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH]:1.0, [C]:5.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (8)
[O=C=O:1.0, [C]=C([CH2])C:1.0, [C]=O:1.0, [C]C(=O)O:1.0, [C]C(=[C])[N]:1.0, [C]C([C])[N]:1.0, [C]O:1.0, [S]C([CH])(C)C:1.0]


ID=Reaction Center at Level: 2 (7)
[O=C(O)C(=O)[CH2]:1.0, O=C=O:2.0, [C]C(=O)O:1.0, [C]C([N])=C(C)C[S]:1.0, [C]C1[N][CH]SC1(C)C:1.0, [C]N([CH])C(C(=O)O)=C([CH2])C:1.0, [C]N1[CH]SC(C)(C)C1C(=O)O:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (1)
[[N]C([S])[CH]:2.0]


ID=Reaction Center at Level: 2 (2)
[[C]N1[C]C([NH])C1S[CH2]:1.0, [NH]C1[C]N2[CH][C]SC21:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: O=O>>O
2: [C]S[CH]>>[CH]S[CH2]
3: [C]C(=O)O>>O=C=O
4: [C]C>>[C]C[S]
5: [N]C([S])[CH]>>[N]C([S])[CH]
6: [C]O>>[C]=O
7: O=O>>[C]O
8: [S]C([CH])(C)C>>[C]=C([CH2])C
9: [C]C([C])[N]>>[C]C(=[C])[N]
10: [C]C(=O)[CH2]>>O=C(O)[CH2]

MMP Level 2
1: O=O>>O
2: [CH]C1[N][CH]C(S1)(C)C>>[C]CSC([N])[CH]
3: O=C(O)C(=O)[CH2]>>O=C=O
4: [S]C([CH])(C)C>>[C]=C(C)CS[CH]
5: [NH]C1[C]N2[CH][C]SC21>>[C]N1[C]C([NH])C1S[CH2]
6: [C]C(=O)O>>O=C=O
7: O=O>>O=C(O)[CH2]
8: [C]C1[N][CH]SC1(C)C>>[C]C([N])=C(C)C[S]
9: [C]N1[CH]SC(C)(C)C1C(=O)O>>[C]N([CH])C(C(=O)O)=C([CH2])C
10: O=C(O)C(=O)C[CH2]>>O=C(O)C[CH2]

MMP Level 3
1: O=O>>O
2: [C]C1N2[C]C([NH])C2SC1(C)C>>[NH]C1[C]N2[C]=C(C)CSC21
3: O=C(O)C(=O)C[CH2]>>O=C=O
4: [C]C1[N][CH]SC1(C)C>>[C]C=1[N]C([CH])SCC1C
5: [C]NC1C(=O)N2C([C])C(SC21)(C)C>>[C]NC1C(=O)N2C([C])=[C]CSC21
6: O=C(O)C(=O)[CH2]>>O=C=O
7: O=O>>O=C(O)C[CH2]
8: O=C(O)C1N2[C][CH]C2SC1(C)C>>[C]N1[CH]SCC(=C1C(=O)O)C
9: O=C1[CH]C2SC(C)(C)C(C(=O)O)N12>>O=C1[CH]C2SCC(=C(C(=O)O)N12)C
10: [C]CCC(=O)C(=O)O>>[C]CCC(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C1N2C(SC(C)(C)C2C(=O)O)C1NC(=O)Cc3ccc(O)cc3>>O=C1N2C(=C(C)CSC2C1NC(=O)Cc3ccc(O)cc3)C(=O)O, O=C1N2C(SC(C)(C)C2C(=O)O)C1NC(=O)Cc3ccc(O)cc3>>O=C1N2C(=C(C)CSC2C1NC(=O)Cc3ccc(O)cc3)C(=O)O, O=C1N2C(SC(C)(C)C2C(=O)O)C1NC(=O)Cc3ccc(O)cc3>>O=C1N2C(=C(C)CSC2C1NC(=O)Cc3ccc(O)cc3)C(=O)O, O=C1N2C(SC(C)(C)C2C(=O)O)C1NC(=O)Cc3ccc(O)cc3>>O=C1N2C(=C(C)CSC2C1NC(=O)Cc3ccc(O)cc3)C(=O)O, O=C1N2C(SC(C)(C)C2C(=O)O)C1NC(=O)Cc3ccc(O)cc3>>O=C1N2C(=C(C)CSC2C1NC(=O)Cc3ccc(O)cc3)C(=O)O]
2: R:M00002, P:M00005	[O=C(O)C(=O)CCC(=O)O>>O=C(O)CCC(=O)O]
3: R:M00002, P:M00006	[O=C(O)C(=O)CCC(=O)O>>O=C=O, O=C(O)C(=O)CCC(=O)O>>O=C=O]
4: R:M00003, P:M00005	[O=O>>O=C(O)CCC(=O)O]
5: R:M00003, P:M00007	[O=O>>O]


//
SELECTED AAM MAPPING
[O:35]=[O:36].[O:25]=[C:26]([OH:27])[C:28](=[O:29])[CH2:30][CH2:31][C:32](=[O:33])[OH:34].[O:1]=[C:2]([OH:3])[CH:4]1[N:5]2[C:6](=[O:7])[CH:8]([NH:9][C:10](=[O:11])[CH2:12][C:13]:3:[CH:14]:[CH:15]:[C:16]([OH:17]):[CH:18]:[CH:19]3)[CH:20]2[S:21][C:22]1([CH3:23])[CH3:24]>>[O:25]=[C:26]=[O:27].[O:1]=[C:2]([OH:3])[C:4]1=[C:22]([CH3:24])[CH2:23][S:21][CH:20]2[N:5]1[C:6](=[O:7])[CH:8]2[NH:9][C:10](=[O:11])[CH2:12][C:13]:3:[CH:19]:[CH:18]:[C:16]([OH:17]):[CH:15]:[CH:14]3.[O:29]=[C:28]([OH:35])[CH2:30][CH2:31][C:32](=[O:33])[OH:34].[OH2:36]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=23, 2=22, 3=4, 4=5, 5=20, 6=21, 7=8, 8=6, 9=7, 10=9, 11=10, 12=11, 13=12, 14=13, 15=14, 16=15, 17=16, 18=18, 19=19, 20=17, 21=2, 22=1, 23=3, 24=24, 25=30, 26=31, 27=32, 28=33, 29=34, 30=28, 31=29, 32=26, 33=25, 34=27, 35=35, 36=36}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=5, 3=4, 4=10, 5=9, 6=13, 7=11, 8=12, 9=14, 10=15, 11=16, 12=17, 13=18, 14=19, 15=20, 16=21, 17=23, 18=24, 19=22, 20=8, 21=7, 22=2, 23=1, 24=3, 25=29, 26=28, 27=26, 28=25, 29=27, 30=30, 31=31, 32=32, 33=34, 34=33, 35=35, 36=36}

