
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O>>[CH]O:1.0, O>>[CH]OC(O)C([CH])O:1.0, O>>[O]C([CH])O:1.0, [CH]:2.0, [CH]C(=[CH])O:1.0, [CH]C(=[CH])OC1OC([CH2])[CH]C(O)C1O>>OC1OC([CH2])[CH]C(O)C1O:1.0, [CH]O:1.0, [CH]OC(O)C([CH])O:1.0, [C]C=C(OC(O[CH])C([CH])O)C=[CH]>>[C]C=C(O)C=[CH]:1.0, [C]O:1.0, [C]OC(O[CH])C([CH])O:1.0, [C]OC(O[CH])C([CH])O>>[CH]OC(O)C([CH])O:1.0, [C]O[CH]:1.0, [C]O[CH]>>[C]O:1.0, [OH]:2.0, [O]:1.0, [O]C([CH])O:2.0, [O]C([CH])OC([CH])=[CH]:1.0, [O]C([CH])OC([CH])=[CH]>>[CH]C(=[CH])O:1.0, [O]C([O])[CH]:1.0, [O]C([O])[CH]>>[O]C([CH])O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [CH]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, [CH]O:1.0, [C]O[CH]:1.0, [O]C([CH])O:1.0, [O]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, [CH]OC(O)C([CH])O:1.0, [C]OC(O[CH])C([CH])O:1.0, [O]C([CH])O:1.0, [O]C([CH])OC([CH])=[CH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]C([CH])O:1.0, [O]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[CH]OC(O)C([CH])O:1.0, [C]OC(O[CH])C([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [O]C([O])[CH]>>[O]C([CH])O
2: O>>[CH]O
3: [C]O[CH]>>[C]O

MMP Level 2
1: [C]OC(O[CH])C([CH])O>>[CH]OC(O)C([CH])O
2: O>>[O]C([CH])O
3: [O]C([CH])OC([CH])=[CH]>>[CH]C(=[CH])O

MMP Level 3
1: [CH]C(=[CH])OC1OC([CH2])[CH]C(O)C1O>>OC1OC([CH2])[CH]C(O)C1O
2: O>>[CH]OC(O)C([CH])O
3: [C]C=C(OC(O[CH])C([CH])O)C=[CH]>>[C]C=C(O)C=[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1oc2cc(OC3OC(CO)C(O)C(O)C3O)ccc2c(c1)C>>O=c1oc2cc(O)ccc2c(c1)C]
2: R:M00001, P:M00004	[O=c1oc2cc(OC3OC(CO)C(O)C(O)C3O)ccc2c(c1)C>>OCC1OC(O)C(O)C(O)C1O]
3: R:M00002, P:M00004	[O>>OCC1OC(O)C(O)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[O:3][C:4]:2:[CH:5]:[C:6]([O:7][CH:8]3[O:9][CH:10]([CH2:11][OH:12])[CH:13]([OH:14])[CH:15]([OH:16])[CH:17]3[OH:18]):[CH:19]:[CH:20]:[C:21]2[C:22](=[CH:23]1)[CH3:24].[OH2:25]>>[O:1]=[C:2]1[O:3][C:4]:2:[CH:5]:[C:6]([OH:7]):[CH:19]:[CH:20]:[C:21]2[C:22](=[CH:23]1)[CH3:24].[OH:12][CH2:11][CH:10]1[O:9][CH:8]([OH:25])[CH:17]([OH:18])[CH:15]([OH:16])[CH:13]1[OH:14]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=22, 3=23, 4=2, 5=1, 6=3, 7=4, 8=21, 9=20, 10=19, 11=6, 12=5, 13=7, 14=8, 15=17, 16=15, 17=13, 18=10, 19=9, 20=11, 21=12, 22=14, 23=16, 24=18, 25=25}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=13, 2=11, 3=12, 4=2, 5=1, 6=3, 7=4, 8=10, 9=9, 10=8, 11=6, 12=5, 13=7, 14=15, 15=16, 16=24, 17=22, 18=20, 19=18, 20=17, 21=19, 22=21, 23=23, 24=25, 25=14}

