
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O:3.0, O=P(O)(O)O:3.0, O=P(O)(O)OC[CH]:1.0, O=P(O)(O)OC[CH]>>O=P(O)(O)O:1.0, O=P(O)(O)OC[CH]>>[CH]CO:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[CH2]>>O=P(O)(O)O:1.0, O>>O=P(O)(O)O:2.0, O>>[P]O:1.0, O[CH2]:1.0, [CH]CO:1.0, [OH]:2.0, [O]:1.0, [O]C([CH])COP(=O)(O)O>>[O]C([CH])CO:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[CH2]>>O[CH2]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=P(O)(O)O:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=P(O)(O)O:2.0, O=P(O)(O)OC[CH]:1.0, O=P(O)(O)O[CH2]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O>>[P]O
2: [P]O[CH2]>>O[CH2]
3: [O]P(=O)(O)O>>O=P(O)(O)O

MMP Level 2
1: O>>O=P(O)(O)O
2: O=P(O)(O)OC[CH]>>[CH]CO
3: O=P(O)(O)O[CH2]>>O=P(O)(O)O

MMP Level 3
1: O>>O=P(O)(O)O
2: [O]C([CH])COP(=O)(O)O>>[O]C([CH])CO
3: O=P(O)(O)OC[CH]>>O=P(O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OCC1OC(n2cnc3c(ncnc32)N)CC1O>>OCC1OC(n2cnc3c(ncnc32)N)CC1O]
2: R:M00001, P:M00004	[O=P(O)(O)OCC1OC(n2cnc3c(ncnc32)N)CC1O>>O=P(O)(O)O]
3: R:M00002, P:M00004	[O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH2:6][CH:7]1[O:8][CH:9]([N:10]:2:[CH:11]:[N:12]:[C:13]:3:[C:14](:[N:15]:[CH:16]:[N:17]:[C:18]32)[NH2:19])[CH2:20][CH:21]1[OH:22].[OH2:23]>>[O:1]=[P:2]([OH:4])([OH:23])[OH:3].[OH:5][CH2:6][CH:7]1[O:8][CH:9]([N:10]:2:[CH:11]:[N:12]:[C:13]:3:[C:14](:[N:15]:[CH:16]:[N:17]:[C:18]32)[NH2:19])[CH2:20][CH:21]1[OH:22]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=7, 4=8, 5=9, 6=10, 7=11, 8=12, 9=13, 10=18, 11=17, 12=16, 13=15, 14=14, 15=19, 16=6, 17=5, 18=2, 19=1, 20=3, 21=4, 22=22, 23=23}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=16, 2=17, 3=3, 4=4, 5=5, 6=6, 7=7, 8=8, 9=9, 10=14, 11=13, 12=12, 13=11, 14=10, 15=15, 16=2, 17=1, 18=18, 19=21, 20=20, 21=19, 22=22, 23=23}

