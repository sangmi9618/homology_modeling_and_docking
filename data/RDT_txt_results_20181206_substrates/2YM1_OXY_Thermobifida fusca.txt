
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0, C-O:2.0, O=O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(C)CC(=C[CH])C=[CH]>>O=C(OCC(=C[CH])C=[CH])C:1.0, O=C(C)CC([CH])=[CH]:1.0, O=C(C)CC([CH])=[CH]>>[C]COC(=O)C:1.0, O=C(C)CC([CH])=[CH]>>[C]OCC([CH])=[CH]:1.0, O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1:1.0, O=C(O[CH2])C:1.0, O=C([CH2])C:1.0, O=C([CH2])C>>[O]C(=O)C:1.0, O=O:4.0, O=O>>O:3.0, O=O>>O=C(OCC([CH])=[CH])C:1.0, O=O>>[C]COC(=O)C:1.0, O=O>>[C]O[CH2]:1.0, [CH2]:3.0, [CH]:1.0, [CH]N([CH])[CH]:1.0, [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]:1.0, [C]:2.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]:1.0, [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O:1.0, [C]C=[CH]:1.0, [C]CC(=O)C:1.0, [C]CC(=O)C>>O=C(O[CH2])C:1.0, [C]COC(=O)C:1.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C=[CH]:1.0, [C]C[C]:1.0, [C]C[C]>>[C]C[O]:1.0, [C]C[O]:1.0, [C]OCC([CH])=[CH]:1.0, [C]O[CH2]:1.0, [N+]:1.0, [N]:1.0, [O]:3.0, [O]C(=O)C:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [CH2]:2.0, [C]:2.0, [O]:4.0]


ID=Reaction Center at Level: 1 (7)
[O:1.0, O=C([CH2])C:1.0, O=O:2.0, [C]C[C]:1.0, [C]C[O]:1.0, [C]O[CH2]:2.0, [O]C(=O)C:1.0]


ID=Reaction Center at Level: 2 (7)
[O:1.0, O=C(C)CC([CH])=[CH]:1.0, O=C(O[CH2])C:1.0, O=O:2.0, [C]CC(=O)C:1.0, [C]COC(=O)C:2.0, [C]OCC([CH])=[CH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]
2: [C]C[CH]>>[C]C=[CH]
3: O=O>>O
4: O=C([CH2])C>>[O]C(=O)C
5: O=O>>[C]O[CH2]
6: [C]C[C]>>[C]C[O]

MMP Level 2
1: [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]
2: [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]
3: O=O>>O
4: [C]CC(=O)C>>O=C(O[CH2])C
5: O=O>>[C]COC(=O)C
6: O=C(C)CC([CH])=[CH]>>[C]OCC([CH])=[CH]

MMP Level 3
1: [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O
2: O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1
3: O=O>>O
4: O=C(C)CC([CH])=[CH]>>[C]COC(=O)C
5: O=O>>O=C(OCC([CH])=[CH])C
6: O=C(C)CC(=C[CH])C=[CH]>>O=C(OCC(=C[CH])C=[CH])C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00005	[O=C(C)Cc1ccccc1>>O=C(OCc1ccccc1)C, O=C(C)Cc1ccccc1>>O=C(OCc1ccccc1)C]
2: R:M00002, P:M00006	[O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O]
3: R:M00004, P:M00005	[O=O>>O=C(OCc1ccccc1)C]
4: R:M00004, P:M00007	[O=O>>O]


//
SELECTED AAM MAPPING
[H+:61].[O:59]=[O:60].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:5][N:6]([CH:7]=[CH:8][CH2:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48].[O:49]=[C:50]([CH3:51])[CH2:52][C:53]:1:[CH:58]:[CH:57]:[CH:56]:[CH:55]:[CH:54]1>>[O:49]=[C:50]([O:59][CH2:52][C:53]:1:[CH:58]:[CH:57]:[CH:56]:[CH:55]:[CH:54]1)[CH3:51].[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:9]:[CH:8]:[CH:7]:[N+:6](:[CH:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48].[OH2:60]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=51, 2=50, 3=49, 4=52, 5=53, 6=54, 7=55, 8=56, 9=57, 10=58, 11=9, 12=8, 13=7, 14=6, 15=5, 16=4, 17=2, 18=1, 19=3, 20=10, 21=47, 22=45, 23=12, 24=11, 25=13, 26=14, 27=15, 28=16, 29=17, 30=18, 31=19, 32=20, 33=21, 34=22, 35=23, 36=24, 37=43, 38=37, 39=26, 40=25, 41=27, 42=28, 43=29, 44=30, 45=35, 46=34, 47=33, 48=32, 49=31, 50=36, 51=38, 52=39, 53=40, 54=41, 55=42, 56=44, 57=46, 58=48, 59=61, 60=59, 61=60}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=59, 2=50, 3=49, 4=51, 5=52, 6=53, 7=54, 8=55, 9=56, 10=57, 11=58, 12=6, 13=5, 14=4, 15=9, 16=8, 17=7, 18=10, 19=47, 20=45, 21=12, 22=11, 23=13, 24=14, 25=15, 26=16, 27=17, 28=18, 29=19, 30=20, 31=21, 32=22, 33=23, 34=24, 35=43, 36=37, 37=26, 38=25, 39=27, 40=28, 41=29, 42=30, 43=35, 44=34, 45=33, 46=32, 47=31, 48=36, 49=38, 50=39, 51=40, 52=41, 53=42, 54=44, 55=46, 56=48, 57=2, 58=1, 59=3, 60=60}

