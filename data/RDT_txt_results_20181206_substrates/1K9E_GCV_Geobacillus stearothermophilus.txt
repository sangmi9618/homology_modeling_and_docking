
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O>>[CH]O:1.0, O>>[CH]OC(O)C([CH])O:1.0, O>>[O]C([CH])O:1.0, [CH]:2.0, [CH]C([CH])O:1.0, [CH]O:2.0, [CH]OC(O)C([CH])O:1.0, [CH]OC(O[CH])C([CH])O:1.0, [CH]OC(O[CH])C([CH])O>>[CH]OC(O)C([CH])O:1.0, [CH]O[CH]:1.0, [CH]O[CH]>>[CH]O:1.0, [C]C1OC(OC([CH])[CH])C(O)C(O)[CH]1>>[C]C1OC(O)C(O)C(O)[CH]1:1.0, [OH]:2.0, [O]:1.0, [O]C([CH])O:2.0, [O]C([CH])OC([CH])[CH]:1.0, [O]C([CH])OC([CH])[CH]>>[CH]C([CH])O:1.0, [O]C([O])C(OC(O[CH])C([CH])O)C([CH])O>>[O]C([O])C(O)C([CH])O:1.0, [O]C([O])[CH]:1.0, [O]C([O])[CH]>>[O]C([CH])O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [CH]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, [CH]O:1.0, [CH]O[CH]:1.0, [O]C([CH])O:1.0, [O]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, [CH]OC(O)C([CH])O:1.0, [CH]OC(O[CH])C([CH])O:1.0, [O]C([CH])O:1.0, [O]C([CH])OC([CH])[CH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]C([CH])O:1.0, [O]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[CH]OC(O)C([CH])O:1.0, [CH]OC(O[CH])C([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]O[CH]>>[CH]O
2: O>>[CH]O
3: [O]C([O])[CH]>>[O]C([CH])O

MMP Level 2
1: [O]C([CH])OC([CH])[CH]>>[CH]C([CH])O
2: O>>[O]C([CH])O
3: [CH]OC(O[CH])C([CH])O>>[CH]OC(O)C([CH])O

MMP Level 3
1: [O]C([O])C(OC(O[CH])C([CH])O)C([CH])O>>[O]C([O])C(O)C([CH])O
2: O>>[CH]OC(O)C([CH])O
3: [C]C1OC(OC([CH])[CH])C(O)C(O)[CH]1>>[C]C1OC(O)C(O)C(O)[CH]1


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C1OC(OC2C(O)C(O)COC2OC3COC(OC4COC(O)C(O)C4O)C(O)C3O)C(O)C(O)C1OC>>O=C(O)C1OC(O)C(O)C(O)C1OC]
2: R:M00001, P:M00004	[O=C(O)C1OC(OC2C(O)C(O)COC2OC3COC(OC4COC(O)C(O)C4O)C(O)C3O)C(O)C(O)C1OC>>OC1OCC(OC2OCC(OC3OCC(O)C(O)C3O)C(O)C2O)C(O)C1O]
3: R:M00002, P:M00003	[O>>O=C(O)C1OC(O)C(O)C(O)C1OC]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]1[O:5][CH:6]([O:7][CH:8]2[CH:9]([OH:10])[CH:11]([OH:12])[CH2:13][O:14][CH:15]2[O:16][CH:17]3[CH2:18][O:19][CH:20]([O:21][CH:22]4[CH2:23][O:24][CH:25]([OH:26])[CH:27]([OH:28])[CH:29]4[OH:30])[CH:31]([OH:32])[CH:33]3[OH:34])[CH:35]([OH:36])[CH:37]([OH:38])[CH:39]1[O:40][CH3:41].[OH2:42]>>[O:1]=[C:2]([OH:3])[CH:4]1[O:5][CH:6]([OH:42])[CH:35]([OH:36])[CH:37]([OH:38])[CH:39]1[O:40][CH3:41].[OH:26][CH:25]1[O:24][CH2:23][CH:22]([O:21][CH:20]2[O:19][CH2:18][CH:17]([O:16][CH:15]3[O:14][CH2:13][CH:11]([OH:12])[CH:9]([OH:10])[CH:8]3[OH:7])[CH:33]([OH:34])[CH:31]2[OH:32])[CH:29]([OH:30])[CH:27]1[OH:28]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=41, 2=40, 3=39, 4=37, 5=35, 6=6, 7=5, 8=4, 9=2, 10=1, 11=3, 12=7, 13=8, 14=9, 15=11, 16=13, 17=14, 18=15, 19=16, 20=17, 21=18, 22=19, 23=20, 24=31, 25=33, 26=34, 27=32, 28=21, 29=22, 30=23, 31=24, 32=25, 33=27, 34=29, 35=30, 36=28, 37=26, 38=12, 39=10, 40=36, 41=38, 42=42}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=42, 2=41, 3=40, 4=38, 5=36, 6=34, 7=33, 8=32, 9=30, 10=29, 11=31, 12=35, 13=37, 14=39, 15=14, 16=15, 17=17, 18=19, 19=12, 20=13, 21=11, 22=10, 23=9, 24=8, 25=7, 26=23, 27=21, 28=22, 29=24, 30=6, 31=5, 32=4, 33=3, 34=2, 35=27, 36=25, 37=26, 38=28, 39=1, 40=20, 41=18, 42=16}

