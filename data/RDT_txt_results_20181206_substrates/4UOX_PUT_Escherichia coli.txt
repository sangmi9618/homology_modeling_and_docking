
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:2.0, C-O:1.0, C=O:3.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(O)C(=O)CC:1.0, O=C(O)C(=O)CC>>O=C(O)CC(N)C(=O)O:1.0, O=C(O)C(=O)CC>>O=CC[CH2]:1.0, O=C(O)C(=O)CC>>[C]CC(N)C(=O)O:1.0, O=C(O)C[CH]:1.0, O=C(O)[CH2]:1.0, O=CC[CH2]:1.0, O=C[CH2]:2.0, O=[CH]:1.0, [CH2]:1.0, [CH2]C:1.0, [CH2]C>>O=C(O)[CH2]:1.0, [CH2]CCCN>>O=CCC[CH2]:1.0, [CH2]CCN:1.0, [CH2]CCN>>O=CC[CH2]:1.0, [CH2]CCN>>[C]CC(N)C(=O)O:1.0, [CH2]CN:2.0, [CH2]CN>>O=C[CH2]:1.0, [CH2]CN>>[C]C([CH2])N:1.0, [CH2]N:1.0, [CH2]N>>[CH]N:1.0, [CH3]:1.0, [CH]:2.0, [CH]N:1.0, [C]:2.0, [C]=O:1.0, [C]=O>>O=[CH]:1.0, [C]C(=O)CC>>[C]C(N)CC(=O)O:1.0, [C]C(=O)[CH2]:2.0, [C]C(=O)[CH2]>>O=C[CH2]:1.0, [C]C(=O)[CH2]>>[C]C([CH2])N:1.0, [C]C([CH2])N:2.0, [C]CC:1.0, [C]CC(N)C(=O)O:1.0, [C]CC>>O=C(O)C[CH]:1.0, [NH2]:2.0, [O]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (6)
[[CH2]:1.0, [CH]:2.0, [C]:3.0, [NH2]:2.0, [OH]:1.0, [O]:3.0]


ID=Reaction Center at Level: 1 (10)
[O=C(O)[CH2]:2.0, O=C[CH2]:1.0, O=[CH]:1.0, [CH2]CN:1.0, [CH2]N:1.0, [CH]N:1.0, [C]=O:2.0, [C]C(=O)[CH2]:1.0, [C]C([CH2])N:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (10)
[O=C(O)C(=O)CC:1.0, O=C(O)C[CH]:2.0, O=C(O)[CH2]:2.0, O=CC[CH2]:1.0, O=C[CH2]:1.0, [CH2]CCN:1.0, [CH2]CN:1.0, [C]C(=O)[CH2]:1.0, [C]C([CH2])N:1.0, [C]CC(N)C(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(=O)[CH2]:1.0, [C]C([CH2])N:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C(=O)CC:1.0, [C]CC(N)C(=O)O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]=O>>O=[CH]
2: [CH2]N>>[CH]N
3: [C]C(=O)[CH2]>>[C]C([CH2])N
4: [CH2]C>>O=C(O)[CH2]
5: [CH2]CN>>O=C[CH2]

MMP Level 2
1: [C]C(=O)[CH2]>>O=C[CH2]
2: [CH2]CN>>[C]C([CH2])N
3: O=C(O)C(=O)CC>>[C]CC(N)C(=O)O
4: [C]CC>>O=C(O)C[CH]
5: [CH2]CCN>>O=CC[CH2]

MMP Level 3
1: O=C(O)C(=O)CC>>O=CC[CH2]
2: [CH2]CCN>>[C]CC(N)C(=O)O
3: O=C(O)C(=O)CC>>O=C(O)CC(N)C(=O)O
4: [C]C(=O)CC>>[C]C(N)CC(=O)O
5: [CH2]CCCN>>O=CCC[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(=O)CC>>O=C(O)CC(N)C(=O)O, O=C(O)C(=O)CC>>O=C(O)CC(N)C(=O)O]
2: R:M00001, P:M00004	[O=C(O)C(=O)CC>>O=CCCCN]
3: R:M00002, P:M00003	[NCCCCN>>O=C(O)CC(N)C(=O)O]
4: R:M00002, P:M00004	[NCCCCN>>O=CCCCN]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[C:4](=[O:5])[CH2:6][CH3:7].[NH2:8][CH2:9][CH2:10][CH2:11][CH2:12][NH2:13]>>[O:5]=[CH:9][CH2:10][CH2:11][CH2:12][NH2:13].[O:14]=[C:7]([OH:15])[CH2:6][CH:4]([NH2:8])[C:2](=[O:1])[OH:3]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=7, 2=6, 3=4, 4=5, 5=2, 6=1, 7=3, 8=11, 9=10, 10=9, 11=8, 12=12, 13=13}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=4, 2=5, 3=7, 4=8, 5=9, 6=6, 7=2, 8=1, 9=3, 10=13, 11=12, 12=11, 13=10, 14=14, 15=15}

