
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%O:1.0, O-P:2.0]

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)C(=O)CC([CH])O>>O=C(O)C1(O)OC([CH])[CH]C(O)C1:1.0, O=C(O)C(=O)C[CH]:1.0, O=C(O)C(=O)C[CH]>>O=C(O)C(O)(O[CH])C[CH]:2.0, O=C(O)C(O)(O[CH])C[CH]:1.0, O=P(O)(O)O:3.0, O=P(O)(O)OCC([CH])O>>[CH]C(O)CO:1.0, O=P(O)(O)OC[CH]:1.0, O=P(O)(O)OC[CH]>>O=P(O)(O)O:1.0, O=P(O)(O)OC[CH]>>[CH]CO:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[CH2]>>O=P(O)(O)O:1.0, O>>O=P(O)(O)O:2.0, O>>[P]O:1.0, O[CH2]:1.0, [CH]C(O)C(O)C(O)[CH2]>>O=C(O)C1(O)OC(C(O)[CH]C1)C(O)[CH2]:1.0, [CH]C([CH])O:1.0, [CH]C([CH])O>>[C]C(O)(OC([CH])[CH])[CH2]:1.0, [CH]CO:1.0, [CH]O:1.0, [CH]O>>[C]O[CH]:1.0, [C]:2.0, [C]=O:1.0, [C]=O>>[C]O:1.0, [C]C(=O)[CH2]:2.0, [C]C(=O)[CH2]>>[C]C([O])(O)[CH2]:2.0, [C]C(O)(OC([CH])[CH])[CH2]:1.0, [C]C([O])(O)[CH2]:2.0, [C]O:1.0, [C]O[CH]:1.0, [OH]:4.0, [O]:3.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[CH2]>>O[CH2]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[O:1.0, [C]:1.0, [OH]:1.0, [O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (7)
[O:1.0, O=P(O)(O)O:1.0, [C]C([O])(O)[CH2]:1.0, [C]O[CH]:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[CH2]:1.0]


ID=Reaction Center at Level: 2 (6)
[O:1.0, O=C(O)C(O)(O[CH])C[CH]:1.0, O=P(O)(O)O:2.0, O=P(O)(O)OC[CH]:1.0, O=P(O)(O)O[CH2]:1.0, [C]C(O)(OC([CH])[CH])[CH2]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[C]=O:1.0, [C]C(=O)[CH2]:1.0, [C]C([O])(O)[CH2]:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(O)C(=O)C[CH]:1.0, O=C(O)C(O)(O[CH])C[CH]:1.0, [C]C(=O)[CH2]:1.0, [C]C([O])(O)[CH2]:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[C]:2.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(=O)[CH2]:1.0, [C]C([O])(O)[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C(=O)C[CH]:1.0, O=C(O)C(O)(O[CH])C[CH]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [O]P(=O)(O)O>>O=P(O)(O)O
2: [C]C(=O)[CH2]>>[C]C([O])(O)[CH2]
3: [C]=O>>[C]O
4: [CH]O>>[C]O[CH]
5: [P]O[CH2]>>O[CH2]
6: O>>[P]O

MMP Level 2
1: O=P(O)(O)O[CH2]>>O=P(O)(O)O
2: O=C(O)C(=O)C[CH]>>O=C(O)C(O)(O[CH])C[CH]
3: [C]C(=O)[CH2]>>[C]C([O])(O)[CH2]
4: [CH]C([CH])O>>[C]C(O)(OC([CH])[CH])[CH2]
5: O=P(O)(O)OC[CH]>>[CH]CO
6: O>>O=P(O)(O)O

MMP Level 3
1: O=P(O)(O)OC[CH]>>O=P(O)(O)O
2: O=C(O)C(=O)CC([CH])O>>O=C(O)C1(O)OC([CH])[CH]C(O)C1
3: O=C(O)C(=O)C[CH]>>O=C(O)C(O)(O[CH])C[CH]
4: [CH]C(O)C(O)C(O)[CH2]>>O=C(O)C1(O)OC(C(O)[CH]C1)C(O)[CH2]
5: O=P(O)(O)OCC([CH])O>>[CH]C(O)CO
6: O>>O=P(O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O>>O=P(O)(O)O]
2: R:M00002, P:M00003	[O=C(O)C(=O)CC(O)C(O)C(O)C(O)COP(=O)(O)O>>O=P(O)(O)O]
3: R:M00002, P:M00004	[O=C(O)C(=O)CC(O)C(O)C(O)C(O)COP(=O)(O)O>>O=C(O)C1(O)OC(C(O)C(O)C1)C(O)CO, O=C(O)C(=O)CC(O)C(O)C(O)C(O)COP(=O)(O)O>>O=C(O)C1(O)OC(C(O)C(O)C1)C(O)CO, O=C(O)C(=O)CC(O)C(O)C(O)C(O)COP(=O)(O)O>>O=C(O)C1(O)OC(C(O)C(O)C1)C(O)CO, O=C(O)C(=O)CC(O)C(O)C(O)C(O)COP(=O)(O)O>>O=C(O)C1(O)OC(C(O)C(O)C1)C(O)CO]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[C:4](=[O:5])[CH2:6][CH:7]([OH:8])[CH:9]([OH:10])[CH:11]([OH:12])[CH:13]([OH:14])[CH2:15][O:16][P:17](=[O:18])([OH:19])[OH:20].[OH2:21]>>[O:1]=[C:2]([OH:3])[C:4]1([OH:5])[O:12][CH:11]([CH:13]([OH:14])[CH2:15][OH:16])[CH:9]([OH:10])[CH:7]([OH:8])[CH2:6]1.[O:18]=[P:17]([OH:20])([OH:21])[OH:19]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=21, 2=6, 3=7, 4=9, 5=11, 6=13, 7=15, 8=16, 9=17, 10=18, 11=19, 12=20, 13=14, 14=12, 15=10, 16=8, 17=4, 18=5, 19=2, 20=1, 21=3}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=19, 2=18, 3=17, 4=20, 5=21, 6=16, 7=14, 8=12, 9=7, 10=6, 11=4, 12=2, 13=1, 14=3, 15=5, 16=8, 17=10, 18=11, 19=9, 20=13, 21=15}

