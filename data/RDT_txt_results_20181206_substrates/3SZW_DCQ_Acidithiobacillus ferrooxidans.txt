
//
FINGERPRINTS BC

FORMED_CLEAVED
[S%S:2.0]

ORDER_CHANGED
[C-O*C=O:2.0]

//
FINGERPRINTS RC
[O=C(C=[CH])C=[CH]:2.0, O=C(C=[CH])C=[CH]>>[CH]C=C(O)C=[CH]:4.0, O=C([CH])[CH]:4.0, O=C([CH])[CH]>>[CH]C(=[CH])O:4.0, O=C1C=C[C]C=C1>>OC=1C=C[C]=CC1:2.0, S:3.0, S>>[S]SSSSS[S]:1.0, S>>[S]SSS[S]:1.0, S>>[S]S[S]:1.0, [CH]C(=[CH])O:4.0, [CH]C=C(O)C=[CH]:2.0, [C]:4.0, [C]=O:2.0, [C]=O>>[C]O:2.0, [C]O:2.0, [OH]:2.0, [O]:2.0, [S]:1.0, [S]SSS[S]:1.0, [S]S[S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[S:1.0, [S]:4.0]


ID=Reaction Center at Level: 1 (2)
[S:1.0, [S]S[S]:4.0]


ID=Reaction Center at Level: 2 (2)
[S:1.0, [S]SSS[S]:4.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:4.0, [OH]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (4)
[O=C([CH])[CH]:2.0, [CH]C(=[CH])O:2.0, [C]=O:2.0, [C]O:2.0]


ID=Reaction Center at Level: 2 (4)
[O=C(C=[CH])C=[CH]:2.0, O=C([CH])[CH]:2.0, [CH]C(=[CH])O:2.0, [CH]C=C(O)C=[CH]:2.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]=O>>[C]O
2: S>>[S]S[S]
3: O=C([CH])[CH]>>[CH]C(=[CH])O

MMP Level 2
1: O=C([CH])[CH]>>[CH]C(=[CH])O
2: S>>[S]SSS[S]
3: O=C(C=[CH])C=[CH]>>[CH]C=C(O)C=[CH]

MMP Level 3
1: O=C(C=[CH])C=[CH]>>[CH]C=C(O)C=[CH]
2: S>>[S]SSSSS[S]
3: O=C1C=C[C]C=C1>>OC=1C=C[C]=CC1


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[S>>S1SSSSSSS1]
2: R:M00002, P:M00004	[O=C1C=CC(=O)C=C1>>Oc1ccc(O)cc1, O=C1C=CC(=O)C=C1>>Oc1ccc(O)cc1]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[CH:3]=[CH:4][C:5](=[O:6])[CH:7]=[CH:8]1.[SH2:9]>>[OH:1][C:2]:1:[CH:3]:[CH:4]:[C:5]([OH:6]):[CH:7]:[CH:8]1.[S:9]1[S:10][S:11][S:12][S:13][S:14][S:15][S:16]1


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=4, 3=3, 4=2, 5=1, 6=8, 7=7, 8=5, 9=6}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=1, 2=2, 3=3, 4=4, 5=5, 6=6, 7=7, 8=8, 9=11, 10=12, 11=13, 12=15, 13=16, 14=10, 15=9, 16=14}

