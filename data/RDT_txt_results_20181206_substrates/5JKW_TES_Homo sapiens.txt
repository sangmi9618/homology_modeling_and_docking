
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%C:6.0, C-C:1.0, C-O:2.0, C=O:2.0, C@C:4.0, O=O:1.0]

ORDER_CHANGED
[C%C*C%C:1.0, C%C*C@C:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O=C([CH])[CH2]:2.0, O=C([CH])[CH2]>>O=CO:1.0, O=C([CH])[CH2]>>[C]=C([CH])[CH2]:1.0, O=C1C=C(C[CH2])C([CH])(C)CC1>>[C]=CC1=C([CH])[CH]C([CH])CC1:1.0, O=C1C=C([CH2])C(C)(CC1)C([CH])[CH2]>>[CH]C1=[C]C=C(O)C=C1:1.0, O=C1C=C([C]CC1)[CH2]>>Oc1ccc2c(c1)CC[CH]C2[CH2]:1.0, O=C1C=C2CC[CH]C([CH2])C2(C)CC1>>[C]C([CH2])C1CCC([CH])=[C]C1[CH2]:1.0, O=C1C=[C]C([CH])(C)CC1>>[CH]C([CH2])c1ccc(O)cc1[CH2]:1.0, O=CO:4.0, O=O:4.0, O=O>>O:3.0, O=O>>O=CO:2.0, O=O>>[CH]O:1.0, O=[CH]:1.0, [CH2]:6.0, [CH3]:1.0, [CH]:9.0, [CH]C(=[CH])O:1.0, [CH]C([CH2])=C(C=[CH])C([CH])[CH2]:1.0, [CH]C([CH2])C(C(=[CH])[CH2])(C)C[CH2]:1.0, [CH]C([CH2])C(C(=[CH])[CH2])(C)C[CH2]>>O=CO:1.0, [CH]C([CH2])C(C(=[CH])[CH2])(C)C[CH2]>>[C]C([CH2])C(C[CH2])C([C])[CH2]:1.0, [CH]C([CH2])C1CC[CH]C1([CH2])C:2.0, [CH]C([CH2])C1CC[CH]C1([CH2])C>>[CH]C([CH2])C1CC[CH]C1([CH2])C:1.0, [CH]C([CH])[CH2]:2.0, [CH]C([CH])[CH2]>>[C]=C([CH])[CH]:1.0, [CH]C1CCC2=C[C]CCC2(C)C1C[CH2]>>[CH]C1=[C]CCC2C1CCC3([CH]CCC23)C:1.0, [CH]C[CH2]:2.0, [CH]C[CH2]>>[CH]C(=[CH])O:1.0, [CH]O:1.0, [C]:6.0, [C]1CCC2C(CCC3([CH]CCC23)C)C1([CH2])C>>[CH]C1CCC=2C=[C]C=CC2C1C[CH2]:1.0, [C]=C([CH2])C=C([CH])O:1.0, [C]=C([CH])CC[CH]:1.0, [C]=C([CH])[CH2]:1.0, [C]=C([CH])[CH]:1.0, [C]=CC(=C([CH])[CH])C[CH2]:1.0, [C]=CC(=O)C[CH2]:1.0, [C]=CC(=O)C[CH2]>>O=CO:1.0, [C]=CC(=O)C[CH2]>>[C]=CC(=C([CH])[CH])C[CH2]:1.0, [C]=O:1.0, [C]=O>>O=[CH]:1.0, [C]C:1.0, [C]C(=[CH])CC[CH]:1.0, [C]C(=[CH])CC[CH]>>[C]=C([CH2])C=C([CH])O:1.0, [C]C(=[CH])[CH2]:1.0, [C]C(=[CH])[CH2]>>[CH]C[CH2]:1.0, [C]C([CH2])=CC(=O)[CH2]:1.0, [C]C([CH2])=CC(=O)[CH2]>>[C]=C([CH])CC[CH]:1.0, [C]C([CH2])C(C[CH2])C([C])[CH2]:2.0, [C]C([CH2])C(C[CH2])C([C])[CH2]>>[CH]C([CH2])=C(C=[CH])C([CH])[CH2]:1.0, [C]C([CH2])C1CCC(=[CH])[C]C1[CH2]>>OC=1C=C[C]=C([CH2])C1:1.0, [C]C([CH])([CH2])C:2.0, [C]C([CH])([CH2])C>>O=CO:1.0, [C]C([CH])([CH2])C>>[CH]C([CH])[CH2]:1.0, [C]C([CH])[CH2]:2.0, [C]C([CH])[CH2]>>[C]C([CH])[CH2]:1.0, [C]C1CCC2(C)C(O)CCC2C1C[CH2]>>[C]C1CCC2(C)C(O)CCC2C1C[CH2]:1.0, [C]C=C(C[CH2])C([CH])([CH2])C:1.0, [C]C=C(C[CH2])C([CH])([CH2])C>>[C]CCC([CH])[CH]:1.0, [C]C=C(O)C=[CH]:1.0, [C]C=C1CCC([CH])[CH]C1([CH2])C>>[CH]c1ccc(O)cc1C[CH2]:1.0, [C]C=CC(=[CH])O:1.0, [C]C=CC(=[C])[CH]:1.0, [C]C=[CH]:2.0, [C]C=[C]:2.0, [C]C=[C]>>[C]C[CH2]:1.0, [C]C>>O=CO:1.0, [C]CCC(=O)[CH]:1.0, [C]CCC(=O)[CH]>>[C]C=CC(=[C])[CH]:1.0, [C]CCC([CH])[CH]:2.0, [C]CCC([CH])[CH]>>[C]C=C(O)C=[CH]:1.0, [C]CCC([C])([CH])C:1.0, [C]CCC([C])([CH])C>>[C]C=CC(=[CH])O:1.0, [C]C[CH2]:4.0, [C]C[CH2]>>[C]C=[CH]:2.0, [C]C[CH2]>>[C]C=[C]:1.0, [OH]:1.0, [O]:4.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (7)
[O:1.0, [CH2]:4.0, [CH3]:1.0, [CH]:10.0, [C]:11.0, [OH]:2.0, [O]:4.0]


ID=Reaction Center at Level: 1 (20)
[O:1.0, O=C([CH])[CH2]:2.0, O=CO:2.0, O=O:2.0, O=[CH]:1.0, [CH]C(=[CH])O:2.0, [CH]C([CH])[CH2]:3.0, [CH]C[CH2]:1.0, [CH]O:1.0, [C]=C([CH])[CH2]:2.0, [C]=C([CH])[CH]:2.0, [C]=O:1.0, [C]C:1.0, [C]C(=[CH])[CH2]:1.0, [C]C([CH])([CH2])C:2.0, [C]C([CH])[CH2]:2.0, [C]C=[CH]:2.0, [C]C=[C]:1.0, [C]C[CH2]:3.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (21)
[O:1.0, O=C([CH])[CH2]:1.0, O=CO:4.0, O=O:2.0, [CH]C(=[CH])O:1.0, [CH]C([CH2])=C(C=[CH])C([CH])[CH2]:2.0, [CH]C([CH2])C(C(=[CH])[CH2])(C)C[CH2]:2.0, [CH]C([CH2])C1CC[CH]C1([CH2])C:2.0, [C]=C([CH2])C=C([CH])O:1.0, [C]=CC(=C([CH])[CH])C[CH2]:2.0, [C]=CC(=O)C[CH2]:2.0, [C]C(=[CH])CC[CH]:1.0, [C]C([CH2])C(C[CH2])C([C])[CH2]:3.0, [C]C([CH])([CH2])C:1.0, [C]C=C(C[CH2])C([CH])([CH2])C:1.0, [C]C=C(O)C=[CH]:2.0, [C]C=CC(=[CH])O:1.0, [C]C=CC(=[C])[CH]:1.0, [C]CCC(=O)[CH]:1.0, [C]CCC([CH])[CH]:1.0, [C]CCC([C])([CH])C:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[CH2]:6.0, [CH]:4.0, [C]:2.0]


ID=Reaction Center at Level: 1 (6)
[[CH]C(=[CH])O:1.0, [CH]C[CH2]:2.0, [C]C(=[CH])[CH2]:1.0, [C]C=[CH]:2.0, [C]C=[C]:2.0, [C]C[CH2]:4.0]


ID=Reaction Center at Level: 2 (11)
[[C]=C([CH2])C=C([CH])O:1.0, [C]=C([CH])CC[CH]:1.0, [C]C(=[CH])CC[CH]:1.0, [C]C([CH2])=CC(=O)[CH2]:1.0, [C]C=C(C[CH2])C([CH])([CH2])C:1.0, [C]C=C(O)C=[CH]:1.0, [C]C=CC(=[CH])O:1.0, [C]C=CC(=[C])[CH]:1.0, [C]CCC(=O)[CH]:1.0, [C]CCC([CH])[CH]:2.0, [C]CCC([C])([CH])C:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[CH]C([CH])[CH2]:1.0, [C]=C([CH])[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[CH]C([CH2])=C(C=[CH])C([CH])[CH2]:1.0, [C]C([CH2])C(C[CH2])C([C])[CH2]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C[CH2]>>[C]C=[C]
2: [C]C=[C]>>[C]C[CH2]
3: O=C([CH])[CH2]>>[C]=C([CH])[CH2]
4: [C]C>>O=CO
5: [CH]C([CH])[CH2]>>[C]=C([CH])[CH]
6: [C]C([CH])[CH2]>>[C]C([CH])[CH2]
7: [C]C(=[CH])[CH2]>>[CH]C[CH2]
8: [C]C[CH2]>>[C]C=[CH]
9: [C]C([CH])([CH2])C>>[CH]C([CH])[CH2]
10: [C]=O>>O=[CH]
11: O=O>>O
12: O=O>>[CH]O
13: [CH]C[CH2]>>[CH]C(=[CH])O
14: [C]C[CH2]>>[C]C=[CH]

MMP Level 2
1: [C]C(=[CH])CC[CH]>>[C]=C([CH2])C=C([CH])O
2: [C]C([CH2])=CC(=O)[CH2]>>[C]=C([CH])CC[CH]
3: [C]=CC(=O)C[CH2]>>[C]=CC(=C([CH])[CH])C[CH2]
4: [C]C([CH])([CH2])C>>O=CO
5: [C]C([CH2])C(C[CH2])C([C])[CH2]>>[CH]C([CH2])=C(C=[CH])C([CH])[CH2]
6: [CH]C([CH2])C1CC[CH]C1([CH2])C>>[CH]C([CH2])C1CC[CH]C1([CH2])C
7: [C]C=C(C[CH2])C([CH])([CH2])C>>[C]CCC([CH])[CH]
8: [C]CCC(=O)[CH]>>[C]C=CC(=[C])[CH]
9: [CH]C([CH2])C(C(=[CH])[CH2])(C)C[CH2]>>[C]C([CH2])C(C[CH2])C([C])[CH2]
10: O=C([CH])[CH2]>>O=CO
11: O=O>>O
12: O=O>>O=CO
13: [C]CCC([CH])[CH]>>[C]C=C(O)C=[CH]
14: [C]CCC([C])([CH])C>>[C]C=CC(=[CH])O

MMP Level 3
1: [C]C=C1CCC([CH])[CH]C1([CH2])C>>[CH]c1ccc(O)cc1C[CH2]
2: O=C1C=C(C[CH2])C([CH])(C)CC1>>[C]=CC1=C([CH])[CH]C([CH])CC1
3: O=C1C=C([C]CC1)[CH2]>>Oc1ccc2c(c1)CC[CH]C2[CH2]
4: [CH]C([CH2])C(C(=[CH])[CH2])(C)C[CH2]>>O=CO
5: [C]1CCC2C(CCC3([CH]CCC23)C)C1([CH2])C>>[CH]C1CCC=2C=[C]C=CC2C1C[CH2]
6: [C]C1CCC2(C)C(O)CCC2C1C[CH2]>>[C]C1CCC2(C)C(O)CCC2C1C[CH2]
7: O=C1C=C2CC[CH]C([CH2])C2(C)CC1>>[C]C([CH2])C1CCC([CH])=[C]C1[CH2]
8: O=C1C=[C]C([CH])(C)CC1>>[CH]C([CH2])c1ccc(O)cc1[CH2]
9: [CH]C1CCC2=C[C]CCC2(C)C1C[CH2]>>[CH]C1=[C]CCC2C1CCC3([CH]CCC23)C
10: [C]=CC(=O)C[CH2]>>O=CO
11: O=O>>O
12: O=O>>O=CO
13: [C]C([CH2])C1CCC(=[CH])[C]C1[CH2]>>OC=1C=C[C]=C([CH2])C1
14: O=C1C=C([CH2])C(C)(CC1)C([CH])[CH2]>>[CH]C1=[C]C=C(O)C=C1


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C1C=C2CCC3C(CCC4(C)C(O)CCC34)C2(C)CC1>>Oc1ccc2c(c1)CCC3C2CCC4(C)C(O)CCC34, O=C1C=C2CCC3C(CCC4(C)C(O)CCC34)C2(C)CC1>>Oc1ccc2c(c1)CCC3C2CCC4(C)C(O)CCC34, O=C1C=C2CCC3C(CCC4(C)C(O)CCC34)C2(C)CC1>>Oc1ccc2c(c1)CCC3C2CCC4(C)C(O)CCC34, O=C1C=C2CCC3C(CCC4(C)C(O)CCC34)C2(C)CC1>>Oc1ccc2c(c1)CCC3C2CCC4(C)C(O)CCC34, O=C1C=C2CCC3C(CCC4(C)C(O)CCC34)C2(C)CC1>>Oc1ccc2c(c1)CCC3C2CCC4(C)C(O)CCC34, O=C1C=C2CCC3C(CCC4(C)C(O)CCC34)C2(C)CC1>>Oc1ccc2c(c1)CCC3C2CCC4(C)C(O)CCC34, O=C1C=C2CCC3C(CCC4(C)C(O)CCC34)C2(C)CC1>>Oc1ccc2c(c1)CCC3C2CCC4(C)C(O)CCC34, O=C1C=C2CCC3C(CCC4(C)C(O)CCC34)C2(C)CC1>>Oc1ccc2c(c1)CCC3C2CCC4(C)C(O)CCC34, O=C1C=C2CCC3C(CCC4(C)C(O)CCC34)C2(C)CC1>>Oc1ccc2c(c1)CCC3C2CCC4(C)C(O)CCC34, O=C1C=C2CCC3C(CCC4(C)C(O)CCC34)C2(C)CC1>>Oc1ccc2c(c1)CCC3C2CCC4(C)C(O)CCC34]
2: R:M00001, P:M00004	[O=C1C=C2CCC3C(CCC4(C)C(O)CCC34)C2(C)CC1>>O=CO, O=C1C=C2CCC3C(CCC4(C)C(O)CCC34)C2(C)CC1>>O=CO]
3: R:M00002, P:M00004	[O=O>>O=CO]
4: R:M00002, P:M00005	[O=O>>O]


//
SELECTED AAM MAPPING
[O:22]=[O:23].[O:1]=[C:2]1[CH:3]=[C:4]2[CH2:5][CH2:6][CH:7]3[CH:8]([CH2:9][CH2:10][C:11]4([CH3:12])[CH:13]([OH:14])[CH2:15][CH2:16][CH:17]34)[C:18]2([CH3:19])[CH2:20][CH2:21]1>>[O:1]=[CH:19][OH:23].[OH2:22].[OH:24][C:6]:1:[CH:20]:[CH:21]:[C:7]2:[C:2](:[CH:5]1)[CH2:3][CH2:4][CH:18]3[CH:8]2[CH2:9][CH2:10][C:11]4([CH3:12])[CH:13]([OH:14])[CH2:15][CH2:16][CH:17]34


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=12, 2=11, 3=10, 4=9, 5=8, 6=7, 7=17, 8=16, 9=15, 10=13, 11=14, 12=6, 13=5, 14=4, 15=3, 16=2, 17=1, 18=21, 19=20, 20=18, 21=19, 22=22, 23=23}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=15, 2=14, 3=13, 4=12, 5=11, 6=10, 7=20, 8=19, 9=18, 10=16, 11=17, 12=9, 13=8, 14=6, 15=5, 16=4, 17=3, 18=2, 19=7, 20=1, 21=22, 22=21, 23=23, 24=24}

