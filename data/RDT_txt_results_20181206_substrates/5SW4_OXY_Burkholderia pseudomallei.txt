
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-O:2.0]

ORDER_CHANGED
[O-O*O=O:1.0]

//
FINGERPRINTS RC
[O:6.0, O=O:4.0, OOOO:2.0, OOOO>>O:3.0, OOOO>>O=O:3.0, [OH]:2.0, [O]:4.0, [O]O:2.0, [O]O>>O:1.0, [O]O>>O=O:1.0, [O]OO:4.0, [O]OO>>O:2.0, [O]OO>>O=O:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[O:2.0, [OH]:1.0, [O]:3.0]


ID=Reaction Center at Level: 1 (3)
[O:2.0, [O]O:1.0, [O]OO:3.0]


ID=Reaction Center at Level: 2 (3)
[O:2.0, OOOO:3.0, [O]OO:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (2)
[[OH]:1.0, [O]:3.0]


ID=Reaction Center at Level: 1 (3)
[O=O:2.0, [O]O:1.0, [O]OO:1.0]


ID=Reaction Center at Level: 2 (3)
[O=O:2.0, OOOO:1.0, [O]OO:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]O>>O
2: [O]OO>>O
3: [O]O>>O=O
4: [O]OO>>O=O

MMP Level 2
1: [O]OO>>O
2: OOOO>>O
3: [O]OO>>O=O
4: OOOO>>O=O

MMP Level 3
1: OOOO>>O
2: OOOO>>O
3: OOOO>>O=O
4: OOOO>>O=O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[OOOO>>O=O, OOOO>>O=O]
2: R:M00001, P:M00003	[OOOO>>O, OOOO>>O]


//
SELECTED AAM MAPPING
[OH:1][O:2][O:3][OH:4]>>[O:1]=[O:2].[OH2:4].[OH2:3]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=1, 2=2, 3=3, 4=4}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=1, 2=2, 3=4}

