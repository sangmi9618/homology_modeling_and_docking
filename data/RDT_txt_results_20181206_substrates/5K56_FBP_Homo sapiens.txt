
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O=P(O)(O)O:3.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[CH2]>>O=P(O)(O)O:1.0, O>>O=P(O)(O)O:2.0, O>>[P]O:1.0, OCC1(O)O[CH][CH]C1O:1.0, O[CH2]:1.0, [C]:2.0, [C]CO:1.0, [C]COP(=O)(O)O:1.0, [C]COP(=O)(O)O>>O=P(O)(O)O:1.0, [C]COP(=O)(O)O>>[C]CO:1.0, [OH]:2.0, [O]:1.0, [O]C([CH])(O)COP(=O)(O)O>>[O]C([CH])(O)CO:1.0, [O]C([CH])(O)[CH2]:2.0, [O]C([CH])(O)[CH2]>>[O]C([CH])(O)[CH2]:1.0, [O]CC1(O)O[CH][CH]C1O:1.0, [O]CC1(O)O[CH][CH]C1O>>OCC1(O)O[CH][CH]C1O:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]OCC1(O)OC([CH2])C(O)C1O>>OCC1(O)OC([CH2])C(O)C1O:1.0, [P]O[CH2]:1.0, [P]O[CH2]>>O[CH2]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=P(O)(O)O:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=P(O)(O)O:2.0, O=P(O)(O)O[CH2]:1.0, [C]COP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[C]:2.0]


ID=Reaction Center at Level: 1 (1)
[[O]C([CH])(O)[CH2]:2.0]


ID=Reaction Center at Level: 2 (2)
[OCC1(O)O[CH][CH]C1O:1.0, [O]CC1(O)O[CH][CH]C1O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: O>>[P]O
2: [O]P(=O)(O)O>>O=P(O)(O)O
3: [O]C([CH])(O)[CH2]>>[O]C([CH])(O)[CH2]
4: [P]O[CH2]>>O[CH2]

MMP Level 2
1: O>>O=P(O)(O)O
2: O=P(O)(O)O[CH2]>>O=P(O)(O)O
3: [O]CC1(O)O[CH][CH]C1O>>OCC1(O)O[CH][CH]C1O
4: [C]COP(=O)(O)O>>[C]CO

MMP Level 3
1: O>>O=P(O)(O)O
2: [C]COP(=O)(O)O>>O=P(O)(O)O
3: [P]OCC1(O)OC([CH2])C(O)C1O>>OCC1(O)OC([CH2])C(O)C1O
4: [O]C([CH])(O)COP(=O)(O)O>>[O]C([CH])(O)CO


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OCC1OC(O)(COP(=O)(O)O)C(O)C1O>>O=P(O)(O)OCC1OC(O)(CO)C(O)C1O, O=P(O)(O)OCC1OC(O)(COP(=O)(O)O)C(O)C1O>>O=P(O)(O)OCC1OC(O)(CO)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OCC1OC(O)(COP(=O)(O)O)C(O)C1O>>O=P(O)(O)O]
3: R:M00002, P:M00004	[O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH2:6][CH:7]1[O:8][C:9]([OH:10])([CH2:11][O:12][P:13](=[O:14])([OH:15])[OH:16])[CH:17]([OH:18])[CH:19]1[OH:20].[OH2:21]>>[O:14]=[P:13]([OH:15])([OH:16])[OH:21].[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH2:6][CH:7]1[O:8][C:9]([OH:10])([CH2:11][OH:12])[CH:17]([OH:18])[CH:19]1[OH:20]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=19, 4=17, 5=9, 6=8, 7=11, 8=12, 9=13, 10=14, 11=15, 12=16, 13=10, 14=18, 15=20, 16=5, 17=2, 18=1, 19=3, 20=4, 21=21}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=15, 4=13, 5=9, 6=8, 7=11, 8=12, 9=10, 10=14, 11=16, 12=5, 13=2, 14=1, 15=3, 16=4, 17=19, 18=18, 19=17, 20=20, 21=21}

