
//
FINGERPRINTS BC

FORMED_CLEAVED
[N-O:1.0]

//
FINGERPRINTS RC
[[N]:2.0, [N]=O:2.0, [N][O-]:1.0, [O-]:1.0, [O-]N=O:3.0, [O-]N=O>>[N]=O:3.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[N]:1.0, [O-]:1.0]


ID=Reaction Center at Level: 1 (2)
[[N][O-]:1.0, [O-]N=O:1.0]


ID=Reaction Center at Level: 2 (1)
[[O-]N=O:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O-]N=O>>[N]=O

MMP Level 2
1: [O-]N=O>>[N]=O

MMP Level 3
1: [O-]N=O>>[N]=O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[[O-]N=O>>[N]=O]


//
SELECTED AAM MAPPING
[O:1]=[N:2][O-:3]>>[O:1]=[NH:2]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=2, 2=1, 3=3}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=1, 2=2}

