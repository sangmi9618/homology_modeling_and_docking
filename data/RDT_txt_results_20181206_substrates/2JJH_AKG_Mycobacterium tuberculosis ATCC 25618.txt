
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:2.0, C=O:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(=O)C[CH2]>>O=C(O)C(N)C[CH2]:1.0, O=C(O)C(=O)C[CH2]>>O=CC[CH2]:1.0, O=C(O)C(N)C[CH2]:1.0, O=CC[CH2]:1.0, O=C[CH2]:2.0, O=[CH]:1.0, [CH2]:1.0, [CH2]CCCN>>O=CCC[CH2]:1.0, [CH2]CCN:1.0, [CH2]CCN>>O=C(O)C(N)C[CH2]:1.0, [CH2]CCN>>O=CC[CH2]:1.0, [CH2]CN:2.0, [CH2]CN>>O=C[CH2]:1.0, [CH2]CN>>[C]C([CH2])N:1.0, [CH2]N:1.0, [CH2]N>>[CH]N:1.0, [CH]:2.0, [CH]N:1.0, [C]:1.0, [C]=O:1.0, [C]=O>>O=[CH]:1.0, [C]C(=O)[CH2]:2.0, [C]C(=O)[CH2]>>O=C[CH2]:1.0, [C]C(=O)[CH2]>>[C]C([CH2])N:1.0, [C]C([CH2])N:2.0, [C]CCC(=O)C(=O)O>>[C]CCC(N)C(=O)O:1.0, [NH2]:2.0, [O]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[CH2]:1.0, [CH]:2.0, [C]:1.0, [NH2]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (8)
[O=C[CH2]:1.0, O=[CH]:1.0, [CH2]CN:1.0, [CH2]N:1.0, [CH]N:1.0, [C]=O:1.0, [C]C(=O)[CH2]:1.0, [C]C([CH2])N:1.0]


ID=Reaction Center at Level: 2 (8)
[O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(N)C[CH2]:1.0, O=CC[CH2]:1.0, O=C[CH2]:1.0, [CH2]CCN:1.0, [CH2]CN:1.0, [C]C(=O)[CH2]:1.0, [C]C([CH2])N:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(=O)[CH2]:1.0, [C]C([CH2])N:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(N)C[CH2]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C(=O)[CH2]>>[C]C([CH2])N
2: [CH2]CN>>O=C[CH2]
3: [CH2]N>>[CH]N
4: [C]=O>>O=[CH]

MMP Level 2
1: O=C(O)C(=O)C[CH2]>>O=C(O)C(N)C[CH2]
2: [CH2]CCN>>O=CC[CH2]
3: [CH2]CN>>[C]C([CH2])N
4: [C]C(=O)[CH2]>>O=C[CH2]

MMP Level 3
1: [C]CCC(=O)C(=O)O>>[C]CCC(N)C(=O)O
2: [CH2]CCCN>>O=CCC[CH2]
3: [CH2]CCN>>O=C(O)C(N)C[CH2]
4: O=C(O)C(=O)C[CH2]>>O=CC[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(=O)CCC(=O)O>>O=C(O)CCC(N)C(=O)O]
2: R:M00001, P:M00004	[O=C(O)C(=O)CCC(=O)O>>O=CCCCC(N)C(=O)O]
3: R:M00002, P:M00003	[O=C(O)C(N)CCCCN>>O=C(O)CCC(N)C(=O)O]
4: R:M00002, P:M00004	[O=C(O)C(N)CCCCN>>O=CCCCC(N)C(=O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[C:4](=[O:5])[CH2:6][CH2:7][C:8](=[O:9])[OH:10].[O:11]=[C:12]([OH:13])[CH:14]([NH2:15])[CH2:16][CH2:17][CH2:18][CH2:19][NH2:20]>>[O:5]=[CH:19][CH2:18][CH2:17][CH2:16][CH:14]([NH2:15])[C:12](=[O:11])[OH:13].[O:9]=[C:8]([OH:10])[CH2:7][CH2:6][CH:4]([NH2:20])[C:2](=[O:1])[OH:3]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=8, 4=9, 5=10, 6=4, 7=5, 8=2, 9=1, 10=3, 11=17, 12=18, 13=19, 14=20, 15=16, 16=14, 17=12, 18=11, 19=13, 20=15}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=5, 2=4, 3=2, 4=1, 5=3, 6=6, 7=8, 8=9, 9=10, 10=7, 11=14, 12=13, 13=12, 14=11, 15=15, 16=16, 17=18, 18=19, 19=20, 20=17}

