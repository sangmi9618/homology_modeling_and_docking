
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0, C=O:2.0]

//
FINGERPRINTS RC
[O:3.0, O=C(N[CH])C(N)C:1.0, O=C(N[CH])C(N)C>>O=C(O)C([CH2])N:1.0, O=C(N[CH])C(N)C>>[C]C(N)CN[CH]:1.0, O=C(O)C([CH2])N:1.0, O=C([CH])O:3.0, O=C([CH])[NH]:2.0, O=C([CH])[NH]>>O=C([CH])O:1.0, O=C([CH])[NH]>>[CH]C[NH]:1.0, O=C([NH])C(N)C>>O=C(O)C(N)C[NH]:1.0, O>>O=C(O)C([CH2])N:1.0, O>>O=C([CH])O:1.0, O>>[C]O:1.0, [CH2]:1.0, [CH3]:1.0, [CH]C:1.0, [CH]C>>O=C([CH])O:1.0, [CH]C[NH]:1.0, [C]:2.0, [C]=O:2.0, [C]=O>>[C]=O:1.0, [C]C(N)C:1.0, [C]C(N)C>>O=C(O)C([CH2])N:1.0, [C]C(N)CN[CH]:1.0, [C]C(NC(=O)C(N)C)C>>[C]C(NCC(N)C(=O)O)C:1.0, [C]O:1.0, [OH]:1.0, [O]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [C]:3.0, [OH]:1.0, [O]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=C([CH])O:2.0, O=C([CH])[NH]:1.0, [C]=O:2.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=C(N[CH])C(N)C:1.0, O=C(O)C([CH2])N:2.0, O=C([CH])O:2.0, O=C([CH])[NH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH]C>>O=C([CH])O
2: O=C([CH])[NH]>>[CH]C[NH]
3: O>>[C]O
4: [C]=O>>[C]=O

MMP Level 2
1: [C]C(N)C>>O=C(O)C([CH2])N
2: O=C(N[CH])C(N)C>>[C]C(N)CN[CH]
3: O>>O=C([CH])O
4: O=C([CH])[NH]>>O=C([CH])O

MMP Level 3
1: O=C([NH])C(N)C>>O=C(O)C(N)C[NH]
2: [C]C(NC(=O)C(N)C)C>>[C]C(NCC(N)C(=O)O)C
3: O>>O=C(O)C([CH2])N
4: O=C(N[CH])C(N)C>>O=C(O)C([CH2])N


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(NC(=O)C(N)C)C>>O=C(O)C(N)CNC(C(=O)O)C, O=C(O)C(NC(=O)C(N)C)C>>O=C(O)C(N)CNC(C(=O)O)C, O=C(O)C(NC(=O)C(N)C)C>>O=C(O)C(N)CNC(C(=O)O)C]
2: R:M00002, P:M00003	[O>>O=C(O)C(N)CNC(C(=O)O)C]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([NH:5][C:6](=[O:7])[CH:8]([NH2:9])[CH3:10])[CH3:11].[OH2:12]>>[O:7]=[C:10]([OH:12])[CH:8]([NH2:9])[CH2:6][NH:5][CH:4]([C:2](=[O:1])[OH:3])[CH3:11]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=10, 2=8, 3=6, 4=7, 5=5, 6=4, 7=11, 8=2, 9=1, 10=3, 11=9, 12=12}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=12, 2=8, 3=9, 4=10, 5=11, 6=7, 7=6, 8=4, 9=2, 10=1, 11=3, 12=5}

