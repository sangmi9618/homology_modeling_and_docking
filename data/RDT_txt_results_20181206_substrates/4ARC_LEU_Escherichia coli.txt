
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)O[P]:1.0, O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]:1.0, O=P(O)(O[P])O[P]:1.0, O=P(O)(O[P])O[P]>>O=P(O)(O)O[P]:1.0, [OH]:1.0, [O]:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)OP(=O)(O)OP(=O)(O)O>>O=P(O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [O]P([O])(=O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (4)
[[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[P]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]P([O])(=O)O>>[O]P(=O)(O)O
2: [P]O[P]>>[P]O

MMP Level 2
1: O=P(O)(O[P])O[P]>>O=P(O)(O)O[P]
2: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O

MMP Level 3
1: [O]P(=O)(O)OP(=O)(O)OP(=O)(O)O>>O=P(O)(O)OP(=O)(O)O
2: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]


//
SELECTED AAM MAPPING
[O:32]=[C:33]([OH:34])[CH:35]([NH2:36])[CH2:37][CH:38]([CH3:39])[CH3:40].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:32]=[C:33]([OH:34])[CH:35]([NH2:36])[CH2:37][CH:38]([CH3:39])[CH3:40].[O:11]=[P:10]([OH:9])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[OH:41]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=39, 33=38, 34=40, 35=37, 36=35, 37=33, 38=32, 39=34, 40=36}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=26, 2=25, 3=24, 4=27, 5=28, 6=29, 7=30, 8=31, 9=32, 10=16, 11=17, 12=18, 13=13, 14=14, 15=15, 16=19, 17=12, 18=11, 19=10, 20=9, 21=20, 22=22, 23=7, 24=8, 25=6, 26=5, 27=2, 28=1, 29=3, 30=4, 31=23, 32=21, 33=40, 34=39, 35=41, 36=38, 37=36, 38=34, 39=33, 40=35, 41=37}

