
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0, C-O:1.0, O=O:1.0]

ORDER_CHANGED
[C%C*C%C:1.0, C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(=O)C[CH2]>>O=C(O)C[CH2]:1.0, O=C(O)C(=O)C[CH2]>>O=C=O:1.0, O=C(O)C(=O)[CH2]:1.0, O=C(O)C(=O)[CH2]>>O=C=O:2.0, O=C(O)C1N2[C]CC2CC1>>O=C(O)C1=CCC2N1[C]C2:1.0, O=C(O)C[CH2]:1.0, O=C(O)[CH2]:2.0, O=C1N2C(C(=O)O)CCC2C1>>O=C1N2C(=CCC2C1)C(=O)O:1.0, O=C=O:3.0, O=O:4.0, O=O>>O:3.0, O=O>>O=C(O)C[CH2]:1.0, O=O>>O=C(O)[CH2]:1.0, O=O>>[C]O:1.0, [CH2]:1.0, [CH]:4.0, [CH]C[CH2]:1.0, [CH]C[CH2]>>[C]=C[CH2]:1.0, [C]:5.0, [C]1N2[CH]CCC2C1:1.0, [C]1N2[CH]CCC2C1>>[C]1N2[C]=CCC2C1:1.0, [C]1N2[C]=CCC2C1:1.0, [C]=C[CH2]:1.0, [C]=O:1.0, [C]C(=O)O:2.0, [C]C(=O)O>>O=C=O:2.0, [C]C(=O)[CH2]:1.0, [C]C(=O)[CH2]>>O=C(O)[CH2]:1.0, [C]C([N])=[CH]:1.0, [C]C([N])[CH2]:1.0, [C]C([N])[CH2]>>[C]C([N])=[CH]:1.0, [C]C1N2C(=O)CC2CC1>>[C]C1=CCC2N1C(=O)C2:1.0, [C]C1[N][CH]CC1:1.0, [C]C1[N][CH]CC1>>[C]C=1[N][CH]CC1:1.0, [C]C=1[N][CH]CC1:1.0, [C]CCC(=O)C(=O)O>>[C]CCC(=O)O:1.0, [C]N1[CH]CC=C1C(=O)O:1.0, [C]N1[CH]CCC1C(=O)O:1.0, [C]N1[CH]CCC1C(=O)O>>[C]N1[CH]CC=C1C(=O)O:1.0, [C]O:2.0, [C]O>>[C]=O:1.0, [N]C([CH2])[CH2]:2.0, [N]C([CH2])[CH2]>>[N]C([CH2])[CH2]:1.0, [OH]:2.0, [O]:3.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [C]:3.0, [OH]:1.0, [O]:2.0]


ID=Reaction Center at Level: 1 (6)
[O:1.0, O=C(O)[CH2]:1.0, O=O:2.0, [C]C(=O)O:1.0, [C]C(=O)[CH2]:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (6)
[O:1.0, O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(=O)[CH2]:1.0, O=C(O)C[CH2]:1.0, O=C(O)[CH2]:1.0, O=O:2.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (5)
[[CH2]:1.0, [CH]:2.0, [C]:3.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (8)
[O=C=O:1.0, [CH]C[CH2]:1.0, [C]=C[CH2]:1.0, [C]=O:1.0, [C]C(=O)O:1.0, [C]C([N])=[CH]:1.0, [C]C([N])[CH2]:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (7)
[O=C(O)C(=O)[CH2]:1.0, O=C=O:2.0, [C]C(=O)O:1.0, [C]C1[N][CH]CC1:1.0, [C]C=1[N][CH]CC1:1.0, [C]N1[CH]CC=C1C(=O)O:1.0, [C]N1[CH]CCC1C(=O)O:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:3.0, [C]:1.0]


ID=Reaction Center at Level: 1 (3)
[[C]C([N])=[CH]:1.0, [C]C([N])[CH2]:1.0, [N]C([CH2])[CH2]:2.0]


ID=Reaction Center at Level: 2 (4)
[[C]1N2[CH]CCC2C1:1.0, [C]1N2[C]=CCC2C1:1.0, [C]N1[CH]CC=C1C(=O)O:1.0, [C]N1[CH]CCC1C(=O)O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]C[CH2]>>[C]=C[CH2]
2: [C]O>>[C]=O
3: [C]C([N])[CH2]>>[C]C([N])=[CH]
4: O=O>>O
5: [C]C(=O)O>>O=C=O
6: [N]C([CH2])[CH2]>>[N]C([CH2])[CH2]
7: O=O>>[C]O
8: [C]C(=O)[CH2]>>O=C(O)[CH2]

MMP Level 2
1: [C]C1[N][CH]CC1>>[C]C=1[N][CH]CC1
2: [C]C(=O)O>>O=C=O
3: [C]N1[CH]CCC1C(=O)O>>[C]N1[CH]CC=C1C(=O)O
4: O=O>>O
5: O=C(O)C(=O)[CH2]>>O=C=O
6: [C]1N2[CH]CCC2C1>>[C]1N2[C]=CCC2C1
7: O=O>>O=C(O)[CH2]
8: O=C(O)C(=O)C[CH2]>>O=C(O)C[CH2]

MMP Level 3
1: O=C(O)C1N2[C]CC2CC1>>O=C(O)C1=CCC2N1[C]C2
2: O=C(O)C(=O)[CH2]>>O=C=O
3: O=C1N2C(C(=O)O)CCC2C1>>O=C1N2C(=CCC2C1)C(=O)O
4: O=O>>O
5: O=C(O)C(=O)C[CH2]>>O=C=O
6: [C]C1N2C(=O)CC2CC1>>[C]C1=CCC2N1C(=O)C2
7: O=O>>O=C(O)C[CH2]
8: [C]CCC(=O)C(=O)O>>[C]CCC(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C1N2C(C(=O)O)CCC2C1>>O=C1N2C(=CCC2C1)C(=O)O, O=C1N2C(C(=O)O)CCC2C1>>O=C1N2C(=CCC2C1)C(=O)O, O=C1N2C(C(=O)O)CCC2C1>>O=C1N2C(=CCC2C1)C(=O)O]
2: R:M00002, P:M00005	[O=C(O)C(=O)CCC(=O)O>>O=C(O)CCC(=O)O]
3: R:M00002, P:M00006	[O=C(O)C(=O)CCC(=O)O>>O=C=O, O=C(O)C(=O)CCC(=O)O>>O=C=O]
4: R:M00003, P:M00005	[O=O>>O=C(O)CCC(=O)O]
5: R:M00003, P:M00007	[O=O>>O]


//
SELECTED AAM MAPPING
[O:22]=[O:23].[O:12]=[C:13]([OH:14])[C:15](=[O:16])[CH2:17][CH2:18][C:19](=[O:20])[OH:21].[O:1]=[C:2]([OH:3])[CH:4]1[N:5]2[C:6](=[O:7])[CH2:8][CH:9]2[CH2:10][CH2:11]1>>[O:12]=[C:13]=[O:14].[O:1]=[C:2]([OH:3])[C:4]1=[CH:11][CH2:10][CH:9]2[N:5]1[C:6](=[O:7])[CH2:8]2.[O:16]=[C:15]([OH:22])[CH2:17][CH2:18][C:19](=[O:20])[OH:21].[OH2:23]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=10, 2=11, 3=4, 4=5, 5=9, 6=8, 7=6, 8=7, 9=2, 10=1, 11=3, 12=17, 13=18, 14=19, 15=20, 16=21, 17=15, 18=16, 19=13, 20=12, 21=14, 22=22, 23=23}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=5, 3=4, 4=8, 5=7, 6=11, 7=9, 8=10, 9=2, 10=1, 11=3, 12=16, 13=15, 14=13, 15=12, 16=14, 17=17, 18=18, 19=19, 20=21, 21=20, 22=22, 23=23}

