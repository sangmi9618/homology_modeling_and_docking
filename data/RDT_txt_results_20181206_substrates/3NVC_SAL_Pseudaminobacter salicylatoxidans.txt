
//
FINGERPRINTS BC

FORMED_CLEAVED
[C=O:2.0, C@C:1.0, O=O:1.0]

ORDER_CHANGED
[C-C*C@C:1.0]

//
FINGERPRINTS RC
[O=C(O)C(=C[CH])C(=[CH])O:1.0, O=C(O)C(=C[CH])C(=[CH])O>>O=C(O)C=C[CH]:1.0, O=C(O)C(=O)C=[CH]:1.0, O=C(O)C=C[CH]:1.0, O=C(O)c1ccccc1O>>O=C(O)C(=O)C=[CH]:1.0, O=C(O)c1ccccc1O>>O=C(O)C=CC=[CH]:1.0, O=C([CH])C(=O)O:1.0, O=O:4.0, O=O>>O=C(O)C(=O)C=[CH]:1.0, O=O>>O=C([CH])C(=O)O:1.0, O=O>>[C]=O:2.0, O=O>>[C]C(=O)O:1.0, O=O>>[C]C(=O)[CH]:1.0, [CH]:2.0, [C]:4.0, [C]=C[CH]:1.0, [C]=C[CH]>>[C]C(=O)[CH]:1.0, [C]=O:2.0, [C]C(=O)O:2.0, [C]C(=O)[CH]:2.0, [C]C(=[CH])C(O)=C[CH]:1.0, [C]C(=[CH])C(O)=C[CH]>>O=C([CH])C(=O)O:1.0, [C]C(=[CH])O:1.0, [C]C(=[CH])O>>[C]C(=O)O:1.0, [C]C(O)=CC=[CH]:1.0, [C]C(O)=CC=[CH]>>O=C(O)C(=O)C=[CH]:1.0, [C]C([C])=[CH]:1.0, [C]C([C])=[CH]>>[C]C=[CH]:1.0, [C]C=[CH]:1.0, [C]c1ccccc1O>>O=C(O)C(=O)C=C[CH]:1.0, [O]:4.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[C]:4.0, [O]:4.0]


ID=Reaction Center at Level: 1 (6)
[O=O:2.0, [C]=O:2.0, [C]C(=O)O:1.0, [C]C(=O)[CH]:1.0, [C]C(=[CH])O:1.0, [C]C([C])=[CH]:1.0]


ID=Reaction Center at Level: 2 (7)
[O=C(O)C(=C[CH])C(=[CH])O:1.0, O=C(O)C(=O)C=[CH]:1.0, O=C([CH])C(=O)O:1.0, O=O:2.0, [C]C(=O)O:1.0, [C]C(=O)[CH]:1.0, [C]C(=[CH])C(O)=C[CH]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:3.0]


ID=Reaction Center at Level: 1 (4)
[[C]=C[CH]:1.0, [C]C(=O)O:1.0, [C]C(=O)[CH]:1.0, [C]C(=[CH])O:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(O)C(=O)C=[CH]:1.0, O=C([CH])C(=O)O:1.0, [C]C(=[CH])C(O)=C[CH]:1.0, [C]C(O)=CC=[CH]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]=C[CH]>>[C]C(=O)[CH]
2: [C]C([C])=[CH]>>[C]C=[CH]
3: O=O>>[C]=O
4: O=O>>[C]=O
5: [C]C(=[CH])O>>[C]C(=O)O

MMP Level 2
1: [C]C(O)=CC=[CH]>>O=C(O)C(=O)C=[CH]
2: O=C(O)C(=C[CH])C(=[CH])O>>O=C(O)C=C[CH]
3: O=O>>[C]C(=O)O
4: O=O>>[C]C(=O)[CH]
5: [C]C(=[CH])C(O)=C[CH]>>O=C([CH])C(=O)O

MMP Level 3
1: [C]c1ccccc1O>>O=C(O)C(=O)C=C[CH]
2: O=C(O)c1ccccc1O>>O=C(O)C=CC=[CH]
3: O=O>>O=C([CH])C(=O)O
4: O=O>>O=C(O)C(=O)C=[CH]
5: O=C(O)c1ccccc1O>>O=C(O)C(=O)C=[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)c1ccccc1O>>O=C(O)C=CC=CC(=O)C(=O)O, O=C(O)c1ccccc1O>>O=C(O)C=CC=CC(=O)C(=O)O, O=C(O)c1ccccc1O>>O=C(O)C=CC=CC(=O)C(=O)O]
2: R:M00002, P:M00003	[O=O>>O=C(O)C=CC=CC(=O)C(=O)O, O=O>>O=C(O)C=CC=CC(=O)C(=O)O]


//
SELECTED AAM MAPPING
[O:11]=[O:12].[O:1]=[C:2]([OH:3])[C:4]:1:[CH:5]:[CH:6]:[CH:7]:[CH:8]:[C:9]1[OH:10]>>[O:1]=[C:2]([OH:3])[CH:4]=[CH:5][CH:6]=[CH:7][C:8](=[O:11])[C:9](=[O:12])[OH:10]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=8, 4=9, 5=4, 6=5, 7=2, 8=1, 9=3, 10=10, 11=11, 12=12}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=8, 4=9, 5=10, 6=11, 7=12, 8=5, 9=4, 10=2, 11=1, 12=3}

