
//
FINGERPRINTS BC

ORDER_CHANGED
[C-C*C=C:1.0, C-O*C=O:1.0]

//
FINGERPRINTS RC
[O=C(O)C(=O)CC(=C[CH])C=[CH]>>[O-]C(=CC(=C[CH])C=[CH])C(=O)O:1.0, O=C(O)C(=O)CC([CH])=[CH]>>[O-]C(=CC([CH])=[CH])C(=O)O:1.0, [CH2]:1.0, [CH]:1.0, [C]:2.0, [C]=O:1.0, [C]=O>>[C][O-]:1.0, [C]C(=O)CC([CH])=[CH]:1.0, [C]C(=O)CC([CH])=[CH]>>[C]C([O-])=CC([CH])=[CH]:1.0, [C]C(=O)[CH2]:2.0, [C]C(=O)[CH2]>>[C]C([O-])=[CH]:2.0, [C]C([O-])=CC([CH])=[CH]:1.0, [C]C([O-])=[CH]:2.0, [C]C=C([O-])C(=O)O:1.0, [C]C=[C]:1.0, [C]CC(=O)C(=O)O:1.0, [C]CC(=O)C(=O)O>>[C]C=C([O-])C(=O)O:2.0, [C]C[C]:1.0, [C]C[C]>>[C]C=[C]:1.0, [C][O-]:1.0, [O-]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (5)
[[CH2]:1.0, [CH]:1.0, [C]:2.0, [O-]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (6)
[[C]=O:1.0, [C]C(=O)[CH2]:1.0, [C]C([O-])=[CH]:1.0, [C]C=[C]:1.0, [C]C[C]:1.0, [C][O-]:1.0]


ID=Reaction Center at Level: 2 (6)
[[C]C(=O)CC([CH])=[CH]:1.0, [C]C(=O)[CH2]:1.0, [C]C([O-])=CC([CH])=[CH]:1.0, [C]C([O-])=[CH]:1.0, [C]C=C([O-])C(=O)O:1.0, [C]CC(=O)C(=O)O:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C(=O)[CH2]>>[C]C([O-])=[CH]
2: [C]C[C]>>[C]C=[C]
3: [C]=O>>[C][O-]

MMP Level 2
1: [C]CC(=O)C(=O)O>>[C]C=C([O-])C(=O)O
2: [C]C(=O)CC([CH])=[CH]>>[C]C([O-])=CC([CH])=[CH]
3: [C]C(=O)[CH2]>>[C]C([O-])=[CH]

MMP Level 3
1: O=C(O)C(=O)CC([CH])=[CH]>>[O-]C(=CC([CH])=[CH])C(=O)O
2: O=C(O)C(=O)CC(=C[CH])C=[CH]>>[O-]C(=CC(=C[CH])C=[CH])C(=O)O
3: [C]CC(=O)C(=O)O>>[C]C=C([O-])C(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)C(=O)Cc1ccc(O)cc1>>[O-]C(=Cc1ccc(O)cc1)C(=O)O, O=C(O)C(=O)Cc1ccc(O)cc1>>[O-]C(=Cc1ccc(O)cc1)C(=O)O, O=C(O)C(=O)Cc1ccc(O)cc1>>[O-]C(=Cc1ccc(O)cc1)C(=O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[C:4](=[O:5])[CH2:6][C:7]:1:[CH:8]:[CH:9]:[C:10]([OH:11]):[CH:12]:[CH:13]1>>[O:1]=[C:2]([OH:3])[C:4]([O-:5])=[CH:6][C:7]:1:[CH:8]:[CH:9]:[C:10]([OH:11]):[CH:12]:[CH:13]1


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=8, 2=9, 3=10, 4=12, 5=13, 6=7, 7=6, 8=4, 9=5, 10=2, 11=1, 12=3, 13=11}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=8, 2=9, 3=10, 4=12, 5=13, 6=7, 7=6, 8=4, 9=2, 10=1, 11=3, 12=5, 13=11}

