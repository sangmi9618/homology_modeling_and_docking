
//
FINGERPRINTS BC

FORMED_CLEAVED
[Br-C:1.0, O-O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C1C=CN([CH])[C]N1>>O=C1N[C]N([CH])C=C1Br:1.0, OO:4.0, OO>>O:3.0, [Br-]:3.0, [Br-]>>[C]Br:1.0, [Br-]>>[C]C(Br)=[CH]:1.0, [Br-]>>[N]C=C(Br)C(=O)[NH]:1.0, [Br]:1.0, [CH]:1.0, [C]:1.0, [C]Br:1.0, [C]C(Br)=[CH]:2.0, [C]C=[CH]:1.0, [C]C=[CH]>>[C]C(Br)=[CH]:1.0, [N]C=C(Br)C(=O)[NH]:1.0, [N]C=CC(=O)[NH]:1.0, [N]C=CC(=O)[NH]>>[N]C=C(Br)C(=O)[NH]:1.0, [OH]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[O:1.0, [Br-]:1.0, [Br]:1.0, [C]:1.0, [OH]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, OO:2.0, [Br-]:1.0, [C]Br:1.0, [C]C(Br)=[CH]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, OO:2.0, [Br-]:1.0, [C]C(Br)=[CH]:1.0, [N]C=C(Br)C(=O)[NH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C=[CH]>>[C]C(Br)=[CH]
2: OO>>O
3: [Br-]>>[C]Br

MMP Level 2
1: [N]C=CC(=O)[NH]>>[N]C=C(Br)C(=O)[NH]
2: OO>>O
3: [Br-]>>[C]C(Br)=[CH]

MMP Level 3
1: O=C1C=CN([CH])[C]N1>>O=C1N[C]N([CH])C=C1Br
2: OO>>O
3: [Br-]>>[N]C=C(Br)C(=O)[NH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=c1ccn(c(=O)[nH]1)C2OC(CO)C(O)C2>>O=c1[nH]c(=O)n(cc1Br)C2OC(CO)C(O)C2]
2: R:M00002, P:M00004	[[Br-]>>O=c1[nH]c(=O)n(cc1Br)C2OC(CO)C(O)C2]
3: R:M00003, P:M00005	[OO>>O]


//
SELECTED AAM MAPPING
[Br-:18].[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][OH:13])[CH:14]([OH:15])[CH2:16]2.[OH:17][OH:19]>>[O:1]=[C:2]1[NH:8][C:6](=[O:7])[N:5]([CH:4]=[C:3]1[Br:18])[CH:9]2[O:10][CH:11]([CH2:12][OH:13])[CH:14]([OH:15])[CH2:16]2.[OH2:17]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=16, 2=14, 3=11, 4=10, 5=9, 6=5, 7=4, 8=3, 9=2, 10=1, 11=8, 12=6, 13=7, 14=12, 15=13, 16=15, 17=19, 18=17, 19=18}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=17, 2=15, 3=12, 4=11, 5=10, 6=6, 7=7, 8=8, 9=2, 10=1, 11=3, 12=4, 13=5, 14=9, 15=13, 16=14, 17=16, 18=18}

