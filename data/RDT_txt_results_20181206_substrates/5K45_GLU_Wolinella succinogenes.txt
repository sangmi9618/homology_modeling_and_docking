
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[N:3.0, O:3.0, O=C(N)CC[CH]>>O=C(O)CC[CH]:1.0, O=C(N)C[CH2]:1.0, O=C(N)C[CH2]>>N:1.0, O=C(N)C[CH2]>>O=C(O)C[CH2]:1.0, O=C(O)C[CH2]:1.0, O=C(O)[CH2]:2.0, O=C([CH2])N:2.0, O=C([CH2])N>>N:1.0, O=C([CH2])N>>O=C(O)[CH2]:1.0, O>>O=C(O)C[CH2]:1.0, O>>O=C(O)[CH2]:1.0, O>>[C]O:1.0, [C]:2.0, [C]N:1.0, [C]N>>N:1.0, [C]O:1.0, [NH2]:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[N:1.0, O:1.0, [C]:2.0, [NH2]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (6)
[N:1.0, O:1.0, O=C(O)[CH2]:1.0, O=C([CH2])N:1.0, [C]N:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (6)
[N:1.0, O:1.0, O=C(N)C[CH2]:1.0, O=C(O)C[CH2]:1.0, O=C(O)[CH2]:1.0, O=C([CH2])N:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O>>[C]O
2: O=C([CH2])N>>O=C(O)[CH2]
3: [C]N>>N

MMP Level 2
1: O>>O=C(O)[CH2]
2: O=C(N)C[CH2]>>O=C(O)C[CH2]
3: O=C([CH2])N>>N

MMP Level 3
1: O>>O=C(O)C[CH2]
2: O=C(N)CC[CH]>>O=C(O)CC[CH]
3: O=C(N)C[CH2]>>N


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)CCC(=O)N>>O=C(O)CCC(N)C(=O)O]
2: R:M00001, P:M00004	[O=C(O)C(N)CCC(=O)N>>N]
3: R:M00002, P:M00003	[O>>O=C(O)CCC(N)C(=O)O]


//
SELECTED AAM MAPPING
[O:9]=[C:8]([NH2:10])[CH2:7][CH2:6][CH:4]([NH2:5])[C:2](=[O:1])[OH:3].[OH2:11]>>[O:9]=[C:8]([OH:11])[CH2:7][CH2:6][CH:4]([NH2:5])[C:2](=[O:1])[OH:3].[NH3:10]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=8, 4=9, 5=10, 6=4, 7=2, 8=1, 9=3, 10=5, 11=11}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=5, 2=4, 3=2, 4=1, 5=3, 6=6, 7=8, 8=9, 9=10, 10=7, 11=11}

