
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)O[CH]>>O=P(O)(O[CH])OP(=O)(O)O[CH2]:1.0, O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[CH2]:2.0, O=P(O)(O[P])O[CH2]>>O=P(O)(O[P])O[CH2]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>[O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP(=O)(O)OC[CH]>>[O]P(=O)(O)OP(=O)(O)OC[CH]:1.0, [O]P(=O)(O)OP([O])(=O)O:2.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:2.0, [O]P([O])(=O)O>>[O]P([O])(=O)O:1.0, [P]:2.0, [P]O:2.0, [P]O>>[P]O[P]:1.0, [P]O[P]:2.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]P([O])(=O)O:2.0, [P]O[P]:2.0]


ID=Reaction Center at Level: 2 (2)
[O=P(O)(O[P])O[CH2]:2.0, [O]P(=O)(O)OP([O])(=O)O:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]P([O])(=O)O>>[O]P([O])(=O)O
2: [P]O[P]>>[P]O
3: [P]O>>[P]O[P]

MMP Level 2
1: O=P(O)(O[P])O[CH2]>>O=P(O)(O[P])O[CH2]
2: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
3: [O]P(=O)(O)O>>[O]P(=O)(O)OP([O])(=O)O

MMP Level 3
1: [O]P(=O)(O)OP(=O)(O)OC[CH]>>[O]P(=O)(O)OP(=O)(O)OC[CH]
2: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]
3: O=P(O)(O)O[CH]>>O=P(O)(O[CH])OP(=O)(O)O[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2>>O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3O)C(O)C2]
2: R:M00001, P:M00004	[O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2>>O=P(O)(O)OP(=O)(O)O]
3: R:M00002, P:M00003	[O=P(O)(O)OC1OC(CO)C(O)C(O)C1O>>O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3O)C(O)C2]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[NH:3][C:4](=[O:5])[N:6]([CH:7]=[C:8]1[CH3:9])[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][P:23](=[O:24])([OH:25])[OH:26])[CH:27]([OH:28])[CH2:29]2.[O:30]=[P:31]([OH:32])([OH:33])[O:34][CH:35]1[O:36][CH:37]([CH2:38][OH:39])[CH:40]([OH:41])[CH:42]([OH:43])[CH:44]1[OH:45]>>[O:1]=[C:2]1[NH:3][C:4](=[O:5])[N:6]([CH:7]=[C:8]1[CH3:9])[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:32][P:31](=[O:30])([OH:33])[O:34][CH:35]3[O:36][CH:37]([CH2:38][OH:39])[CH:40]([OH:41])[CH:42]([OH:43])[CH:44]3[OH:45])[CH:27]([OH:28])[CH2:29]2.[O:20]=[P:19]([OH:18])([OH:21])[O:22][P:23](=[O:24])([OH:25])[OH:26]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=4, 6=5, 7=3, 8=2, 9=1, 10=10, 11=29, 12=27, 13=12, 14=11, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=25, 28=26, 29=28, 30=38, 31=37, 32=40, 33=42, 34=44, 35=35, 36=36, 37=34, 38=31, 39=30, 40=32, 41=33, 42=45, 43=43, 44=41, 45=39}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=4, 6=5, 7=3, 8=2, 9=1, 10=10, 11=36, 12=34, 13=12, 14=11, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=32, 27=30, 28=28, 29=25, 30=24, 31=26, 32=27, 33=29, 34=31, 35=33, 36=35, 37=39, 38=38, 39=37, 40=40, 41=41, 42=42, 43=43, 44=44, 45=45}

