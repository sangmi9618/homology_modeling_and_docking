
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%C:1.0, C%N:1.0, C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O=CCC([CH])=[CH]>>[C]C=C1C(=[CH])CCNC1CC([CH])=[CH]:1.0, O=C[CH2]:2.0, O=C[CH2]>>O:1.0, O=C[CH2]>>[C]C([NH])[CH2]:1.0, O=[CH]:1.0, O=[CH]>>O:1.0, OC1=[C]C=C(C=C1)C[CH2]>>[C]CC1NCCC2=C[C]=C(O)C=C21:1.0, [CH2]CN:1.0, [CH2]CN>>[C]C([CH2])NC[CH2]:1.0, [CH2]N:1.0, [CH2]N>>[CH]N[CH2]:1.0, [CH]:3.0, [CH]N[CH2]:1.0, [C]:1.0, [C]C(=[CH])C(N[CH2])C[C]:1.0, [C]C([CH2])NC[CH2]:1.0, [C]C([CH])=[CH]:1.0, [C]C([NH])[CH2]:1.0, [C]C=C(C(=[CH])[CH2])C([NH])[CH2]:1.0, [C]C=CC(=[CH])[CH2]:1.0, [C]C=CC(=[CH])[CH2]>>[C]C=C(C(=[CH])[CH2])C([NH])[CH2]:1.0, [C]C=[CH]:1.0, [C]C=[CH]>>[C]C([CH])=[CH]:1.0, [C]CC=O:1.0, [C]CC=O>>O:1.0, [C]CC=O>>[C]C(=[CH])C(N[CH2])C[C]:1.0, [C]CCN>>[C]CC1NCC[C]C1=[CH]:1.0, [NH2]:1.0, [NH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[O:1.0, [CH]:3.0, [C]:1.0, [NH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (6)
[O:1.0, O=C[CH2]:1.0, O=[CH]:1.0, [CH]N[CH2]:1.0, [C]C([CH])=[CH]:1.0, [C]C([NH])[CH2]:2.0]


ID=Reaction Center at Level: 2 (6)
[O:1.0, O=C[CH2]:1.0, [C]C(=[CH])C(N[CH2])C[C]:2.0, [C]C([CH2])NC[CH2]:1.0, [C]C=C(C(=[CH])[CH2])C([NH])[CH2]:1.0, [C]CC=O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (2)
[O=C[CH2]:1.0, [C]C([NH])[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[[C]C(=[CH])C(N[CH2])C[C]:1.0, [C]CC=O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: O=C[CH2]>>[C]C([NH])[CH2]
2: O=[CH]>>O
3: [CH2]N>>[CH]N[CH2]
4: [C]C=[CH]>>[C]C([CH])=[CH]

MMP Level 2
1: [C]CC=O>>[C]C(=[CH])C(N[CH2])C[C]
2: O=C[CH2]>>O
3: [CH2]CN>>[C]C([CH2])NC[CH2]
4: [C]C=CC(=[CH])[CH2]>>[C]C=C(C(=[CH])[CH2])C([NH])[CH2]

MMP Level 3
1: O=CCC([CH])=[CH]>>[C]C=C1C(=[CH])CCNC1CC([CH])=[CH]
2: [C]CC=O>>O
3: [C]CCN>>[C]CC1NCC[C]C1=[CH]
4: OC1=[C]C=C(C=C1)C[CH2]>>[C]CC1NCCC2=C[C]=C(O)C=C21


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[Oc1ccc(cc1O)CCN>>Oc1ccc(cc1)CC2NCCc3cc(O)c(O)cc32, Oc1ccc(cc1O)CCN>>Oc1ccc(cc1)CC2NCCc3cc(O)c(O)cc32]
2: R:M00002, P:M00003	[O=CCc1ccc(O)cc1>>O]
3: R:M00002, P:M00004	[O=CCc1ccc(O)cc1>>Oc1ccc(cc1)CC2NCCc3cc(O)c(O)cc32]


//
SELECTED AAM MAPPING
[O:12]=[CH:13][CH2:14][C:15]:1:[CH:16]:[CH:17]:[C:18]([OH:19]):[CH:20]:[CH:21]1.[OH:1][C:2]:1:[CH:3]:[CH:4]:[C:5](:[CH:6]:[C:7]1[OH:8])[CH2:9][CH2:10][NH2:11]>>[OH2:12].[OH:19][C:18]:1:[CH:17]:[CH:16]:[C:15](:[CH:21]:[CH:20]1)[CH2:14][CH:13]2[NH:11][CH2:10][CH2:9][C:5]:3:[CH:6]:[C:7]([OH:8]):[C:2]([OH:1]):[CH:3]:[C:4]32


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=4, 2=3, 3=2, 4=7, 5=6, 6=5, 7=9, 8=10, 9=11, 10=8, 11=1, 12=16, 13=17, 14=18, 15=20, 16=21, 17=15, 18=14, 19=13, 20=12, 21=19}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=21, 2=12, 3=11, 4=10, 5=9, 6=20, 7=19, 8=17, 9=15, 10=14, 11=13, 12=16, 13=18, 14=8, 15=5, 16=4, 17=3, 18=2, 19=7, 20=6, 21=1}

