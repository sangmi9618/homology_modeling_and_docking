
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)OC([CH])[CH]:1.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[CH]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O[CH]:1.0, OC1[CH]OC([CH2])C1O>>O=P(O)(O)OC1C(O)[CH]OC1[CH2]:1.0, [CH]C([CH])O:1.0, [CH]C([CH])O>>O=P(O)(O)OC([CH])[CH]:1.0, [CH]O:1.0, [CH]O>>[P]O[CH]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC([CH])[CH]:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[O]P(=O)(O)O:2.0, [P]O[CH]:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)OC([CH])[CH]:1.0, O=P(O)(O)O[CH]:1.0, O=P(O)(O)O[P]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[P]>>[P]O
2: [O]P(=O)(O)O>>[O]P(=O)(O)O
3: [CH]O>>[P]O[CH]

MMP Level 2
1: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
2: O=P(O)(O)O[P]>>O=P(O)(O)O[CH]
3: [CH]C([CH])O>>O=P(O)(O)OC([CH])[CH]

MMP Level 3
1: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
2: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC([CH])[CH]
3: OC1[CH]OC([CH2])C1O>>O=P(O)(O)OC1C(O)[CH]OC1[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OC1C(O)C(OC1COP(=O)(O)OS(=O)(=O)O)n2cnc3c(ncnc32)N]
3: R:M00002, P:M00004	[O=P(O)(OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O)OS(=O)(=O)O>>O=P(O)(O)OC1C(O)C(OC1COP(=O)(O)OS(=O)(=O)O)n2cnc3c(ncnc32)N]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31].[O:32]=[P:33]([OH:34])([O:35][CH2:36][CH:37]1[O:38][CH:39]([N:40]:2:[CH:41]:[N:42]:[C:43]:3:[C:44](:[N:45]:[CH:46]:[N:47]:[C:48]32)[NH2:49])[CH:50]([OH:51])[CH:52]1[OH:53])[O:54][S:55](=[O:56])(=[O:57])[OH:58]>>[O:1]=[P:2]([OH:3])([OH:4])[O:53][CH:52]1[CH:50]([OH:51])[CH:39]([O:38][CH:37]1[CH2:36][O:35][P:33](=[O:32])([OH:34])[O:54][S:55](=[O:56])(=[O:57])[OH:58])[N:40]:2:[CH:41]:[N:42]:[C:43]:3:[C:44](:[N:45]:[CH:46]:[N:47]:[C:48]32)[NH2:49].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=46, 33=47, 34=48, 35=43, 36=44, 37=45, 38=49, 39=42, 40=41, 41=40, 42=39, 43=50, 44=52, 45=37, 46=38, 47=36, 48=35, 49=33, 50=32, 51=34, 52=54, 53=55, 54=56, 55=57, 56=58, 57=53, 58=51}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=51, 2=52, 3=53, 4=48, 5=49, 6=50, 7=54, 8=47, 9=46, 10=45, 11=44, 12=55, 13=57, 14=42, 15=43, 16=41, 17=40, 18=37, 19=38, 20=39, 21=36, 22=33, 23=32, 24=34, 25=35, 26=58, 27=56, 28=28, 29=29, 30=30, 31=25, 32=26, 33=27, 34=31, 35=24, 36=23, 37=22, 38=9, 39=7, 40=6, 41=11, 42=10, 43=12, 44=13, 45=14, 46=15, 47=16, 48=17, 49=18, 50=19, 51=20, 52=21, 53=5, 54=2, 55=1, 56=3, 57=4, 58=8}

