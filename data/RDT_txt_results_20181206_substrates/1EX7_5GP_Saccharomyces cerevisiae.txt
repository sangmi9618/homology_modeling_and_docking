
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[CH2]>>O=P(O)(O)OP(=O)(O)O[CH2]:1.0, O=P(O)(O)O[P]:2.0, O=P(O)(O)O[P]>>O=P(O)(O)O[P]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:4.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:2.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O>>[P]O[P]:1.0, [P]O[P]:2.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]P(=O)(O)O:2.0, [P]O[P]:2.0]


ID=Reaction Center at Level: 2 (2)
[O=P(O)(O)O[P]:2.0, [O]P(=O)(O)OP(=O)(O)O:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]P(=O)(O)O>>[O]P(=O)(O)O
2: [P]O[P]>>[P]O
3: [P]O>>[P]O[P]

MMP Level 2
1: O=P(O)(O)O[P]>>O=P(O)(O)O[P]
2: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
3: [O]P(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O

MMP Level 3
1: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O
2: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
3: O=P(O)(O)O[CH2]>>O=P(O)(O)OP(=O)(O)O[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)O)C(O)C3O]
3: R:M00002, P:M00004	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)O)C(O)C3O]


//
SELECTED AAM MAPPING
[O:32]=[C:33]1[N:34]=[C:35]([NH2:36])[NH:37][C:38]:2:[C:39]1:[N:40]:[CH:41]:[N:42]2[CH:43]3[O:44][CH:45]([CH2:46][O:47][P:48](=[O:49])([OH:50])[OH:51])[CH:52]([OH:53])[CH:54]3[OH:55].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:32]=[C:33]1[N:34]=[C:35]([NH2:36])[NH:37][C:38]:2:[C:39]1:[N:40]:[CH:41]:[N:42]2[CH:43]3[O:44][CH:45]([CH2:46][O:47][P:48](=[O:49])([OH:50])[O:51][P:2](=[O:1])([OH:3])[OH:4])[CH:52]([OH:53])[CH:54]3[OH:55].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=41, 33=40, 34=39, 35=38, 36=42, 37=43, 38=54, 39=52, 40=45, 41=44, 42=46, 43=47, 44=48, 45=49, 46=50, 47=51, 48=53, 49=55, 50=37, 51=35, 52=34, 53=33, 54=32, 55=36}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=48, 2=49, 3=50, 4=45, 5=46, 6=47, 7=51, 8=44, 9=43, 10=42, 11=41, 12=52, 13=54, 14=39, 15=40, 16=38, 17=37, 18=34, 19=35, 20=36, 21=33, 22=30, 23=29, 24=31, 25=32, 26=55, 27=53, 28=10, 29=9, 30=8, 31=7, 32=11, 33=12, 34=27, 35=25, 36=14, 37=13, 38=15, 39=16, 40=17, 41=18, 42=19, 43=20, 44=21, 45=22, 46=23, 47=24, 48=26, 49=28, 50=6, 51=4, 52=3, 53=2, 54=1, 55=5}

