
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:3.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)[CH2]:2.0, O=C(OCC[N+](C)(C)C)[CH2]>>OCC[N+](C)(C)C:1.0, O=C(OC[CH2])[CH2]:2.0, O=C(OC[CH2])[CH2]>>O=C(O)[CH2]:2.0, O>>OC[CH2]:1.0, O>>O[CH2]:1.0, O>>[N+]CCO:1.0, OC[CH2]:2.0, O[CH2]:1.0, [CH2]:3.0, [C]O:2.0, [C]OCC[N+]:2.0, [C]OCC[N+]>>[N+]CCO:1.0, [C]O[CH2]:2.0, [C]O[CH2]>>[C]O:2.0, [N+]CCO:1.0, [N+]CCOC(=O)C[CH2]>>O=C(O)C[CH2]:2.0, [OH]:3.0, [O]:2.0, [O]C[CH2]:2.0, [O]C[CH2]>>OC[CH2]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [CH2]:3.0, [OH]:1.0, [O]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, OC[CH2]:1.0, O[CH2]:1.0, [C]O[CH2]:2.0, [O]C[CH2]:2.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=C(OC[CH2])[CH2]:2.0, OC[CH2]:1.0, [C]OCC[N+]:2.0, [N+]CCO:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O>>O[CH2]
2: [C]O[CH2]>>[C]O
3: [O]C[CH2]>>OC[CH2]

MMP Level 2
1: O>>OC[CH2]
2: O=C(OC[CH2])[CH2]>>O=C(O)[CH2]
3: [C]OCC[N+]>>[N+]CCO

MMP Level 3
1: O>>[N+]CCO
2: [N+]CCOC(=O)C[CH2]>>O=C(O)C[CH2]
3: O=C(OCC[N+](C)(C)C)[CH2]>>OCC[N+](C)(C)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(OCC[N+](C)(C)C)CCC(=O)OCC[N+](C)(C)C>>OCC[N+](C)(C)C]
2: R:M00001, P:M00004	[O=C(OCC[N+](C)(C)C)CCC(=O)OCC[N+](C)(C)C>>O=C(O)CCC(=O)O]
3: R:M00002, P:M00003	[O>>OCC[N+](C)(C)C]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([O:3][CH2:16][CH2:17][N+:18]([CH3:19])([CH3:20])[CH3:21])[CH2:4][CH2:5][C:6](=[O:7])[O:8][CH2:9][CH2:10][N+:11]([CH3:12])([CH3:13])[CH3:14].[OH2:15]>>[O:1]=[C:2]([OH:3])[CH2:4][CH2:5][C:6](=[O:7])[OH:8].[OH:15][CH2:9][CH2:10][N+:11]([CH3:12])([CH3:13])[CH3:14]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=7, 2=6, 3=8, 4=9, 5=5, 6=4, 7=3, 8=2, 9=1, 10=10, 11=11, 12=12, 13=13, 14=14, 15=15, 16=16, 17=17, 18=18, 19=19, 20=20, 21=21}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=13, 2=12, 3=14, 4=15, 5=11, 6=10, 7=9, 8=5, 9=4, 10=2, 11=1, 12=3, 13=6, 14=7, 15=8}

