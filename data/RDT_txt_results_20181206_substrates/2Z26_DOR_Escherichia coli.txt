
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(N)N[CH]>>O=C1N[CH]CC(=O)N1:1.0, O=C(O)C[CH]:1.0, O=C(O)C[CH]>>O:1.0, O=C(O)C[CH]>>[C]NC(=O)C[CH]:1.0, O=C(O)[CH2]:2.0, O=C(O)[CH2]>>O:1.0, O=C(O)[CH2]>>O=C([NH])[CH2]:1.0, O=C([NH])N:1.0, O=C([NH])N>>O=C([NH])NC(=O)[CH2]:1.0, O=C([NH])NC(=O)[CH2]:1.0, O=C([NH])[CH2]:1.0, [C]:2.0, [C]C([NH])CC(=O)O>>[C]C1NC(=O)NC(=O)C1:1.0, [C]N:1.0, [C]N>>[C]N[C]:1.0, [C]NC(=O)C[CH]:1.0, [C]N[C]:1.0, [C]O:1.0, [C]O>>O:1.0, [NH2]:1.0, [NH]:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [C]:2.0, [NH]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=C(O)[CH2]:1.0, O=C([NH])[CH2]:1.0, [C]N[C]:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=C(O)C[CH]:1.0, O=C(O)[CH2]:1.0, O=C([NH])NC(=O)[CH2]:1.0, [C]NC(=O)C[CH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=C(O)[CH2]>>O=C([NH])[CH2]
2: [C]N>>[C]N[C]
3: [C]O>>O

MMP Level 2
1: O=C(O)C[CH]>>[C]NC(=O)C[CH]
2: O=C([NH])N>>O=C([NH])NC(=O)[CH2]
3: O=C(O)[CH2]>>O

MMP Level 3
1: [C]C([NH])CC(=O)O>>[C]C1NC(=O)NC(=O)C1
2: O=C(N)N[CH]>>O=C1N[CH]CC(=O)N1
3: O=C(O)C[CH]>>O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(N)NC(C(=O)O)CC(=O)O>>O=C1NC(=O)CC(N1)C(=O)O, O=C(N)NC(C(=O)O)CC(=O)O>>O=C1NC(=O)CC(N1)C(=O)O]
2: R:M00001, P:M00003	[O=C(N)NC(C(=O)O)CC(=O)O>>O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([NH2:3])[NH:4][CH:5]([C:6](=[O:7])[OH:8])[CH2:9][C:10](=[O:11])[OH:12]>>[O:1]=[C:2]1[NH:3][C:10](=[O:11])[CH2:9][CH:5]([NH:4]1)[C:6](=[O:7])[OH:8].[OH2:12]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=5, 3=6, 4=7, 5=8, 6=4, 7=2, 8=1, 9=3, 10=10, 11=11, 12=12}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=8, 4=2, 5=1, 6=3, 7=4, 8=5, 9=9, 10=10, 11=11, 12=12}

