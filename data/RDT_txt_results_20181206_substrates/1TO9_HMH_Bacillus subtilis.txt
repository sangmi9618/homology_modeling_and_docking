
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O:3.0, O>>O[CH2]:1.0, O>>[C]C(=[CH])CO:1.0, O>>[C]CO:1.0, O[CH2]:1.0, [CH2]:2.0, [C]=1SC=NC1C:1.0, [C]C(=[CH])CO:1.0, [C]C(=[CH])C[N+]([C])=[CH]:1.0, [C]C(=[CH])C[N+]([C])=[CH]>>[C]C(=[CH])CO:1.0, [C]C(=[CH])C[n+]1csc([CH2])c1C>>n1csc([CH2])c1C:1.0, [C]CO:2.0, [C]C[N+]:1.0, [C]C[N+]1=CS[C]=C1C:1.0, [C]C[N+]1=CS[C]=C1C>>[C]=1SC=NC1C:1.0, [C]C[N+]>>[C]CO:1.0, [C]N=[CH]:1.0, [C][N+](=[CH])[CH2]:1.0, [C][N+](=[CH])[CH2]>>[C]N=[CH]:1.0, [N+]:1.0, [N]:1.0, [N]C=C(C(=[N])N)C[N+]1=CS[C]=C1C>>[N]C=C(C(=[N])N)CO:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [CH2]:2.0, [N+]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O[CH2]:1.0, [C]CO:1.0, [C]C[N+]:1.0, [C][N+](=[CH])[CH2]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, [C]C(=[CH])CO:1.0, [C]C(=[CH])C[N+]([C])=[CH]:1.0, [C]CO:1.0, [C]C[N+]1=CS[C]=C1C:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C][N+](=[CH])[CH2]>>[C]N=[CH]
2: O>>O[CH2]
3: [C]C[N+]>>[C]CO

MMP Level 2
1: [C]C[N+]1=CS[C]=C1C>>[C]=1SC=NC1C
2: O>>[C]CO
3: [C]C(=[CH])C[N+]([C])=[CH]>>[C]C(=[CH])CO

MMP Level 3
1: [C]C(=[CH])C[n+]1csc([CH2])c1C>>n1csc([CH2])c1C
2: O>>[C]C(=[CH])CO
3: [N]C=C(C(=[N])N)C[N+]1=CS[C]=C1C>>[N]C=C(C(=[N])N)CO


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[OCCc1sc[n+](c1C)Cc2cnc(nc2N)C>>OCc1cnc(nc1N)C]
2: R:M00001, P:M00004	[OCCc1sc[n+](c1C)Cc2cnc(nc2N)C>>OCCc1scnc1C]
3: R:M00002, P:M00003	[O>>OCc1cnc(nc1N)C]


//
SELECTED AAM MAPPING
[OH2:19].[OH:1][CH2:2][CH2:3][C:4]:1:[S:5]:[CH:6]:[N+:7](:[C:8]1[CH3:9])[CH2:10][C:11]:2:[CH:12]:[N:13]:[C:14](:[N:15]:[C:16]2[NH2:17])[CH3:18]>>[OH:19][CH2:10][C:11]:1:[CH:12]:[N:13]:[C:14](:[N:15]:[C:16]1[NH2:17])[CH3:18].[OH:1][CH2:2][CH2:3][C:4]:1:[S:5]:[CH:6]:[N:7]:[C:8]1[CH3:9]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=4, 4=5, 5=6, 6=7, 7=10, 8=11, 9=12, 10=13, 11=14, 12=15, 13=16, 14=17, 15=18, 16=3, 17=2, 18=1, 19=19}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=10, 2=6, 3=5, 4=4, 5=3, 6=8, 7=7, 8=9, 9=2, 10=1, 11=19, 12=18, 13=14, 14=15, 15=16, 16=17, 17=13, 18=12, 19=11}

