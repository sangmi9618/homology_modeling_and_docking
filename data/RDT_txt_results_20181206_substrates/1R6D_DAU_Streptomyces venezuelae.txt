
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O:3.0, O=C([CH])C(O)C([CH])O:1.0, O=C([CH])[CH]:2.0, OCC1O[CH]C(O)C(O)C1O>>O=C1C(O)C(O)[CH]OC1C:1.0, O[CH2]:1.0, O[CH2]>>O:1.0, [CH2]:1.0, [CH3]:1.0, [CH]:3.0, [CH]C:1.0, [CH]C(O)C(O)C([CH])O:1.0, [CH]C(O)C(O)C([CH])O>>O=C([CH])C(O)C([CH])O:1.0, [CH]C([CH])O:3.0, [CH]C([CH])O>>O=C([CH])[CH]:2.0, [CH]C([CH])O>>[C]C([CH])O:1.0, [CH]CO:2.0, [CH]CO>>O:1.0, [CH]CO>>[CH]C:1.0, [CH]O:1.0, [CH]O>>[C]=O:1.0, [CH]OC(CO)C([CH])O>>O=C([CH])C(O[CH])C:1.0, [C]:1.0, [C]=O:1.0, [C]C([CH])O:1.0, [C]C([O])C:1.0, [OH]:2.0, [O]:1.0, [O]C(C(=O)C([CH])O)C:1.0, [O]C([CH2])C(O)C([CH])O:1.0, [O]C([CH2])C(O)C([CH])O>>[O]C(C(=O)C([CH])O)C:2.0, [O]C([CH])CO:1.0, [O]C([CH])CO>>O:1.0, [O]C([CH])CO>>[C]C([O])C:1.0, [O]C1OC([CH2])C(O)C(O)C1O>>[O]C1OC(C(=O)C(O)C1O)C:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[O:1.0, [CH2]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (3)
[O:1.0, O[CH2]:1.0, [CH]CO:1.0]


ID=Reaction Center at Level: 2 (3)
[O:1.0, [CH]CO:1.0, [O]C([CH])CO:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH]:1.0, [C]:1.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([CH])[CH]:1.0, [CH]C([CH])O:1.0, [CH]O:1.0, [C]=O:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C([CH])[CH]:1.0, [CH]C([CH])O:1.0, [O]C(C(=O)C([CH])O)C:1.0, [O]C([CH2])C(O)C([CH])O:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:3.0, [C]:1.0]


ID=Reaction Center at Level: 1 (3)
[O=C([CH])[CH]:1.0, [CH]C([CH])O:2.0, [C]C([CH])O:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C([CH])C(O)C([CH])O:1.0, [CH]C(O)C(O)C([CH])O:1.0, [O]C(C(=O)C([CH])O)C:1.0, [O]C([CH2])C(O)C([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]C([CH])O>>O=C([CH])[CH]
2: [CH]O>>[C]=O
3: O[CH2]>>O
4: [CH]C([CH])O>>[C]C([CH])O
5: [CH]CO>>[CH]C

MMP Level 2
1: [O]C([CH2])C(O)C([CH])O>>[O]C(C(=O)C([CH])O)C
2: [CH]C([CH])O>>O=C([CH])[CH]
3: [CH]CO>>O
4: [CH]C(O)C(O)C([CH])O>>O=C([CH])C(O)C([CH])O
5: [O]C([CH])CO>>[C]C([O])C

MMP Level 3
1: OCC1O[CH]C(O)C(O)C1O>>O=C1C(O)C(O)[CH]OC1C
2: [O]C([CH2])C(O)C([CH])O>>[O]C(C(=O)C([CH])O)C
3: [O]C([CH])CO>>O
4: [O]C1OC([CH2])C(O)C(O)C1O>>[O]C1OC(C(=O)C(O)C1O)C
5: [CH]OC(CO)C([CH])O>>O=C([CH])C(O[CH])C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3O)C(O)C2>>O]
2: R:M00001, P:M00003	[O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3O)C(O)C2>>O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(C(=O)C(O)C3O)C)C(O)C2, O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3O)C(O)C2>>O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(C(=O)C(O)C3O)C)C(O)C2, O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3O)C(O)C2>>O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(C(=O)C(O)C3O)C)C(O)C2, O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3O)C(O)C2>>O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(C(=O)C(O)C3O)C)C(O)C2]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[NH:3][C:4](=[O:5])[N:6]([CH:7]=[C:8]1[CH3:9])[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH:23]3[O:24][CH:25]([CH2:26][OH:27])[CH:28]([OH:29])[CH:30]([OH:31])[CH:32]3[OH:33])[CH:34]([OH:35])[CH2:36]2>>[O:1]=[C:2]1[NH:3][C:4](=[O:5])[N:6]([CH:7]=[C:8]1[CH3:9])[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH:23]3[O:24][CH:25]([C:28](=[O:29])[CH:30]([OH:31])[CH:32]3[OH:33])[CH3:26])[CH:34]([OH:35])[CH2:36]2.[OH2:27]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=4, 6=5, 7=3, 8=2, 9=1, 10=10, 11=36, 12=34, 13=12, 14=11, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=32, 27=30, 28=28, 29=25, 30=24, 31=26, 32=27, 33=29, 34=31, 35=33, 36=35}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=36, 2=32, 3=25, 4=26, 5=27, 6=28, 7=30, 8=23, 9=24, 10=22, 11=19, 12=20, 13=21, 14=18, 15=15, 16=16, 17=17, 18=14, 19=13, 20=12, 21=33, 22=35, 23=10, 24=11, 25=6, 26=7, 27=8, 28=2, 29=1, 30=3, 31=4, 32=5, 33=9, 34=34, 35=31, 36=29}

