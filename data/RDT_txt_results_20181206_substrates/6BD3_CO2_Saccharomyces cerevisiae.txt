
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:2.0, C-O:1.0]

ORDER_CHANGED
[C-O*C=O:2.0]

//
FINGERPRINTS RC
[O=C(O)C(=O)[CH2]:1.0, O=C(O)C(=O)[CH2]>>O=C=O:2.0, O=C(O)C(O)(C(=O)C)C:1.0, O=C(O[CH2])C(=O)C>>O=C(O)C(O)(C(=O)C)C:1.0, O=C=O:3.0, [CH2]:1.0, [CH3]:1.0, [C]:6.0, [C]=O:2.0, [C]=O>>[C]O:1.0, [C]C:1.0, [C]C(=O)C:4.0, [C]C(=O)C>>[C]C([C])(O)C:2.0, [C]C(=O)COC(=O)C(=O)C>>[C]C(O)(C(=O)O)C:1.0, [C]C(=O)O:3.0, [C]C(=O)O>>O=C=O:2.0, [C]C(=O)OCC(=O)C(=O)O>>[C]C(O)(C(=O)C)C:1.0, [C]C(=O)OC[C]:1.0, [C]C(=O)OC[C]>>[C]C(=O)O:1.0, [C]C(=O)[CH2]:1.0, [C]C(=O)[CH2]>>[C]C(=O)C:1.0, [C]C(O)(C(=O)C)C:1.0, [C]C([C])(O)C:2.0, [C]C[O]:1.0, [C]C[O]>>[C]C:1.0, [C]O:3.0, [C]O>>[C]=O:1.0, [C]OCC(=O)C(=O)O>>O=C(O)C(O)(C(=O)C)C:1.0, [C]OCC([C])=O:1.0, [C]OCC([C])=O>>[C]C(=O)C:1.0, [C]O[CH2]:1.0, [C]O[CH2]>>[C]O:1.0, [OH]:3.0, [O]:3.0, [O]C(=O)C(=O)C:1.0, [O]C(=O)C(=O)C>>O=C(O)C(O)(C(=O)C)C:2.0, [O]CC(=O)C(=O)O:1.0, [O]CC(=O)C(=O)O>>O=C=O:1.0, [O]CC(=O)C(=O)O>>[C]C(O)(C(=O)C)C:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH2]:1.0, [C]:4.0, [O]:1.0]


ID=Reaction Center at Level: 1 (6)
[[C]C(=O)C:1.0, [C]C(=O)O:1.0, [C]C(=O)[CH2]:1.0, [C]C([C])(O)C:1.0, [C]C[O]:1.0, [C]O[CH2]:1.0]


ID=Reaction Center at Level: 2 (6)
[O=C(O)C(=O)[CH2]:1.0, O=C(O)C(O)(C(=O)C)C:1.0, [C]C(=O)OC[C]:1.0, [C]C(O)(C(=O)C)C:1.0, [C]OCC([C])=O:1.0, [O]CC(=O)C(=O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:4.0, [OH]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (6)
[O=C=O:1.0, [C]=O:2.0, [C]C(=O)C:1.0, [C]C(=O)O:1.0, [C]C([C])(O)C:1.0, [C]O:2.0]


ID=Reaction Center at Level: 2 (7)
[O=C(O)C(=O)[CH2]:1.0, O=C(O)C(O)(C(=O)C)C:1.0, O=C=O:2.0, [C]C(=O)C:1.0, [C]C(=O)O:1.0, [C]C([C])(O)C:1.0, [O]C(=O)C(=O)C:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]O>>[C]=O
2: [C]C(=O)C>>[C]C([C])(O)C
3: [C]C(=O)[CH2]>>[C]C(=O)C
4: [C]O[CH2]>>[C]O
5: [C]C[O]>>[C]C
6: [C]=O>>[C]O
7: [C]C(=O)O>>O=C=O

MMP Level 2
1: [C]C(=O)O>>O=C=O
2: [O]C(=O)C(=O)C>>O=C(O)C(O)(C(=O)C)C
3: [O]CC(=O)C(=O)O>>[C]C(O)(C(=O)C)C
4: [C]C(=O)OC[C]>>[C]C(=O)O
5: [C]OCC([C])=O>>[C]C(=O)C
6: [C]C(=O)C>>[C]C([C])(O)C
7: O=C(O)C(=O)[CH2]>>O=C=O

MMP Level 3
1: O=C(O)C(=O)[CH2]>>O=C=O
2: O=C(O[CH2])C(=O)C>>O=C(O)C(O)(C(=O)C)C
3: [C]OCC(=O)C(=O)O>>O=C(O)C(O)(C(=O)C)C
4: [C]C(=O)COC(=O)C(=O)C>>[C]C(O)(C(=O)O)C
5: [C]C(=O)OCC(=O)C(=O)O>>[C]C(O)(C(=O)C)C
6: [O]C(=O)C(=O)C>>O=C(O)C(O)(C(=O)C)C
7: [O]CC(=O)C(=O)O>>O=C=O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)C(=O)COC(=O)C(=O)C>>O=C=O, O=C(O)C(=O)COC(=O)C(=O)C>>O=C=O]
2: R:M00001, P:M00003	[O=C(O)C(=O)COC(=O)C(=O)C>>O=C(O)C(O)(C(=O)C)C, O=C(O)C(=O)COC(=O)C(=O)C>>O=C(O)C(O)(C(=O)C)C, O=C(O)C(=O)COC(=O)C(=O)C>>O=C(O)C(O)(C(=O)C)C, O=C(O)C(=O)COC(=O)C(=O)C>>O=C(O)C(O)(C(=O)C)C, O=C(O)C(=O)COC(=O)C(=O)C>>O=C(O)C(O)(C(=O)C)C]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[C:4](=[O:5])[CH2:6][O:7][C:8](=[O:9])[C:10](=[O:11])[CH3:12]>>[O:1]=[C:2]=[O:3].[O:9]=[C:8]([OH:7])[C:10]([OH:11])([C:4](=[O:5])[CH3:6])[CH3:12]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=12, 2=10, 3=11, 4=8, 5=9, 6=7, 7=6, 8=4, 9=5, 10=2, 11=1, 12=3}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=11, 2=10, 3=12, 4=8, 5=6, 6=7, 7=4, 8=9, 9=2, 10=1, 11=3, 12=5}

