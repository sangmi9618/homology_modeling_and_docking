
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%C:1.0, C-C:8.0, C-O:5.0]

ORDER_CHANGED
[C-C*C=C:5.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1:1.0, O=P(O)(O)O[P]>>[O]C([CH])COP(=O)(O)O[P]:1.0, O=P(O)(O[P])OCC1[C][CH]1>>[O]C([CH])COP(=O)(O)O[P]:1.0, [CH2]:12.0, [CH2]C(=CC[CH2])C:3.0, [CH2]C(=CC[CH2])C>>[C]CCC=[C]:1.0, [CH2]C1([CH][CH]1)C:2.0, [CH2]C1([CH][CH]1)C>>[CH]=C([CH2])C:1.0, [CH2]C1([CH][CH]1)C>>[C]=C[CH2]:1.0, [CH2]C=C(C)C:2.0, [CH2]C=C(C)C>>[CH2]C=C(C)C:1.0, [CH2]C=C(C)C>>[CH2]CC=C(C)C:2.0, [CH2]C=C(C)C[CH2]:3.0, [CH2]C=C(C)C[CH2]>>[CH2]C(=CC[CH2])C:1.0, [CH2]C=C(C)C[CH2]>>[CH2]C=C(C)C:1.0, [CH2]CC=C(C)C:2.0, [CH2]CC=C(C)C>>[C]CCC=C(C)C:1.0, [CH2]CC=C(C)C>>[C]CCC=[C]:1.0, [CH3]:8.0, [CH]:8.0, [CH]=C(C)C:6.0, [CH]=C(C)C>>[CH2]C=C(C)C:1.0, [CH]=C(C)C>>[CH]=C(C)C:1.0, [CH]=C(C)C>>[C]=C[CH2]:1.0, [CH]=C([CH2])C:6.0, [CH]=C([CH2])C>>[CH]=C(C)C:1.0, [CH]=C([CH2])C>>[C]=C[CH2]:1.0, [CH]C1C([CH2])C1(C)C[CH2]:1.0, [CH]C1C([CH2])C1(C)C[CH2]>>[CH2]C(=CC[CH2])C:1.0, [CH]C1C([CH2])C1(C)C[CH2]>>[CH2]C=C(C)C[CH2]:1.0, [CH]C1[C][CH]1:1.0, [CH]C1[C][CH]1>>[CH]C[CH2]:1.0, [CH]CCC(=CC[CH2])C>>[C]CCC=C(C)C[CH2]:1.0, [CH]CCC(=[CH])C:1.0, [CH]CCC(=[CH])C>>[CH2]C=C(C)C[CH2]:1.0, [CH]CCC1([CH][CH]1)C:1.0, [CH]CCC1([CH][CH]1)C>>[CH2]C=C(C)C[CH2]:1.0, [CH]C[CH2]:4.0, [CH]N([CH])[CH]:1.0, [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]:1.0, [C]:6.0, [C]1[CH]C1[CH2]:1.0, [C]1[CH]C1[CH2]>>[CH]C[CH2]:1.0, [C]=CC1C(CO[P])C1(C)C[CH2]>>[C]=CCCC=C([CH2])C:1.0, [C]=CC1C(C[O])C1(C)CC[CH]>>[CH]CCC=C(C)C[CH2]:1.0, [C]=CC1C([CH2])C1([CH2])C:1.0, [C]=CC1C([CH2])C1([CH2])C>>[C]=CCC[CH]:1.0, [C]=CCCC(=C[CH2])C>>[CH]CCC(=CC[CH2])C:1.0, [C]=CCCC1(C)C([CH])C1[CH2]>>[CH]CCC(=CC[CH2])C:1.0, [C]=CCC[CH]:2.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[CH2]:5.0, [C]=C[CH2]>>[CH]C[CH2]:2.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]C:8.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]:1.0, [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O:1.0, [C]C=[CH]:1.0, [C]C>>[CH]=C(C)C:1.0, [C]C>>[C]C:3.0, [C]CCC=C(C)C>>[CH]=C(C)CCC=C(C)C:1.0, [C]CCC=C(C)C[CH2]>>[CH]=C(C)CCC=C([CH2])C:1.0, [C]CCC=[C]:2.0, [C]C[CH2]:2.0, [C]C[CH2]>>[CH]=C([CH2])C:2.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C=[CH]:1.0, [N+]:1.0, [N]:1.0, [OH]:3.0, [O]:5.0, [O]C([CH])COP(=O)(O)O[P]>>O=P(O)(O)O[P]:2.0, [O]C([CH])CO[P]:4.0, [O]C([CH])CO[P]>>[O]C([CH])CO[P]:2.0, [O]CC1C(C=C([CH2])C)C1(C)C[CH2]>>[C]=CCCC=C([CH2])C:1.0, [O]CC1C([CH])C1([CH2])C:1.0, [O]CC1C([CH])C1([CH2])C>>[C]=CCC[CH]:1.0, [O]C[CH]:5.0, [O]C[CH]>>[C]C:1.0, [O]C[CH]>>[O]C[CH]:2.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)OC[CH]:1.0, [O]P(=O)(O)OCC1C([CH])C1([CH2])C>>[CH2]C=C(C)C[CH2]:1.0, [O]P(=O)(O)OCC1O[CH][CH]C1O>>[O]P(=O)(O)OCC1O[CH][CH]C1O:2.0, [O]P(=O)(O)OC[CH]:5.0, [O]P(=O)(O)OC[CH]>>[O]P(=O)(O)O:2.0, [O]P(=O)(O)OC[CH]>>[O]P(=O)(O)OC[CH]:1.0, [P]O:3.0, [P]O>>[P]O[CH2]:1.0, [P]OCC1[C][CH]1:1.0, [P]OCC1[C][CH]1>>[CH]=C([CH2])C:1.0, [P]O[CH2]:5.0, [P]O[CH2]>>[P]O:2.0, [P]O[CH2]>>[P]O[CH2]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[CH2]:6.0, [CH3]:7.0, [CH]:2.0, [C]:8.0, [O]:5.0]


ID=Reaction Center at Level: 1 (8)
[[CH2]C1([CH][CH]1)C:2.0, [CH]=C(C)C:3.0, [CH]=C([CH2])C:3.0, [CH]C1[C][CH]1:1.0, [C]1[CH]C1[CH2]:1.0, [C]C:7.0, [O]C[CH]:6.0, [P]O[CH2]:5.0]


ID=Reaction Center at Level: 2 (11)
[[CH2]C1([CH][CH]1)C:1.0, [CH2]C=C(C)C:3.0, [CH2]C=C(C)C[CH2]:3.0, [CH]=C(C)C:3.0, [CH]=C([CH2])C:3.0, [CH]C1C([CH2])C1(C)C[CH2]:2.0, [C]=CC1C([CH2])C1([CH2])C:1.0, [O]C([CH])CO[P]:4.0, [O]CC1C([CH])C1([CH2])C:1.0, [O]P(=O)(O)OC[CH]:5.0, [P]OCC1[C][CH]1:2.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH2]:4.0, [CH3]:1.0, [CH]:5.0, [C]:6.0]


ID=Reaction Center at Level: 1 (7)
[[CH2]C1([CH][CH]1)C:1.0, [CH]=C(C)C:2.0, [CH]=C([CH2])C:3.0, [CH]C[CH2]:2.0, [C]=C[CH2]:5.0, [C]C:1.0, [C]C[CH2]:2.0]


ID=Reaction Center at Level: 2 (9)
[[CH2]C(=CC[CH2])C:3.0, [CH2]C=C(C)C:2.0, [CH2]C=C(C)C[CH2]:3.0, [CH2]CC=C(C)C:2.0, [CH]=C(C)C:1.0, [CH]C1C([CH2])C1(C)C[CH2]:1.0, [CH]CCC(=[CH])C:1.0, [CH]CCC1([CH][CH]1)C:1.0, [C]CCC=[C]:2.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH2]:2.0, [CH]:2.0]


ID=Reaction Center at Level: 1 (3)
[[CH]C1[C][CH]1:1.0, [CH]C[CH2]:2.0, [C]1[CH]C1[CH2]:1.0]


ID=Reaction Center at Level: 2 (3)
[[C]=CC1C([CH2])C1([CH2])C:1.0, [C]=CCC[CH]:2.0, [O]CC1C([CH])C1([CH2])C:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]=C[CH2]>>[CH]C[CH2]
2: [CH]=C([CH2])C>>[C]=C[CH2]
3: [C]C[CH]>>[C]C=[CH]
4: [P]O>>[P]O[CH2]
5: [C]C>>[C]C
6: [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]
7: [C]C>>[C]C
8: [O]C[CH]>>[O]C[CH]
9: [C]C[CH2]>>[CH]=C([CH2])C
10: [CH]C1[C][CH]1>>[CH]C[CH2]
11: [P]O[CH2]>>[P]O
12: [P]O[CH2]>>[P]O
13: [C]=C[CH2]>>[CH]C[CH2]
14: [O]C[CH]>>[O]C[CH]
15: [C]1[CH]C1[CH2]>>[CH]C[CH2]
16: [C]C>>[CH]=C(C)C
17: [P]O[CH2]>>[P]O[CH2]
18: [O]C[CH]>>[C]C
19: [C]C>>[C]C
20: [CH2]C1([CH][CH]1)C>>[C]=C[CH2]
21: [CH]=C(C)C>>[C]=C[CH2]
22: [C]C[CH2]>>[CH]=C([CH2])C

MMP Level 2
1: [CH2]CC=C(C)C>>[C]CCC=[C]
2: [CH2]C=C(C)C[CH2]>>[CH2]C(=CC[CH2])C
3: [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]
4: [O]P(=O)(O)O>>[O]P(=O)(O)OC[CH]
5: [CH2]C1([CH][CH]1)C>>[CH]=C([CH2])C
6: [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]
7: [CH]=C(C)C>>[CH]=C(C)C
8: [O]C([CH])CO[P]>>[O]C([CH])CO[P]
9: [CH]CCC1([CH][CH]1)C>>[CH2]C=C(C)C[CH2]
10: [C]=CC1C([CH2])C1([CH2])C>>[C]=CCC[CH]
11: [O]P(=O)(O)OC[CH]>>[O]P(=O)(O)O
12: [O]P(=O)(O)OC[CH]>>[O]P(=O)(O)O
13: [CH2]C(=CC[CH2])C>>[C]CCC=[C]
14: [O]C([CH])CO[P]>>[O]C([CH])CO[P]
15: [O]CC1C([CH])C1([CH2])C>>[C]=CCC[CH]
16: [CH]=C(C)C>>[CH2]C=C(C)C
17: [O]P(=O)(O)OC[CH]>>[O]P(=O)(O)OC[CH]
18: [P]OCC1[C][CH]1>>[CH]=C([CH2])C
19: [CH]=C([CH2])C>>[CH]=C(C)C
20: [CH]C1C([CH2])C1(C)C[CH2]>>[CH2]C(=CC[CH2])C
21: [CH2]C=C(C)C>>[CH2]CC=C(C)C
22: [CH]CCC(=[CH])C>>[CH2]C=C(C)C[CH2]

MMP Level 3
1: [C]CCC=C(C)C>>[CH]=C(C)CCC=C(C)C
2: [CH]CCC(=CC[CH2])C>>[C]CCC=C(C)C[CH2]
3: O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1
4: O=P(O)(O)O[P]>>[O]C([CH])COP(=O)(O)O[P]
5: [CH]C1C([CH2])C1(C)C[CH2]>>[CH2]C=C(C)C[CH2]
6: [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O
7: [CH2]C=C(C)C>>[CH2]C=C(C)C
8: [O]P(=O)(O)OCC1O[CH][CH]C1O>>[O]P(=O)(O)OCC1O[CH][CH]C1O
9: [C]=CCCC1(C)C([CH])C1[CH2]>>[CH]CCC(=CC[CH2])C
10: [O]CC1C(C=C([CH2])C)C1(C)C[CH2]>>[C]=CCCC=C([CH2])C
11: [O]C([CH])COP(=O)(O)O[P]>>O=P(O)(O)O[P]
12: [O]C([CH])COP(=O)(O)O[P]>>O=P(O)(O)O[P]
13: [C]CCC=C(C)C[CH2]>>[CH]=C(C)CCC=C([CH2])C
14: [O]P(=O)(O)OCC1O[CH][CH]C1O>>[O]P(=O)(O)OCC1O[CH][CH]C1O
15: [C]=CC1C(CO[P])C1(C)C[CH2]>>[C]=CCCC=C([CH2])C
16: [CH2]C=C(C)C>>[CH2]CC=C(C)C
17: O=P(O)(O[P])OCC1[C][CH]1>>[O]C([CH])COP(=O)(O)O[P]
18: [O]P(=O)(O)OCC1C([CH])C1([CH2])C>>[CH2]C=C(C)C[CH2]
19: [CH2]C=C(C)C[CH2]>>[CH2]C=C(C)C
20: [C]=CC1C(C[O])C1(C)CC[CH]>>[CH]CCC=C(C)C[CH2]
21: [CH2]CC=C(C)C>>[C]CCC=C(C)C
22: [C]=CCCC(=C[CH2])C>>[CH]CCC(=CC[CH2])C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OCC1C(C=C(C)CCC=C(C)CCC=C(C)C)C1(C)CCC=C(C)CCC=C(C)C>>C(=C(C)C)CCC(=CCCC(=CCCC=C(C)CCC=C(C)CCC=C(C)C)C)C, O=P(O)(O)OP(=O)(O)OCC1C(C=C(C)CCC=C(C)CCC=C(C)C)C1(C)CCC=C(C)CCC=C(C)C>>C(=C(C)C)CCC(=CCCC(=CCCC=C(C)CCC=C(C)CCC=C(C)C)C)C, O=P(O)(O)OP(=O)(O)OCC1C(C=C(C)CCC=C(C)CCC=C(C)C)C1(C)CCC=C(C)CCC=C(C)C>>C(=C(C)C)CCC(=CCCC(=CCCC=C(C)CCC=C(C)CCC=C(C)C)C)C, O=P(O)(O)OP(=O)(O)OCC1C(C=C(C)CCC=C(C)CCC=C(C)C)C1(C)CCC=C(C)CCC=C(C)C>>C(=C(C)C)CCC(=CCCC(=CCCC=C(C)CCC=C(C)CCC=C(C)C)C)C, O=P(O)(O)OP(=O)(O)OCC1C(C=C(C)CCC=C(C)CCC=C(C)C)C1(C)CCC=C(C)CCC=C(C)C>>C(=C(C)C)CCC(=CCCC(=CCCC=C(C)CCC=C(C)CCC=C(C)C)C)C, O=P(O)(O)OP(=O)(O)OCC1C(C=C(C)CCC=C(C)CCC=C(C)C)C1(C)CCC=C(C)CCC=C(C)C>>C(=C(C)C)CCC(=CCCC(=CCCC=C(C)CCC=C(C)CCC=C(C)C)C)C, O=P(O)(O)OP(=O)(O)OCC1C(C=C(C)CCC=C(C)CCC=C(C)C)C1(C)CCC=C(C)CCC=C(C)C>>C(=C(C)C)CCC(=CCCC(=CCCC=C(C)CCC=C(C)CCC=C(C)C)C)C, O=P(O)(O)OP(=O)(O)OCC1C(C=C(C)CCC=C(C)CCC=C(C)C)C1(C)CCC=C(C)CCC=C(C)C>>C(=C(C)C)CCC(=CCCC(=CCCC=C(C)CCC=C(C)CCC=C(C)C)C)C, O=P(O)(O)OP(=O)(O)OCC1C(C=C(C)CCC=C(C)CCC=C(C)C)C1(C)CCC=C(C)CCC=C(C)C>>C(=C(C)C)CCC(=CCCC(=CCCC=C(C)CCC=C(C)CCC=C(C)C)C)C, O=P(O)(O)OP(=O)(O)OCC1C(C=C(C)CCC=C(C)CCC=C(C)C)C1(C)CCC=C(C)CCC=C(C)C>>C(=C(C)C)CCC(=CCCC(=CCCC=C(C)CCC=C(C)CCC=C(C)C)C)C, O=P(O)(O)OP(=O)(O)OCC1C(C=C(C)CCC=C(C)CCC=C(C)C)C1(C)CCC=C(C)CCC=C(C)C>>C(=C(C)C)CCC(=CCCC(=CCCC=C(C)CCC=C(C)CCC=C(C)C)C)C, O=P(O)(O)OP(=O)(O)OCC1C(C=C(C)CCC=C(C)CCC=C(C)C)C1(C)CCC=C(C)CCC=C(C)C>>C(=C(C)C)CCC(=CCCC(=CCCC=C(C)CCC=C(C)CCC=C(C)C)C)C, O=P(O)(O)OP(=O)(O)OCC1C(C=C(C)CCC=C(C)CCC=C(C)C)C1(C)CCC=C(C)CCC=C(C)C>>C(=C(C)C)CCC(=CCCC(=CCCC=C(C)CCC=C(C)CCC=C(C)C)C)C, O=P(O)(O)OP(=O)(O)OCC1C(C=C(C)CCC=C(C)CCC=C(C)C)C1(C)CCC=C(C)CCC=C(C)C>>C(=C(C)C)CCC(=CCCC(=CCCC=C(C)CCC=C(C)CCC=C(C)C)C)C]
2: R:M00001, P:M00006	[O=P(O)(O)OP(=O)(O)OCC1C(C=C(C)CCC=C(C)CCC=C(C)C)C1(C)CCC=C(C)CCC=C(C)C>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O, O=P(O)(O)OP(=O)(O)OCC1C(C=C(C)CCC=C(C)CCC=C(C)C)C1(C)CCC=C(C)CCC=C(C)C>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O]
3: R:M00002, P:M00005	[O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=P(O)(O)OP(=O)(O)O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=P(O)(O)OP(=O)(O)O]
4: R:M00002, P:M00006	[O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O]


//
SELECTED AAM MAPPING
[H+:88].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:5][N:6]([CH:7]=[CH:8][CH2:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48].[O:49]=[P:50]([OH:51])([OH:52])[O:53][P:54](=[O:55])([OH:56])[O:57][CH2:58][CH:59]1[CH:60]([CH:61]=[C:62]([CH3:63])[CH2:64][CH2:65][CH:66]=[C:67]([CH3:68])[CH2:69][CH2:70][CH:71]=[C:72]([CH3:73])[CH3:74])[C:75]1([CH3:76])[CH2:77][CH2:78][CH:79]=[C:80]([CH3:81])[CH2:82][CH2:83][CH:84]=[C:85]([CH3:86])[CH3:87]>>[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:9]:[CH:8]:[CH:7]:[N+:6](:[CH:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:51][P:50](=[O:49])([OH:52])[O:53][P:54](=[O:55])([OH:56])[O:57][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:42])[OH:41])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48].[O:16]=[P:15]([OH:14])([OH:17])[O:18][P:19](=[O:20])([OH:21])[OH:22].[CH:85](=[C:87]([CH3:81])[CH3:86])[CH2:84][CH2:83][C:82](=[CH:80][CH2:79][CH2:78][C:77](=[CH:75][CH2:59][CH2:60][CH:61]=[C:62]([CH3:63])[CH2:64][CH2:65][CH:66]=[C:67]([CH3:68])[CH2:69][CH2:70][CH:71]=[C:72]([CH3:73])[CH3:74])[CH3:58])[CH3:76]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=73, 2=72, 3=71, 4=70, 5=69, 6=67, 7=66, 8=65, 9=64, 10=62, 11=61, 12=60, 13=59, 14=75, 15=76, 16=77, 17=78, 18=79, 19=80, 20=81, 21=82, 22=83, 23=84, 24=85, 25=86, 26=87, 27=58, 28=57, 29=54, 30=55, 31=56, 32=53, 33=50, 34=49, 35=51, 36=52, 37=63, 38=68, 39=74, 40=9, 41=8, 42=7, 43=6, 44=5, 45=4, 46=2, 47=1, 48=3, 49=10, 50=47, 51=45, 52=12, 53=11, 54=13, 55=14, 56=15, 57=16, 58=17, 59=18, 60=19, 61=20, 62=21, 63=22, 64=23, 65=24, 66=43, 67=37, 68=26, 69=25, 70=27, 71=28, 72=29, 73=30, 74=35, 75=34, 76=33, 77=32, 78=31, 79=36, 80=38, 81=39, 82=40, 83=41, 84=42, 85=44, 86=46, 87=48, 88=88}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=51, 2=50, 3=49, 4=53, 5=54, 6=55, 7=56, 8=57, 9=58, 10=59, 11=60, 12=61, 13=62, 14=63, 15=64, 16=66, 17=67, 18=68, 19=69, 20=71, 21=72, 22=73, 23=74, 24=75, 25=76, 26=70, 27=65, 28=77, 29=78, 30=52, 31=81, 32=80, 33=79, 34=82, 35=83, 36=84, 37=85, 38=86, 39=87, 40=6, 41=5, 42=4, 43=9, 44=8, 45=7, 46=10, 47=47, 48=45, 49=12, 50=11, 51=13, 52=14, 53=15, 54=16, 55=17, 56=18, 57=19, 58=20, 59=21, 60=22, 61=23, 62=24, 63=43, 64=37, 65=26, 66=25, 67=27, 68=28, 69=29, 70=30, 71=35, 72=34, 73=33, 74=32, 75=31, 76=36, 77=38, 78=39, 79=40, 80=41, 81=42, 82=44, 83=46, 84=48, 85=2, 86=1, 87=3}

