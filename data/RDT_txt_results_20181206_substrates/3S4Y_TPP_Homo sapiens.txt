
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]:1.0, O=P(O)(O[P])O[CH2]:1.0, O=P(O)(O[P])O[P]:1.0, O=P(O)(O[P])O[P]>>O=P(O)(O[P])O[CH2]:1.0, OC[CH2]:1.0, OC[CH2]>>[O]P(=O)(O)OC[CH2]:1.0, O[CH2]:1.0, O[CH2]>>[P]O[CH2]:1.0, [C]CCO>>[C]CCOP(=O)(O)O[P]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OC[CH2]:1.0, [O]P(=O)(O)OP(=O)(O)OP(=O)(O)O>>O=P(O)(O)OP(=O)(O)OC[CH2]:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:2.0, [O]P([O])(=O)O>>[O]P([O])(=O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[O]P([O])(=O)O:2.0, [P]O[CH2]:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O[P])O[CH2]:1.0, O=P(O)(O[P])O[P]:1.0, [O]P(=O)(O)OC[CH2]:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]P([O])(=O)O>>[O]P([O])(=O)O
2: [P]O[P]>>[P]O
3: O[CH2]>>[P]O[CH2]

MMP Level 2
1: O=P(O)(O[P])O[P]>>O=P(O)(O[P])O[CH2]
2: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
3: OC[CH2]>>[O]P(=O)(O)OC[CH2]

MMP Level 3
1: [O]P(=O)(O)OP(=O)(O)OP(=O)(O)O>>O=P(O)(O)OP(=O)(O)OC[CH2]
2: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]
3: [C]CCO>>[C]CCOP(=O)(O)O[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCCc1sc[n+](c1C)Cc2cnc(nc2N)C]
3: R:M00002, P:M00004	[OCCc1sc[n+](c1C)Cc2cnc(nc2N)C>>O=P(O)(O)OP(=O)(O)OCCc1sc[n+](c1C)Cc2cnc(nc2N)C]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31].[OH:32][CH2:33][CH2:34][C:35]:1:[S:36]:[CH:37]:[N+:38](:[C:39]1[CH3:40])[CH2:41][C:42]:2:[CH:43]:[N:44]:[C:45](:[N:46]:[C:47]2[NH2:48])[CH3:49]>>[O:11]=[P:10]([OH:9])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:32][CH2:33][CH2:34][C:35]:1:[S:36]:[CH:37]:[N+:38](:[C:39]1[CH3:40])[CH2:41][C:42]:2:[CH:43]:[N:44]:[C:45](:[N:46]:[C:47]2[NH2:48])[CH3:49]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=40, 33=39, 34=35, 35=36, 36=37, 37=38, 38=41, 39=42, 40=43, 41=44, 42=45, 43=46, 44=47, 45=48, 46=49, 47=34, 48=33, 49=32}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=42, 2=43, 3=44, 4=39, 5=40, 6=41, 7=45, 8=38, 9=37, 10=36, 11=35, 12=46, 13=48, 14=33, 15=34, 16=32, 17=31, 18=28, 19=27, 20=29, 21=30, 22=49, 23=47, 24=17, 25=16, 26=12, 27=13, 28=14, 29=15, 30=18, 31=19, 32=20, 33=21, 34=22, 35=23, 36=24, 37=25, 38=26, 39=11, 40=10, 41=9, 42=6, 43=7, 44=8, 45=5, 46=2, 47=1, 48=3, 49=4}

