
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(N[CH])C([CH2])N:1.0, O=C(N[CH])C([CH2])N>>O=C(O)C([CH2])N:1.0, O=C(O)C([CH2])N:1.0, O=C([CH])O:2.0, O=C([CH])[NH]:1.0, O=C([CH])[NH]>>O=C([CH])O:1.0, O=C([NH])C(NC(=O)C([CH2])N)C(C)C>>O=C([NH])C(N)C(C)C:1.0, O>>O=C(O)C([CH2])N:1.0, O>>O=C([CH])O:1.0, O>>[C]O:1.0, [CH]N:1.0, [C]:2.0, [C]C([CH])N:1.0, [C]C([CH])NC(=O)C(N)C[CH2]>>O=C(O)C(N)C[CH2]:1.0, [C]C([CH])NC(=O)[CH]:1.0, [C]C([CH])NC(=O)[CH]>>[C]C([CH])N:1.0, [C]N[CH]:1.0, [C]N[CH]>>[CH]N:1.0, [C]O:1.0, [NH2]:1.0, [NH]:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [C]:2.0, [NH]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=C([CH])O:1.0, O=C([CH])[NH]:1.0, [C]N[CH]:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=C(N[CH])C([CH2])N:1.0, O=C(O)C([CH2])N:1.0, O=C([CH])O:1.0, [C]C([CH])NC(=O)[CH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O>>[C]O
2: O=C([CH])[NH]>>O=C([CH])O
3: [C]N[CH]>>[CH]N

MMP Level 2
1: O>>O=C([CH])O
2: O=C(N[CH])C([CH2])N>>O=C(O)C([CH2])N
3: [C]C([CH])NC(=O)[CH]>>[C]C([CH])N

MMP Level 3
1: O>>O=C(O)C([CH2])N
2: [C]C([CH])NC(=O)C(N)C[CH2]>>O=C(O)C(N)C[CH2]
3: O=C([NH])C(NC(=O)C([CH2])N)C(C)C>>O=C([NH])C(N)C(C)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)CCC(NC(=O)C(NC(=O)C(N)CCSC)C(C)C)C(=O)NC(C(=O)NC(C(=O)NC(C(=O)NC(C(=O)O)Cc1ccc(O)cc1)C(C)CC)CCC(=O)O)C(C)CC>>O=C(O)C(N)CCSC]
2: R:M00001, P:M00004	[O=C(O)CCC(NC(=O)C(NC(=O)C(N)CCSC)C(C)C)C(=O)NC(C(=O)NC(C(=O)NC(C(=O)NC(C(=O)O)Cc1ccc(O)cc1)C(C)CC)CCC(=O)O)C(C)CC>>O=C(O)CCC(NC(=O)C(N)C(C)C)C(=O)NC(C(=O)NC(C(=O)NC(C(=O)NC(C(=O)O)Cc1ccc(O)cc1)C(C)CC)CCC(=O)O)C(C)CC]
3: R:M00002, P:M00003	[O>>O=C(O)C(N)CCSC]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH2:4][CH2:5][CH:6]([NH:7][C:8](=[O:9])[CH:10]([NH:11][C:12](=[O:13])[CH:14]([NH2:15])[CH2:16][CH2:17][S:18][CH3:19])[CH:20]([CH3:21])[CH3:22])[C:23](=[O:24])[NH:25][CH:26]([C:27](=[O:28])[NH:29][CH:30]([C:31](=[O:32])[NH:33][CH:34]([C:35](=[O:36])[NH:37][CH:38]([C:39](=[O:40])[OH:41])[CH2:42][C:43]:1:[CH:44]:[CH:45]:[C:46]([OH:47]):[CH:48]:[CH:49]1)[CH:50]([CH3:51])[CH2:52][CH3:53])[CH2:54][CH2:55][C:56](=[O:57])[OH:58])[CH:59]([CH3:60])[CH2:61][CH3:62].[OH2:63]>>[O:1]=[C:2]([OH:3])[CH2:4][CH2:5][CH:6]([NH:7][C:8](=[O:9])[CH:10]([NH2:11])[CH:20]([CH3:21])[CH3:22])[C:23](=[O:24])[NH:25][CH:26]([C:27](=[O:28])[NH:29][CH:30]([C:31](=[O:32])[NH:33][CH:34]([C:35](=[O:36])[NH:37][CH:38]([C:39](=[O:40])[OH:41])[CH2:42][C:43]:1:[CH:44]:[CH:45]:[C:46]([OH:47]):[CH:48]:[CH:49]1)[CH:50]([CH3:51])[CH2:52][CH3:53])[CH2:54][CH2:55][C:56](=[O:57])[OH:58])[CH:59]([CH3:60])[CH2:61][CH3:62].[O:13]=[C:12]([OH:63])[CH:14]([NH2:15])[CH2:16][CH2:17][S:18][CH3:19]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=62, 2=61, 3=59, 4=60, 5=26, 6=27, 7=28, 8=29, 9=30, 10=54, 11=55, 12=56, 13=57, 14=58, 15=31, 16=32, 17=33, 18=34, 19=50, 20=51, 21=52, 22=53, 23=35, 24=36, 25=37, 26=38, 27=42, 28=43, 29=49, 30=48, 31=46, 32=45, 33=44, 34=47, 35=39, 36=40, 37=41, 38=25, 39=23, 40=24, 41=6, 42=5, 43=4, 44=2, 45=1, 46=3, 47=7, 48=8, 49=9, 50=10, 51=20, 52=21, 53=22, 54=11, 55=12, 56=13, 57=14, 58=16, 59=17, 60=18, 61=19, 62=15, 63=63}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=63, 2=62, 3=61, 4=60, 5=58, 6=56, 7=55, 8=57, 9=59, 10=54, 11=53, 12=51, 13=52, 14=18, 15=19, 16=20, 17=21, 18=22, 19=46, 20=47, 21=48, 22=49, 23=50, 24=23, 25=24, 26=25, 27=26, 28=42, 29=43, 30=44, 31=45, 32=27, 33=28, 34=29, 35=30, 36=34, 37=35, 38=36, 39=37, 40=38, 41=40, 42=41, 43=39, 44=31, 45=32, 46=33, 47=17, 48=15, 49=16, 50=6, 51=5, 52=4, 53=2, 54=1, 55=3, 56=7, 57=8, 58=9, 59=10, 60=12, 61=13, 62=14, 63=11}

