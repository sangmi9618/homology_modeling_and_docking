
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)C(N)CO>>O=C(O)C(N)CC1=CN[C]C1=[CH]:1.0, O[CH2]:1.0, O[CH2]>>O:1.0, [CH2]:2.0, [CH]:1.0, [CH]=C1[C]NC=C1:1.0, [CH]=C1[C]NC=C1>>[CH]CC1=CN[C]C1=[CH]:1.0, [CH]C=c1cc[nH]c1=[CH]>>[C]C(N)Cc1c[nH]c(=[CH])c1=C[CH]:1.0, [CH]CC1=CN[C]C1=[CH]:1.0, [CH]CO:2.0, [CH]CO>>O:1.0, [CH]CO>>[C]C[CH]:1.0, [C]:1.0, [C]C(=[CH])CC([C])N:1.0, [C]C(=[CH])[CH2]:1.0, [C]C(N)CO:1.0, [C]C(N)CO>>O:1.0, [C]C(N)CO>>[C]C(=[CH])CC([C])N:1.0, [C]C=[CH]:1.0, [C]C=[CH]>>[C]C(=[CH])[CH2]:1.0, [C]C[CH]:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [CH2]:2.0, [C]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O[CH2]:1.0, [CH]CO:1.0, [C]C(=[CH])[CH2]:1.0, [C]C[CH]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, [CH]CC1=CN[C]C1=[CH]:1.0, [CH]CO:1.0, [C]C(=[CH])CC([C])N:1.0, [C]C(N)CO:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O[CH2]>>O
2: [C]C=[CH]>>[C]C(=[CH])[CH2]
3: [CH]CO>>[C]C[CH]

MMP Level 2
1: [CH]CO>>O
2: [CH]=C1[C]NC=C1>>[CH]CC1=CN[C]C1=[CH]
3: [C]C(N)CO>>[C]C(=[CH])CC([C])N

MMP Level 3
1: [C]C(N)CO>>O
2: [CH]C=c1cc[nH]c1=[CH]>>[C]C(N)Cc1c[nH]c(=[CH])c1=C[CH]
3: O=C(O)C(N)CO>>O=C(O)C(N)CC1=CN[C]C1=[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)CO>>O=C(O)C(N)Cc1c[nH]c2ccccc21]
2: R:M00001, P:M00004	[O=C(O)C(N)CO>>O]
3: R:M00002, P:M00003	[c1ccc2[nH]ccc2c1>>O=C(O)C(N)Cc1c[nH]c2ccccc21]


//
SELECTED AAM MAPPING
[O:10]=[C:11]([OH:12])[CH:13]([NH2:14])[CH2:15][OH:16].[CH:1]:1:[CH:2]:[CH:3]:[C:4]:2:[NH:5]:[CH:6]:[CH:7]:[C:8]2:[CH:9]1>>[O:10]=[C:11]([OH:12])[CH:13]([NH2:14])[CH2:15][C:7]:1:[CH:6]:[NH:5]:[C:4]:2:[CH:3]:[CH:2]:[CH:1]:[CH:9]:[C:8]21.[OH2:16]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=15, 2=13, 3=11, 4=10, 5=12, 6=14, 7=16, 8=1, 9=2, 10=3, 11=4, 12=8, 13=9, 14=7, 15=6, 16=5}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=13, 2=12, 3=11, 4=10, 5=15, 6=14, 7=7, 8=8, 9=9, 10=6, 11=4, 12=2, 13=1, 14=3, 15=5, 16=16}

