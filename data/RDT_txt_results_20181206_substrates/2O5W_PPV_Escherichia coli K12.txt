
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O:3.0, O=P(O)(O)O[P]:1.0, O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]:1.0, O=P(O)(O[P])O[P]:1.0, O=P(O)(O[P])O[P]>>O=P(O)(O)O[P]:1.0, O>>O=P(O)(O)O[P]:1.0, O>>[O]P(=O)(O)O:1.0, O>>[P]O:1.0, [OH]:2.0, [O]:1.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)OP(=O)(O)OP(=O)(O)O>>O=P(O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [O]P([O])(=O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, [O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[P]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[P]>>[P]O
2: [O]P([O])(=O)O>>[O]P(=O)(O)O
3: O>>[P]O

MMP Level 2
1: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
2: O=P(O)(O[P])O[P]>>O=P(O)(O)O[P]
3: O>>[O]P(=O)(O)O

MMP Level 3
1: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]
2: [O]P(=O)(O)OP(=O)(O)OP(=O)(O)O>>O=P(O)(O)OP(=O)(O)O
3: O>>O=P(O)(O)O[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1nc(N)[nH]c2NCC(=Nc12)C(O)C(O)COP(=O)(O)OP(=O)(O)OP(=O)(O)O>>O=c1nc(N)[nH]c2NCC(=Nc12)C(O)C(O)COP(=O)(O)O]
2: R:M00001, P:M00004	[O=c1nc(N)[nH]c2NCC(=Nc12)C(O)C(O)COP(=O)(O)OP(=O)(O)OP(=O)(O)O>>O=P(O)(O)OP(=O)(O)O]
3: R:M00002, P:M00004	[O>>O=P(O)(O)OP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]=2[NH:8][CH2:9][C:10](=[N:11][C:12]12)[CH:13]([OH:14])[CH:15]([OH:16])[CH2:17][O:18][P:19](=[O:20])([OH:21])[O:22][P:23](=[O:24])([OH:25])[O:26][P:27](=[O:28])([OH:29])[OH:30].[OH2:31]>>[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]=2[NH:8][CH2:9][C:10](=[N:11][C:12]12)[CH:13]([OH:14])[CH:15]([OH:16])[CH2:17][O:18][P:19](=[O:20])([OH:21])[OH:22].[O:24]=[P:23]([OH:31])([OH:25])[O:26][P:27](=[O:28])([OH:30])[OH:29]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=10, 3=11, 4=12, 5=7, 6=8, 7=6, 8=4, 9=3, 10=2, 11=1, 12=5, 13=13, 14=15, 15=17, 16=18, 17=19, 18=20, 19=21, 20=22, 21=23, 22=24, 23=25, 24=26, 25=27, 26=28, 27=29, 28=30, 29=16, 30=14, 31=31}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=9, 2=10, 3=11, 4=12, 5=7, 6=8, 7=6, 8=4, 9=3, 10=2, 11=1, 12=5, 13=13, 14=15, 15=17, 16=18, 17=19, 18=20, 19=21, 20=22, 21=16, 22=14, 23=25, 24=24, 25=23, 26=26, 27=27, 28=28, 29=29, 30=30, 31=31}

