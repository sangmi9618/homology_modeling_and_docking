
//
FINGERPRINTS BC

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1:1.0, O=C(O)C(=O)C:1.0, O=C(O)C(=O)C>>O=C(O)C(O)C:3.0, O=C(O)C(O)C:1.0, [CH2]:1.0, [CH]:2.0, [CH]N([CH])[CH]:1.0, [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]:1.0, [CH]O:1.0, [CH][N+]([CH])=[CH]:1.0, [C]:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=O:1.0, [C]=O>>[CH]O:1.0, [C]C(=O)C:2.0, [C]C(=O)C>>[C]C(O)C:2.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]:1.0, [C]C(O)C:2.0, [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O:1.0, [C]C=[CH]:1.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C=[CH]:1.0, [N+]:1.0, [N]:1.0, [OH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH]:1.0, [C]:1.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[CH]O:1.0, [C]=O:1.0, [C]C(=O)C:1.0, [C]C(O)C:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(O)C(=O)C:1.0, O=C(O)C(O)C:1.0, [C]C(=O)C:1.0, [C]C(O)C:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(=O)C:1.0, [C]C(O)C:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C(=O)C:1.0, O=C(O)C(O)C:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C[CH]>>[C]C=[CH]
2: [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]
3: [C]C(=O)C>>[C]C(O)C
4: [C]=O>>[CH]O

MMP Level 2
1: [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]
2: [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]
3: O=C(O)C(=O)C>>O=C(O)C(O)C
4: [C]C(=O)C>>[C]C(O)C

MMP Level 3
1: O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1
2: [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O
3: O=C(O)C(=O)C>>O=C(O)C(O)C
4: O=C(O)C(=O)C>>O=C(O)C(O)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C(O)C(=O)C>>O=C(O)C(O)C, O=C(O)C(=O)C>>O=C(O)C(O)C]
2: R:M00002, P:M00005	[O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O]


//
SELECTED AAM MAPPING
[H+:51].[O:45]=[C:46]([OH:47])[C:48](=[O:49])[CH3:50].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:5][N:6]([CH:7]=[CH:8][CH2:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44]>>[O:45]=[C:46]([OH:47])[CH:48]([OH:49])[CH3:50].[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:9]:[CH:8]:[CH:7]:[N+:6](:[CH:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=50, 2=48, 3=49, 4=46, 5=45, 6=47, 7=9, 8=8, 9=7, 10=6, 11=5, 12=4, 13=2, 14=1, 15=3, 16=10, 17=43, 18=41, 19=12, 20=11, 21=13, 22=14, 23=15, 24=16, 25=17, 26=18, 27=19, 28=20, 29=21, 30=22, 31=23, 32=24, 33=39, 34=37, 35=26, 36=25, 37=27, 38=28, 39=29, 40=30, 41=35, 42=34, 43=33, 44=32, 45=31, 46=36, 47=38, 48=40, 49=42, 50=44, 51=51}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=50, 2=48, 3=46, 4=45, 5=47, 6=49, 7=6, 8=5, 9=4, 10=9, 11=8, 12=7, 13=10, 14=43, 15=41, 16=12, 17=11, 18=13, 19=14, 20=15, 21=16, 22=17, 23=18, 24=19, 25=20, 26=21, 27=22, 28=23, 29=24, 30=39, 31=37, 32=26, 33=25, 34=27, 35=28, 36=29, 37=30, 38=35, 39=34, 40=33, 41=32, 42=31, 43=36, 44=38, 45=40, 46=42, 47=44, 48=2, 49=1, 50=3}

