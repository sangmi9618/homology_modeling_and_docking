
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:2.0, C=O:2.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(=O)C[CH2]>>O=C(O)C(N)C[CH2]:1.0, O=C(O)C(=O)C[CH2]>>[C]CC(=O)C(=O)O:1.0, O=C(O)C(N)C[CH2]:1.0, O=C(O)CC(N)C(=O)O>>O=C(O)C(=O)CC(=O)O:1.0, [CH]:2.0, [CH]N:2.0, [CH]N>>[CH]N:1.0, [C]:2.0, [C]=O:2.0, [C]=O>>[C]=O:1.0, [C]C(=O)[CH2]:4.0, [C]C(=O)[CH2]>>[C]C(=O)[CH2]:1.0, [C]C(=O)[CH2]>>[C]C([CH2])N:1.0, [C]C([CH2])N:4.0, [C]C([CH2])N>>[C]C(=O)[CH2]:1.0, [C]C([CH2])N>>[C]C([CH2])N:1.0, [C]CC(=O)C(=O)O:1.0, [C]CC(N)C(=O)O:1.0, [C]CC(N)C(=O)O>>O=C(O)C(N)C[CH2]:1.0, [C]CC(N)C(=O)O>>[C]CC(=O)C(=O)O:1.0, [C]CCC(=O)C(=O)O>>[C]CCC(N)C(=O)O:1.0, [NH2]:2.0, [O]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[CH]:2.0, [C]:2.0, [NH2]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (4)
[[CH]N:2.0, [C]=O:2.0, [C]C(=O)[CH2]:2.0, [C]C([CH2])N:2.0]


ID=Reaction Center at Level: 2 (6)
[O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(N)C[CH2]:1.0, [C]C(=O)[CH2]:2.0, [C]C([CH2])N:2.0, [C]CC(=O)C(=O)O:1.0, [C]CC(N)C(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:2.0, [C]:2.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(=O)[CH2]:2.0, [C]C([CH2])N:2.0]


ID=Reaction Center at Level: 2 (4)
[O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(N)C[CH2]:1.0, [C]CC(=O)C(=O)O:1.0, [C]CC(N)C(=O)O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C([CH2])N>>[C]C(=O)[CH2]
2: [C]=O>>[C]=O
3: [CH]N>>[CH]N
4: [C]C(=O)[CH2]>>[C]C([CH2])N

MMP Level 2
1: [C]CC(N)C(=O)O>>[C]CC(=O)C(=O)O
2: [C]C(=O)[CH2]>>[C]C(=O)[CH2]
3: [C]C([CH2])N>>[C]C([CH2])N
4: O=C(O)C(=O)C[CH2]>>O=C(O)C(N)C[CH2]

MMP Level 3
1: O=C(O)CC(N)C(=O)O>>O=C(O)C(=O)CC(=O)O
2: O=C(O)C(=O)C[CH2]>>[C]CC(=O)C(=O)O
3: [C]CC(N)C(=O)O>>O=C(O)C(N)C[CH2]
4: [C]CCC(=O)C(=O)O>>[C]CCC(N)C(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)CC(N)C(=O)O>>O=C(O)C(=O)CC(=O)O]
2: R:M00001, P:M00004	[O=C(O)CC(N)C(=O)O>>O=C(O)CCC(N)C(=O)O]
3: R:M00002, P:M00003	[O=C(O)C(=O)CCC(=O)O>>O=C(O)C(=O)CC(=O)O]
4: R:M00002, P:M00004	[O=C(O)C(=O)CCC(=O)O>>O=C(O)CCC(N)C(=O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[C:4](=[O:5])[CH2:6][CH2:7][C:8](=[O:9])[OH:10].[O:11]=[C:12]([OH:13])[CH2:14][CH:15]([NH2:16])[C:17](=[O:18])[OH:19]>>[O:18]=[C:17]([OH:19])[C:15](=[O:5])[CH2:14][C:12](=[O:11])[OH:13].[O:9]=[C:8]([OH:10])[CH2:7][CH2:6][CH:4]([NH2:16])[C:2](=[O:1])[OH:3]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=14, 2=15, 3=17, 4=18, 5=19, 6=16, 7=12, 8=11, 9=13, 10=6, 11=7, 12=8, 13=9, 14=10, 15=4, 16=5, 17=2, 18=1, 19=3}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=16, 2=14, 3=15, 4=12, 5=11, 6=13, 7=17, 8=18, 9=19, 10=5, 11=4, 12=2, 13=1, 14=3, 15=6, 16=8, 17=9, 18=10, 19=7}

