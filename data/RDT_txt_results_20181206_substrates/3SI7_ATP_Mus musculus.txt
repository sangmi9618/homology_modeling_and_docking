
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O:3.0, O=P(O)(O)O[CH2]>>O=P(O)(O)OP(=O)(O)O[CH2]:1.0, O>>O=P(O)(O)O[P]:1.0, O>>[O]P(=O)(O)O:1.0, O>>[P]O:1.0, [OH]:2.0, [O]:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [P]O:2.0, [P]O>>[P]O[P]:1.0, [P]O[P]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (4)
[O:1.0, [O]P(=O)(O)O:2.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=P(O)(O)O[P]:2.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O>>[P]O
2: [P]O>>[P]O[P]

MMP Level 2
1: O>>[O]P(=O)(O)O
2: [O]P(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O

MMP Level 3
1: O>>O=P(O)(O)O[P]
2: O=P(O)(O)O[CH2]>>O=P(O)(O)OP(=O)(O)O[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00002, P:M00003	[O=P(O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH2:6][CH:7]1[O:8][CH:9]([N:10]:2:[CH:11]:[N:12]:[C:13]:3:[C:14](:[N:15]:[CH:16]:[N:17]:[C:18]32)[NH2:19])[CH:20]([OH:21])[CH:22]1[OH:23].[OH2:24]>>[O:28]=[P:29]([OH:30])([OH:31])[OH:32].[O:25]=[P:26]([OH:24])([OH:27])[O:3][P:2](=[O:1])([OH:4])[O:5][CH2:6][CH:7]1[O:8][CH:9]([N:10]:2:[CH:11]:[N:12]:[C:13]:3:[C:14](:[N:15]:[CH:16]:[N:17]:[C:18]32)[NH2:19])[CH:20]([OH:21])[CH:22]1[OH:23]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=16, 3=17, 4=18, 5=13, 6=14, 7=15, 8=19, 9=12, 10=11, 11=10, 12=9, 13=20, 14=22, 15=7, 16=8, 17=6, 18=5, 19=2, 20=1, 21=3, 22=4, 23=23, 24=21}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=17, 5=18, 6=19, 7=23, 8=16, 9=15, 10=14, 11=13, 12=24, 13=26, 14=11, 15=12, 16=10, 17=9, 18=6, 19=7, 20=8, 21=5, 22=2, 23=1, 24=3, 25=4, 26=27, 27=25, 28=30, 29=29, 30=28, 31=31, 32=32}

