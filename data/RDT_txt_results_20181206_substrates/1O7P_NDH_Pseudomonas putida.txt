
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:2.0, O=O:1.0]

ORDER_CHANGED
[C%C*C@C:1.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1:1.0, O=O:4.0, O=O>>[CH]C([CH])O:1.0, O=O>>[CH]O:2.0, O=O>>[C]C(=[CH])C(O)C([CH])O:1.0, O=O>>[C]C(O)C(O)C=[CH]:1.0, O=O>>[C]C([CH])O:1.0, [CH2]:1.0, [CH]:5.0, [CH]=C1[C]C=CC=C1>>[CH]=C1[C]C=CC(O)C1O:1.0, [CH]C([CH])O:2.0, [CH]C=[CH]:1.0, [CH]C=[CH]>>[CH]C([CH])O:1.0, [CH]C=c1ccccc1=[CH]>>[CH]C=C1C(=[CH])C=CC(O)C1O:1.0, [CH]N([CH])[CH]:1.0, [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]:1.0, [CH]O:2.0, [CH][N+]([CH])=[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]C(=[CH])C(O)C([CH])O:1.0, [C]C(=[CH])C=C[CH]:2.0, [C]C(=[CH])C=C[CH]>>[C]C(=[CH])C(O)C([CH])O:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]:1.0, [C]C(O)C(O)C=[CH]:1.0, [C]C([CH])O:2.0, [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O:1.0, [C]C=CC=[CH]:1.0, [C]C=CC=[CH]>>[C]C(O)C(O)C=[CH]:1.0, [C]C=[CH]:2.0, [C]C=[CH]>>[C]C([CH])O:1.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C=[CH]:1.0, [N+]:1.0, [N]:1.0, [OH]:2.0, [O]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [OH]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (4)
[O=O:2.0, [CH]C([CH])O:1.0, [CH]O:2.0, [C]C([CH])O:1.0]


ID=Reaction Center at Level: 2 (5)
[O=O:2.0, [CH]C([CH])O:1.0, [C]C(=[CH])C(O)C([CH])O:1.0, [C]C(O)C(O)C=[CH]:1.0, [C]C([CH])O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (1)
[[CH]:4.0]


ID=Reaction Center at Level: 1 (4)
[[CH]C([CH])O:1.0, [CH]C=[CH]:1.0, [C]C([CH])O:1.0, [C]C=[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]C(=[CH])C(O)C([CH])O:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(O)C(O)C=[CH]:1.0, [C]C=CC=[CH]:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:4.0]


ID=Reaction Center at Level: 1 (4)
[[CH]C([CH])O:1.0, [CH]C=[CH]:1.0, [C]C([CH])O:1.0, [C]C=[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]C(=[CH])C(O)C([CH])O:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(O)C(O)C=[CH]:1.0, [C]C=CC=[CH]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C[CH]>>[C]C=[CH]
2: O=O>>[CH]O
3: [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]
4: [CH]C=[CH]>>[CH]C([CH])O
5: [C]C=[CH]>>[C]C([CH])O
6: O=O>>[CH]O

MMP Level 2
1: [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]
2: O=O>>[C]C([CH])O
3: [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]
4: [C]C=CC=[CH]>>[C]C(O)C(O)C=[CH]
5: [C]C(=[CH])C=C[CH]>>[C]C(=[CH])C(O)C([CH])O
6: O=O>>[CH]C([CH])O

MMP Level 3
1: O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1
2: O=O>>[C]C(=[CH])C(O)C([CH])O
3: [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O
4: [CH]=C1[C]C=CC=C1>>[CH]=C1[C]C=CC(O)C1O
5: [CH]C=c1ccccc1=[CH]>>[CH]C=C1C(=[CH])C=CC(O)C1O
6: O=O>>[C]C(O)C(O)C=[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00005	[c1ccc2ccccc2c1>>OC1C=Cc2ccccc2C1O, c1ccc2ccccc2c1>>OC1C=Cc2ccccc2C1O]
2: R:M00002, P:M00006	[O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O]
3: R:M00004, P:M00005	[O=O>>OC1C=Cc2ccccc2C1O, O=O>>OC1C=Cc2ccccc2C1O]


//
SELECTED AAM MAPPING
[H+:57].[O:55]=[O:56].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:5][N:6]([CH:7]=[CH:8][CH2:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44].[CH:45]:1:[CH:46]:[CH:47]:[C:48]:2:[CH:49]:[CH:50]:[CH:51]:[CH:52]:[C:53]2:[CH:54]1>>[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:9]:[CH:8]:[CH:7]:[N+:6](:[CH:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44].[OH:55][CH:50]1[CH:51]=[CH:52][C:53]:2:[CH:54]:[CH:45]:[CH:46]:[CH:47]:[C:48]2[CH:49]1[OH:56]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=45, 2=46, 3=47, 4=48, 5=49, 6=50, 7=51, 8=52, 9=53, 10=54, 11=9, 12=8, 13=7, 14=6, 15=5, 16=4, 17=2, 18=1, 19=3, 20=10, 21=43, 22=41, 23=12, 24=11, 25=13, 26=14, 27=15, 28=16, 29=17, 30=18, 31=19, 32=20, 33=21, 34=22, 35=23, 36=24, 37=39, 38=37, 39=26, 40=25, 41=27, 42=28, 43=29, 44=30, 45=35, 46=34, 47=33, 48=32, 49=31, 50=36, 51=38, 52=40, 53=42, 54=44, 55=57, 56=55, 57=56}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=51, 2=52, 3=53, 4=54, 5=55, 6=46, 7=47, 8=48, 9=49, 10=50, 11=45, 12=56, 13=6, 14=5, 15=4, 16=9, 17=8, 18=7, 19=10, 20=43, 21=41, 22=12, 23=11, 24=13, 25=14, 26=15, 27=16, 28=17, 29=18, 30=19, 31=20, 32=21, 33=22, 34=23, 35=24, 36=39, 37=37, 38=26, 39=25, 40=27, 41=28, 42=29, 43=30, 44=35, 45=34, 46=33, 47=32, 48=31, 49=36, 50=38, 51=40, 52=42, 53=44, 54=2, 55=1, 56=3}

