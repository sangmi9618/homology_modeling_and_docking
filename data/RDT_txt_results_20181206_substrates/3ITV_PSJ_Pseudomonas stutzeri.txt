
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%O:1.0]

ORDER_CHANGED
[C%O*C=O:1.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O=C(CO)C(O)C(O)C(O)[CH2]>>OCC1O[CH]C(O)C(O)C1O:1.0, O=C(CO)C(O)C([CH])O>>OCC1OC(O)[CH]C(O)C1O:1.0, O=C(CO)C([CH])O:1.0, O=C(CO)C([CH])O>>OCC1OC(O)C(O)[CH]C1O:1.0, O=C(CO)C([CH])O>>[CH]OC(CO)C([CH])O:1.0, O=C([CH2])C(O)C([CH])O:1.0, O=C([CH2])C(O)C([CH])O>>[O]C([CH2])C(O)C([CH])O:1.0, O=C([CH])[CH2]:2.0, O=C([CH])[CH2]>>[CH]C(O)OC([CH])[CH2]:1.0, O=C([CH])[CH2]>>[O]C([CH])[CH2]:1.0, [CH2]:1.0, [CH]:4.0, [CH]C(O)C(O)CO>>OC1OC([CH2])[CH]C(O)C1O:1.0, [CH]C(O)CO:1.0, [CH]C(O)CO>>[CH]OC(O)C([CH])O:1.0, [CH]C(O)OC([CH])[CH2]:1.0, [CH]C([CH])O:1.0, [CH]CO:1.0, [CH]CO>>[O]C([CH])O:1.0, [CH]OC(CO)C([CH])O:1.0, [CH]OC(O)C([CH])O:1.0, [CH]O[CH]:1.0, [C]:1.0, [C]=O:1.0, [C]=O>>[CH]O[CH]:1.0, [C]C([CH])O:1.0, [C]C([CH])O>>[CH]C([CH])O:1.0, [O]:2.0, [O]C([CH2])C(O)C([CH])O:1.0, [O]C([CH])O:1.0, [O]C([CH])[CH2]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (2)
[[CH]O[CH]:1.0, [O]C([CH])O:1.0]


ID=Reaction Center at Level: 2 (2)
[[CH]C(O)OC([CH])[CH2]:1.0, [CH]OC(O)C([CH])O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[CH]:1.0, [C]:1.0, [O]:2.0]


ID=Reaction Center at Level: 1 (4)
[O=C([CH])[CH2]:1.0, [CH]O[CH]:1.0, [C]=O:1.0, [O]C([CH])[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(CO)C([CH])O:1.0, O=C([CH])[CH2]:1.0, [CH]C(O)OC([CH])[CH2]:1.0, [CH]OC(CO)C([CH])O:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:3.0, [C]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([CH])[CH2]:1.0, [CH]C([CH])O:1.0, [C]C([CH])O:1.0, [O]C([CH])[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(CO)C([CH])O:1.0, O=C([CH2])C(O)C([CH])O:1.0, [CH]OC(CO)C([CH])O:1.0, [O]C([CH2])C(O)C([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]=O>>[CH]O[CH]
2: [CH]CO>>[O]C([CH])O
3: [C]C([CH])O>>[CH]C([CH])O
4: O=C([CH])[CH2]>>[O]C([CH])[CH2]

MMP Level 2
1: O=C([CH])[CH2]>>[CH]C(O)OC([CH])[CH2]
2: [CH]C(O)CO>>[CH]OC(O)C([CH])O
3: O=C([CH2])C(O)C([CH])O>>[O]C([CH2])C(O)C([CH])O
4: O=C(CO)C([CH])O>>[CH]OC(CO)C([CH])O

MMP Level 3
1: O=C(CO)C([CH])O>>OCC1OC(O)C(O)[CH]C1O
2: [CH]C(O)C(O)CO>>OC1OC([CH2])[CH]C(O)C1O
3: O=C(CO)C(O)C(O)C(O)[CH2]>>OCC1O[CH]C(O)C(O)C1O
4: O=C(CO)C(O)C([CH])O>>OCC1OC(O)[CH]C(O)C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(CO)C(O)C(O)C(O)CO>>OCC1OC(O)C(O)C(O)C1O, O=C(CO)C(O)C(O)C(O)CO>>OCC1OC(O)C(O)C(O)C1O, O=C(CO)C(O)C(O)C(O)CO>>OCC1OC(O)C(O)C(O)C1O, O=C(CO)C(O)C(O)C(O)CO>>OCC1OC(O)C(O)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([CH2:3][OH:4])[CH:5]([OH:6])[CH:7]([OH:8])[CH:9]([OH:10])[CH2:11][OH:12]>>[OH:4][CH2:3][CH:2]1[O:1][CH:11]([OH:12])[CH:9]([OH:10])[CH:7]([OH:8])[CH:5]1[OH:6]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=11, 2=9, 3=7, 4=5, 5=2, 6=1, 7=3, 8=4, 9=6, 10=8, 11=10, 12=12}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=2, 2=3, 3=11, 4=9, 5=7, 6=5, 7=4, 8=6, 9=8, 10=10, 11=12, 12=1}

