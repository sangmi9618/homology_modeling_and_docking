
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0, C-N:1.0, C-O:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

//
FINGERPRINTS RC
[O=C(O)C1=N[C]=[C]S1:1.0, O=C(O)C1=N[C]=[C]S1>>O=C=O:1.0, O=C(O)C1=N[C]=[C]S1>>[C]1=[C][N+]([CH2])=CS1:1.0, O=C(O)c1nc(c(s1)[CH2])C>>[C]C(=[CH])C[n+]1csc([CH2])c1C:1.0, O=C(O)c1nc(c(s1)[CH2])C>>[C]C[n+]1csc([CH2])c1C:1.0, O=C=O:3.0, [CH2]:2.0, [CH]:1.0, [C]:3.0, [C]1=[C][N+]([CH2])=CS1:1.0, [C]=O:1.0, [C]C(=O)O:2.0, [C]C(=O)O>>O=C=O:2.0, [C]C(=[CH])COP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, [C]C(=[CH])CO[P]:1.0, [C]C(=[CH])CO[P]>>[C]C(=[CH])C[N+]([C])=[CH]:1.0, [C]C(=[CH])C[N+]([C])=[CH]:1.0, [C]C(=[N])[S]:1.0, [C]C(=[N])[S]>>[N+]=C[S]:1.0, [C]C1=NC(=[C]S1)C:1.0, [C]C1=NC(=[C]S1)C>>[C]C[N+]1=CS[C]=C1C:1.0, [C]COP([O])(=O)O:1.0, [C]COP([O])(=O)O>>[O]P(=O)(O)O:1.0, [C]C[N+]:1.0, [C]C[N+]1=CS[C]=C1C:1.0, [C]C[O]:1.0, [C]C[O]>>[C]C[N+]:1.0, [C]N=[C]:1.0, [C]N=[C]>>[C][N+](=[CH])[CH2]:1.0, [C]O:1.0, [C]O>>[C]=O:1.0, [C][N+](=[CH])[CH2]:1.0, [N+]:1.0, [N+]=C[S]:1.0, [N]:1.0, [N]=C([S])C(=O)O:1.0, [N]=C([S])C(=O)O>>O=C=O:2.0, [N]C=C(C(=[N])N)COP([O])(=O)O>>[N]C=C(C(=[N])N)C[N+]1=CS[C]=C1C:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[CH2]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[CH2]:2.0, [C]:2.0, [N+]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (6)
[[C]C(=O)O:1.0, [C]C(=[N])[S]:1.0, [C]C[N+]:1.0, [C]C[O]:1.0, [C][N+](=[CH])[CH2]:1.0, [P]O[CH2]:1.0]


ID=Reaction Center at Level: 2 (6)
[O=C(O)C1=N[C]=[C]S1:1.0, [C]C(=[CH])CO[P]:1.0, [C]C(=[CH])C[N+]([C])=[CH]:1.0, [C]COP([O])(=O)O:1.0, [C]C[N+]1=CS[C]=C1C:1.0, [N]=C([S])C(=O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C=O:1.0, [C]=O:1.0, [C]C(=O)O:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (3)
[O=C=O:2.0, [C]C(=O)O:1.0, [N]=C([S])C(=O)O:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C(=O)O>>O=C=O
2: [C]N=[C]>>[C][N+](=[CH])[CH2]
3: [C]O>>[C]=O
4: [C]C(=[N])[S]>>[N+]=C[S]
5: [P]O[CH2]>>[P]O
6: [C]C[O]>>[C]C[N+]

MMP Level 2
1: [N]=C([S])C(=O)O>>O=C=O
2: [C]C1=NC(=[C]S1)C>>[C]C[N+]1=CS[C]=C1C
3: [C]C(=O)O>>O=C=O
4: O=C(O)C1=N[C]=[C]S1>>[C]1=[C][N+]([CH2])=CS1
5: [C]COP([O])(=O)O>>[O]P(=O)(O)O
6: [C]C(=[CH])CO[P]>>[C]C(=[CH])C[N+]([C])=[CH]

MMP Level 3
1: O=C(O)C1=N[C]=[C]S1>>O=C=O
2: O=C(O)c1nc(c(s1)[CH2])C>>[C]C(=[CH])C[n+]1csc([CH2])c1C
3: [N]=C([S])C(=O)O>>O=C=O
4: O=C(O)c1nc(c(s1)[CH2])C>>[C]C[n+]1csc([CH2])c1C
5: [C]C(=[CH])COP(=O)(O)O[P]>>O=P(O)(O)O[P]
6: [N]C=C(C(=[N])N)COP([O])(=O)O>>[N]C=C(C(=[N])N)C[N+]1=CS[C]=C1C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OCc1cnc(nc1N)C>>O=P(O)(O)OP(=O)(O)O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OCc1cnc(nc1N)C>>O=P(O)(O)OCCc1sc[n+](c1C)Cc2cnc(nc2N)C]
3: R:M00002, P:M00004	[O=C(O)c1nc(c(s1)CCOP(=O)(O)O)C>>O=P(O)(O)OCCc1sc[n+](c1C)Cc2cnc(nc2N)C, O=C(O)c1nc(c(s1)CCOP(=O)(O)O)C>>O=P(O)(O)OCCc1sc[n+](c1C)Cc2cnc(nc2N)C]
4: R:M00002, P:M00005	[O=C(O)c1nc(c(s1)CCOP(=O)(O)O)C>>O=C=O, O=C(O)c1nc(c(s1)CCOP(=O)(O)O)C>>O=C=O]


//
SELECTED AAM MAPPING
[O:19]=[C:20]([OH:21])[C:22]:1:[N:23]:[C:24](:[C:25](:[S:26]1)[CH2:27][CH2:28][O:29][P:30](=[O:31])([OH:32])[OH:33])[CH3:34].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][CH2:10][C:11]:1:[CH:12]:[N:13]:[C:14](:[N:15]:[C:16]1[NH2:17])[CH3:18]>>[O:19]=[C:20]=[O:21].[O:31]=[P:30]([OH:32])([OH:33])[O:29][CH2:28][CH2:27][C:25]:1:[S:26]:[CH:22]:[N+:23](:[C:24]1[CH3:34])[CH2:10][C:11]:2:[CH:12]:[N:13]:[C:14](:[N:15]:[C:16]2[NH2:17])[CH3:18].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[OH:9]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=18, 2=14, 3=13, 4=12, 5=11, 6=16, 7=15, 8=17, 9=10, 10=9, 11=6, 12=7, 13=8, 14=5, 15=2, 16=1, 17=3, 18=4, 19=34, 20=24, 21=25, 22=26, 23=22, 24=23, 25=20, 26=19, 27=21, 28=27, 29=28, 30=29, 31=30, 32=31, 33=32, 34=33}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=25, 2=24, 3=23, 4=26, 5=27, 6=28, 7=29, 8=30, 9=31, 10=13, 11=12, 12=8, 13=9, 14=10, 15=11, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=7, 26=6, 27=5, 28=2, 29=1, 30=3, 31=4, 32=33, 33=32, 34=34}

