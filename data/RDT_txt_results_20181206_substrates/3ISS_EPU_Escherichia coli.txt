
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:2.0]

//
FINGERPRINTS RC
[O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(OC([CH])[CH])=C:1.0, O=C(O)C(OP(=O)(O)O)=C>>O=P(O)(O)O:1.0, O=C(O)C(O[CH])=C:1.0, O=C(O)C(O[P])=C:1.0, O=C(O)C(O[P])=C>>O=C(O)C(O[CH])=C:1.0, O=P(O)(O)O:1.0, [CH]C([CH])O:1.0, [CH]C([CH])O>>[C]C(OC([CH])[CH])=C:1.0, [CH]C([NH])C(O)C([CH])O>>O=C(O)C(OC(C([CH])[NH])C([CH])O)=C:1.0, [CH]O:1.0, [CH]O>>[C]O[CH]:1.0, [C]:2.0, [C]C(OC([CH])[CH])=C:1.0, [C]C(OP(=O)(O)O)=C:1.0, [C]C(OP(=O)(O)O)=C>>O=P(O)(O)O:1.0, [C]C([O])=C:2.0, [C]C([O])=C>>[C]C([O])=C:1.0, [C]O[CH]:1.0, [C]O[P]:1.0, [C]O[P]>>[P]O:1.0, [OH]:2.0, [O]:2.0, [P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[C]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (3)
[[C]C([O])=C:2.0, [C]O[CH]:1.0, [C]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(O)C(O[CH])=C:1.0, O=C(O)C(O[P])=C:1.0, [C]C(OC([CH])[CH])=C:1.0, [C]C(OP(=O)(O)O)=C:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH]O>>[C]O[CH]
2: [C]O[P]>>[P]O
3: [C]C([O])=C>>[C]C([O])=C

MMP Level 2
1: [CH]C([CH])O>>[C]C(OC([CH])[CH])=C
2: [C]C(OP(=O)(O)O)=C>>O=P(O)(O)O
3: O=C(O)C(O[P])=C>>O=C(O)C(O[CH])=C

MMP Level 3
1: [CH]C([NH])C(O)C([CH])O>>O=C(O)C(OC(C([CH])[NH])C([CH])O)=C
2: O=C(O)C(OP(=O)(O)O)=C>>O=P(O)(O)O
3: O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(OC([CH])[CH])=C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(OP(=O)(O)O)=C>>O=P(O)(O)O]
2: R:M00001, P:M00004	[O=C(O)C(OP(=O)(O)O)=C>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(OC(=C)C(=O)O)C3NC(=O)C)C(O)C2O]
3: R:M00002, P:M00004	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3NC(=O)C)C(O)C2O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(OC(=C)C(=O)O)C3NC(=O)C)C(O)C2O]


//
SELECTED AAM MAPPING
[O:40]=[C:41]([OH:42])[C:43]([O:44][P:45](=[O:46])([OH:47])[OH:48])=[CH2:49].[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][CH:22]3[O:23][CH:24]([CH2:25][OH:26])[CH:27]([OH:28])[CH:29]([OH:30])[CH:31]3[NH:32][C:33](=[O:34])[CH3:35])[CH:36]([OH:37])[CH:38]2[OH:39]>>[O:40]=[C:41]([OH:42])[C:43]([O:30][CH:29]1[CH:27]([OH:28])[CH:24]([O:23][CH:22]([O:21][P:18](=[O:19])([OH:20])[O:17][P:14](=[O:15])([OH:16])[O:13][CH2:12][CH:11]2[O:10][CH:9]([N:5]3[CH:4]=[CH:3][C:2](=[O:1])[NH:8][C:6]3=[O:7])[CH:38]([OH:39])[CH:36]2[OH:37])[CH:31]1[NH:32][C:33](=[O:34])[CH3:35])[CH2:25][OH:26])=[CH2:49].[O:46]=[P:45]([OH:44])([OH:47])[OH:48]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=49, 2=43, 3=41, 4=40, 5=42, 6=44, 7=45, 8=46, 9=47, 10=48, 11=35, 12=33, 13=34, 14=32, 15=31, 16=29, 17=27, 18=24, 19=23, 20=22, 21=21, 22=18, 23=19, 24=20, 25=17, 26=14, 27=15, 28=16, 29=13, 30=12, 31=11, 32=36, 33=38, 34=9, 35=10, 36=5, 37=4, 38=3, 39=2, 40=1, 41=8, 42=6, 43=7, 44=39, 45=37, 46=25, 47=26, 48=28, 49=30}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=47, 2=46, 3=45, 4=48, 5=49, 6=41, 7=39, 8=40, 9=38, 10=37, 11=6, 12=7, 13=9, 14=10, 15=11, 16=12, 17=13, 18=14, 19=15, 20=16, 21=17, 22=18, 23=19, 24=20, 25=21, 26=22, 27=35, 28=33, 29=24, 30=23, 31=25, 32=26, 33=27, 34=28, 35=29, 36=30, 37=31, 38=32, 39=34, 40=36, 41=42, 42=43, 43=8, 44=5, 45=4, 46=44, 47=2, 48=1, 49=3}

