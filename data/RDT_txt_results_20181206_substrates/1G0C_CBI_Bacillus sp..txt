
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0]

//
FINGERPRINTS RC
[[CH]:1.0, [CH]O:1.0, [CH]O[CH]:1.0, [CH]O[CH]>>[CH]O:1.0, [OH]:1.0, [O]:1.0, [O]C([CH2])C(OC(O[CH])C([CH])O)C([CH])O>>[CH]OC(O)C([CH])O:1.0, [O]C([CH2])C(O[CH])C([CH])O:1.0, [O]C([CH])O:1.0, [O]C([CH])OC([CH])[CH]:1.0, [O]C([CH])OC([CH])[CH]>>[O]C([CH])O:1.0, [O]C([CH])[CH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (2)
[[CH]O[CH]:1.0, [O]C([CH])[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[O]C([CH2])C(O[CH])C([CH])O:1.0, [O]C([CH])OC([CH])[CH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH]O[CH]>>[CH]O

MMP Level 2
1: [O]C([CH])OC([CH])[CH]>>[O]C([CH])O

MMP Level 3
1: [O]C([CH2])C(OC(O[CH])C([CH])O)C([CH])O>>[CH]OC(O)C([CH])O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[OCC1OC(OC2C(O)C(O)C(OC2CO)OC3C(O)C(O)C(O)OC3CO)C(O)C(O)C1O>>OCC1OC(OC2C(O)C(O)C(O)OC2CO)C(O)C(O)C1O]


//
SELECTED AAM MAPPING
[OH2:35].[OH:1][CH2:2][CH:3]1[O:4][CH:5]([O:6][CH:7]2[CH:8]([OH:9])[CH:10]([OH:11])[CH:12]([O:13][CH:14]2[CH2:15][OH:16])[O:17][CH:24]3[CH:25]([OH:26])[CH:27]([OH:28])[CH:29]([OH:30])[O:31][CH:32]3[CH2:33][OH:34])[CH:18]([OH:19])[CH:20]([OH:21])[CH:22]1[OH:23]>>[OH:1][CH2:2][CH:3]1[O:4][CH:5]([O:6][CH:7]2[CH:8]([OH:9])[CH:10]([OH:11])[CH:12]([OH:17])[O:13][CH:14]2[CH2:15][OH:16])[CH:18]([OH:19])[CH:20]([OH:21])[CH:22]1[OH:23]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=2, 2=3, 3=33, 4=31, 5=29, 6=5, 7=4, 8=6, 9=7, 10=14, 11=13, 12=12, 13=10, 14=8, 15=9, 16=11, 17=17, 18=18, 19=26, 20=25, 21=23, 22=21, 23=19, 24=20, 25=22, 26=24, 27=27, 28=28, 29=15, 30=16, 31=30, 32=32, 33=34, 34=1, 35=35}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=2, 2=3, 3=22, 4=20, 5=18, 6=5, 7=4, 8=6, 9=7, 10=15, 11=14, 12=12, 13=10, 14=8, 15=9, 16=11, 17=13, 18=16, 19=17, 20=19, 21=21, 22=23, 23=1}

