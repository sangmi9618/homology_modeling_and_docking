
//
FINGERPRINTS BC

FORMED_CLEAVED
[C@N:1.0]

ORDER_CHANGED
[C-C*C@C:1.0, O-O*O=O:1.0]

//
FINGERPRINTS RC
[O=C(O)C1=C[C]=[C]N1:1.0, O=O:4.0, O=O>>OO:6.0, OO:4.0, [CH2]:2.0, [CH]:6.0, [CH]C[CH]:1.0, [CH]C[CH]>>[C]C=[C]:1.0, [C]:4.0, [C]=C([CH])C(C([O-])=O)C[CH]:1.0, [C]=C([CH])C(C([O-])=O)C[CH]>>[C]C=C(C([C])=[C])C(=O)O:1.0, [C]C(=O)C=1NC(C([O-])=O)C[CH]C1[CH]>>[C]C1=[C]C=C(N=C1C([C])=O)C(=O)O:1.0, [C]C(=O)O:3.0, [C]C(=[CH])CC(C([O-])=O)[NH3+]>>[C]c1cc([nH]c1[C])C(=O)O:1.0, [C]C(=[CH])CC([C])[NH3+]:1.0, [C]C(=[CH])CC([C])[NH3+]>>[C]C1=[C]NC([C])=C1:1.0, [C]C(=[CH])[NH]:1.0, [C]C(=[C])N=C([C])[CH]:1.0, [C]C(=[C])NC([C])[CH2]:1.0, [C]C(=[C])NC([C])[CH2]>>[C]C(=[C])N=C([C])[CH]:1.0, [C]C(=[C])[NH]:1.0, [C]C(=[N])[CH]:1.0, [C]C([CH2])=CC(=[C])[CH]:1.0, [C]C([CH2])=CC(=[C])[CH]>>[C]C=1C=[C]NC1C([C])=[C]:1.0, [C]C([CH2])C([O-])=O>>[C]C(=[CH])C(=O)O:1.0, [C]C([CH2])C=1C=C(C(=O)[C]C1[NH])C[CH]>>[C]c1cc2C(=O)[C]C([N])=C(c2[nH]1)C([C])=[CH]:1.0, [C]C([CH2])[NH3+]:1.0, [C]C([CH2])[NH3+]>>[C]C(=[CH])[NH]:1.0, [C]C([C])=CC([C])=[N]:1.0, [C]C([C])=[CH]:1.0, [C]C([C])CC([C])[NH]:1.0, [C]C([C])CC([C])[NH]>>[C]C([C])=CC([C])=[N]:1.0, [C]C([C])[CH2]:1.0, [C]C([C])[CH2]>>[C]C([C])=[CH]:1.0, [C]C([NH])[CH2]:1.0, [C]C([NH])[CH2]>>[C]C(=[N])[CH]:1.0, [C]C1=[C]C([C])CC(N1)C([O-])=O>>[C]C=1[C]=C([C])C=C(N1)C(=O)O:1.0, [C]C1=[C]NC([C])=C1:1.0, [C]C=1C=[C]NC1C([C])=[C]:1.0, [C]C=1NC([C])CC(C1C=[C])C([O-])=O>>[C]c1nc([C])c(c(c1)C(=O)O)C(=[C])[NH]:1.0, [C]C=C(C([C])=O)CC(C([O-])=O)[NH3+]>>[C]c1[nH]c(cc1C([C])=O)C(=O)O:1.0, [C]C=C(C([C])=[C])C(=O)O:1.0, [C]C=[C]:3.0, [C]C=[C]>>[C]C(=[C])[NH]:1.0, [C]CC(C([O-])=O)[NH3+]:1.0, [C]CC(C([O-])=O)[NH3+]>>O=C(O)C1=C[C]=[C]N1:1.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C=[C]:1.0, [C]N=C(C=[C])C(=O)O:1.0, [C]N=[C]:1.0, [C]NC(C([O-])=O)C[CH]:1.0, [C]NC(C([O-])=O)C[CH]>>[C]N=C(C=[C])C(=O)O:1.0, [C]N[CH]:1.0, [C]N[CH]>>[C]N=[C]:1.0, [C]O:3.0, [C][O-]:3.0, [C][O-]>>[C]O:3.0, [NH]:1.0, [N]:1.0, [O-]:3.0, [O-]C(=O)C([CH2])[NH3+]>>O=C(O)C(=[CH])[NH]:1.0, [O-]C(=O)C([NH])[CH2]>>[N]=C([CH])C(=O)O:1.0, [O-]C(=O)C1N[C]=C([CH])C(C([O-])=O)C1>>[C]C1=[C]N=C(C=C1C(=O)O)C(=O)O:1.0, [O-]C(=O)[CH]:3.0, [O-]C(=O)[CH]>>[C]C(=O)O:3.0, [OH]:5.0, [O]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[C]:1.0, [NH]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(=[C])[NH]:1.0, [C]N[C]:1.0]


ID=Reaction Center at Level: 2 (2)
[[C]C1=[C]C=C([C])N1:1.0, [C]C=1C=[C]NC1C([C])=[C]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (5)
[[CH2]:1.0, [CH]:2.0, [C]:1.0, [OH]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (6)
[O=O:2.0, OO:2.0, [C]C(=[CH])[NH]:1.0, [C]C([CH2])[NH3+]:1.0, [C]C=[C]:1.0, [C]C[CH]:1.0]


ID=Reaction Center at Level: 2 (6)
[O=C(O)C1=C[C]=[C]N1:1.0, O=O:2.0, OO:2.0, [C]C(=[CH])CC([C])[NH3+]:1.0, [C]C1=[C]NC([C])=C1:1.0, [C]CC(C([O-])=O)[NH3+]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C][O-]>>[C]O
2: [CH]C[CH]>>[C]C=[C]
3: O=O>>OO
4: [C]C[CH]>>[C]C=[C]
5: [C][O-]>>[C]O
6: [C][O-]>>[C]O
7: [C]C([CH2])[NH3+]>>[C]C(=[CH])[NH]
8: [C]N[CH]>>[C]N=[C]
9: [C]C([NH])[CH2]>>[C]C(=[N])[CH]
10: [C]C=[C]>>[C]C(=[C])[NH]
11: [C]C([C])[CH2]>>[C]C([C])=[CH]

MMP Level 2
1: [O-]C(=O)[CH]>>[C]C(=O)O
2: [C]C([C])CC([C])[NH]>>[C]C([C])=CC([C])=[N]
3: O=O>>OO
4: [C]C(=[CH])CC([C])[NH3+]>>[C]C1=[C]NC([C])=C1
5: [O-]C(=O)[CH]>>[C]C(=O)O
6: [O-]C(=O)[CH]>>[C]C(=O)O
7: [C]CC(C([O-])=O)[NH3+]>>O=C(O)C1=C[C]=[C]N1
8: [C]C(=[C])NC([C])[CH2]>>[C]C(=[C])N=C([C])[CH]
9: [C]NC(C([O-])=O)C[CH]>>[C]N=C(C=[C])C(=O)O
10: [C]C([CH2])=CC(=[C])[CH]>>[C]C=1C=[C]NC1C([C])=[C]
11: [C]=C([CH])C(C([O-])=O)C[CH]>>[C]C=C(C([C])=[C])C(=O)O

MMP Level 3
1: [O-]C(=O)C([NH])[CH2]>>[N]=C([CH])C(=O)O
2: [O-]C(=O)C1N[C]=C([CH])C(C([O-])=O)C1>>[C]C1=[C]N=C(C=C1C(=O)O)C(=O)O
3: O=O>>OO
4: [C]C=C(C([C])=O)CC(C([O-])=O)[NH3+]>>[C]c1[nH]c(cc1C([C])=O)C(=O)O
5: [O-]C(=O)C([CH2])[NH3+]>>O=C(O)C(=[CH])[NH]
6: [C]C([CH2])C([O-])=O>>[C]C(=[CH])C(=O)O
7: [C]C(=[CH])CC(C([O-])=O)[NH3+]>>[C]c1cc([nH]c1[C])C(=O)O
8: [C]C(=O)C=1NC(C([O-])=O)C[CH]C1[CH]>>[C]C1=[C]C=C(N=C1C([C])=O)C(=O)O
9: [C]C1=[C]C([C])CC(N1)C([O-])=O>>[C]C=1[C]=C([C])C=C(N1)C(=O)O
10: [C]C([CH2])C=1C=C(C(=O)[C]C1[NH])C[CH]>>[C]c1cc2C(=O)[C]C([N])=C(c2[nH]1)C([C])=[CH]
11: [C]C=1NC([C])CC(C1C=[C])C([O-])=O>>[C]c1nc([C])c(c(c1)C(=O)O)C(=[C])[NH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[[O-]C(=O)C1NC=2C(=O)C(=O)C(=CC2C(C([O-])=O)C1)CC(C([O-])=O)[NH3+]>>O=C1C(=O)c2cc([nH]c2-c3c1nc(cc3C(=O)O)C(=O)O)C(=O)O, [O-]C(=O)C1NC=2C(=O)C(=O)C(=CC2C(C([O-])=O)C1)CC(C([O-])=O)[NH3+]>>O=C1C(=O)c2cc([nH]c2-c3c1nc(cc3C(=O)O)C(=O)O)C(=O)O, [O-]C(=O)C1NC=2C(=O)C(=O)C(=CC2C(C([O-])=O)C1)CC(C([O-])=O)[NH3+]>>O=C1C(=O)c2cc([nH]c2-c3c1nc(cc3C(=O)O)C(=O)O)C(=O)O, [O-]C(=O)C1NC=2C(=O)C(=O)C(=CC2C(C([O-])=O)C1)CC(C([O-])=O)[NH3+]>>O=C1C(=O)c2cc([nH]c2-c3c1nc(cc3C(=O)O)C(=O)O)C(=O)O, [O-]C(=O)C1NC=2C(=O)C(=O)C(=CC2C(C([O-])=O)C1)CC(C([O-])=O)[NH3+]>>O=C1C(=O)c2cc([nH]c2-c3c1nc(cc3C(=O)O)C(=O)O)C(=O)O, [O-]C(=O)C1NC=2C(=O)C(=O)C(=CC2C(C([O-])=O)C1)CC(C([O-])=O)[NH3+]>>O=C1C(=O)c2cc([nH]c2-c3c1nc(cc3C(=O)O)C(=O)O)C(=O)O, [O-]C(=O)C1NC=2C(=O)C(=O)C(=CC2C(C([O-])=O)C1)CC(C([O-])=O)[NH3+]>>O=C1C(=O)c2cc([nH]c2-c3c1nc(cc3C(=O)O)C(=O)O)C(=O)O, [O-]C(=O)C1NC=2C(=O)C(=O)C(=CC2C(C([O-])=O)C1)CC(C([O-])=O)[NH3+]>>O=C1C(=O)c2cc([nH]c2-c3c1nc(cc3C(=O)O)C(=O)O)C(=O)O, [O-]C(=O)C1NC=2C(=O)C(=O)C(=CC2C(C([O-])=O)C1)CC(C([O-])=O)[NH3+]>>O=C1C(=O)c2cc([nH]c2-c3c1nc(cc3C(=O)O)C(=O)O)C(=O)O, [O-]C(=O)C1NC=2C(=O)C(=O)C(=CC2C(C([O-])=O)C1)CC(C([O-])=O)[NH3+]>>O=C1C(=O)c2cc([nH]c2-c3c1nc(cc3C(=O)O)C(=O)O)C(=O)O]
2: R:M00002, P:M00004	[O=O>>OO]


//
SELECTED AAM MAPPING
[O:25]=[O:26].[O:1]=[C:2]1[C:3](=[O:4])[C:5](=[CH:6][C:7]2=[C:8]1[NH:9][CH:10]([C:11](=[O:12])[O-:13])[CH2:14][CH:15]2[C:16](=[O:17])[O-:18])[CH2:19][CH:20]([C:21](=[O:22])[O-:23])[NH3+:24]>>[O:12]=[C:11]([OH:13])[C:10]:1:[N:9]:[C:8]:2[C:2](=[O:1])[C:3](=[O:4])[C:5]:3:[CH:19]:[C:20](:[NH:24]:[C:6]3[C:7]2:[C:15](:[CH:14]1)[C:16](=[O:17])[OH:18])[C:21](=[O:22])[OH:23].[OH2:27].[OH:25][OH:26]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=14, 2=15, 3=7, 4=8, 5=2, 6=1, 7=3, 8=4, 9=5, 10=6, 11=19, 12=20, 13=21, 14=22, 15=23, 16=24, 17=9, 18=10, 19=11, 20=12, 21=13, 22=16, 23=17, 24=18, 25=25, 26=26}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=18, 2=17, 3=16, 4=6, 5=7, 6=8, 7=9, 8=10, 9=11, 10=15, 11=14, 12=13, 13=12, 14=22, 15=23, 16=24, 17=5, 18=4, 19=2, 20=1, 21=3, 22=19, 23=20, 24=21, 25=25, 26=26, 27=27}

