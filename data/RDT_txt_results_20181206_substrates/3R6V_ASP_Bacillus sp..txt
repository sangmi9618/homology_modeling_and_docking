
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0]

ORDER_CHANGED
[C-C*C=C:1.0]

//
FINGERPRINTS RC
[N:3.0, O=C(O)CC(N)C(=O)O>>O=C(O)C=CC(=O)O:2.0, [CH2]:1.0, [CH]:3.0, [CH]N:1.0, [CH]N>>N:1.0, [C]C(N)CC(=O)O:1.0, [C]C(N)CC(=O)O>>[C]C=CC(=O)O:1.0, [C]C([CH2])N:2.0, [C]C([CH2])N>>N:1.0, [C]C([CH2])N>>[C]C=[CH]:1.0, [C]C=CC(=O)O:2.0, [C]C=[CH]:2.0, [C]CC(N)C(=O)O:1.0, [C]CC(N)C(=O)O>>N:1.0, [C]CC(N)C(=O)O>>[C]C=CC(=O)O:1.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C=[CH]:1.0, [NH2]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[N:1.0, [CH]:1.0, [NH2]:1.0]


ID=Reaction Center at Level: 1 (3)
[N:1.0, [CH]N:1.0, [C]C([CH2])N:1.0]


ID=Reaction Center at Level: 2 (3)
[N:1.0, [C]C([CH2])N:1.0, [C]CC(N)C(=O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (2)
[[CH2]:1.0, [CH]:3.0]


ID=Reaction Center at Level: 1 (3)
[[C]C([CH2])N:1.0, [C]C=[CH]:2.0, [C]C[CH]:1.0]


ID=Reaction Center at Level: 2 (3)
[[C]C(N)CC(=O)O:1.0, [C]C=CC(=O)O:2.0, [C]CC(N)C(=O)O:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C([CH2])N>>[C]C=[CH]
2: [C]C[CH]>>[C]C=[CH]
3: [CH]N>>N

MMP Level 2
1: [C]CC(N)C(=O)O>>[C]C=CC(=O)O
2: [C]C(N)CC(=O)O>>[C]C=CC(=O)O
3: [C]C([CH2])N>>N

MMP Level 3
1: O=C(O)CC(N)C(=O)O>>O=C(O)C=CC(=O)O
2: O=C(O)CC(N)C(=O)O>>O=C(O)C=CC(=O)O
3: [C]CC(N)C(=O)O>>N


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)CC(N)C(=O)O>>O=C(O)C=CC(=O)O, O=C(O)CC(N)C(=O)O>>O=C(O)C=CC(=O)O]
2: R:M00001, P:M00003	[O=C(O)CC(N)C(=O)O>>N]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH2:4][CH:5]([NH2:6])[C:7](=[O:8])[OH:9]>>[O:1]=[C:2]([OH:3])[CH:4]=[CH:5][C:7](=[O:8])[OH:9].[NH3:6]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=4, 2=5, 3=7, 4=8, 5=9, 6=6, 7=2, 8=1, 9=3}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=5, 2=4, 3=2, 4=1, 5=3, 6=6, 7=7, 8=8, 9=9}

