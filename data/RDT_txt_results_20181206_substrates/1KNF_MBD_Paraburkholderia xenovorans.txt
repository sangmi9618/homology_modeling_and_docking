
//
FINGERPRINTS BC

FORMED_CLEAVED
[C=O:2.0, C@C:1.0, O=O:1.0]

//
FINGERPRINTS RC
[O=C(C=[CH])C:1.0, O=C(O)C(=[CH])O:1.0, O=C([CH])C:2.0, O=O:4.0, O=O>>O=C(C=[CH])C:1.0, O=O>>O=C(O)C(=[CH])O:1.0, O=O>>O=C([CH])C:1.0, O=O>>[C]=O:2.0, O=O>>[C]C(=O)O:1.0, Oc1cccc(c1O)C>>O=C(C=C[CH])C:1.0, Oc1cccc(c1O)C>>O=C(O)C(O)=C[CH]:1.0, [CH]C(=C(O)C(=[CH])O)C:1.0, [CH]C(=C(O)C(=[CH])O)C>>O=C(O)C(=[CH])O:1.0, [C]:4.0, [C]=C([CH])C:1.0, [C]=C([CH])C>>O=C([CH])C:1.0, [C]=O:2.0, [C]C(=O)O:2.0, [C]C(=[C])O:1.0, [C]C(=[C])O>>[C]C(=O)O:1.0, [C]C(O)=C(C=[CH])C:1.0, [C]C(O)=C(C=[CH])C>>O=C(C=[CH])C:1.0, [O]:4.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[C]:4.0, [O]:4.0]


ID=Reaction Center at Level: 1 (6)
[O=C([CH])C:1.0, O=O:2.0, [C]=C([CH])C:1.0, [C]=O:2.0, [C]C(=O)O:1.0, [C]C(=[C])O:1.0]


ID=Reaction Center at Level: 2 (7)
[O=C(C=[CH])C:1.0, O=C(O)C(=[CH])O:1.0, O=C([CH])C:1.0, O=O:2.0, [CH]C(=C(O)C(=[CH])O)C:1.0, [C]C(=O)O:1.0, [C]C(O)=C(C=[CH])C:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=O>>[C]=O
2: [C]=C([CH])C>>O=C([CH])C
3: [C]C(=[C])O>>[C]C(=O)O
4: O=O>>[C]=O

MMP Level 2
1: O=O>>O=C([CH])C
2: [C]C(O)=C(C=[CH])C>>O=C(C=[CH])C
3: [CH]C(=C(O)C(=[CH])O)C>>O=C(O)C(=[CH])O
4: O=O>>[C]C(=O)O

MMP Level 3
1: O=O>>O=C(C=[CH])C
2: Oc1cccc(c1O)C>>O=C(C=C[CH])C
3: Oc1cccc(c1O)C>>O=C(O)C(O)=C[CH]
4: O=O>>O=C(O)C(=[CH])O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[Oc1cccc(c1O)C>>O=C(O)C(O)=CC=CC(=O)C, Oc1cccc(c1O)C>>O=C(O)C(O)=CC=CC(=O)C]
2: R:M00002, P:M00003	[O=O>>O=C(O)C(O)=CC=CC(=O)C, O=O>>O=C(O)C(O)=CC=CC(=O)C]


//
SELECTED AAM MAPPING
[O:10]=[O:11].[OH:1][C:2]:1:[CH:3]:[CH:4]:[CH:5]:[C:6](:[C:7]1[OH:8])[CH3:9]>>[O:10]=[C:7]([OH:8])[C:2]([OH:1])=[CH:3][CH:4]=[CH:5][C:6](=[O:11])[CH3:9]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=6, 3=7, 4=2, 5=3, 6=4, 7=5, 8=1, 9=8, 10=10, 11=11}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=11, 2=9, 3=10, 4=8, 5=7, 6=6, 7=4, 8=2, 9=1, 10=3, 11=5}

