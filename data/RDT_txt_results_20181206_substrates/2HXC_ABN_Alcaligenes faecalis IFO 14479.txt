
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C=O:1.0]

//
FINGERPRINTS RC
[N:3.0, O:3.0, O=CC([CH])=[CH]:1.0, O=[CH]:1.0, O>>O=CC([CH])=[CH]:1.0, O>>O=[CH]:1.0, O>>[C]C=O:1.0, [CH2]:1.0, [CH2]N:1.0, [CH2]N>>N:1.0, [CH]:1.0, [CH]C(=[CH])CN:1.0, [CH]C(=[CH])CN>>N:1.0, [CH]C(=[CH])CN>>O=CC([CH])=[CH]:1.0, [CH]C=C(C=[CH])CN>>O=CC(=C[CH])C=[CH]:1.0, [C]C=O:2.0, [C]CN:2.0, [C]CN>>N:1.0, [C]CN>>[C]C=O:1.0, [NH2]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (6)
[N:1.0, O:1.0, [CH2]:1.0, [CH]:1.0, [NH2]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (6)
[N:1.0, O:1.0, O=[CH]:1.0, [CH2]N:1.0, [C]C=O:1.0, [C]CN:1.0]


ID=Reaction Center at Level: 2 (6)
[N:1.0, O:1.0, O=CC([CH])=[CH]:1.0, [CH]C(=[CH])CN:1.0, [C]C=O:1.0, [C]CN:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH2]N>>N
2: O>>O=[CH]
3: [C]CN>>[C]C=O

MMP Level 2
1: [C]CN>>N
2: O>>[C]C=O
3: [CH]C(=[CH])CN>>O=CC([CH])=[CH]

MMP Level 3
1: [CH]C(=[CH])CN>>N
2: O>>O=CC([CH])=[CH]
3: [CH]C=C(C=[CH])CN>>O=CC(=C[CH])C=[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O>>O=Cc1ccccc1]
2: R:M00002, P:M00003	[NCc1ccccc1>>N]
3: R:M00002, P:M00004	[NCc1ccccc1>>O=Cc1ccccc1]


//
SELECTED AAM MAPPING
[OH2:9].[NH2:1][CH2:2][C:3]:1:[CH:4]:[CH:5]:[CH:6]:[CH:7]:[CH:8]1>>[O:9]=[CH:2][C:3]:1:[CH:4]:[CH:5]:[CH:6]:[CH:7]:[CH:8]1.[NH3:1]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=6, 3=5, 4=4, 5=3, 6=8, 7=7, 8=2, 9=1}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=9, 2=6, 3=5, 4=4, 5=3, 6=8, 7=7, 8=2, 9=1}

