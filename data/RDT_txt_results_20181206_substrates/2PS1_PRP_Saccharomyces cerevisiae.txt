
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O=P(O)(O)O[P]>>O=P(O)(O[P])OC1O[CH][CH]C1O:1.0, [CH]:2.0, [C]C(=[CH])N(C(=O)[NH])C([O])[CH]:1.0, [C]C(=[CH])N(C(=O)[NH])C1OC([CH2])C(O)C1O>>[O]P(=O)(O)OC1OC([CH2])C(O)C1O:1.0, [C]N([C])C1O[CH][CH]C1O:1.0, [C]N([C])C1O[CH][CH]C1O>>[P]OC1O[CH][CH]C1O:1.0, [C]N([C])[CH]:1.0, [N]:1.0, [N]C([O])[CH]:1.0, [N]C([O])[CH]>>[O]C([O])[CH]:1.0, [OH]:1.0, [O]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [O]C([O])[CH]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>[O]C([CH])OP([O])(=O)O:1.0, [P]O:1.0, [P]O>>[P]O[CH]:1.0, [P]OC1O[CH][CH]C1O:1.0, [P]O[CH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [N]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[C]N([C])[CH]:1.0, [N]C([O])[CH]:1.0, [O]C([O])[CH]:1.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]C(=[CH])N(C(=O)[NH])C([O])[CH]:1.0, [C]N([C])C1O[CH][CH]C1O:1.0, [O]C([CH])OP([O])(=O)O:1.0, [P]OC1O[CH][CH]C1O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O>>[P]O[CH]
2: [N]C([O])[CH]>>[O]C([O])[CH]

MMP Level 2
1: [O]P(=O)(O)O>>[O]C([CH])OP([O])(=O)O
2: [C]N([C])C1O[CH][CH]C1O>>[P]OC1O[CH][CH]C1O

MMP Level 3
1: O=P(O)(O)O[P]>>O=P(O)(O[P])OC1O[CH][CH]C1O
2: [C]C(=[CH])N(C(=O)[NH])C1OC([CH2])C(O)C1O>>[O]P(=O)(O)OC1OC([CH2])C(O)C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1cc(C(=O)O)n(c(=O)[nH]1)C2OC(COP(=O)(O)O)C(O)C2O>>O=P(O)(O)OCC1OC(OP(=O)(O)OP(=O)(O)O)C(O)C1O]
2: R:M00002, P:M00003	[O=P(O)(O)OP(=O)(O)O>>O=P(O)(O)OCC1OC(OP(=O)(O)OP(=O)(O)O)C(O)C1O]


//
SELECTED AAM MAPPING
[O:23]=[C:24]([OH:25])[C:26]1=[CH:27][C:28](=[O:29])[NH:30][C:31](=[O:32])[N:33]1[CH:1]2[O:2][CH:3]([CH2:4][O:5][P:6](=[O:7])([OH:8])[OH:9])[CH:10]([OH:11])[CH:12]2[OH:13].[O:14]=[P:15]([OH:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[OH:22]>>[O:7]=[P:6]([OH:9])([OH:8])[O:5][CH2:4][CH:3]1[O:2][CH:1]([O:16][P:15](=[O:14])([OH:17])[O:18][P:19](=[O:20])([OH:21])[OH:22])[CH:12]([OH:13])[CH:10]1[OH:11]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=5, 2=4, 3=11, 4=9, 5=10, 6=8, 7=6, 8=7, 9=12, 10=23, 11=21, 12=14, 13=13, 14=15, 15=16, 16=17, 17=18, 18=19, 19=20, 20=22, 21=24, 22=2, 23=1, 24=3, 25=27, 26=26, 27=25, 28=28, 29=29, 30=30, 31=31, 32=32, 33=33}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=21, 4=19, 5=9, 6=8, 7=10, 8=11, 9=12, 10=13, 11=14, 12=15, 13=16, 14=17, 15=18, 16=20, 17=22, 18=5, 19=2, 20=1, 21=3, 22=4}

