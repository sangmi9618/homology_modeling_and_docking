
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O=P(O)(O)O:1.0, O=P(O)(O)O>>O=P(O)(O)OC1O[CH][CH]C1O:1.0, O=P(O)(O)O>>[O]C([CH])OP(=O)(O)O:1.0, [CH]:2.0, [C]C1=[C]N=CN1:1.0, [C]C1=[C][N]C=N1:1.0, [C]C1=[C][N]C=N1>>[C]C1=[C]N=CN1:1.0, [C]N([CH])C1O[CH][CH]C1O:1.0, [C]N([CH])C1O[CH][CH]C1O>>[P]OC1O[CH][CH]C1O:1.0, [C]N([CH])[CH]:1.0, [C]N([CH])[CH]>>[C]N=[CH]:1.0, [C]N=[CH]:2.0, [C]N=[CH]>>[C]N[CH]:1.0, [C]N[CH]:1.0, [C]c1ncn(c1N=[CH])C2O[CH][CH]C2O>>[C]c1[nH]cnc1N=[CH]:1.0, [NH]:1.0, [N]:3.0, [N]C([O])[CH]:1.0, [N]C([O])[CH]>>[O]C([O])[CH]:1.0, [N]C1=[C]N=CN1C([O])[CH]:1.0, [N]C1=[C]N=CN1C([O])[CH]>>[N]C1=[C]NC=N1:1.0, [N]C1=[C]N=CN1C2OC([CH2])C(O)C2O>>O=P(O)(O)OC1OC([CH2])C(O)C1O:1.0, [N]C1=[C]NC=N1:1.0, [N]c1c(ncn1[CH])C(=[N])N>>[N]c1nc[nH]c1C(=[N])N:1.0, [OH]:1.0, [O]:1.0, [O]C([CH])OP(=O)(O)O:1.0, [O]C([O])[CH]:1.0, [P]O:1.0, [P]O>>[P]O[CH]:1.0, [P]OC1O[CH][CH]C1O:1.0, [P]O[CH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [N]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[C]N([CH])[CH]:1.0, [N]C([O])[CH]:1.0, [O]C([O])[CH]:1.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]N([CH])C1O[CH][CH]C1O:1.0, [N]C1=[C]N=CN1C([O])[CH]:1.0, [O]C([CH])OP(=O)(O)O:1.0, [P]OC1O[CH][CH]C1O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]N=[CH]>>[C]N[CH]
2: [P]O>>[P]O[CH]
3: [N]C([O])[CH]>>[O]C([O])[CH]
4: [C]N([CH])[CH]>>[C]N=[CH]

MMP Level 2
1: [C]C1=[C][N]C=N1>>[C]C1=[C]N=CN1
2: O=P(O)(O)O>>[O]C([CH])OP(=O)(O)O
3: [C]N([CH])C1O[CH][CH]C1O>>[P]OC1O[CH][CH]C1O
4: [N]C1=[C]N=CN1C([O])[CH]>>[N]C1=[C]NC=N1

MMP Level 3
1: [N]c1c(ncn1[CH])C(=[N])N>>[N]c1nc[nH]c1C(=[N])N
2: O=P(O)(O)O>>O=P(O)(O)OC1O[CH][CH]C1O
3: [N]C1=[C]N=CN1C2OC([CH2])C(O)C2O>>O=P(O)(O)OC1OC([CH2])C(O)C1O
4: [C]c1ncn(c1N=[CH])C2O[CH][CH]C2O>>[C]c1[nH]cnc1N=[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[OC1C(O)C(OC1n2cnc3c(ncnc32)N)CSC>>n1cnc(N)c2[nH]cnc12, OC1C(O)C(OC1n2cnc3c(ncnc32)N)CSC>>n1cnc(N)c2[nH]cnc12]
2: R:M00001, P:M00004	[OC1C(O)C(OC1n2cnc3c(ncnc32)N)CSC>>O=P(O)(O)OC1OC(CSC)C(O)C1O]
3: R:M00002, P:M00004	[O=P(O)(O)O>>O=P(O)(O)OC1OC(CSC)C(O)C1O]


//
SELECTED AAM MAPPING
[O:21]=[P:22]([OH:23])([OH:24])[OH:25].[OH:1][CH:2]1[CH:3]([OH:4])[CH:5]([O:6][CH:7]1[N:8]:2:[CH:9]:[N:10]:[C:11]:3:[C:12](:[N:13]:[CH:14]:[N:15]:[C:16]32)[NH2:17])[CH2:18][S:19][CH3:20]>>[O:21]=[P:22]([OH:23])([OH:24])[O:25][CH:7]1[O:6][CH:5]([CH2:18][S:19][CH3:20])[CH:3]([OH:4])[CH:2]1[OH:1].[N:15]:1:[CH:14]:[N:13]:[C:12]([NH2:17]):[C:11]:2:[NH:10]:[CH:9]:[N:8]:[C:16]12


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=20, 2=19, 3=18, 4=5, 5=3, 6=2, 7=7, 8=6, 9=8, 10=9, 11=10, 12=11, 13=16, 14=15, 15=14, 16=13, 17=12, 18=17, 19=1, 20=4, 21=23, 22=22, 23=21, 24=24, 25=25}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=23, 2=24, 3=25, 4=21, 5=22, 6=19, 7=18, 8=17, 9=16, 10=20, 11=11, 12=10, 13=9, 14=8, 15=12, 16=14, 17=6, 18=7, 19=5, 20=2, 21=1, 22=3, 23=4, 24=15, 25=13}

