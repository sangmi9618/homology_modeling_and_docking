
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%C:1.0, C%O:2.0, C-C:1.0, C-O:2.0]

STEREO_CHANGED
[C(R/S):4.0]

//
FINGERPRINTS RC
[O:3.0, O=C(NC1OC([CH2])[CH]C(O)C1O)C>>O=C(NC1C(O)O[CH]C(O)C1O)C:1.0, O>>[CH]O:1.0, O>>[CH]OC(O)C([CH])[NH]:1.0, O>>[O]C([CH])O:1.0, O[CH2]:1.0, [CH2]:2.0, [CH]:6.0, [CH]C(=[CH])O:1.0, [CH]C(=[CH])OC[CH]:1.0, [CH]C(=[CH])OC[CH]>>[CH]C(=[CH])O:1.0, [CH]C(O)C(O)C([CH])O:1.0, [CH]C(O)C(O)C([CH])O>>[O]C([CH2])C(O)C([CH])O:1.0, [CH]C(O)OC([CH])[CH2]:1.0, [CH]C([CH])O:3.0, [CH]C([CH])O>>[CH]C(O)OC([CH])[CH2]:1.0, [CH]C([CH])O>>[CH]C([CH])O:1.0, [CH]C([CH])[NH]:1.0, [CH]C([NH])OC([CH])[CH2]:1.0, [CH]C([NH])OC([CH])[CH2]>>[CH]CO:1.0, [CH]CO:2.0, [CH]O:2.0, [CH]O>>[CH]O[CH]:1.0, [CH]OC(COC([CH])=[CH])C([CH])O>>[C]NC1C(O)OC([CH2])[CH]C1O:1.0, [CH]OC(O)C([CH])[NH]:1.0, [CH]O[CH]:2.0, [CH]O[CH]>>O[CH2]:1.0, [C]NC(C([O])O)C([CH])O:1.0, [C]NC(O[CH])C([CH])O:1.0, [C]NC(O[CH])C([CH])O>>[C]NC(C([O])O)C([CH])O:1.0, [C]NC1OC(C[O])C(O)[CH]C1O>>[O]C([CH])CO:1.0, [C]O:1.0, [C]OCC([O])[CH]:1.0, [C]OCC([O])[CH]>>[CH]OC(O)C([CH])[NH]:1.0, [C]OCC1OC([NH])[CH]C(O)C1O>>[CH]OC(CO)C([CH])O:1.0, [C]O[CH2]:1.0, [C]O[CH2]>>[C]O:1.0, [NH]C1OC([CH2])C(O)C(O)C1O>>[NH]C1[CH]OC(CO)C(O)C1O:1.0, [OH]:4.0, [O]:3.0, [O]C([CH2])C(O)C([CH])O:1.0, [O]C([CH2])C(O)C([CH])O>>[NH]C1[CH]C(O)C(OC1O)CO:1.0, [O]C([CH])CO:1.0, [O]C([CH])COC(=C[CH])C=[CH]>>[CH]C=C(O)C=[CH]:1.0, [O]C([CH])O:2.0, [O]C([CH])[CH2]:1.0, [O]C([CH])[CH2]>>[CH]CO:1.0, [O]C([CH])[NH]:1.0, [O]C([CH])[NH]>>[CH]C([CH])[NH]:1.0, [O]CC(O[CH])C([CH])O:1.0, [O]CC(O[CH])C([CH])O>>[O]C([CH])CO:1.0, [O]C[CH]:1.0, [O]C[CH]>>[O]C([CH])O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[O:1.0, [CH2]:2.0, [CH]:6.0, [OH]:1.0, [O]:3.0]


ID=Reaction Center at Level: 1 (9)
[O:1.0, [CH]C([CH])[NH]:1.0, [CH]O:1.0, [CH]O[CH]:2.0, [C]O[CH2]:1.0, [O]C([CH])O:3.0, [O]C([CH])[CH2]:1.0, [O]C([CH])[NH]:1.0, [O]C[CH]:2.0]


ID=Reaction Center at Level: 2 (10)
[O:1.0, [CH]C(=[CH])OC[CH]:1.0, [CH]C(O)OC([CH])[CH2]:1.0, [CH]C([NH])OC([CH])[CH2]:1.0, [CH]OC(O)C([CH])[NH]:3.0, [C]NC(C([O])O)C([CH])O:1.0, [C]NC(O[CH])C([CH])O:1.0, [C]OCC([O])[CH]:2.0, [O]C([CH])O:1.0, [O]CC(O[CH])C([CH])O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH2]:2.0, [CH]:6.0]


ID=Reaction Center at Level: 1 (7)
[[CH]C([CH])O:2.0, [CH]C([CH])[NH]:1.0, [CH]CO:1.0, [O]C([CH])O:1.0, [O]C([CH])[CH2]:1.0, [O]C([CH])[NH]:1.0, [O]C[CH]:1.0]


ID=Reaction Center at Level: 2 (8)
[[CH]C(O)C(O)C([CH])O:1.0, [CH]OC(O)C([CH])[NH]:1.0, [C]NC(C([O])O)C([CH])O:1.0, [C]NC(O[CH])C([CH])O:1.0, [C]OCC([O])[CH]:1.0, [O]C([CH2])C(O)C([CH])O:1.0, [O]C([CH])CO:1.0, [O]CC(O[CH])C([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [O]C[CH]>>[O]C([CH])O
2: [C]O[CH2]>>[C]O
3: O>>[CH]O
4: [CH]O>>[CH]O[CH]
5: [O]C([CH])[NH]>>[CH]C([CH])[NH]
6: [O]C([CH])[CH2]>>[CH]CO
7: [CH]C([CH])O>>[CH]C([CH])O
8: [CH]O[CH]>>O[CH2]

MMP Level 2
1: [C]OCC([O])[CH]>>[CH]OC(O)C([CH])[NH]
2: [CH]C(=[CH])OC[CH]>>[CH]C(=[CH])O
3: O>>[O]C([CH])O
4: [CH]C([CH])O>>[CH]C(O)OC([CH])[CH2]
5: [C]NC(O[CH])C([CH])O>>[C]NC(C([O])O)C([CH])O
6: [O]CC(O[CH])C([CH])O>>[O]C([CH])CO
7: [CH]C(O)C(O)C([CH])O>>[O]C([CH2])C(O)C([CH])O
8: [CH]C([NH])OC([CH])[CH2]>>[CH]CO

MMP Level 3
1: [CH]OC(COC([CH])=[CH])C([CH])O>>[C]NC1C(O)OC([CH2])[CH]C1O
2: [O]C([CH])COC(=C[CH])C=[CH]>>[CH]C=C(O)C=[CH]
3: O>>[CH]OC(O)C([CH])[NH]
4: [O]C([CH2])C(O)C([CH])O>>[NH]C1[CH]C(O)C(OC1O)CO
5: O=C(NC1OC([CH2])[CH]C(O)C1O)C>>O=C(NC1C(O)O[CH]C(O)C1O)C
6: [C]OCC1OC([NH])[CH]C(O)C1O>>[CH]OC(CO)C([CH])O
7: [NH]C1OC([CH2])C(O)C(O)C1O>>[NH]C1[CH]OC(CO)C(O)C1O
8: [C]NC1OC(C[O])C(O)[CH]C1O>>[O]C([CH])CO


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[[O-][N+](=O)c1ccc(OCC2OC(NC(=O)C)C(O)C(O)C2O)cc1>>[O-][N+](=O)c1ccc(O)cc1]
2: R:M00001, P:M00004	[[O-][N+](=O)c1ccc(OCC2OC(NC(=O)C)C(O)C(O)C2O)cc1>>O=C(NC1C(O)OC(CO)C(O)C1O)C, [O-][N+](=O)c1ccc(OCC2OC(NC(=O)C)C(O)C(O)C2O)cc1>>O=C(NC1C(O)OC(CO)C(O)C1O)C, [O-][N+](=O)c1ccc(OCC2OC(NC(=O)C)C(O)C(O)C2O)cc1>>O=C(NC1C(O)OC(CO)C(O)C1O)C, [O-][N+](=O)c1ccc(OCC2OC(NC(=O)C)C(O)C(O)C2O)cc1>>O=C(NC1C(O)OC(CO)C(O)C1O)C, [O-][N+](=O)c1ccc(OCC2OC(NC(=O)C)C(O)C(O)C2O)cc1>>O=C(NC1C(O)OC(CO)C(O)C1O)C, [O-][N+](=O)c1ccc(OCC2OC(NC(=O)C)C(O)C(O)C2O)cc1>>O=C(NC1C(O)OC(CO)C(O)C1O)C]
3: R:M00002, P:M00004	[O>>O=C(NC1C(O)OC(CO)C(O)C1O)C]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([NH:3][CH:4]1[O:5][CH:6]([CH2:7][O:8][C:9]:2:[CH:10]:[CH:11]:[C:12](:[CH:13]:[CH:14]2)[N+:15](=[O:16])[O-:17])[CH:18]([OH:19])[CH:20]([OH:21])[CH:22]1[OH:23])[CH3:24].[OH2:25]>>[O:1]=[C:2]([NH:3][CH:4]1[CH:7]([OH:25])[O:19][CH:18]([CH2:6][OH:5])[CH:20]([OH:21])[CH:22]1[OH:23])[CH3:24].[O:16]=[N+:15]([O-:17])[C:12]:1:[CH:11]:[CH:10]:[C:9]([OH:8]):[CH:14]:[CH:13]1


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=2, 3=1, 4=3, 5=4, 6=22, 7=20, 8=18, 9=6, 10=5, 11=7, 12=8, 13=9, 14=10, 15=11, 16=12, 17=13, 18=14, 19=15, 20=16, 21=17, 22=19, 23=21, 24=23, 25=25}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=24, 5=25, 6=19, 7=17, 8=16, 9=18, 10=23, 11=15, 12=2, 13=1, 14=3, 15=4, 16=13, 17=11, 18=8, 19=7, 20=5, 21=6, 22=9, 23=10, 24=12, 25=14}

