
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

//
FINGERPRINTS RC
[O=C(O)C(=O)O:2.0, O=C(O)C(=O)O>>O=C=O:3.0, O=C(O)C(=O)O>>O=CO:2.0, O=C=O:3.0, O=CO:2.0, [CH]:1.0, [C]:3.0, [C]=O:1.0, [C]C(=O)O:3.0, [C]C(=O)O>>O=C=O:2.0, [C]C(=O)O>>O=CO:1.0, [C]O:1.0, [C]O>>[C]=O:1.0, [OH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (1)
[[C]:2.0]


ID=Reaction Center at Level: 1 (1)
[[C]C(=O)O:2.0]


ID=Reaction Center at Level: 2 (1)
[O=C(O)C(=O)O:2.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C=O:1.0, [C]=O:1.0, [C]C(=O)O:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (3)
[O=C(O)C(=O)O:1.0, O=C=O:2.0, [C]C(=O)O:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C(=O)O>>O=C=O
2: [C]O>>[C]=O
3: [C]C(=O)O>>O=CO

MMP Level 2
1: O=C(O)C(=O)O>>O=C=O
2: [C]C(=O)O>>O=C=O
3: O=C(O)C(=O)O>>O=CO

MMP Level 3
1: O=C(O)C(=O)O>>O=C=O
2: O=C(O)C(=O)O>>O=C=O
3: O=C(O)C(=O)O>>O=CO


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(=O)O>>O=CO]
2: R:M00001, P:M00004	[O=C(O)C(=O)O>>O=C=O, O=C(O)C(=O)O>>O=C=O]


//
SELECTED AAM MAPPING
[H+:7].[O:1]=[C:2]([OH:3])[C:4](=[O:5])[OH:6]>>[O:1]=[C:2]=[O:3].[O:5]=[CH:4][OH:6]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=2, 2=1, 3=4, 4=5, 5=6, 6=3, 7=7}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=5, 2=4, 3=6, 4=2, 5=1, 6=3}

