
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:2.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O=P(O)(O)OC1C(O)C(O)[CH]C(O)C1O>>[O]C1[CH]C(O)C(O)C(OP(=O)(O)O)C1O:1.0, O=P(O)(O[P])OC(O[CH])C([CH])[NH]>>O=P(O)(O)O[P]:1.0, OC1[CH]C(O)C(O)C(O)C1O>>[CH]OC1C(O)[CH]C(O)C(O)C1O:1.0, [CH]:6.0, [CH]C(O)C(O)C([CH])O:1.0, [CH]C(O)C(O)C([CH])O>>[CH]OC(OC(C([CH])O)C([CH])O)C([CH])[NH]:1.0, [CH]C(O)C(O)C([CH])O>>[O]C([CH])C(O)C([CH])O:1.0, [CH]C([CH])O:3.0, [CH]C([CH])O>>[CH]C([CH])O:1.0, [CH]C([CH])O>>[O]C([CH])OC([CH])[CH]:1.0, [CH]O:1.0, [CH]O>>[CH]O[CH]:1.0, [CH]OC(O[CH])C([CH])[NH]:1.0, [CH]O[CH]:1.0, [C]NC1C(O)[CH]C(OC1OP([O])(=O)O)[CH2]>>[C]NC1C(OC([CH2])[CH]C1O)OC([CH])[CH]:1.0, [OH]:2.0, [O]:2.0, [O]C([CH])C(O)C([CH])O:1.0, [O]C([CH])OC([CH])[CH]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]C([CH])[CH]:2.0, [O]C([CH])[CH]>>[O]C([CH])[CH]:1.0, [O]C([O])[CH]:2.0, [O]C([O])[CH]>>[O]C([O])[CH]:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]OC(C([CH])O)C([CH])O:2.0, [P]OC(C([CH])O)C([CH])O>>[P]OC(C([CH])O)C([CH])O:1.0, [P]OC(O[CH])C([CH])[NH]:1.0, [P]OC(O[CH])C([CH])[NH]>>[CH]OC(O[CH])C([CH])[NH]:1.0, [P]O[CH]:1.0, [P]O[CH]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (3)
[[CH]O[CH]:1.0, [O]C([O])[CH]:2.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[CH]OC(O[CH])C([CH])[NH]:1.0, [O]C([CH])OC([CH])[CH]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [P]OC(O[CH])C([CH])[NH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:4.0]


ID=Reaction Center at Level: 1 (2)
[[CH]C([CH])O:2.0, [O]C([CH])[CH]:2.0]


ID=Reaction Center at Level: 2 (3)
[[CH]C(O)C(O)C([CH])O:1.0, [O]C([CH])C(O)C([CH])O:1.0, [P]OC(C([CH])O)C([CH])O:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]O>>[CH]O[CH]
2: [O]C([CH])[CH]>>[O]C([CH])[CH]
3: [O]C([O])[CH]>>[O]C([O])[CH]
4: [P]O[CH]>>[P]O
5: [CH]C([CH])O>>[CH]C([CH])O

MMP Level 2
1: [CH]C([CH])O>>[O]C([CH])OC([CH])[CH]
2: [P]OC(C([CH])O)C([CH])O>>[P]OC(C([CH])O)C([CH])O
3: [P]OC(O[CH])C([CH])[NH]>>[CH]OC(O[CH])C([CH])[NH]
4: [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O
5: [CH]C(O)C(O)C([CH])O>>[O]C([CH])C(O)C([CH])O

MMP Level 3
1: [CH]C(O)C(O)C([CH])O>>[CH]OC(OC(C([CH])O)C([CH])O)C([CH])[NH]
2: O=P(O)(O)OC1C(O)C(O)[CH]C(O)C1O>>[O]C1[CH]C(O)C(O)C(OP(=O)(O)O)C1O
3: [C]NC1C(O)[CH]C(OC1OP([O])(=O)O)[CH2]>>[C]NC1C(OC([CH2])[CH]C1O)OC([CH])[CH]
4: O=P(O)(O[P])OC(O[CH])C([CH])[NH]>>O=P(O)(O)O[P]
5: OC1[CH]C(O)C(O)C(O)C1O>>[CH]OC1C(O)[CH]C(O)C(O)C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3NC(=O)C)C(O)C2O>>O=C(NC1C(O)C(O)C(OC1OC2C(O)C(O)C(O)C(OP(=O)(O)O)C2O)CO)C]
2: R:M00001, P:M00004	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3NC(=O)C)C(O)C2O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)O)C(O)C2O]
3: R:M00002, P:M00003	[O=P(O)(O)OC1C(O)C(O)C(O)C(O)C1O>>O=C(NC1C(O)C(O)C(OC1OC2C(O)C(O)C(O)C(OP(=O)(O)O)C2O)CO)C, O=P(O)(O)OC1C(O)C(O)C(O)C(O)C1O>>O=C(NC1C(O)C(O)C(OC1OC2C(O)C(O)C(O)C(OP(=O)(O)O)C2O)CO)C, O=P(O)(O)OC1C(O)C(O)C(O)C(O)C1O>>O=C(NC1C(O)C(O)C(OC1OC2C(O)C(O)C(O)C(OP(=O)(O)O)C2O)CO)C]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][CH:22]3[O:23][CH:24]([CH2:25][OH:26])[CH:27]([OH:28])[CH:29]([OH:30])[CH:31]3[NH:32][C:33](=[O:34])[CH3:35])[CH:36]([OH:37])[CH:38]2[OH:39].[O:40]=[P:41]([OH:42])([OH:43])[O:44][CH:45]1[CH:46]([OH:47])[CH:48]([OH:49])[CH:50]([OH:51])[CH:52]([OH:53])[CH:54]1[OH:55]>>[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[OH:21])[CH:36]([OH:37])[CH:38]2[OH:39].[O:34]=[C:33]([NH:32][CH:31]1[CH:29]([OH:30])[CH:27]([OH:28])[CH:24]([O:23][CH:22]1[O:49][CH:48]2[CH:50]([OH:51])[CH:52]([OH:53])[CH:54]([OH:55])[CH:45]([O:44][P:41](=[O:40])([OH:42])[OH:43])[CH:46]2[OH:47])[CH2:25][OH:26])[CH3:35]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=35, 2=33, 3=34, 4=32, 5=31, 6=29, 7=27, 8=24, 9=23, 10=22, 11=21, 12=18, 13=19, 14=20, 15=17, 16=14, 17=15, 18=16, 19=13, 20=12, 21=11, 22=36, 23=38, 24=9, 25=10, 26=5, 27=4, 28=3, 29=2, 30=1, 31=8, 32=6, 33=7, 34=39, 35=37, 36=25, 37=26, 38=28, 39=30, 40=52, 41=54, 42=45, 43=46, 44=48, 45=50, 46=51, 47=49, 48=47, 49=44, 50=41, 51=40, 52=42, 53=43, 54=55, 55=53}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=30, 2=2, 3=1, 4=3, 5=4, 6=5, 7=7, 8=9, 9=10, 10=11, 11=12, 12=13, 13=14, 14=16, 15=18, 16=20, 17=26, 18=27, 19=21, 20=22, 21=23, 22=24, 23=25, 24=19, 25=17, 26=15, 27=28, 28=29, 29=8, 30=6, 31=33, 32=34, 33=35, 34=36, 35=37, 36=38, 37=32, 38=31, 39=39, 40=54, 41=52, 42=41, 43=40, 44=42, 45=43, 46=44, 47=45, 48=46, 49=47, 50=48, 51=49, 52=50, 53=51, 54=53, 55=55}

