
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0, O-P:1.0, O=O:1.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O:3.0, O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1:1.0, O=O:4.0, O=O>>O:3.0, O=O>>OCC([CH2])C:1.0, O=O>>O[CH2]:1.0, O=O>>[CH]CO:1.0, O=P(O)(O)OC([CH])[CH]:1.0, O=P(O)(O)OC([CH])[CH]>>[CH]C([CH])O:1.0, O=P(O)(O)O[CH]:1.0, OC1[CH]OC([CH2])C1O:1.0, OCC(C)C[CH2]:1.0, OCC([CH2])C:1.0, O[CH2]:1.0, [CH2]:2.0, [CH2]C(C)C:2.0, [CH2]C(C)C>>OCC([CH2])C:1.0, [CH2]C(C)C>>[CH2]C([CH2])C:1.0, [CH2]C([CH2])C:1.0, [CH2]CC(C)C:1.0, [CH2]CC(C)C>>OCC(C)C[CH2]:2.0, [CH2]CCC(C)C>>OCC(C)CC[CH2]:1.0, [CH3]:1.0, [CH]:5.0, [CH]C:1.0, [CH]C([CH])O:3.0, [CH]C([CH])O>>[CH]C([CH])O:1.0, [CH]C>>[CH]CO:1.0, [CH]CO:2.0, [CH]N([CH])[CH]:1.0, [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]:1.0, [CH]O:1.0, [CH][N+]([CH])=[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]:1.0, [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O:1.0, [C]C=[CH]:1.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C=[CH]:1.0, [N+]:1.0, [N]:1.0, [N]C1OC(C[O])C(O)C1O[P]>>[N]C1OC(C[O])C(O)C1O:1.0, [N]C1O[CH]C(O)C1OP(=O)(O)O>>[N]C1O[CH]C(O)C1O:1.0, [OH]:2.0, [O]:3.0, [O]C1[CH]OC([CH2])C1O:1.0, [O]C1[CH]OC([CH2])C1O>>OC1[CH]OC([CH2])C1O:1.0, [O]P(=O)(O)O:1.0, [P]:1.0, [P]O[CH]:1.0, [P]O[CH]>>[CH]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[O:1.0, [CH2]:1.0, [OH]:1.0, [O]:3.0, [P]:1.0]


ID=Reaction Center at Level: 1 (6)
[O:1.0, O=O:2.0, O[CH2]:1.0, [CH]CO:1.0, [O]P(=O)(O)O:1.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (6)
[O:1.0, O=O:2.0, O=P(O)(O)OC([CH])[CH]:1.0, O=P(O)(O)O[CH]:1.0, OCC([CH2])C:1.0, [CH]CO:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:4.0]


ID=Reaction Center at Level: 1 (3)
[[CH2]C(C)C:1.0, [CH2]C([CH2])C:1.0, [CH]C([CH])O:2.0]


ID=Reaction Center at Level: 2 (4)
[OC1[CH]OC([CH2])C1O:1.0, OCC(C)C[CH2]:1.0, [CH2]CC(C)C:1.0, [O]C1[CH]OC([CH2])C1O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C[CH]>>[C]C=[CH]
2: [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]
3: [CH]C([CH])O>>[CH]C([CH])O
4: [CH]C>>[CH]CO
5: O=O>>O[CH2]
6: [CH2]C(C)C>>[CH2]C([CH2])C
7: O=O>>O
8: [P]O[CH]>>[CH]O

MMP Level 2
1: [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]
2: [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]
3: [O]C1[CH]OC([CH2])C1O>>OC1[CH]OC([CH2])C1O
4: [CH2]C(C)C>>OCC([CH2])C
5: O=O>>[CH]CO
6: [CH2]CC(C)C>>OCC(C)C[CH2]
7: O=O>>O
8: O=P(O)(O)OC([CH])[CH]>>[CH]C([CH])O

MMP Level 3
1: O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1
2: [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O
3: [N]C1OC(C[O])C(O)C1O[P]>>[N]C1OC(C[O])C(O)C1O
4: [CH2]CC(C)C>>OCC(C)C[CH2]
5: O=O>>OCC([CH2])C
6: [CH2]CCC(C)C>>OCC(C)CC[CH2]
7: O=O>>O
8: [N]C1O[CH]C(O)C1OP(=O)(O)O>>[N]C1O[CH]C(O)C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00005	[O=C1C=C2CCC3C(CCC4(C)C3CCC4C(C)CCCC(C)C)C2(C)CC1>>O=C1C=C2CCC3C(CCC4(C)C3CCC4C(C)CCCC(C)CO)C2(C)CC1, O=C1C=C2CCC3C(CCC4(C)C3CCC4C(C)CCCC(C)C)C2(C)CC1>>O=C1C=C2CCC3C(CCC4(C)C3CCC4C(C)CCCC(C)CO)C2(C)CC1]
2: R:M00002, P:M00005	[O=O>>O=C1C=C2CCC3C(CCC4(C)C3CCC4C(C)CCCC(C)CO)C2(C)CC1]
3: R:M00002, P:M00006	[O=O>>O]
4: R:M00003, P:M00007	[O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O]


//
SELECTED AAM MAPPING
[H+:79].[O:73]=[O:74].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:5][N:6]([CH:7]=[CH:8][CH2:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:75](=[O:76])([OH:77])[OH:78])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44].[O:45]=[C:46]1[CH:47]=[C:48]2[CH2:49][CH2:50][CH:51]3[CH:52]([CH2:53][CH2:54][C:55]4([CH3:56])[CH:57]([CH2:58][CH2:59][CH:60]34)[CH:61]([CH3:62])[CH2:63][CH2:64][CH2:65][CH:66]([CH3:67])[CH3:68])[C:69]2([CH3:70])[CH2:71][CH2:72]1>>[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:9]:[CH:8]:[CH:7]:[N+:6](:[CH:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44].[O:45]=[C:46]1[CH:47]=[C:48]2[CH2:49][CH2:50][CH:51]3[CH:52]([CH2:53][CH2:54][C:55]4([CH3:56])[CH:57]([CH2:58][CH2:59][CH:60]34)[CH:61]([CH3:62])[CH2:63][CH2:64][CH2:65][CH:66]([CH3:67])[CH2:68][OH:73])[C:69]2([CH3:70])[CH2:71][CH2:72]1.[OH2:74]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=66, 2=65, 3=67, 4=68, 5=69, 6=70, 7=71, 8=72, 9=61, 10=62, 11=63, 12=64, 13=59, 14=58, 15=57, 16=56, 17=55, 18=54, 19=53, 20=52, 21=51, 22=50, 23=49, 24=76, 25=75, 26=73, 27=74, 28=60, 29=77, 30=78, 31=9, 32=8, 33=7, 34=6, 35=5, 36=4, 37=2, 38=1, 39=3, 40=10, 41=47, 42=45, 43=12, 44=11, 45=13, 46=14, 47=15, 48=16, 49=17, 50=18, 51=19, 52=20, 53=21, 54=22, 55=23, 56=24, 57=43, 58=37, 59=26, 60=25, 61=27, 62=28, 63=29, 64=30, 65=35, 66=34, 67=33, 68=32, 69=31, 70=36, 71=38, 72=39, 73=40, 74=41, 75=42, 76=44, 77=46, 78=48, 79=79}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=67, 2=66, 3=65, 4=64, 5=63, 6=61, 7=62, 8=57, 9=58, 10=59, 11=60, 12=55, 13=54, 14=53, 15=52, 16=51, 17=50, 18=49, 19=48, 20=47, 21=46, 22=45, 23=73, 24=72, 25=70, 26=71, 27=56, 28=68, 29=69, 30=74, 31=6, 32=5, 33=4, 34=9, 35=8, 36=7, 37=10, 38=43, 39=41, 40=12, 41=11, 42=13, 43=14, 44=15, 45=16, 46=17, 47=18, 48=19, 49=20, 50=21, 51=22, 52=23, 53=24, 54=39, 55=37, 56=26, 57=25, 58=27, 59=28, 60=29, 61=30, 62=35, 63=34, 64=33, 65=32, 66=31, 67=36, 68=38, 69=40, 70=42, 71=44, 72=2, 73=1, 74=3}

