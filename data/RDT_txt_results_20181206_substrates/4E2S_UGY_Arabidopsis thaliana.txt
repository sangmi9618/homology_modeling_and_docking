
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[N:3.0, O:3.0, O=C([CH])O:1.0, O>>[CH]O:1.0, O>>[C]C([NH])O:1.0, O>>[C]NC(O)C(=O)O:1.0, [CH]:2.0, [CH]O:1.0, [CH][NH3+]:1.0, [CH][NH3+]>>N:1.0, [C]C([NH])O:2.0, [C]C([NH])[NH3+]:2.0, [C]C([NH])[NH3+]>>N:1.0, [C]C([NH])[NH3+]>>[C]C([NH])O:1.0, [C]NC(C([O-])=O)[NH3+]:1.0, [C]NC(C([O-])=O)[NH3+]>>N:1.0, [C]NC(C([O-])=O)[NH3+]>>[C]NC(O)C(=O)O:1.0, [C]NC(O)C(=O)O:1.0, [C]O:1.0, [C][O-]:1.0, [C][O-]>>[C]O:1.0, [NH3+]:1.0, [O-]:1.0, [O-]C(=O)C(NC(=O)N)[NH3+]>>O=C(O)C(O)NC(=O)N:1.0, [O-]C(=O)C([NH])[NH3+]>>O=C(O)C([NH])O:1.0, [O-]C(=O)[CH]:1.0, [O-]C(=O)[CH]>>O=C([CH])O:1.0, [OH]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[N:1.0, O:1.0, [CH]:2.0, [NH3+]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (6)
[N:1.0, O:1.0, [CH]O:1.0, [CH][NH3+]:1.0, [C]C([NH])O:1.0, [C]C([NH])[NH3+]:1.0]


ID=Reaction Center at Level: 2 (6)
[N:1.0, O:1.0, [C]C([NH])O:1.0, [C]C([NH])[NH3+]:1.0, [C]NC(C([O-])=O)[NH3+]:1.0, [C]NC(O)C(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O>>[CH]O
2: [C]C([NH])[NH3+]>>[C]C([NH])O
3: [CH][NH3+]>>N
4: [C][O-]>>[C]O

MMP Level 2
1: O>>[C]C([NH])O
2: [C]NC(C([O-])=O)[NH3+]>>[C]NC(O)C(=O)O
3: [C]C([NH])[NH3+]>>N
4: [O-]C(=O)[CH]>>O=C([CH])O

MMP Level 3
1: O>>[C]NC(O)C(=O)O
2: [O-]C(=O)C(NC(=O)N)[NH3+]>>O=C(O)C(O)NC(=O)N
3: [C]NC(C([O-])=O)[NH3+]>>N
4: [O-]C(=O)C([NH])[NH3+]>>O=C(O)C([NH])O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[[O-]C(=O)C(NC(=O)N)[NH3+]>>O=C(O)C(O)NC(=O)N, [O-]C(=O)C(NC(=O)N)[NH3+]>>O=C(O)C(O)NC(=O)N]
2: R:M00001, P:M00004	[[O-]C(=O)C(NC(=O)N)[NH3+]>>N]
3: R:M00002, P:M00003	[O>>O=C(O)C(O)NC(=O)N]


//
SELECTED AAM MAPPING
[O:7]=[C:6]([O-:8])[CH:5]([NH:4][C:2](=[O:1])[NH2:3])[NH3+:9].[OH2:10]>>[O:7]=[C:6]([OH:8])[CH:5]([OH:10])[NH:4][C:2](=[O:1])[NH2:3].[NH3:9]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=5, 2=6, 3=7, 4=8, 5=9, 6=4, 7=2, 8=1, 9=3, 10=10}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=4, 2=2, 3=1, 4=3, 5=6, 6=7, 7=8, 8=9, 9=5, 10=10}

