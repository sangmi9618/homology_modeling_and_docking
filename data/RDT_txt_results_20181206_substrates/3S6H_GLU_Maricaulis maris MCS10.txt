
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-S:1.0]

//
FINGERPRINTS RC
[O=C(N[CH])C:1.0, O=C(O)C(N)C[CH2]>>O=C(O)C(NC(=O)C)C[CH2]:1.0, O=C(SCC[NH])C>>[NH]CCS:1.0, O=C(SC[CH2])C:1.0, O=C(SC[CH2])C>>SC[CH2]:1.0, O=C(SC[CH2])C>>[C]C([CH2])NC(=O)C:1.0, O=C(S[CH2])C:1.0, O=C(S[CH2])C>>O=C(N[CH])C:1.0, O=C([NH])C:1.0, O=C([S])C:1.0, O=C([S])C>>O=C([NH])C:1.0, SC[CH2]:1.0, S[CH2]:1.0, [CH]N:1.0, [CH]N>>[C]N[CH]:1.0, [C]:2.0, [C]C([CH2])N:1.0, [C]C([CH2])N>>[C]C([CH2])NC(=O)C:1.0, [C]C([CH2])NC(=O)C:1.0, [C]N[CH]:1.0, [C]S[CH2]:1.0, [C]S[CH2]>>S[CH2]:1.0, [NH2]:1.0, [NH]:1.0, [SH]:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [NH]:1.0, [S]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([NH])C:1.0, O=C([S])C:1.0, [C]N[CH]:1.0, [C]S[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(N[CH])C:1.0, O=C(SC[CH2])C:1.0, O=C(S[CH2])C:1.0, [C]C([CH2])NC(=O)C:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=C([S])C>>O=C([NH])C
2: [C]S[CH2]>>S[CH2]
3: [CH]N>>[C]N[CH]

MMP Level 2
1: O=C(S[CH2])C>>O=C(N[CH])C
2: O=C(SC[CH2])C>>SC[CH2]
3: [C]C([CH2])N>>[C]C([CH2])NC(=O)C

MMP Level 3
1: O=C(SC[CH2])C>>[C]C([CH2])NC(=O)C
2: O=C(SCC[NH])C>>[NH]CCS
3: O=C(O)C(N)C[CH2]>>O=C(O)C(NC(=O)C)C[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C>>O=C(NCCS)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O]
2: R:M00001, P:M00004	[O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C>>O=C(O)CCC(NC(=O)C)C(=O)O]
3: R:M00002, P:M00004	[O=C(O)CCC(N)C(=O)O>>O=C(O)CCC(NC(=O)C)C(=O)O]


//
SELECTED AAM MAPPING
[O:52]=[C:53]([OH:54])[CH2:55][CH2:56][CH:57]([NH2:58])[C:59](=[O:60])[OH:61].[O:1]=[C:2]([S:3][CH2:4][CH2:5][NH:6][C:7](=[O:8])[CH2:9][CH2:10][NH:11][C:12](=[O:13])[CH:14]([OH:15])[C:16]([CH3:17])([CH3:18])[CH2:19][O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[O:28][CH2:29][CH:30]1[O:31][CH:32]([N:33]:2:[CH:34]:[N:35]:[C:36]:3:[C:37](:[N:38]:[CH:39]:[N:40]:[C:41]32)[NH2:42])[CH:43]([OH:44])[CH:45]1[O:46][P:47](=[O:48])([OH:49])[OH:50])[CH3:51]>>[O:52]=[C:53]([OH:54])[CH2:55][CH2:56][CH:57]([NH:58][C:2](=[O:1])[CH3:51])[C:59](=[O:60])[OH:61].[O:8]=[C:7]([NH:6][CH2:5][CH2:4][SH:3])[CH2:9][CH2:10][NH:11][C:12](=[O:13])[CH:14]([OH:15])[C:16]([CH3:17])([CH3:18])[CH2:19][O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[O:28][CH2:29][CH:30]1[O:31][CH:32]([N:33]:2:[CH:34]:[N:35]:[C:36]:3:[C:37](:[N:38]:[CH:39]:[N:40]:[C:41]32)[NH2:42])[CH:43]([OH:44])[CH:45]1[O:46][P:47](=[O:48])([OH:49])[OH:50]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=51, 2=2, 3=1, 4=3, 5=4, 6=5, 7=6, 8=7, 9=8, 10=9, 11=10, 12=11, 13=12, 14=13, 15=14, 16=16, 17=17, 18=18, 19=19, 20=20, 21=21, 22=22, 23=23, 24=24, 25=25, 26=26, 27=27, 28=28, 29=29, 30=30, 31=45, 32=43, 33=32, 34=31, 35=33, 36=34, 37=35, 38=36, 39=41, 40=40, 41=39, 42=38, 43=37, 44=42, 45=44, 46=46, 47=47, 48=48, 49=49, 50=50, 51=15, 52=56, 53=55, 54=53, 55=52, 56=54, 57=57, 58=59, 59=60, 60=61, 61=58}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=15, 2=14, 3=16, 4=17, 5=18, 6=19, 7=20, 8=21, 9=22, 10=23, 11=24, 12=25, 13=26, 14=27, 15=28, 16=43, 17=41, 18=30, 19=29, 20=31, 21=32, 22=33, 23=34, 24=39, 25=38, 26=37, 27=36, 28=35, 29=40, 30=42, 31=44, 32=45, 33=46, 34=47, 35=48, 36=12, 37=10, 38=11, 39=9, 40=8, 41=7, 42=2, 43=1, 44=3, 45=4, 46=5, 47=6, 48=13, 49=58, 50=56, 51=57, 52=55, 53=54, 54=53, 55=52, 56=50, 57=49, 58=51, 59=59, 60=60, 61=61}

