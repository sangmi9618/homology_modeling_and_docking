
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(O)C([CH2])N:1.0, O=C(O)C([CH2])N>>O=C=O:2.0, O=C(O)CC(N)C(=O)O>>O=C(O)CCN:1.0, O=C([CH])O:2.0, O=C([CH])O>>O=C=O:2.0, O=C=O:3.0, [CH2]:1.0, [CH2]CN:1.0, [CH]:1.0, [C]:2.0, [C]=O:1.0, [C]C([CH2])N:1.0, [C]C([CH2])N>>[CH2]CN:1.0, [C]CC(N)C(=O)O:1.0, [C]CC(N)C(=O)O>>O=C=O:1.0, [C]CC(N)C(=O)O>>[C]CCN:1.0, [C]CCN:1.0, [C]O:1.0, [C]O>>[C]=O:1.0, [OH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[O=C([CH])O:1.0, [C]C([CH2])N:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C([CH2])N:1.0, [C]CC(N)C(=O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([CH])O:1.0, O=C=O:1.0, [C]=O:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (3)
[O=C(O)C([CH2])N:1.0, O=C([CH])O:1.0, O=C=O:2.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH2]:1.0, [CH]:1.0]


ID=Reaction Center at Level: 1 (2)
[[CH2]CN:1.0, [C]C([CH2])N:1.0]


ID=Reaction Center at Level: 2 (2)
[[C]CC(N)C(=O)O:1.0, [C]CCN:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]O>>[C]=O
2: [C]C([CH2])N>>[CH2]CN
3: O=C([CH])O>>O=C=O

MMP Level 2
1: O=C([CH])O>>O=C=O
2: [C]CC(N)C(=O)O>>[C]CCN
3: O=C(O)C([CH2])N>>O=C=O

MMP Level 3
1: O=C(O)C([CH2])N>>O=C=O
2: O=C(O)CC(N)C(=O)O>>O=C(O)CCN
3: [C]CC(N)C(=O)O>>O=C=O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)CC(N)C(=O)O>>O=C=O, O=C(O)CC(N)C(=O)O>>O=C=O]
2: R:M00001, P:M00003	[O=C(O)CC(N)C(=O)O>>O=C(O)CCN]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH2:4][CH:5]([NH2:6])[C:7](=[O:8])[OH:9]>>[O:8]=[C:7]=[O:9].[O:1]=[C:2]([OH:3])[CH2:4][CH2:5][NH2:6]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=4, 2=5, 3=7, 4=8, 5=9, 6=6, 7=2, 8=1, 9=3}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=8, 2=7, 3=9, 4=4, 5=5, 6=6, 7=2, 8=1, 9=3}

