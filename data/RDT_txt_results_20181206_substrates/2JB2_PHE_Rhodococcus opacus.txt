
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C=O:1.0]

ORDER_CHANGED
[O-O*O=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[N:3.0, O:3.0, O=C(O)C(N)CC([CH])=[CH]>>O=C(O)C(=O)CC([CH])=[CH]:1.0, O=O:4.0, O=O>>OO:6.0, O>>[C]=O:1.0, O>>[C]C(=O)[CH2]:1.0, O>>[C]CC(=O)C(=O)O:1.0, OO:4.0, [CH]:1.0, [CH]N:1.0, [CH]N>>N:1.0, [C]:1.0, [C]=O:1.0, [C]C(=O)[CH2]:2.0, [C]C([CH2])N:2.0, [C]C([CH2])N>>N:1.0, [C]C([CH2])N>>[C]C(=O)[CH2]:1.0, [C]CC(=O)C(=O)O:1.0, [C]CC(N)C(=O)O:1.0, [C]CC(N)C(=O)O>>N:1.0, [C]CC(N)C(=O)O>>[C]CC(=O)C(=O)O:1.0, [NH2]:1.0, [OH]:2.0, [O]:3.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (6)
[N:1.0, O:1.0, [CH]:1.0, [C]:1.0, [NH2]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (6)
[N:1.0, O:1.0, [CH]N:1.0, [C]=O:1.0, [C]C(=O)[CH2]:1.0, [C]C([CH2])N:1.0]


ID=Reaction Center at Level: 2 (6)
[N:1.0, O:1.0, [C]C(=O)[CH2]:1.0, [C]C([CH2])N:1.0, [C]CC(=O)C(=O)O:1.0, [C]CC(N)C(=O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (2)
[[OH]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (2)
[O=O:2.0, OO:2.0]


ID=Reaction Center at Level: 2 (2)
[O=O:2.0, OO:2.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(=O)[CH2]:1.0, [C]C([CH2])N:1.0]


ID=Reaction Center at Level: 2 (2)
[[C]CC(=O)C(=O)O:1.0, [C]CC(N)C(=O)O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C([CH2])N>>[C]C(=O)[CH2]
2: O>>[C]=O
3: [CH]N>>N
4: O=O>>OO

MMP Level 2
1: [C]CC(N)C(=O)O>>[C]CC(=O)C(=O)O
2: O>>[C]C(=O)[CH2]
3: [C]C([CH2])N>>N
4: O=O>>OO

MMP Level 3
1: O=C(O)C(N)CC([CH])=[CH]>>O=C(O)C(=O)CC([CH])=[CH]
2: O>>[C]CC(=O)C(=O)O
3: [C]CC(N)C(=O)O>>N
4: O=O>>OO


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00005	[O=C(O)C(N)Cc1ccccc1>>N]
2: R:M00001, P:M00006	[O=C(O)C(N)Cc1ccccc1>>O=C(O)C(=O)Cc1ccccc1]
3: R:M00002, P:M00006	[O>>O=C(O)C(=O)Cc1ccccc1]
4: R:M00003, P:M00004	[O=O>>OO]


//
SELECTED AAM MAPPING
[O:13]=[O:14].[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][C:7]:1:[CH:8]:[CH:9]:[CH:10]:[CH:11]:[CH:12]1.[OH2:15]>>[O:1]=[C:2]([OH:3])[C:4](=[O:15])[CH2:6][C:7]:1:[CH:8]:[CH:9]:[CH:10]:[CH:11]:[CH:12]1.[OH:13][OH:14].[NH3:5]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=10, 2=9, 3=8, 4=7, 5=12, 6=11, 7=6, 8=4, 9=2, 10=1, 11=3, 12=5, 13=15, 14=13, 15=14}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=13, 2=14, 3=15, 4=10, 5=9, 6=8, 7=7, 8=12, 9=11, 10=6, 11=4, 12=5, 13=2, 14=1, 15=3}

