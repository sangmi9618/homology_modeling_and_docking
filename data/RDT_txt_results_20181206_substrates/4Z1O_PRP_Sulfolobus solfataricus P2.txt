
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O=P(O)(O[P])OC1O[CH][CH]C1O>>O=P(O)(O)O[P]:1.0, [CH]:2.0, [C]C1=[C]N=CN1:1.0, [C]C1=[C]N=CN1>>[C]C1=[C][N]C=N1:1.0, [C]C1=[C][N]C=N1:1.0, [C]N([CH])C1O[CH][CH]C1O:1.0, [C]N=[CH]:1.0, [C]N[CH]:1.0, [C]N[CH]>>[C]N=[CH]:1.0, [NH]:1.0, [N]:1.0, [N]C(=O)c1[nH]cnc1[NH]>>[N]C(=O)c1ncn([CH])c1[NH]:1.0, [N]C([O])[CH]:1.0, [OH]:1.0, [O]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]C([O])[CH]:1.0, [O]C([O])[CH]>>[N]C([O])[CH]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OC1OC([CH2])C(O)C1O>>[NH]C1=[C]N=CN1C2OC([CH2])C(O)C2O:1.0, [P]O:1.0, [P]OC1O[CH][CH]C1O:1.0, [P]OC1O[CH][CH]C1O>>[C]N([CH])C1O[CH][CH]C1O:1.0, [P]O[CH]:1.0, [P]O[CH]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [N]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[C]N([CH])[CH]:1.0, [N]C([O])[CH]:1.0, [O]C([O])[CH]:1.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]N([CH])C1O[CH][CH]C1O:1.0, [O]C([CH])N1C=N[C]=C1[NH]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [P]OC1O[CH][CH]C1O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]N[CH]>>[C]N=[CH]
2: [O]C([O])[CH]>>[N]C([O])[CH]
3: [P]O[CH]>>[P]O

MMP Level 2
1: [C]C1=[C]N=CN1>>[C]C1=[C][N]C=N1
2: [P]OC1O[CH][CH]C1O>>[C]N([CH])C1O[CH][CH]C1O
3: [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O

MMP Level 3
1: [N]C(=O)c1[nH]cnc1[NH]>>[N]C(=O)c1ncn([CH])c1[NH]
2: [O]P(=O)(O)OC1OC([CH2])C(O)C1O>>[NH]C1=[C]N=CN1C2OC([CH2])C(O)C2O
3: O=P(O)(O[P])OC1O[CH][CH]C1O>>O=P(O)(O)O[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OCC1OC(OP(=O)(O)OP(=O)(O)O)C(O)C1O>>O=c1nc[nH]c2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O]
2: R:M00001, P:M00004	[O=P(O)(O)OCC1OC(OP(=O)(O)OP(=O)(O)O)C(O)C1O>>O=P(O)(O)OP(=O)(O)O]
3: R:M00002, P:M00003	[O=c1nc[nH]c2nc[nH]c12>>O=c1nc[nH]c2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O]


//
SELECTED AAM MAPPING
[O:23]=[C:24]1[N:25]=[CH:26][NH:27][C:28]:2:[N:29]:[CH:30]:[NH:31]:[C:32]12.[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH2:6][CH:7]1[O:8][CH:9]([O:10][P:11](=[O:12])([OH:13])[O:14][P:15](=[O:16])([OH:17])[OH:18])[CH:19]([OH:20])[CH:21]1[OH:22]>>[O:23]=[C:24]1[N:25]=[CH:26][NH:27][C:28]:2:[C:32]1:[N:31]:[CH:30]:[N:29]2[CH:9]3[O:8][CH:7]([CH2:6][O:5][P:2](=[O:1])([OH:3])[OH:4])[CH:21]([OH:22])[CH:19]3[OH:20].[O:12]=[P:11]([OH:10])([OH:13])[O:14][P:15](=[O:16])([OH:17])[OH:18]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=21, 4=19, 5=9, 6=8, 7=10, 8=11, 9=12, 10=13, 11=14, 12=15, 13=16, 14=17, 15=18, 16=20, 17=22, 18=5, 19=2, 20=1, 21=3, 22=4, 23=30, 24=29, 25=28, 26=32, 27=31, 28=24, 29=23, 30=25, 31=26, 32=27}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=4, 2=3, 3=2, 4=1, 5=7, 6=6, 7=5, 8=10, 9=9, 10=8, 11=11, 12=22, 13=20, 14=13, 15=12, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=21, 23=23, 24=26, 25=25, 26=24, 27=27, 28=28, 29=29, 30=30, 31=31, 32=32}

