
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(O)C(O)(O[CH])C[CH]:1.0, O=C(O)C(O)(O[CH])C[CH]>>O=C(O)C(O[CH])(OP(=O)(O)O[CH2])C[CH]:1.0, O=C(O)C(O)(O[CH])C[CH]>>O=C(O)C(O[P])(O[CH])C[CH]:1.0, O=C(O)C(O[P])(O[CH])C[CH]:1.0, O=C(O)C1(O)OC([CH])[CH]C(O)C1>>[O]P(=O)(O)OC1(OC([CH])[CH]C(O)C1)C(=O)O:1.0, O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[CH2]:1.0, O=P(O)(O[P])O[CH2]>>[C]OP(=O)(O)O[CH2]:1.0, [C]:2.0, [C]C([O])(O)[CH2]:2.0, [C]C([O])(O)[CH2]>>[C]C([O])(OP([O])(=O)O)[CH2]:1.0, [C]C([O])(O)[CH2]>>[C]C([O])([O])[CH2]:1.0, [C]C([O])(OP([O])(=O)O)[CH2]:1.0, [C]C([O])([O])[CH2]:1.0, [C]O:1.0, [C]O>>[C]O[P]:1.0, [C]OP(=O)(O)O[CH2]:1.0, [C]O[P]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)OC[CH]>>[C]C([O])(OP(=O)(O)OC[CH])[CH2]:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:2.0, [O]P([O])(=O)O>>[O]P([O])(=O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[C]O[P]:1.0, [O]P([O])(=O)O:2.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O[P])O[CH2]:1.0, [C]C([O])(OP([O])(=O)O)[CH2]:1.0, [C]OP(=O)(O)O[CH2]:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[C]:2.0]


ID=Reaction Center at Level: 1 (2)
[[C]C([O])(O)[CH2]:1.0, [C]C([O])([O])[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C(O)(O[CH])C[CH]:1.0, O=C(O)C(O[P])(O[CH])C[CH]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]O>>[C]O[P]
2: [C]C([O])(O)[CH2]>>[C]C([O])([O])[CH2]
3: [P]O[P]>>[P]O
4: [O]P([O])(=O)O>>[O]P([O])(=O)O

MMP Level 2
1: [C]C([O])(O)[CH2]>>[C]C([O])(OP([O])(=O)O)[CH2]
2: O=C(O)C(O)(O[CH])C[CH]>>O=C(O)C(O[P])(O[CH])C[CH]
3: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
4: O=P(O)(O[P])O[CH2]>>[C]OP(=O)(O)O[CH2]

MMP Level 3
1: O=C(O)C(O)(O[CH])C[CH]>>O=C(O)C(O[CH])(OP(=O)(O)O[CH2])C[CH]
2: O=C(O)C1(O)OC([CH])[CH]C(O)C1>>[O]P(=O)(O)OC1(OC([CH])[CH]C(O)C1)C(=O)O
3: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]
4: [O]P(=O)(O)OP(=O)(O)OC[CH]>>[C]C([O])(OP(=O)(O)OC[CH])[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2O>>O=P(O)(O)OP(=O)(O)O]
2: R:M00001, P:M00004	[O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2O>>O=c1nc(N)ccn1C2OC(COP(=O)(O)OC3(OC(C(O)C(O)C3)C(O)CO)C(=O)O)C(O)C2O]
3: R:M00002, P:M00004	[O=C(O)C1(O)OC(C(O)C(O)C1)C(O)CO>>O=c1nc(N)ccn1C2OC(COP(=O)(O)OC3(OC(C(O)C(O)C3)C(O)CO)C(=O)O)C(O)C2O, O=C(O)C1(O)OC(C(O)C(O)C1)C(O)CO>>O=c1nc(N)ccn1C2OC(COP(=O)(O)OC3(OC(C(O)C(O)C3)C(O)CO)C(=O)O)C(O)C2O]


//
SELECTED AAM MAPPING
[O:30]=[C:31]([OH:32])[C:33]1([OH:34])[O:35][CH:36]([CH:37]([OH:38])[CH2:39][OH:40])[CH:41]([OH:42])[CH:43]([OH:44])[CH2:45]1.[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[CH:6]=[CH:7][N:8]1[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][P:22](=[O:23])([OH:24])[OH:25])[CH:26]([OH:27])[CH:28]2[OH:29]>>[O:30]=[C:31]([OH:32])[C:33]1([O:35][CH:36]([CH:37]([OH:38])[CH2:39][OH:40])[CH:41]([OH:42])[CH:43]([OH:44])[CH2:45]1)[O:34][P:14](=[O:15])([OH:16])[O:13][CH2:12][CH:11]2[O:10][CH:9]([N:8]3[CH:7]=[CH:6][C:4](=[N:3][C:2]3=[O:1])[NH2:5])[CH:28]([OH:29])[CH:26]2[OH:27].[O:19]=[P:18]([OH:17])([OH:20])[O:21][P:22](=[O:23])([OH:24])[OH:25]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=8, 4=2, 5=1, 6=3, 7=4, 8=5, 9=9, 10=28, 11=26, 12=11, 13=10, 14=12, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=25, 28=27, 29=29, 30=45, 31=43, 32=41, 33=36, 34=35, 35=33, 36=31, 37=30, 38=32, 39=34, 40=37, 41=39, 42=40, 43=38, 44=42, 45=44}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=39, 2=38, 3=37, 4=40, 5=41, 6=42, 7=43, 8=44, 9=45, 10=15, 11=13, 12=11, 13=6, 14=5, 15=4, 16=2, 17=1, 18=3, 19=16, 20=17, 21=18, 22=19, 23=20, 24=21, 25=22, 26=35, 27=33, 28=24, 29=23, 30=25, 31=26, 32=27, 33=28, 34=29, 35=30, 36=31, 37=32, 38=34, 39=36, 40=7, 41=9, 42=10, 43=8, 44=12, 45=14}

