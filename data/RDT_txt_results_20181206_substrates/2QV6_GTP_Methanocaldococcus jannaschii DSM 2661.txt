
//
FINGERPRINTS BC

FORMED_CLEAVED
[C=O:1.0, C@N:1.0, O-P:2.0]

ORDER_CHANGED
[C-N*C@N:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C[NH]:2.0, O=P(O)(O)O:1.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O:1.0, O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]:1.0, O=P(O)(O[P])O[P]:1.0, O=[CH]:1.0, O>>O=C[NH]:1.0, O>>O=[CH]:1.0, O>>[C]NC=O:1.0, [CH]:2.0, [CH]N1[C]=[C]N=C1:1.0, [CH]N1[C]=[C]N=C1>>[C]NC=O:1.0, [C]=C([NH])NC([O])[CH]:1.0, [C]C(=[C])NC=O:1.0, [C]C1=[C][N]C=N1:1.0, [C]C1=[C][N]C=N1>>[C]C(=[C])NC=O:1.0, [C]N([CH])[CH]:1.0, [C]N([CH])[CH]>>[C]N[CH]:1.0, [C]N=[CH]:1.0, [C]N=[CH]>>[C]N[CH]:1.0, [C]NC=O:1.0, [C]N[CH]:2.0, [C]Nc1c([C])ncn1C2O[CH][CH]C2O>>[C]NC(NC1O[CH][CH]C1O)=C([C])[NH]:1.0, [C]c1ncn(c1[NH])C([O])[CH]>>[C]C(=[C])NC=O:1.0, [NH]:2.0, [N]:2.0, [N]C(=O)c1ncn([CH])c1[NH]>>[N]C(=O)C(NC=O)=C([NH])[NH]:1.0, [N]C=[N]:1.0, [N]C=[N]>>O=C[NH]:1.0, [OH]:2.0, [O]:3.0, [O]C([CH])N1C=N[C]=C1[NH]:1.0, [O]C([CH])N1C=N[C]=C1[NH]>>[C]=C([NH])NC([O])[CH]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [P]:1.0, [P]O:2.0, [P]O[P]:2.0, [P]O[P]>>[P]O:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[O:1.0, [CH]:2.0, [N]:1.0, [O]:3.0, [P]:2.0]


ID=Reaction Center at Level: 1 (7)
[O:1.0, O=C[NH]:1.0, O=[CH]:1.0, [C]N([CH])[CH]:1.0, [N]C=[N]:1.0, [O]P([O])(=O)O:2.0, [P]O[P]:2.0]


ID=Reaction Center at Level: 2 (8)
[O:1.0, O=C[NH]:1.0, O=P(O)(O[P])O[P]:2.0, [CH]N1[C]=[C]N=C1:1.0, [C]NC=O:1.0, [O]C([CH])N1C=N[C]=C1[NH]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [NH]:1.0, [N]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C[NH]:1.0, [C]N=[CH]:1.0, [C]N[CH]:1.0, [N]C=[N]:1.0]


ID=Reaction Center at Level: 2 (4)
[[CH]N1[C]=[C]N=C1:1.0, [C]C(=[C])NC=O:1.0, [C]C1=[C][N]C=N1:1.0, [C]NC=O:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [N]C=[N]>>O=C[NH]
2: [P]O[P]>>[P]O
3: [C]N([CH])[CH]>>[C]N[CH]
4: O>>O=[CH]
5: [C]N=[CH]>>[C]N[CH]
6: [P]O[P]>>[P]O

MMP Level 2
1: [CH]N1[C]=[C]N=C1>>[C]NC=O
2: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
3: [O]C([CH])N1C=N[C]=C1[NH]>>[C]=C([NH])NC([O])[CH]
4: O>>O=C[NH]
5: [C]C1=[C][N]C=N1>>[C]C(=[C])NC=O
6: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O

MMP Level 3
1: [C]c1ncn(c1[NH])C([O])[CH]>>[C]C(=[C])NC=O
2: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]
3: [C]Nc1c([C])ncn1C2O[CH][CH]C2O>>[C]NC(NC1O[CH][CH]C1O)=C([C])[NH]
4: O>>[C]NC=O
5: [N]C(=O)c1ncn([CH])c1[NH]>>[N]C(=O)C(NC=O)=C([NH])[NH]
6: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O>>O=CNc1c(=O)nc(N)[nH]c1NC2OC(COP(=O)(O)O)C(O)C2O]
2: R:M00002, P:M00003	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C3O>>O=P(O)(O)O]
3: R:M00002, P:M00004	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C3O>>O=CNc1c(=O)nc(N)[nH]c1NC2OC(COP(=O)(O)O)C(O)C2O, O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C3O>>O=CNc1c(=O)nc(N)[nH]c1NC2OC(COP(=O)(O)O)C(O)C2O, O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C3O>>O=CNc1c(=O)nc(N)[nH]c1NC2OC(COP(=O)(O)O)C(O)C2O, O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C3O>>O=CNc1c(=O)nc(N)[nH]c1NC2OC(COP(=O)(O)O)C(O)C2O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[C:8]1:[N:9]:[CH:10]:[N:11]2[CH:12]3[O:13][CH:14]([CH2:15][O:16][P:17](=[O:18])([OH:19])[O:20][P:31](=[O:32])([OH:33])[O:21][P:22](=[O:23])([OH:24])[OH:25])[CH:26]([OH:27])[CH:28]3[OH:29].[OH2:30]>>[O:30]=[CH:10][NH:9][C:8]=1[C:2](=[O:1])[N:3]=[C:4]([NH2:5])[NH:6][C:7]1[NH:11][CH:12]2[O:13][CH:14]([CH2:15][O:16][P:17](=[O:18])([OH:20])[OH:19])[CH:26]([OH:27])[CH:28]2[OH:29].[O:23]=[P:22]([OH:21])([OH:24])[OH:25]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=33, 2=10, 3=9, 4=8, 5=7, 6=11, 7=12, 8=31, 9=29, 10=14, 11=13, 12=15, 13=16, 14=17, 15=18, 16=19, 17=20, 18=21, 19=22, 20=23, 21=24, 22=25, 23=26, 24=27, 25=28, 26=30, 27=32, 28=6, 29=4, 30=3, 31=2, 32=1, 33=5}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=28, 2=27, 3=26, 4=29, 5=30, 6=16, 7=15, 8=22, 9=24, 10=13, 11=14, 12=12, 13=11, 14=4, 15=5, 16=6, 17=7, 18=8, 19=10, 20=9, 21=3, 22=2, 23=1, 24=25, 25=23, 26=17, 27=18, 28=19, 29=20, 30=21}

