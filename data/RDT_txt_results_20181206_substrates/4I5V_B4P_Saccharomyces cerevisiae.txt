
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)O:1.0, O=P(O)(O)O>>O=P(O)(O)OP(=O)(O)O[CH2]:1.0, O=P(O)(O)O>>[O]P(=O)(O)OP(=O)(O)O:1.0, O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[CH2]:2.0, O=P(O)(O[P])O[CH2]>>O=P(O)(O[P])O[CH2]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)OC[CH]>>O=P(O)(O)OP(=O)(O)OC[CH]:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:2.0, [O]P([O])(=O)O>>[O]P([O])(=O)O:1.0, [P]:2.0, [P]O:2.0, [P]O>>[P]O[P]:1.0, [P]O[P]:2.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]P([O])(=O)O:2.0, [P]O[P]:2.0]


ID=Reaction Center at Level: 2 (3)
[O=P(O)(O[P])O[CH2]:2.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[P]>>[P]O
2: [O]P([O])(=O)O>>[O]P([O])(=O)O
3: [P]O>>[P]O[P]

MMP Level 2
1: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
2: O=P(O)(O[P])O[CH2]>>O=P(O)(O[P])O[CH2]
3: O=P(O)(O)O>>[O]P(=O)(O)OP(=O)(O)O

MMP Level 3
1: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]
2: [O]P(=O)(O)OP(=O)(O)OC[CH]>>O=P(O)(O)OP(=O)(O)OC[CH]
3: O=P(O)(O)O>>O=P(O)(O)OP(=O)(O)O[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O>>O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
3: R:M00002, P:M00003	[O=P(O)(O)O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]


//
SELECTED AAM MAPPING
[O:54]=[P:55]([OH:56])([OH:57])[OH:58].[O:33]=[P:32]([OH:34])([O:35][CH2:36][CH:37]1[O:38][CH:39]([N:40]:2:[CH:41]:[N:42]:[C:43]:3:[C:44](:[N:45]:[CH:46]:[N:47]:[C:48]32)[NH2:49])[CH:50]([OH:51])[CH:52]1[OH:53])[O:31][P:28](=[O:29])([OH:30])[O:27][P:24](=[O:25])([OH:26])[O:23][P:2](=[O:1])([OH:3])[O:4][CH2:5][CH:6]4[O:7][CH:8]([N:9]:5:[CH:10]:[N:11]:[C:12]:6:[C:13](:[N:14]:[CH:15]:[N:16]:[C:17]65)[NH2:18])[CH:19]([OH:20])[CH:21]4[OH:22]>>[O:54]=[P:55]([OH:56])([OH:57])[O:58][P:2](=[O:1])([OH:3])[O:4][CH2:5][CH:6]1[O:7][CH:8]([N:9]:2:[CH:10]:[N:11]:[C:12]:3:[C:13](:[N:14]:[CH:15]:[N:16]:[C:17]32)[NH2:18])[CH:19]([OH:20])[CH:21]1[OH:22].[O:25]=[P:24]([OH:23])([OH:26])[O:27][P:28](=[O:29])([OH:30])[O:31][P:32](=[O:33])([OH:34])[O:35][CH2:36][CH:37]1[O:38][CH:39]([N:40]:2:[CH:41]:[N:42]:[C:43]:3:[C:44](:[N:45]:[CH:46]:[N:47]:[C:48]32)[NH2:49])[CH:50]([OH:51])[CH:52]1[OH:53]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=46, 2=47, 3=48, 4=43, 5=44, 6=45, 7=49, 8=42, 9=41, 10=40, 11=39, 12=50, 13=52, 14=37, 15=38, 16=36, 17=35, 18=32, 19=33, 20=34, 21=31, 22=28, 23=29, 24=30, 25=27, 26=24, 27=25, 28=26, 29=23, 30=2, 31=1, 32=3, 33=4, 34=5, 35=6, 36=21, 37=19, 38=8, 39=7, 40=9, 41=10, 42=11, 43=12, 44=17, 45=16, 46=15, 47=14, 48=13, 49=18, 50=20, 51=22, 52=53, 53=51, 54=56, 55=55, 56=54, 57=57, 58=58}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=51, 2=52, 3=53, 4=48, 5=49, 6=50, 7=54, 8=47, 9=46, 10=45, 11=44, 12=55, 13=57, 14=42, 15=43, 16=41, 17=40, 18=37, 19=38, 20=39, 21=36, 22=33, 23=32, 24=34, 25=35, 26=58, 27=56, 28=24, 29=25, 30=26, 31=21, 32=22, 33=23, 34=27, 35=20, 36=19, 37=18, 38=17, 39=28, 40=30, 41=15, 42=16, 43=14, 44=13, 45=10, 46=11, 47=12, 48=9, 49=6, 50=7, 51=8, 52=5, 53=2, 54=1, 55=3, 56=4, 57=31, 58=29}

