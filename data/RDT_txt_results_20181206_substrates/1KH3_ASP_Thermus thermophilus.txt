
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C=O:1.0, O-P:2.0]

ORDER_CHANGED
[C-N*C=N:1.0]

//
FINGERPRINTS RC
[O=C(N)NCC[CH2]>>[CH]NC(=NCC[CH2])N:1.0, O=C(N)NC[CH2]:1.0, O=C(N)NC[CH2]>>[C]C([CH2])NC(=NC[CH2])N:1.0, O=C(N)NC[CH2]>>[NH]C(=NC[CH2])N:1.0, O=C(N)N[CH2]:1.0, O=C(N)N[CH2]>>O=P(O)(O)O[P]:1.0, O=C(N)N[CH2]>>[CH]NC(=N[CH2])N:1.0, O=C([NH])N:2.0, O=C([NH])N>>[N]=C([NH])N:1.0, O=C([NH])N>>[O]P(=O)(O)O:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]:1.0, O=P(O)(O[P])O[P]:1.0, O=P(O)(O[P])O[P]>>O=P(O)(O)O[P]:1.0, [CH]N:1.0, [CH]N>>[C]N[CH]:1.0, [CH]NC(=N[CH2])N:1.0, [C]:2.0, [C]=N[CH2]:1.0, [C]=O:1.0, [C]=O>>[P]O:1.0, [C]C([CH2])N:1.0, [C]C([CH2])N>>[C]C([CH2])NC(=[N])N:1.0, [C]C([CH2])NC(=[N])N:1.0, [C]CC(N)C(=O)O>>[C]CC(NC(=N[CH2])N)C(=O)O:1.0, [C]N[CH2]:1.0, [C]N[CH2]>>[C]=N[CH2]:1.0, [C]N[CH]:1.0, [NH2]:1.0, [NH]:2.0, [NH]C(=NC[CH2])N:1.0, [N]:1.0, [N]=C([NH])N:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)OP(=O)(O)OP(=O)(O)O>>O=P(O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [O]P([O])(=O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[C]:2.0, [NH]:1.0, [OH]:1.0, [O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (8)
[O=C([NH])N:1.0, [C]=O:1.0, [C]N[CH]:1.0, [N]=C([NH])N:1.0, [O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (8)
[O=C(N)N[CH2]:1.0, O=C([NH])N:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[P]:1.0, [CH]NC(=N[CH2])N:1.0, [C]C([CH2])NC(=[N])N:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [NH]:1.0, [N]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([NH])N:1.0, [C]=N[CH2]:1.0, [C]N[CH2]:1.0, [N]=C([NH])N:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(N)NC[CH2]:1.0, O=C(N)N[CH2]:1.0, [CH]NC(=N[CH2])N:1.0, [NH]C(=NC[CH2])N:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]N[CH2]>>[C]=N[CH2]
2: [C]=O>>[P]O
3: [P]O[P]>>[P]O
4: [CH]N>>[C]N[CH]
5: [O]P([O])(=O)O>>[O]P(=O)(O)O
6: O=C([NH])N>>[N]=C([NH])N

MMP Level 2
1: O=C(N)NC[CH2]>>[NH]C(=NC[CH2])N
2: O=C([NH])N>>[O]P(=O)(O)O
3: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
4: [C]C([CH2])N>>[C]C([CH2])NC(=[N])N
5: O=P(O)(O[P])O[P]>>O=P(O)(O)O[P]
6: O=C(N)N[CH2]>>[CH]NC(=N[CH2])N

MMP Level 3
1: O=C(N)NCC[CH2]>>[CH]NC(=NCC[CH2])N
2: O=C(N)N[CH2]>>O=P(O)(O)O[P]
3: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]
4: [C]CC(N)C(=O)O>>[C]CC(NC(=N[CH2])N)C(=O)O
5: [O]P(=O)(O)OP(=O)(O)OP(=O)(O)O>>O=P(O)(O)OP(=O)(O)O
6: O=C(N)NC[CH2]>>[C]C([CH2])NC(=NC[CH2])N


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00005	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)O]
3: R:M00002, P:M00005	[O=C(N)NCCCC(N)C(=O)O>>O=P(O)(O)OP(=O)(O)O]
4: R:M00002, P:M00006	[O=C(N)NCCCC(N)C(=O)O>>O=C(O)CC(NC(=NCCCC(N)C(=O)O)N)C(=O)O, O=C(N)NCCCC(N)C(=O)O>>O=C(O)CC(NC(=NCCCC(N)C(=O)O)N)C(=O)O]
5: R:M00003, P:M00006	[O=C(O)CC(N)C(=O)O>>O=C(O)CC(NC(=NCCCC(N)C(=O)O)N)C(=O)O]


//
SELECTED AAM MAPPING
[O:44]=[C:45]([OH:46])[CH2:47][CH:48]([NH2:49])[C:50](=[O:51])[OH:52].[O:42]=[C:41]([OH:43])[CH:39]([NH2:40])[CH2:38][CH2:37][CH2:36][NH:35][C:33](=[O:32])[NH2:34].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:44]=[C:45]([OH:46])[CH2:47][CH:48]([NH:49][C:33](=[N:35][CH2:36][CH2:37][CH2:38][CH:39]([NH2:40])[C:41](=[O:42])[OH:43])[NH2:34])[C:50](=[O:51])[OH:52].[O:11]=[P:10]([OH:9])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[OH:32]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=37, 33=38, 34=39, 35=41, 36=42, 37=43, 38=40, 39=36, 40=35, 41=33, 42=32, 43=34, 44=47, 45=48, 46=50, 47=51, 48=52, 49=49, 50=45, 51=44, 52=46}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=16, 2=17, 3=18, 4=13, 5=14, 6=15, 7=19, 8=12, 9=11, 10=10, 11=9, 12=20, 13=22, 14=7, 15=8, 16=6, 17=5, 18=2, 19=1, 20=3, 21=4, 22=23, 23=21, 24=46, 25=45, 26=44, 27=47, 28=48, 29=49, 30=50, 31=51, 32=52, 33=33, 34=34, 35=35, 36=37, 37=38, 38=39, 39=36, 40=32, 41=31, 42=30, 43=40, 44=29, 45=28, 46=27, 47=25, 48=24, 49=26, 50=41, 51=42, 52=43}

