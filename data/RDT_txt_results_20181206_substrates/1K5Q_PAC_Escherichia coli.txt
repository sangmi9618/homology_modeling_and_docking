
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)[CH2]:2.0, O=C([NH])[CH2]:1.0, O=C([NH])[CH2]>>O=C(O)[CH2]:1.0, O>>O=C(O)[CH2]:1.0, O>>[C]CC(=O)O:1.0, O>>[C]O:1.0, [CH]N:1.0, [C]:2.0, [C]C([CH])N:1.0, [C]C([CH])NC(=O)CC([CH])=[CH]>>O=C(O)CC([CH])=[CH]:1.0, [C]C([CH])NC(=O)[CH2]:1.0, [C]C([CH])NC(=O)[CH2]>>[C]C([CH])N:1.0, [C]CC(=O)NC1C(=O)[N]C1[S]>>O=C1[N]C([S])C1N:1.0, [C]CC(=O)N[CH]:1.0, [C]CC(=O)N[CH]>>[C]CC(=O)O:1.0, [C]CC(=O)O:1.0, [C]N[CH]:1.0, [C]N[CH]>>[CH]N:1.0, [C]O:1.0, [NH2]:1.0, [NH]:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [C]:2.0, [NH]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=C(O)[CH2]:1.0, O=C([NH])[CH2]:1.0, [C]N[CH]:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=C(O)[CH2]:1.0, [C]C([CH])NC(=O)[CH2]:1.0, [C]CC(=O)N[CH]:1.0, [C]CC(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=C([NH])[CH2]>>O=C(O)[CH2]
2: O>>[C]O
3: [C]N[CH]>>[CH]N

MMP Level 2
1: [C]CC(=O)N[CH]>>[C]CC(=O)O
2: O>>O=C(O)[CH2]
3: [C]C([CH])NC(=O)[CH2]>>[C]C([CH])N

MMP Level 3
1: [C]C([CH])NC(=O)CC([CH])=[CH]>>O=C(O)CC([CH])=[CH]
2: O>>[C]CC(=O)O
3: [C]CC(=O)NC1C(=O)[N]C1[S]>>O=C1[N]C([S])C1N


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C1N2C(SC(C)(C)C2C(=O)O)C1NC(=O)Cc3ccccc3>>O=C(O)Cc1ccccc1]
2: R:M00001, P:M00004	[O=C1N2C(SC(C)(C)C2C(=O)O)C1NC(=O)Cc3ccccc3>>O=C1N2C(SC(C)(C)C2C(=O)O)C1N]
3: R:M00002, P:M00003	[O>>O=C(O)Cc1ccccc1]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]1[N:5]2[C:6](=[O:7])[CH:8]([NH:9][C:10](=[O:11])[CH2:12][C:13]:3:[CH:14]:[CH:15]:[CH:16]:[CH:17]:[CH:18]3)[CH:19]2[S:20][C:21]1([CH3:22])[CH3:23].[OH2:24]>>[O:11]=[C:10]([OH:24])[CH2:12][C:13]:1:[CH:14]:[CH:15]:[CH:16]:[CH:17]:[CH:18]1.[O:1]=[C:2]([OH:3])[CH:4]1[N:5]2[C:6](=[O:7])[CH:8]([NH2:9])[CH:19]2[S:20][C:21]1([CH3:22])[CH3:23]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=22, 2=21, 3=4, 4=5, 5=19, 6=20, 7=8, 8=6, 9=7, 10=9, 11=10, 12=11, 13=12, 14=13, 15=14, 16=15, 17=16, 18=17, 19=18, 20=2, 21=1, 22=3, 23=23, 24=24}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=22, 2=21, 3=20, 4=19, 5=24, 6=23, 7=18, 8=16, 9=15, 10=17, 11=13, 12=12, 13=4, 14=5, 15=10, 16=11, 17=8, 18=6, 19=7, 20=9, 21=2, 22=1, 23=3, 24=14}

