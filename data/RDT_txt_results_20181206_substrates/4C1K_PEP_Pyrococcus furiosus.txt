
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0, C-O:1.0, O-P:2.0]

ORDER_CHANGED
[C-C*C=C:1.0, C-O*C=O:2.0]

STEREO_CHANGED
[C(R/S):3.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)C(=O)C[CH]:1.0, O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(=O)CC([CH])O:1.0, O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(=O)C[CH]:1.0, O=C(O)C(O[P])=C:1.0, O=C(O)C(O[P])=C>>O=C(O)C(=O)CC(O)C([CH])O:1.0, O=C(O)C(O[P])=C>>O=C(O)C(=O)C[CH]:1.0, O=CCC(O)CO[P]>>[P]OCC(O)C(O)C(O)[CH2]:1.0, O=CCC(O)[CH2]:1.0, O=CCC(O)[CH2]>>OC([CH2])C(O)C(O)[CH2]:1.0, O=CCC(O)[CH2]>>[C]C(=O)CC(O)C(O)C(O)[CH2]:1.0, O=CC[CH]:1.0, O=CC[CH]>>[C]CC(O)C([CH])O:2.0, O=C[CH2]:2.0, O=C[CH2]>>[CH]C(O)[CH2]:2.0, O=P(O)(O)O:3.0, O=[CH]:1.0, O=[CH]>>[CH]O:1.0, O>>O=P(O)(O)O:2.0, O>>[P]O:1.0, OC([CH2])C(O)C(O)[CH2]:1.0, OC([CH2])[CH2]:1.0, OC([CH2])[CH2]>>[CH]C(O)[CH2]:1.0, [CH2]:3.0, [CH]:5.0, [CH]C(O)[CH2]:3.0, [CH]C([CH])O:1.0, [CH]C[CH]:1.0, [CH]C[CH]>>[CH]C([CH])O:1.0, [CH]O:1.0, [C]:2.0, [C]=C:1.0, [C]=C>>[C]C[CH]:1.0, [C]=O:1.0, [C]C(=O)CC([CH])O:1.0, [C]C(=O)[CH2]:2.0, [C]C(OP(=O)(O)O)=C:1.0, [C]C(OP(=O)(O)O)=C>>O=P(O)(O)O:1.0, [C]C(OP(=O)(O)O)=C>>[C]C(=O)[CH2]:1.0, [C]C([O])=C:2.0, [C]C([O])=C>>[C]C(=O)CC([CH])O:1.0, [C]C([O])=C>>[C]C(=O)[CH2]:1.0, [C]CC(O)C([CH])O:1.0, [C]C[CH]:1.0, [C]OP(=O)(O)O:1.0, [C]OP(=O)(O)O>>O=P(O)(O)O:1.0, [C]O[P]:1.0, [C]O[P]>>[C]=O:1.0, [OH]:2.0, [O]:3.0, [O]CC(O)C([CH])O:1.0, [O]CC(O)CC=O>>[C]CC(O)C(O)C(O)C[O]:1.0, [O]CC(O)C[CH]:1.0, [O]CC(O)C[CH]>>[O]CC(O)C([CH])O:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [P]:2.0, [P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (6)
[O:1.0, [CH2]:1.0, [CH]:2.0, [OH]:2.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (9)
[O:1.0, O=P(O)(O)O:1.0, [CH]C(O)[CH2]:1.0, [CH]C([CH])O:1.0, [CH]O:1.0, [C]C[CH]:1.0, [C]O[P]:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0]


ID=Reaction Center at Level: 2 (8)
[O:1.0, O=P(O)(O)O:2.0, OC([CH2])C(O)C(O)[CH2]:1.0, [CH]C([CH])O:1.0, [C]C(=O)CC([CH])O:1.0, [C]C(OP(=O)(O)O)=C:1.0, [C]CC(O)C([CH])O:1.0, [C]OP(=O)(O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (5)
[[CH2]:2.0, [CH]:2.0, [C]:2.0, [OH]:1.0, [O]:3.0]


ID=Reaction Center at Level: 1 (10)
[O=C[CH2]:1.0, O=[CH]:1.0, [CH]C(O)[CH2]:1.0, [CH]O:1.0, [C]=C:1.0, [C]=O:1.0, [C]C(=O)[CH2]:1.0, [C]C([O])=C:1.0, [C]C[CH]:1.0, [C]O[P]:1.0]


ID=Reaction Center at Level: 2 (10)
[O=C(O)C(=O)C[CH]:1.0, O=C(O)C(O[P])=C:1.0, O=CC[CH]:1.0, O=C[CH2]:1.0, [CH]C(O)[CH2]:1.0, [C]C(=O)CC([CH])O:1.0, [C]C(=O)[CH2]:1.0, [C]C(OP(=O)(O)O)=C:1.0, [C]C([O])=C:1.0, [C]CC(O)C([CH])O:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH2]:1.0, [CH]:5.0]


ID=Reaction Center at Level: 1 (5)
[O=C[CH2]:1.0, OC([CH2])[CH2]:1.0, [CH]C(O)[CH2]:2.0, [CH]C([CH])O:1.0, [CH]C[CH]:1.0]


ID=Reaction Center at Level: 2 (6)
[O=CCC(O)[CH2]:1.0, O=CC[CH]:1.0, OC([CH2])C(O)C(O)[CH2]:1.0, [C]CC(O)C([CH])O:1.0, [O]CC(O)C([CH])O:1.0, [O]CC(O)C[CH]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: O=[CH]>>[CH]O
2: [C]C([O])=C>>[C]C(=O)[CH2]
3: [C]=C>>[C]C[CH]
4: [C]O[P]>>[C]=O
5: [CH]C[CH]>>[CH]C([CH])O
6: [O]P(=O)(O)O>>O=P(O)(O)O
7: O=C[CH2]>>[CH]C(O)[CH2]
8: O>>[P]O
9: OC([CH2])[CH2]>>[CH]C(O)[CH2]

MMP Level 2
1: O=C[CH2]>>[CH]C(O)[CH2]
2: O=C(O)C(O[P])=C>>O=C(O)C(=O)C[CH]
3: [C]C([O])=C>>[C]C(=O)CC([CH])O
4: [C]C(OP(=O)(O)O)=C>>[C]C(=O)[CH2]
5: O=CCC(O)[CH2]>>OC([CH2])C(O)C(O)[CH2]
6: [C]OP(=O)(O)O>>O=P(O)(O)O
7: O=CC[CH]>>[C]CC(O)C([CH])O
8: O>>O=P(O)(O)O
9: [O]CC(O)C[CH]>>[O]CC(O)C([CH])O

MMP Level 3
1: O=CC[CH]>>[C]CC(O)C([CH])O
2: O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(=O)CC([CH])O
3: O=C(O)C(O[P])=C>>O=C(O)C(=O)CC(O)C([CH])O
4: O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(=O)C[CH]
5: [O]CC(O)CC=O>>[C]CC(O)C(O)C(O)C[O]
6: [C]C(OP(=O)(O)O)=C>>O=P(O)(O)O
7: O=CCC(O)[CH2]>>[C]C(=O)CC(O)C(O)C(O)[CH2]
8: O>>O=P(O)(O)O
9: O=CCC(O)CO[P]>>[P]OCC(O)C(O)C(O)[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(=O)CC(O)C(O)C(O)COP(=O)(O)O, O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(=O)CC(O)C(O)C(O)COP(=O)(O)O, O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(=O)CC(O)C(O)C(O)COP(=O)(O)O]
2: R:M00001, P:M00005	[O=C(O)C(OP(=O)(O)O)=C>>O=P(O)(O)O]
3: R:M00002, P:M00004	[O=CCC(O)COP(=O)(O)O>>O=C(O)C(=O)CC(O)C(O)C(O)COP(=O)(O)O, O=CCC(O)COP(=O)(O)O>>O=C(O)C(=O)CC(O)C(O)C(O)COP(=O)(O)O, O=CCC(O)COP(=O)(O)O>>O=C(O)C(=O)CC(O)C(O)C(O)COP(=O)(O)O, O=CCC(O)COP(=O)(O)O>>O=C(O)C(=O)CC(O)C(O)C(O)COP(=O)(O)O]
4: R:M00003, P:M00005	[O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[CH:2][CH2:3][CH:4]([OH:5])[CH2:6][O:7][P:8](=[O:9])([OH:10])[OH:11].[O:12]=[C:13]([OH:14])[C:15]([O:16][P:17](=[O:18])([OH:19])[OH:20])=[CH2:21].[OH2:22]>>[O:12]=[C:13]([OH:14])[C:15](=[O:16])[CH2:21][CH:2]([OH:1])[CH:3]([OH:23])[CH:4]([OH:5])[CH2:6][O:7][P:8](=[O:9])([OH:11])[OH:10].[O:18]=[P:17]([OH:19])([OH:20])[OH:22]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=21, 2=15, 3=13, 4=12, 5=14, 6=16, 7=17, 8=18, 9=19, 10=20, 11=3, 12=2, 13=1, 14=4, 15=6, 16=7, 17=8, 18=9, 19=10, 20=11, 21=5, 22=22}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=9, 4=11, 5=13, 6=14, 7=15, 8=16, 9=17, 10=18, 11=12, 12=10, 13=8, 14=4, 15=5, 16=2, 17=1, 18=3, 19=21, 20=20, 21=19, 22=22, 23=23}

