
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

//
FINGERPRINTS RC
[N#C:2.0, N#CC(O)(C)C:2.0, N#CC(O)(C)C>>N#C:2.0, N#CC(O)(C)C>>O=C(C)C:3.0, O=C(C)C:3.0, [CH]:1.0, [C]:3.0, [C]=O:1.0, [C]C#N:1.0, [C]C#N>>N#C:1.0, [C]C(O)(C)C:2.0, [C]C(O)(C)C>>O=C(C)C:2.0, [C]O:1.0, [C]O>>[C]=O:1.0, [OH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (1)
[[C]:2.0]


ID=Reaction Center at Level: 1 (2)
[[C]C#N:1.0, [C]C(O)(C)C:1.0]


ID=Reaction Center at Level: 2 (1)
[N#CC(O)(C)C:2.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C(C)C:1.0, [C]=O:1.0, [C]C(O)(C)C:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (3)
[N#CC(O)(C)C:1.0, O=C(C)C:2.0, [C]C(O)(C)C:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C#N>>N#C
2: [C]O>>[C]=O
3: [C]C(O)(C)C>>O=C(C)C

MMP Level 2
1: N#CC(O)(C)C>>N#C
2: [C]C(O)(C)C>>O=C(C)C
3: N#CC(O)(C)C>>O=C(C)C

MMP Level 3
1: N#CC(O)(C)C>>N#C
2: N#CC(O)(C)C>>O=C(C)C
3: N#CC(O)(C)C>>O=C(C)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[N#CC(O)(C)C>>N#C]
2: R:M00001, P:M00003	[N#CC(O)(C)C>>O=C(C)C, N#CC(O)(C)C>>O=C(C)C]


//
SELECTED AAM MAPPING
[N:1]#[C:2][C:3]([OH:4])([CH3:5])[CH3:6]>>[N:1]#[CH:2].[O:4]=[C:3]([CH3:5])[CH3:6]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=5, 2=3, 3=6, 4=2, 5=1, 6=4}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=5, 3=3, 4=2, 5=1, 6=4}

