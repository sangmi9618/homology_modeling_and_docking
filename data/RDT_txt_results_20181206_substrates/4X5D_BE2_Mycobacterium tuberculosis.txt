
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O=P(O)(O)O[P]>>O=P(O)(O[P])OC1O[CH][CH]C1O:1.0, [CH]:2.0, [C]C(=[CH])C(=C[CH])NC1O[CH][CH]C1O>>[C]C(=[CH])C(N)=C[CH]:1.0, [C]C(=[CH])N:1.0, [C]C(=[CH])NC([O])[CH]:1.0, [C]C(=[CH])NC([O])[CH]>>[C]C(=[CH])N:1.0, [C]C(=[CH])NC1OC([CH2])C(O)C1O>>[O]P(=O)(O)OC1OC([CH2])C(O)C1O:1.0, [C]N:1.0, [C]NC1O[CH][CH]C1O:1.0, [C]NC1O[CH][CH]C1O>>[P]OC1O[CH][CH]C1O:1.0, [C]N[CH]:1.0, [C]N[CH]>>[C]N:1.0, [NH2]:1.0, [NH]:1.0, [OH]:1.0, [O]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [O]C([CH])[NH]:1.0, [O]C([CH])[NH]>>[O]C([O])[CH]:1.0, [O]C([O])[CH]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>[O]C([CH])OP([O])(=O)O:1.0, [P]O:1.0, [P]O>>[P]O[CH]:1.0, [P]OC1O[CH][CH]C1O:1.0, [P]O[CH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [NH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[C]N[CH]:1.0, [O]C([CH])[NH]:1.0, [O]C([O])[CH]:1.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]C(=[CH])NC([O])[CH]:1.0, [C]NC1O[CH][CH]C1O:1.0, [O]C([CH])OP([O])(=O)O:1.0, [P]OC1O[CH][CH]C1O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O>>[P]O[CH]
2: [C]N[CH]>>[C]N
3: [O]C([CH])[NH]>>[O]C([O])[CH]

MMP Level 2
1: [O]P(=O)(O)O>>[O]C([CH])OP([O])(=O)O
2: [C]C(=[CH])NC([O])[CH]>>[C]C(=[CH])N
3: [C]NC1O[CH][CH]C1O>>[P]OC1O[CH][CH]C1O

MMP Level 3
1: O=P(O)(O)O[P]>>O=P(O)(O[P])OC1O[CH][CH]C1O
2: [C]C(=[CH])C(=C[CH])NC1O[CH][CH]C1O>>[C]C(=[CH])C(N)=C[CH]
3: [C]C(=[CH])NC1OC([CH2])C(O)C1O>>[O]P(=O)(O)OC1OC([CH2])C(O)C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)c1ccccc1NC2OC(COP(=O)(O)O)C(O)C2O>>O=C(O)c1ccccc1N]
2: R:M00001, P:M00004	[O=C(O)c1ccccc1NC2OC(COP(=O)(O)O)C(O)C2O>>O=P(O)(O)OCC1OC(OP(=O)(O)OP(=O)(O)O)C(O)C1O]
3: R:M00002, P:M00004	[O=P(O)(O)OP(=O)(O)O>>O=P(O)(O)OCC1OC(OP(=O)(O)OP(=O)(O)O)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[C:4]:1:[CH:5]:[CH:6]:[CH:7]:[CH:8]:[C:9]1[NH:10][CH:11]2[O:12][CH:13]([CH2:14][O:15][P:16](=[O:17])([OH:18])[OH:19])[CH:20]([OH:21])[CH:22]2[OH:23].[O:24]=[P:25]([OH:26])([OH:27])[O:28][P:29](=[O:30])([OH:31])[OH:32]>>[O:1]=[C:2]([OH:3])[C:4]:1:[CH:5]:[CH:6]:[CH:7]:[CH:8]:[C:9]1[NH2:10].[O:17]=[P:16]([OH:19])([OH:18])[O:15][CH2:14][CH:13]1[O:12][CH:11]([O:26][P:25](=[O:24])([OH:27])[O:28][P:29](=[O:30])([OH:31])[OH:32])[CH:22]([OH:23])[CH:20]1[OH:21]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=8, 4=9, 5=4, 6=5, 7=2, 8=1, 9=3, 10=10, 11=11, 12=22, 13=20, 14=13, 15=12, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=21, 23=23, 24=26, 25=25, 26=24, 27=27, 28=28, 29=29, 30=30, 31=31, 32=32}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=28, 2=29, 3=30, 4=31, 5=26, 6=27, 7=24, 8=23, 9=25, 10=32, 11=6, 12=7, 13=21, 14=19, 15=9, 16=8, 17=10, 18=11, 19=12, 20=13, 21=14, 22=15, 23=16, 24=17, 25=18, 26=20, 27=22, 28=5, 29=2, 30=1, 31=3, 32=4}

