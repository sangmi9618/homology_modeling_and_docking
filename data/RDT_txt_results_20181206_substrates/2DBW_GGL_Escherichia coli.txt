
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(N[CH])C[CH2]:1.0, O=C(N[CH])C[CH2]>>O=C(O)C[CH2]:1.0, O=C(O)C[CH2]:1.0, O=C(O)[CH2]:2.0, O=C([NH])C(NC(=O)C[CH2])CS>>O=C([NH])C(N)CS:1.0, O=C([NH])[CH2]:1.0, O=C([NH])[CH2]>>O=C(O)[CH2]:1.0, O>>O=C(O)C[CH2]:1.0, O>>O=C(O)[CH2]:1.0, O>>[C]O:1.0, [CH]N:1.0, [C]:2.0, [C]C([CH2])N:1.0, [C]C([CH2])NC(=O)CC[CH]>>O=C(O)CC[CH]:1.0, [C]C([CH2])NC(=O)[CH2]:1.0, [C]C([CH2])NC(=O)[CH2]>>[C]C([CH2])N:1.0, [C]N[CH]:1.0, [C]N[CH]>>[CH]N:1.0, [C]O:1.0, [NH2]:1.0, [NH]:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [C]:2.0, [NH]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=C(O)[CH2]:1.0, O=C([NH])[CH2]:1.0, [C]N[CH]:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=C(N[CH])C[CH2]:1.0, O=C(O)C[CH2]:1.0, O=C(O)[CH2]:1.0, [C]C([CH2])NC(=O)[CH2]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]N[CH]>>[CH]N
2: O>>[C]O
3: O=C([NH])[CH2]>>O=C(O)[CH2]

MMP Level 2
1: [C]C([CH2])NC(=O)[CH2]>>[C]C([CH2])N
2: O>>O=C(O)[CH2]
3: O=C(N[CH])C[CH2]>>O=C(O)C[CH2]

MMP Level 3
1: O=C([NH])C(NC(=O)C[CH2])CS>>O=C([NH])C(N)CS
2: O>>O=C(O)C[CH2]
3: [C]C([CH2])NC(=O)CC[CH]>>O=C(O)CC[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)CNC(=O)C(NC(=O)CCC(N)C(=O)O)CS>>O=C(O)CNC(=O)C(N)CS]
2: R:M00001, P:M00004	[O=C(O)CNC(=O)C(NC(=O)CCC(N)C(=O)O)CS>>O=C(O)CCC(N)C(=O)O]
3: R:M00002, P:M00004	[O>>O=C(O)CCC(N)C(=O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH2:4][NH:5][C:6](=[O:7])[CH:8]([NH:9][C:10](=[O:11])[CH2:12][CH2:13][CH:14]([NH2:15])[C:16](=[O:17])[OH:18])[CH2:19][SH:20].[OH2:21]>>[O:1]=[C:2]([OH:3])[CH2:4][NH:5][C:6](=[O:7])[CH:8]([NH2:9])[CH2:19][SH:20].[O:11]=[C:10]([OH:21])[CH2:12][CH2:13][CH:14]([NH2:15])[C:16](=[O:17])[OH:18]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=13, 2=12, 3=10, 4=11, 5=9, 6=8, 7=19, 8=20, 9=6, 10=7, 11=5, 12=4, 13=2, 14=1, 15=3, 16=14, 17=16, 18=17, 19=18, 20=15, 21=21}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=10, 2=8, 3=6, 4=7, 5=5, 6=4, 7=2, 8=1, 9=3, 10=9, 11=11, 12=16, 13=15, 14=13, 15=12, 16=14, 17=17, 18=19, 19=20, 20=21, 21=18}

