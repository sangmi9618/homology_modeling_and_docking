
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=CNC(C)CO>>OCC(N)C:1.0, O=CNC([CH2])C:1.0, O=CNC([CH2])C>>O=CO:1.0, O=CNC([CH2])C>>[CH2]C(N)C:1.0, O=CN[CH]:1.0, O=CN[CH]>>O=CO:1.0, O=CO:3.0, O=C[NH]:1.0, O=C[NH]>>O=CO:1.0, O>>O=CO:2.0, O>>[CH]O:1.0, [CH2]C(N)C:1.0, [CH]:2.0, [CH]N:1.0, [CH]N[CH]:1.0, [CH]N[CH]>>[CH]N:1.0, [CH]O:1.0, [NH2]:1.0, [NH]:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [CH]:2.0, [NH]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=CO:1.0, O=C[NH]:1.0, [CH]N[CH]:1.0, [CH]O:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=CNC([CH2])C:1.0, O=CN[CH]:1.0, O=CO:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=C[NH]>>O=CO
2: O>>[CH]O
3: [CH]N[CH]>>[CH]N

MMP Level 2
1: O=CN[CH]>>O=CO
2: O>>O=CO
3: O=CNC([CH2])C>>[CH2]C(N)C

MMP Level 3
1: O=CNC([CH2])C>>O=CO
2: O>>O=CO
3: O=CNC(C)CO>>OCC(N)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=CNC(C)CO>>OCC(N)C]
2: R:M00001, P:M00004	[O=CNC(C)CO>>O=CO]
3: R:M00002, P:M00004	[O>>O=CO]


//
SELECTED AAM MAPPING
[O:1]=[CH:2][NH:3][CH:4]([CH3:5])[CH2:6][OH:7].[OH2:8]>>[O:1]=[CH:2][OH:8].[OH:7][CH2:6][CH:4]([NH2:3])[CH3:5]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=5, 2=4, 3=6, 4=7, 5=3, 6=2, 7=1, 8=8}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=5, 2=3, 3=2, 4=1, 5=4, 6=7, 7=6, 8=8}

