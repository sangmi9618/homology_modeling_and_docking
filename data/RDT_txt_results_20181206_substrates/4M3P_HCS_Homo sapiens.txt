
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-S:1.0]

//
FINGERPRINTS RC
[O=C(O)[CH2]:1.0, S(C)C[CH2]:1.0, S([CH2])C:2.0, SC[CH2]:1.0, SC[CH2]>>S(C)C[CH2]:1.0, S[CH2]:1.0, S[CH2]>>S([CH2])C:1.0, [CH2]N(C)C:1.0, [CH2][N+](C)(C)C:2.0, [CH2][N+](C)(C)C>>S([CH2])C:1.0, [CH2][N+](C)(C)C>>[CH2]N(C)C:1.0, [CH3]:2.0, [CH]CCS>>[CH]CCSC:1.0, [C]CN(C)C:1.0, [C]C[N+](C)(C)C:1.0, [C]C[N+](C)(C)C>>S(C)C[CH2]:1.0, [C]C[N+](C)(C)C>>[C]CN(C)C:1.0, [C]O:1.0, [C][O-]:1.0, [C][O-]>>[C]O:1.0, [N+]:1.0, [N+]C:1.0, [N+]C>>[S]C:1.0, [N+]CC([O-])=O>>[N]CC(=O)O:1.0, [N]:1.0, [O-]:1.0, [O-]C(=O)C[N+](C)(C)C>>O=C(O)CN(C)C:1.0, [O-]C(=O)[CH2]:1.0, [O-]C(=O)[CH2]>>O=C(O)[CH2]:1.0, [OH]:1.0, [SH]:1.0, [S]:1.0, [S]C:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH3]:2.0, [N+]:1.0, [S]:1.0]


ID=Reaction Center at Level: 1 (4)
[S([CH2])C:1.0, [CH2][N+](C)(C)C:1.0, [N+]C:1.0, [S]C:1.0]


ID=Reaction Center at Level: 2 (4)
[S(C)C[CH2]:1.0, S([CH2])C:1.0, [CH2][N+](C)(C)C:1.0, [C]C[N+](C)(C)C:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: S[CH2]>>S([CH2])C
2: [N+]C>>[S]C
3: [C][O-]>>[C]O
4: [CH2][N+](C)(C)C>>[CH2]N(C)C

MMP Level 2
1: SC[CH2]>>S(C)C[CH2]
2: [CH2][N+](C)(C)C>>S([CH2])C
3: [O-]C(=O)[CH2]>>O=C(O)[CH2]
4: [C]C[N+](C)(C)C>>[C]CN(C)C

MMP Level 3
1: [CH]CCS>>[CH]CCSC
2: [C]C[N+](C)(C)C>>S(C)C[CH2]
3: [N+]CC([O-])=O>>[N]CC(=O)O
4: [O-]C(=O)C[N+](C)(C)C>>O=C(O)CN(C)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)CCS>>O=C(O)C(N)CCSC]
2: R:M00002, P:M00003	[[O-]C(=O)C[N+](C)(C)C>>O=C(O)C(N)CCSC]
3: R:M00002, P:M00004	[[O-]C(=O)C[N+](C)(C)C>>O=C(O)CN(C)C, [O-]C(=O)C[N+](C)(C)C>>O=C(O)CN(C)C]


//
SELECTED AAM MAPPING
[O:9]=[C:10]([O-:11])[CH2:12][N+:13]([CH3:14])([CH3:15])[CH3:16].[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][SH:8]>>[O:9]=[C:10]([OH:11])[CH2:12][N:13]([CH3:14])[CH3:15].[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][S:8][CH3:16]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=8, 4=4, 5=2, 6=1, 7=3, 8=5, 9=14, 10=13, 11=15, 12=16, 13=12, 14=10, 15=9, 16=11}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=4, 6=2, 7=1, 8=3, 9=5, 10=15, 11=14, 12=16, 13=13, 14=11, 15=10, 16=12}

