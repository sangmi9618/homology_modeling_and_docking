
//
FINGERPRINTS BC

FORMED_CLEAVED
[C=N:2.0]

//
FINGERPRINTS RC
[N(=C(N)N)C[CH2]:1.0, N([CH2])=C(N)N:2.0, N([CH2])=C(N)N>>N([CH2])=C(N)N:1.0, O=C(O)CN=C(N)N>>O=C(O)CN:1.0, [CH2]CCN>>N(=C(N)N)CC[CH2]:1.0, [CH2]CN:1.0, [CH2]CN>>N(=C(N)N)C[CH2]:1.0, [CH2]N:2.0, [CH2]N>>[C]=N[CH2]:1.0, [C]:2.0, [C]=N[CH2]:2.0, [C]=N[CH2]>>[CH2]N:1.0, [C]CN:1.0, [C]CN=C(N)N:1.0, [C]CN=C(N)N>>N(=C(N)N)C[CH2]:1.0, [C]CN=C(N)N>>[C]CN:1.0, [NH2]:2.0, [N]:2.0, [N]=C(N)N:2.0, [N]=C(N)N>>[N]=C(N)N:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[C]:2.0, [N]:2.0]


ID=Reaction Center at Level: 1 (2)
[[C]=N[CH2]:2.0, [N]=C(N)N:2.0]


ID=Reaction Center at Level: 2 (3)
[N(=C(N)N)C[CH2]:1.0, N([CH2])=C(N)N:2.0, [C]CN=C(N)N:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [N]=C(N)N>>[N]=C(N)N
2: [C]=N[CH2]>>[CH2]N
3: [CH2]N>>[C]=N[CH2]

MMP Level 2
1: N([CH2])=C(N)N>>N([CH2])=C(N)N
2: [C]CN=C(N)N>>[C]CN
3: [CH2]CN>>N(=C(N)N)C[CH2]

MMP Level 3
1: [C]CN=C(N)N>>N(=C(N)N)C[CH2]
2: O=C(O)CN=C(N)N>>O=C(O)CN
3: [CH2]CCN>>N(=C(N)N)CC[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)CCCN>>O=C(O)C(N)CCCN=C(N)N]
2: R:M00002, P:M00003	[O=C(O)CN=C(N)N>>O=C(O)C(N)CCCN=C(N)N]
3: R:M00002, P:M00004	[O=C(O)CN=C(N)N>>O=C(O)CN]


//
SELECTED AAM MAPPING
[O:10]=[C:11]([OH:12])[CH2:13][N:14]=[C:15]([NH2:16])[NH2:17].[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][CH2:8][NH2:9]>>[O:10]=[C:11]([OH:12])[CH2:13][NH2:14].[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][CH2:8][N:9]=[C:15]([NH2:16])[NH2:17]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=7, 2=6, 3=4, 4=2, 5=1, 6=3, 7=5, 8=8, 9=9, 10=13, 11=11, 12=10, 13=12, 14=14, 15=15, 16=16, 17=17}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=7, 2=6, 3=4, 4=2, 5=1, 6=3, 7=5, 8=8, 9=9, 10=10, 11=11, 12=12, 13=16, 14=14, 15=13, 16=15, 17=17}

