
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)O[CH2]>>O=P(O)(O[CH2])OP(=O)(O)O[CH2]:1.0, O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[CH2]:2.0, O=P(O)(O[P])O[CH2]>>O=P(O)(O[P])O[CH2]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>[O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP(=O)(O)OC[CH]>>[O]P(=O)(O)OP(=O)(O)OC[CH]:1.0, [O]P(=O)(O)OP([O])(=O)O:2.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:2.0, [O]P([O])(=O)O>>[O]P([O])(=O)O:1.0, [P]:2.0, [P]O:2.0, [P]O>>[P]O[P]:1.0, [P]O[P]:2.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]P([O])(=O)O:2.0, [P]O[P]:2.0]


ID=Reaction Center at Level: 2 (2)
[O=P(O)(O[P])O[CH2]:2.0, [O]P(=O)(O)OP([O])(=O)O:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]P([O])(=O)O>>[O]P([O])(=O)O
2: [P]O>>[P]O[P]
3: [P]O[P]>>[P]O

MMP Level 2
1: O=P(O)(O[P])O[CH2]>>O=P(O)(O[P])O[CH2]
2: [O]P(=O)(O)O>>[O]P(=O)(O)OP([O])(=O)O
3: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O

MMP Level 3
1: [O]P(=O)(O)OP(=O)(O)OC[CH]>>[O]P(=O)(O)OP(=O)(O)OC[CH]
2: O=P(O)(O)O[CH2]>>O=P(O)(O[CH2])OP(=O)(O)O[CH2]
3: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2O>>O=P(O)(O)OP(=O)(O)O]
2: R:M00001, P:M00004	[O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2O>>O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)OCC(O)C(O)(C)CO)C(O)C2O]
3: R:M00002, P:M00004	[O=P(O)(O)OCC(O)C(O)(C)CO>>O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)OCC(O)C(O)(C)CO)C(O)C2O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[CH:6]=[CH:7][N:8]1[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][P:22](=[O:23])([OH:24])[OH:25])[CH:26]([OH:27])[CH:28]2[OH:29].[O:30]=[P:31]([OH:32])([OH:33])[O:34][CH2:35][CH:36]([OH:37])[C:38]([OH:39])([CH3:40])[CH2:41][OH:42]>>[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[CH:6]=[CH:7][N:8]1[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:32][P:31](=[O:30])([OH:33])[O:34][CH2:35][CH:36]([OH:37])[C:38]([OH:39])([CH3:40])[CH2:41][OH:42])[CH:26]([OH:27])[CH:28]2[OH:29].[O:19]=[P:18]([OH:17])([OH:20])[O:21][P:22](=[O:23])([OH:24])[OH:25]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=8, 4=2, 5=1, 6=3, 7=4, 8=5, 9=9, 10=28, 11=26, 12=11, 13=10, 14=12, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=25, 28=27, 29=29, 30=40, 31=38, 32=41, 33=42, 34=36, 35=35, 36=34, 37=31, 38=30, 39=32, 40=33, 41=37, 42=39}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=36, 2=35, 3=34, 4=37, 5=38, 6=39, 7=40, 8=41, 9=42, 10=27, 11=25, 12=28, 13=29, 14=23, 15=22, 16=21, 17=18, 18=19, 19=20, 20=17, 21=14, 22=15, 23=16, 24=13, 25=12, 26=11, 27=30, 28=32, 29=9, 30=10, 31=8, 32=7, 33=6, 34=4, 35=3, 36=2, 37=1, 38=5, 39=33, 40=31, 41=24, 42=26}

