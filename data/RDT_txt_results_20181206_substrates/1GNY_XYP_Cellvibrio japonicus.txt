
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O>>[CH]C(O)C(O)O[CH2]:1.0, O>>[CH]O:1.0, O>>[O]C([CH])O:1.0, [CH]:2.0, [CH]C(=[CH])O:1.0, [CH]C(=[CH])OC1OC[CH]C(O)C1O>>OC1OC[CH]C(O)C1O:1.0, [CH]C(O)C(O)O[CH2]:1.0, [CH]C=C(OC(O[CH2])C([CH])O)C=[CH]>>[CH]C=C(O)C=[CH]:1.0, [CH]O:1.0, [C]O:1.0, [C]OC(O[CH2])C([CH])O:1.0, [C]OC(O[CH2])C([CH])O>>[CH]C(O)C(O)O[CH2]:1.0, [C]O[CH]:1.0, [C]O[CH]>>[C]O:1.0, [OH]:2.0, [O]:1.0, [O]C([CH])O:2.0, [O]C([CH])OC([CH])=[CH]:1.0, [O]C([CH])OC([CH])=[CH]>>[CH]C(=[CH])O:1.0, [O]C([O])[CH]:1.0, [O]C([O])[CH]>>[O]C([CH])O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [CH]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, [CH]O:1.0, [C]O[CH]:1.0, [O]C([CH])O:1.0, [O]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, [CH]C(O)C(O)O[CH2]:1.0, [C]OC(O[CH2])C([CH])O:1.0, [O]C([CH])O:1.0, [O]C([CH])OC([CH])=[CH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]C([CH])O:1.0, [O]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[CH]C(O)C(O)O[CH2]:1.0, [C]OC(O[CH2])C([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]O[CH]>>[C]O
2: O>>[CH]O
3: [O]C([O])[CH]>>[O]C([CH])O

MMP Level 2
1: [O]C([CH])OC([CH])=[CH]>>[CH]C(=[CH])O
2: O>>[O]C([CH])O
3: [C]OC(O[CH2])C([CH])O>>[CH]C(O)C(O)O[CH2]

MMP Level 3
1: [CH]C=C(OC(O[CH2])C([CH])O)C=[CH]>>[CH]C=C(O)C=[CH]
2: O>>[CH]C(O)C(O)O[CH2]
3: [CH]C(=[CH])OC1OC[CH]C(O)C1O>>OC1OC[CH]C(O)C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[[O-][N+](=O)c1ccc(OC2OCC(O)C(O)C2O)cc1>>[O-][N+](=O)c1ccc(O)cc1]
2: R:M00001, P:M00004	[[O-][N+](=O)c1ccc(OC2OCC(O)C(O)C2O)cc1>>OC1OCC(O)C(O)C1O]
3: R:M00002, P:M00004	[O>>OC1OCC(O)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[N+:2]([O-:3])[C:4]:1:[CH:5]:[CH:6]:[C:7]([O:8][CH:9]2[O:10][CH2:11][CH:12]([OH:13])[CH:14]([OH:15])[CH:16]2[OH:17]):[CH:18]:[CH:19]1.[OH2:20]>>[O:1]=[N+:2]([O-:3])[C:4]:1:[CH:5]:[CH:6]:[C:7]([OH:8]):[CH:18]:[CH:19]1.[OH:20][CH:9]1[O:10][CH2:11][CH:12]([OH:13])[CH:14]([OH:15])[CH:16]1[OH:17]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=11, 2=12, 3=14, 4=16, 5=9, 6=10, 7=8, 8=7, 9=6, 10=5, 11=4, 12=19, 13=18, 14=2, 15=1, 16=3, 17=17, 18=15, 19=13, 20=20}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=15, 2=16, 3=17, 4=19, 5=20, 6=14, 7=12, 8=11, 9=13, 10=18, 11=4, 12=5, 13=7, 14=9, 15=2, 16=3, 17=1, 18=10, 19=8, 20=6}

