
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0, O-P:2.0]

ORDER_CHANGED
[C-C*C=C:1.0, C-O*C=O:2.0]

//
FINGERPRINTS RC
[O=C(O)C(=O)CC(=O)O>>O=C(O)C(OP(=O)(O)O)=C:1.0, O=C(O)C(=O)CC(=O)O>>O=C(O)C(O[P])=C:1.0, O=C(O)C(O[P])=C:1.0, O=C(O)[CH2]:2.0, O=C(O)[CH2]>>O=C=O:2.0, O=C=O:3.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>[C]OP(=O)(O)O:1.0, [CH2]:2.0, [C]:4.0, [C]=C:1.0, [C]=O:2.0, [C]=O>>[C]O[P]:1.0, [C]C(=O)CC(=O)O:1.0, [C]C(=O)CC(=O)O>>O=C=O:1.0, [C]C(=O)CC(=O)O>>[C]C([O])=C:1.0, [C]C(=O)[CH2]:2.0, [C]C(=O)[CH2]>>[C]C(OP(=O)(O)O)=C:1.0, [C]C(=O)[CH2]>>[C]C([O])=C:1.0, [C]C(OP(=O)(O)O)=C:1.0, [C]C([O])=C:2.0, [C]CC(=O)C(=O)O:1.0, [C]CC(=O)C(=O)O>>O=C(O)C(OP(=O)(O)O)=C:1.0, [C]CC(=O)C(=O)O>>O=C(O)C(O[P])=C:1.0, [C]CC(=O)O:1.0, [C]CC(=O)O>>O=C=O:2.0, [C]C[C]:1.0, [C]C[C]>>[C]=C:1.0, [C]O:1.0, [C]O>>[C]=O:1.0, [C]OP(=O)(O)O:1.0, [C]O[P]:1.0, [OH]:2.0, [O]:4.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[C]C(OP(=O)(O)O)=C:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[CH2]:1.0, [C]:1.0, [O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[O=C(O)[CH2]:1.0, [C]C[C]:1.0, [C]O[P]:1.0, [O]P(=O)(O)O:2.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (6)
[O=P(O)(O)O[P]:1.0, [C]C(=O)CC(=O)O:1.0, [C]C(OP(=O)(O)O)=C:1.0, [C]CC(=O)O:1.0, [C]OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH2]:2.0, [C]:4.0, [OH]:1.0, [O]:3.0]


ID=Reaction Center at Level: 1 (9)
[O=C(O)[CH2]:1.0, O=C=O:1.0, [C]=C:1.0, [C]=O:2.0, [C]C(=O)[CH2]:1.0, [C]C([O])=C:1.0, [C]C[C]:1.0, [C]O:1.0, [C]O[P]:1.0]


ID=Reaction Center at Level: 2 (9)
[O=C(O)C(O[P])=C:1.0, O=C(O)[CH2]:1.0, O=C=O:2.0, [C]C(=O)CC(=O)O:1.0, [C]C(=O)[CH2]:1.0, [C]C(OP(=O)(O)O)=C:1.0, [C]C([O])=C:1.0, [C]CC(=O)C(=O)O:1.0, [C]CC(=O)O:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]P(=O)(O)O>>[O]P(=O)(O)O
2: [C]O>>[C]=O
3: [C]=O>>[C]O[P]
4: [C]C(=O)[CH2]>>[C]C([O])=C
5: O=C(O)[CH2]>>O=C=O
6: [C]C[C]>>[C]=C
7: [P]O[P]>>[P]O

MMP Level 2
1: O=P(O)(O)O[P]>>[C]OP(=O)(O)O
2: O=C(O)[CH2]>>O=C=O
3: [C]C(=O)[CH2]>>[C]C(OP(=O)(O)O)=C
4: [C]CC(=O)C(=O)O>>O=C(O)C(O[P])=C
5: [C]CC(=O)O>>O=C=O
6: [C]C(=O)CC(=O)O>>[C]C([O])=C
7: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O

MMP Level 3
1: [O]P(=O)(O)OP(=O)(O)O>>[C]C(OP(=O)(O)O)=C
2: [C]CC(=O)O>>O=C=O
3: [C]CC(=O)C(=O)O>>O=C(O)C(OP(=O)(O)O)=C
4: O=C(O)C(=O)CC(=O)O>>O=C(O)C(OP(=O)(O)O)=C
5: [C]C(=O)CC(=O)O>>O=C=O
6: O=C(O)C(=O)CC(=O)O>>O=C(O)C(O[P])=C
7: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C3O>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)O)C(O)C3O]
2: R:M00001, P:M00004	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C3O>>O=C(O)C(OP(=O)(O)O)=C]
3: R:M00002, P:M00004	[O=C(O)C(=O)CC(=O)O>>O=C(O)C(OP(=O)(O)O)=C, O=C(O)C(=O)CC(=O)O>>O=C(O)C(OP(=O)(O)O)=C, O=C(O)C(=O)CC(=O)O>>O=C(O)C(OP(=O)(O)O)=C]
4: R:M00002, P:M00005	[O=C(O)C(=O)CC(=O)O>>O=C=O, O=C(O)C(=O)CC(=O)O>>O=C=O]


//
SELECTED AAM MAPPING
[O:33]=[C:34]([OH:35])[C:36](=[O:37])[CH2:38][C:39](=[O:40])[OH:41].[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[C:8]1:[N:9]:[CH:10]:[N:11]2[CH:12]3[O:13][CH:14]([CH2:15][O:16][P:17](=[O:18])([OH:19])[O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[OH:28])[CH:29]([OH:30])[CH:31]3[OH:32]>>[O:40]=[C:39]=[O:41].[O:33]=[C:34]([OH:35])[C:36]([O:37][P:25](=[O:26])([OH:27])[OH:28])=[CH2:38].[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[C:8]1:[N:9]:[CH:10]:[N:11]2[CH:12]3[O:13][CH:14]([CH2:15][O:16][P:17](=[O:18])([OH:19])[O:20][P:21](=[O:22])([OH:23])[OH:24])[CH:29]([OH:30])[CH:31]3[OH:32]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=10, 2=9, 3=8, 4=7, 5=11, 6=12, 7=31, 8=29, 9=14, 10=13, 11=15, 12=16, 13=17, 14=18, 15=19, 16=20, 17=21, 18=22, 19=23, 20=24, 21=25, 22=26, 23=27, 24=28, 25=30, 26=32, 27=6, 28=4, 29=3, 30=2, 31=1, 32=5, 33=38, 34=36, 35=37, 36=34, 37=33, 38=35, 39=39, 40=40, 41=41}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=10, 2=9, 3=8, 4=7, 5=11, 6=12, 7=27, 8=25, 9=14, 10=13, 11=15, 12=16, 13=17, 14=18, 15=19, 16=20, 17=21, 18=22, 19=23, 20=24, 21=26, 22=28, 23=6, 24=4, 25=3, 26=2, 27=1, 28=5, 29=38, 30=32, 31=30, 32=29, 33=31, 34=33, 35=34, 36=35, 37=36, 38=37, 39=40, 40=39, 41=41}

