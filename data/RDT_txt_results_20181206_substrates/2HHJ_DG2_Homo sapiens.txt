
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=C(OP(=O)(O)O)C(O)[CH2]>>[O]C([CH2])C(=O)O:1.0, O=C([CH])O:1.0, O=C([CH])OP(=O)(O)O:1.0, O=C([CH])OP(=O)(O)O>>O=C([CH])O:1.0, O=C([CH])OP(=O)(O)O>>[C]C(OP(=O)(O)O)[CH2]:1.0, O=P(O)(O)O[CH]:1.0, [CH]O:1.0, [CH]O>>[P]O[CH]:1.0, [C]C(O)[CH2]:1.0, [C]C(O)[CH2]>>[C]C(OP(=O)(O)O)[CH2]:1.0, [C]C(OP(=O)(O)O)[CH2]:1.0, [C]O:1.0, [C]OP(=O)(O)O:1.0, [C]OP(=O)(O)O>>O=P(O)(O)O[CH]:1.0, [C]O[P]:1.0, [C]O[P]>>[C]O:1.0, [OH]:2.0, [O]:2.0, [O]C(=O)C(O)C[O]>>[O]CC(OP(=O)(O)O)C(=O)O:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O[CH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[C]O[P]:1.0, [O]P(=O)(O)O:2.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C([CH])OP(=O)(O)O:1.0, O=P(O)(O)O[CH]:1.0, [C]C(OP(=O)(O)O)[CH2]:1.0, [C]OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]O[P]>>[C]O
2: [O]P(=O)(O)O>>[O]P(=O)(O)O
3: [CH]O>>[P]O[CH]

MMP Level 2
1: O=C([CH])OP(=O)(O)O>>O=C([CH])O
2: [C]OP(=O)(O)O>>O=P(O)(O)O[CH]
3: [C]C(O)[CH2]>>[C]C(OP(=O)(O)O)[CH2]

MMP Level 3
1: O=C(OP(=O)(O)O)C(O)[CH2]>>[O]C([CH2])C(=O)O
2: O=C([CH])OP(=O)(O)O>>[C]C(OP(=O)(O)O)[CH2]
3: [O]C(=O)C(O)C[O]>>[O]CC(OP(=O)(O)O)C(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(OP(=O)(O)O)C(O)COP(=O)(O)O>>O=C(O)C(OP(=O)(O)O)COP(=O)(O)O, O=C(OP(=O)(O)O)C(O)COP(=O)(O)O>>O=C(O)C(OP(=O)(O)O)COP(=O)(O)O, O=C(OP(=O)(O)O)C(O)COP(=O)(O)O>>O=C(O)C(OP(=O)(O)O)COP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([O:3][P:4](=[O:5])([OH:6])[OH:7])[CH:8]([OH:9])[CH2:10][O:11][P:12](=[O:13])([OH:14])[OH:15]>>[O:1]=[C:2]([OH:3])[CH:8]([O:9][P:4](=[O:5])([OH:6])[OH:7])[CH2:10][O:11][P:12](=[O:13])([OH:14])[OH:15]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=10, 2=8, 3=2, 4=1, 5=3, 6=4, 7=5, 8=6, 9=7, 10=9, 11=11, 12=12, 13=13, 14=14, 15=15}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=10, 2=4, 3=2, 4=1, 5=3, 6=5, 7=6, 8=7, 9=8, 10=9, 11=11, 12=12, 13=13, 14=14, 15=15}

