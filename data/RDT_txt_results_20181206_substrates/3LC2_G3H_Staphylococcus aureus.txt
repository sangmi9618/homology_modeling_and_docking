
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0]

//
FINGERPRINTS RC
[O=C(N)C=1C=[N+]C=CC1>>O=C(N)C1=C[N]C=CC1:1.0, O=C(O[P])C(O)[CH2]:1.0, O=C([CH])OP(=O)(O)O:1.0, O=CC(O)[CH2]:1.0, O=CC(O)[CH2]>>O=C(O[P])C(O)[CH2]:1.0, O=C[CH]:1.0, O=C[CH]>>[O]C(=O)[CH]:1.0, O=P(O)(O)O:1.0, O=P(O)(O)O>>O=C(OP(=O)(O)O)C(O)[CH2]:1.0, O=P(O)(O)O>>O=C([CH])OP(=O)(O)O:1.0, [CH2]:1.0, [CH]:2.0, [CH]N([CH])[CH]:1.0, [CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]>>[CH]N([CH])[CH]:1.0, [C]:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN(C=[CH])C([O])[CH]:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])C=C[CH]>>[C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C=[CH]:1.0, [C]C=[CH]>>[C]C[CH]:1.0, [C]C[CH]:1.0, [C]O[P]:1.0, [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]C1=CN(C=CC1)C2O[CH][CH]C2O:1.0, [N+]:1.0, [N]:1.0, [OH]:1.0, [O]:1.0, [O]C(=O)[CH]:1.0, [O]CC(O)C=O>>[O]CC(O)C(=O)OP(=O)(O)O:1.0, [P]O:1.0, [P]O>>[C]O[P]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[C]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]O[P]:1.0, [O]C(=O)[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O[P])C(O)[CH2]:1.0, O=C([CH])OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C=[CH]>>[C]C[CH]
2: O=C[CH]>>[O]C(=O)[CH]
3: [CH][N+]([CH])=[CH]>>[CH]N([CH])[CH]
4: [P]O>>[C]O[P]

MMP Level 2
1: [C]C(=[CH])C=C[CH]>>[C]C(=[CH])CC=[CH]
2: O=CC(O)[CH2]>>O=C(O[P])C(O)[CH2]
3: [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN(C=[CH])C([O])[CH]
4: O=P(O)(O)O>>O=C([CH])OP(=O)(O)O

MMP Level 3
1: O=C(N)C=1C=[N+]C=CC1>>O=C(N)C1=C[N]C=CC1
2: [O]CC(O)C=O>>[O]CC(O)C(=O)OP(=O)(O)O
3: [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]C1=CN(C=CC1)C2O[CH][CH]C2O
4: O=P(O)(O)O>>O=C(OP(=O)(O)O)C(O)[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00005	[O=P(O)(O)O>>O=C(OP(=O)(O)O)C(O)COP(=O)(O)O]
2: R:M00002, P:M00005	[O=CC(O)COP(=O)(O)O>>O=C(OP(=O)(O)O)C(O)COP(=O)(O)O]
3: R:M00003, P:M00004	[O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O, O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O]


//
SELECTED AAM MAPPING
[O:45]=[CH:46][CH:47]([OH:48])[CH2:49][O:50][P:51](=[O:52])([OH:53])[OH:54].[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:5]:[CH:6]:[CH:7]:[N+:8](:[CH:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44].[O:55]=[P:56]([OH:57])([OH:58])[OH:59]>>[O:1]=[C:2]([NH2:3])[C:4]1=[CH:9][N:8]([CH:7]=[CH:6][CH2:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44].[O:45]=[C:46]([O:57][P:56](=[O:55])([OH:58])[OH:59])[CH:47]([OH:48])[CH2:49][O:50][P:51](=[O:52])([OH:53])[OH:54]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=57, 2=56, 3=55, 4=58, 5=59, 6=49, 7=47, 8=46, 9=45, 10=48, 11=50, 12=51, 13=52, 14=53, 15=54, 16=6, 17=5, 18=4, 19=9, 20=8, 21=7, 22=10, 23=43, 24=41, 25=12, 26=11, 27=13, 28=14, 29=15, 30=16, 31=17, 32=18, 33=19, 34=20, 35=21, 36=22, 37=23, 38=24, 39=39, 40=37, 41=26, 42=25, 43=27, 44=28, 45=29, 46=30, 47=35, 48=34, 49=33, 50=32, 51=31, 52=36, 53=38, 54=40, 55=42, 56=44, 57=2, 58=1, 59=3}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=5, 6=4, 7=2, 8=1, 9=3, 10=10, 11=43, 12=41, 13=12, 14=11, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=39, 28=37, 29=26, 30=25, 31=27, 32=28, 33=29, 34=30, 35=35, 36=34, 37=33, 38=32, 39=31, 40=36, 41=38, 42=40, 43=42, 44=44, 45=54, 46=52, 47=46, 48=45, 49=47, 50=48, 51=49, 52=50, 53=51, 54=53, 55=55, 56=56, 57=57, 58=58, 59=59}

