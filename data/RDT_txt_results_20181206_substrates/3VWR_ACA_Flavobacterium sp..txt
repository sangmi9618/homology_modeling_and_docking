
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)[CH2]:1.0, O>>O=C(O)C[CH2]:1.0, O>>O=C(O)[CH2]:1.0, O>>[C]O:1.0, [C]O:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[O:1.0, [C]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (3)
[O:1.0, O=C(O)[CH2]:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (3)
[O:1.0, O=C(O)C[CH2]:1.0, O=C(O)[CH2]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O>>[C]O

MMP Level 2
1: O>>O=C(O)[CH2]

MMP Level 3
1: O>>O=C(O)C[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O>>O=C(O)CCCCCN]


//
SELECTED AAM MAPPING
[OH2:1]>>[O:19]=[C:20]([OH:1])[CH2:21][CH2:22][CH2:23][CH2:24][CH2:25][NH2:26].[O:2]=[C:3]([OH:4])[CH2:5][CH2:6][CH2:7][CH2:8][CH2:9][NH:10][C:11](=[O:12])[CH2:13][CH2:14][CH2:15][CH2:16][CH2:17][NH2:18]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=1}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=23, 2=22, 3=21, 4=19, 5=18, 6=20, 7=24, 8=25, 9=26, 10=14, 11=13, 12=12, 13=10, 14=11, 15=9, 16=8, 17=7, 18=6, 19=5, 20=4, 21=2, 22=1, 23=3, 24=15, 25=16, 26=17}

