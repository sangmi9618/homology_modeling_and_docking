
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:2.0, C=O:1.0]

//
FINGERPRINTS RC
[N:3.0, O:3.0, O=C(N)N:4.0, O=C(N)N>>N:2.0, O=C(N)N>>O=C=O:3.0, O=C=O:3.0, O>>O=C=O:2.0, O>>[C]=O:1.0, [C]:2.0, [C]=O:1.0, [C]N:2.0, [C]N>>N:1.0, [NH2]:2.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[N:1.0, O:1.0, [C]:3.0, [NH2]:2.0, [O]:1.0]


ID=Reaction Center at Level: 1 (6)
[N:1.0, O:1.0, O=C(N)N:2.0, O=C=O:1.0, [C]=O:1.0, [C]N:2.0]


ID=Reaction Center at Level: 2 (4)
[N:1.0, O:1.0, O=C(N)N:4.0, O=C=O:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=C(N)N>>O=C=O
2: [C]N>>N
3: O>>[C]=O

MMP Level 2
1: O=C(N)N>>O=C=O
2: O=C(N)N>>N
3: O>>O=C=O

MMP Level 3
1: O=C(N)N>>O=C=O
2: O=C(N)N>>N
3: O>>O=C=O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(N)N>>O=C=O]
2: R:M00001, P:M00004	[O=C(N)N>>N]
3: R:M00002, P:M00003	[O>>O=C=O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([NH2:3])[NH2:5].[OH2:4]>>[O:1]=[C:2]=[O:4].[NH3:3]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=2, 2=1, 3=3, 4=4, 5=5}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=2, 2=1, 3=3, 4=4}

