
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

//
FINGERPRINTS RC
[O=C=O:3.0, [CH]:1.0, [C]:3.0, [C]=O:1.0, [C]C(=O)O:2.0, [C]C(=O)O>>O=C=O:2.0, [C]C([N])=[CH]:1.0, [C]C([N])=[CH]>>[N]C=[CH]:1.0, [C]C=C(C(=O)O)N([C])[CH]:1.0, [C]C=C(C(=O)O)N([C])[CH]>>O=C=O:1.0, [C]C=C(C(=O)O)N([C])[CH]>>[C]C=CN([C])[CH]:1.0, [C]C=CN([C])[CH]:1.0, [C]O:1.0, [C]O>>[C]=O:1.0, [N]C(=[CH])C(=O)O:1.0, [N]C(=[CH])C(=O)O>>O=C=O:2.0, [N]C=[CH]:1.0, [OH]:1.0, [O]:1.0, [O]C([CH])n1c(=O)[nH]c(=O)cc1C(=O)O>>[O]C([CH])n1ccc(=O)[nH]c1=O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (1)
[[C]:2.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(=O)O:1.0, [C]C([N])=[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[C]C=C(C(=O)O)N([C])[CH]:1.0, [N]C(=[CH])C(=O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C=O:1.0, [C]=O:1.0, [C]C(=O)O:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (3)
[O=C=O:2.0, [C]C(=O)O:1.0, [N]C(=[CH])C(=O)O:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]O>>[C]=O
2: [C]C([N])=[CH]>>[N]C=[CH]
3: [C]C(=O)O>>O=C=O

MMP Level 2
1: [C]C(=O)O>>O=C=O
2: [C]C=C(C(=O)O)N([C])[CH]>>[C]C=CN([C])[CH]
3: [N]C(=[CH])C(=O)O>>O=C=O

MMP Level 3
1: [N]C(=[CH])C(=O)O>>O=C=O
2: [O]C([CH])n1c(=O)[nH]c(=O)cc1C(=O)O>>[O]C([CH])n1ccc(=O)[nH]c1=O
3: [C]C=C(C(=O)O)N([C])[CH]>>O=C=O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1cc(C(=O)O)n(c(=O)[nH]1)C2OC(COP(=O)(O)O)C(O)C2O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)O)C(O)C2O]
2: R:M00001, P:M00004	[O=c1cc(C(=O)O)n(c(=O)[nH]1)C2OC(COP(=O)(O)O)C(O)C2O>>O=C=O, O=c1cc(C(=O)O)n(c(=O)[nH]1)C2OC(COP(=O)(O)O)C(O)C2O>>O=C=O]


//
SELECTED AAM MAPPING
[H+:25].[O:1]=[C:2]([OH:3])[C:4]1=[CH:5][C:6](=[O:7])[NH:8][C:9](=[O:10])[N:11]1[CH:12]2[O:13][CH:14]([CH2:15][O:16][P:17](=[O:18])([OH:19])[OH:20])[CH:21]([OH:22])[CH:23]2[OH:24]>>[O:1]=[C:2]=[O:3].[O:7]=[C:6]1[CH:5]=[CH:4][N:11]([C:9](=[O:10])[NH:8]1)[CH:12]2[O:13][CH:14]([CH2:15][O:16][P:17](=[O:18])([OH:19])[OH:20])[CH:21]([OH:22])[CH:23]2[OH:24]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=5, 2=4, 3=11, 4=9, 5=10, 6=8, 7=6, 8=7, 9=12, 10=23, 11=21, 12=14, 13=13, 14=15, 15=16, 16=17, 17=18, 18=19, 19=20, 20=22, 21=24, 22=2, 23=1, 24=3, 25=25}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=3, 2=4, 3=5, 4=6, 5=7, 6=8, 7=2, 8=1, 9=9, 10=20, 11=18, 12=11, 13=10, 14=12, 15=13, 16=14, 17=15, 18=16, 19=17, 20=19, 21=21, 22=23, 23=22, 24=24}

