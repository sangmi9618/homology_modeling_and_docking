
//
FINGERPRINTS BC

FORMED_CLEAVED
[O%P:1.0, O-P:1.0]

//
FINGERPRINTS RC
[O=P(O)(O[CH])O[CH2]:1.0, O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[CH2]:1.0, O=P(O)(O[P])O[CH2]>>O=P(O)(O[CH])O[CH2]:1.0, OC1[CH]OC([CH2])C1O>>O=P1(O)OCC2O[CH]C(O)C2O1:1.0, [CH]C([CH])O:1.0, [CH]C([CH])O>>[O]P(=O)(O)OC([CH])[CH]:1.0, [CH]O:1.0, [CH]O>>[P]O[CH]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OC([CH])[CH]:1.0, [O]P(=O)(O)OP(=O)(O)OC[CH]>>O=P1(O)OC[CH]C([CH])O1:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:2.0, [O]P([O])(=O)O>>[O]P([O])(=O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[O]P([O])(=O)O:2.0, [P]O[CH]:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O[CH])O[CH2]:1.0, O=P(O)(O[P])O[CH2]:1.0, [O]P(=O)(O)OC([CH])[CH]:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH]O>>[P]O[CH]
2: [P]O[P]>>[P]O
3: [O]P([O])(=O)O>>[O]P([O])(=O)O

MMP Level 2
1: [CH]C([CH])O>>[O]P(=O)(O)OC([CH])[CH]
2: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
3: O=P(O)(O[P])O[CH2]>>O=P(O)(O[CH])O[CH2]

MMP Level 3
1: OC1[CH]OC([CH2])C1O>>O=P1(O)OCC2O[CH]C(O)C2O1
2: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]
3: [O]P(=O)(O)OP(=O)(O)OC[CH]>>O=P1(O)OC[CH]C([CH])O1


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P1(O)OCC2OC(n3cnc4c(ncnc43)N)C(O)C2O1, O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P1(O)OCC2OC(n3cnc4c(ncnc43)N)C(O)C2O1]
2: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[OH:9].[O:11]=[P:10]1([OH:12])[O:13][CH2:14][CH:15]2[O:16][CH:17]([N:18]:3:[CH:19]:[N:20]:[C:21]:4:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]43)[NH2:27])[CH:28]([OH:29])[CH:30]2[O:31]1


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=5, 2=6, 3=21, 4=19, 5=8, 6=7, 7=9, 8=10, 9=11, 10=12, 11=17, 12=16, 13=15, 14=14, 15=13, 16=18, 17=20, 18=22, 19=2, 20=1, 21=4, 22=3, 23=25, 24=24, 25=23, 26=26, 27=27, 28=28, 29=29, 30=30, 31=31}

