
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:2.0]

//
FINGERPRINTS RC
[O=C(O)CCC(=O)S[CH2]>>O=C(O)CCC(=O)O:1.0, O=C(O)C[CH2]:1.0, O=C(O)[CH2]:1.0, O=C(S[CH2])C[CH2]:1.0, O=C(S[CH2])C[CH2]>>O=CS[CH2]:1.0, O=C([S])[CH2]:1.0, O=C([S])[CH2]>>O=C[S]:1.0, O=CO:2.0, O=CO>>O=C(O)C[CH2]:1.0, O=CO>>O=C(O)[CH2]:1.0, O=CO>>[C]CCC(=O)O:1.0, O=CS[CH2]:1.0, O=C[S]:1.0, [CH2]:2.0, [CH]:2.0, [C]:2.0, [C]CCC(=O)O:1.0, [C]CCC(=O)SC[CH2]>>O=CSC[CH2]:1.0, [C]CCC(=O)[S]:1.0, [C]CCC(=O)[S]>>[C]CCC(=O)O:1.0, [C]C[CH2]:2.0, [C]C[CH2]>>[C]C[CH2]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH2]:2.0, [C]:2.0]


ID=Reaction Center at Level: 1 (3)
[O=C(O)[CH2]:1.0, O=C([S])[CH2]:1.0, [C]C[CH2]:2.0]


ID=Reaction Center at Level: 2 (4)
[O=C(O)C[CH2]:1.0, O=C(S[CH2])C[CH2]:1.0, [C]CCC(=O)O:1.0, [C]CCC(=O)[S]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=C([S])[CH2]>>O=C[S]
2: [C]C[CH2]>>[C]C[CH2]
3: O=CO>>O=C(O)[CH2]

MMP Level 2
1: O=C(S[CH2])C[CH2]>>O=CS[CH2]
2: [C]CCC(=O)[S]>>[C]CCC(=O)O
3: O=CO>>O=C(O)C[CH2]

MMP Level 3
1: [C]CCC(=O)SC[CH2]>>O=CSC[CH2]
2: O=C(O)CCC(=O)S[CH2]>>O=C(O)CCC(=O)O
3: O=CO>>[C]CCC(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)CCC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O>>O=CSCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O]
2: R:M00001, P:M00004	[O=C(O)CCC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O>>O=C(O)CCC(=O)O]
3: R:M00002, P:M00004	[O=CO>>O=C(O)CCC(=O)O]


//
SELECTED AAM MAPPING
[O:56]=[CH:57][OH:58].[O:1]=[C:2]([OH:3])[CH2:4][CH2:5][C:6](=[O:7])[S:8][CH2:9][CH2:10][NH:11][C:12](=[O:13])[CH2:14][CH2:15][NH:16][C:17](=[O:18])[CH:19]([OH:20])[C:21]([CH3:22])([CH3:23])[CH2:24][O:25][P:26](=[O:27])([OH:28])[O:29][P:30](=[O:31])([OH:32])[O:33][CH2:34][CH:35]1[O:36][CH:37]([N:38]:2:[CH:39]:[N:40]:[C:41]:3:[C:42](:[N:43]:[CH:44]:[N:45]:[C:46]32)[NH2:47])[CH:48]([OH:49])[CH:50]1[O:51][P:52](=[O:53])([OH:54])[OH:55]>>[O:7]=[CH:6][S:8][CH2:9][CH2:10][NH:11][C:12](=[O:13])[CH2:14][CH2:15][NH:16][C:17](=[O:18])[CH:19]([OH:20])[C:21]([CH3:22])([CH3:23])[CH2:24][O:25][P:26](=[O:27])([OH:28])[O:29][P:30](=[O:31])([OH:32])[O:33][CH2:34][CH:35]1[O:36][CH:37]([N:38]:2:[CH:39]:[N:40]:[C:41]:3:[C:42](:[N:43]:[CH:44]:[N:45]:[C:46]32)[NH2:47])[CH:48]([OH:49])[CH:50]1[O:51][P:52](=[O:53])([OH:54])[OH:55].[O:1]=[C:2]([OH:3])[CH2:4][CH2:5][C:57](=[O:56])[OH:58]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=22, 2=21, 3=23, 4=24, 5=25, 6=26, 7=27, 8=28, 9=29, 10=30, 11=31, 12=32, 13=33, 14=34, 15=35, 16=50, 17=48, 18=37, 19=36, 20=38, 21=39, 22=40, 23=41, 24=46, 25=45, 26=44, 27=43, 28=42, 29=47, 30=49, 31=51, 32=52, 33=53, 34=54, 35=55, 36=19, 37=17, 38=18, 39=16, 40=15, 41=14, 42=12, 43=13, 44=11, 45=10, 46=9, 47=8, 48=6, 49=7, 50=5, 51=4, 52=2, 53=1, 54=3, 55=20, 56=57, 57=56, 58=58}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=17, 2=16, 3=18, 4=19, 5=20, 6=21, 7=22, 8=23, 9=24, 10=25, 11=26, 12=27, 13=28, 14=29, 15=30, 16=45, 17=43, 18=32, 19=31, 20=33, 21=34, 22=35, 23=36, 24=41, 25=40, 26=39, 27=38, 28=37, 29=42, 30=44, 31=46, 32=47, 33=48, 34=49, 35=50, 36=14, 37=12, 38=13, 39=11, 40=10, 41=9, 42=7, 43=8, 44=6, 45=5, 46=4, 47=3, 48=2, 49=1, 50=15, 51=55, 52=54, 53=52, 54=51, 55=53, 56=56, 57=57, 58=58}

