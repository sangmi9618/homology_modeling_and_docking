
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-S:1.0, C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[N:3.0, O=C(O)C(=O)CC:1.0, O=C(O)C(N)CCS>>O=C(O)C(=O)CC:1.0, O=C(O)C(N)C[CH2]:1.0, O=C(O)C(N)C[CH2]>>N:1.0, O=C(O)C(N)C[CH2]>>O=C(O)C(=O)CC:1.0, S:3.0, SC[CH2]:2.0, SC[CH2]>>S:1.0, SC[CH2]>>[CH2]C:1.0, S[CH2]:1.0, S[CH2]>>S:1.0, [CH2]:1.0, [CH2]C:1.0, [CH3]:1.0, [CH]:1.0, [CH]CCS:1.0, [CH]CCS>>S:1.0, [CH]CCS>>[C]CC:1.0, [CH]N:1.0, [CH]N>>N:1.0, [C]:1.0, [C]C(=O)[CH2]:1.0, [C]C(N)CCS>>[C]C(=O)CC:1.0, [C]C([CH2])N:2.0, [C]C([CH2])N>>N:1.0, [C]C([CH2])N>>[C]C(=O)[CH2]:1.0, [C]CC:1.0, [NH2]:1.0, [SH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (8)
[N:1.0, S:1.0, [CH2]:1.0, [CH]:1.0, [C]:1.0, [NH2]:1.0, [O]:1.0, [SH]:1.0]


ID=Reaction Center at Level: 1 (8)
[N:1.0, S:1.0, SC[CH2]:1.0, S[CH2]:1.0, [CH]N:1.0, [C]=O:1.0, [C]C(=O)[CH2]:1.0, [C]C([CH2])N:1.0]


ID=Reaction Center at Level: 2 (8)
[N:1.0, O=C(O)C(=O)CC:1.0, O=C(O)C(N)C[CH2]:1.0, S:1.0, SC[CH2]:1.0, [CH]CCS:1.0, [C]C(=O)[CH2]:1.0, [C]C([CH2])N:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(=O)[CH2]:1.0, [C]C([CH2])N:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C(=O)CC:1.0, O=C(O)C(N)C[CH2]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]N>>N
2: S[CH2]>>S
3: [C]C([CH2])N>>[C]C(=O)[CH2]
4: SC[CH2]>>[CH2]C

MMP Level 2
1: [C]C([CH2])N>>N
2: SC[CH2]>>S
3: O=C(O)C(N)C[CH2]>>O=C(O)C(=O)CC
4: [CH]CCS>>[C]CC

MMP Level 3
1: O=C(O)C(N)C[CH2]>>N
2: [CH]CCS>>S
3: O=C(O)C(N)CCS>>O=C(O)C(=O)CC
4: [C]C(N)CCS>>[C]C(=O)CC


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)C(N)CCS>>O=C(O)C(=O)CC, O=C(O)C(N)CCS>>O=C(O)C(=O)CC]
2: R:M00001, P:M00003	[O=C(O)C(N)CCS>>N]
3: R:M00001, P:M00004	[O=C(O)C(N)CCS>>S]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][SH:8]>>[O:1]=[C:2]([OH:3])[C:4](=[O:9])[CH2:6][CH3:7].[SH2:8].[NH3:5]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=8, 4=4, 5=2, 6=1, 7=3, 8=5}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=7, 2=6, 3=4, 4=5, 5=2, 6=1, 7=3, 8=9, 9=8}

