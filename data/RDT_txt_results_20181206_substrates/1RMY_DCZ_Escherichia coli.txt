
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O:3.0, O=P(O)(O)O:3.0, O=P(O)(O)OC([CH])[CH2]:1.0, O=P(O)(O)OC([CH])[CH2]>>O=P(O)(O)O:1.0, O=P(O)(O)OC([CH])[CH2]>>[CH]C(O)[CH2]:1.0, O=P(O)(O)OC1C[CH]OC1[CH2]>>OC1C[CH]OC1[CH2]:1.0, O=P(O)(O)O[CH]:1.0, O=P(O)(O)O[CH]>>O=P(O)(O)O:1.0, O>>O=P(O)(O)O:2.0, O>>[P]O:1.0, [CH]C(O)[CH2]:1.0, [CH]O:1.0, [OH]:2.0, [O]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH]:1.0, [P]O[CH]>>[CH]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=P(O)(O)O:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=P(O)(O)O:2.0, O=P(O)(O)OC([CH])[CH2]:1.0, O=P(O)(O)O[CH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[CH]>>[CH]O
2: [O]P(=O)(O)O>>O=P(O)(O)O
3: O>>[P]O

MMP Level 2
1: O=P(O)(O)OC([CH])[CH2]>>[CH]C(O)[CH2]
2: O=P(O)(O)O[CH]>>O=P(O)(O)O
3: O>>O=P(O)(O)O

MMP Level 3
1: O=P(O)(O)OC1C[CH]OC1[CH2]>>OC1C[CH]OC1[CH2]
2: O=P(O)(O)OC([CH])[CH2]>>O=P(O)(O)O
3: O>>O=P(O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1nc(N)ccn1C2OC(CO)C(OP(=O)(O)O)C2>>O=c1nc(N)ccn1C2OC(CO)C(O)C2]
2: R:M00001, P:M00004	[O=c1nc(N)ccn1C2OC(CO)C(OP(=O)(O)O)C2>>O=P(O)(O)O]
3: R:M00002, P:M00004	[O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[CH:6]=[CH:7][N:8]1[CH:9]2[O:10][CH:11]([CH2:12][OH:13])[CH:14]([O:15][P:16](=[O:17])([OH:18])[OH:19])[CH2:20]2.[OH2:21]>>[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[CH:6]=[CH:7][N:8]1[CH:9]2[O:10][CH:11]([CH2:12][OH:13])[CH:14]([OH:15])[CH2:20]2.[O:17]=[P:16]([OH:19])([OH:21])[OH:18]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=20, 2=14, 3=11, 4=10, 5=9, 6=8, 7=7, 8=6, 9=4, 10=3, 11=2, 12=1, 13=5, 14=12, 15=13, 16=15, 17=16, 18=17, 19=18, 20=19, 21=21}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=16, 2=14, 3=11, 4=10, 5=9, 6=8, 7=7, 8=6, 9=4, 10=3, 11=2, 12=1, 13=5, 14=12, 15=13, 16=15, 17=19, 18=18, 19=17, 20=20, 21=21}

