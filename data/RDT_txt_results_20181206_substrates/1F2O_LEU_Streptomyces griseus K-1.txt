
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:2.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)C([CH2])N:1.0, O=C(OC(=C[CH])C=[CH])C([CH2])N>>[CH]C=C(O)C=[CH]:1.0, O=C(OC([CH])=[CH])C(N)C[CH]>>O=C(O)C(N)C[CH]:1.0, O=C([CH])O:2.0, O=C([CH])OC([CH])=[CH]:1.0, O=C([CH])OC([CH])=[CH]>>[CH]C(=[CH])O:1.0, O>>O=C(O)C([CH2])N:1.0, O>>O=C([CH])O:1.0, O>>[C]O:1.0, [CH]C(=[CH])O:1.0, [C]:2.0, [C]O:2.0, [C]OC(=O)C([CH2])N:1.0, [C]OC(=O)C([CH2])N>>O=C(O)C([CH2])N:1.0, [C]O[C]:1.0, [C]O[C]>>[C]O:1.0, [OH]:2.0, [O]:1.0, [O]C(=O)[CH]:1.0, [O]C(=O)[CH]>>O=C([CH])O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [C]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=C([CH])O:1.0, [C]O:1.0, [C]O[C]:1.0, [O]C(=O)[CH]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=C(O)C([CH2])N:1.0, O=C([CH])O:1.0, O=C([CH])OC([CH])=[CH]:1.0, [C]OC(=O)C([CH2])N:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O>>[C]O
2: [C]O[C]>>[C]O
3: [O]C(=O)[CH]>>O=C([CH])O

MMP Level 2
1: O>>O=C([CH])O
2: O=C([CH])OC([CH])=[CH]>>[CH]C(=[CH])O
3: [C]OC(=O)C([CH2])N>>O=C(O)C([CH2])N

MMP Level 3
1: O>>O=C(O)C([CH2])N
2: O=C(OC(=C[CH])C=[CH])C([CH2])N>>[CH]C=C(O)C=[CH]
3: O=C(OC([CH])=[CH])C(N)C[CH]>>O=C(O)C(N)C[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O>>O=C(O)C(N)CC(C)C]
2: R:M00002, P:M00003	[[O-][N+](=O)c1ccc(OC(=O)C(N)CC(C)C)cc1>>[O-][N+](=O)c1ccc(O)cc1]
3: R:M00002, P:M00004	[[O-][N+](=O)c1ccc(OC(=O)C(N)CC(C)C)cc1>>O=C(O)C(N)CC(C)C]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([O:3][C:4]:1:[CH:5]:[CH:6]:[C:7](:[CH:8]:[CH:9]1)[N+:10](=[O:11])[O-:12])[CH:13]([NH2:14])[CH2:15][CH:16]([CH3:17])[CH3:18].[OH2:19]>>[O:1]=[C:2]([OH:19])[CH:13]([NH2:14])[CH2:15][CH:16]([CH3:18])[CH3:17].[O:11]=[N+:10]([O-:12])[C:7]:1:[CH:6]:[CH:5]:[C:4]([OH:3]):[CH:9]:[CH:8]1


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=19, 2=17, 3=16, 4=18, 5=15, 6=13, 7=2, 8=1, 9=3, 10=4, 11=5, 12=6, 13=7, 14=8, 15=9, 16=10, 17=11, 18=12, 19=14}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=5, 2=6, 3=7, 4=9, 5=10, 6=4, 7=2, 8=1, 9=3, 10=8, 11=18, 12=17, 13=19, 14=16, 15=14, 16=12, 17=11, 18=13, 19=15}

