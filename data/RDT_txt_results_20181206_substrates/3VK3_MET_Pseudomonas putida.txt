
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-S:1.0, C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[N:3.0, O:3.0, O=C(O)C(=O)CC:1.0, O=C(O)C(N)CC[S]>>O=C(O)C(=O)CC:1.0, O=C(O)C(N)C[CH2]:1.0, O=C(O)C(N)C[CH2]>>N:1.0, O=C(O)C(N)C[CH2]>>O=C(O)C(=O)CC:1.0, O>>O=C(O)C(=O)CC:1.0, O>>[C]=O:1.0, O>>[C]C(=O)[CH2]:1.0, S(C)C[CH2]:1.0, S(C)C[CH2]>>SC:1.0, S([CH2])C:1.0, S([CH2])C>>SC:1.0, SC:2.0, [CH2]:1.0, [CH2]C:1.0, [CH3]:1.0, [CH]:1.0, [CH]CCSC:1.0, [CH]CCSC>>SC:1.0, [CH]CCSC>>[C]CC:1.0, [CH]N:1.0, [CH]N>>N:1.0, [C]:1.0, [C]=O:1.0, [C]C(=O)[CH2]:2.0, [C]C(N)CCSC>>[C]C(=O)CC:1.0, [C]C([CH2])N:2.0, [C]C([CH2])N>>N:1.0, [C]C([CH2])N>>[C]C(=O)[CH2]:1.0, [C]CC:1.0, [NH2]:1.0, [O]:1.0, [SH]:1.0, [S]:1.0, [S]C[CH2]:1.0, [S]C[CH2]>>[CH2]C:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (8)
[N:1.0, O:1.0, [CH2]:1.0, [CH]:1.0, [C]:1.0, [NH2]:1.0, [O]:1.0, [S]:1.0]


ID=Reaction Center at Level: 1 (8)
[N:1.0, O:1.0, S([CH2])C:1.0, [CH]N:1.0, [C]=O:1.0, [C]C(=O)[CH2]:1.0, [C]C([CH2])N:1.0, [S]C[CH2]:1.0]


ID=Reaction Center at Level: 2 (8)
[N:1.0, O:1.0, O=C(O)C(=O)CC:1.0, O=C(O)C(N)C[CH2]:1.0, S(C)C[CH2]:1.0, [CH]CCSC:1.0, [C]C(=O)[CH2]:1.0, [C]C([CH2])N:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(=O)[CH2]:1.0, [C]C([CH2])N:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C(=O)CC:1.0, O=C(O)C(N)C[CH2]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [S]C[CH2]>>[CH2]C
2: [CH]N>>N
3: O>>[C]=O
4: [C]C([CH2])N>>[C]C(=O)[CH2]
5: S([CH2])C>>SC

MMP Level 2
1: [CH]CCSC>>[C]CC
2: [C]C([CH2])N>>N
3: O>>[C]C(=O)[CH2]
4: O=C(O)C(N)C[CH2]>>O=C(O)C(=O)CC
5: S(C)C[CH2]>>SC

MMP Level 3
1: [C]C(N)CCSC>>[C]C(=O)CC
2: O=C(O)C(N)C[CH2]>>N
3: O>>O=C(O)C(=O)CC
4: O=C(O)C(N)CC[S]>>O=C(O)C(=O)CC
5: [CH]CCSC>>SC


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)CCSC>>SC]
2: R:M00001, P:M00004	[O=C(O)C(N)CCSC>>N]
3: R:M00001, P:M00005	[O=C(O)C(N)CCSC>>O=C(O)C(=O)CC, O=C(O)C(N)CCSC>>O=C(O)C(=O)CC]
4: R:M00002, P:M00005	[O>>O=C(O)C(=O)CC]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][S:8][CH3:9].[OH2:10]>>[O:1]=[C:2]([OH:3])[C:4](=[O:10])[CH2:6][CH3:7].[SH:8][CH3:9].[NH3:5]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=4, 6=2, 7=1, 8=3, 9=5, 10=10}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=10, 4=7, 5=6, 6=4, 7=5, 8=2, 9=1, 10=3}

