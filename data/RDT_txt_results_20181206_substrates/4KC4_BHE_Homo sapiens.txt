
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:2.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O=P(O)(O[P])OC(O[CH])C([CH])O>>O=P(O)(O)O[P]:1.0, [CH]:6.0, [CH]C([CH])O:3.0, [CH]C([CH])O>>[CH]C([CH])O:1.0, [CH]C([CH])O>>[O]C([CH])OC([CH])[CH]:1.0, [CH]C([CH])OC1OC([CH]C(O)C1O)C>>[CH]C([CH])OC1OC([CH]C(O)C1O)C:1.0, [CH]O:1.0, [CH]O>>[CH]O[CH]:1.0, [CH]OC(O[CH])C([CH])O:3.0, [CH]OC(O[CH])C([CH])O>>[CH]OC(O[CH])C([CH])O:1.0, [CH]O[CH]:1.0, [OH]:2.0, [O]:2.0, [O]C([CH2])C(O)C([CH])O:1.0, [O]C([CH2])C(O)C([CH])O>>[O]C([CH])C(O)C([O])[CH2]:1.0, [O]C([CH])C(O)C([CH])O>>[O]C([CH])C(OC(O[CH])C([CH])O)C([CH])O:1.0, [O]C([CH])C(O)C([O])[CH2]:1.0, [O]C([CH])OC([CH])[CH]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]C([O])[CH]:4.0, [O]C([O])[CH]>>[O]C([O])[CH]:2.0, [O]C1[CH]OC(CO)C(O)C1O>>[O]C1[CH]OC(CO)C(O)C1O[CH]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OC1OC([CH2])[CH]C(O)C1O>>[CH]C([CH])OC1OC([CH2])[CH]C(O)C1O:1.0, [P]O:1.0, [P]OC(O[CH])C([CH])O:1.0, [P]OC(O[CH])C([CH])O>>[CH]OC(O[CH])C([CH])O:1.0, [P]O[CH]:1.0, [P]O[CH]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (3)
[[CH]O[CH]:1.0, [O]C([O])[CH]:2.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[CH]OC(O[CH])C([CH])O:1.0, [O]C([CH])OC([CH])[CH]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [P]OC(O[CH])C([CH])O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:4.0]


ID=Reaction Center at Level: 1 (2)
[[CH]C([CH])O:2.0, [O]C([O])[CH]:2.0]


ID=Reaction Center at Level: 2 (3)
[[CH]OC(O[CH])C([CH])O:2.0, [O]C([CH2])C(O)C([CH])O:1.0, [O]C([CH])C(O)C([O])[CH2]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [O]C([O])[CH]>>[O]C([O])[CH]
2: [O]C([O])[CH]>>[O]C([O])[CH]
3: [CH]O>>[CH]O[CH]
4: [P]O[CH]>>[P]O
5: [CH]C([CH])O>>[CH]C([CH])O

MMP Level 2
1: [P]OC(O[CH])C([CH])O>>[CH]OC(O[CH])C([CH])O
2: [CH]OC(O[CH])C([CH])O>>[CH]OC(O[CH])C([CH])O
3: [CH]C([CH])O>>[O]C([CH])OC([CH])[CH]
4: [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O
5: [O]C([CH2])C(O)C([CH])O>>[O]C([CH])C(O)C([O])[CH2]

MMP Level 3
1: [O]P(=O)(O)OC1OC([CH2])[CH]C(O)C1O>>[CH]C([CH])OC1OC([CH2])[CH]C(O)C1O
2: [CH]C([CH])OC1OC([CH]C(O)C1O)C>>[CH]C([CH])OC1OC([CH]C(O)C1O)C
3: [O]C([CH])C(O)C([CH])O>>[O]C([CH])C(OC(O[CH])C([CH])O)C([CH])O
4: O=P(O)(O[P])OC(O[CH])C([CH])O>>O=P(O)(O)O[P]
5: [O]C1[CH]OC(CO)C(O)C1O>>[O]C1[CH]OC(CO)C(O)C1O[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3O)C(O)C2O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)O)C(O)C2O]
2: R:M00001, P:M00004	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3O)C(O)C2O>>OCC1OC(OC2C(O)C(OC(OCCCCCCCC)C2OC3OC(C)C(O)C(O)C3O)CO)C(O)C(O)C1O]
3: R:M00002, P:M00004	[OCC1OC(OCCCCCCCC)C(OC2OC(C)C(O)C(O)C2O)C(O)C1O>>OCC1OC(OC2C(O)C(OC(OCCCCCCCC)C2OC3OC(C)C(O)C(O)C3O)CO)C(O)C(O)C1O, OCC1OC(OCCCCCCCC)C(OC2OC(C)C(O)C(O)C2O)C(O)C1O>>OCC1OC(OC2C(O)C(OC(OCCCCCCCC)C2OC3OC(C)C(O)C(O)C3O)CO)C(O)C(O)C1O, OCC1OC(OCCCCCCCC)C(OC2OC(C)C(O)C(O)C2O)C(O)C1O>>OCC1OC(OC2C(O)C(OC(OCCCCCCCC)C2OC3OC(C)C(O)C(O)C3O)CO)C(O)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][CH:22]3[O:23][CH:24]([CH2:25][OH:26])[CH:27]([OH:28])[CH:29]([OH:30])[CH:31]3[OH:32])[CH:33]([OH:34])[CH:35]2[OH:36].[OH:37][CH2:38][CH:39]1[O:40][CH:41]([O:42][CH2:43][CH2:44][CH2:45][CH2:46][CH2:47][CH2:48][CH2:49][CH3:50])[CH:51]([O:52][CH:53]2[O:54][CH:55]([CH3:56])[CH:57]([OH:58])[CH:59]([OH:60])[CH:61]2[OH:62])[CH:63]([OH:64])[CH:65]1[OH:66]>>[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[OH:21])[CH:33]([OH:34])[CH:35]2[OH:36].[OH:26][CH2:25][CH:24]1[O:23][CH:22]([O:64][CH:63]2[CH:65]([OH:66])[CH:39]([O:40][CH:41]([O:42][CH2:43][CH2:44][CH2:45][CH2:46][CH2:47][CH2:48][CH2:49][CH3:50])[CH:51]2[O:52][CH:53]3[O:54][CH:55]([CH3:56])[CH:57]([OH:58])[CH:59]([OH:60])[CH:61]3[OH:62])[CH2:38][OH:37])[CH:31]([OH:32])[CH:29]([OH:30])[CH:27]1[OH:28]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=3, 2=4, 3=5, 4=6, 5=7, 6=8, 7=2, 8=1, 9=9, 10=35, 11=33, 12=11, 13=10, 14=12, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=31, 26=29, 27=27, 28=24, 29=23, 30=25, 31=26, 32=28, 33=30, 34=32, 35=34, 36=36, 37=50, 38=49, 39=48, 40=47, 41=46, 42=45, 43=44, 44=43, 45=42, 46=41, 47=51, 48=63, 49=65, 50=39, 51=40, 52=38, 53=37, 54=66, 55=64, 56=52, 57=53, 58=61, 59=59, 60=57, 61=55, 62=54, 63=56, 64=58, 65=60, 66=62}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=44, 2=45, 3=46, 4=47, 5=48, 6=49, 7=43, 8=42, 9=50, 10=65, 11=63, 12=52, 13=51, 14=53, 15=54, 16=55, 17=56, 18=57, 19=58, 20=59, 21=60, 22=61, 23=62, 24=64, 25=66, 26=21, 27=20, 28=19, 29=18, 30=17, 31=16, 32=15, 33=14, 34=13, 35=12, 36=22, 37=7, 38=8, 39=10, 40=11, 41=34, 42=35, 43=9, 44=6, 45=5, 46=36, 47=38, 48=40, 49=3, 50=4, 51=2, 52=1, 53=41, 54=39, 55=37, 56=23, 57=24, 58=32, 59=30, 60=28, 61=26, 62=25, 63=27, 64=29, 65=31, 66=33}

