
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0, O-O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O=C([CH])NCC(=O)O>>O=C([CH])NC(O)C(=O)O:1.0, OO:4.0, OO>>O:3.0, OO>>[CH]O:1.0, OO>>[C]C([NH])O:1.0, OO>>[C]NC(O)C(=O)O:1.0, [CH2]:1.0, [CH]:1.0, [CH]O:1.0, [C]C([NH])O:2.0, [C]C[NH]:1.0, [C]C[NH]>>[C]C([NH])O:1.0, [C]NC(O)C(=O)O:1.0, [C]NCC(=O)O:1.0, [C]NCC(=O)O>>[C]NC(O)C(=O)O:1.0, [OH]:3.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[O:1.0, [CH]:1.0, [OH]:3.0]


ID=Reaction Center at Level: 1 (4)
[O:1.0, OO:2.0, [CH]O:1.0, [C]C([NH])O:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, OO:2.0, [C]C([NH])O:1.0, [C]NC(O)C(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH2]:1.0, [CH]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]C([NH])O:1.0, [C]C[NH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[C]NC(O)C(=O)O:1.0, [C]NCC(=O)O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: OO>>[CH]O
2: [C]C[NH]>>[C]C([NH])O
3: OO>>O

MMP Level 2
1: OO>>[C]C([NH])O
2: [C]NCC(=O)O>>[C]NC(O)C(=O)O
3: OO>>O

MMP Level 3
1: OO>>[C]NC(O)C(=O)O
2: O=C([CH])NCC(=O)O>>O=C([CH])NC(O)C(=O)O
3: OO>>O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)CNC(=O)C(NC(=O)C(NS(=O)(=O)c1cccc2c1cccc2N(C)C)Cc3ccc(O)cc3)C(C)C>>O=C(O)C(O)NC(=O)C(NC(=O)C(NS(=O)(=O)c1cccc2c1cccc2N(C)C)Cc3ccc(O)cc3)C(C)C]
2: R:M00002, P:M00003	[OO>>O=C(O)C(O)NC(=O)C(NC(=O)C(NS(=O)(=O)c1cccc2c1cccc2N(C)C)Cc3ccc(O)cc3)C(C)C]
3: R:M00002, P:M00004	[OO>>O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH2:4][NH:5][C:6](=[O:7])[CH:8]([NH:9][C:10](=[O:11])[CH:12]([NH:13][S:14](=[O:15])(=[O:16])[C:17]:1:[CH:18]:[CH:19]:[CH:20]:[C:21]:2:[C:22]1:[CH:23]:[CH:24]:[CH:25]:[C:26]2[N:27]([CH3:28])[CH3:29])[CH2:30][C:31]:3:[CH:32]:[CH:33]:[C:34]([OH:35]):[CH:36]:[CH:37]3)[CH:38]([CH3:39])[CH3:40].[OH:41][OH:42]>>[O:1]=[C:2]([OH:3])[CH:4]([OH:41])[NH:5][C:6](=[O:7])[CH:8]([NH:9][C:10](=[O:11])[CH:12]([NH:13][S:14](=[O:15])(=[O:16])[C:17]:1:[CH:18]:[CH:19]:[CH:20]:[C:21]:2:[C:22]1:[CH:23]:[CH:24]:[CH:25]:[C:26]2[N:27]([CH3:28])[CH3:29])[CH2:30][C:31]:3:[CH:32]:[CH:33]:[C:34]([OH:35]):[CH:36]:[CH:37]3)[CH:38]([CH3:39])[CH3:40].[OH2:42]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=39, 2=38, 3=40, 4=8, 5=6, 6=7, 7=5, 8=4, 9=2, 10=1, 11=3, 12=9, 13=10, 14=11, 15=12, 16=30, 17=31, 18=32, 19=33, 20=34, 21=36, 22=37, 23=35, 24=13, 25=14, 26=15, 27=16, 28=17, 29=18, 30=19, 31=20, 32=21, 33=22, 34=23, 35=24, 36=25, 37=26, 38=27, 39=28, 40=29, 41=41, 42=42}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=40, 2=39, 3=41, 4=9, 5=7, 6=8, 7=6, 8=4, 9=2, 10=1, 11=3, 12=5, 13=10, 14=11, 15=12, 16=13, 17=31, 18=32, 19=33, 20=34, 21=35, 22=37, 23=38, 24=36, 25=14, 26=15, 27=16, 28=17, 29=18, 30=19, 31=20, 32=21, 33=22, 34=23, 35=24, 36=25, 37=26, 38=27, 39=28, 40=29, 41=30, 42=42}

