
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[[CH]:3.0, [CH]C([CH])OC1OC([CH2])[CH]C(O)C1O>>OC1OC([CH2])[CH]C(O)C1O:1.0, [CH]O:1.0, [CH]OC(O)C([CH])O:1.0, [CH]OC(O[CH])C([CH])O:1.0, [CH]OC(O[CH])C([CH])O>>[CH]OC(O)C([CH])O:1.0, [CH]O[CH]:1.0, [CH]O[CH]>>[CH]O:1.0, [OH]:1.0, [O]:1.0, [O]C([CH2])C(OC(O[CH])C([CH])O)C([CH])O>>[CH]OC(O)C([CH])O:1.0, [O]C([CH2])C(O[CH])C([CH])O:1.0, [O]C([CH])O:2.0, [O]C([CH])OC([CH])[CH]:1.0, [O]C([CH])OC([CH])[CH]>>[O]C([CH])O:1.0, [O]C([CH])[CH]:1.0, [O]C([O])[CH]:1.0, [O]C([O])[CH]>>[O]C([CH])O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (2)
[[CH]O[CH]:1.0, [O]C([CH])[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[O]C([CH2])C(O[CH])C([CH])O:1.0, [O]C([CH])OC([CH])[CH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]C([CH])O:1.0, [O]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[CH]OC(O)C([CH])O:1.0, [CH]OC(O[CH])C([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [O]C([O])[CH]>>[O]C([CH])O
2: [CH]O[CH]>>[CH]O

MMP Level 2
1: [CH]OC(O[CH])C([CH])O>>[CH]OC(O)C([CH])O
2: [O]C([CH])OC([CH])[CH]>>[O]C([CH])O

MMP Level 3
1: [CH]C([CH])OC1OC([CH2])[CH]C(O)C1O>>OC1OC([CH2])[CH]C(O)C1O
2: [O]C([CH2])C(OC(O[CH])C([CH])O)C([CH])O>>[CH]OC(O)C([CH])O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[OCC1OC(OC2C(O)C(O)C(O)OC2CO)C(O)C(O)C1O>>OCC1OC(O)C(O)C(O)C1O, OCC1OC(OC2C(O)C(O)C(O)OC2CO)C(O)C(O)C1O>>OCC1OC(O)C(O)C(O)C1O]


//
SELECTED AAM MAPPING
[OH:1][CH2:2][CH:3]1[O:4][CH:5]([O:6][CH:13]2[CH:14]([OH:15])[CH:16]([OH:17])[CH:18]([OH:19])[O:20][CH:21]2[CH2:22][OH:23])[CH:7]([OH:8])[CH:9]([OH:10])[CH:11]1[OH:12]>>[OH:1][CH2:2][CH:3]1[O:4][CH:5]([OH:6])[CH:7]([OH:8])[CH:9]([OH:10])[CH:11]1[OH:12]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=2, 2=3, 3=22, 4=20, 5=18, 6=5, 7=4, 8=6, 9=7, 10=15, 11=14, 12=12, 13=10, 14=8, 15=9, 16=11, 17=13, 18=16, 19=17, 20=19, 21=21, 22=23, 23=1}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=2, 2=3, 3=11, 4=9, 5=7, 6=5, 7=4, 8=6, 9=8, 10=10, 11=12, 12=1}

