
//
FINGERPRINTS BC

//
FINGERPRINTS RC
[[Hg+2]:3.0, [Hg+]:3.0, [Hg+]>>[Hg+2]:3.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[Hg+2]:1.0, [Hg+]:1.0]


ID=Reaction Center at Level: 1 (2)
[[Hg+2]:1.0, [Hg+]:1.0]


ID=Reaction Center at Level: 2 (2)
[[Hg+2]:1.0, [Hg+]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [Hg+]>>[Hg+2]

MMP Level 2
1: [Hg+]>>[Hg+2]

MMP Level 3
1: [Hg+]>>[Hg+2]


//
REACTION MMP (RPAIR)
1: R:M00002, P:M00004	[[Hg+]>>[Hg+2]]


//
SELECTED AAM MAPPING
[H+:3].[Hg+:1].[CH4:2]>>[Hg+2:1].[CH4:2]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=2, 2=1, 3=3}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=2, 2=1}

