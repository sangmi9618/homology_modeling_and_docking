
//
FINGERPRINTS BC

ORDER_CHANGED
[C-O*C=O:1.0]

//
FINGERPRINTS RC
[O=C([CH2])[CH2]:2.0, OC([CH2])[CH2]:2.0, OC([CH2])[CH2]>>O=C([CH2])[CH2]:2.0, [CH]:1.0, [CH]C=C1N=C(C=[CH])C([CH])=[N+]C1=[CH]>>[CH]C=C1NC(=C[CH])C(=[CH])[N]C1=[CH]:1.0, [CH]C=c1c(=[CH])nc([CH])c(C=[CH])[n+]1C>>[CH]C=C1C(=[CH])NC(=[CH])C(=C[CH])N1C:1.0, [CH]O:1.0, [CH]O>>[C]=O:1.0, [C]:1.0, [C]=O:1.0, [C]C(=[CH])N(C([C])=[CH])C:1.0, [C]C(=[CH])NC([C])=[CH]:1.0, [C]C([CH])=NC([C])=[CH]:1.0, [C]C([CH])=NC([C])=[CH]>>[C]C(=[CH])NC([C])=[CH]:1.0, [C]C([CH])=[N+](C([C])=[CH])C:1.0, [C]C([CH])=[N+](C([C])=[CH])C>>[C]C(=[CH])N(C([C])=[CH])C:1.0, [C]N([C])C:1.0, [C]N=[C]:1.0, [C]N=[C]>>[C]N[C]:1.0, [C]N[C]:1.0, [C][N+](=[C])C:1.0, [C][N+](=[C])C>>[C]N([C])C:1.0, [N+]:1.0, [NH]:1.0, [N]:2.0, [O-]:1.0, [O-]S(=O)(=O)OC>>O=S(=O)(O)OC:1.0, [O-][S]:1.0, [O-][S]>>[S]O:1.0, [OH]:2.0, [O]:1.0, [O]CC(=O)CO:1.0, [O]CC(O)CO:1.0, [O]CC(O)CO>>[O]CC(=O)CO:2.0, [O]S(=O)(=O)O:1.0, [O]S([O-])(=O)=O:1.0, [O]S([O-])(=O)=O>>[O]S(=O)(=O)O:1.0, [P]OCC(O)CO>>O=C(CO)CO[P]:1.0, [S]O:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH]:1.0, [C]:1.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([CH2])[CH2]:1.0, OC([CH2])[CH2]:1.0, [CH]O:1.0, [C]=O:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C([CH2])[CH2]:1.0, OC([CH2])[CH2]:1.0, [O]CC(=O)CO:1.0, [O]CC(O)CO:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]N=[C]>>[C]N[C]
2: [CH]O>>[C]=O
3: [C][N+](=[C])C>>[C]N([C])C
4: [O-][S]>>[S]O
5: OC([CH2])[CH2]>>O=C([CH2])[CH2]

MMP Level 2
1: [C]C([CH])=NC([C])=[CH]>>[C]C(=[CH])NC([C])=[CH]
2: OC([CH2])[CH2]>>O=C([CH2])[CH2]
3: [C]C([CH])=[N+](C([C])=[CH])C>>[C]C(=[CH])N(C([C])=[CH])C
4: [O]S([O-])(=O)=O>>[O]S(=O)(=O)O
5: [O]CC(O)CO>>[O]CC(=O)CO

MMP Level 3
1: [CH]C=C1N=C(C=[CH])C([CH])=[N+]C1=[CH]>>[CH]C=C1NC(=C[CH])C(=[CH])[N]C1=[CH]
2: [O]CC(O)CO>>[O]CC(=O)CO
3: [CH]C=c1c(=[CH])nc([CH])c(C=[CH])[n+]1C>>[CH]C=C1C(=[CH])NC(=[CH])C(=C[CH])N1C
4: [O-]S(=O)(=O)OC>>O=S(=O)(O)OC
5: [P]OCC(O)CO>>O=C(CO)CO[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=P(O)(O)OCC(O)CO>>O=C(CO)COP(=O)(O)O, O=P(O)(O)OCC(O)CO>>O=C(CO)COP(=O)(O)O]
2: R:M00002, P:M00005	[n1c2ccccc2[n+](c3ccccc13)C>>c1ccc2c(c1)Nc3ccccc3N2C, n1c2ccccc2[n+](c3ccccc13)C>>c1ccc2c(c1)Nc3ccccc3N2C]
3: R:M00003, P:M00006	[[O-]S(=O)(=O)OC>>O=S(=O)(O)OC]


//
SELECTED AAM MAPPING
[O:16]=[P:17]([OH:18])([OH:19])[O:20][CH2:21][CH:22]([OH:23])[CH2:24][OH:25].[O:26]=[S:27](=[O:28])([O-:29])[O:30][CH3:31].[N:1]:1:[C:2]:2:[CH:3]:[CH:4]:[CH:5]:[CH:6]:[C:7]2:[N+:8](:[C:9]:3:[CH:10]:[CH:11]:[CH:12]:[CH:13]:[C:14]13)[CH3:15]>>[O:23]=[C:22]([CH2:24][OH:25])[CH2:21][O:20][P:17](=[O:16])([OH:18])[OH:19].[O:26]=[S:27](=[O:28])([OH:29])[O:30][CH3:31].[CH:4]:1:[CH:5]:[CH:6]:[C:7]2:[C:2](:[CH:3]1)[NH:1][C:14]:3:[CH:13]:[CH:12]:[CH:11]:[CH:10]:[C:9]3[N:8]2[CH3:15]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=22, 3=21, 4=20, 5=17, 6=16, 7=18, 8=19, 9=23, 10=25, 11=15, 12=8, 13=7, 14=6, 15=5, 16=4, 17=3, 18=2, 19=1, 20=14, 21=13, 22=12, 23=11, 24=10, 25=9, 26=31, 27=30, 28=27, 29=26, 30=28, 31=29}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=18, 2=17, 3=16, 4=20, 5=21, 6=22, 7=23, 8=24, 9=25, 10=19, 11=15, 12=14, 13=4, 14=3, 15=2, 16=1, 17=6, 18=5, 19=7, 20=8, 21=9, 22=10, 23=11, 24=12, 25=13, 26=31, 27=30, 28=27, 29=26, 30=28, 31=29}

