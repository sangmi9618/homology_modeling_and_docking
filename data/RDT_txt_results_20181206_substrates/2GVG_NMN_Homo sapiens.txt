
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O=P(O)(O[P])OC1O[CH][CH]C1O>>O=P(O)(O)O[P]:1.0, [CH]:2.0, [CH]N=[CH]:1.0, [CH]N=[CH]>>[CH][N+]([CH])=[CH]:1.0, [CH][N+](=[CH])C1O[CH][CH]C1O:1.0, [CH][N+]([CH])=[CH]:1.0, [C]=CN=C[CH]:1.0, [C]=CN=C[CH]>>[C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]c1cnccc1>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O:1.0, [N+]:1.0, [N+]C([O])[CH]:1.0, [N]:1.0, [OH]:1.0, [O]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]C([O])[CH]:1.0, [O]C([O])[CH]>>[N+]C([O])[CH]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OC1OC([CH2])C(O)C1O>>[C]=C[N+](=C[CH])C1OC([CH2])C(O)C1O:1.0, [P]O:1.0, [P]OC1O[CH][CH]C1O:1.0, [P]OC1O[CH][CH]C1O>>[CH][N+](=[CH])C1O[CH][CH]C1O:1.0, [P]O[CH]:1.0, [P]O[CH]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [N+]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[CH][N+]([CH])=[CH]:1.0, [N+]C([O])[CH]:1.0, [O]C([O])[CH]:1.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[CH][N+](=[CH])C1O[CH][CH]C1O:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [P]OC1O[CH][CH]C1O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]C([O])[CH]>>[N+]C([O])[CH]
2: [P]O[CH]>>[P]O
3: [CH]N=[CH]>>[CH][N+]([CH])=[CH]

MMP Level 2
1: [P]OC1O[CH][CH]C1O>>[CH][N+](=[CH])C1O[CH][CH]C1O
2: [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O
3: [C]=CN=C[CH]>>[C]=C[N+](=C[CH])C([O])[CH]

MMP Level 3
1: [O]P(=O)(O)OC1OC([CH2])C(O)C1O>>[C]=C[N+](=C[CH])C1OC([CH2])C(O)C1O
2: O=P(O)(O[P])OC1O[CH][CH]C1O>>O=P(O)(O)O[P]
3: [C]c1cnccc1>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(N)c1cnccc1>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)O)C(O)C2O]
2: R:M00002, P:M00003	[O=P(O)(O)OCC1OC(OP(=O)(O)OP(=O)(O)O)C(O)C1O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)O)C(O)C2O]
3: R:M00002, P:M00004	[O=P(O)(O)OCC1OC(OP(=O)(O)OP(=O)(O)O)C(O)C1O>>O=P(O)(O)OP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:23]=[C:24]([NH2:25])[C:26]:1:[CH:27]:[N:28]:[CH:29]:[CH:30]:[CH:31]1.[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH2:6][CH:7]1[O:8][CH:9]([O:10][P:11](=[O:12])([OH:13])[O:14][P:15](=[O:16])([OH:17])[OH:18])[CH:19]([OH:20])[CH:21]1[OH:22]>>[O:23]=[C:24]([NH2:25])[C:26]:1:[CH:31]:[CH:30]:[CH:29]:[N+:28](:[CH:27]1)[CH:9]2[O:8][CH:7]([CH2:6][O:5][P:2](=[O:1])([OH:3])[OH:4])[CH:21]([OH:22])[CH:19]2[OH:20].[O:12]=[P:11]([OH:10])([OH:13])[O:14][P:15](=[O:16])([OH:17])[OH:18]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=30, 2=31, 3=26, 4=27, 5=28, 6=29, 7=24, 8=23, 9=25, 10=6, 11=7, 12=21, 13=19, 14=9, 15=8, 16=10, 17=11, 18=12, 19=13, 20=14, 21=15, 22=16, 23=17, 24=18, 25=20, 26=22, 27=5, 28=2, 29=1, 30=3, 31=4}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=5, 3=4, 4=9, 5=8, 6=7, 7=10, 8=21, 9=19, 10=12, 11=11, 12=13, 13=14, 14=15, 15=16, 16=17, 17=18, 18=20, 19=22, 20=2, 21=1, 22=3, 23=25, 24=24, 25=23, 26=26, 27=27, 28=28, 29=29, 30=30, 31=31}

