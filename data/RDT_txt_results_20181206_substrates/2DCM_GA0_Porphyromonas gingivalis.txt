
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C([CH])NC([CH])=[CH]:1.0, O=C([CH])NC([CH])=[CH]>>[CH]C(=[CH])N:1.0, O=C([CH])O:2.0, O=C([CH])[NH]:1.0, O=C([CH])[NH]>>O=C([CH])O:1.0, O>>O=C([CH])O:1.0, O>>[C]O:1.0, O>>[N]C([CH2])C(=O)O:1.0, [CH]C(=[CH])N:1.0, [C]:2.0, [C]C=C(C=[CH])NC(=O)C([N])[CH2]>>[C]C=C(N)C=[CH]:1.0, [C]N:1.0, [C]N1CCCC1C(=O)NC([CH])=[CH]>>[C]N1CCCC1C(=O)O:1.0, [C]NC(=O)C([N])[CH2]:1.0, [C]NC(=O)C([N])[CH2]>>[N]C([CH2])C(=O)O:1.0, [C]N[C]:1.0, [C]N[C]>>[C]N:1.0, [C]O:1.0, [NH2]:1.0, [NH]:1.0, [N]C([CH2])C(=O)O:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [C]:2.0, [NH]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=C([CH])O:1.0, O=C([CH])[NH]:1.0, [C]N[C]:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=C([CH])NC([CH])=[CH]:1.0, O=C([CH])O:1.0, [C]NC(=O)C([N])[CH2]:1.0, [N]C([CH2])C(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O>>[C]O
2: [C]N[C]>>[C]N
3: O=C([CH])[NH]>>O=C([CH])O

MMP Level 2
1: O>>O=C([CH])O
2: O=C([CH])NC([CH])=[CH]>>[CH]C(=[CH])N
3: [C]NC(=O)C([N])[CH2]>>[N]C([CH2])C(=O)O

MMP Level 3
1: O>>[N]C([CH2])C(=O)O
2: [C]C=C(C=[CH])NC(=O)C([N])[CH2]>>[C]C=C(N)C=[CH]
3: [C]N1CCCC1C(=O)NC([CH])=[CH]>>[C]N1CCCC1C(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(NC(C(=O)N1CCCC1C(=O)Nc2ccc3ccccc3c2)C)CN>>O=C(O)C1N(C(=O)C(NC(=O)CN)C)CCC1]
2: R:M00001, P:M00004	[O=C(NC(C(=O)N1CCCC1C(=O)Nc2ccc3ccccc3c2)C)CN>>Nc1ccc2ccccc2c1]
3: R:M00002, P:M00003	[O>>O=C(O)C1N(C(=O)C(NC(=O)CN)C)CCC1]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([NH:3][CH:4]([C:5](=[O:6])[N:7]1[CH2:8][CH2:9][CH2:10][CH:11]1[C:12](=[O:13])[NH:14][C:15]:2:[CH:16]:[CH:17]:[C:18]:3:[CH:19]:[CH:20]:[CH:21]:[CH:22]:[C:23]3:[CH:24]2)[CH3:25])[CH2:26][NH2:27].[OH2:28]>>[O:13]=[C:12]([OH:28])[CH:11]1[N:7]([C:5](=[O:6])[CH:4]([NH:3][C:2](=[O:1])[CH2:26][NH2:27])[CH3:25])[CH2:8][CH2:9][CH2:10]1.[NH2:14][C:15]:1:[CH:16]:[CH:17]:[C:18]:2:[CH:19]:[CH:20]:[CH:21]:[CH:22]:[C:23]2:[CH:24]1


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=25, 2=4, 3=5, 4=6, 5=7, 6=8, 7=9, 8=10, 9=11, 10=12, 11=13, 12=14, 13=15, 14=24, 15=23, 16=22, 17=21, 18=20, 19=19, 20=18, 21=17, 22=16, 23=3, 24=2, 25=1, 26=26, 27=27, 28=28}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=14, 2=8, 3=6, 4=7, 5=5, 6=15, 7=16, 8=17, 9=4, 10=2, 11=1, 12=3, 13=9, 14=10, 15=11, 16=12, 17=13, 18=24, 19=25, 20=26, 21=27, 22=28, 23=19, 24=20, 25=21, 26=22, 27=23, 28=18}

