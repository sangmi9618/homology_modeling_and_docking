
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%N:1.0, C=O:1.0]

ORDER_CHANGED
[O-O*O=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(C([CH])=[CH])C[CH2]:1.0, O=O:4.0, O=O>>OO:6.0, O>>O=C(C([CH])=[CH])C[CH2]:1.0, O>>[C]=O:1.0, O>>[C]C(=O)[CH2]:1.0, OO:4.0, [CH2]CNC:1.0, [CH2]NC:1.0, [CH]:1.0, [CH]=CC(=C[NH])C1N(C)CCC1>>O=C(C(C=[CH])=C[NH])CC[CH2]:1.0, [CH]C(=[CH])C1N(C)CCC1:1.0, [CH]C(=[CH])C1N(C)CCC1>>O=C(C([CH])=[CH])C[CH2]:1.0, [CH]C(=[CH])C1N(C)CCC1>>[CH2]CCNC:1.0, [CH]N([CH2])C:1.0, [CH]N([CH2])C>>[CH2]NC:1.0, [C]:1.0, [C]=O:1.0, [C]C(=O)[CH2]:2.0, [C]C([N])[CH2]:1.0, [C]C([N])[CH2]>>[C]C(=O)[CH2]:1.0, [C]C1N(C)CCC1:1.0, [C]C1N(C)CCC1>>[CH2]CNC:1.0, [NH]:1.0, [N]:1.0, [OH]:2.0, [O]:3.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[O:1.0, [CH]:1.0, [C]:1.0, [N]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, [CH]N([CH2])C:1.0, [C]=O:1.0, [C]C(=O)[CH2]:1.0, [C]C([N])[CH2]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=C(C([CH])=[CH])C[CH2]:1.0, [CH]C(=[CH])C1N(C)CCC1:1.0, [C]C(=O)[CH2]:1.0, [C]C1N(C)CCC1:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (2)
[[OH]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (2)
[O=O:2.0, OO:2.0]


ID=Reaction Center at Level: 2 (2)
[O=O:2.0, OO:2.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(=O)[CH2]:1.0, [C]C([N])[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(C([CH])=[CH])C[CH2]:1.0, [CH]C(=[CH])C1N(C)CCC1:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]N([CH2])C>>[CH2]NC
2: O>>[C]=O
3: O=O>>OO
4: [C]C([N])[CH2]>>[C]C(=O)[CH2]

MMP Level 2
1: [C]C1N(C)CCC1>>[CH2]CNC
2: O>>[C]C(=O)[CH2]
3: O=O>>OO
4: [CH]C(=[CH])C1N(C)CCC1>>O=C(C([CH])=[CH])C[CH2]

MMP Level 3
1: [CH]C(=[CH])C1N(C)CCC1>>[CH2]CCNC
2: O>>O=C(C([CH])=[CH])C[CH2]
3: O=O>>OO
4: [CH]=CC(=C[NH])C1N(C)CCC1>>O=C(C(C=[CH])=C[NH])CC[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00005	[O>>O=c1ccc(c[nH]1)C(=O)CCCNC]
2: R:M00002, P:M00004	[O=O>>OO]
3: R:M00003, P:M00005	[O=c1ccc(c[nH]1)C2N(C)CCC2>>O=c1ccc(c[nH]1)C(=O)CCCNC, O=c1ccc(c[nH]1)C2N(C)CCC2>>O=c1ccc(c[nH]1)C(=O)CCCNC]


//
SELECTED AAM MAPPING
[O:14]=[O:15].[O:1]=[C:2]1[CH:3]=[CH:4][C:5](=[CH:6][NH:7]1)[CH:8]2[N:9]([CH3:10])[CH2:11][CH2:12][CH2:13]2.[OH2:16]>>[O:1]=[C:2]1[CH:3]=[CH:4][C:5](=[CH:6][NH:7]1)[C:8](=[O:16])[CH2:13][CH2:12][CH2:11][NH:9][CH3:10].[OH:14][OH:15]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=16, 2=14, 3=15, 4=10, 5=9, 6=11, 7=12, 8=13, 9=8, 10=5, 11=6, 12=7, 13=2, 14=1, 15=3, 16=4}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=15, 2=16, 3=14, 4=13, 5=12, 6=11, 7=10, 8=8, 9=9, 10=5, 11=6, 12=7, 13=2, 14=1, 15=3, 16=4}

