
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0, C-S:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O>>O[CH2]:1.0, O>>[CH]CO:1.0, O>>[O]C([CH])CO:1.0, OC1[CH][CH]OC1CSC[CH2]>>OCC1O[CH][CH]C1O:1.0, OCC1O[CH][CH]C1O:1.0, O[CH2]:1.0, S([CH2])[CH2]:1.0, S([CH2])[CH2]>>S[CH2]:1.0, SC[CH2]:1.0, S[CH2]:1.0, [CH2]:2.0, [CH]:2.0, [CH]CO:2.0, [CH]CSC[CH2]:1.0, [CH]CSC[CH2]>>SC[CH2]:1.0, [N]C1OC(CS[CH2])C(O)C1O>>[N]C1OC(CO)C(O)C1O:1.0, [OH]:1.0, [O]C([CH])CO:1.0, [O]C([CH])CSCC[CH]>>[CH]CCS:1.0, [O]C([CH])CS[CH2]:1.0, [O]C([CH])CS[CH2]>>[O]C([CH])CO:1.0, [O]C([CH])[CH2]:2.0, [O]C([CH])[CH2]>>[O]C([CH])[CH2]:1.0, [SH]:1.0, [S]:1.0, [S]CC1O[CH][CH]C1O:1.0, [S]CC1O[CH][CH]C1O>>OCC1O[CH][CH]C1O:1.0, [S]C[CH]:1.0, [S]C[CH]>>[CH]CO:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [CH2]:2.0, [OH]:1.0, [S]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O[CH2]:1.0, S([CH2])[CH2]:1.0, [CH]CO:1.0, [S]C[CH]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, [CH]CO:1.0, [CH]CSC[CH2]:1.0, [O]C([CH])CO:1.0, [O]C([CH])CS[CH2]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (1)
[[O]C([CH])[CH2]:2.0]


ID=Reaction Center at Level: 2 (2)
[OCC1O[CH][CH]C1O:1.0, [S]CC1O[CH][CH]C1O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: S([CH2])[CH2]>>S[CH2]
2: O>>O[CH2]
3: [O]C([CH])[CH2]>>[O]C([CH])[CH2]
4: [S]C[CH]>>[CH]CO

MMP Level 2
1: [CH]CSC[CH2]>>SC[CH2]
2: O>>[CH]CO
3: [S]CC1O[CH][CH]C1O>>OCC1O[CH][CH]C1O
4: [O]C([CH])CS[CH2]>>[O]C([CH])CO

MMP Level 3
1: [O]C([CH])CSCC[CH]>>[CH]CCS
2: O>>[O]C([CH])CO
3: [N]C1OC(CS[CH2])C(O)C1O>>[N]C1OC(CO)C(O)C1O
4: OC1[CH][CH]OC1CSC[CH2]>>OCC1O[CH][CH]C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)CCSCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(O)C(N)CCS]
2: R:M00001, P:M00004	[O=C(O)C(N)CCSCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O, O=C(O)C(N)CCSCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
3: R:M00002, P:M00004	[O>>OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][S:8][CH2:9][CH:10]1[O:11][CH:12]([N:13]:2:[CH:14]:[N:15]:[C:16]:3:[C:17](:[N:18]:[CH:19]:[N:20]:[C:21]32)[NH2:22])[CH:23]([OH:24])[CH:25]1[OH:26].[OH2:27]>>[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][SH:8].[OH:27][CH2:9][CH:10]1[O:11][CH:12]([N:13]:2:[CH:14]:[N:15]:[C:16]:3:[C:17](:[N:18]:[CH:19]:[N:20]:[C:21]32)[NH2:22])[CH:23]([OH:24])[CH:25]1[OH:26]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=19, 2=20, 3=21, 4=16, 5=17, 6=18, 7=22, 8=15, 9=14, 10=13, 11=12, 12=23, 13=25, 14=10, 15=11, 16=9, 17=8, 18=7, 19=6, 20=4, 21=2, 22=1, 23=3, 24=5, 25=26, 26=24, 27=27}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=25, 2=26, 3=27, 4=23, 5=21, 6=20, 7=22, 8=24, 9=12, 10=13, 11=14, 12=9, 13=10, 14=11, 15=15, 16=8, 17=7, 18=6, 19=5, 20=16, 21=18, 22=3, 23=4, 24=2, 25=1, 26=19, 27=17}

