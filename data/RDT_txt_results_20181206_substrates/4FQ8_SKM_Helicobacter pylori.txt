
//
FINGERPRINTS BC

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(N)C=1C=[N+]C=CC1>>O=C(N)C1=C[N]C=CC1:1.0, O=C([CH])[CH]:2.0, [CH2]:1.0, [CH]:2.0, [CH]C([CH])O:2.0, [CH]C([CH])O>>O=C([CH])[CH]:2.0, [CH]N([CH])[CH]:1.0, [CH]O:1.0, [CH]O>>[C]=O:1.0, [CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]>>[CH]N([CH])[CH]:1.0, [C]:1.0, [C]=CC(=O)C([CH])O:1.0, [C]=CC(O)C([CH])O:1.0, [C]=CC(O)C([CH])O>>[C]=CC(=O)C([CH])O:2.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN(C=[CH])C([O])[CH]:1.0, [C]=O:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])C=C[CH]>>[C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C1=CC(O)C(O)C(O)C1>>[C]C1=CC(=O)C(O)C(O)C1:1.0, [C]C=[CH]:1.0, [C]C=[CH]>>[C]C[CH]:1.0, [C]C[CH]:1.0, [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]C1=CN(C=CC1)C2O[CH][CH]C2O:1.0, [N+]:1.0, [N]:1.0, [OH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH]:1.0, [C]:1.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([CH])[CH]:1.0, [CH]C([CH])O:1.0, [CH]O:1.0, [C]=O:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C([CH])[CH]:1.0, [CH]C([CH])O:1.0, [C]=CC(=O)C([CH])O:1.0, [C]=CC(O)C([CH])O:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[O=C([CH])[CH]:1.0, [CH]C([CH])O:1.0]


ID=Reaction Center at Level: 2 (2)
[[C]=CC(=O)C([CH])O:1.0, [C]=CC(O)C([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C=[CH]>>[C]C[CH]
2: [CH]C([CH])O>>O=C([CH])[CH]
3: [CH][N+]([CH])=[CH]>>[CH]N([CH])[CH]
4: [CH]O>>[C]=O

MMP Level 2
1: [C]C(=[CH])C=C[CH]>>[C]C(=[CH])CC=[CH]
2: [C]=CC(O)C([CH])O>>[C]=CC(=O)C([CH])O
3: [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN(C=[CH])C([O])[CH]
4: [CH]C([CH])O>>O=C([CH])[CH]

MMP Level 3
1: O=C(N)C=1C=[N+]C=CC1>>O=C(N)C1=C[N]C=CC1
2: [C]C1=CC(O)C(O)C(O)C1>>[C]C1=CC(=O)C(O)C(O)C1
3: [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]C1=CN(C=CC1)C2O[CH][CH]C2O
4: [C]=CC(O)C([CH])O>>[C]=CC(=O)C([CH])O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C1=CC(O)C(O)C(O)C1>>O=C1C=C(C(=O)O)CC(O)C1O, O=C(O)C1=CC(O)C(O)C(O)C1>>O=C1C=C(C(=O)O)CC(O)C1O]
2: R:M00002, P:M00004	[O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O, O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O]


//
SELECTED AAM MAPPING
[O:49]=[C:50]([OH:51])[C:52]1=[CH:53][CH:54]([OH:55])[CH:56]([OH:57])[CH:58]([OH:59])[CH2:60]1.[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:5]:[CH:6]:[CH:7]:[N+:8](:[CH:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48]>>[H+:61].[O:49]=[C:50]([OH:51])[C:52]1=[CH:53][C:54](=[O:55])[CH:56]([OH:57])[CH:58]([OH:59])[CH2:60]1.[O:1]=[C:2]([NH2:3])[C:4]1=[CH:9][N:8]([CH:7]=[CH:6][CH2:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=60, 2=58, 3=56, 4=54, 5=53, 6=52, 7=50, 8=49, 9=51, 10=55, 11=57, 12=59, 13=6, 14=5, 15=4, 16=9, 17=8, 18=7, 19=10, 20=47, 21=45, 22=12, 23=11, 24=13, 25=14, 26=15, 27=16, 28=17, 29=18, 30=19, 31=20, 32=21, 33=22, 34=23, 35=24, 36=43, 37=37, 38=26, 39=25, 40=27, 41=28, 42=29, 43=30, 44=35, 45=34, 46=33, 47=32, 48=31, 49=36, 50=38, 51=39, 52=40, 53=41, 54=42, 55=44, 56=46, 57=48, 58=2, 59=1, 60=3}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=60, 2=58, 3=56, 4=54, 5=55, 6=53, 7=52, 8=50, 9=49, 10=51, 11=57, 12=59, 13=9, 14=8, 15=7, 16=6, 17=5, 18=4, 19=2, 20=1, 21=3, 22=10, 23=47, 24=45, 25=12, 26=11, 27=13, 28=14, 29=15, 30=16, 31=17, 32=18, 33=19, 34=20, 35=21, 36=22, 37=23, 38=24, 39=43, 40=37, 41=26, 42=25, 43=27, 44=28, 45=29, 46=30, 47=35, 48=34, 49=33, 50=32, 51=31, 52=36, 53=38, 54=39, 55=40, 56=41, 57=42, 58=44, 59=46, 60=48, 61=61}

