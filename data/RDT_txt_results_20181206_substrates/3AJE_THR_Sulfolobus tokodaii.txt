
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0, O-P:2.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)C(N)C(O)C>>O=C(O[P])NC(C(=O)O)C(O)C:1.0, O=C(O[P])N[CH]:1.0, O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[CH2]:1.0, O=P(O)(O[P])O[CH2]>>[C]OP(=O)(O)O[CH2]:1.0, [CH]N:1.0, [CH]N>>[C]N[CH]:1.0, [C]:2.0, [C]C([CH])N:1.0, [C]C([CH])N>>[C]C([CH])NC([O])=O:1.0, [C]C([CH])NC([O])=O:1.0, [C]N[CH]:1.0, [C]O:1.0, [C]O>>[C]O[P]:1.0, [C]OP(=O)(O)O[CH2]:1.0, [C]O[P]:1.0, [C][O-]:1.0, [C][O-]>>O:1.0, [NH2]:1.0, [NH]:1.0, [O-]:1.0, [O-]C(=O)O:4.0, [O-]C(=O)O>>O:2.0, [O-]C(=O)O>>O=C(OP(=O)(O)O[CH2])N[CH]:1.0, [O-]C(=O)O>>O=C(O[P])N[CH]:1.0, [O-]C(=O)O>>[C]C([CH])NC(=O)OP([O])(=O)O:1.0, [O-]C(=O)O>>[O]C(=O)[NH]:1.0, [O-]C(=O)O>>[O]P(=O)(O)OC(=O)[NH]:1.0, [OH]:2.0, [O]:2.0, [O]C(=O)[NH]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OC(=O)[NH]:1.0, [O]P(=O)(O)OP(=O)(O)OC[CH]>>O=C([NH])OP(=O)(O)OC[CH]:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:2.0, [O]P([O])(=O)O>>[O]P([O])(=O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[C]:2.0, [NH]:1.0, [O-]:1.0, [O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (7)
[[C]N[CH]:1.0, [C]O[P]:1.0, [C][O-]:1.0, [O-]C(=O)O:1.0, [O]C(=O)[NH]:1.0, [O]P([O])(=O)O:2.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (7)
[O=C(O[P])N[CH]:1.0, O=P(O)(O[P])O[CH2]:1.0, [C]C([CH])NC([O])=O:1.0, [C]OP(=O)(O)O[CH2]:1.0, [O-]C(=O)O:2.0, [O]P(=O)(O)OC(=O)[NH]:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C][O-]>>O
2: [C]O>>[C]O[P]
3: [P]O[P]>>[P]O
4: [CH]N>>[C]N[CH]
5: [O]P([O])(=O)O>>[O]P([O])(=O)O
6: [O-]C(=O)O>>[O]C(=O)[NH]

MMP Level 2
1: [O-]C(=O)O>>O
2: [O-]C(=O)O>>[O]P(=O)(O)OC(=O)[NH]
3: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
4: [C]C([CH])N>>[C]C([CH])NC([O])=O
5: O=P(O)(O[P])O[CH2]>>[C]OP(=O)(O)O[CH2]
6: [O-]C(=O)O>>O=C(O[P])N[CH]

MMP Level 3
1: [O-]C(=O)O>>O
2: [O-]C(=O)O>>O=C(OP(=O)(O)O[CH2])N[CH]
3: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]
4: O=C(O)C(N)C(O)C>>O=C(O[P])NC(C(=O)O)C(O)C
5: [O]P(=O)(O)OP(=O)(O)OC[CH]>>O=C([NH])OP(=O)(O)OC[CH]
6: [O-]C(=O)O>>[C]C([CH])NC(=O)OP([O])(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(O)C(NC(=O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O)C(O)C]
2: R:M00001, P:M00006	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)O]
3: R:M00002, P:M00004	[O=C(O)C(N)C(O)C>>O=C(O)C(NC(=O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O)C(O)C]
4: R:M00003, P:M00004	[[O-]C(=O)O>>O=C(O)C(NC(=O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O)C(O)C, [O-]C(=O)O>>O=C(O)C(NC(=O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O)C(O)C]
5: R:M00003, P:M00005	[[O-]C(=O)O>>O]


//
SELECTED AAM MAPPING
[O:40]=[C:41]([O-:42])[OH:43].[O:32]=[C:33]([OH:34])[CH:35]([NH2:36])[CH:37]([OH:38])[CH3:39].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:32]=[C:33]([OH:34])[CH:35]([NH:36][C:41](=[O:40])[O:43][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31])[CH:37]([OH:38])[CH3:39].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[OH:9].[OH2:42]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=39, 33=37, 34=35, 35=33, 36=32, 37=34, 38=36, 39=38, 40=41, 41=40, 42=43, 43=42}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=33, 2=31, 3=4, 4=2, 5=1, 6=3, 7=5, 8=6, 9=7, 10=8, 11=9, 12=10, 13=11, 14=12, 15=13, 16=14, 17=29, 18=27, 19=16, 20=15, 21=17, 22=18, 23=19, 24=20, 25=25, 26=24, 27=23, 28=22, 29=21, 30=26, 31=28, 32=30, 33=32, 34=43, 35=36, 36=35, 37=34, 38=37, 39=38, 40=39, 41=40, 42=41, 43=42}

