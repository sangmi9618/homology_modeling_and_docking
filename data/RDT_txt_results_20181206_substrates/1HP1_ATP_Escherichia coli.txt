
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:6.0]

//
FINGERPRINTS RC
[O:9.0, O=P(O)(O)O:11.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O:1.0, O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O:1.0, O=P(O)(O[P])O[CH2]:1.0, O=P(O)(O[P])O[CH2]>>O=P(O)(O)O:1.0, O=P(O)(O[P])O[P]:1.0, O=P(O)(O[P])O[P]>>O=P(O)(O)O:1.0, O>>O=P(O)(O)O:6.0, O>>[P]O:3.0, O[CH2]:1.0, [CH]CO:1.0, [OH]:6.0, [O]:3.0, [O]C([CH])COP(=O)(O)O[P]>>[O]C([CH])CO:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OC[CH]:1.0, [O]P(=O)(O)OC[CH]>>[CH]CO:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O:2.0, [O]P(=O)(O)OP(=O)(O)OC[CH]>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O>>O=P(O)(O)O:1.0, [O]P([O])(=O)O:2.0, [O]P([O])(=O)O>>O=P(O)(O)O:2.0, [P]:6.0, [P]O:5.0, [P]O[CH2]:1.0, [P]O[CH2]>>O[CH2]:1.0, [P]O[P]:2.0, [P]O[P]>>[P]O:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:3.0, [OH]:3.0, [O]:3.0, [P]:6.0]


ID=Reaction Center at Level: 1 (7)
[O:3.0, O=P(O)(O)O:3.0, [O]P(=O)(O)O:1.0, [O]P([O])(=O)O:2.0, [P]O:3.0, [P]O[CH2]:1.0, [P]O[P]:2.0]


ID=Reaction Center at Level: 2 (8)
[O:3.0, O=P(O)(O)O:6.0, O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[CH2]:1.0, O=P(O)(O[P])O[P]:1.0, [O]P(=O)(O)OC[CH]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[P]>>[P]O
2: O>>[P]O
3: [P]O[P]>>[P]O
4: [O]P([O])(=O)O>>O=P(O)(O)O
5: [O]P([O])(=O)O>>O=P(O)(O)O
6: [P]O[CH2]>>O[CH2]
7: [O]P(=O)(O)O>>O=P(O)(O)O

MMP Level 2
1: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O
2: O>>O=P(O)(O)O
3: [O]P(=O)(O)OP([O])(=O)O>>O=P(O)(O)O
4: O=P(O)(O[P])O[CH2]>>O=P(O)(O)O
5: O=P(O)(O[P])O[P]>>O=P(O)(O)O
6: [O]P(=O)(O)OC[CH]>>[CH]CO
7: O=P(O)(O)O[P]>>O=P(O)(O)O

MMP Level 3
1: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O
2: O>>O=P(O)(O)O
3: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O
4: [O]P(=O)(O)OP(=O)(O)OC[CH]>>O=P(O)(O)O
5: [O]P(=O)(O)OP(=O)(O)OP(=O)(O)O>>O=P(O)(O)O
6: [O]C([CH])COP(=O)(O)O[P]>>[O]C([CH])CO
7: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)O, O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)O, O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)O, O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)O, O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)O]
3: R:M00002, P:M00004	[O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31].[OH2:32].[OH2:33].[OH2:34]>>[O:11]=[P:10]([OH:34])([OH:9])[OH:12].[O:7]=[P:6]([OH:33])([OH:5])[OH:8].[O:1]=[P:2]([OH:3])([OH:4])[OH:32].[OH:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=34}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=12, 2=13, 3=14, 4=9, 5=10, 6=11, 7=15, 8=8, 9=7, 10=6, 11=5, 12=16, 13=18, 14=3, 15=4, 16=2, 17=1, 18=19, 19=17, 20=32, 21=31, 22=30, 23=33, 24=34}

