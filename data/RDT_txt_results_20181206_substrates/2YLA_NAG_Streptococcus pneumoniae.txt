
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O>>[CH]C([CH])O:1.0, O>>[CH]O:1.0, O>>[O]C(O)C(O)C([CH])O:1.0, [CH]:4.0, [CH]C([CH])O:2.0, [CH]O:2.0, [CH]OC(O)C([CH])[NH]:1.0, [CH]OC(O[CH])C([CH])[NH]:1.0, [CH]OC(O[CH])C([CH])[NH]>>[CH]OC(O)C([CH])[NH]:1.0, [CH]O[CH]:1.0, [CH]O[CH]>>[CH]O:1.0, [C]NC1C(OC([CH2])[CH]C1O)OC([CH])[CH]>>[C]NC1C(O)OC([CH2])[CH]C1O:1.0, [OH]:2.0, [O]:1.0, [O]C(O)C(O)C([CH])O:1.0, [O]C(O)C(OC(O[CH])C([CH])[NH])C([CH])O>>[CH]OC(O)C([CH])[NH]:1.0, [O]C(O)C(O[CH])C([CH])O:1.0, [O]C(O)C(O[CH])C([CH])O>>[O]C(O)C(O)C([CH])O:1.0, [O]C([CH])O:2.0, [O]C([CH])OC([CH])[CH]:1.0, [O]C([CH])OC([CH])[CH]>>[O]C([CH])O:1.0, [O]C([CH])OC1C(O)O[CH]C(O)C1O>>OC1O[CH]C(O)C(O)C1O:1.0, [O]C([CH])[CH]:1.0, [O]C([CH])[CH]>>[CH]C([CH])O:1.0, [O]C([O])[CH]:1.0, [O]C([O])[CH]>>[O]C([CH])O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [CH]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, [CH]C([CH])O:1.0, [CH]O:1.0, [CH]O[CH]:1.0, [O]C([CH])[CH]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, [CH]C([CH])O:1.0, [O]C(O)C(O)C([CH])O:1.0, [O]C(O)C(O[CH])C([CH])O:1.0, [O]C([CH])OC([CH])[CH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]C([CH])O:1.0, [O]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[CH]OC(O)C([CH])[NH]:1.0, [CH]OC(O[CH])C([CH])[NH]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: O>>[CH]O
2: [CH]O[CH]>>[CH]O
3: [O]C([CH])[CH]>>[CH]C([CH])O
4: [O]C([O])[CH]>>[O]C([CH])O

MMP Level 2
1: O>>[CH]C([CH])O
2: [O]C([CH])OC([CH])[CH]>>[O]C([CH])O
3: [O]C(O)C(O[CH])C([CH])O>>[O]C(O)C(O)C([CH])O
4: [CH]OC(O[CH])C([CH])[NH]>>[CH]OC(O)C([CH])[NH]

MMP Level 3
1: O>>[O]C(O)C(O)C([CH])O
2: [O]C(O)C(OC(O[CH])C([CH])[NH])C([CH])O>>[CH]OC(O)C([CH])[NH]
3: [O]C([CH])OC1C(O)O[CH]C(O)C1O>>OC1O[CH]C(O)C(O)C1O
4: [C]NC1C(OC([CH2])[CH]C1O)OC([CH])[CH]>>[C]NC1C(O)OC([CH2])[CH]C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(NC1C(O)C(O)C(OC1OC2C(O)OC(CO)C(O)C2O)CO)C>>O=C(NC1C(O)OC(CO)C(O)C1O)C, O=C(NC1C(O)C(O)C(OC1OC2C(O)OC(CO)C(O)C2O)CO)C>>O=C(NC1C(O)OC(CO)C(O)C1O)C]
2: R:M00001, P:M00004	[O=C(NC1C(O)C(O)C(OC1OC2C(O)OC(CO)C(O)C2O)CO)C>>OCC1OC(O)C(O)C(O)C1O]
3: R:M00002, P:M00004	[O>>OCC1OC(O)C(O)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([NH:3][CH:4]1[CH:5]([OH:6])[CH:7]([OH:8])[CH:9]([O:10][CH:11]1[O:12][CH:13]2[CH:14]([OH:15])[O:16][CH:17]([CH2:18][OH:19])[CH:20]([OH:21])[CH:22]2[OH:23])[CH2:24][OH:25])[CH3:26].[OH2:27]>>[O:1]=[C:2]([NH:3][CH:4]1[CH:11]([OH:12])[O:10][CH:9]([CH2:24][OH:25])[CH:7]([OH:8])[CH:5]1[OH:6])[CH3:26].[OH:19][CH2:18][CH:17]1[O:16][CH:14]([OH:15])[CH:13]([OH:27])[CH:22]([OH:23])[CH:20]1[OH:21]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=26, 2=2, 3=1, 4=3, 5=4, 6=5, 7=7, 8=9, 9=10, 10=11, 11=12, 12=13, 13=22, 14=20, 15=17, 16=16, 17=14, 18=15, 19=18, 20=19, 21=21, 22=23, 23=24, 24=25, 25=8, 26=6, 27=27}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=15, 2=2, 3=1, 4=3, 5=4, 6=13, 7=11, 8=8, 9=7, 10=5, 11=6, 12=9, 13=10, 14=12, 15=14, 16=17, 17=18, 18=26, 19=24, 20=22, 21=20, 22=19, 23=21, 24=23, 25=25, 26=27, 27=16}

