
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-S:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(N)C=1C=[N+]C=CC1>>O=C(N)C1=C[N]C=CC1:1.0, O=C(O)CC(O)(C)CCO>>O=C([S])CC(O)(C)CC(=O)O:1.0, O=C(SC[CH2])[CH2]:1.0, O=C([S])[CH2]:2.0, OC([CH2])([CH2])C:2.0, OC([CH2])([CH2])C>>OC([CH2])([CH2])C:1.0, OCCC(O)([CH2])C>>O=C(SC[CH2])CC(O)([CH2])C:1.0, OC[CH2]:2.0, OC[CH2]>>O=C([S])[CH2]:2.0, O[CH2]:1.0, O[CH2]>>[C]=O:1.0, SC[CH2]:1.0, SC[CH2]>>O=C(SC[CH2])[CH2]:1.0, S[CH2]:1.0, S[CH2]>>[C]S[CH2]:1.0, [CH2]:2.0, [CH]:1.0, [CH]N([CH])[CH]:1.0, [CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]>>[CH]N([CH])[CH]:1.0, [C]:3.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN(C=[CH])C([O])[CH]:1.0, [C]=O:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])C=C[CH]>>[C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C=[CH]:1.0, [C]C=[CH]>>[C]C[CH]:1.0, [C]CC(=O)S[CH2]:1.0, [C]CC(O)(C)C[CH2]:1.0, [C]CC(O)(C)C[CH2]>>[C]CC(O)(C)C[C]:1.0, [C]CC(O)(C)C[C]:1.0, [C]CCO:1.0, [C]CCO>>[C]CC(=O)S[CH2]:2.0, [C]C[CH]:1.0, [C]S[CH2]:1.0, [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]C1=CN(C=CC1)C2O[CH][CH]C2O:1.0, [N+]:1.0, [NH]CCS>>[C]CC(=O)SCC[NH]:1.0, [N]:1.0, [OH]:1.0, [O]:1.0, [SH]:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[C]:1.0, [S]:1.0]


ID=Reaction Center at Level: 1 (2)
[O=C([S])[CH2]:1.0, [C]S[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(SC[CH2])[CH2]:1.0, [C]CC(=O)S[CH2]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH2]:1.0, [C]:1.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([S])[CH2]:1.0, OC[CH2]:1.0, O[CH2]:1.0, [C]=O:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C([S])[CH2]:1.0, OC[CH2]:1.0, [C]CC(=O)S[CH2]:1.0, [C]CCO:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[C]:2.0]


ID=Reaction Center at Level: 1 (1)
[OC([CH2])([CH2])C:2.0]


ID=Reaction Center at Level: 2 (2)
[[C]CC(O)(C)C[CH2]:1.0, [C]CC(O)(C)C[C]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: OC[CH2]>>O=C([S])[CH2]
2: S[CH2]>>[C]S[CH2]
3: [C]C=[CH]>>[C]C[CH]
4: [CH][N+]([CH])=[CH]>>[CH]N([CH])[CH]
5: O[CH2]>>[C]=O
6: OC([CH2])([CH2])C>>OC([CH2])([CH2])C

MMP Level 2
1: [C]CCO>>[C]CC(=O)S[CH2]
2: SC[CH2]>>O=C(SC[CH2])[CH2]
3: [C]C(=[CH])C=C[CH]>>[C]C(=[CH])CC=[CH]
4: [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN(C=[CH])C([O])[CH]
5: OC[CH2]>>O=C([S])[CH2]
6: [C]CC(O)(C)C[CH2]>>[C]CC(O)(C)C[C]

MMP Level 3
1: OCCC(O)([CH2])C>>O=C(SC[CH2])CC(O)([CH2])C
2: [NH]CCS>>[C]CC(=O)SCC[NH]
3: O=C(N)C=1C=[N+]C=CC1>>O=C(N)C1=C[N]C=CC1
4: [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]C1=CN(C=CC1)C2O[CH][CH]C2O
5: [C]CCO>>[C]CC(=O)S[CH2]
6: O=C(O)CC(O)(C)CCO>>O=C([S])CC(O)(C)CC(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00006	[O=C(NCCS)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O>>O=C(O)CC(O)(C)CC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O]
2: R:M00002, P:M00006	[O=C(O)CC(O)(C)CCO>>O=C(O)CC(O)(C)CC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O, O=C(O)CC(O)(C)CCO>>O=C(O)CC(O)(C)CC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O, O=C(O)CC(O)(C)CCO>>O=C(O)CC(O)(C)CC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O]
3: R:M00003, P:M00004	[O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O, O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O]


//
SELECTED AAM MAPPING
[O:97]=[C:98]([OH:99])[CH2:100][C:101]([OH:102])([CH3:103])[CH2:104][CH2:105][OH:106].[O:49]=[C:50]([NH2:51])[C:52]:1:[CH:53]:[CH:54]:[CH:55]:[N+:56](:[CH:57]1)[CH:58]2[O:59][CH:60]([CH2:61][O:62][P:63](=[O:64])([OH:65])[O:66][P:67](=[O:68])([OH:69])[O:70][CH2:71][CH:72]3[O:73][CH:74]([N:75]:4:[CH:76]:[N:77]:[C:78]:5:[C:79](:[N:80]:[CH:81]:[N:82]:[C:83]54)[NH2:84])[CH:85]([O:86][P:87](=[O:88])([OH:89])[OH:90])[CH:91]3[OH:92])[CH:93]([OH:94])[CH:95]2[OH:96].[O:1]=[C:2]([NH:3][CH2:4][CH2:5][SH:6])[CH2:7][CH2:8][NH:9][C:10](=[O:11])[CH:12]([OH:13])[C:14]([CH3:15])([CH3:16])[CH2:17][O:18][P:19](=[O:20])([OH:21])[O:22][P:23](=[O:24])([OH:25])[O:26][CH2:27][CH:28]1[O:29][CH:30]([N:31]:2:[CH:32]:[N:33]:[C:34]:3:[C:35](:[N:36]:[CH:37]:[N:38]:[C:39]32)[NH2:40])[CH:41]([OH:42])[CH:43]1[O:44][P:45](=[O:46])([OH:47])[OH:48]>>[H+:107].[O:97]=[C:98]([OH:99])[CH2:100][C:101]([OH:102])([CH3:103])[CH2:104][C:105](=[O:106])[S:6][CH2:5][CH2:4][NH:3][C:2](=[O:1])[CH2:7][CH2:8][NH:9][C:10](=[O:11])[CH:12]([OH:13])[C:14]([CH3:15])([CH3:16])[CH2:17][O:18][P:19](=[O:20])([OH:21])[O:22][P:23](=[O:24])([OH:25])[O:26][CH2:27][CH:28]1[O:29][CH:30]([N:31]:2:[CH:32]:[N:33]:[C:34]:3:[C:35](:[N:36]:[CH:37]:[N:38]:[C:39]32)[NH2:40])[CH:41]([OH:42])[CH:43]1[O:44][P:45](=[O:46])([OH:47])[OH:48].[O:49]=[C:50]([NH2:51])[C:52]1=[CH:57][N:56]([CH:55]=[CH:54][CH2:53]1)[CH:58]2[O:59][CH:60]([CH2:61][O:62][P:63](=[O:64])([OH:65])[O:66][P:67](=[O:68])([OH:69])[O:70][CH2:71][CH:72]3[O:73][CH:74]([N:75]:4:[CH:76]:[N:77]:[C:78]:5:[C:79](:[N:80]:[CH:81]:[N:82]:[C:83]54)[NH2:84])[CH:85]([O:86][P:87](=[O:88])([OH:89])[OH:90])[CH:91]3[OH:92])[CH:93]([OH:94])[CH:95]2[OH:96]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=15, 2=14, 3=16, 4=17, 5=18, 6=19, 7=20, 8=21, 9=22, 10=23, 11=24, 12=25, 13=26, 14=27, 15=28, 16=43, 17=41, 18=30, 19=29, 20=31, 21=32, 22=33, 23=34, 24=39, 25=38, 26=37, 27=36, 28=35, 29=40, 30=42, 31=44, 32=45, 33=46, 34=47, 35=48, 36=12, 37=10, 38=11, 39=9, 40=8, 41=7, 42=2, 43=1, 44=3, 45=4, 46=5, 47=6, 48=13, 49=103, 50=101, 51=104, 52=105, 53=106, 54=100, 55=98, 56=97, 57=99, 58=102, 59=54, 60=53, 61=52, 62=57, 63=56, 64=55, 65=58, 66=95, 67=93, 68=60, 69=59, 70=61, 71=62, 72=63, 73=64, 74=65, 75=66, 76=67, 77=68, 78=69, 79=70, 80=71, 81=72, 82=91, 83=85, 84=74, 85=73, 86=75, 87=76, 88=77, 89=78, 90=83, 91=82, 92=81, 93=80, 94=79, 95=84, 96=86, 97=87, 98=88, 99=89, 100=90, 101=92, 102=94, 103=96, 104=50, 105=49, 106=51}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=67, 2=66, 3=65, 4=64, 5=63, 6=62, 7=60, 8=59, 9=61, 10=68, 11=105, 12=103, 13=70, 14=69, 15=71, 16=72, 17=73, 18=74, 19=75, 20=76, 21=77, 22=78, 23=79, 24=80, 25=81, 26=82, 27=101, 28=95, 29=84, 30=83, 31=85, 32=86, 33=87, 34=88, 35=93, 36=92, 37=91, 38=90, 39=89, 40=94, 41=96, 42=97, 43=98, 44=99, 45=100, 46=102, 47=104, 48=106, 49=107, 50=7, 51=5, 52=4, 53=2, 54=1, 55=3, 56=8, 57=9, 58=10, 59=11, 60=12, 61=13, 62=14, 63=15, 64=16, 65=17, 66=18, 67=19, 68=20, 69=21, 70=22, 71=24, 72=25, 73=26, 74=27, 75=28, 76=29, 77=30, 78=31, 79=32, 80=33, 81=34, 82=35, 83=36, 84=37, 85=38, 86=53, 87=51, 88=40, 89=39, 90=41, 91=42, 92=43, 93=44, 94=49, 95=48, 96=47, 97=46, 98=45, 99=50, 100=52, 101=54, 102=55, 103=56, 104=57, 105=58, 106=23, 107=6}

