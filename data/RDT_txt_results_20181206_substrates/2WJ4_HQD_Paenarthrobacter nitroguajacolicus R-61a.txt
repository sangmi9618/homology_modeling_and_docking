
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%C:2.0, C-O:1.0, C=O:1.0, O=O:1.0]

ORDER_CHANGED
[C#O*C-O:1.0]

//
FINGERPRINTS RC
[O=C([NH])C:2.0, O=C1C(=[CH])[C]NC(=C1O)C>>[C-]#[O+]:1.0, O=C1[C]C(=[CH])NC(=C1O)C>>[C]C(=[CH])NC(=O)C:1.0, O=O:4.0, O=O>>O=C([NH])C:1.0, O=O>>[C]=O:1.0, O=O>>[C]C(=O)O:1.0, O=O>>[C]C(=[CH])C(=O)O:1.0, O=O>>[C]NC(=O)C:1.0, O=O>>[C]O:1.0, O=c1c(O)c([nH]c(=[CH])c1=C[CH])C>>O=C(O)C(=C[CH])C(=[CH])[NH]:1.0, [C-]:1.0, [C-]#[O+]:4.0, [C]:5.0, [C]=C([NH])C:1.0, [C]=C([NH])C>>O=C([NH])C:1.0, [C]=O:1.0, [C]C(=O)C(O)=C([NH])C:1.0, [C]C(=O)C(O)=C([NH])C>>[C-]#[O+]:2.0, [C]C(=O)O:2.0, [C]C(=[CH])C(=O)C(=[C])O:1.0, [C]C(=[CH])C(=O)C(=[C])O>>[C]C(=[CH])C(=O)O:1.0, [C]C(=[CH])C(=O)O:1.0, [C]C(=[C])O:2.0, [C]C(=[C])O>>[C-]#[O+]:2.0, [C]C([C])=O:1.0, [C]C([C])=O>>[C]C(=O)O:1.0, [C]NC(=C([C])O)C:1.0, [C]NC(=C([C])O)C>>[C]NC(=O)C:1.0, [C]NC(=O)C:1.0, [C]O:2.0, [C]O>>[C-]#[O+]:1.0, [O+]:1.0, [OH]:2.0, [O]:3.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[C]:6.0, [OH]:1.0, [O]:3.0]


ID=Reaction Center at Level: 1 (8)
[O=C([NH])C:1.0, O=O:2.0, [C]=C([NH])C:1.0, [C]=O:1.0, [C]C(=O)O:1.0, [C]C(=[C])O:2.0, [C]C([C])=O:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (8)
[O=C([NH])C:1.0, O=O:2.0, [C]C(=O)C(O)=C([NH])C:2.0, [C]C(=O)O:1.0, [C]C(=[CH])C(=O)C(=[C])O:1.0, [C]C(=[CH])C(=O)O:1.0, [C]NC(=C([C])O)C:1.0, [C]NC(=O)C:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[C-]:1.0, [C]:1.0, [O+]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (3)
[[C-]#[O+]:2.0, [C]C(=[C])O:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (3)
[[C-]#[O+]:2.0, [C]C(=O)C(O)=C([NH])C:1.0, [C]C(=[C])O:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C([C])=O>>[C]C(=O)O
2: [C]O>>[C-]#[O+]
3: O=O>>[C]=O
4: [C]C(=[C])O>>[C-]#[O+]
5: [C]=C([NH])C>>O=C([NH])C
6: O=O>>[C]O

MMP Level 2
1: [C]C(=[CH])C(=O)C(=[C])O>>[C]C(=[CH])C(=O)O
2: [C]C(=[C])O>>[C-]#[O+]
3: O=O>>O=C([NH])C
4: [C]C(=O)C(O)=C([NH])C>>[C-]#[O+]
5: [C]NC(=C([C])O)C>>[C]NC(=O)C
6: O=O>>[C]C(=O)O

MMP Level 3
1: O=c1c(O)c([nH]c(=[CH])c1=C[CH])C>>O=C(O)C(=C[CH])C(=[CH])[NH]
2: [C]C(=O)C(O)=C([NH])C>>[C-]#[O+]
3: O=O>>[C]NC(=O)C
4: O=C1C(=[CH])[C]NC(=C1O)C>>[C-]#[O+]
5: O=C1[C]C(=[CH])NC(=C1O)C>>[C]C(=[CH])NC(=O)C
6: O=O>>[C]C(=[CH])C(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=O>>O=C(O)c1ccccc1NC(=O)C, O=O>>O=C(O)c1ccccc1NC(=O)C]
2: R:M00002, P:M00003	[O=c1c(O)c([nH]c2ccccc12)C>>[C-]#[O+], O=c1c(O)c([nH]c2ccccc12)C>>[C-]#[O+]]
3: R:M00002, P:M00004	[O=c1c(O)c([nH]c2ccccc12)C>>O=C(O)c1ccccc1NC(=O)C, O=c1c(O)c([nH]c2ccccc12)C>>O=C(O)c1ccccc1NC(=O)C]


//
SELECTED AAM MAPPING
[O:14]=[O:15].[O:1]=[C:2]1[C:3]([OH:4])=[C:5]([NH:6][C:7]:2:[CH:8]:[CH:9]:[CH:10]:[CH:11]:[C:12]12)[CH3:13]>>[C-:3]#[O+:4].[O:1]=[C:2]([OH:14])[C:12]:1:[CH:11]:[CH:10]:[CH:9]:[CH:8]:[C:7]1[NH:6][C:5](=[O:15])[CH3:13]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=14, 2=15, 3=13, 4=5, 5=3, 6=2, 7=1, 8=12, 9=11, 10=10, 11=9, 12=8, 13=7, 14=6, 15=4}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=14, 2=15, 3=13, 4=11, 5=12, 6=10, 7=9, 8=8, 9=7, 10=6, 11=5, 12=4, 13=2, 14=1, 15=3}

