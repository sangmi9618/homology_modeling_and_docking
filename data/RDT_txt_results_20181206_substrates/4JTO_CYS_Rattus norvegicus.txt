
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-S:1.0, O=O:1.0, O=S:1.0]

//
FINGERPRINTS RC
[O=O:4.0, O=O>>O=S:1.0, O=O>>O=S(O)C[CH]:2.0, O=O>>O=S(O)[CH2]:2.0, O=O>>[S]O:1.0, O=S:1.0, O=S(O)C[CH]:1.0, O=S(O)[CH2]:3.0, S[CH2]:1.0, S[CH2]>>O=S(O)[CH2]:1.0, [CH]CS:1.0, [CH]CS>>O=S(O)C[CH]:1.0, [C]C(N)CS>>[C]C(N)CS(=O)O:1.0, [OH]:1.0, [O]:3.0, [SH]:1.0, [S]:1.0, [S]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[OH]:1.0, [O]:3.0, [S]:2.0]


ID=Reaction Center at Level: 1 (4)
[O=O:2.0, O=S:1.0, O=S(O)[CH2]:2.0, [S]O:1.0]


ID=Reaction Center at Level: 2 (3)
[O=O:2.0, O=S(O)C[CH]:2.0, O=S(O)[CH2]:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=O>>[S]O
2: O=O>>O=S
3: S[CH2]>>O=S(O)[CH2]

MMP Level 2
1: O=O>>O=S(O)[CH2]
2: O=O>>O=S(O)[CH2]
3: [CH]CS>>O=S(O)C[CH]

MMP Level 3
1: O=O>>O=S(O)C[CH]
2: O=O>>O=S(O)C[CH]
3: [C]C(N)CS>>[C]C(N)CS(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)CS>>O=C(O)C(N)CS(=O)O]
2: R:M00002, P:M00003	[O=O>>O=C(O)C(N)CS(=O)O, O=O>>O=C(O)C(N)CS(=O)O]


//
SELECTED AAM MAPPING
[O:8]=[O:9].[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][SH:7]>>[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][S:7](=[O:8])[OH:9]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=4, 3=2, 4=1, 5=3, 6=5, 7=7, 8=8, 9=9}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=4, 3=2, 4=1, 5=3, 6=5, 7=7, 8=8, 9=9}

