
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[[CH2]:2.0, [CH]C(=[CH])N:1.0, [CH]C(=[CH])N>>[C]CNC([CH])=[CH]:1.0, [CH]C=C(N)C=[CH]>>[N]=C([CH2])CNC(=C[CH])C=[CH]:1.0, [C]CNC([CH])=[CH]:1.0, [C]COP([O])(=O)O:1.0, [C]COP([O])(=O)O>>[O]P(=O)(O)O:1.0, [C]C[NH]:1.0, [C]C[O]:1.0, [C]C[O]>>[C]C[NH]:1.0, [C]N:1.0, [C]N=C(C[NH])COP([O])(=O)O>>[C]N=C(C[NH])CNC([CH])=[CH]:1.0, [C]N>>[C]N[CH2]:1.0, [C]NCC(=[N])[CH2]:1.0, [C]N[CH2]:1.0, [NH2]:1.0, [NH]:1.0, [N]=C([CH2])COP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, [N]=C([CH2])CO[P]:1.0, [N]=C([CH2])CO[P]>>[C]NCC(=[N])[CH2]:1.0, [OH]:1.0, [O]:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[CH2]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH2]:2.0, [NH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[C]C[NH]:1.0, [C]C[O]:1.0, [C]N[CH2]:1.0, [P]O[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]CNC([CH])=[CH]:1.0, [C]COP([O])(=O)O:1.0, [C]NCC(=[N])[CH2]:1.0, [N]=C([CH2])CO[P]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]N>>[C]N[CH2]
2: [C]C[O]>>[C]C[NH]
3: [P]O[CH2]>>[P]O

MMP Level 2
1: [CH]C(=[CH])N>>[C]CNC([CH])=[CH]
2: [N]=C([CH2])CO[P]>>[C]NCC(=[N])[CH2]
3: [C]COP([O])(=O)O>>[O]P(=O)(O)O

MMP Level 3
1: [CH]C=C(N)C=[CH]>>[N]=C([CH2])CNC(=C[CH])C=[CH]
2: [C]N=C(C[NH])COP([O])(=O)O>>[C]N=C(C[NH])CNC([CH])=[CH]
3: [N]=C([CH2])COP(=O)(O)O[P]>>O=P(O)(O)O[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1nc(N)[nH]c2NCC(=Nc12)COP(=O)(O)OP(=O)(O)O>>O=P(O)(O)OP(=O)(O)O]
2: R:M00001, P:M00004	[O=c1nc(N)[nH]c2NCC(=Nc12)COP(=O)(O)OP(=O)(O)O>>O=c1nc(N)[nH]c2NCC(=Nc12)CNc3ccc(cc3)C(=O)O]
3: R:M00002, P:M00004	[O=C(O)c1ccc(N)cc1>>O=c1nc(N)[nH]c2NCC(=Nc12)CNc3ccc(cc3)C(=O)O]


//
SELECTED AAM MAPPING
[O:23]=[C:24]([OH:25])[C:26]:1:[CH:27]:[CH:28]:[C:29]([NH2:30]):[CH:31]:[CH:32]1.[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]=2[NH:8][CH2:9][C:10](=[N:11][C:12]12)[CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[OH:22]>>[O:23]=[C:24]([OH:25])[C:26]:1:[CH:32]:[CH:31]:[C:29](:[CH:28]:[CH:27]1)[NH:30][CH2:13][C:10]2=[N:11][C:12]=3[C:2](=[O:1])[N:3]=[C:4]([NH2:5])[NH:6][C:7]3[NH:8][CH2:9]2.[O:16]=[P:15]([OH:14])([OH:17])[O:18][P:19](=[O:20])([OH:21])[OH:22]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=10, 3=11, 4=12, 5=7, 6=8, 7=6, 8=4, 9=3, 10=2, 11=1, 12=5, 13=13, 14=14, 15=15, 16=16, 17=17, 18=18, 19=19, 20=20, 21=21, 22=22, 23=27, 24=28, 25=29, 26=31, 27=32, 28=26, 29=24, 30=23, 31=25, 32=30}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=26, 2=25, 3=24, 4=27, 5=28, 6=29, 7=30, 8=31, 9=32, 10=23, 11=12, 12=13, 13=14, 14=21, 15=22, 16=20, 17=18, 18=17, 19=15, 20=16, 21=19, 22=11, 23=10, 24=7, 25=6, 26=5, 27=4, 28=9, 29=8, 30=2, 31=1, 32=3}

