
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:4.0]

STEREO_CHANGED
[C(R/S):4.0]

//
FINGERPRINTS RC
[O=P(O)(O[P])OC(O[CH])C([CH])O>>O=P(O)(O)O[P]:1.0, OC1OC([CH2])[CH]C(O)C1O>>[CH]C([CH])OC1OC([CH2])[CH]C(O)C1O:1.0, OCC1O[CH]C(O)C(O)C1O>>[O]C([CH])OC1C(O)C(O)[CH]OC1CO:1.0, [CH]:10.0, [CH]C(O)C(O)C([CH])O:1.0, [CH]C(O)C(O)C([CH])O>>[O]C([CH])C(O)C([CH])O:1.0, [CH]C([CH])O:4.0, [CH]C([CH])O>>[CH]C([CH])O:1.0, [CH]C([CH])O>>[O]C([CH])O:1.0, [CH]C([CH])O>>[O]C([CH])[CH]:1.0, [CH]C([CH])OC1OC([CH2])[CH]C(O)C1O>>[CH]C([CH])OC1OC([CH2])[CH]C(O)C1O:1.0, [CH]O:3.0, [CH]O>>[CH]O:1.0, [CH]O>>[CH]O[CH]:1.0, [CH]OC(O)C([CH])O:2.0, [CH]OC(O)C([CH])O>>[CH]OC(O[CH])C([CH])O:1.0, [CH]OC(O)C([CH])O>>[O]C([CH2])C(OC(O[CH])C([CH])O)C([CH])O:1.0, [CH]OC(O[CH])C([CH])O:3.0, [CH]OC(O[CH])C([CH])O>>[CH]OC(O[CH])C([CH])O:1.0, [CH]O[CH]:1.0, [OH]:4.0, [O]:2.0, [O]C([CH2])C(O)C([CH])O:1.0, [O]C([CH2])C(O)C([CH])O>>[CH]OC(O)C([CH])O:1.0, [O]C([CH2])C(O)C([CH])O>>[O]C([CH2])C(O[CH])C([CH])O:1.0, [O]C([CH2])C(O[CH])C([CH])O:1.0, [O]C([CH])C(O)C([CH])O:1.0, [O]C([CH])O:4.0, [O]C([CH])O>>[O]C([CH])OC([CH])[CH]:1.0, [O]C([CH])O>>[O]C([O])[CH]:1.0, [O]C([CH])OC([CH])[CH]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]C([CH])[CH]:1.0, [O]C([O])[CH]:4.0, [O]C([O])[CH]>>[O]C([CH])O:1.0, [O]C([O])[CH]>>[O]C([O])[CH]:1.0, [O]C1OC([CH2])C(O)C(O)C1O>>[CH]OC1C(O)C(O)C(O)OC1[CH2]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OC1OC([CH2])[CH]C(O)C1O>>OC1OC([CH2])[CH]C(O)C1O:1.0, [P]O:1.0, [P]OC(O[CH])C([CH])O:1.0, [P]OC(O[CH])C([CH])O>>[CH]OC(O)C([CH])O:1.0, [P]O[CH]:1.0, [P]O[CH]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH]:4.0, [OH]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (7)
[[CH]C([CH])O:1.0, [CH]O:2.0, [CH]O[CH]:1.0, [O]C([CH])O:1.0, [O]C([CH])[CH]:1.0, [O]C([O])[CH]:1.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (8)
[[CH]C([CH])O:1.0, [CH]OC(O)C([CH])O:1.0, [O]C([CH2])C(O)C([CH])O:1.0, [O]C([CH2])C(O[CH])C([CH])O:1.0, [O]C([CH])O:1.0, [O]C([CH])OC([CH])[CH]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [P]OC(O[CH])C([CH])O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:8.0]


ID=Reaction Center at Level: 1 (3)
[[CH]C([CH])O:2.0, [O]C([CH])O:2.0, [O]C([O])[CH]:4.0]


ID=Reaction Center at Level: 2 (5)
[[CH]C(O)C(O)C([CH])O:1.0, [CH]OC(O)C([CH])O:2.0, [CH]OC(O[CH])C([CH])O:3.0, [O]C([CH])C(O)C([CH])O:1.0, [P]OC(O[CH])C([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [O]C([O])[CH]>>[O]C([CH])O
2: [CH]O>>[CH]O[CH]
3: [CH]O>>[CH]O
4: [CH]C([CH])O>>[CH]C([CH])O
5: [O]C([O])[CH]>>[O]C([O])[CH]
6: [O]C([CH])O>>[O]C([O])[CH]
7: [P]O[CH]>>[P]O
8: [CH]C([CH])O>>[O]C([CH])[CH]

MMP Level 2
1: [P]OC(O[CH])C([CH])O>>[CH]OC(O)C([CH])O
2: [O]C([CH])O>>[O]C([CH])OC([CH])[CH]
3: [CH]C([CH])O>>[O]C([CH])O
4: [CH]C(O)C(O)C([CH])O>>[O]C([CH])C(O)C([CH])O
5: [CH]OC(O[CH])C([CH])O>>[CH]OC(O[CH])C([CH])O
6: [CH]OC(O)C([CH])O>>[CH]OC(O[CH])C([CH])O
7: [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O
8: [O]C([CH2])C(O)C([CH])O>>[O]C([CH2])C(O[CH])C([CH])O

MMP Level 3
1: [O]P(=O)(O)OC1OC([CH2])[CH]C(O)C1O>>OC1OC([CH2])[CH]C(O)C1O
2: [CH]OC(O)C([CH])O>>[O]C([CH2])C(OC(O[CH])C([CH])O)C([CH])O
3: [O]C([CH2])C(O)C([CH])O>>[CH]OC(O)C([CH])O
4: [O]C1OC([CH2])C(O)C(O)C1O>>[CH]OC1C(O)C(O)C(O)OC1[CH2]
5: [CH]C([CH])OC1OC([CH2])[CH]C(O)C1O>>[CH]C([CH])OC1OC([CH2])[CH]C(O)C1O
6: OC1OC([CH2])[CH]C(O)C1O>>[CH]C([CH])OC1OC([CH2])[CH]C(O)C1O
7: O=P(O)(O[P])OC(O[CH])C([CH])O>>O=P(O)(O)O[P]
8: OCC1O[CH]C(O)C(O)C1O>>[O]C([CH])OC1C(O)C(O)[CH]OC1CO


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3O)C(O)C2O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)O)C(O)C2O]
2: R:M00001, P:M00004	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3O)C(O)C2O>>OCC1OC(OC2C(O)C(O)C(OC2CO)OC3C(O)C(O)C(O)OC3CO)C(O)C(O)C1O, O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3O)C(O)C2O>>OCC1OC(OC2C(O)C(O)C(OC2CO)OC3C(O)C(O)C(O)OC3CO)C(O)C(O)C1O, O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3O)C(O)C2O>>OCC1OC(OC2C(O)C(O)C(OC2CO)OC3C(O)C(O)C(O)OC3CO)C(O)C(O)C1O, O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3O)C(O)C2O>>OCC1OC(OC2C(O)C(O)C(OC2CO)OC3C(O)C(O)C(O)OC3CO)C(O)C(O)C1O]
3: R:M00002, P:M00004	[OCC1OC(OC2C(O)C(O)C(O)OC2CO)C(O)C(O)C1O>>OCC1OC(OC2C(O)C(O)C(OC2CO)OC3C(O)C(O)C(O)OC3CO)C(O)C(O)C1O, OCC1OC(OC2C(O)C(O)C(O)OC2CO)C(O)C(O)C1O>>OCC1OC(OC2C(O)C(O)C(OC2CO)OC3C(O)C(O)C(O)OC3CO)C(O)C(O)C1O, OCC1OC(OC2C(O)C(O)C(O)OC2CO)C(O)C(O)C1O>>OCC1OC(OC2C(O)C(O)C(OC2CO)OC3C(O)C(O)C(O)OC3CO)C(O)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][CH:22]3[O:23][CH:24]([CH2:25][OH:26])[CH:27]([OH:28])[CH:29]([OH:30])[CH:31]3[OH:32])[CH:33]([OH:34])[CH:35]2[OH:36].[OH:37][CH2:38][CH:39]1[O:40][CH:41]([O:42][CH:43]2[CH:44]([OH:45])[CH:46]([OH:47])[CH:48]([OH:49])[O:50][CH:51]2[CH2:52][OH:53])[CH:54]([OH:55])[CH:56]([OH:57])[CH:58]1[OH:59]>>[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[OH:21])[CH:33]([OH:34])[CH:35]2[OH:36].[OH:37][CH2:38][CH:39]1[O:40][CH:41]([O:42][CH:43]2[CH:44]([OH:45])[CH:46]([OH:47])[CH:48]([O:50][CH:51]2[CH2:52][OH:53])[O:49][CH:27]3[CH:29]([OH:30])[CH:31]([OH:32])[CH:22]([OH:28])[O:23][CH:24]3[CH2:25][OH:26])[CH:54]([OH:55])[CH:56]([OH:57])[CH:58]1[OH:59]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=3, 2=4, 3=5, 4=6, 5=7, 6=8, 7=2, 8=1, 9=9, 10=35, 11=33, 12=11, 13=10, 14=12, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=31, 26=29, 27=27, 28=24, 29=23, 30=25, 31=26, 32=28, 33=30, 34=32, 35=34, 36=36, 37=38, 38=39, 39=58, 40=56, 41=54, 42=41, 43=40, 44=42, 45=43, 46=51, 47=50, 48=48, 49=46, 50=44, 51=45, 52=47, 53=49, 54=52, 55=53, 56=55, 57=57, 58=59, 59=37}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=37, 2=38, 3=39, 4=40, 5=41, 6=42, 7=36, 8=35, 9=43, 10=58, 11=56, 12=45, 13=44, 14=46, 15=47, 16=48, 17=49, 18=50, 19=51, 20=52, 21=53, 22=54, 23=55, 24=57, 25=59, 26=2, 27=3, 28=33, 29=31, 30=29, 31=5, 32=4, 33=6, 34=7, 35=14, 36=13, 37=12, 38=10, 39=8, 40=9, 41=11, 42=17, 43=18, 44=26, 45=25, 46=23, 47=21, 48=19, 49=20, 50=22, 51=24, 52=27, 53=28, 54=15, 55=16, 56=30, 57=32, 58=34, 59=1}

