
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:2.0]

//
FINGERPRINTS RC
[[CH3]:2.0, [CH]C(=[CH])N:1.0, [CH]C(=[CH])N(C)C:1.0, [CH]C(=[CH])N(C)C>>[CH]C(=[CH])NC:2.0, [CH]C(=[CH])N>>[CH]C(=[CH])NC:1.0, [CH]C(=[CH])NC:2.0, [CH]C=C(C=[CH])N(C)C>>[CH]C=C(C=[CH])NC:1.0, [CH]C=C(N)C=[CH]>>[CH]C=C(C=[CH])NC:1.0, [C]N:1.0, [C]N(C)C:2.0, [C]N(C)C>>[C]NC:2.0, [C]N>>[C]NC:1.0, [C]NC:3.0, [Cu+2]:3.0, [Cu+2]>>[Cu+]:3.0, [Cu+]:3.0, [NH2]:1.0, [NH]:2.0, [NH]C:1.0, [N]:1.0, [N]C:1.0, [N]C>>[NH]C:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[CH3]:2.0, [Cu+2]:1.0, [Cu+]:1.0, [NH]:1.0, [N]:1.0]


ID=Reaction Center at Level: 1 (6)
[[C]N(C)C:1.0, [C]NC:1.0, [Cu+2]:1.0, [Cu+]:1.0, [NH]C:1.0, [N]C:1.0]


ID=Reaction Center at Level: 2 (6)
[[CH]C(=[CH])N(C)C:1.0, [CH]C(=[CH])NC:1.0, [C]N(C)C:1.0, [C]NC:1.0, [Cu+2]:1.0, [Cu+]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]N(C)C>>[C]NC
2: [N]C>>[NH]C
3: [C]N>>[C]NC
4: [Cu+2]>>[Cu+]

MMP Level 2
1: [CH]C(=[CH])N(C)C>>[CH]C(=[CH])NC
2: [C]N(C)C>>[C]NC
3: [CH]C(=[CH])N>>[CH]C(=[CH])NC
4: [Cu+2]>>[Cu+]

MMP Level 3
1: [CH]C=C(C=[CH])N(C)C>>[CH]C=C(C=[CH])NC
2: [CH]C(=[CH])N(C)C>>[CH]C(=[CH])NC
3: [CH]C=C(N)C=[CH]>>[CH]C=C(C=[CH])NC
4: [Cu+2]>>[Cu+]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[Nc1ccc(cc1)N(C)C>>c1cc(ccc1NC)NC, Nc1ccc(cc1)N(C)C>>c1cc(ccc1NC)NC, Nc1ccc(cc1)N(C)C>>c1cc(ccc1NC)NC]
2: R:M00002, P:M00005	[[Cu+2]>>[Cu+]]


//
SELECTED AAM MAPPING
[Cu+2:11].[O:12]=[O:13].[NH2:1][C:2]:1:[CH:3]:[CH:4]:[C:5](:[CH:6]:[CH:7]1)[N:8]([CH3:9])[CH3:10]>>[Cu+:11].[CH:6]:1:[CH:7]:[C:2](:[CH:3]:[CH:4]:[C:5]1[NH:8][CH3:10])[NH:1][CH3:9]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=10, 4=5, 5=4, 6=3, 7=2, 8=7, 9=6, 10=1, 11=13, 12=11, 13=12}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=8, 2=7, 3=6, 4=1, 5=2, 6=3, 7=4, 8=5, 9=9, 10=10, 11=11}

