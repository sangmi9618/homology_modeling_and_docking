
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%O:1.0]

ORDER_CHANGED
[C%O*C=O:1.0]

STEREO_CHANGED
[C(R/S):3.0]

//
FINGERPRINTS RC
[O=C([CH2])C(O)C(O)[CH2]:1.0, O=C([CH])[CH2]:2.0, O=P(O)(O)OC1OC([CH2])C(O)C1O>>[C]C(O)C(O)COP(=O)(O)O:1.0, OC1[CH]OC([CH2])C1O:1.0, OC1[CH]OC([CH2])C1O>>O=C([CH2])C(O)C(O)[CH2]:1.0, [CH2]:1.0, [CH]:4.0, [CH]C([CH])O:1.0, [CH]C([CH])O>>[C]C([CH])O:1.0, [CH]O[CH]:1.0, [CH]O[CH]>>[C]=O:1.0, [C]:1.0, [C]=O:1.0, [C]C([CH])O:1.0, [O]:2.0, [O]C([CH])[CH2]:1.0, [O]C([CH])[CH2]>>O=C([CH])[CH2]:1.0, [O]C([O])[CH]:1.0, [O]C([O])[CH]>>[O]C[CH]:1.0, [O]C1OC(CO[P])C(O)C1O>>O=C(CO[P])C(O)C(O)[CH2]:1.0, [O]C1OC([CH2])[CH][CH]1:1.0, [O]C1OC([CH2])[CH][CH]1>>O=C([CH])[CH2]:1.0, [O]CC(=O)C([CH])O:1.0, [O]CC1OC(O[P])C(O)C1O>>[O]CC(=O)C([CH])O:1.0, [O]CC1OC([O])C(O)C1O>>[O]CC(=O)C(O)C(O)C[O]:1.0, [O]CC1O[CH][CH]C1O:1.0, [O]CC1O[CH][CH]C1O>>[O]CC(=O)C([CH])O:1.0, [O]C[CH]:1.0, [P]OC1O[CH][CH]C1O:1.0, [P]OC1O[CH][CH]C1O>>[P]OCC([CH])O:1.0, [P]OCC([CH])O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (2)
[[CH]O[CH]:1.0, [O]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[O]C1OC([CH2])[CH][CH]1:1.0, [P]OC1O[CH][CH]C1O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[CH]:1.0, [C]:1.0, [O]:2.0]


ID=Reaction Center at Level: 1 (4)
[O=C([CH])[CH2]:1.0, [CH]O[CH]:1.0, [C]=O:1.0, [O]C([CH])[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C([CH])[CH2]:1.0, [O]C1OC([CH2])[CH][CH]1:1.0, [O]CC(=O)C([CH])O:1.0, [O]CC1O[CH][CH]C1O:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (3)
[[CH2]:1.0, [CH]:4.0, [C]:1.0]


ID=Reaction Center at Level: 1 (6)
[O=C([CH])[CH2]:1.0, [CH]C([CH])O:1.0, [C]C([CH])O:1.0, [O]C([CH])[CH2]:1.0, [O]C([O])[CH]:1.0, [O]C[CH]:1.0]


ID=Reaction Center at Level: 2 (6)
[O=C([CH2])C(O)C(O)[CH2]:1.0, OC1[CH]OC([CH2])C1O:1.0, [O]CC(=O)C([CH])O:1.0, [O]CC1O[CH][CH]C1O:1.0, [P]OC1O[CH][CH]C1O:1.0, [P]OCC([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]C([CH])O>>[C]C([CH])O
2: [CH]O[CH]>>[C]=O
3: [O]C([O])[CH]>>[O]C[CH]
4: [O]C([CH])[CH2]>>O=C([CH])[CH2]

MMP Level 2
1: OC1[CH]OC([CH2])C1O>>O=C([CH2])C(O)C(O)[CH2]
2: [O]C1OC([CH2])[CH][CH]1>>O=C([CH])[CH2]
3: [P]OC1O[CH][CH]C1O>>[P]OCC([CH])O
4: [O]CC1O[CH][CH]C1O>>[O]CC(=O)C([CH])O

MMP Level 3
1: [O]CC1OC([O])C(O)C1O>>[O]CC(=O)C(O)C(O)C[O]
2: [O]CC1OC(O[P])C(O)C1O>>[O]CC(=O)C([CH])O
3: O=P(O)(O)OC1OC([CH2])C(O)C1O>>[C]C(O)C(O)COP(=O)(O)O
4: [O]C1OC(CO[P])C(O)C1O>>O=C(CO[P])C(O)C(O)[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=P(O)(O)OCC1OC(OP(=O)(O)O)C(O)C1O>>O=C(COP(=O)(O)O)C(O)C(O)COP(=O)(O)O, O=P(O)(O)OCC1OC(OP(=O)(O)O)C(O)C1O>>O=C(COP(=O)(O)O)C(O)C(O)COP(=O)(O)O, O=P(O)(O)OCC1OC(OP(=O)(O)O)C(O)C1O>>O=C(COP(=O)(O)O)C(O)C(O)COP(=O)(O)O, O=P(O)(O)OCC1OC(OP(=O)(O)O)C(O)C1O>>O=C(COP(=O)(O)O)C(O)C(O)COP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH2:6][CH:7]1[O:8][CH:9]([O:10][P:11](=[O:12])([OH:13])[OH:14])[CH:15]([OH:16])[CH:17]1[OH:18]>>[O:8]=[C:7]([CH2:6][O:5][P:2](=[O:1])([OH:4])[OH:3])[CH:17]([OH:18])[CH:15]([OH:16])[CH2:9][O:10][P:11](=[O:12])([OH:13])[OH:14]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=17, 4=15, 5=9, 6=8, 7=10, 8=11, 9=12, 10=13, 11=14, 12=16, 13=18, 14=5, 15=2, 16=1, 17=3, 18=4}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=13, 2=11, 3=9, 4=2, 5=1, 6=3, 7=4, 8=5, 9=6, 10=7, 11=8, 12=10, 13=12, 14=14, 15=15, 16=16, 17=17, 18=18}

