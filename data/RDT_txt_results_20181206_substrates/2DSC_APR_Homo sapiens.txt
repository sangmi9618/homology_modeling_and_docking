
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O[CH2])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]:1.0, O=P(O)(O[P])O[CH2]:1.0, O=P(O)(O[P])O[CH2]>>O=P(O)(O)O[CH2]:1.0, O>>O=P(O)(O)O[CH2]:1.0, O>>[O]P(=O)(O)O:1.0, O>>[P]O:1.0, OC1OC([CH2])C(O)C1O>>OC1OC([CH2])C(O)C1O:1.0, OC1O[CH][CH]C1O:2.0, OC1O[CH][CH]C1O>>OC1O[CH][CH]C1O:1.0, [CH]:2.0, [OH]:2.0, [O]:1.0, [O]C([CH])O:2.0, [O]C([CH])O>>[O]C([CH])O:1.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)OP(=O)(O)OC[CH]>>O=P(O)(O)OC[CH]:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [O]P([O])(=O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, [O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O[P])O[CH2]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (1)
[[O]C([CH])O:2.0]


ID=Reaction Center at Level: 2 (1)
[OC1O[CH][CH]C1O:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: O>>[P]O
2: [O]C([CH])O>>[O]C([CH])O
3: [O]P([O])(=O)O>>[O]P(=O)(O)O
4: [P]O[P]>>[P]O

MMP Level 2
1: O>>[O]P(=O)(O)O
2: OC1O[CH][CH]C1O>>OC1O[CH][CH]C1O
3: O=P(O)(O[P])O[CH2]>>O=P(O)(O)O[CH2]
4: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O

MMP Level 3
1: O>>O=P(O)(O)O[CH2]
2: OC1OC([CH2])C(O)C1O>>OC1OC([CH2])C(O)C1O
3: [O]P(=O)(O)OP(=O)(O)OC[CH]>>O=P(O)(O)OC[CH]
4: O=P(O)(O[CH2])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(OCC1OC(O)C(O)C1O)OP(=O)(O)OCC2OC(n3cnc4c(ncnc43)N)C(O)C2O>>O=P(O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(OCC1OC(O)C(O)C1O)OP(=O)(O)OCC2OC(n3cnc4c(ncnc43)N)C(O)C2O>>O=P(O)(O)OCC1OC(O)C(O)C1O, O=P(O)(OCC1OC(O)C(O)C1O)OP(=O)(O)OCC2OC(n3cnc4c(ncnc43)N)C(O)C2O>>O=P(O)(O)OCC1OC(O)C(O)C1O]
3: R:M00002, P:M00004	[O>>O=P(O)(O)OCC1OC(O)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([O:4][CH2:5][CH:6]1[O:7][CH:8]([OH:9])[CH:10]([OH:11])[CH:12]1[OH:13])[O:14][P:15](=[O:16])([OH:17])[O:18][CH2:19][CH:20]2[O:21][CH:22]([N:23]:3:[CH:24]:[N:25]:[C:26]:4:[C:27](:[N:28]:[CH:29]:[N:30]:[C:31]43)[NH2:32])[CH:33]([OH:34])[CH:35]2[OH:36].[OH2:37]>>[O:1]=[P:2]([OH:37])([OH:3])[O:4][CH2:5][CH:6]1[O:7][CH:8]([OH:9])[CH:10]([OH:11])[CH:12]1[OH:13].[O:16]=[P:15]([OH:14])([OH:17])[O:18][CH2:19][CH:20]1[O:21][CH:22]([N:23]:2:[CH:24]:[N:25]:[C:26]:3:[C:27](:[N:28]:[CH:29]:[N:30]:[C:31]32)[NH2:32])[CH:33]([OH:34])[CH:35]1[OH:36]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=29, 2=30, 3=31, 4=26, 5=27, 6=28, 7=32, 8=25, 9=24, 10=23, 11=22, 12=33, 13=35, 14=20, 15=21, 16=19, 17=18, 18=15, 19=16, 20=17, 21=14, 22=2, 23=1, 24=3, 25=4, 26=5, 27=6, 28=12, 29=10, 30=8, 31=7, 32=9, 33=11, 34=13, 35=36, 36=34, 37=37}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=16, 2=17, 3=18, 4=13, 5=14, 6=15, 7=19, 8=12, 9=11, 10=10, 11=9, 12=20, 13=22, 14=7, 15=8, 16=6, 17=5, 18=2, 19=1, 20=3, 21=4, 22=23, 23=21, 24=29, 25=30, 26=36, 27=34, 28=32, 29=31, 30=33, 31=35, 32=37, 33=28, 34=25, 35=24, 36=26, 37=27}

