
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O:3.0, O=P(O)(O)O:3.0, O=P(O)(O)OC1O[CH][CH]C1O>>OC1O[CH][CH]C1O:1.0, O=P(O)(O)O[CH]:1.0, O=P(O)(O)O[CH]>>O=P(O)(O)O:1.0, O>>O=P(O)(O)O:2.0, O>>[P]O:1.0, [CH]O:1.0, [OH]:2.0, [O]:1.0, [O]C([CH])O:1.0, [O]C([CH])OP(=O)(O)O:1.0, [O]C([CH])OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]C([CH])OP(=O)(O)O>>[O]C([CH])O:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH]:1.0, [P]O[CH]>>[CH]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=P(O)(O)O:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=P(O)(O)O:2.0, O=P(O)(O)O[CH]:1.0, [O]C([CH])OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O>>[P]O
2: [P]O[CH]>>[CH]O
3: [O]P(=O)(O)O>>O=P(O)(O)O

MMP Level 2
1: O>>O=P(O)(O)O
2: [O]C([CH])OP(=O)(O)O>>[O]C([CH])O
3: O=P(O)(O)O[CH]>>O=P(O)(O)O

MMP Level 3
1: O>>O=P(O)(O)O
2: O=P(O)(O)OC1O[CH][CH]C1O>>OC1O[CH][CH]C1O
3: [O]C([CH])OP(=O)(O)O>>O=P(O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OC1OC(COP(=O)(O)OP(=O)(O)OCC2OC(n3cnc4c(ncnc43)N)C(O)C2O)C(O)C1O>>O=P(O)(OCC1OC(O)C(O)C1O)OP(=O)(O)OCC2OC(n3cnc4c(ncnc43)N)C(O)C2O]
2: R:M00001, P:M00004	[O=P(O)(O)OC1OC(COP(=O)(O)OP(=O)(O)OCC2OC(n3cnc4c(ncnc43)N)C(O)C2O)C(O)C1O>>O=P(O)(O)O]
3: R:M00002, P:M00004	[O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH:6]1[O:7][CH:8]([CH2:9][O:10][P:11](=[O:12])([OH:13])[O:14][P:15](=[O:16])([OH:17])[O:18][CH2:19][CH:20]2[O:21][CH:22]([N:23]:3:[CH:24]:[N:25]:[C:26]:4:[C:27](:[N:28]:[CH:29]:[N:30]:[C:31]43)[NH2:32])[CH:33]([OH:34])[CH:35]2[OH:36])[CH:37]([OH:38])[CH:39]1[OH:40].[OH2:41]>>[O:1]=[P:2]([OH:4])([OH:41])[OH:3].[O:12]=[P:11]([OH:13])([O:10][CH2:9][CH:8]1[O:7][CH:6]([OH:5])[CH:39]([OH:40])[CH:37]1[OH:38])[O:14][P:15](=[O:16])([OH:17])[O:18][CH2:19][CH:20]2[O:21][CH:22]([N:23]:3:[CH:24]:[N:25]:[C:26]:4:[C:27](:[N:28]:[CH:29]:[N:30]:[C:31]43)[NH2:32])[CH:33]([OH:34])[CH:35]2[OH:36]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=29, 2=30, 3=31, 4=26, 5=27, 6=28, 7=32, 8=25, 9=24, 10=23, 11=22, 12=33, 13=35, 14=20, 15=21, 16=19, 17=18, 18=15, 19=16, 20=17, 21=14, 22=11, 23=12, 24=13, 25=10, 26=9, 27=8, 28=37, 29=39, 30=6, 31=7, 32=5, 33=2, 34=1, 35=3, 36=4, 37=40, 38=38, 39=36, 40=34, 41=41}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=29, 2=30, 3=31, 4=26, 5=27, 6=28, 7=32, 8=25, 9=24, 10=23, 11=22, 12=33, 13=35, 14=20, 15=21, 16=19, 17=18, 18=15, 19=16, 20=17, 21=14, 22=2, 23=1, 24=3, 25=4, 26=5, 27=6, 28=12, 29=10, 30=8, 31=7, 32=9, 33=11, 34=13, 35=36, 36=34, 37=39, 38=38, 39=37, 40=40, 41=41}

