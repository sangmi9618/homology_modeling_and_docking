
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)OC[CH]:1.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O[CH2]:1.0, O[CH2]:1.0, O[CH2]>>[P]O[CH2]:1.0, [CH]CO:1.0, [CH]CO>>O=P(O)(O)OC[CH]:1.0, [OH]:2.0, [O]:2.0, [O]C([CH])CO>>[O]C([CH])COP(=O)(O)O:1.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC[CH]:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[O]P(=O)(O)O:2.0, [P]O[CH2]:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)OC[CH]:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[P]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[P]>>[P]O
2: O[CH2]>>[P]O[CH2]
3: [O]P(=O)(O)O>>[O]P(=O)(O)O

MMP Level 2
1: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
2: [CH]CO>>O=P(O)(O)OC[CH]
3: O=P(O)(O)O[P]>>O=P(O)(O)O[CH2]

MMP Level 3
1: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
2: [O]C([CH])CO>>[O]C([CH])COP(=O)(O)O
3: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1[nH]c(=O)n(cc1C)C2OC(CO)C(O)C2>>O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)O)C(O)C2]
2: R:M00002, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)O)C(O)C2]
3: R:M00002, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]


//
SELECTED AAM MAPPING
[O:32]=[C:33]1[NH:34][C:35](=[O:36])[N:37]([CH:38]=[C:39]1[CH3:40])[CH:41]2[O:42][CH:43]([CH2:44][OH:45])[CH:46]([OH:47])[CH2:48]2.[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:32]=[C:33]1[NH:34][C:35](=[O:36])[N:37]([CH:38]=[C:39]1[CH3:40])[CH:41]2[O:42][CH:43]([CH2:44][O:45][P:2](=[O:1])([OH:3])[OH:4])[CH:46]([OH:47])[CH2:48]2.[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=40, 2=39, 3=38, 4=37, 5=35, 6=36, 7=34, 8=33, 9=32, 10=41, 11=48, 12=46, 13=43, 14=42, 15=44, 16=45, 17=47, 18=24, 19=25, 20=26, 21=21, 22=22, 23=23, 24=27, 25=20, 26=19, 27=18, 28=17, 29=28, 30=30, 31=15, 32=16, 33=14, 34=13, 35=10, 36=11, 37=12, 38=9, 39=6, 40=7, 41=8, 42=5, 43=2, 44=1, 45=3, 46=4, 47=31, 48=29}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=36, 2=35, 3=34, 4=33, 5=31, 6=32, 7=30, 8=29, 9=28, 10=37, 11=48, 12=46, 13=39, 14=38, 15=40, 16=41, 17=42, 18=43, 19=44, 20=45, 21=47, 22=20, 23=21, 24=22, 25=17, 26=18, 27=19, 28=23, 29=16, 30=15, 31=14, 32=13, 33=24, 34=26, 35=11, 36=12, 37=10, 38=9, 39=6, 40=7, 41=8, 42=5, 43=2, 44=1, 45=3, 46=4, 47=27, 48=25}

