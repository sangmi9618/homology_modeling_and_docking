
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0, O-P:2.0]

ORDER_CHANGED
[C-C*C=C:1.0, C-O*C=O:2.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)C(=O)C[CH]:1.0, O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(=O)CC([CH])O:1.0, O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(=O)C[CH]:1.0, O=C(O)C(O[P])=C:1.0, O=C(O)C(O[P])=C>>O=C(O)C(=O)CC(O)C([CH])O:1.0, O=C(O)C(O[P])=C>>O=C(O)C(=O)C[CH]:1.0, O=CC(O)C(O)C(O)[CH2]>>[C]CC(O)C(O)C(O)C(O)[CH2]:1.0, O=CC(O)C([CH])O:1.0, O=CC(O)C([CH])O>>[CH]C(O)C(O)C(O)[CH2]:1.0, O=CC(O)C([CH])O>>[C]C(=O)CC(O)C(O)C([CH])O:1.0, O=CC([CH])O:1.0, O=CC([CH])O>>[C]CC(O)C([CH])O:2.0, O=C[CH]:2.0, O=C[CH]>>[CH]C(O)[CH2]:2.0, O=P(O)(O)O:3.0, O=[CH]:1.0, O=[CH]>>[CH]O:1.0, O>>O=P(O)(O)O:2.0, O>>[P]O:1.0, [CH2]:2.0, [CH]:4.0, [CH]C(O)C(O)C(O)[CH2]:1.0, [CH]C(O)[CH2]:2.0, [CH]C([CH])O:2.0, [CH]C([CH])O>>[CH]C([CH])O:1.0, [CH]O:1.0, [C]:2.0, [C]=C:1.0, [C]=C>>[C]C[CH]:1.0, [C]=O:1.0, [C]C(=O)CC([CH])O:1.0, [C]C(=O)[CH2]:2.0, [C]C(OP(=O)(O)O)=C:1.0, [C]C(OP(=O)(O)O)=C>>O=P(O)(O)O:1.0, [C]C(OP(=O)(O)O)=C>>[C]C(=O)[CH2]:1.0, [C]C([O])=C:2.0, [C]C([O])=C>>[C]C(=O)CC([CH])O:1.0, [C]C([O])=C>>[C]C(=O)[CH2]:1.0, [C]CC(O)C([CH])O:1.0, [C]C[CH]:1.0, [C]OP(=O)(O)O:1.0, [C]OP(=O)(O)O>>O=P(O)(O)O:1.0, [C]O[P]:1.0, [C]O[P]>>[C]=O:1.0, [OH]:2.0, [O]:3.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [P]:2.0, [P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (6)
[O:1.0, [CH2]:1.0, [CH]:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (7)
[O:1.0, O=P(O)(O)O:1.0, [CH]C(O)[CH2]:1.0, [C]C[CH]:1.0, [C]O[P]:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0]


ID=Reaction Center at Level: 2 (6)
[O:1.0, O=P(O)(O)O:2.0, [C]C(=O)CC([CH])O:1.0, [C]C(OP(=O)(O)O)=C:1.0, [C]CC(O)C([CH])O:1.0, [C]OP(=O)(O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (5)
[[CH2]:2.0, [CH]:2.0, [C]:2.0, [OH]:1.0, [O]:3.0]


ID=Reaction Center at Level: 1 (10)
[O=C[CH]:1.0, O=[CH]:1.0, [CH]C(O)[CH2]:1.0, [CH]O:1.0, [C]=C:1.0, [C]=O:1.0, [C]C(=O)[CH2]:1.0, [C]C([O])=C:1.0, [C]C[CH]:1.0, [C]O[P]:1.0]


ID=Reaction Center at Level: 2 (10)
[O=C(O)C(=O)C[CH]:1.0, O=C(O)C(O[P])=C:1.0, O=CC([CH])O:1.0, O=C[CH]:1.0, [CH]C(O)[CH2]:1.0, [C]C(=O)CC([CH])O:1.0, [C]C(=O)[CH2]:1.0, [C]C(OP(=O)(O)O)=C:1.0, [C]C([O])=C:1.0, [C]CC(O)C([CH])O:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:4.0]


ID=Reaction Center at Level: 1 (3)
[O=C[CH]:1.0, [CH]C(O)[CH2]:1.0, [CH]C([CH])O:2.0]


ID=Reaction Center at Level: 2 (4)
[O=CC(O)C([CH])O:1.0, O=CC([CH])O:1.0, [CH]C(O)C(O)C(O)[CH2]:1.0, [C]CC(O)C([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]O[P]>>[C]=O
2: O>>[P]O
3: O=C[CH]>>[CH]C(O)[CH2]
4: O=[CH]>>[CH]O
5: [C]=C>>[C]C[CH]
6: [C]C([O])=C>>[C]C(=O)[CH2]
7: [O]P(=O)(O)O>>O=P(O)(O)O
8: [CH]C([CH])O>>[CH]C([CH])O

MMP Level 2
1: [C]C(OP(=O)(O)O)=C>>[C]C(=O)[CH2]
2: O>>O=P(O)(O)O
3: O=CC([CH])O>>[C]CC(O)C([CH])O
4: O=C[CH]>>[CH]C(O)[CH2]
5: [C]C([O])=C>>[C]C(=O)CC([CH])O
6: O=C(O)C(O[P])=C>>O=C(O)C(=O)C[CH]
7: [C]OP(=O)(O)O>>O=P(O)(O)O
8: O=CC(O)C([CH])O>>[CH]C(O)C(O)C(O)[CH2]

MMP Level 3
1: O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(=O)C[CH]
2: O>>O=P(O)(O)O
3: O=CC(O)C([CH])O>>[C]C(=O)CC(O)C(O)C([CH])O
4: O=CC([CH])O>>[C]CC(O)C([CH])O
5: O=C(O)C(O[P])=C>>O=C(O)C(=O)CC(O)C([CH])O
6: O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(=O)CC([CH])O
7: [C]C(OP(=O)(O)O)=C>>O=P(O)(O)O
8: O=CC(O)C(O)C(O)[CH2]>>[C]CC(O)C(O)C(O)C(O)[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(=O)CC(O)C(O)C(O)C(O)COP(=O)(O)O, O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(=O)CC(O)C(O)C(O)C(O)COP(=O)(O)O, O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(=O)CC(O)C(O)C(O)C(O)COP(=O)(O)O]
2: R:M00001, P:M00005	[O=C(O)C(OP(=O)(O)O)=C>>O=P(O)(O)O]
3: R:M00002, P:M00004	[O=CC(O)C(O)C(O)COP(=O)(O)O>>O=C(O)C(=O)CC(O)C(O)C(O)C(O)COP(=O)(O)O, O=CC(O)C(O)C(O)COP(=O)(O)O>>O=C(O)C(=O)CC(O)C(O)C(O)C(O)COP(=O)(O)O, O=CC(O)C(O)C(O)COP(=O)(O)O>>O=C(O)C(=O)CC(O)C(O)C(O)C(O)COP(=O)(O)O]
4: R:M00003, P:M00005	[O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[CH:2][CH:3]([OH:4])[CH:5]([OH:6])[CH:7]([OH:8])[CH2:9][O:10][P:11](=[O:12])([OH:13])[OH:14].[O:15]=[C:16]([OH:17])[C:18]([O:19][P:20](=[O:21])([OH:22])[OH:23])=[CH2:24].[OH2:25]>>[O:15]=[C:16]([OH:17])[C:18](=[O:19])[CH2:24][CH:2]([OH:1])[CH:3]([OH:4])[CH:5]([OH:6])[CH:7]([OH:8])[CH2:9][O:10][P:11](=[O:12])([OH:13])[OH:14].[O:21]=[P:20]([OH:22])([OH:23])[OH:25]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=18, 3=16, 4=15, 5=17, 6=19, 7=20, 8=21, 9=22, 10=23, 11=9, 12=7, 13=5, 14=3, 15=2, 16=1, 17=4, 18=6, 19=8, 20=10, 21=11, 22=12, 23=13, 24=14, 25=25}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=9, 4=11, 5=13, 6=15, 7=16, 8=17, 9=18, 10=19, 11=20, 12=14, 13=12, 14=10, 15=8, 16=4, 17=5, 18=2, 19=1, 20=3, 21=23, 22=22, 23=21, 24=24, 25=25}

