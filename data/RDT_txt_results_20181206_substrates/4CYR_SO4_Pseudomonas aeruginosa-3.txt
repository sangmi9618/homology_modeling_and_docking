
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-S:2.0]

//
FINGERPRINTS RC
[O=S(=O)(O)OC(=C[CH])C=[CH]>>[CH]C=C(O)C=[CH]:1.0, O=S(=O)(O)OC([CH])=[CH]:1.0, O=S(=O)(O)OC([CH])=[CH]>>[CH]C(=[CH])O:1.0, O=S(=O)(O)OC([CH])=[CH]>>[O-]S([O-])(=O)=O:1.0, [CH]C(=[CH])O:1.0, [C]O:1.0, [C]OS(=O)(=O)O:1.0, [C]OS(=O)(=O)O>>[O-]S([O-])(=O)=O:2.0, [C]O[S]:1.0, [C]O[S]>>[C]O:1.0, [O-]:1.0, [O-]S([O-])(=O)=O:3.0, [O-][S]:1.0, [OH]:2.0, [O]:1.0, [O]S(=O)(=O)O:2.0, [O]S(=O)(=O)O>>[O-]S([O-])(=O)=O:2.0, [S]:2.0, [S]O:1.0, [S]O>>[O-][S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[O-]:1.0, [O]:1.0, [S]:2.0]


ID=Reaction Center at Level: 1 (4)
[[C]O[S]:1.0, [O-]S([O-])(=O)=O:1.0, [O-][S]:1.0, [O]S(=O)(=O)O:1.0]


ID=Reaction Center at Level: 2 (3)
[O=S(=O)(O)OC([CH])=[CH]:1.0, [C]OS(=O)(=O)O:1.0, [O-]S([O-])(=O)=O:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]S(=O)(=O)O>>[O-]S([O-])(=O)=O
2: [S]O>>[O-][S]
3: [C]O[S]>>[C]O

MMP Level 2
1: [C]OS(=O)(=O)O>>[O-]S([O-])(=O)=O
2: [O]S(=O)(=O)O>>[O-]S([O-])(=O)=O
3: O=S(=O)(O)OC([CH])=[CH]>>[CH]C(=[CH])O

MMP Level 3
1: O=S(=O)(O)OC([CH])=[CH]>>[O-]S([O-])(=O)=O
2: [C]OS(=O)(=O)O>>[O-]S([O-])(=O)=O
3: O=S(=O)(O)OC(=C[CH])C=[CH]>>[CH]C=C(O)C=[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[[O-][N+](=O)c1ccc(OS(=O)(=O)O)cc1>>[O-][N+](=O)c1ccc(O)cc1]
2: R:M00001, P:M00004	[[O-][N+](=O)c1ccc(OS(=O)(=O)O)cc1>>[O-]S([O-])(=O)=O, [O-][N+](=O)c1ccc(OS(=O)(=O)O)cc1>>[O-]S([O-])(=O)=O]


//
SELECTED AAM MAPPING
[O:1]=[N+:2]([O-:3])[C:4]:1:[CH:5]:[CH:6]:[C:7]([O:8][S:9](=[O:10])(=[O:11])[OH:12]):[CH:13]:[CH:14]1.[OH2:15]>>[O:1]=[N+:2]([O-:3])[C:4]:1:[CH:5]:[CH:6]:[C:7]([OH:8]):[CH:13]:[CH:14]1.[O:10]=[S:9](=[O:11])([O-:12])[O-:15]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=5, 2=6, 3=7, 4=13, 5=14, 6=4, 7=2, 8=1, 9=3, 10=8, 11=9, 12=10, 13=11, 14=12, 15=15}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=5, 2=6, 3=7, 4=9, 5=10, 6=4, 7=2, 8=1, 9=3, 10=8, 11=14, 12=12, 13=11, 14=13, 15=15}

