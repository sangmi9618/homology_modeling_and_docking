
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O=P(O)(O)O[P]>>O=P(O)(O[P])OC1O[CH][CH]C1O:1.0, [CH]:2.0, [C]C1=[C]N=CN1:1.0, [C]C1=[C][N]C=N1:1.0, [C]C1=[C][N]C=N1>>[C]C1=[C]N=CN1:1.0, [C]N([CH])C1O[CH][CH]C1O:1.0, [C]N([CH])C1O[CH][CH]C1O>>[P]OC1O[CH][CH]C1O:1.0, [C]N([CH])[CH]:1.0, [C]N([CH])[CH]>>[C]N=[CH]:1.0, [C]N=[CH]:2.0, [C]N=[CH]>>[C]N[CH]:1.0, [C]N[CH]:1.0, [C]Nc1c([C])ncn1C2O[CH][CH]C2O>>[C]Nc1nc[nH]c1[C]:1.0, [NH]:1.0, [NH]C1=[C]N=CN1C2OC([CH2])C(O)C2O>>[O]P(=O)(O)OC1OC([CH2])C(O)C1O:1.0, [NH]C1=[C]NC=N1:1.0, [N]:3.0, [N]C(=O)c1ncn([CH])c1[NH]>>[N]C(=O)c1[nH]cnc1[NH]:1.0, [N]C([O])[CH]:1.0, [N]C([O])[CH]>>[O]C([O])[CH]:1.0, [OH]:1.0, [O]:1.0, [O]C([CH])N1C=N[C]=C1[NH]:1.0, [O]C([CH])N1C=N[C]=C1[NH]>>[NH]C1=[C]NC=N1:1.0, [O]C([CH])OP([O])(=O)O:1.0, [O]C([O])[CH]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>[O]C([CH])OP([O])(=O)O:1.0, [P]O:1.0, [P]O>>[P]O[CH]:1.0, [P]OC1O[CH][CH]C1O:1.0, [P]O[CH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [N]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[C]N([CH])[CH]:1.0, [N]C([O])[CH]:1.0, [O]C([O])[CH]:1.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]N([CH])C1O[CH][CH]C1O:1.0, [O]C([CH])N1C=N[C]=C1[NH]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [P]OC1O[CH][CH]C1O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [N]C([O])[CH]>>[O]C([O])[CH]
2: [C]N([CH])[CH]>>[C]N=[CH]
3: [C]N=[CH]>>[C]N[CH]
4: [P]O>>[P]O[CH]

MMP Level 2
1: [C]N([CH])C1O[CH][CH]C1O>>[P]OC1O[CH][CH]C1O
2: [O]C([CH])N1C=N[C]=C1[NH]>>[NH]C1=[C]NC=N1
3: [C]C1=[C][N]C=N1>>[C]C1=[C]N=CN1
4: [O]P(=O)(O)O>>[O]C([CH])OP([O])(=O)O

MMP Level 3
1: [NH]C1=[C]N=CN1C2OC([CH2])C(O)C2O>>[O]P(=O)(O)OC1OC([CH2])C(O)C1O
2: [C]Nc1c([C])ncn1C2O[CH][CH]C2O>>[C]Nc1nc[nH]c1[C]
3: [N]C(=O)c1ncn([CH])c1[NH]>>[N]C(=O)c1[nH]cnc1[NH]
4: O=P(O)(O)O[P]>>O=P(O)(O[P])OC1O[CH][CH]C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O>>O=c1nc(N)[nH]c2nc[nH]c12, O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O>>O=c1nc(N)[nH]c2nc[nH]c12]
2: R:M00001, P:M00004	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O>>O=P(O)(O)OCC1OC(OP(=O)(O)OP(=O)(O)O)C(O)C1O]
3: R:M00002, P:M00004	[O=P(O)(O)OP(=O)(O)O>>O=P(O)(O)OCC1OC(OP(=O)(O)OP(=O)(O)O)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[C:8]1:[N:9]:[CH:10]:[N:11]2[CH:12]3[O:13][CH:14]([CH2:15][O:16][P:17](=[O:18])([OH:19])[OH:20])[CH:21]([OH:22])[CH:23]3[OH:24].[O:25]=[P:26]([OH:27])([OH:28])[O:29][P:30](=[O:31])([OH:32])[OH:33]>>[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[N:11]:[CH:10]:[NH:9]:[C:8]12.[O:18]=[P:17]([OH:20])([OH:19])[O:16][CH2:15][CH:14]1[O:13][CH:12]([O:27][P:26](=[O:25])([OH:28])[O:29][P:30](=[O:31])([OH:32])[OH:33])[CH:23]([OH:24])[CH:21]1[OH:22]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=10, 2=9, 3=8, 4=7, 5=11, 6=12, 7=23, 8=21, 9=14, 10=13, 11=15, 12=16, 13=17, 14=18, 15=19, 16=20, 17=22, 18=24, 19=6, 20=4, 21=3, 22=2, 23=1, 24=5, 25=27, 26=26, 27=25, 28=28, 29=29, 30=30, 31=31, 32=32, 33=33}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=31, 2=30, 3=29, 4=33, 5=32, 6=24, 7=23, 8=25, 9=26, 10=28, 11=27, 12=6, 13=7, 14=21, 15=19, 16=9, 17=8, 18=10, 19=11, 20=12, 21=13, 22=14, 23=15, 24=16, 25=17, 26=18, 27=20, 28=22, 29=5, 30=2, 31=1, 32=3, 33=4}

