
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0, C=O:1.0]

ORDER_CHANGED
[C-C*C=C:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)C(=O)C:1.0, O=C(O)C(OC([CH])[CH])=C>>O=C(O)C(=O)C:1.0, O=C(O)C(O[CH])=C:1.0, O=C(O)C(O[CH])=C>>O=C(O)C(=O)C:2.0, O>>O=C(O)C(=O)C:1.0, O>>[C]=O:1.0, O>>[C]C(=O)C:1.0, [CH2]:1.0, [CH3]:1.0, [CH]C([CH])O:1.0, [CH]O:1.0, [C]:2.0, [C]=C:1.0, [C]=C>>[C]C:1.0, [C]=O:1.0, [C]C:1.0, [C]C(=O)C:3.0, [C]C(O)C(OC(=C)C(=O)O)C=[CH]>>[C]C(O)C(O)C=[CH]:1.0, [C]C(OC([CH])[CH])=C:1.0, [C]C(OC([CH])[CH])=C>>[CH]C([CH])O:1.0, [C]C([O])=C:2.0, [C]C([O])=C>>[C]C(=O)C:2.0, [C]O[CH]:1.0, [C]O[CH]>>[CH]O:1.0, [OH]:1.0, [O]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[O:1.0, [C]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, [C]=O:1.0, [C]C(=O)C:1.0, [C]C([O])=C:1.0, [C]O[CH]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=C(O)C(=O)C:1.0, O=C(O)C(O[CH])=C:1.0, [C]C(=O)C:1.0, [C]C(OC([CH])[CH])=C:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[CH2]:1.0, [CH3]:1.0, [C]:2.0]


ID=Reaction Center at Level: 1 (4)
[[C]=C:1.0, [C]C:1.0, [C]C(=O)C:1.0, [C]C([O])=C:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(O)C(=O)C:1.0, O=C(O)C(O[CH])=C:1.0, [C]C(=O)C:1.0, [C]C([O])=C:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O>>[C]=O
2: [C]O[CH]>>[CH]O
3: [C]C([O])=C>>[C]C(=O)C
4: [C]=C>>[C]C

MMP Level 2
1: O>>[C]C(=O)C
2: [C]C(OC([CH])[CH])=C>>[CH]C([CH])O
3: O=C(O)C(O[CH])=C>>O=C(O)C(=O)C
4: [C]C([O])=C>>[C]C(=O)C

MMP Level 3
1: O>>O=C(O)C(=O)C
2: [C]C(O)C(OC(=C)C(=O)O)C=[CH]>>[C]C(O)C(O)C=[CH]
3: O=C(O)C(OC([CH])[CH])=C>>O=C(O)C(=O)C
4: O=C(O)C(O[CH])=C>>O=C(O)C(=O)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O>>O=C(O)C(=O)C]
2: R:M00002, P:M00003	[O=C(O)C1=CC=CC(OC(=C)C(=O)O)C1O>>O=C(O)C(=O)C, O=C(O)C1=CC=CC(OC(=C)C(=O)O)C1O>>O=C(O)C(=O)C]
3: R:M00002, P:M00004	[O=C(O)C1=CC=CC(OC(=C)C(=O)O)C1O>>O=C(O)C1=CC=CC(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[C:4]([O:5][CH:6]1[CH:7]=[CH:8][CH:9]=[C:10]([C:11](=[O:12])[OH:13])[CH:14]1[OH:15])=[CH2:16].[OH2:17]>>[O:1]=[C:2]([OH:3])[C:4](=[O:17])[CH3:16].[O:12]=[C:11]([OH:13])[C:10]1=[CH:9][CH:8]=[CH:7][CH:6]([OH:5])[CH:14]1[OH:15]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=17, 2=16, 3=4, 4=2, 5=1, 6=3, 7=5, 8=6, 9=7, 10=8, 11=9, 12=10, 13=14, 14=15, 15=11, 16=12, 17=13}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=17, 2=15, 3=16, 4=13, 5=12, 6=14, 7=6, 8=7, 9=8, 10=10, 11=4, 12=5, 13=2, 14=1, 15=3, 16=11, 17=9}

