
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%C:1.0, C%N:1.0, C=O:1.0, C@C:1.0, C@N:2.0]

//
FINGERPRINTS RC
[N:3.0, O:3.0, O>>[C]=C([NH])C(=O)N=[CH]:1.0, O>>[C]=O:1.0, O>>[C]C([N])=O:1.0, [CH]:2.0, [C]:6.0, [C]1=NC=Nc2nc[nH]c12>>N:1.0, [C]=C([NH])C(=N[CH])N:1.0, [C]=C([NH])C(=N[CH])N>>[C]1N=CNc2nc[nH]c12:1.0, [C]=C([NH])C(=N[CH])N>>[C]N=CN[C]:1.0, [C]=C([NH])C(=O)N=[CH]:1.0, [C]=C([N])N=C[N]:1.0, [C]=C([N])N=C[N]>>N:1.0, [C]=C([N])NC=[N]:1.0, [C]=C([N])[NH]:1.0, [C]=C([N])[N]:1.0, [C]=C([N])[N]>>[C]=C([N])[NH]:1.0, [C]=O:1.0, [C]C(=[C])[NH]:2.0, [C]C(=[C])[NH]>>[C]C(=[C])[NH]:1.0, [C]C(=[N])N:2.0, [C]C(=[N])N>>[C]=C([N])NC=[N]:1.0, [C]C(=[N])N>>[N]=C[NH]:1.0, [C]C([N])=O:2.0, [C]N:1.0, [C]N=CN=[C]:1.0, [C]N=CN=[C]>>[C]=C([NH])C(=O)N=[CH]:1.0, [C]N=CN[C]:1.0, [C]N=[CH]:1.0, [C]N=[CH]>>N:1.0, [C]N>>[C]N[CH]:1.0, [C]N[CH]:1.0, [C]c1[nH]cnc1N=[CH]:1.0, [C]c1[nH]cnc1N=[CH]>>[C]c1[nH]cnc1N[CH]:1.0, [C]c1[nH]cnc1N[CH]:1.0, [NH2]:1.0, [NH]:1.0, [N]:1.0, [N]=C[NH]:1.0, [N]C(=O)c1[nH]cnc1[NH]:1.0, [N]C=1[C]=C(N=CN1)N>>O=c1nc[nH]c2nc[nH]c12:1.0, [N]C=[N]:1.0, [N]C=[N]>>[C]C([N])=O:1.0, [N]c1nc[nH]c1C(=[N])N:1.0, [N]c1nc[nH]c1C(=[N])N>>[N]C(=O)c1[nH]cnc1[NH]:1.0, [O]:1.0, n1cnc(N)c2[nH]cnc12>>O=c1nc[nH]c2nc[nH]c12:2.0, n1cnc(N)c2[nH]cnc12>>[N]C1=[C]C(=O)N=CN1:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (7)
[N:1.0, O:1.0, [CH]:1.0, [C]:7.0, [NH]:1.0, [N]:2.0, [O]:1.0]


ID=Reaction Center at Level: 1 (11)
[N:1.0, O:1.0, [C]=C([N])[NH]:1.0, [C]=C([N])[N]:1.0, [C]=O:1.0, [C]C(=[C])[NH]:2.0, [C]C(=[N])N:1.0, [C]C([N])=O:2.0, [C]N=[CH]:2.0, [C]N[CH]:1.0, [N]C=[N]:1.0]


ID=Reaction Center at Level: 2 (12)
[N:1.0, O:1.0, [C]=C([NH])C(=N[CH])N:1.0, [C]=C([NH])C(=O)N=[CH]:2.0, [C]=C([N])N=C[N]:2.0, [C]=C([N])NC=[N]:1.0, [C]C([N])=O:1.0, [C]N=CN=[C]:1.0, [C]c1[nH]cnc1N=[CH]:1.0, [C]c1[nH]cnc1N[CH]:1.0, [N]C(=O)c1[nH]cnc1[NH]:1.0, [N]c1nc[nH]c1C(=[N])N:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]N=[CH]>>N
2: [C]=C([N])[N]>>[C]=C([N])[NH]
3: O>>[C]=O
4: [C]C(=[C])[NH]>>[C]C(=[C])[NH]
5: [C]N>>[C]N[CH]
6: [N]C=[N]>>[C]C([N])=O
7: [C]C(=[N])N>>[N]=C[NH]

MMP Level 2
1: [C]=C([N])N=C[N]>>N
2: [C]c1[nH]cnc1N=[CH]>>[C]c1[nH]cnc1N[CH]
3: O>>[C]C([N])=O
4: [N]c1nc[nH]c1C(=[N])N>>[N]C(=O)c1[nH]cnc1[NH]
5: [C]C(=[N])N>>[C]=C([N])NC=[N]
6: [C]N=CN=[C]>>[C]=C([NH])C(=O)N=[CH]
7: [C]=C([NH])C(=N[CH])N>>[C]N=CN[C]

MMP Level 3
1: [C]1=NC=Nc2nc[nH]c12>>N
2: n1cnc(N)c2[nH]cnc12>>O=c1nc[nH]c2nc[nH]c12
3: O>>[C]=C([NH])C(=O)N=[CH]
4: n1cnc(N)c2[nH]cnc12>>O=c1nc[nH]c2nc[nH]c12
5: [C]=C([NH])C(=N[CH])N>>[C]1N=CNc2nc[nH]c12
6: [N]C=1[C]=C(N=CN1)N>>O=c1nc[nH]c2nc[nH]c12
7: n1cnc(N)c2[nH]cnc12>>[N]C1=[C]C(=O)N=CN1


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[n1cnc(N)c2[nH]cnc12>>O=c1nc[nH]c2nc[nH]c12, n1cnc(N)c2[nH]cnc12>>O=c1nc[nH]c2nc[nH]c12, n1cnc(N)c2[nH]cnc12>>O=c1nc[nH]c2nc[nH]c12, n1cnc(N)c2[nH]cnc12>>O=c1nc[nH]c2nc[nH]c12, n1cnc(N)c2[nH]cnc12>>O=c1nc[nH]c2nc[nH]c12]
2: R:M00001, P:M00004	[n1cnc(N)c2[nH]cnc12>>N]
3: R:M00002, P:M00003	[O>>O=c1nc[nH]c2nc[nH]c12]


//
SELECTED AAM MAPPING
[OH2:11].[N:1]:1:[CH:2]:[N:3]:[C:4]([NH2:5]):[C:6]:2:[NH:7]:[CH:8]:[N:9]:[C:10]12>>[O:11]=[C:2]1[N:3]=[CH:4][NH:5][C:10]:2:[N:9]:[CH:8]:[NH:7]:[C:6]12.[NH3:1]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=8, 2=9, 3=10, 4=6, 5=7, 6=4, 7=3, 8=2, 9=1, 10=5, 11=11}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=8, 2=7, 3=6, 4=10, 5=9, 6=2, 7=1, 8=3, 9=4, 10=5, 11=11}

