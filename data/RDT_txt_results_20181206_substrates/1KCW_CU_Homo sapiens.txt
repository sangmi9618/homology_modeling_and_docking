
//
FINGERPRINTS BC

FORMED_CLEAVED
[O=O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=O:4.0, O=O>>O:3.0, [Fe+2]:3.0, [Fe+2]>>[Fe+3]:3.0, [Fe+3]:3.0, [O]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [Fe+2]:1.0, [Fe+3]:1.0, [O]:2.0]


ID=Reaction Center at Level: 1 (4)
[O:1.0, O=O:2.0, [Fe+2]:1.0, [Fe+3]:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=O:2.0, [Fe+2]:1.0, [Fe+3]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=O>>O
2: [Fe+2]>>[Fe+3]

MMP Level 2
1: O=O>>O
2: [Fe+2]>>[Fe+3]

MMP Level 3
1: O=O>>O
2: [Fe+2]>>[Fe+3]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[[Fe+2]>>[Fe+3]]
2: R:M00003, P:M00005	[O=O>>O]


//
SELECTED AAM MAPPING
[H+:4].[Fe+2:2].[O:1]=[O:3]>>[Fe+3:2].[OH2:1]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=3, 2=4, 3=1, 4=2}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=1, 2=2}

