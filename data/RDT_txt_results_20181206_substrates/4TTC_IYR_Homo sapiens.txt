
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-I:1.0]

//
FINGERPRINTS RC
[Ic1cc([CH2])ccc1O>>Oc1ccc([CH2])cc1:1.0, O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1:1.0, [CH2]:1.0, [CH]:2.0, [CH]N([CH])[CH]:1.0, [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]:1.0, [C]:1.0, [C]=C(I)[CH]:2.0, [C]=C(I)[CH]>>[C]=C[CH]:1.0, [C]=C(I)[CH]>>[I-]:1.0, [C]=CC(I)=C([CH])O:1.0, [C]=CC(I)=C([CH])O>>[C]=CC=C([CH])O:1.0, [C]=CC(I)=C([CH])O>>[I-]:1.0, [C]=CC=C([CH])O:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]:1.0, [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O:1.0, [C]C=[CH]:1.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C=[CH]:1.0, [C]I:1.0, [C]I>>[I-]:1.0, [I-]:3.0, [I]:1.0, [N+]:1.0, [N]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[C]:1.0, [I-]:1.0, [I]:1.0]


ID=Reaction Center at Level: 1 (3)
[[C]=C(I)[CH]:1.0, [C]I:1.0, [I-]:1.0]


ID=Reaction Center at Level: 2 (3)
[[C]=C(I)[CH]:1.0, [C]=CC(I)=C([CH])O:1.0, [I-]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]=C(I)[CH]>>[C]=C[CH]
2: [C]C[CH]>>[C]C=[CH]
3: [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]
4: [C]I>>[I-]

MMP Level 2
1: [C]=CC(I)=C([CH])O>>[C]=CC=C([CH])O
2: [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]
3: [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]
4: [C]=C(I)[CH]>>[I-]

MMP Level 3
1: Ic1cc([CH2])ccc1O>>Oc1ccc([CH2])cc1
2: O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1
3: [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O
4: [C]=CC(I)=C([CH])O>>[I-]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C(O)C(N)Cc1ccc(O)c(I)c1>>O=C(O)C(N)Cc1ccc(O)cc1]
2: R:M00001, P:M00005	[O=C(O)C(N)Cc1ccc(O)c(I)c1>>[I-]]
3: R:M00002, P:M00006	[O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O]


//
SELECTED AAM MAPPING
[H+:63].[O:49]=[C:50]([OH:51])[CH:52]([NH2:53])[CH2:54][C:55]:1:[CH:56]:[CH:57]:[C:58]([OH:59]):[C:60]([I:61]):[CH:62]1.[O:1]=[C:2]([NH2:3])[C:4]1=[CH:5][N:6]([CH:7]=[CH:8][CH2:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48]>>[I-:61].[O:49]=[C:50]([OH:51])[CH:52]([NH2:53])[CH2:54][C:55]:1:[CH:62]:[CH:60]:[C:58]([OH:59]):[CH:57]:[CH:56]1.[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:9]:[CH:8]:[CH:7]:[N+:6](:[CH:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=56, 2=57, 3=58, 4=60, 5=62, 6=55, 7=54, 8=52, 9=50, 10=49, 11=51, 12=53, 13=61, 14=59, 15=9, 16=8, 17=7, 18=6, 19=5, 20=4, 21=2, 22=1, 23=3, 24=10, 25=47, 26=45, 27=12, 28=11, 29=13, 30=14, 31=15, 32=16, 33=17, 34=18, 35=19, 36=20, 37=21, 38=22, 39=23, 40=24, 41=43, 42=37, 43=26, 44=25, 45=27, 46=28, 47=29, 48=30, 49=35, 50=34, 51=33, 52=32, 53=31, 54=36, 55=38, 56=39, 57=40, 58=41, 59=42, 60=44, 61=46, 62=48, 63=63}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=56, 2=57, 3=58, 4=60, 5=61, 6=55, 7=54, 8=52, 9=50, 10=49, 11=51, 12=53, 13=59, 14=62, 15=6, 16=5, 17=4, 18=9, 19=8, 20=7, 21=10, 22=47, 23=45, 24=12, 25=11, 26=13, 27=14, 28=15, 29=16, 30=17, 31=18, 32=19, 33=20, 34=21, 35=22, 36=23, 37=24, 38=43, 39=37, 40=26, 41=25, 42=27, 43=28, 44=29, 45=30, 46=35, 47=34, 48=33, 49=32, 50=31, 51=36, 52=38, 53=39, 54=40, 55=41, 56=42, 57=44, 58=46, 59=48, 60=2, 61=1, 62=3}

