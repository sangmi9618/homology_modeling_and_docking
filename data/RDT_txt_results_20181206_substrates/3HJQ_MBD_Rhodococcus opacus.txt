
//
FINGERPRINTS BC

FORMED_CLEAVED
[C=O:2.0, C@C:1.0, O=O:1.0]

ORDER_CHANGED
[C-C*C@C:3.0, C=C*C@C:2.0]

//
FINGERPRINTS RC
[O=C(O)C(=C[CH])C:1.0, O=C(O)C(=[CH])C:1.0, O=C(O)C=C[CH]:1.0, O=C(O)C=[CH]:1.0, O=C([CH])O:2.0, O=O:4.0, O=O>>O=C(O)C(=[CH])C:1.0, O=O>>O=C(O)C=[CH]:1.0, O=O>>O=C([CH])O:1.0, O=O>>[C]=O:2.0, O=O>>[C]C(=O)O:1.0, OC1=[C]C=CC=C1C>>[C]C=CC=C(C(=O)O)C:1.0, OC1=[C]C=CC=C1O>>[C]=CC=CC(=O)O:1.0, OC=1[C]=C(C=CC1)C>>[C]C(=CC=CC(=O)O)C:1.0, Oc1cccc(c1O)C>>O=C(O)C(=CC=[CH])C:1.0, Oc1cccc(c1O)C>>O=C(O)C(=C[CH])C:1.0, Oc1cccc(c1O)C>>O=C(O)C=C[CH]:1.0, [CH]:6.0, [CH]C(=C(O)C(=[CH])O)C:1.0, [CH]C(=C(O)C(=[CH])O)C>>O=C(O)C(=[CH])C:1.0, [CH]C=[CH]:2.0, [CH]C=[CH]>>[CH]C=[CH]:1.0, [C]:6.0, [C]=C(C=C[CH])C:1.0, [C]=C(C=C[CH])C>>[C]C(=CC=[CH])C:1.0, [C]=C(O)C(O)=C[CH]:1.0, [C]=C(O)C(O)=C[CH]>>O=C(O)C=[CH]:1.0, [C]=C([CH])C:1.0, [C]=C([CH])C>>[C]C(=[CH])C:1.0, [C]=C[CH]:2.0, [C]=C[CH]>>[C]C=[CH]:1.0, [C]=O:2.0, [C]C(=CC=[CH])C:1.0, [C]C(=O)O:2.0, [C]C(=[CH])C:1.0, [C]C(=[CH])O:1.0, [C]C(=[CH])O>>O=C([CH])O:1.0, [C]C(=[C])O:1.0, [C]C(=[C])O>>[C]C(=O)O:1.0, [C]C(O)=C(C=[CH])C:1.0, [C]C(O)=C(C=[CH])C>>O=C(O)C(=C[CH])C:1.0, [C]C(O)=CC=[CH]:1.0, [C]C(O)=CC=[CH]>>O=C(O)C=C[CH]:1.0, [C]C=CC=[C]:2.0, [C]C=CC=[C]>>[C]C=CC=[C]:1.0, [C]C=[CH]:2.0, [C]C=[CH]>>[C]=C[CH]:1.0, [O]:4.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[C]:4.0, [O]:4.0]


ID=Reaction Center at Level: 1 (6)
[O=C([CH])O:1.0, O=O:2.0, [C]=O:2.0, [C]C(=O)O:1.0, [C]C(=[CH])O:1.0, [C]C(=[C])O:1.0]


ID=Reaction Center at Level: 2 (7)
[O=C(O)C(=[CH])C:1.0, O=C(O)C=[CH]:1.0, O=C([CH])O:1.0, O=O:2.0, [CH]C(=C(O)C(=[CH])O)C:1.0, [C]=C(O)C(O)=C[CH]:1.0, [C]C(=O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (2)
[[CH]:6.0, [C]:6.0]


ID=Reaction Center at Level: 1 (9)
[O=C([CH])O:1.0, [CH]C=[CH]:2.0, [C]=C([CH])C:1.0, [C]=C[CH]:2.0, [C]C(=O)O:1.0, [C]C(=[CH])C:1.0, [C]C(=[CH])O:1.0, [C]C(=[C])O:1.0, [C]C=[CH]:2.0]


ID=Reaction Center at Level: 2 (11)
[O=C(O)C(=C[CH])C:1.0, O=C(O)C(=[CH])C:1.0, O=C(O)C=C[CH]:1.0, O=C(O)C=[CH]:1.0, [CH]C(=C(O)C(=[CH])O)C:1.0, [C]=C(C=C[CH])C:1.0, [C]=C(O)C(O)=C[CH]:1.0, [C]C(=CC=[CH])C:1.0, [C]C(O)=C(C=[CH])C:1.0, [C]C(O)=CC=[CH]:1.0, [C]C=CC=[C]:2.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]=C[CH]>>[C]C=[CH]
2: [C]C(=[CH])O>>O=C([CH])O
3: [CH]C=[CH]>>[CH]C=[CH]
4: O=O>>[C]=O
5: [C]=C([CH])C>>[C]C(=[CH])C
6: [C]C=[CH]>>[C]=C[CH]
7: [C]C(=[C])O>>[C]C(=O)O
8: O=O>>[C]=O

MMP Level 2
1: [C]C(O)=CC=[CH]>>O=C(O)C=C[CH]
2: [C]=C(O)C(O)=C[CH]>>O=C(O)C=[CH]
3: [C]C=CC=[C]>>[C]C=CC=[C]
4: O=O>>[C]C(=O)O
5: [C]C(O)=C(C=[CH])C>>O=C(O)C(=C[CH])C
6: [C]=C(C=C[CH])C>>[C]C(=CC=[CH])C
7: [CH]C(=C(O)C(=[CH])O)C>>O=C(O)C(=[CH])C
8: O=O>>O=C([CH])O

MMP Level 3
1: OC1=[C]C=CC=C1O>>[C]=CC=CC(=O)O
2: Oc1cccc(c1O)C>>O=C(O)C=C[CH]
3: OC=1[C]=C(C=CC1)C>>[C]C(=CC=CC(=O)O)C
4: O=O>>O=C(O)C(=[CH])C
5: Oc1cccc(c1O)C>>O=C(O)C(=CC=[CH])C
6: OC1=[C]C=CC=C1C>>[C]C=CC=C(C(=O)O)C
7: Oc1cccc(c1O)C>>O=C(O)C(=C[CH])C
8: O=O>>O=C(O)C=[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[Oc1cccc(c1O)C>>O=C(O)C=CC=C(C(=O)O)C, Oc1cccc(c1O)C>>O=C(O)C=CC=C(C(=O)O)C, Oc1cccc(c1O)C>>O=C(O)C=CC=C(C(=O)O)C, Oc1cccc(c1O)C>>O=C(O)C=CC=C(C(=O)O)C, Oc1cccc(c1O)C>>O=C(O)C=CC=C(C(=O)O)C, Oc1cccc(c1O)C>>O=C(O)C=CC=C(C(=O)O)C]
2: R:M00002, P:M00003	[O=O>>O=C(O)C=CC=C(C(=O)O)C, O=O>>O=C(O)C=CC=C(C(=O)O)C]


//
SELECTED AAM MAPPING
[O:10]=[O:11].[OH:1][C:2]:1:[CH:3]:[CH:4]:[CH:5]:[C:6](:[C:7]1[OH:8])[CH3:9]>>[O:10]=[C:2]([OH:1])[CH:3]=[CH:4][CH:5]=[C:6]([C:7](=[O:11])[OH:8])[CH3:9]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=6, 3=7, 4=2, 5=3, 6=4, 7=5, 8=1, 9=8, 10=10, 11=11}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=11, 2=7, 3=6, 4=5, 5=4, 6=2, 7=1, 8=3, 9=8, 10=9, 11=10}

