
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O:3.0, O>>OC1O[CH][CH]C1O:1.0, O>>[CH]O:1.0, O>>[O]C([CH])O:1.0, OC1O[CH][CH]C1O:1.0, [CH]:2.0, [CH]N=[CH]:1.0, [CH]O:1.0, [CH][N+](=[CH])C1O[CH][CH]C1O:1.0, [CH][N+](=[CH])C1O[CH][CH]C1O>>OC1O[CH][CH]C1O:1.0, [CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]>>[CH]N=[CH]:1.0, [C]=CN=C[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN=C[CH]:1.0, [C]=C[N+](=C[CH])C1OC([CH2])C(O)C1O>>OC1OC([CH2])C(O)C1O:1.0, [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]c1cnccc1:1.0, [N+]:1.0, [N+]C([O])[CH]:1.0, [N+]C([O])[CH]>>[O]C([CH])O:1.0, [N]:1.0, [OH]:1.0, [O]C([CH])O:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [CH]:2.0, [N+]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, [CH]O:1.0, [CH][N+]([CH])=[CH]:1.0, [N+]C([O])[CH]:1.0, [O]C([CH])O:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, OC1O[CH][CH]C1O:1.0, [CH][N+](=[CH])C1O[CH][CH]C1O:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [O]C([CH])O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH][N+]([CH])=[CH]>>[CH]N=[CH]
2: O>>[CH]O
3: [N+]C([O])[CH]>>[O]C([CH])O

MMP Level 2
1: [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN=C[CH]
2: O>>[O]C([CH])O
3: [CH][N+](=[CH])C1O[CH][CH]C1O>>OC1O[CH][CH]C1O

MMP Level 3
1: [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]c1cnccc1
2: O>>OC1O[CH][CH]C1O
3: [C]=C[N+](=C[CH])C1OC([CH2])C(O)C1O>>OC1OC([CH2])C(O)C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=P(O)(OCC1OC(O)C(O)C1O)OP(=O)(O)OCC2OC(n3cnc4c(ncnc43)N)C(O)C2O]
2: R:M00001, P:M00004	[O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)c1cnccc1]
3: R:M00002, P:M00003	[O>>O=P(O)(OCC1OC(O)C(O)C1O)OP(=O)(O)OCC2OC(n3cnc4c(ncnc43)N)C(O)C2O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:5]:[CH:6]:[CH:7]:[N+:8](:[CH:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44].[OH2:45]>>[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:9]:[N:8]:[CH:7]:[CH:6]:[CH:5]1.[O:16]=[P:15]([OH:17])([O:14][CH2:13][CH:12]1[O:11][CH:10]([OH:45])[CH:43]([OH:44])[CH:41]1[OH:42])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]2[O:25][CH:26]([N:27]:3:[CH:28]:[N:29]:[C:30]:4:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]43)[NH2:36])[CH:37]([OH:38])[CH:39]2[OH:40]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=5, 3=4, 4=9, 5=8, 6=7, 7=10, 8=43, 9=41, 10=12, 11=11, 12=13, 13=14, 14=15, 15=16, 16=17, 17=18, 18=19, 19=20, 20=21, 21=22, 22=23, 23=24, 24=39, 25=37, 26=26, 27=25, 28=27, 29=28, 30=29, 31=30, 32=35, 33=34, 34=33, 35=32, 36=31, 37=36, 38=38, 39=40, 40=42, 41=44, 42=2, 43=1, 44=3, 45=45}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=29, 2=30, 3=31, 4=26, 5=27, 6=28, 7=32, 8=25, 9=24, 10=23, 11=22, 12=33, 13=35, 14=20, 15=21, 16=19, 17=18, 18=15, 19=16, 20=17, 21=14, 22=2, 23=1, 24=3, 25=4, 26=5, 27=6, 28=12, 29=10, 30=8, 31=7, 32=9, 33=11, 34=13, 35=36, 36=34, 37=44, 38=45, 39=40, 40=41, 41=42, 42=43, 43=38, 44=37, 45=39}

