
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:2.0]

//
FINGERPRINTS RC
[O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(OC([CH])[CH2])=C:1.0, O=C(O)C(OP(=O)(O)O)=C>>O=P(O)(O)O:1.0, O=C(O)C(O[CH])=C:1.0, O=C(O)C(O[P])=C:1.0, O=C(O)C(O[P])=C>>O=C(O)C(O[CH])=C:1.0, O=P(O)(O)O:1.0, [CH]C(O)[CH2]:1.0, [CH]C(O)[CH2]>>[C]C(OC([CH])[CH2])=C:1.0, [CH]O:1.0, [CH]O>>[C]O[CH]:1.0, [C]:2.0, [C]C(OC([CH])[CH2])=C:1.0, [C]C(OP(=O)(O)O)=C:1.0, [C]C(OP(=O)(O)O)=C>>O=P(O)(O)O:1.0, [C]C([O])=C:2.0, [C]C([O])=C>>[C]C([O])=C:1.0, [C]CC(O)C([CH])O>>[C]CC(OC(=C)C(=O)O)C([CH])O:1.0, [C]O[CH]:1.0, [C]O[P]:1.0, [C]O[P]>>[P]O:1.0, [OH]:2.0, [O]:2.0, [P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[C]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (3)
[[C]C([O])=C:2.0, [C]O[CH]:1.0, [C]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(O)C(O[CH])=C:1.0, O=C(O)C(O[P])=C:1.0, [C]C(OC([CH])[CH2])=C:1.0, [C]C(OP(=O)(O)O)=C:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH]O>>[C]O[CH]
2: [C]O[P]>>[P]O
3: [C]C([O])=C>>[C]C([O])=C

MMP Level 2
1: [CH]C(O)[CH2]>>[C]C(OC([CH])[CH2])=C
2: [C]C(OP(=O)(O)O)=C>>O=P(O)(O)O
3: O=C(O)C(O[P])=C>>O=C(O)C(O[CH])=C

MMP Level 3
1: [C]CC(O)C([CH])O>>[C]CC(OC(=C)C(=O)O)C([CH])O
2: O=C(O)C(OP(=O)(O)O)=C>>O=P(O)(O)O
3: O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C(OC([CH])[CH2])=C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(OP(=O)(O)O)=C>>O=P(O)(O)O]
2: R:M00001, P:M00004	[O=C(O)C(OP(=O)(O)O)=C>>O=C(O)C1=CC(OP(=O)(O)O)C(O)C(OC(=C)C(=O)O)C1]
3: R:M00002, P:M00004	[O=C(O)C1=CC(OP(=O)(O)O)C(O)C(O)C1>>O=C(O)C1=CC(OP(=O)(O)O)C(O)C(OC(=C)C(=O)O)C1]


//
SELECTED AAM MAPPING
[O:17]=[C:18]([OH:19])[C:20]([O:21][P:22](=[O:23])([OH:24])[OH:25])=[CH2:26].[O:1]=[C:2]([OH:3])[C:4]1=[CH:5][CH:6]([O:7][P:8](=[O:9])([OH:10])[OH:11])[CH:12]([OH:13])[CH:14]([OH:15])[CH2:16]1>>[O:17]=[C:18]([OH:19])[C:20]([O:15][CH:14]1[CH2:16][C:4](=[CH:5][CH:6]([O:7][P:8](=[O:9])([OH:11])[OH:10])[CH:12]1[OH:13])[C:2](=[O:1])[OH:3])=[CH2:26].[O:23]=[P:22]([OH:21])([OH:24])[OH:25]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=26, 2=20, 3=18, 4=17, 5=19, 6=21, 7=22, 8=23, 9=24, 10=25, 11=16, 12=14, 13=12, 14=6, 15=5, 16=4, 17=2, 18=1, 19=3, 20=7, 21=8, 22=9, 23=10, 24=11, 25=13, 26=15}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=24, 2=23, 3=22, 4=25, 5=26, 6=21, 7=4, 8=2, 9=1, 10=3, 11=5, 12=6, 13=7, 14=8, 15=9, 16=10, 17=16, 18=17, 19=11, 20=12, 21=13, 22=14, 23=15, 24=18, 25=19, 26=20}

