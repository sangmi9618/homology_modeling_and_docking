
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(N[CH])C[CH2]:1.0, O=C(N[CH])C[CH2]>>O=C(O)C[CH2]:1.0, O=C(O)C[CH2]:1.0, O=C(O)[CH2]:2.0, O=C([NH])C(NC(=O)C[CH2])C[S]>>O=C([NH])C(N)C[S]:1.0, O=C([NH])[CH2]:1.0, O=C([NH])[CH2]>>O=C(O)[CH2]:1.0, O>>O=C(O)C[CH2]:1.0, O>>O=C(O)[CH2]:1.0, O>>[C]O:1.0, [CH]N:1.0, [C]:2.0, [C]C([CH2])N:1.0, [C]C([CH2])NC(=O)CC[CH]>>O=C(O)CC[CH]:1.0, [C]C([CH2])NC(=O)[CH2]:1.0, [C]C([CH2])NC(=O)[CH2]>>[C]C([CH2])N:1.0, [C]N[CH]:1.0, [C]N[CH]>>[CH]N:1.0, [C]O:1.0, [NH2]:1.0, [NH]:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [C]:2.0, [NH]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=C(O)[CH2]:1.0, O=C([NH])[CH2]:1.0, [C]N[CH]:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=C(N[CH])C[CH2]:1.0, O=C(O)C[CH2]:1.0, O=C(O)[CH2]:1.0, [C]C([CH2])NC(=O)[CH2]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]N[CH]>>[CH]N
2: O=C([NH])[CH2]>>O=C(O)[CH2]
3: O>>[C]O

MMP Level 2
1: [C]C([CH2])NC(=O)[CH2]>>[C]C([CH2])N
2: O=C(N[CH])C[CH2]>>O=C(O)C[CH2]
3: O>>O=C(O)[CH2]

MMP Level 3
1: O=C([NH])C(NC(=O)C[CH2])C[S]>>O=C([NH])C(N)C[S]
2: [C]C([CH2])NC(=O)CC[CH]>>O=C(O)CC[CH]
3: O>>O=C(O)C[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)CNC(=O)C(NC(=O)CCC(N)C(=O)O)CSC>>O=C(O)CNC(=O)C(N)CSC]
2: R:M00001, P:M00004	[O=C(O)CNC(=O)C(NC(=O)CCC(N)C(=O)O)CSC>>O=C(O)CCC(N)C(=O)O]
3: R:M00002, P:M00004	[O>>O=C(O)CCC(N)C(=O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH2:4][NH:5][C:6](=[O:7])[CH:8]([NH:9][C:10](=[O:11])[CH2:12][CH2:13][CH:14]([NH2:15])[C:16](=[O:17])[OH:18])[CH2:19][S:20][CH3:21].[OH2:22]>>[O:1]=[C:2]([OH:3])[CH2:4][NH:5][C:6](=[O:7])[CH:8]([NH2:9])[CH2:19][S:20][CH3:21].[O:11]=[C:10]([OH:22])[CH2:12][CH2:13][CH:14]([NH2:15])[C:16](=[O:17])[OH:18]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=21, 2=20, 3=19, 4=8, 5=6, 6=7, 7=5, 8=4, 9=2, 10=1, 11=3, 12=9, 13=10, 14=11, 15=12, 16=13, 17=14, 18=16, 19=17, 20=18, 21=15, 22=22}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=12, 2=11, 3=10, 4=8, 5=6, 6=7, 7=5, 8=4, 9=2, 10=1, 11=3, 12=9, 13=17, 14=16, 15=14, 16=13, 17=15, 18=18, 19=20, 20=21, 21=22, 22=19}

