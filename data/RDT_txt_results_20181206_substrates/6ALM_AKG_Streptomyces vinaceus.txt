
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0, C-O:2.0, O=O:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(=O)C[CH2]>>O=C(O)C[CH2]:1.0, O=C(O)C(=O)C[CH2]>>O=C=O:1.0, O=C(O)C(=O)[CH2]:1.0, O=C(O)C(=O)[CH2]>>O=C=O:2.0, O=C(O)C[CH2]:1.0, O=C(O)[CH2]:2.0, O=C=O:3.0, O=O:4.0, O=O>>O=C(O)C[CH2]:1.0, O=O>>O=C(O)[CH2]:1.0, O=O>>[CH]C(O)[CH2]:1.0, O=O>>[CH]O:1.0, O=O>>[C]C(N)C(O)C[CH2]:1.0, O=O>>[C]O:1.0, [CH2]:1.0, [CH]:1.0, [CH]C(O)[CH2]:2.0, [CH]C[CH2]:1.0, [CH]C[CH2]>>[CH]C(O)[CH2]:1.0, [CH]O:1.0, [C]:4.0, [C]=O:1.0, [C]C(=O)O:2.0, [C]C(=O)O>>O=C=O:2.0, [C]C(=O)[CH2]:1.0, [C]C(=O)[CH2]>>O=C(O)[CH2]:1.0, [C]C(N)C(O)C[CH2]:1.0, [C]C(N)CC[CH2]:1.0, [C]C(N)CC[CH2]>>[C]C(N)C(O)C[CH2]:1.0, [C]CCC(=O)C(=O)O>>[C]CCC(=O)O:1.0, [C]O:2.0, [C]O>>[C]=O:1.0, [N]CCCC(N)C(=O)O>>[N]CCC(O)C(N)C(=O)O:1.0, [OH]:3.0, [O]:3.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[CH]:1.0, [C]:3.0, [OH]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (7)
[O=C(O)[CH2]:1.0, O=O:2.0, [CH]C(O)[CH2]:1.0, [CH]O:1.0, [C]C(=O)O:1.0, [C]C(=O)[CH2]:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (7)
[O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(=O)[CH2]:1.0, O=C(O)C[CH2]:1.0, O=C(O)[CH2]:1.0, O=O:2.0, [CH]C(O)[CH2]:1.0, [C]C(N)C(O)C[CH2]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C=O:1.0, [C]=O:1.0, [C]C(=O)O:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (3)
[O=C(O)C(=O)[CH2]:1.0, O=C=O:2.0, [C]C(=O)O:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH2]:1.0, [CH]:1.0]


ID=Reaction Center at Level: 1 (2)
[[CH]C(O)[CH2]:1.0, [CH]C[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[[C]C(N)C(O)C[CH2]:1.0, [C]C(N)CC[CH2]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]C[CH2]>>[CH]C(O)[CH2]
2: O=O>>[CH]O
3: [C]C(=O)O>>O=C=O
4: [C]C(=O)[CH2]>>O=C(O)[CH2]
5: O=O>>[C]O
6: [C]O>>[C]=O

MMP Level 2
1: [C]C(N)CC[CH2]>>[C]C(N)C(O)C[CH2]
2: O=O>>[CH]C(O)[CH2]
3: O=C(O)C(=O)[CH2]>>O=C=O
4: O=C(O)C(=O)C[CH2]>>O=C(O)C[CH2]
5: O=O>>O=C(O)[CH2]
6: [C]C(=O)O>>O=C=O

MMP Level 3
1: [N]CCCC(N)C(=O)O>>[N]CCC(O)C(N)C(=O)O
2: O=O>>[C]C(N)C(O)C[CH2]
3: O=C(O)C(=O)C[CH2]>>O=C=O
4: [C]CCC(=O)C(=O)O>>[C]CCC(=O)O
5: O=O>>O=C(O)C[CH2]
6: O=C(O)C(=O)[CH2]>>O=C=O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C(O)C(N)CCCN=C(N)N>>O=C(O)C(N)C(O)CCN=C(N)N]
2: R:M00002, P:M00005	[O=C(O)C(=O)CCC(=O)O>>O=C(O)CCC(=O)O]
3: R:M00002, P:M00006	[O=C(O)C(=O)CCC(=O)O>>O=C=O, O=C(O)C(=O)CCC(=O)O>>O=C=O]
4: R:M00003, P:M00004	[O=O>>O=C(O)C(N)C(O)CCN=C(N)N]
5: R:M00003, P:M00005	[O=O>>O=C(O)CCC(=O)O]


//
SELECTED AAM MAPPING
[O:23]=[O:24].[O:13]=[C:14]([OH:15])[C:16](=[O:17])[CH2:18][CH2:19][C:20](=[O:21])[OH:22].[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][CH2:8][N:9]=[C:10]([NH2:11])[NH2:12]>>[O:13]=[C:14]=[O:15].[O:17]=[C:16]([OH:24])[CH2:18][CH2:19][C:20](=[O:21])[OH:22].[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH:6]([OH:23])[CH2:7][CH2:8][N:9]=[C:10]([NH2:12])[NH2:11]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=7, 2=6, 3=4, 4=2, 5=1, 6=3, 7=5, 8=8, 9=9, 10=10, 11=11, 12=12, 13=18, 14=19, 15=20, 16=21, 17=22, 18=16, 19=17, 20=14, 21=13, 22=15, 23=23, 24=24}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=8, 2=9, 3=10, 4=11, 5=12, 6=13, 7=6, 8=4, 9=2, 10=1, 11=3, 12=5, 13=7, 14=18, 15=17, 16=15, 17=14, 18=16, 19=19, 20=20, 21=21, 22=23, 23=22, 24=24}

