
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

//
FINGERPRINTS RC
[O=C(O)C(=O)C>>O=C(O)C(=O)CC(O)C[CH2]:1.0, O=CC[CH2]:1.0, O=CC[CH2]>>[C]CC(O)C[CH2]:2.0, O=C[CH2]:2.0, O=C[CH2]>>OC([CH2])[CH2]:2.0, O=[CH]:1.0, O=[CH]>>[CH]O:1.0, OC([CH2])[CH2]:2.0, [CH2]:1.0, [CH3]:1.0, [CH]:2.0, [CH]O:1.0, [C]C:1.0, [C]C(=O)C:1.0, [C]C(=O)C>>[C]C(=O)CC(O)[CH2]:1.0, [C]C(=O)CC(O)[CH2]:1.0, [C]C>>[C]C[CH]:1.0, [C]CC(O)C[CH2]:1.0, [C]CCC=O>>[C]C(=O)CC(O)CC[C]:1.0, [C]C[CH]:1.0, [OH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH2]:1.0, [CH]:1.0]


ID=Reaction Center at Level: 1 (2)
[OC([CH2])[CH2]:1.0, [C]C[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[C]C(=O)CC(O)[CH2]:1.0, [C]CC(O)C[CH2]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C[CH2]:1.0, O=[CH]:1.0, OC([CH2])[CH2]:1.0, [CH]O:1.0]


ID=Reaction Center at Level: 2 (4)
[O=CC[CH2]:1.0, O=C[CH2]:1.0, OC([CH2])[CH2]:1.0, [C]CC(O)C[CH2]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=C[CH2]>>OC([CH2])[CH2]
2: [C]C>>[C]C[CH]
3: O=[CH]>>[CH]O

MMP Level 2
1: O=CC[CH2]>>[C]CC(O)C[CH2]
2: [C]C(=O)C>>[C]C(=O)CC(O)[CH2]
3: O=C[CH2]>>OC([CH2])[CH2]

MMP Level 3
1: [C]CCC=O>>[C]C(=O)CC(O)CC[C]
2: O=C(O)C(=O)C>>O=C(O)C(=O)CC(O)C[CH2]
3: O=CC[CH2]>>[C]CC(O)C[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(=O)C>>O=C(O)C(=O)CC(O)CCC(=O)O]
2: R:M00002, P:M00003	[O=CCCC(=O)O>>O=C(O)C(=O)CC(O)CCC(=O)O, O=CCCC(=O)O>>O=C(O)C(=O)CC(O)CCC(=O)O]


//
SELECTED AAM MAPPING
[O:1]=[CH:2][CH2:3][CH2:4][C:5](=[O:6])[OH:7].[O:8]=[C:9]([OH:10])[C:11](=[O:12])[CH3:13]>>[O:8]=[C:9]([OH:10])[C:11](=[O:12])[CH2:13][CH:2]([OH:1])[CH2:3][CH2:4][C:5](=[O:6])[OH:7]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=13, 2=11, 3=12, 4=9, 5=8, 6=10, 7=3, 8=4, 9=5, 10=6, 11=7, 12=2, 13=1}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=9, 2=10, 3=11, 4=12, 5=13, 6=7, 7=6, 8=4, 9=5, 10=2, 11=1, 12=3, 13=8}

