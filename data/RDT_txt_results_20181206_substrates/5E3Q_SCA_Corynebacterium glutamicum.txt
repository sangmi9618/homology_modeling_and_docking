
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%N:1.0, C-N:1.0, C-S:1.0, C=O:1.0]

//
FINGERPRINTS RC
[O=C(N[CH])C[CH2]:1.0, O=C(O)C(=N[CH])C[CH2]:1.0, O=C(O)C(=N[CH])C[CH2]>>O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(=O)C[CH2]:1.0, O=C(O)C1=NC(C(=O)O)CCC1>>O=C(O)C(NC(=O)C[CH2])C[CH2]:1.0, O=C(SCC[NH])C[CH2]>>[NH]CCS:1.0, O=C(SC[CH2])[CH2]:1.0, O=C(SC[CH2])[CH2]>>SC[CH2]:1.0, O=C(S[CH2])C[CH2]:1.0, O=C(S[CH2])C[CH2]>>O=C(N[CH])C[CH2]:1.0, O=C([NH])[CH2]:1.0, O=C([S])[CH2]:1.0, O=C([S])[CH2]>>O=C([NH])[CH2]:1.0, SC[CH2]:1.0, S[CH2]:1.0, [C]:4.0, [C]=N[CH]:1.0, [C]=N[CH]>>[C]N[CH]:1.0, [C]C(=NC([C])[CH2])[CH2]:1.0, [C]C(=NC([C])[CH2])[CH2]>>[C]C([CH2])NC(=O)[CH2]:1.0, [C]C(=O)[CH2]:1.0, [C]C(=[N])[CH2]:1.0, [C]C(=[N])[CH2]>>[C]C(=O)[CH2]:1.0, [C]C([CH2])NC(=O)[CH2]:1.0, [C]C1N=C(C(=O)O)CCC1>>O=C(O)C(=O)CC[CH2]:1.0, [C]CCC(=O)SC[CH2]>>[C]CCC(=O)NC([C])[CH2]:1.0, [C]N[CH]:1.0, [C]S[CH2]:1.0, [C]S[CH2]>>S[CH2]:1.0, [NH]:1.0, [N]:1.0, [SH]:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[C]:4.0, [NH]:1.0, [N]:1.0, [O]:1.0, [S]:1.0]


ID=Reaction Center at Level: 1 (8)
[O=C([NH])[CH2]:1.0, O=C([S])[CH2]:1.0, [C]=N[CH]:1.0, [C]=O:1.0, [C]C(=O)[CH2]:1.0, [C]C(=[N])[CH2]:1.0, [C]N[CH]:1.0, [C]S[CH2]:1.0]


ID=Reaction Center at Level: 2 (8)
[O=C(N[CH])C[CH2]:1.0, O=C(O)C(=N[CH])C[CH2]:1.0, O=C(O)C(=O)C[CH2]:1.0, O=C(SC[CH2])[CH2]:1.0, O=C(S[CH2])C[CH2]:1.0, [C]C(=NC([C])[CH2])[CH2]:1.0, [C]C(=O)[CH2]:1.0, [C]C([CH2])NC(=O)[CH2]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C(=[N])[CH2]>>[C]C(=O)[CH2]
2: O=C([S])[CH2]>>O=C([NH])[CH2]
3: [C]S[CH2]>>S[CH2]
4: [C]=N[CH]>>[C]N[CH]

MMP Level 2
1: O=C(O)C(=N[CH])C[CH2]>>O=C(O)C(=O)C[CH2]
2: O=C(S[CH2])C[CH2]>>O=C(N[CH])C[CH2]
3: O=C(SC[CH2])[CH2]>>SC[CH2]
4: [C]C(=NC([C])[CH2])[CH2]>>[C]C([CH2])NC(=O)[CH2]

MMP Level 3
1: [C]C1N=C(C(=O)O)CCC1>>O=C(O)C(=O)CC[CH2]
2: [C]CCC(=O)SC[CH2]>>[C]CCC(=O)NC([C])[CH2]
3: O=C(SCC[NH])C[CH2]>>[NH]CCS
4: O=C(O)C1=NC(C(=O)O)CCC1>>O=C(O)C(NC(=O)C[CH2])C[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)CCC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O>>O=C(NCCS)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O]
2: R:M00001, P:M00004	[O=C(O)CCC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O>>O=C(O)C(=O)CCCC(NC(=O)CCC(=O)O)C(=O)O]
3: R:M00002, P:M00004	[O=C(O)C1=NC(C(=O)O)CCC1>>O=C(O)C(=O)CCCC(NC(=O)CCC(=O)O)C(=O)O, O=C(O)C1=NC(C(=O)O)CCC1>>O=C(O)C(=O)CCCC(NC(=O)CCC(=O)O)C(=O)O]


//
SELECTED AAM MAPPING
[O:56]=[C:57]([OH:58])[C:59]1=[N:60][CH:61]([C:62](=[O:63])[OH:64])[CH2:65][CH2:66][CH2:67]1.[O:1]=[C:2]([OH:3])[CH2:4][CH2:5][C:6](=[O:7])[S:8][CH2:9][CH2:10][NH:11][C:12](=[O:13])[CH2:14][CH2:15][NH:16][C:17](=[O:18])[CH:19]([OH:20])[C:21]([CH3:22])([CH3:23])[CH2:24][O:25][P:26](=[O:27])([OH:28])[O:29][P:30](=[O:31])([OH:32])[O:33][CH2:34][CH:35]1[O:36][CH:37]([N:38]:2:[CH:39]:[N:40]:[C:41]:3:[C:42](:[N:43]:[CH:44]:[N:45]:[C:46]32)[NH2:47])[CH:48]([OH:49])[CH:50]1[O:51][P:52](=[O:53])([OH:54])[OH:55]>>[O:56]=[C:57]([OH:58])[C:59](=[O:68])[CH2:67][CH2:66][CH2:65][CH:61]([NH:60][C:6](=[O:7])[CH2:5][CH2:4][C:2](=[O:1])[OH:3])[C:62](=[O:63])[OH:64].[O:13]=[C:12]([NH:11][CH2:10][CH2:9][SH:8])[CH2:14][CH2:15][NH:16][C:17](=[O:18])[CH:19]([OH:20])[C:21]([CH3:22])([CH3:23])[CH2:24][O:25][P:26](=[O:27])([OH:28])[O:29][P:30](=[O:31])([OH:32])[O:33][CH2:34][CH:35]1[O:36][CH:37]([N:38]:2:[CH:39]:[N:40]:[C:41]:3:[C:42](:[N:43]:[CH:44]:[N:45]:[C:46]32)[NH2:47])[CH:48]([OH:49])[CH:50]1[O:51][P:52](=[O:53])([OH:54])[OH:55]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=22, 2=21, 3=23, 4=24, 5=25, 6=26, 7=27, 8=28, 9=29, 10=30, 11=31, 12=32, 13=33, 14=34, 15=35, 16=50, 17=48, 18=37, 19=36, 20=38, 21=39, 22=40, 23=41, 24=46, 25=45, 26=44, 27=43, 28=42, 29=47, 30=49, 31=51, 32=52, 33=53, 34=54, 35=55, 36=19, 37=17, 38=18, 39=16, 40=15, 41=14, 42=12, 43=13, 44=11, 45=10, 46=9, 47=8, 48=6, 49=7, 50=5, 51=4, 52=2, 53=1, 54=3, 55=20, 56=66, 57=65, 58=61, 59=60, 60=59, 61=67, 62=57, 63=56, 64=58, 65=62, 66=63, 67=64}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=15, 2=14, 3=16, 4=17, 5=18, 6=19, 7=20, 8=21, 9=22, 10=23, 11=24, 12=25, 13=26, 14=27, 15=28, 16=43, 17=41, 18=30, 19=29, 20=31, 21=32, 22=33, 23=34, 24=39, 25=38, 26=37, 27=36, 28=35, 29=40, 30=42, 31=44, 32=45, 33=46, 34=47, 35=48, 36=12, 37=10, 38=11, 39=9, 40=8, 41=7, 42=2, 43=1, 44=3, 45=4, 46=5, 47=6, 48=13, 49=55, 50=56, 51=57, 52=66, 53=67, 54=68, 55=58, 56=59, 57=60, 58=61, 59=62, 60=63, 61=64, 62=65, 63=54, 64=52, 65=53, 66=50, 67=49, 68=51}

