
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)OC[CH]:1.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O[CH2]:1.0, O[CH2]:1.0, O[CH2]>>[P]O[CH2]:1.0, [CH]CO:1.0, [CH]CO>>O=P(O)(O)OC[CH]:1.0, [OH]:2.0, [O]:2.0, [O]C([CH])CO>>[O]C([CH])COP(=O)(O)O:1.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC[CH]:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[O]P(=O)(O)O:2.0, [P]O[CH2]:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)OC[CH]:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[P]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]P(=O)(O)O>>[O]P(=O)(O)O
2: [P]O[P]>>[P]O
3: O[CH2]>>[P]O[CH2]

MMP Level 2
1: O=P(O)(O)O[P]>>O=P(O)(O)O[CH2]
2: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
3: [CH]CO>>O=P(O)(O)OC[CH]

MMP Level 3
1: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC[CH]
2: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
3: [O]C([CH])CO>>[O]C([CH])COP(=O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=c1nc(N)ccn1C2OC(COP(=O)(O)O)C(O)C2O]
3: R:M00002, P:M00004	[O=c1nc(N)ccn1C2OC(CO)C(O)C2O>>O=c1nc(N)ccn1C2OC(COP(=O)(O)O)C(O)C2O]


//
SELECTED AAM MAPPING
[O:32]=[C:33]1[N:34]=[C:35]([NH2:36])[CH:37]=[CH:38][N:39]1[CH:40]2[O:41][CH:42]([CH2:43][OH:44])[CH:45]([OH:46])[CH:47]2[OH:48].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:32]=[C:33]1[N:34]=[C:35]([NH2:36])[CH:37]=[CH:38][N:39]1[CH:40]2[O:41][CH:42]([CH2:43][O:44][P:2](=[O:1])([OH:3])[OH:4])[CH:45]([OH:46])[CH:47]2[OH:48].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=37, 33=38, 34=39, 35=33, 36=32, 37=34, 38=35, 39=36, 40=40, 41=47, 42=45, 43=42, 44=41, 45=43, 46=44, 47=46, 48=48}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=17, 5=18, 6=19, 7=23, 8=16, 9=15, 10=14, 11=13, 12=24, 13=26, 14=11, 15=12, 16=10, 17=9, 18=6, 19=7, 20=8, 21=5, 22=2, 23=1, 24=3, 25=4, 26=27, 27=25, 28=33, 29=34, 30=35, 31=29, 32=28, 33=30, 34=31, 35=32, 36=36, 37=47, 38=45, 39=38, 40=37, 41=39, 42=40, 43=41, 44=42, 45=43, 46=44, 47=46, 48=48}

