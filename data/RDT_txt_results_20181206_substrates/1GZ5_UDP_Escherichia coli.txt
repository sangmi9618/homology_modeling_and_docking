
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=P(O)(O[P])OC(O[CH])C([CH])O>>O=P(O)(O)O[P]:1.0, OC1OC([CH2])[CH]C(O)C1O>>[O]C([CH])OC1OC([CH2])[CH]C(O)C1O:1.0, [CH]:4.0, [CH]O:1.0, [CH]O>>[CH]O[CH]:1.0, [CH]OC(O)C([CH])O:1.0, [CH]OC(O)C([CH])O>>[CH]OC(OC(O[CH])C([CH])O)C([CH])O:1.0, [CH]OC(O)C([CH])O>>[CH]OC(O[CH])C([CH])O:1.0, [CH]OC(O[CH])C([CH])O:2.0, [CH]O[CH]:1.0, [OH]:2.0, [O]:2.0, [O]C([CH])O:2.0, [O]C([CH])O>>[O]C([CH])OC([O])[CH]:1.0, [O]C([CH])O>>[O]C([O])[CH]:1.0, [O]C([CH])OC([O])[CH]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]C([O])[CH]:3.0, [O]C([O])[CH]>>[O]C([O])[CH]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OC1OC([CH2])[CH]C(O)C1O>>[O]C([CH])OC1OC([CH2])[CH]C(O)C1O:1.0, [P]O:1.0, [P]OC(O[CH])C([CH])O:1.0, [P]OC(O[CH])C([CH])O>>[CH]OC(O[CH])C([CH])O:1.0, [P]O[CH]:1.0, [P]O[CH]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (3)
[[CH]O[CH]:1.0, [O]C([O])[CH]:2.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[CH]OC(O[CH])C([CH])O:1.0, [O]C([CH])OC([O])[CH]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [P]OC(O[CH])C([CH])O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]C([CH])O:1.0, [O]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[CH]OC(O)C([CH])O:1.0, [CH]OC(O[CH])C([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [O]C([O])[CH]>>[O]C([O])[CH]
2: [O]C([CH])O>>[O]C([O])[CH]
3: [P]O[CH]>>[P]O
4: [CH]O>>[CH]O[CH]

MMP Level 2
1: [P]OC(O[CH])C([CH])O>>[CH]OC(O[CH])C([CH])O
2: [CH]OC(O)C([CH])O>>[CH]OC(O[CH])C([CH])O
3: [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O
4: [O]C([CH])O>>[O]C([CH])OC([O])[CH]

MMP Level 3
1: [O]P(=O)(O)OC1OC([CH2])[CH]C(O)C1O>>[O]C([CH])OC1OC([CH2])[CH]C(O)C1O
2: OC1OC([CH2])[CH]C(O)C1O>>[O]C([CH])OC1OC([CH2])[CH]C(O)C1O
3: O=P(O)(O[P])OC(O[CH])C([CH])O>>O=P(O)(O)O[P]
4: [CH]OC(O)C([CH])O>>[CH]OC(OC(O[CH])C([CH])O)C([CH])O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3O)C(O)C2O>>O=P(O)(O)OCC1OC(OC2OC(CO)C(O)C(O)C2O)C(O)C(O)C1O]
2: R:M00001, P:M00004	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3O)C(O)C2O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)O)C(O)C2O]
3: R:M00002, P:M00003	[O=P(O)(O)OCC1OC(O)C(O)C(O)C1O>>O=P(O)(O)OCC1OC(OC2OC(CO)C(O)C(O)C2O)C(O)C(O)C1O, O=P(O)(O)OCC1OC(O)C(O)C(O)C1O>>O=P(O)(O)OCC1OC(OC2OC(CO)C(O)C(O)C2O)C(O)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][CH:22]3[O:23][CH:24]([CH2:25][OH:26])[CH:27]([OH:28])[CH:29]([OH:30])[CH:31]3[OH:32])[CH:33]([OH:34])[CH:35]2[OH:36].[O:37]=[P:38]([OH:39])([OH:40])[O:41][CH2:42][CH:43]1[O:44][CH:45]([OH:46])[CH:47]([OH:48])[CH:49]([OH:50])[CH:51]1[OH:52]>>[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[OH:21])[CH:33]([OH:34])[CH:35]2[OH:36].[O:37]=[P:38]([OH:39])([OH:40])[O:41][CH2:42][CH:43]1[O:44][CH:45]([O:46][CH:22]2[O:23][CH:24]([CH2:25][OH:26])[CH:27]([OH:28])[CH:29]([OH:30])[CH:31]2[OH:32])[CH:47]([OH:48])[CH:49]([OH:50])[CH:51]1[OH:52]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=3, 2=4, 3=5, 4=6, 5=7, 6=8, 7=2, 8=1, 9=9, 10=35, 11=33, 12=11, 13=10, 14=12, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=31, 26=29, 27=27, 28=24, 29=23, 30=25, 31=26, 32=28, 33=30, 34=32, 35=34, 36=36, 37=42, 38=43, 39=51, 40=49, 41=47, 42=45, 43=44, 44=46, 45=48, 46=50, 47=52, 48=41, 49=38, 50=37, 51=39, 52=40}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=14, 2=13, 3=16, 4=18, 5=20, 6=11, 7=12, 8=10, 9=9, 10=22, 11=24, 12=26, 13=7, 14=8, 15=6, 16=5, 17=2, 18=1, 19=3, 20=4, 21=27, 22=25, 23=23, 24=21, 25=19, 26=17, 27=15, 28=30, 29=31, 30=32, 31=33, 32=34, 33=35, 34=29, 35=28, 36=36, 37=51, 38=49, 39=38, 40=37, 41=39, 42=40, 43=41, 44=42, 45=43, 46=44, 47=45, 48=46, 49=47, 50=48, 51=50, 52=52}

