
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[CH]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O[CH]:1.0, OC1OC([CH2])[CH]C(O)C1O>>O=P(O)(O)OC1OC([CH2])[CH]C(O)C1O:1.0, [CH]:2.0, [CH]O:1.0, [CH]O>>[P]O[CH]:1.0, [CH]OC(O)C([CH])O:1.0, [CH]OC(O)C([CH])O>>O=P(O)(O)OC(O[CH])C([CH])O:1.0, [CH]OC(O)C([CH])O>>[P]OC(O[CH])C([CH])O:1.0, [OH]:2.0, [O]:2.0, [O]C([CH])O:2.0, [O]C([CH])O>>[O]C([CH])OP(=O)(O)O:1.0, [O]C([CH])O>>[O]C([O])[CH]:1.0, [O]C([CH])OP(=O)(O)O:1.0, [O]C([O])[CH]:1.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]C([CH])OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]OC(O[CH])C([CH])O:1.0, [P]O[CH]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[O]P(=O)(O)O:2.0, [P]O[CH]:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)O[CH]:1.0, O=P(O)(O)O[P]:1.0, [O]C([CH])OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]C([CH])O:1.0, [O]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[CH]OC(O)C([CH])O:1.0, [P]OC(O[CH])C([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [O]P(=O)(O)O>>[O]P(=O)(O)O
2: [O]C([CH])O>>[O]C([O])[CH]
3: [P]O[P]>>[P]O
4: [CH]O>>[P]O[CH]

MMP Level 2
1: O=P(O)(O)O[P]>>O=P(O)(O)O[CH]
2: [CH]OC(O)C([CH])O>>[P]OC(O[CH])C([CH])O
3: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
4: [O]C([CH])O>>[O]C([CH])OP(=O)(O)O

MMP Level 3
1: [O]P(=O)(O)OP(=O)(O)O>>[O]C([CH])OP(=O)(O)O
2: OC1OC([CH2])[CH]C(O)C1O>>O=P(O)(O)OC1OC([CH2])[CH]C(O)C1O
3: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
4: [CH]OC(O)C([CH])O>>O=P(O)(O)OC(O[CH])C([CH])O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OC1OC(CO)C(O)C(O)C1O]
3: R:M00002, P:M00004	[OCC1OC(O)C(O)C(O)C1O>>O=P(O)(O)OC1OC(CO)C(O)C(O)C1O, OCC1OC(O)C(O)C(O)C1O>>O=P(O)(O)OC1OC(CO)C(O)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31].[OH:32][CH2:33][CH:34]1[O:35][CH:36]([OH:37])[CH:38]([OH:39])[CH:40]([OH:41])[CH:42]1[OH:43]>>[O:1]=[P:2]([OH:3])([OH:4])[O:37][CH:36]1[O:35][CH:34]([CH2:33][OH:32])[CH:42]([OH:43])[CH:40]([OH:41])[CH:38]1[OH:39].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=33, 33=34, 34=42, 35=40, 36=38, 37=36, 38=35, 39=37, 40=39, 41=41, 42=43, 43=32}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=17, 5=18, 6=19, 7=23, 8=16, 9=15, 10=14, 11=13, 12=24, 13=26, 14=11, 15=12, 16=10, 17=9, 18=6, 19=7, 20=8, 21=5, 22=2, 23=1, 24=3, 25=4, 26=27, 27=25, 28=36, 29=35, 30=38, 31=40, 32=42, 33=33, 34=34, 35=32, 36=29, 37=28, 38=30, 39=31, 40=43, 41=41, 42=39, 43=37}

