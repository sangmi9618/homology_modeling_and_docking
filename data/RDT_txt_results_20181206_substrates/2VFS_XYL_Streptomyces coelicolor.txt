
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%O:1.0]

ORDER_CHANGED
[O-O*O=O:1.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O=O:4.0, O=O>>OO:6.0, OC([CH2])C(O)C(O)[CH2]:1.0, OC([CH2])C(O)C(O)[CH2]>>[CH]C(O)C(O)C(O)[CH2]:1.0, OCC(O)C(O)C(O)CO>>OC1OCC(O)C(O)C1O:1.0, OCC(O)C(O)C(O)[CH2]>>OC1OCC(O)C(O)C1O:1.0, OO:4.0, O[CH2]:1.0, O[CH2]>>[CH]O[CH2]:1.0, [CH2]:1.0, [CH]:5.0, [CH]C(O)C(O)C(O)[CH2]:1.0, [CH]C(O)C(O)CO:1.0, [CH]C(O)C(O)CO>>OC1OC[CH]C(O)C1O:1.0, [CH]C(O)C(O)CO>>[O]C(O)C(O)C([CH])O:1.0, [CH]C(O)C(O)O[CH2]:1.0, [CH]C(O)CO:1.0, [CH]C(O)CO>>OC1OCC(O)[CH]C1O:1.0, [CH]C(O)CO>>[CH]C(O)C(O)O[CH2]:1.0, [CH]C(O)[CH2]:1.0, [CH]C(O)[CH2]>>[CH]C([CH])O:1.0, [CH]C([CH])O:3.0, [CH]C([CH])O>>[CH]C([CH])O:1.0, [CH]CO:2.0, [CH]CO>>[CH]COC([CH])O:1.0, [CH]CO>>[O]C([CH])O:1.0, [CH]COC([CH])O:1.0, [CH]O[CH2]:1.0, [OH]:3.0, [O]:3.0, [O]C(O)C(O)C([CH])O:1.0, [O]C([CH])O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (2)
[[CH]O[CH2]:1.0, [O]C([CH])O:1.0]


ID=Reaction Center at Level: 2 (2)
[[CH]C(O)C(O)O[CH2]:1.0, [CH]COC([CH])O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (2)
[[OH]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (2)
[O=O:2.0, OO:2.0]


ID=Reaction Center at Level: 2 (2)
[O=O:2.0, OO:2.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:4.0]


ID=Reaction Center at Level: 1 (2)
[[CH]C(O)[CH2]:1.0, [CH]C([CH])O:3.0]


ID=Reaction Center at Level: 2 (4)
[OC([CH2])C(O)C(O)[CH2]:1.0, [CH]C(O)C(O)C(O)[CH2]:1.0, [CH]C(O)C(O)CO:1.0, [O]C(O)C(O)C([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: O=O>>OO
2: [CH]C(O)[CH2]>>[CH]C([CH])O
3: O[CH2]>>[CH]O[CH2]
4: [CH]CO>>[O]C([CH])O
5: [CH]C([CH])O>>[CH]C([CH])O

MMP Level 2
1: O=O>>OO
2: [CH]C(O)C(O)CO>>[O]C(O)C(O)C([CH])O
3: [CH]CO>>[CH]COC([CH])O
4: [CH]C(O)CO>>[CH]C(O)C(O)O[CH2]
5: OC([CH2])C(O)C(O)[CH2]>>[CH]C(O)C(O)C(O)[CH2]

MMP Level 3
1: O=O>>OO
2: OCC(O)C(O)C(O)[CH2]>>OC1OCC(O)C(O)C1O
3: [CH]C(O)CO>>OC1OCC(O)[CH]C1O
4: [CH]C(O)C(O)CO>>OC1OC[CH]C(O)C1O
5: OCC(O)C(O)C(O)CO>>OC1OCC(O)C(O)C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[OCC(O)C(O)C(O)CO>>OC1OCC(O)C(O)C1O, OCC(O)C(O)C(O)CO>>OC1OCC(O)C(O)C1O, OCC(O)C(O)C(O)CO>>OC1OCC(O)C(O)C1O, OCC(O)C(O)C(O)CO>>OC1OCC(O)C(O)C1O]
2: R:M00002, P:M00004	[O=O>>OO]


//
SELECTED AAM MAPPING
[O:11]=[O:12].[OH:1][CH2:2][CH:3]([OH:4])[CH:5]([OH:6])[CH:7]([OH:8])[CH2:9][OH:10]>>[OH:11][OH:12].[OH:1][CH:2]1[O:10][CH2:9][CH:7]([OH:8])[CH:5]([OH:6])[CH:3]1[OH:4]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=7, 3=5, 4=3, 5=2, 6=1, 7=4, 8=6, 9=8, 10=10, 11=11, 12=12}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=4, 2=5, 3=7, 4=9, 5=2, 6=3, 7=1, 8=10, 9=8, 10=6, 11=11, 12=12}

