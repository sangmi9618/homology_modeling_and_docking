
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O:3.0, O=P(O)(O)O:3.0, O=P(O)(O)OC[CH]:1.0, O=P(O)(O)OC[CH]>>O=P(O)(O)O:1.0, O=P(O)(O)OC[CH]>>[CH]CO:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[CH2]>>O=P(O)(O)O:1.0, O>>O=P(O)(O)O:2.0, O>>[P]O:1.0, O[CH2]:1.0, [CH]CO:1.0, [OH]:2.0, [O]:1.0, [O]C([CH])COP(=O)(O)O>>[O]C([CH])CO:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[CH2]>>O[CH2]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=P(O)(O)O:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=P(O)(O)O:2.0, O=P(O)(O)OC[CH]:1.0, O=P(O)(O)O[CH2]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O>>[P]O
2: [O]P(=O)(O)O>>O=P(O)(O)O
3: [P]O[CH2]>>O[CH2]

MMP Level 2
1: O>>O=P(O)(O)O
2: O=P(O)(O)O[CH2]>>O=P(O)(O)O
3: O=P(O)(O)OC[CH]>>[CH]CO

MMP Level 3
1: O>>O=P(O)(O)O
2: O=P(O)(O)OC[CH]>>O=P(O)(O)O
3: [O]C([CH])COP(=O)(O)O>>[O]C([CH])CO


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1nc[nH]c2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O>>O=c1nc[nH]c2c1ncn2C3OC(CO)C(O)C3O]
2: R:M00001, P:M00004	[O=c1nc[nH]c2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O>>O=P(O)(O)O]
3: R:M00002, P:M00004	[O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[CH:4][NH:5][C:6]:2:[C:7]1:[N:8]:[CH:9]:[N:10]2[CH:11]3[O:12][CH:13]([CH2:14][O:15][P:16](=[O:17])([OH:18])[OH:19])[CH:20]([OH:21])[CH:22]3[OH:23].[OH2:24]>>[O:1]=[C:2]1[N:3]=[CH:4][NH:5][C:6]:2:[C:7]1:[N:8]:[CH:9]:[N:10]2[CH:11]3[O:12][CH:13]([CH2:14][OH:15])[CH:20]([OH:21])[CH:22]3[OH:23].[O:17]=[P:16]([OH:19])([OH:24])[OH:18]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=4, 2=3, 3=2, 4=1, 5=7, 6=6, 7=5, 8=10, 9=9, 10=8, 11=11, 12=22, 13=20, 14=13, 15=12, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=21, 23=23, 24=24}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=4, 2=3, 3=2, 4=1, 5=7, 6=6, 7=5, 8=10, 9=9, 10=8, 11=11, 12=18, 13=16, 14=13, 15=12, 16=14, 17=15, 18=17, 19=19, 20=22, 21=21, 22=20, 23=23, 24=24}

