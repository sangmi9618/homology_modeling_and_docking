
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-S:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)O[CH2]>>O=P(O)(O[CH2])OS(=O)(=O)O:1.0, O=S(=O)(O)O[P]:1.0, [C]=C([CH])O:1.0, [C]=C([CH])OS(=O)(=O)O:1.0, [C]=C([CH])OS(=O)(=O)O>>[C]=C([CH])O:1.0, [C]=C([CH])OS(=O)(=O)O>>[O]P(=O)(O)OS(=O)(=O)O:1.0, [C]=CC(OS(=O)(=O)O)=C([CH])O>>[C]=CC(O)=C([CH])O:1.0, [C]O:1.0, [C]OS(=O)(=O)O:1.0, [C]OS(=O)(=O)O>>O=S(=O)(O)O[P]:1.0, [C]O[S]:1.0, [C]O[S]>>[C]O:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>[O]P(=O)(O)OS(=O)(=O)O:1.0, [O]P(=O)(O)OS(=O)(=O)O:1.0, [O]S(=O)(=O)O:2.0, [O]S(=O)(=O)O>>[O]S(=O)(=O)O:1.0, [P]O:1.0, [P]O>>[P]O[S]:1.0, [P]O[S]:1.0, [S]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [S]:2.0]


ID=Reaction Center at Level: 1 (3)
[[C]O[S]:1.0, [O]S(=O)(=O)O:2.0, [P]O[S]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=S(=O)(O)O[P]:1.0, [C]=C([CH])OS(=O)(=O)O:1.0, [C]OS(=O)(=O)O:1.0, [O]P(=O)(O)OS(=O)(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O>>[P]O[S]
2: [O]S(=O)(=O)O>>[O]S(=O)(=O)O
3: [C]O[S]>>[C]O

MMP Level 2
1: [O]P(=O)(O)O>>[O]P(=O)(O)OS(=O)(=O)O
2: [C]OS(=O)(=O)O>>O=S(=O)(O)O[P]
3: [C]=C([CH])OS(=O)(=O)O>>[C]=C([CH])O

MMP Level 3
1: O=P(O)(O)O[CH2]>>O=P(O)(O[CH2])OS(=O)(=O)O
2: [C]=C([CH])OS(=O)(=O)O>>[O]P(=O)(O)OS(=O)(=O)O
3: [C]=CC(OS(=O)(=O)O)=C([CH])O>>[C]=CC(O)=C([CH])O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O>>O=P(O)(O)OC1C(O)C(OC1COP(=O)(O)OS(=O)(=O)O)n2cnc3c(ncnc32)N]
2: R:M00002, P:M00003	[O=S(=O)(O)Oc1cc(ccc1O)CCN>>O=P(O)(O)OC1C(O)C(OC1COP(=O)(O)OS(=O)(=O)O)n2cnc3c(ncnc32)N]
3: R:M00002, P:M00004	[O=S(=O)(O)Oc1cc(ccc1O)CCN>>Oc1ccc(cc1O)CCN]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH2:6][CH:7]1[O:8][CH:9]([N:10]:2:[CH:11]:[N:12]:[C:13]:3:[C:14](:[N:15]:[CH:16]:[N:17]:[C:18]32)[NH2:19])[CH:20]([OH:21])[CH:22]1[O:23][P:24](=[O:25])([OH:26])[OH:27].[O:28]=[S:29](=[O:30])([OH:31])[O:32][C:33]:1:[CH:34]:[C:35](:[CH:36]:[CH:37]:[C:38]1[OH:39])[CH2:40][CH2:41][NH2:42]>>[O:25]=[P:24]([OH:26])([OH:27])[O:23][CH:22]1[CH:20]([OH:21])[CH:9]([O:8][CH:7]1[CH2:6][O:5][P:2](=[O:1])([OH:3])[O:4][S:29](=[O:28])(=[O:30])[OH:31])[N:10]:2:[CH:11]:[N:12]:[C:13]:3:[C:14](:[N:15]:[CH:16]:[N:17]:[C:18]32)[NH2:19].[OH:39][C:38]:1:[CH:37]:[CH:36]:[C:35](:[CH:34]:[C:33]1[OH:32])[CH2:40][CH2:41][NH2:42]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=16, 2=17, 3=18, 4=13, 5=14, 6=15, 7=19, 8=12, 9=11, 10=10, 11=9, 12=20, 13=22, 14=7, 15=8, 16=6, 17=5, 18=2, 19=1, 20=3, 21=4, 22=23, 23=24, 24=25, 25=26, 26=27, 27=21, 28=36, 29=37, 30=38, 31=33, 32=34, 33=35, 34=40, 35=41, 36=42, 37=32, 38=29, 39=28, 40=30, 41=31, 42=39}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=28, 2=29, 3=30, 4=25, 5=26, 6=27, 7=31, 8=24, 9=23, 10=22, 11=9, 12=7, 13=6, 14=11, 15=10, 16=12, 17=13, 18=14, 19=15, 20=16, 21=17, 22=18, 23=19, 24=20, 25=21, 26=5, 27=2, 28=1, 29=3, 30=4, 31=8, 32=35, 33=34, 34=33, 35=38, 36=37, 37=36, 38=40, 39=41, 40=42, 41=39, 42=32}

