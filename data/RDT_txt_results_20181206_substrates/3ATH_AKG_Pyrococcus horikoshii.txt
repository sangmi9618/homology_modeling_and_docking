
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:2.0, C=O:2.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(=O)C[CH2]>>O=C(O)C(N)C[CH2]:1.0, O=C(O)C(=O)C[CH2]>>[C]CC(=O)C(=O)O:1.0, O=C(O)C(N)C[CH2]:1.0, [CH]:2.0, [CH]N:2.0, [CH]N>>[CH]N:1.0, [C]:2.0, [C]=O:2.0, [C]=O>>[C]=O:1.0, [C]C(=O)CC(N)C(=O)O>>[C]C(=O)CC(=O)C(=O)O:1.0, [C]C(=O)[CH2]:4.0, [C]C(=O)[CH2]>>[C]C(=O)[CH2]:1.0, [C]C(=O)[CH2]>>[C]C([CH2])N:1.0, [C]C([CH2])N:4.0, [C]C([CH2])N>>[C]C(=O)[CH2]:1.0, [C]C([CH2])N>>[C]C([CH2])N:1.0, [C]CC(=O)C(=O)O:1.0, [C]CC(N)C(=O)O:1.0, [C]CC(N)C(=O)O>>O=C(O)C(N)C[CH2]:1.0, [C]CC(N)C(=O)O>>[C]CC(=O)C(=O)O:1.0, [C]CCC(=O)C(=O)O>>[C]CCC(N)C(=O)O:1.0, [NH2]:2.0, [O]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[CH]:2.0, [C]:2.0, [NH2]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (4)
[[CH]N:2.0, [C]=O:2.0, [C]C(=O)[CH2]:2.0, [C]C([CH2])N:2.0]


ID=Reaction Center at Level: 2 (6)
[O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(N)C[CH2]:1.0, [C]C(=O)[CH2]:2.0, [C]C([CH2])N:2.0, [C]CC(=O)C(=O)O:1.0, [C]CC(N)C(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:2.0, [C]:2.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(=O)[CH2]:2.0, [C]C([CH2])N:2.0]


ID=Reaction Center at Level: 2 (4)
[O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(N)C[CH2]:1.0, [C]CC(=O)C(=O)O:1.0, [C]CC(N)C(=O)O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C([CH2])N>>[C]C(=O)[CH2]
2: [CH]N>>[CH]N
3: [C]=O>>[C]=O
4: [C]C(=O)[CH2]>>[C]C([CH2])N

MMP Level 2
1: [C]CC(N)C(=O)O>>[C]CC(=O)C(=O)O
2: [C]C([CH2])N>>[C]C([CH2])N
3: [C]C(=O)[CH2]>>[C]C(=O)[CH2]
4: O=C(O)C(=O)C[CH2]>>O=C(O)C(N)C[CH2]

MMP Level 3
1: [C]C(=O)CC(N)C(=O)O>>[C]C(=O)CC(=O)C(=O)O
2: [C]CC(N)C(=O)O>>O=C(O)C(N)C[CH2]
3: O=C(O)C(=O)C[CH2]>>[C]CC(=O)C(=O)O
4: [C]CCC(=O)C(=O)O>>[C]CCC(N)C(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)CC(=O)c1ccccc1N>>O=C(O)C(=O)CC(=O)c1ccccc1N]
2: R:M00001, P:M00004	[O=C(O)C(N)CC(=O)c1ccccc1N>>O=C(O)CCC(N)C(=O)O]
3: R:M00002, P:M00003	[O=C(O)C(=O)CCC(=O)O>>O=C(O)C(=O)CC(=O)c1ccccc1N]
4: R:M00002, P:M00004	[O=C(O)C(=O)CCC(=O)O>>O=C(O)CCC(N)C(=O)O]


//
SELECTED AAM MAPPING
[O:16]=[C:17]([OH:18])[C:19](=[O:20])[CH2:21][CH2:22][C:23](=[O:24])[OH:25].[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][C:7](=[O:8])[C:9]:1:[CH:10]:[CH:11]:[CH:12]:[CH:13]:[C:14]1[NH2:15]>>[O:1]=[C:2]([OH:3])[C:4](=[O:20])[CH2:6][C:7](=[O:8])[C:9]:1:[CH:10]:[CH:11]:[CH:12]:[CH:13]:[C:14]1[NH2:15].[O:24]=[C:23]([OH:25])[CH2:22][CH2:21][CH:19]([NH2:5])[C:17](=[O:16])[OH:18]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=11, 2=12, 3=13, 4=14, 5=9, 6=10, 7=7, 8=8, 9=6, 10=4, 11=2, 12=1, 13=3, 14=5, 15=15, 16=21, 17=22, 18=23, 19=24, 20=25, 21=19, 22=20, 23=17, 24=16, 25=18}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=11, 2=12, 3=13, 4=14, 5=9, 6=10, 7=7, 8=8, 9=6, 10=4, 11=5, 12=2, 13=1, 14=3, 15=15, 16=20, 17=19, 18=17, 19=16, 20=18, 21=21, 22=23, 23=24, 24=25, 25=22}

