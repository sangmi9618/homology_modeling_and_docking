
//
FINGERPRINTS BC

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[OCC1O[CH]C(O)C(O)C1O>>OCC1O[CH]C(O)C(O)C1O:1.0, [CH]:2.0, [CH]C([CH])O:2.0, [CH]C([CH])O>>[CH]C([CH])O:1.0, [O]C([CH2])C(O)C([CH])O:2.0, [O]C([CH2])C(O)C([CH])O>>[O]C([CH2])C(O)C([CH])O:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (1)
[[CH]C([CH])O:2.0]


ID=Reaction Center at Level: 2 (1)
[[O]C([CH2])C(O)C([CH])O:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]C([CH])O>>[CH]C([CH])O

MMP Level 2
1: [O]C([CH2])C(O)C([CH])O>>[O]C([CH2])C(O)C([CH])O

MMP Level 3
1: OCC1O[CH]C(O)C(O)C1O>>OCC1O[CH]C(O)C(O)C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3O)C(O)C2O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3O)C(O)C2O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][CH:22]3[O:23][CH:24]([CH2:25][OH:26])[CH:27]([OH:28])[CH:29]([OH:30])[CH:31]3[OH:32])[CH:33]([OH:34])[CH:35]2[OH:36]>>[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][CH:22]3[O:23][CH:24]([CH2:25][OH:26])[CH:27]([OH:28])[CH:29]([OH:30])[CH:31]3[OH:32])[CH:33]([OH:34])[CH:35]2[OH:36]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=3, 2=4, 3=5, 4=6, 5=7, 6=8, 7=2, 8=1, 9=9, 10=35, 11=33, 12=11, 13=10, 14=12, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=31, 26=29, 27=27, 28=24, 29=23, 30=25, 31=26, 32=28, 33=30, 34=32, 35=34, 36=36}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=3, 2=4, 3=5, 4=6, 5=7, 6=8, 7=2, 8=1, 9=9, 10=35, 11=33, 12=11, 13=10, 14=12, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=31, 26=29, 27=27, 28=24, 29=23, 30=25, 31=26, 32=28, 33=30, 34=32, 35=34, 36=36}

