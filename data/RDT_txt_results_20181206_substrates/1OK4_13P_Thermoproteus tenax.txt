
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%C:1.0, C%O:1.0]

ORDER_CHANGED
[C%O*C=O:1.0, C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):3.0]

//
FINGERPRINTS RC
[O=C([CH2])CO:1.0, O=C([CH2])[CH2]:2.0, O=CC(O)[CH2]:1.0, O=C[CH]:2.0, O=[CH]:1.0, OC1(OC([CH2])[CH][CH]1)[CH2]:1.0, OC1(OC([CH2])[CH][CH]1)[CH2]>>O=C([CH2])[CH2]:1.0, OC1[CH]OC(O)([CH2])C1O:1.0, OC1[CH]OC(O)([CH2])C1O>>O=CC(O)[CH2]:2.0, OC1[C]OC([CH2])C1O:1.0, OC1[C]OC([CH2])C1O>>O=C([CH2])CO:1.0, [CH2]:1.0, [CH]:5.0, [CH]C(O)[CH2]:1.0, [CH]C([CH])O:1.0, [CH]C([CH])O>>[C]CO:1.0, [CH]O:1.0, [CH]O>>O=[CH]:1.0, [C]:2.0, [C]=O:1.0, [C]C([CH])O:2.0, [C]C([CH])O>>O=C[CH]:2.0, [C]CO:1.0, [C]O[CH]:1.0, [C]O[CH]>>[C]=O:1.0, [OH]:1.0, [O]:3.0, [O]C([CH])(O)[CH2]:1.0, [O]C([CH])(O)[CH2]>>[CH]C(O)[CH2]:1.0, [O]C([CH])[CH2]:1.0, [O]C([CH])[CH2]>>O=C([CH2])[CH2]:1.0, [O]CC(=O)CO:1.0, [O]CC(O)C=O:1.0, [O]CC1(O)OC([CH2])C(O)C1O>>[O]CC(O)C=O:1.0, [O]CC1(O)O[CH][CH]C1O:1.0, [O]CC1(O)O[CH][CH]C1O>>[O]CC(O)C=O:1.0, [O]CC1OC(O)(C[O])C(O)C1O>>[O]CC(=O)CO:1.0, [O]CC1OC(O)([CH2])C(O)C1O>>[O]CC(=O)CO:1.0, [O]CC1O[C][CH]C1O:1.0, [O]CC1O[C][CH]C1O>>[O]CC(=O)CO:1.0, [P]OCC1(O)OC([CH2])C(O)C1O>>O=CC(O)CO[P]:1.0, [P]OCC1OC(O)([CH2])C(O)C1O>>O=C(CO)CO[P]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [C]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[CH]C([CH])O:1.0, [C]C([CH])O:1.0, [C]O[CH]:1.0, [O]C([CH])(O)[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[OC1(OC([CH2])[CH][CH]1)[CH2]:1.0, OC1[CH]OC(O)([CH2])C1O:1.0, OC1[C]OC([CH2])C1O:1.0, [O]CC1(O)O[CH][CH]C1O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH]:3.0, [C]:1.0, [OH]:1.0, [O]:3.0]


ID=Reaction Center at Level: 1 (8)
[O=C([CH2])[CH2]:1.0, O=C[CH]:1.0, O=[CH]:1.0, [CH]O:1.0, [C]=O:1.0, [C]C([CH])O:1.0, [C]O[CH]:1.0, [O]C([CH])[CH2]:1.0]


ID=Reaction Center at Level: 2 (8)
[O=C([CH2])[CH2]:1.0, O=CC(O)[CH2]:1.0, O=C[CH]:1.0, OC1(OC([CH2])[CH][CH]1)[CH2]:1.0, OC1[CH]OC(O)([CH2])C1O:1.0, [C]C([CH])O:1.0, [O]CC(=O)CO:1.0, [O]CC1O[C][CH]C1O:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (3)
[[CH2]:1.0, [CH]:4.0, [C]:1.0]


ID=Reaction Center at Level: 1 (6)
[O=C([CH2])[CH2]:1.0, O=C[CH]:1.0, [CH]C([CH])O:1.0, [C]C([CH])O:1.0, [C]CO:1.0, [O]C([CH])[CH2]:1.0]


ID=Reaction Center at Level: 2 (6)
[O=C([CH2])CO:1.0, O=CC(O)[CH2]:1.0, OC1[CH]OC(O)([CH2])C1O:1.0, OC1[C]OC([CH2])C1O:1.0, [O]CC(=O)CO:1.0, [O]CC1O[C][CH]C1O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]C([CH])O>>[C]CO
2: [C]O[CH]>>[C]=O
3: [O]C([CH])[CH2]>>O=C([CH2])[CH2]
4: [C]C([CH])O>>O=C[CH]
5: [O]C([CH])(O)[CH2]>>[CH]C(O)[CH2]
6: [CH]O>>O=[CH]

MMP Level 2
1: OC1[C]OC([CH2])C1O>>O=C([CH2])CO
2: OC1(OC([CH2])[CH][CH]1)[CH2]>>O=C([CH2])[CH2]
3: [O]CC1O[C][CH]C1O>>[O]CC(=O)CO
4: OC1[CH]OC(O)([CH2])C1O>>O=CC(O)[CH2]
5: [O]CC1(O)O[CH][CH]C1O>>[O]CC(O)C=O
6: [C]C([CH])O>>O=C[CH]

MMP Level 3
1: [O]CC1OC(O)([CH2])C(O)C1O>>[O]CC(=O)CO
2: [O]CC1OC(O)(C[O])C(O)C1O>>[O]CC(=O)CO
3: [P]OCC1OC(O)([CH2])C(O)C1O>>O=C(CO)CO[P]
4: [O]CC1(O)OC([CH2])C(O)C1O>>[O]CC(O)C=O
5: [P]OCC1(O)OC([CH2])C(O)C1O>>O=CC(O)CO[P]
6: OC1[CH]OC(O)([CH2])C1O>>O=CC(O)[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=P(O)(O)OCC1OC(O)(COP(=O)(O)O)C(O)C1O>>O=CC(O)COP(=O)(O)O, O=P(O)(O)OCC1OC(O)(COP(=O)(O)O)C(O)C1O>>O=CC(O)COP(=O)(O)O, O=P(O)(O)OCC1OC(O)(COP(=O)(O)O)C(O)C1O>>O=CC(O)COP(=O)(O)O]
2: R:M00001, P:M00003	[O=P(O)(O)OCC1OC(O)(COP(=O)(O)O)C(O)C1O>>O=C(CO)COP(=O)(O)O, O=P(O)(O)OCC1OC(O)(COP(=O)(O)O)C(O)C1O>>O=C(CO)COP(=O)(O)O, O=P(O)(O)OCC1OC(O)(COP(=O)(O)O)C(O)C1O>>O=C(CO)COP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH2:6][CH:7]1[O:8][C:9]([OH:10])([CH2:11][O:12][P:13](=[O:14])([OH:15])[OH:16])[CH:17]([OH:18])[CH:19]1[OH:20]>>[O:18]=[CH:17][CH:9]([OH:10])[CH2:11][O:12][P:13](=[O:14])([OH:16])[OH:15].[O:8]=[C:7]([CH2:19][OH:20])[CH2:6][O:5][P:2](=[O:1])([OH:3])[OH:4]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=19, 4=17, 5=9, 6=8, 7=11, 8=12, 9=13, 10=14, 11=15, 12=16, 13=10, 14=18, 15=20, 16=5, 17=2, 18=1, 19=3, 20=4}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=5, 2=3, 3=2, 4=1, 5=4, 6=6, 7=7, 8=8, 9=9, 10=10, 11=13, 12=12, 13=11, 14=15, 15=16, 16=17, 17=18, 18=19, 19=20, 20=14}

