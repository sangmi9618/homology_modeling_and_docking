
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O=C(N)N[CH]:1.0, O=C(OP(=O)(O)O)N:1.0, O=C(OP(=O)(O)O)N>>O=P(O)(O)O:2.0, O=C(OP(=O)(O)O)N>>[C]C([CH2])NC(=O)N:1.0, O=C(O[P])N:1.0, O=C(O[P])N>>O=C(N)N[CH]:1.0, O=C([NH])N:1.0, O=P(O)(O)O:1.0, [CH]N:1.0, [CH]N>>[C]N[CH]:1.0, [C]:2.0, [C]C([CH2])N:1.0, [C]C([CH2])N>>[C]C([CH2])NC(=O)N:1.0, [C]C([CH2])NC(=O)N:1.0, [C]CC(N)C(=O)O>>[C]CC(NC(=O)N)C(=O)O:1.0, [C]N[CH]:1.0, [C]O[P]:1.0, [C]O[P]>>[P]O:1.0, [NH2]:1.0, [NH]:1.0, [OH]:1.0, [O]:1.0, [O]C(=O)N:1.0, [O]C(=O)N>>O=C([NH])N:1.0, [P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [NH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([NH])N:1.0, [C]N[CH]:1.0, [C]O[P]:1.0, [O]C(=O)N:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(N)N[CH]:1.0, O=C(OP(=O)(O)O)N:1.0, O=C(O[P])N:1.0, [C]C([CH2])NC(=O)N:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]C(=O)N>>O=C([NH])N
2: [CH]N>>[C]N[CH]
3: [C]O[P]>>[P]O

MMP Level 2
1: O=C(O[P])N>>O=C(N)N[CH]
2: [C]C([CH2])N>>[C]C([CH2])NC(=O)N
3: O=C(OP(=O)(O)O)N>>O=P(O)(O)O

MMP Level 3
1: O=C(OP(=O)(O)O)N>>[C]C([CH2])NC(=O)N
2: [C]CC(N)C(=O)O>>[C]CC(NC(=O)N)C(=O)O
3: O=C(OP(=O)(O)O)N>>O=P(O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C(O)CC(N)C(=O)O>>O=C(N)NC(C(=O)O)CC(=O)O]
2: R:M00002, P:M00003	[O=C(OP(=O)(O)O)N>>O=P(O)(O)O]
3: R:M00002, P:M00004	[O=C(OP(=O)(O)O)N>>O=C(N)NC(C(=O)O)CC(=O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH2:4][CH:5]([NH2:6])[C:7](=[O:8])[OH:9].[O:10]=[C:11]([O:12][P:13](=[O:14])([OH:15])[OH:16])[NH2:17]>>[O:1]=[C:2]([OH:3])[CH2:4][CH:5]([NH:6][C:11](=[O:10])[NH2:17])[C:7](=[O:8])[OH:9].[O:14]=[P:13]([OH:12])([OH:15])[OH:16]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=4, 2=5, 3=7, 4=8, 5=9, 6=6, 7=2, 8=1, 9=3, 10=11, 11=10, 12=17, 13=12, 14=13, 15=14, 16=15, 17=16}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=15, 2=14, 3=13, 4=16, 5=17, 6=9, 7=5, 8=6, 9=7, 10=8, 11=4, 12=2, 13=1, 14=3, 15=10, 16=11, 17=12}

