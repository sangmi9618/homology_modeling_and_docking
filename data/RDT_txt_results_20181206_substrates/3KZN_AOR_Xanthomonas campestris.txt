
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(N)NC[CH2]:1.0, O=C(N)N[CH2]:1.0, O=C(O)C(NC(=O)C)CC[CH2]>>O=C(O)C(NC(=O)C)CC[CH2]:1.0, O=C(OP(=O)(O)O)N:1.0, O=C(OP(=O)(O)O)N>>O=C(N)NC[CH2]:1.0, O=C(OP(=O)(O)O)N>>O=P(O)(O)O:2.0, O=C(O[P])N:1.0, O=C(O[P])N>>O=C(N)N[CH2]:1.0, O=C([NH])N:1.0, O=P(O)(O)O:1.0, [CH2]CCN>>O=C(N)NCC[CH2]:1.0, [CH2]CN:1.0, [CH2]CN>>O=C(N)NC[CH2]:1.0, [CH2]N:1.0, [CH2]N>>[C]N[CH2]:1.0, [CH]:2.0, [C]:2.0, [C]C([NH])[CH2]:2.0, [C]C([NH])[CH2]>>[C]C([NH])[CH2]:1.0, [C]NC(C(=O)O)C[CH2]:2.0, [C]NC(C(=O)O)C[CH2]>>[C]NC(C(=O)O)C[CH2]:1.0, [C]N[CH2]:1.0, [C]O[P]:1.0, [C]O[P]>>[P]O:1.0, [NH2]:1.0, [NH]:1.0, [OH]:1.0, [O]:1.0, [O]C(=O)N:1.0, [O]C(=O)N>>O=C([NH])N:1.0, [P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [NH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([NH])N:1.0, [C]N[CH2]:1.0, [C]O[P]:1.0, [O]C(=O)N:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(N)NC[CH2]:1.0, O=C(N)N[CH2]:1.0, O=C(OP(=O)(O)O)N:1.0, O=C(O[P])N:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (1)
[[C]C([NH])[CH2]:2.0]


ID=Reaction Center at Level: 2 (1)
[[C]NC(C(=O)O)C[CH2]:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C([NH])[CH2]>>[C]C([NH])[CH2]
2: [CH2]N>>[C]N[CH2]
3: [C]O[P]>>[P]O
4: [O]C(=O)N>>O=C([NH])N

MMP Level 2
1: [C]NC(C(=O)O)C[CH2]>>[C]NC(C(=O)O)C[CH2]
2: [CH2]CN>>O=C(N)NC[CH2]
3: O=C(OP(=O)(O)O)N>>O=P(O)(O)O
4: O=C(O[P])N>>O=C(N)N[CH2]

MMP Level 3
1: O=C(O)C(NC(=O)C)CC[CH2]>>O=C(O)C(NC(=O)C)CC[CH2]
2: [CH2]CCN>>O=C(N)NCC[CH2]
3: O=C(OP(=O)(O)O)N>>O=P(O)(O)O
4: O=C(OP(=O)(O)O)N>>O=C(N)NC[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(OP(=O)(O)O)N>>O=P(O)(O)O]
2: R:M00001, P:M00004	[O=C(OP(=O)(O)O)N>>O=C(N)NCCCC(NC(=O)C)C(=O)O]
3: R:M00002, P:M00004	[O=C(O)C(NC(=O)C)CCCN>>O=C(N)NCCCC(NC(=O)C)C(=O)O, O=C(O)C(NC(=O)C)CCCN>>O=C(N)NCCCC(NC(=O)C)C(=O)O]


//
SELECTED AAM MAPPING
[O:13]=[C:14]([O:15][P:16](=[O:17])([OH:18])[OH:19])[NH2:20].[O:1]=[C:2]([OH:3])[CH:4]([NH:5][C:6](=[O:7])[CH3:8])[CH2:9][CH2:10][CH2:11][NH2:12]>>[O:13]=[C:14]([NH2:20])[NH:12][CH2:11][CH2:10][CH2:9][CH:4]([NH:5][C:6](=[O:7])[CH3:8])[C:2](=[O:1])[OH:3].[O:17]=[P:16]([OH:15])([OH:18])[OH:19]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=14, 2=13, 3=20, 4=15, 5=16, 6=17, 7=18, 8=19, 9=8, 10=6, 11=7, 12=5, 13=4, 14=9, 15=10, 16=11, 17=12, 18=2, 19=1, 20=3}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=18, 2=17, 3=16, 4=19, 5=20, 6=12, 7=10, 8=11, 9=9, 10=8, 11=7, 12=6, 13=5, 14=4, 15=2, 16=1, 17=3, 18=13, 19=14, 20=15}

