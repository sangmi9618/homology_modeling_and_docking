
//
FINGERPRINTS BC

STEREO_CHANGED
[C(R/S):3.0]

//
FINGERPRINTS RC
[[CH]:6.0, [CH]C(O)C(O)C([CH])O:2.0, [CH]C(O)C(O)C([CH])O>>[CH]C(O)C(O)C([CH])O:1.0, [CH]C([CH])O:2.0, [CH]C([CH])O>>[CH]C([CH])O:1.0, [CH]OC(CO)C([CH])O:2.0, [CH]OC(CO)C([CH])O>>[CH]OC(CO)C([CH])O:1.0, [O]C([CH])[CH2]:2.0, [O]C([CH])[CH2]>>[O]C([CH])[CH2]:1.0, [O]C([O])[CH]:2.0, [O]C([O])[CH]>>[O]C([O])[CH]:1.0, [O]C1OC(CO)C(O)C(O)[CH]1>>[O]C1OC(CO)C(O)C(O)[CH]1:1.0, [O]C1OC([CH2])C(O)C(O)C1O>>[O]C1OC([CH2])C(O)C(O)C1O:1.0, [O]P(=O)(O)OC1OC([CH2])[CH]C(O)C1O>>[O]P(=O)(O)OC1OC([CH2])[CH]C(O)C1O:1.0, [P]OC(O[CH])C([CH])O:2.0, [P]OC(O[CH])C([CH])O>>[P]OC(O[CH])C([CH])O:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:6.0]


ID=Reaction Center at Level: 1 (3)
[[CH]C([CH])O:2.0, [O]C([CH])[CH2]:2.0, [O]C([O])[CH]:2.0]


ID=Reaction Center at Level: 2 (3)
[[CH]C(O)C(O)C([CH])O:2.0, [CH]OC(CO)C([CH])O:2.0, [P]OC(O[CH])C([CH])O:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: [O]C([CH])[CH2]>>[O]C([CH])[CH2]
2: [O]C([O])[CH]>>[O]C([O])[CH]
3: [CH]C([CH])O>>[CH]C([CH])O

MMP Level 2
1: [CH]OC(CO)C([CH])O>>[CH]OC(CO)C([CH])O
2: [P]OC(O[CH])C([CH])O>>[P]OC(O[CH])C([CH])O
3: [CH]C(O)C(O)C([CH])O>>[CH]C(O)C(O)C([CH])O

MMP Level 3
1: [O]C1OC(CO)C(O)C(O)[CH]1>>[O]C1OC(CO)C(O)C(O)[CH]1
2: [O]P(=O)(O)OC1OC([CH2])[CH]C(O)C1O>>[O]P(=O)(O)OC1OC([CH2])[CH]C(O)C1O
3: [O]C1OC([CH2])C(O)C(O)C1O>>[O]C1OC([CH2])C(O)C(O)C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OC4OC(CO)C(O)C(O)C4O)C(O)C3O>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OC4OC(CO)C(O)C(O)C4O)C(O)C3O, O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OC4OC(CO)C(O)C(O)C4O)C(O)C3O>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OC4OC(CO)C(O)C(O)C4O)C(O)C3O, O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OC4OC(CO)C(O)C(O)C4O)C(O)C3O>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OC4OC(CO)C(O)C(O)C4O)C(O)C3O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[C:8]1:[N:9]:[CH:10]:[N:11]2[CH:12]3[O:13][CH:14]([CH2:15][O:16][P:17](=[O:18])([OH:19])[O:20][P:21](=[O:22])([OH:23])[O:24][CH:25]4[O:26][CH:27]([CH2:28][OH:29])[CH:30]([OH:31])[CH:32]([OH:33])[CH:34]4[OH:35])[CH:36]([OH:37])[CH:38]3[OH:39]>>[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[C:8]1:[N:9]:[CH:10]:[N:11]2[CH:12]3[O:13][CH:14]([CH2:15][O:16][P:17](=[O:18])([OH:19])[O:20][P:21](=[O:22])([OH:23])[O:24][CH:25]4[O:26][CH:27]([CH2:28][OH:29])[CH:30]([OH:31])[CH:32]([OH:33])[CH:34]4[OH:35])[CH:36]([OH:37])[CH:38]3[OH:39]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=10, 2=9, 3=8, 4=7, 5=11, 6=12, 7=38, 8=36, 9=14, 10=13, 11=15, 12=16, 13=17, 14=18, 15=19, 16=20, 17=21, 18=22, 19=23, 20=24, 21=25, 22=34, 23=32, 24=30, 25=27, 26=26, 27=28, 28=29, 29=31, 30=33, 31=35, 32=37, 33=39, 34=6, 35=4, 36=3, 37=2, 38=1, 39=5}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=10, 2=9, 3=8, 4=7, 5=11, 6=12, 7=38, 8=36, 9=14, 10=13, 11=15, 12=16, 13=17, 14=18, 15=19, 16=20, 17=21, 18=22, 19=23, 20=24, 21=25, 22=34, 23=32, 24=30, 25=27, 26=26, 27=28, 28=29, 29=31, 30=33, 31=35, 32=37, 33=39, 34=6, 35=4, 36=3, 37=2, 38=1, 39=5}

