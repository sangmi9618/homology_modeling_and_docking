
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0]

//
FINGERPRINTS RC
[O=P(O)(O[P])OC(O[CH2])C([CH])O>>O=P(O)(O)O[P]:1.0, [CH]:1.0, [OH]:1.0, [O]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]C([O])[CH]:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]OC(O[CH2])C([CH])O:1.0, [P]O[CH]:1.0, [P]O[CH]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (2)
[[O]C([O])[CH]:1.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[O]C([CH])OP([O])(=O)O:1.0, [P]OC(O[CH2])C([CH])O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[CH]>>[P]O

MMP Level 2
1: [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O

MMP Level 3
1: O=P(O)(O[P])OC(O[CH2])C([CH])O>>O=P(O)(O)O[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OCC(O)C(O)C3O)C(O)C2O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)O)C(O)C2O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][CH:26]3[O:27][CH2:28][CH:29]([OH:30])[CH:31]([OH:32])[CH:33]3[OH:34])[CH:22]([OH:23])[CH:24]2[OH:25]>>[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[OH:21])[CH:22]([OH:23])[CH:24]2[OH:25]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=27, 4=29, 5=22, 6=23, 7=21, 8=18, 9=19, 10=20, 11=17, 12=14, 13=15, 14=16, 15=13, 16=12, 17=11, 18=31, 19=33, 20=9, 21=10, 22=5, 23=4, 24=3, 25=2, 26=1, 27=8, 28=6, 29=7, 30=34, 31=32, 32=30, 33=28, 34=26}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=3, 2=4, 3=5, 4=6, 5=7, 6=8, 7=2, 8=1, 9=9, 10=24, 11=22, 12=11, 13=10, 14=12, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=23, 25=25}

