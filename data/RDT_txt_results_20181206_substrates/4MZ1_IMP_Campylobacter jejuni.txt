
//
FINGERPRINTS BC

FORMED_CLEAVED
[C=O:1.0]

ORDER_CHANGED
[C%N*C%N:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C([NH])[NH]:2.0, O>>O=C([NH])[NH]:1.0, O>>[C]=O:1.0, O>>[C]NC(=O)N[C]:1.0, [CH]:1.0, [C]:1.0, [C]=O:1.0, [C]C(=O)N=C[NH]:1.0, [C]C(=O)N=C[NH]>>[C]C(=O)NC(=O)[NH]:1.0, [C]C(=O)NC(=O)[NH]:1.0, [C]N=CN[C]:1.0, [C]N=CN[C]>>[C]NC(=O)N[C]:1.0, [C]N=[CH]:1.0, [C]N=[CH]>>[C]N[C]:1.0, [C]NC(=O)N[C]:1.0, [C]N[C]:1.0, [NH]:1.0, [N]:1.0, [N]=C[NH]:1.0, [N]=C[NH]>>O=C([NH])[NH]:1.0, [N]C1=[C]C(=O)N=CN1>>[N]C1=[C]C(=O)NC(=O)N1:1.0, [N]C1=[C]NC=NC1=O>>[N]C1=[C]NC(=O)NC1=O:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[O:1.0, [C]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (3)
[O:1.0, O=C([NH])[NH]:1.0, [C]=O:1.0]


ID=Reaction Center at Level: 2 (3)
[O:1.0, O=C([NH])[NH]:1.0, [C]NC(=O)N[C]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH]:1.0, [C]:1.0, [NH]:1.0, [N]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([NH])[NH]:1.0, [C]N=[CH]:1.0, [C]N[C]:1.0, [N]=C[NH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]C(=O)N=C[NH]:1.0, [C]C(=O)NC(=O)[NH]:1.0, [C]N=CN[C]:1.0, [C]NC(=O)N[C]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [N]=C[NH]>>O=C([NH])[NH]
2: O>>[C]=O
3: [C]N=[CH]>>[C]N[C]

MMP Level 2
1: [C]N=CN[C]>>[C]NC(=O)N[C]
2: O>>O=C([NH])[NH]
3: [C]C(=O)N=C[NH]>>[C]C(=O)NC(=O)[NH]

MMP Level 3
1: [N]C1=[C]C(=O)N=CN1>>[N]C1=[C]C(=O)NC(=O)N1
2: O>>[C]NC(=O)N[C]
3: [N]C1=[C]NC=NC1=O>>[N]C1=[C]NC(=O)NC1=O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00005	[O>>O=c1[nH]c(=O)c2ncn(c2[nH]1)C3OC(COP(=O)(O)O)C(O)C3O]
2: R:M00002, P:M00005	[O=c1nc[nH]c2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O>>O=c1[nH]c(=O)c2ncn(c2[nH]1)C3OC(COP(=O)(O)O)C(O)C3O, O=c1nc[nH]c2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O>>O=c1[nH]c(=O)c2ncn(c2[nH]1)C3OC(COP(=O)(O)O)C(O)C3O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[CH:4][NH:5][C:6]:2:[C:7]1:[N:8]:[CH:9]:[N:10]2[CH:11]3[O:12][CH:13]([CH2:14][O:15][P:16](=[O:17])([OH:18])[OH:19])[CH:20]([OH:21])[CH:22]3[OH:23].[OH2:24]>>[H+:69].[O:25]=[C:26]([NH2:27])[C:28]1=[CH:29][N:30]([CH:31]=[CH:32][CH2:33]1)[CH:34]2[O:35][CH:36]([CH2:37][O:38][P:39](=[O:40])([OH:41])[O:42][P:43](=[O:44])([OH:45])[O:46][CH2:47][CH:48]3[O:49][CH:50]([N:51]:4:[CH:52]:[N:53]:[C:54]:5:[C:55](:[N:56]:[CH:57]:[N:58]:[C:59]54)[NH2:60])[CH:61]([OH:62])[CH:63]3[OH:64])[CH:65]([OH:66])[CH:67]2[OH:68].[O:24]=[C:4]1[NH:3][C:2](=[O:1])[C:7]:2:[N:8]:[CH:9]:[N:10](:[C:6]2[NH:5]1)[CH:11]3[O:12][CH:13]([CH2:14][O:15][P:16](=[O:17])([OH:18])[OH:19])[CH:20]([OH:21])[CH:22]3[OH:23]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=4, 3=3, 4=2, 5=1, 6=7, 7=6, 8=5, 9=10, 10=9, 11=8, 12=11, 13=22, 14=20, 15=13, 16=12, 17=14, 18=15, 19=16, 20=17, 21=18, 22=19, 23=21, 24=23}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=5, 6=4, 7=2, 8=1, 9=3, 10=10, 11=43, 12=41, 13=12, 14=11, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=39, 28=37, 29=26, 30=25, 31=27, 32=28, 33=29, 34=30, 35=35, 36=34, 37=33, 38=32, 39=31, 40=36, 41=38, 42=40, 43=42, 44=44, 45=69, 46=52, 47=51, 48=50, 49=54, 50=53, 51=56, 52=67, 53=65, 54=58, 55=57, 56=59, 57=60, 58=61, 59=62, 60=63, 61=64, 62=66, 63=68, 64=55, 65=46, 66=45, 67=47, 68=48, 69=49}

