
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

//
FINGERPRINTS RC
[O=C(O)C[CH2]:1.0, O=C(O)C[CH2]>>O=C=O:2.0, O=C(O)[CH2]:2.0, O=C(O)[CH2]>>O=C=O:2.0, O=C=O:3.0, [CH2]:1.0, [C]:2.0, [C]=O:1.0, [C]CCC(=O)O:1.0, [C]CCC(=O)O>>O=C=O:1.0, [C]C[CH2]:1.0, [C]O:1.0, [C]O>>[C]=O:1.0, [OH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH2]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[O=C(O)[CH2]:1.0, [C]C[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C[CH2]:1.0, [C]CCC(=O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C(O)[CH2]:1.0, O=C=O:1.0, [C]=O:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (3)
[O=C(O)C[CH2]:1.0, O=C(O)[CH2]:1.0, O=C=O:2.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=C(O)[CH2]>>O=C=O
2: [C]O>>[C]=O

MMP Level 2
1: O=C(O)C[CH2]>>O=C=O
2: O=C(O)[CH2]>>O=C=O

MMP Level 3
1: [C]CCC(=O)O>>O=C=O
2: O=C(O)C[CH2]>>O=C=O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)CCc1c2[nH]c(c1C)Cc3[nH]c(c(c3CCC(=O)O)C)Cc4[nH]c(c(c4CCC(=O)O)C)Cc5[nH]c(c(c5CCC(=O)O)C)C2>>O=C=O, O=C(O)CCc1c2[nH]c(c1C)Cc3[nH]c(c(c3CCC(=O)O)C)Cc4[nH]c(c(c4CCC(=O)O)C)Cc5[nH]c(c(c5CCC(=O)O)C)C2>>O=C=O]


//
SELECTED AAM MAPPING
[O:33]=[C:32]([OH:34])[CH2:31][CH2:30][C:29]:1:[C:25]2:[NH:26]:[C:27](:[C:28]1[CH3:35])[CH2:36][C:37]:3:[NH:38]:[C:39](:[C:40](:[C:41]3[CH2:42][CH2:43][C:44](=[O:45])[OH:46])[CH3:47])[CH2:48][C:7]:4:[NH:8]:[C:9](:[C:10](:[C:6]4[CH2:5][CH2:4][C:2](=[O:1])[OH:3])[CH3:11])[CH2:12][C:13]:5:[NH:14]:[C:15](:[C:16](:[C:17]5[CH2:18][CH2:19][C:20](=[O:21])[OH:22])[CH3:23])[CH2:24]2>>[O:1]=[C:2]=[O:3]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=11, 2=10, 3=9, 4=12, 5=13, 6=17, 7=16, 8=15, 9=14, 10=24, 11=25, 12=29, 13=28, 14=27, 15=26, 16=36, 17=37, 18=41, 19=40, 20=39, 21=38, 22=48, 23=7, 24=6, 25=5, 26=4, 27=2, 28=1, 29=3, 30=8, 31=47, 32=42, 33=43, 34=44, 35=45, 36=46, 37=35, 38=30, 39=31, 40=32, 41=33, 42=34, 43=23, 44=18, 45=19, 46=20, 47=21, 48=22}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=2, 2=1, 3=3}

