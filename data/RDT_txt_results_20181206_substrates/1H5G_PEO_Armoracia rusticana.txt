
//
FINGERPRINTS BC

FORMED_CLEAVED
[C=O:1.0, O-O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C([CH])[CH]:1.0, OO:4.0, OO>>O:3.0, OO>>O=C([CH])[CH]:1.0, OO>>[C]=CC(=O)C=[CH]:1.0, OO>>[C]=O:1.0, [C]=O:1.0, [OH]:2.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [C]:1.0, [OH]:2.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O:1.0, O=C([CH])[CH]:1.0, OO:2.0, [C]=O:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=C([CH])[CH]:1.0, OO:2.0, [C]=CC(=O)C=[CH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: OO>>O
2: OO>>[C]=O

MMP Level 2
1: OO>>O
2: OO>>O=C([CH])[CH]

MMP Level 3
1: OO>>O
2: OO>>[C]=CC(=O)C=[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[OO>>O=C1C=CC(C(OC)=C1)=C2C=CC(=O)C=C2OC]
2: R:M00001, P:M00003	[OO>>O]


//
SELECTED AAM MAPPING
[OH:1][OH:2]>>[O:2]=[C:3]1[CH:4]=[CH:5][C:6]([C:7]([O:8][CH3:9])=[CH:10]1)=[C:11]2[CH:12]=[CH:13][C:14](=[O:15])[CH:16]=[C:17]2[O:18][CH3:19].[OH2:1]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=1, 2=2}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=8, 2=7, 3=6, 4=9, 5=2, 6=1, 7=3, 8=4, 9=5, 10=10, 11=11, 12=12, 13=13, 14=14, 15=15, 16=16, 17=17, 18=18, 19=19}

