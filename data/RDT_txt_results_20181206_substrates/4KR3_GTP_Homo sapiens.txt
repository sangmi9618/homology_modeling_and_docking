
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O:3.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[CH2]:1.0, O=P(O)(O[P])O[CH2]>>O=P(O)(O)O[CH2]:1.0, O>>O=P(O)(O)O[CH2]:1.0, O>>[O]P(=O)(O)O:1.0, O>>[P]O:1.0, [OH]:2.0, [O]:1.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)OP(=O)(O)OC[CH]>>O=P(O)(O)OC[CH]:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [O]P([O])(=O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, [O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O[P])O[CH2]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[P]>>[P]O
2: [O]P([O])(=O)O>>[O]P(=O)(O)O
3: O>>[P]O

MMP Level 2
1: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
2: O=P(O)(O[P])O[CH2]>>O=P(O)(O)O[CH2]
3: O>>[O]P(=O)(O)O

MMP Level 3
1: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]
2: [O]P(=O)(O)OP(=O)(O)OC[CH]>>O=P(O)(O)OC[CH]
3: O>>O=P(O)(O)O[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(=O)nc(N)[nH]c65)C(O)C4O)C(O)C3O>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C3O]
2: R:M00001, P:M00004	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(=O)nc(N)[nH]c65)C(O)C4O)C(O)C3O>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O]
3: R:M00002, P:M00004	[O>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O]


//
SELECTED AAM MAPPING
[O:42]=[C:41]1[N:43]=[C:44]([NH2:45])[NH:46][C:47]:2:[C:40]1:[N:39]:[CH:38]:[N:37]2[CH:36]3[O:35][CH:34]([CH2:33][O:32][P:29](=[O:30])([OH:31])[O:28][P:25](=[O:26])([OH:27])[O:24][P:21](=[O:22])([OH:23])[O:20][P:17](=[O:18])([OH:19])[O:16][CH2:15][CH:14]4[O:13][CH:12]([N:11]:5:[CH:10]:[N:9]:[C:8]:6[C:2](=[O:1])[N:3]=[C:4]([NH2:5])[NH:6][C:7]65)[CH:54]([OH:55])[CH:52]4[OH:53])[CH:50]([OH:51])[CH:48]3[OH:49].[OH2:56]>>[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[C:8]1:[N:9]:[CH:10]:[N:11]2[CH:12]3[O:13][CH:14]([CH2:15][O:16][P:17](=[O:18])([OH:56])[OH:19])[CH:52]([OH:53])[CH:54]3[OH:55].[O:42]=[C:41]1[N:43]=[C:44]([NH2:45])[NH:46][C:47]:2:[C:40]1:[N:39]:[CH:38]:[N:37]2[CH:36]3[O:35][CH:34]([CH2:33][O:32][P:29](=[O:30])([OH:31])[O:28][P:25](=[O:26])([OH:27])[O:24][P:21](=[O:22])([OH:20])[OH:23])[CH:50]([OH:51])[CH:48]3[OH:49]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=38, 2=39, 3=40, 4=47, 5=37, 6=36, 7=48, 8=50, 9=34, 10=35, 11=33, 12=32, 13=29, 14=30, 15=31, 16=28, 17=25, 18=26, 19=27, 20=24, 21=21, 22=22, 23=23, 24=20, 25=17, 26=18, 27=19, 28=16, 29=15, 30=14, 31=52, 32=54, 33=12, 34=13, 35=11, 36=10, 37=9, 38=8, 39=7, 40=6, 41=4, 42=3, 43=2, 44=1, 45=5, 46=55, 47=53, 48=51, 49=49, 50=46, 51=44, 52=43, 53=41, 54=42, 55=45, 56=56}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=10, 2=9, 3=8, 4=7, 5=11, 6=12, 7=31, 8=29, 9=14, 10=13, 11=15, 12=16, 13=17, 14=18, 15=19, 16=20, 17=21, 18=22, 19=23, 20=24, 21=25, 22=26, 23=27, 24=28, 25=30, 26=32, 27=6, 28=4, 29=3, 30=2, 31=1, 32=5, 33=42, 34=41, 35=40, 36=39, 37=43, 38=44, 39=55, 40=53, 41=46, 42=45, 43=47, 44=48, 45=49, 46=50, 47=51, 48=52, 49=54, 50=56, 51=38, 52=36, 53=35, 54=34, 55=33, 56=37}

