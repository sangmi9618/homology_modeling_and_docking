
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(O)C(O)CC(=O)S[CH2]>>O=C(S[CH2])C:1.0, O=C([S])C:1.0, O=C([S])CC(O)C(=O)O>>O=CC(=O)O:1.0, O=CC(=O)O:1.0, O=[CH]:1.0, [CH2]:1.0, [CH3]:1.0, [CH]:2.0, [CH]O:1.0, [CH]O>>O=[CH]:1.0, [C]C:1.0, [C]C(O)CC(=O)[S]:1.0, [C]C(O)CC(=O)[S]>>O=C([S])C:1.0, [C]C(O)[CH2]:2.0, [C]C(O)[CH2]>>[C]C=O:2.0, [C]C=O:2.0, [C]CC(O)C(=O)O:1.0, [C]CC(O)C(=O)O>>O=CC(=O)O:2.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C:1.0, [OH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH2]:1.0, [CH]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(O)[CH2]:1.0, [C]C[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[C]C(O)CC(=O)[S]:1.0, [C]CC(O)C(=O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=[CH]:1.0, [CH]O:1.0, [C]C(O)[CH2]:1.0, [C]C=O:1.0]


ID=Reaction Center at Level: 2 (4)
[O=CC(=O)O:1.0, [C]C(O)[CH2]:1.0, [C]C=O:1.0, [C]CC(O)C(=O)O:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(O)[CH2]:1.0, [C]C=O:1.0]


ID=Reaction Center at Level: 2 (2)
[O=CC(=O)O:1.0, [C]CC(O)C(=O)O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C(O)[CH2]>>[C]C=O
2: [C]C[CH]>>[C]C
3: [CH]O>>O=[CH]

MMP Level 2
1: [C]CC(O)C(=O)O>>O=CC(=O)O
2: [C]C(O)CC(=O)[S]>>O=C([S])C
3: [C]C(O)[CH2]>>[C]C=O

MMP Level 3
1: O=C([S])CC(O)C(=O)O>>O=CC(=O)O
2: O=C(O)C(O)CC(=O)S[CH2]>>O=C(S[CH2])C
3: [C]CC(O)C(=O)O>>O=CC(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)C(O)CC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O>>O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C]
2: R:M00001, P:M00003	[O=C(O)C(O)CC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O>>O=CC(=O)O, O=C(O)C(O)CC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O>>O=CC(=O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([OH:5])[CH2:6][C:7](=[O:8])[S:9][CH2:10][CH2:11][NH:12][C:13](=[O:14])[CH2:15][CH2:16][NH:17][C:18](=[O:19])[CH:20]([OH:21])[C:22]([CH3:23])([CH3:24])[CH2:25][O:26][P:27](=[O:28])([OH:29])[O:30][P:31](=[O:32])([OH:33])[O:34][CH2:35][CH:36]1[O:37][CH:38]([N:39]:2:[CH:40]:[N:41]:[C:42]:3:[C:43](:[N:44]:[CH:45]:[N:46]:[C:47]32)[NH2:48])[CH:49]([OH:50])[CH:51]1[O:52][P:53](=[O:54])([OH:55])[OH:56]>>[O:5]=[CH:4][C:2](=[O:1])[OH:3].[O:8]=[C:7]([S:9][CH2:10][CH2:11][NH:12][C:13](=[O:14])[CH2:15][CH2:16][NH:17][C:18](=[O:19])[CH:20]([OH:21])[C:22]([CH3:23])([CH3:24])[CH2:25][O:26][P:27](=[O:28])([OH:29])[O:30][P:31](=[O:32])([OH:33])[O:34][CH2:35][CH:36]1[O:37][CH:38]([N:39]:2:[CH:40]:[N:41]:[C:42]:3:[C:43](:[N:44]:[CH:45]:[N:46]:[C:47]32)[NH2:48])[CH:49]([OH:50])[CH:51]1[O:52][P:53](=[O:54])([OH:55])[OH:56])[CH3:6]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=23, 2=22, 3=24, 4=25, 5=26, 6=27, 7=28, 8=29, 9=30, 10=31, 11=32, 12=33, 13=34, 14=35, 15=36, 16=51, 17=49, 18=38, 19=37, 20=39, 21=40, 22=41, 23=42, 24=47, 25=46, 26=45, 27=44, 28=43, 29=48, 30=50, 31=52, 32=53, 33=54, 34=55, 35=56, 36=20, 37=18, 38=19, 39=17, 40=16, 41=15, 42=13, 43=14, 44=12, 45=11, 46=10, 47=9, 48=7, 49=8, 50=6, 51=4, 52=2, 53=1, 54=3, 55=5, 56=21}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=51, 2=2, 3=1, 4=3, 5=4, 6=5, 7=6, 8=7, 9=8, 10=9, 11=10, 12=11, 13=12, 14=13, 15=14, 16=16, 17=17, 18=18, 19=19, 20=20, 21=21, 22=22, 23=23, 24=24, 25=25, 26=26, 27=27, 28=28, 29=29, 30=30, 31=45, 32=43, 33=32, 34=31, 35=33, 36=34, 37=35, 38=36, 39=41, 40=40, 41=39, 42=38, 43=37, 44=42, 45=44, 46=46, 47=47, 48=48, 49=49, 50=50, 51=15, 52=53, 53=52, 54=54, 55=55, 56=56}

