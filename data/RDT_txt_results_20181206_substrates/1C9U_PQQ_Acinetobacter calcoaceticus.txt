
//
FINGERPRINTS BC

ORDER_CHANGED
[C%C*C%C:1.0, C-O*C=O:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(O[CH])C[CH]:1.0, OC1OC([CH2])[CH]C(O)C1>>O=C1OC([CH2])[CH]C(O)C1:1.0, [CH]:1.0, [CH]O:1.0, [CH]O>>[C]=O:1.0, [CH]OC(O)C[CH]:1.0, [CH]OC(O)C[CH]>>O=C(O[CH])C[CH]:2.0, [C]:5.0, [C]=O:2.0, [C]=O>>[C]O:1.0, [C]C(=O)C(=O)C(=[C])[N]:1.0, [C]C(=O)C(=O)C(=[C])[N]>>[C]C(=O)C(O)=C([C])[NH]:2.0, [C]C(=O)C(O)=C([C])[NH]:1.0, [C]C(=[C])N=C([C])[CH]:1.0, [C]C(=[C])N=C([C])[CH]>>[C]C(=[C])NC([C])=[CH]:1.0, [C]C(=[C])NC([C])=[CH]:1.0, [C]C(=[C])O:2.0, [C]C(=[C])[NH]:1.0, [C]C(=[C])[N]:1.0, [C]C(=[C])[N]>>[C]C(=[C])[NH]:1.0, [C]C([C])=C(N=[C])C([C])=O:1.0, [C]C([C])=C(N=[C])C([C])=O>>[C]NC(C([C])=[C])=C([C])O:1.0, [C]C([C])=O:2.0, [C]C([C])=O>>[C]C(=[C])O:2.0, [C]C1=[C]C=C(N=C1C([C])=O)C(=O)O>>[C]C1=[C]C=C(NC1=C([C])O)C(=O)O:1.0, [C]C=1[C]=C([CH])C(=O)C(=O)C1N=[C]>>[C]NC=1C(=[C])[C]=C([CH])C(=O)C1O:1.0, [C]N=[C]:1.0, [C]N=[C]>>[C]N[C]:1.0, [C]NC(C([C])=[C])=C([C])O:1.0, [C]N[C]:1.0, [C]O:1.0, [C]c1nc2C(=O)C(=O)[C]=C([NH])c2c([C])c1>>[C]C1=CC([C])=C2C([NH])=[C]C(=O)C(O)=C2N1:1.0, [NH]:1.0, [N]:1.0, [OH]:2.0, [O]:2.0, [O]C(=O)[CH2]:2.0, [O]C(O)[CH2]:2.0, [O]C(O)[CH2]>>[O]C(=O)[CH2]:2.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH]:1.0, [C]:5.0, [OH]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (9)
[[CH]O:1.0, [C]=O:2.0, [C]C(=[C])O:1.0, [C]C(=[C])[NH]:1.0, [C]C(=[C])[N]:1.0, [C]C([C])=O:1.0, [C]O:1.0, [O]C(=O)[CH2]:1.0, [O]C(O)[CH2]:1.0]


ID=Reaction Center at Level: 2 (10)
[O=C(O[CH])C[CH]:1.0, [CH]OC(O)C[CH]:1.0, [C]C(=O)C(=O)C(=[C])[N]:1.0, [C]C(=O)C(O)=C([C])[NH]:1.0, [C]C(=[C])O:1.0, [C]C([C])=C(N=[C])C([C])=O:1.0, [C]C([C])=O:1.0, [C]NC(C([C])=[C])=C([C])O:1.0, [O]C(=O)[CH2]:1.0, [O]C(O)[CH2]:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[O]C(=O)[CH2]:1.0, [O]C(O)[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O[CH])C[CH]:1.0, [CH]OC(O)C[CH]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]=O>>[C]O
2: [C]C([C])=O>>[C]C(=[C])O
3: [CH]O>>[C]=O
4: [C]N=[C]>>[C]N[C]
5: [O]C(O)[CH2]>>[O]C(=O)[CH2]
6: [C]C(=[C])[N]>>[C]C(=[C])[NH]

MMP Level 2
1: [C]C([C])=O>>[C]C(=[C])O
2: [C]C(=O)C(=O)C(=[C])[N]>>[C]C(=O)C(O)=C([C])[NH]
3: [O]C(O)[CH2]>>[O]C(=O)[CH2]
4: [C]C(=[C])N=C([C])[CH]>>[C]C(=[C])NC([C])=[CH]
5: [CH]OC(O)C[CH]>>O=C(O[CH])C[CH]
6: [C]C([C])=C(N=[C])C([C])=O>>[C]NC(C([C])=[C])=C([C])O

MMP Level 3
1: [C]C(=O)C(=O)C(=[C])[N]>>[C]C(=O)C(O)=C([C])[NH]
2: [C]C=1[C]=C([CH])C(=O)C(=O)C1N=[C]>>[C]NC=1C(=[C])[C]=C([CH])C(=O)C1O
3: [CH]OC(O)C[CH]>>O=C(O[CH])C[CH]
4: [C]C1=[C]C=C(N=C1C([C])=O)C(=O)O>>[C]C1=[C]C=C(NC1=C([C])O)C(=O)O
5: OC1OC([CH2])[CH]C(O)C1>>O=C1OC([CH2])[CH]C(O)C1
6: [C]c1nc2C(=O)C(=O)[C]=C([NH])c2c([C])c1>>[C]C1=CC([C])=C2C([NH])=[C]C(=O)C(O)=C2N1


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[OCC1OC(O)CC(O)C1O>>O=C1OC(CO)C(O)C(O)C1, OCC1OC(O)CC(O)C1O>>O=C1OC(CO)C(O)C(O)C1]
2: R:M00002, P:M00004	[O=C1C(=O)c2cc([nH]c2-c3c1nc(cc3C(=O)O)C(=O)O)C(=O)O>>O=c1c(O)c-2[nH]c(cc(c2c3[nH]c(cc13)C(=O)O)C(=O)O)C(=O)O, O=C1C(=O)c2cc([nH]c2-c3c1nc(cc3C(=O)O)C(=O)O)C(=O)O>>O=c1c(O)c-2[nH]c(cc(c2c3[nH]c(cc13)C(=O)O)C(=O)O)C(=O)O, O=C1C(=O)c2cc([nH]c2-c3c1nc(cc3C(=O)O)C(=O)O)C(=O)O>>O=c1c(O)c-2[nH]c(cc(c2c3[nH]c(cc13)C(=O)O)C(=O)O)C(=O)O, O=C1C(=O)c2cc([nH]c2-c3c1nc(cc3C(=O)O)C(=O)O)C(=O)O>>O=c1c(O)c-2[nH]c(cc(c2c3[nH]c(cc13)C(=O)O)C(=O)O)C(=O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[C:4]:1:[N:5]:[C:6]:2[C:7](=[O:8])[C:9](=[O:10])[C:11]:3:[CH:12]:[C:13](:[NH:14]:[C:15]3[C:16]2:[C:17](:[CH:18]1)[C:19](=[O:20])[OH:21])[C:22](=[O:23])[OH:24].[OH:25][CH2:26][CH:27]1[O:28][CH:29]([OH:30])[CH2:31][CH:32]([OH:33])[CH:34]1[OH:35]>>[O:1]=[C:2]([OH:3])[C:4]1=[CH:18][C:17]([C:19](=[O:20])[OH:21])=[C:16]2[C:6]([NH:5]1)=[C:7]([OH:8])[C:9](=[O:10])[C:11]:3:[CH:12]:[C:13](:[NH:14]:[C:15]32)[C:22](=[O:23])[OH:24].[O:30]=[C:29]1[O:28][CH:27]([CH2:26][OH:25])[CH:34]([OH:35])[CH:32]([OH:33])[CH2:31]1


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=31, 2=32, 3=34, 4=27, 5=28, 6=29, 7=30, 8=26, 9=25, 10=35, 11=33, 12=18, 13=17, 14=16, 15=6, 16=7, 17=8, 18=9, 19=10, 20=11, 21=15, 22=14, 23=13, 24=12, 25=22, 26=23, 27=24, 28=5, 29=4, 30=2, 31=1, 32=3, 33=19, 34=20, 35=21}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=35, 2=33, 3=31, 4=28, 5=27, 6=26, 7=25, 8=29, 9=30, 10=32, 11=34, 12=18, 13=19, 14=20, 15=21, 16=17, 17=15, 18=16, 19=13, 20=11, 21=10, 22=6, 23=5, 24=4, 25=12, 26=2, 27=1, 28=3, 29=7, 30=8, 31=9, 32=14, 33=22, 34=23, 35=24}

