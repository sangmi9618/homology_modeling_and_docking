
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%O:1.0, C-S:2.0, C=O:1.0]

ORDER_CHANGED
[C%C*C%C:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C([CH])[CH]:2.0, O=C([CH])[CH]>>[C]=C([CH])S:1.0, O=C1C([NH])[CH]OC2COP(=O)(O)OC12>>[O]CC1O[CH]C([NH])C(S)=C1S:1.0, S:6.0, S>>[CH]C(S)=C(S)C([CH])[NH]:1.0, S>>[C]=C([CH])S:2.0, S>>[C]S:2.0, S>>[O]C([CH2])C(S)=C([CH])S:1.0, [CH]:1.0, [CH]C(S)=C(S)C([CH])[NH]:1.0, [C]:3.0, [C]=C([CH])S:4.0, [C]=O:1.0, [C]C([CH])OP([O])(=O)O:1.0, [C]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [C]C([O])[CH]:1.0, [C]C([O])[CH]>>[C]=C([CH])S:1.0, [C]NC1C(=O)C(O[P])C(OC1[NH])[CH2]>>[C]NC1C(S)=C(S)C(OC1[NH])[CH2]:1.0, [C]S:2.0, [OH]:1.0, [O]:2.0, [O]C([CH2])C(O[P])C(=O)[CH]:1.0, [O]C([CH2])C(O[P])C(=O)[CH]>>[O]C([CH2])C(S)=C([CH])S:1.0, [O]C([CH2])C(S)=C([CH])S:1.0, [O]C([CH])C(=O)C([CH])[NH]:1.0, [O]C([CH])C(=O)C([CH])[NH]>>[CH]C(S)=C(S)C([CH])[NH]:1.0, [O]C1COP(=O)(O)OC1C(=O)[CH]>>O=P(O)(O)O[CH2]:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[CH]:1.0, [P]O[CH]>>[P]O:1.0, [SH]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[S:2.0, [CH]:1.0, [C]:3.0, [O]:2.0, [SH]:2.0]


ID=Reaction Center at Level: 1 (7)
[O=C([CH])[CH]:1.0, S:2.0, [C]=C([CH])S:2.0, [C]=O:1.0, [C]C([O])[CH]:1.0, [C]S:2.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (8)
[O=C([CH])[CH]:1.0, S:2.0, [CH]C(S)=C(S)C([CH])[NH]:1.0, [C]=C([CH])S:2.0, [C]C([CH])OP([O])(=O)O:1.0, [O]C([CH2])C(O[P])C(=O)[CH]:1.0, [O]C([CH2])C(S)=C([CH])S:1.0, [O]C([CH])C(=O)C([CH])[NH]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:3.0]


ID=Reaction Center at Level: 1 (3)
[O=C([CH])[CH]:1.0, [C]=C([CH])S:2.0, [C]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[CH]C(S)=C(S)C([CH])[NH]:1.0, [O]C([CH2])C(O[P])C(=O)[CH]:1.0, [O]C([CH2])C(S)=C([CH])S:1.0, [O]C([CH])C(=O)C([CH])[NH]:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]=C([CH])S:1.0, [C]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[O]C([CH2])C(O[P])C(=O)[CH]:1.0, [O]C([CH2])C(S)=C([CH])S:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: S>>[C]S
2: O=C([CH])[CH]>>[C]=C([CH])S
3: [C]C([O])[CH]>>[C]=C([CH])S
4: [P]O[CH]>>[P]O
5: S>>[C]S

MMP Level 2
1: S>>[C]=C([CH])S
2: [O]C([CH])C(=O)C([CH])[NH]>>[CH]C(S)=C(S)C([CH])[NH]
3: [O]C([CH2])C(O[P])C(=O)[CH]>>[O]C([CH2])C(S)=C([CH])S
4: [C]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O
5: S>>[C]=C([CH])S

MMP Level 3
1: S>>[CH]C(S)=C(S)C([CH])[NH]
2: [C]NC1C(=O)C(O[P])C(OC1[NH])[CH2]>>[C]NC1C(S)=C(S)C(OC1[NH])[CH2]
3: O=C1C([NH])[CH]OC2COP(=O)(O)OC12>>[O]CC1O[CH]C([NH])C(S)=C1S
4: [O]C1COP(=O)(O)OC1C(=O)[CH]>>O=P(O)(O)O[CH2]
5: S>>[O]C([CH2])C(S)=C([CH])S


//
REACTION MMP (RPAIR)
1: R:M00002, P:M00004	[O=c1nc(N)[nH]c2NC3OC4COP(=O)(O)OC4C(=O)C3Nc12>>O=c1nc(N)[nH]c2NC3OC(C(S)=C(S)C3Nc12)COP(=O)(O)O, O=c1nc(N)[nH]c2NC3OC4COP(=O)(O)OC4C(=O)C3Nc12>>O=c1nc(N)[nH]c2NC3OC(C(S)=C(S)C3Nc12)COP(=O)(O)O, O=c1nc(N)[nH]c2NC3OC4COP(=O)(O)OC4C(=O)C3Nc12>>O=c1nc(N)[nH]c2NC3OC(C(S)=C(S)C3Nc12)COP(=O)(O)O]
2: R:M00003, P:M00004	[S>>O=c1nc(N)[nH]c2NC3OC(C(S)=C(S)C3Nc12)COP(=O)(O)O, S>>O=c1nc(N)[nH]c2NC3OC(C(S)=C(S)C3Nc12)COP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]=2[NH:8][CH:9]3[O:10][CH:11]4[CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][CH:18]4[C:19](=[O:25])[CH:20]3[NH:21][C:22]12.[OH2:26].[SH2:23].[SH2:24]>>[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]=2[NH:8][CH:9]3[O:10][CH:11]([C:18]([SH:23])=[C:19]([SH:24])[CH:20]3[NH:21][C:22]12)[CH2:12][O:13][P:14](=[O:15])([OH:17])[OH:16]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=26, 2=12, 3=11, 4=18, 5=19, 6=20, 7=21, 8=9, 9=10, 10=8, 11=7, 12=23, 13=22, 14=2, 15=1, 16=3, 17=4, 18=6, 19=5, 20=17, 21=14, 22=15, 23=13, 24=16, 25=25}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=19, 2=11, 3=12, 4=14, 5=16, 6=9, 7=10, 8=8, 9=7, 10=18, 11=17, 12=2, 13=1, 14=3, 15=4, 16=6, 17=5, 18=15, 19=13, 20=20, 21=21, 22=22, 23=23, 24=24}

