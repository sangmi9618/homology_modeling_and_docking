
//
FINGERPRINTS BC

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[OCC1O[CH]C(O)C(O)C1O>>OCC1O[CH]C(O)C(O)C1O:2.0, [CH]:4.0, [CH]C([CH])O:4.0, [CH]C([CH])O>>[CH]C([CH])O:2.0, [O]C([CH2])C(O)C([CH])O:4.0, [O]C([CH2])C(O)C([CH])O>>[O]C([CH2])C(O)C([CH])O:2.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:4.0]


ID=Reaction Center at Level: 1 (1)
[[CH]C([CH])O:4.0]


ID=Reaction Center at Level: 2 (1)
[[O]C([CH2])C(O)C([CH])O:4.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]C([CH])O>>[CH]C([CH])O
2: [CH]C([CH])O>>[CH]C([CH])O

MMP Level 2
1: [O]C([CH2])C(O)C([CH])O>>[O]C([CH2])C(O)C([CH])O
2: [O]C([CH2])C(O)C([CH])O>>[O]C([CH2])C(O)C([CH])O

MMP Level 3
1: OCC1O[CH]C(O)C(O)C1O>>OCC1O[CH]C(O)C(O)C1O
2: OCC1O[CH]C(O)C(O)C1O>>OCC1O[CH]C(O)C(O)C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3O)C(O)C2O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3O)C(O)C2O]
2: R:M00002, P:M00003	[O=P(O)(O)OC1OC(CO)C(O)C(O)C1O>>O=P(O)(O)OC1OC(CO)C(O)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][CH:22]3[O:23][CH:24]([CH2:25][OH:26])[CH:27]([OH:28])[CH:29]([OH:30])[CH:31]3[OH:32])[CH:33]([OH:34])[CH:35]2[OH:36].[O:37]=[P:38]([OH:39])([OH:40])[O:41][CH:42]1[O:43][CH:44]([CH2:45][OH:46])[CH:47]([OH:48])[CH:49]([OH:50])[CH:51]1[OH:52]>>[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][CH:22]3[O:23][CH:24]([CH2:25][OH:26])[CH:27]([OH:28])[CH:29]([OH:30])[CH:31]3[OH:32])[CH:33]([OH:34])[CH:35]2[OH:36].[O:37]=[P:38]([OH:39])([OH:40])[O:41][CH:42]1[O:43][CH:44]([CH2:45][OH:46])[CH:47]([OH:48])[CH:49]([OH:50])[CH:51]1[OH:52]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=3, 2=4, 3=5, 4=6, 5=7, 6=8, 7=2, 8=1, 9=9, 10=35, 11=33, 12=11, 13=10, 14=12, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=31, 26=29, 27=27, 28=24, 29=23, 30=25, 31=26, 32=28, 33=30, 34=32, 35=34, 36=36, 37=45, 38=44, 39=47, 40=49, 41=51, 42=42, 43=43, 44=41, 45=38, 46=37, 47=39, 48=40, 49=52, 50=50, 51=48, 52=46}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=45, 2=44, 3=47, 4=49, 5=51, 6=42, 7=43, 8=41, 9=38, 10=37, 11=39, 12=40, 13=52, 14=50, 15=48, 16=46, 17=3, 18=4, 19=5, 20=6, 21=7, 22=8, 23=2, 24=1, 25=9, 26=35, 27=33, 28=11, 29=10, 30=12, 31=13, 32=14, 33=15, 34=16, 35=17, 36=18, 37=19, 38=20, 39=21, 40=22, 41=31, 42=29, 43=27, 44=24, 45=23, 46=25, 47=26, 48=28, 49=30, 50=32, 51=34, 52=36}

