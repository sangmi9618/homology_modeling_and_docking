
//
FINGERPRINTS BC

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1:1.0, O=C(O)C(=O)C(C)(C)CO>>O=C(O)C(O)C(C)(C)CO:1.0, O=C(O)C(=O)C([CH2])(C)C:1.0, O=C(O)C(=O)C([CH2])(C)C>>O=C(O)C(O)C([CH2])(C)C:2.0, O=C(O)C(O)C([CH2])(C)C:1.0, [CH2]:1.0, [CH]:2.0, [CH]N([CH])[CH]:1.0, [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]:1.0, [CH]O:1.0, [CH][N+]([CH])=[CH]:1.0, [C]:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=O:1.0, [C]=O>>[CH]O:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]:1.0, [C]C([C])=O:2.0, [C]C([C])=O>>[C]C([C])O:2.0, [C]C([C])O:2.0, [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O:1.0, [C]C=[CH]:1.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C=[CH]:1.0, [N+]:1.0, [N]:1.0, [OH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH]:1.0, [C]:1.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[CH]O:1.0, [C]=O:1.0, [C]C([C])=O:1.0, [C]C([C])O:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(O)C(=O)C([CH2])(C)C:1.0, O=C(O)C(O)C([CH2])(C)C:1.0, [C]C([C])=O:1.0, [C]C([C])O:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]C([C])=O:1.0, [C]C([C])O:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C(=O)C([CH2])(C)C:1.0, O=C(O)C(O)C([CH2])(C)C:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C[CH]>>[C]C=[CH]
2: [C]C([C])=O>>[C]C([C])O
3: [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]
4: [C]=O>>[CH]O

MMP Level 2
1: [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]
2: O=C(O)C(=O)C([CH2])(C)C>>O=C(O)C(O)C([CH2])(C)C
3: [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]
4: [C]C([C])=O>>[C]C([C])O

MMP Level 3
1: O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1
2: O=C(O)C(=O)C(C)(C)CO>>O=C(O)C(O)C(C)(C)CO
3: [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O
4: O=C(O)C(=O)C([CH2])(C)C>>O=C(O)C(O)C([CH2])(C)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C(O)C(=O)C(C)(C)CO>>O=C(O)C(O)C(C)(C)CO, O=C(O)C(=O)C(C)(C)CO>>O=C(O)C(O)C(C)(C)CO]
2: R:M00002, P:M00005	[O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O]


//
SELECTED AAM MAPPING
[H+:55].[O:45]=[C:46]([OH:47])[C:48](=[O:49])[C:50]([CH3:51])([CH3:52])[CH2:53][OH:54].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:5][N:6]([CH:7]=[CH:8][CH2:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44]>>[O:45]=[C:46]([OH:47])[CH:48]([OH:49])[C:50]([CH3:51])([CH3:52])[CH2:53][OH:54].[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:9]:[CH:8]:[CH:7]:[N+:6](:[CH:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=51, 2=50, 3=52, 4=53, 5=54, 6=48, 7=49, 8=46, 9=45, 10=47, 11=9, 12=8, 13=7, 14=6, 15=5, 16=4, 17=2, 18=1, 19=3, 20=10, 21=43, 22=41, 23=12, 24=11, 25=13, 26=14, 27=15, 28=16, 29=17, 30=18, 31=19, 32=20, 33=21, 34=22, 35=23, 36=24, 37=39, 38=37, 39=26, 40=25, 41=27, 42=28, 43=29, 44=30, 45=35, 46=34, 47=33, 48=32, 49=31, 50=36, 51=38, 52=40, 53=42, 54=44, 55=55}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=51, 2=50, 3=52, 4=53, 5=54, 6=48, 7=46, 8=45, 9=47, 10=49, 11=6, 12=5, 13=4, 14=9, 15=8, 16=7, 17=10, 18=43, 19=41, 20=12, 21=11, 22=13, 23=14, 24=15, 25=16, 26=17, 27=18, 28=19, 29=20, 30=21, 31=22, 32=23, 33=24, 34=39, 35=37, 36=26, 37=25, 38=27, 39=28, 40=29, 41=30, 42=35, 43=34, 44=33, 45=32, 46=31, 47=36, 48=38, 49=40, 50=42, 51=44, 52=2, 53=1, 54=3}

