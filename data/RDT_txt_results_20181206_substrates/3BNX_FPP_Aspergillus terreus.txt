
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%C:2.0, C-C:2.0, C-O:1.0]

ORDER_CHANGED
[C%C*C=C:1.0, C-C*C=C:2.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[[CH2]:5.0, [CH2]C(=CC[CH2])C:1.0, [CH2]C(=CC[CH2])C>>[C]C(C)CC[CH2]:1.0, [CH2]C([CH2])C(=C)C:1.0, [CH2]C=C(C)C:1.0, [CH2]C=C(C)C>>[CH2]C([CH2])C(=C)C:2.0, [CH2]C=C(C)C[CH2]:2.0, [CH2]C=C(C)C[CH2]>>[CH]C([CH2])(C(=C[CH2])C[CH2])C:1.0, [CH2]C=C(C)C[CH2]>>[CH]CC(C(=[CH])[CH2])(C)C([CH2])C:1.0, [CH2]C=C(C)C[CH2]>>[C]C([CH2])(C)C(C)C[CH2]:1.0, [CH2]CC=C(C)C:1.0, [CH2]CC=C(C)C>>[C]CC(C(=C)C)C[CH]:2.0, [CH3]:3.0, [CH]:4.0, [CH]=C(C)C:2.0, [CH]=C(C)C>>[CH]C(=C)C:2.0, [CH]=C([CH2])C:3.0, [CH]=C([CH2])C>>[C]C(=[CH])[CH2]:1.0, [CH]=C([CH2])C>>[C]C([CH2])C:1.0, [CH]=C([CH2])C>>[C]C([CH])([CH2])C:1.0, [CH]C(=C)C:2.0, [CH]C([CH2])(C(=C[CH2])C[CH2])C:1.0, [CH]CC(C(=[CH])[CH2])(C)C([CH2])C:1.0, [CH]CCC(=CC[CH2])C>>[CH]CC1(C(=[CH])CCCC1C)C:1.0, [CH]CCC(=[CH])C:1.0, [CH]CCC(=[CH])C>>[CH]CC(C(=[CH])[CH2])(C)C([CH2])C:1.0, [CH]C[CH2]:1.0, [CH]C[CH]:1.0, [C]:6.0, [C]=C:1.0, [C]=CCCC(=C[CH2])C>>[C]C1CC=C2CCCC(C)C2(C)C1:1.0, [C]=CCOP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, [C]=CCO[P]:1.0, [C]=CCO[P]>>[C]C([CH2])CC=[C]:1.0, [C]=C[CH2]:2.0, [C]=C[CH2]>>[CH]C[CH2]:1.0, [C]=C[CH2]>>[C]C([CH2])[CH2]:1.0, [C]C:3.0, [C]C(=[CH])[CH2]:1.0, [C]C(C)CC[CH2]:1.0, [C]C([CH2])(C)C(C)C[CH2]:1.0, [C]C([CH2])C:1.0, [C]C([CH2])CC=[C]:1.0, [C]C([CH2])[CH2]:1.0, [C]C([CH])([CH2])C:2.0, [C]C>>[C]=C:1.0, [C]C>>[C]C:1.0, [C]CC(C(=C)C)C[CH]:1.0, [C]CCC=C(C)C>>[CH]C1([C]=CCC(C(=C)C)C1)C:1.0, [C]CCC=C(C)C[CH2]>>[C]1CCCC(C)C1([CH2])C:1.0, [C]C[CH2]:1.0, [C]C[CH2]>>[C]C([CH])([CH2])C:1.0, [OH]:1.0, [O]:1.0, [O]CC=C(C)CC[CH]>>[CH]1CC=C2CCCC(C)C2(C)C1:1.0, [O]C[CH]:1.0, [O]C[CH]>>[CH]C[CH]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OCC=C([CH2])C>>[C]1C([CH2])=CCC(C(=C)C)C1:1.0, [O]P(=O)(O)OC[CH]:1.0, [O]P(=O)(O)OC[CH]>>[O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[CH2]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[CH2]:2.0, [CH3]:2.0, [CH]:1.0, [C]:4.0, [O]:1.0]


ID=Reaction Center at Level: 1 (8)
[[CH]=C([CH2])C:1.0, [CH]C[CH]:1.0, [C]C:2.0, [C]C(=[CH])[CH2]:1.0, [C]C([CH2])[CH2]:1.0, [C]C([CH])([CH2])C:2.0, [O]C[CH]:1.0, [P]O[CH2]:1.0]


ID=Reaction Center at Level: 2 (9)
[[CH2]C=C(C)C[CH2]:1.0, [CH]=C([CH2])C:1.0, [CH]C([CH2])(C(=C[CH2])C[CH2])C:1.0, [CH]CC(C(=[CH])[CH2])(C)C([CH2])C:2.0, [C]=CCO[P]:1.0, [C]C([CH2])CC=[C]:1.0, [C]C([CH])([CH2])C:1.0, [C]CC(C(=C)C)C[CH]:1.0, [O]P(=O)(O)OC[CH]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH2]:2.0, [CH3]:1.0, [CH]:4.0, [C]:3.0]


ID=Reaction Center at Level: 1 (9)
[[CH]=C(C)C:1.0, [CH]=C([CH2])C:1.0, [CH]C(=C)C:1.0, [CH]C[CH2]:1.0, [C]=C:1.0, [C]=C[CH2]:2.0, [C]C:1.0, [C]C([CH2])C:1.0, [C]C([CH2])[CH2]:1.0]


ID=Reaction Center at Level: 2 (10)
[[CH2]C(=CC[CH2])C:1.0, [CH2]C([CH2])C(=C)C:1.0, [CH2]C=C(C)C:1.0, [CH2]C=C(C)C[CH2]:1.0, [CH2]CC=C(C)C:1.0, [CH]=C(C)C:1.0, [CH]C(=C)C:1.0, [C]C(C)CC[CH2]:1.0, [C]C([CH2])(C)C(C)C[CH2]:1.0, [C]CC(C(=C)C)C[CH]:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (3)
[[CH2]:1.0, [CH]:2.0, [C]:1.0]


ID=Reaction Center at Level: 1 (4)
[[C]=C[CH2]:1.0, [C]C([CH2])[CH2]:1.0, [C]C([CH])([CH2])C:1.0, [C]C[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[[CH2]CC=C(C)C:1.0, [CH]CC(C(=[CH])[CH2])(C)C([CH2])C:1.0, [CH]CCC(=[CH])C:1.0, [C]CC(C(=C)C)C[CH]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [O]C[CH]>>[CH]C[CH]
2: [C]=C[CH2]>>[CH]C[CH2]
3: [C]C[CH2]>>[C]C([CH])([CH2])C
4: [C]C>>[C]=C
5: [C]C>>[C]C
6: [CH]=C(C)C>>[CH]C(=C)C
7: [CH]=C([CH2])C>>[C]C(=[CH])[CH2]
8: [C]=C[CH2]>>[C]C([CH2])[CH2]
9: [CH]=C([CH2])C>>[C]C([CH2])C
10: [P]O[CH2]>>[P]O

MMP Level 2
1: [C]=CCO[P]>>[C]C([CH2])CC=[C]
2: [CH2]C(=CC[CH2])C>>[C]C(C)CC[CH2]
3: [CH]CCC(=[CH])C>>[CH]CC(C(=[CH])[CH2])(C)C([CH2])C
4: [CH]=C(C)C>>[CH]C(=C)C
5: [CH]=C([CH2])C>>[C]C([CH])([CH2])C
6: [CH2]C=C(C)C>>[CH2]C([CH2])C(=C)C
7: [CH2]C=C(C)C[CH2]>>[CH]C([CH2])(C(=C[CH2])C[CH2])C
8: [CH2]CC=C(C)C>>[C]CC(C(=C)C)C[CH]
9: [CH2]C=C(C)C[CH2]>>[C]C([CH2])(C)C(C)C[CH2]
10: [O]P(=O)(O)OC[CH]>>[O]P(=O)(O)O

MMP Level 3
1: [O]P(=O)(O)OCC=C([CH2])C>>[C]1C([CH2])=CCC(C(=C)C)C1
2: [C]CCC=C(C)C[CH2]>>[C]1CCCC(C)C1([CH2])C
3: [C]=CCCC(=C[CH2])C>>[C]C1CC=C2CCCC(C)C2(C)C1
4: [CH2]C=C(C)C>>[CH2]C([CH2])C(=C)C
5: [CH2]C=C(C)C[CH2]>>[CH]CC(C(=[CH])[CH2])(C)C([CH2])C
6: [CH2]CC=C(C)C>>[C]CC(C(=C)C)C[CH]
7: [O]CC=C(C)CC[CH]>>[CH]1CC=C2CCCC(C)C2(C)C1
8: [C]CCC=C(C)C>>[CH]C1([C]=CCC(C(=C)C)C1)C
9: [CH]CCC(=CC[CH2])C>>[CH]CC1(C(=[CH])CCCC1C)C
10: [C]=CCOP(=O)(O)O[P]>>O=P(O)(O)O[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=P(O)(O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)C>>C=C(C)C1CC=C2CCCC(C)C2(C)C1, O=P(O)(O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)C>>C=C(C)C1CC=C2CCCC(C)C2(C)C1, O=P(O)(O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)C>>C=C(C)C1CC=C2CCCC(C)C2(C)C1, O=P(O)(O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)C>>C=C(C)C1CC=C2CCCC(C)C2(C)C1, O=P(O)(O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)C>>C=C(C)C1CC=C2CCCC(C)C2(C)C1, O=P(O)(O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)C>>C=C(C)C1CC=C2CCCC(C)C2(C)C1, O=P(O)(O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)C>>C=C(C)C1CC=C2CCCC(C)C2(C)C1, O=P(O)(O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)C>>C=C(C)C1CC=C2CCCC(C)C2(C)C1, O=P(O)(O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)C>>C=C(C)C1CC=C2CCCC(C)C2(C)C1]
2: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)C>>O=P(O)(O)OP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][CH2:10][CH:11]=[C:12]([CH3:13])[CH2:14][CH2:15][CH:16]=[C:17]([CH3:18])[CH2:19][CH2:20][CH:21]=[C:22]([CH3:23])[CH3:24]>>[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[OH:9].[CH2:24]=[C:22]([CH3:23])[CH:21]1[CH2:10][CH:11]=[C:12]2[CH2:14][CH2:15][CH2:16][CH:17]([CH3:18])[C:19]2([CH3:13])[CH2:20]1


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=23, 2=22, 3=21, 4=20, 5=19, 6=17, 7=16, 8=15, 9=14, 10=12, 11=11, 12=10, 13=9, 14=6, 15=7, 16=8, 17=5, 18=2, 19=1, 20=3, 21=4, 22=13, 23=18, 24=24}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=12, 2=11, 3=10, 4=9, 5=8, 6=7, 7=6, 8=5, 9=4, 10=15, 11=13, 12=14, 13=2, 14=1, 15=3, 16=18, 17=17, 18=16, 19=19, 20=20, 21=21, 22=22, 23=23, 24=24}

