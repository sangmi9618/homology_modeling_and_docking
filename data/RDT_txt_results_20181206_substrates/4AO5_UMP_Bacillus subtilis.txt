
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O:3.0, O=P(O)(O)O[P]:1.0, O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]:1.0, O=P(O)(O[P])O[P]:1.0, O=P(O)(O[P])O[P]>>O=P(O)(O)O[P]:1.0, O>>O=P(O)(O)O[P]:1.0, O>>[O]P(=O)(O)O:1.0, O>>[P]O:1.0, [OH]:2.0, [O]:1.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)OP(=O)(O)OP(=O)(O)O>>O=P(O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [O]P([O])(=O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, [O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[P]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[P]>>[P]O
2: [O]P([O])(=O)O>>[O]P(=O)(O)O
3: O>>[P]O

MMP Level 2
1: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
2: O=P(O)(O[P])O[P]>>O=P(O)(O)O[P]
3: O>>[O]P(=O)(O)O

MMP Level 3
1: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]
2: [O]P(=O)(O)OP(=O)(O)OP(=O)(O)O>>O=P(O)(O)OP(=O)(O)O
3: O>>O=P(O)(O)O[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O>>O=P(O)(O)OP(=O)(O)O]
2: R:M00002, P:M00003	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2>>O=P(O)(O)OP(=O)(O)O]
3: R:M00002, P:M00004	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)O)C(O)C2]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][P:22](=[O:23])([OH:24])[OH:25])[CH:26]([OH:27])[CH2:28]2.[OH2:29]>>[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[OH:17])[CH:26]([OH:27])[CH2:28]2.[O:19]=[P:18]([OH:29])([OH:20])[O:21][P:22](=[O:23])([OH:25])[OH:24]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=29, 2=28, 3=26, 4=11, 5=10, 6=9, 7=5, 8=4, 9=3, 10=2, 11=1, 12=8, 13=6, 14=7, 15=12, 16=13, 17=14, 18=15, 19=16, 20=17, 21=18, 22=19, 23=20, 24=21, 25=22, 26=23, 27=24, 28=25, 29=27}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=23, 2=22, 3=21, 4=24, 5=25, 6=26, 7=27, 8=28, 9=29, 10=20, 11=18, 12=11, 13=10, 14=9, 15=5, 16=4, 17=3, 18=2, 19=1, 20=8, 21=6, 22=7, 23=12, 24=13, 25=14, 26=15, 27=16, 28=17, 29=19}

