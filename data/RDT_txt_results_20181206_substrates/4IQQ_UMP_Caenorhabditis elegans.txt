
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%N:2.0, C-C:1.0]

ORDER_CHANGED
[C%N*C%N:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C1C=CN([CH])[C]N1>>O=C1N[C]N([CH])C=C1C:1.0, [CH2]:1.0, [CH3]:1.0, [CH]:2.0, [CH]C(=[CH])N1C[N][CH]C1:1.0, [CH]C(=[CH])N1C[N][CH]C1>>[C]CNC([CH])=[CH]:1.0, [C]:2.0, [C]C:1.0, [C]C(=[CH])C:2.0, [C]C(=[C])N1CN(C([CH])=[CH])CC1[CH2]>>[N]C=C(C(=O)[NH])C:1.0, [C]C(=[C])N1C[N]CC1[CH2]:1.0, [C]C(=[C])N1C[N]CC1[CH2]>>[C]C(=[C])N=C([CH2])[CH2]:1.0, [C]C(=[C])N=C([CH2])[CH2]:1.0, [C]C1=[C]NCC2N1CN([C])C2>>[C]NCC1=NC([C])=[C]NC1:1.0, [C]C=[CH]:1.0, [C]C=[CH]>>[C]C(=[CH])C:1.0, [C]CNC([CH])=[CH]:1.0, [C]N([CH2])[CH2]:1.0, [C]N([CH2])[CH2]>>[C]N[CH2]:1.0, [C]N([CH])[CH2]:1.0, [C]N([CH])[CH2]>>[C]N=[C]:1.0, [C]N1CN(C(=C[CH])C=[CH])CC1[CH2]>>[N]=C([CH2])CNC(=C[CH])C=[CH]:1.0, [C]N1CN2C(=C([NH])NCC2C1)C([N])=O>>[N]C(=O)C=1N=C(CNC1[NH])C[NH]:1.0, [C]N1C[N]CC1C[NH]:1.0, [C]N1C[N]CC1C[NH]>>[C]N=C(C[NH])C[NH]:1.0, [C]N1[CH]CN([C])C1:1.0, [C]N1[CH]CN([C])C1>>[C]C(=[CH])C:1.0, [C]N=C(C[NH])C[NH]:1.0, [C]N=[C]:1.0, [C]N[CH2]:1.0, [NH]:1.0, [N]:3.0, [N]=C([CH2])[CH2]:1.0, [N]C([CH2])[CH2]:1.0, [N]C([CH2])[CH2]>>[N]=C([CH2])[CH2]:1.0, [N]C=C(C(=O)[NH])C:1.0, [N]C=CC(=O)[NH]:1.0, [N]C=CC(=O)[NH]>>[N]C=C(C(=O)[NH])C:1.0, [N]C[N]:1.0, [N]C[N]>>[C]C:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[CH2]:2.0, [CH3]:1.0, [C]:1.0, [N]:2.0]


ID=Reaction Center at Level: 1 (5)
[[C]C:1.0, [C]C(=[CH])C:1.0, [C]N([CH2])[CH2]:1.0, [C]N([CH])[CH2]:1.0, [N]C[N]:2.0]


ID=Reaction Center at Level: 2 (5)
[[CH]C(=[CH])N1C[N][CH]C1:1.0, [C]C(=[CH])C:1.0, [C]C(=[C])N1C[N]CC1[CH2]:1.0, [C]N1[CH]CN([C])C1:2.0, [N]C=C(C(=O)[NH])C:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[CH]:1.0, [C]:1.0, [N]:2.0]


ID=Reaction Center at Level: 1 (4)
[[C]N([CH])[CH2]:1.0, [C]N=[C]:1.0, [N]=C([CH2])[CH2]:1.0, [N]C([CH2])[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]C(=[C])N1C[N]CC1[CH2]:1.0, [C]C(=[C])N=C([CH2])[CH2]:1.0, [C]N1C[N]CC1C[NH]:1.0, [C]N=C(C[NH])C[NH]:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[N]=C([CH2])[CH2]:1.0, [N]C([CH2])[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[[C]N1C[N]CC1C[NH]:1.0, [C]N=C(C[NH])C[NH]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [N]C([CH2])[CH2]>>[N]=C([CH2])[CH2]
2: [C]N([CH2])[CH2]>>[C]N[CH2]
3: [C]N([CH])[CH2]>>[C]N=[C]
4: [C]C=[CH]>>[C]C(=[CH])C
5: [N]C[N]>>[C]C

MMP Level 2
1: [C]N1C[N]CC1C[NH]>>[C]N=C(C[NH])C[NH]
2: [CH]C(=[CH])N1C[N][CH]C1>>[C]CNC([CH])=[CH]
3: [C]C(=[C])N1C[N]CC1[CH2]>>[C]C(=[C])N=C([CH2])[CH2]
4: [N]C=CC(=O)[NH]>>[N]C=C(C(=O)[NH])C
5: [C]N1[CH]CN([C])C1>>[C]C(=[CH])C

MMP Level 3
1: [C]C1=[C]NCC2N1CN([C])C2>>[C]NCC1=NC([C])=[C]NC1
2: [C]N1CN(C(=C[CH])C=[CH])CC1[CH2]>>[N]=C([CH2])CNC(=C[CH])C=[CH]
3: [C]N1CN2C(=C([NH])NCC2C1)C([N])=O>>[N]C(=O)C=1N=C(CNC1[NH])C[NH]
4: O=C1C=CN([CH])[C]N1>>O=C1N[C]N([CH])C=C1C
5: [C]C(=[C])N1CN(C([CH])=[CH])CC1[CH2]>>[N]C=C(C(=O)[NH])C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1nc(N)[nH]c2NCC3N(c12)CN(c4ccc(cc4)C(=O)NC(C(=O)O)CCC(=O)O)C3>>O=c1nc(N)[nH]c2NCC(=Nc12)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)O, O=c1nc(N)[nH]c2NCC3N(c12)CN(c4ccc(cc4)C(=O)NC(C(=O)O)CCC(=O)O)C3>>O=c1nc(N)[nH]c2NCC(=Nc12)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)O, O=c1nc(N)[nH]c2NCC3N(c12)CN(c4ccc(cc4)C(=O)NC(C(=O)O)CCC(=O)O)C3>>O=c1nc(N)[nH]c2NCC(=Nc12)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)O]
2: R:M00001, P:M00004	[O=c1nc(N)[nH]c2NCC3N(c12)CN(c4ccc(cc4)C(=O)NC(C(=O)O)CCC(=O)O)C3>>O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)O)C(O)C2]
3: R:M00002, P:M00004	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)O)C(O)C2>>O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)O)C(O)C2]


//
SELECTED AAM MAPPING
[O:31]=[C:30]([OH:32])[CH2:29][CH2:28][CH:24]([NH:23][C:21](=[O:22])[C:18]:1:[CH:17]:[CH:16]:[C:15](:[CH:20]:[CH:19]1)[N:14]2[CH2:13][N:11]3[C:12]=4[C:2](=[O:1])[N:3]=[C:4]([NH2:5])[NH:6][C:7]4[NH:8][CH2:9][CH:10]3[CH2:33]2)[C:25](=[O:26])[OH:27].[O:34]=[C:35]1[CH:36]=[CH:37][N:38]([C:39](=[O:40])[NH:41]1)[CH:42]2[O:43][CH:44]([CH2:45][O:46][P:47](=[O:48])([OH:49])[OH:50])[CH:51]([OH:52])[CH2:53]2>>[O:31]=[C:30]([OH:32])[CH2:29][CH2:28][CH:24]([NH:23][C:21](=[O:22])[C:18]:1:[CH:17]:[CH:16]:[C:15](:[CH:20]:[CH:19]1)[NH:14][CH2:33][C:10]2=[N:11][C:12]=3[C:2](=[O:1])[N:3]=[C:4]([NH2:5])[NH:6][C:7]3[NH:8][CH2:9]2)[C:25](=[O:26])[OH:27].[O:34]=[C:35]1[NH:41][C:39](=[O:40])[N:38]([CH:37]=[C:36]1[CH3:13])[CH:42]2[O:43][CH:44]([CH2:45][O:46][P:47](=[O:48])([OH:49])[OH:50])[CH:51]([OH:52])[CH2:53]2


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=10, 3=33, 4=14, 5=13, 6=11, 7=12, 8=7, 9=8, 10=6, 11=4, 12=3, 13=2, 14=1, 15=5, 16=15, 17=16, 18=17, 19=18, 20=19, 21=20, 22=21, 23=22, 24=23, 25=24, 26=28, 27=29, 28=30, 29=31, 30=32, 31=25, 32=26, 33=27, 34=53, 35=51, 36=44, 37=43, 38=42, 39=38, 40=37, 41=36, 42=35, 43=34, 44=41, 45=39, 46=40, 47=45, 48=46, 49=47, 50=48, 51=49, 52=50, 53=52}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=9, 2=10, 3=11, 4=12, 5=7, 6=8, 7=6, 8=4, 9=3, 10=2, 11=1, 12=5, 13=13, 14=14, 15=15, 16=16, 17=17, 18=18, 19=19, 20=20, 21=21, 22=22, 23=23, 24=24, 25=28, 26=29, 27=30, 28=31, 29=32, 30=25, 31=26, 32=27, 33=41, 34=40, 35=39, 36=38, 37=36, 38=37, 39=35, 40=34, 41=33, 42=42, 43=53, 44=51, 45=44, 46=43, 47=45, 48=46, 49=47, 50=48, 51=49, 52=50, 53=52}

