
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-O:1.0, O-S:1.0]

//
FINGERPRINTS RC
[N#CSO:1.0, N#C[S-]:1.0, N#C[S-]>>N#CSO:2.0, O:3.0, OO:4.0, OO>>N#CSO:1.0, OO>>O:3.0, OO>>[C]SO:1.0, OO>>[S]O:1.0, [C]SO:2.0, [C][S-]:1.0, [C][S-]>>[C]SO:1.0, [OH]:3.0, [S-]:1.0, [S]:1.0, [S]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[O:1.0, [OH]:3.0, [S]:1.0]


ID=Reaction Center at Level: 1 (4)
[O:1.0, OO:2.0, [C]SO:1.0, [S]O:1.0]


ID=Reaction Center at Level: 2 (4)
[N#CSO:1.0, O:1.0, OO:2.0, [C]SO:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: OO>>O
2: OO>>[S]O
3: [C][S-]>>[C]SO

MMP Level 2
1: OO>>O
2: OO>>[C]SO
3: N#C[S-]>>N#CSO

MMP Level 3
1: OO>>O
2: OO>>N#CSO
3: N#C[S-]>>N#CSO


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[N#C[S-]>>N#CSO]
2: R:M00002, P:M00003	[OO>>N#CSO]
3: R:M00002, P:M00004	[OO>>O]


//
SELECTED AAM MAPPING
[N:1]#[C:2][S-:3].[OH:4][OH:5]>>[N:1]#[C:2][S:3][OH:4].[OH2:5]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=2, 2=1, 3=3, 4=4, 5=5}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=2, 2=1, 3=3, 4=4, 5=5}

