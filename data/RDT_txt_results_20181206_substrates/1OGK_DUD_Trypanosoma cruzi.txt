
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O:3.0, O=P(O)(O)O:3.0, O=P(O)(O)OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O:1.0, O>>O=P(O)(O)O:2.0, O>>[P]O:1.0, [OH]:2.0, [O]:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=P(O)(O)O:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=P(O)(O)O:2.0, O=P(O)(O)O[P]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]P(=O)(O)O>>O=P(O)(O)O
2: [P]O[P]>>[P]O
3: O>>[P]O

MMP Level 2
1: O=P(O)(O)O[P]>>O=P(O)(O)O
2: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
3: O>>O=P(O)(O)O

MMP Level 3
1: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O
2: O=P(O)(O)OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]
3: O>>O=P(O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)O)C(O)C2>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)O)C(O)C2]
2: R:M00001, P:M00004	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)O)C(O)C2>>O=P(O)(O)O]
3: R:M00002, P:M00004	[O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[OH:21])[CH:22]([OH:23])[CH2:24]2.[OH2:25]>>[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[OH:17])[CH:22]([OH:23])[CH2:24]2.[O:19]=[P:18]([OH:21])([OH:25])[OH:20]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=22, 3=11, 4=10, 5=9, 6=5, 7=4, 8=3, 9=2, 10=1, 11=8, 12=6, 13=7, 14=12, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=23, 25=25}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=18, 3=11, 4=10, 5=9, 6=5, 7=4, 8=3, 9=2, 10=1, 11=8, 12=6, 13=7, 14=12, 15=13, 16=14, 17=15, 18=16, 19=17, 20=19, 21=23, 22=22, 23=21, 24=24, 25=25}

