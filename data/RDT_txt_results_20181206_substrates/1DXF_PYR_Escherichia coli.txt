
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O=C(O)C(=O)C>>[C]C(O)C(O)CC(=O)C(=O)O:1.0, O=C(O)C(O)C(O)[CH2]:1.0, O=CC(O)C(=O)O:1.0, O=CC(O)C(=O)O>>O=C(O)C(O)C(O)[CH2]:1.0, O=CC(O)C(=O)O>>[C]C(=O)CC(O)C(O)C(=O)O:1.0, O=CC(O)C(=O)O>>[C]CC(O)C(O)C(=O)O:1.0, O=C[CH]:2.0, O=C[CH]>>[CH]C(O)[CH2]:2.0, O=[CH]:1.0, O=[CH]>>[CH]O:1.0, [CH2]:1.0, [CH3]:1.0, [CH]:4.0, [CH]C(O)[CH2]:2.0, [CH]O:1.0, [C]C:1.0, [C]C(=O)C:1.0, [C]C(=O)C>>[C]C(=O)CC([CH])O:1.0, [C]C(=O)CC([CH])O:1.0, [C]C(O)C=O:1.0, [C]C(O)C=O>>[C]CC(O)C([C])O:2.0, [C]C([CH])O:2.0, [C]C([CH])O>>[C]C([CH])O:1.0, [C]C>>[C]C[CH]:1.0, [C]CC(O)C([C])O:1.0, [C]C[CH]:1.0, [OH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH2]:1.0, [CH]:1.0]


ID=Reaction Center at Level: 1 (2)
[[CH]C(O)[CH2]:1.0, [C]C[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[C]C(=O)CC([CH])O:1.0, [C]CC(O)C([C])O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C[CH]:1.0, O=[CH]:1.0, [CH]C(O)[CH2]:1.0, [CH]O:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C[CH]:1.0, [CH]C(O)[CH2]:1.0, [C]C(O)C=O:1.0, [C]CC(O)C([C])O:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:4.0]


ID=Reaction Center at Level: 1 (3)
[O=C[CH]:1.0, [CH]C(O)[CH2]:1.0, [C]C([CH])O:2.0]


ID=Reaction Center at Level: 2 (4)
[O=C(O)C(O)C(O)[CH2]:1.0, O=CC(O)C(=O)O:1.0, [C]C(O)C=O:1.0, [C]CC(O)C([C])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C>>[C]C[CH]
2: O=C[CH]>>[CH]C(O)[CH2]
3: [C]C([CH])O>>[C]C([CH])O
4: O=[CH]>>[CH]O

MMP Level 2
1: [C]C(=O)C>>[C]C(=O)CC([CH])O
2: [C]C(O)C=O>>[C]CC(O)C([C])O
3: O=CC(O)C(=O)O>>O=C(O)C(O)C(O)[CH2]
4: O=C[CH]>>[CH]C(O)[CH2]

MMP Level 3
1: O=C(O)C(=O)C>>[C]C(O)C(O)CC(=O)C(=O)O
2: O=CC(O)C(=O)O>>[C]C(=O)CC(O)C(O)C(=O)O
3: O=CC(O)C(=O)O>>[C]CC(O)C(O)C(=O)O
4: [C]C(O)C=O>>[C]CC(O)C([C])O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(=O)C>>O=C(O)C(=O)CC(O)C(O)C(=O)O]
2: R:M00002, P:M00003	[O=CC(O)C(=O)O>>O=C(O)C(=O)CC(O)C(O)C(=O)O, O=CC(O)C(=O)O>>O=C(O)C(=O)CC(O)C(O)C(=O)O, O=CC(O)C(=O)O>>O=C(O)C(=O)CC(O)C(O)C(=O)O]


//
SELECTED AAM MAPPING
[O:1]=[CH:2][CH:3]([OH:4])[C:5](=[O:6])[OH:7].[O:8]=[C:9]([OH:10])[C:11](=[O:12])[CH3:13]>>[O:8]=[C:9]([OH:10])[C:11](=[O:12])[CH2:13][CH:2]([OH:1])[CH:3]([OH:4])[C:5](=[O:6])[OH:7]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=13, 2=11, 3=12, 4=9, 5=8, 6=10, 7=2, 8=1, 9=3, 10=5, 11=6, 12=7, 13=4}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=9, 4=11, 5=12, 6=13, 7=10, 8=8, 9=4, 10=5, 11=2, 12=1, 13=3}

