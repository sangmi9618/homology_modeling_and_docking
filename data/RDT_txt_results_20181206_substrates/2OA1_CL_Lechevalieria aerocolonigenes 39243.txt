
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-Cl:1.0, O=O:1.0]

ORDER_CHANGED
[C%C*C%C:1.0, C%N*C%N:2.0]

//
FINGERPRINTS RC
[O:3.0, O=C1N[C]NC2=C1NC([CH])=[C]N2[CH2]>>O=C1N[C]N=C2C1=NC([CH])=[C]N2[CH2]:1.0, O=O:4.0, O=O>>O:3.0, O=c1[nH]c(=O)c2N[C]=C([CH])N(c2[nH]1)C[CH]>>O=C1N=C2C(=N[C]=C([CH])N2C[CH])C(=O)N1:1.0, [CH]:1.0, [C]:5.0, [C]1=CNc2ccccc12>>Clc1cccc2[C]=CNc12:1.0, [C]=C([NH])C(Cl)=C[CH]:1.0, [C]=C([N])NC(=O)[NH]:1.0, [C]=C([N])NC(=O)[NH]>>[C]C([N])=NC(=O)[NH]:1.0, [C]=C([N])[NH]:1.0, [C]=C([N])[NH]>>[C]C([N])=[N]:1.0, [C]=CC=1NC(=C([NH])[N]C1[CH])C(=O)[NH]>>[C]=CC=1N=C(C(=[N])[N]C1[CH])C(=O)[NH]:1.0, [C]=C[CH]:1.0, [C]=C[CH]>>[C]C(Cl)=[CH]:1.0, [C]C(=[C])NC(=[C])[CH]:1.0, [C]C(=[C])NC(=[C])[CH]>>[C]C([C])=NC(=[C])[CH]:1.0, [C]C(=[C])[NH]:1.0, [C]C(=[C])[NH]>>[C]C([C])=[N]:1.0, [C]C(Cl)=[CH]:2.0, [C]C([C])=NC(=[C])[CH]:1.0, [C]C([C])=[N]:1.0, [C]C([NH])=CC=[CH]:1.0, [C]C([NH])=CC=[CH]>>[C]=C([NH])C(Cl)=C[CH]:1.0, [C]C([N])=NC(=O)[NH]:1.0, [C]C([N])=[N]:1.0, [C]Cl:1.0, [C]N([CH2])C=1NC(=O)N[C]C1[NH]>>[C]N([CH2])C1=NC(=O)N[C]C1=[N]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]:1.0, [C]N=[C]:2.0, [C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]NC(=C([C])[NH])N([C])[CH2]>>[C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]NC(=C([N])[NH])C(=O)[NH]:1.0, [C]NC(=C([N])[NH])C(=O)[NH]>>[C]N=C(C([N])=[N])C(=O)[NH]:1.0, [C]N[C]:2.0, [C]N[C]>>[C]N=[C]:2.0, [Cl-]:3.0, [Cl-]>>[C]=C([NH])C(Cl)=C[CH]:1.0, [Cl-]>>[C]C(Cl)=[CH]:1.0, [Cl-]>>[C]Cl:1.0, [Cl]:1.0, [NH]:2.0, [N]:2.0, [O]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[O:1.0, [C]:1.0, [Cl-]:1.0, [Cl]:1.0, [O]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=O:2.0, [C]C(Cl)=[CH]:1.0, [C]Cl:1.0, [Cl-]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=O:2.0, [C]=C([NH])C(Cl)=C[CH]:1.0, [C]C(Cl)=[CH]:1.0, [Cl-]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:4.0, [NH]:2.0, [N]:2.0]


ID=Reaction Center at Level: 1 (6)
[[C]=C([N])[NH]:1.0, [C]C(=[C])[NH]:1.0, [C]C([C])=[N]:1.0, [C]C([N])=[N]:1.0, [C]N=[C]:2.0, [C]N[C]:2.0]


ID=Reaction Center at Level: 2 (8)
[[C]=C([N])NC(=O)[NH]:1.0, [C]C(=[C])NC(=[C])[CH]:1.0, [C]C([C])=NC(=[C])[CH]:1.0, [C]C([N])=NC(=O)[NH]:1.0, [C]N=C(C([C])=[N])N([C])[CH2]:1.0, [C]N=C(C([N])=[N])C(=O)[NH]:1.0, [C]NC(=C([C])[NH])N([C])[CH2]:1.0, [C]NC(=C([N])[NH])C(=O)[NH]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]N[C]>>[C]N=[C]
2: [C]C(=[C])[NH]>>[C]C([C])=[N]
3: [C]=C([N])[NH]>>[C]C([N])=[N]
4: [C]=C[CH]>>[C]C(Cl)=[CH]
5: O=O>>O
6: [C]N[C]>>[C]N=[C]
7: [Cl-]>>[C]Cl

MMP Level 2
1: [C]C(=[C])NC(=[C])[CH]>>[C]C([C])=NC(=[C])[CH]
2: [C]NC(=C([N])[NH])C(=O)[NH]>>[C]N=C(C([N])=[N])C(=O)[NH]
3: [C]NC(=C([C])[NH])N([C])[CH2]>>[C]N=C(C([C])=[N])N([C])[CH2]
4: [C]C([NH])=CC=[CH]>>[C]=C([NH])C(Cl)=C[CH]
5: O=O>>O
6: [C]=C([N])NC(=O)[NH]>>[C]C([N])=NC(=O)[NH]
7: [Cl-]>>[C]C(Cl)=[CH]

MMP Level 3
1: [C]=CC=1NC(=C([NH])[N]C1[CH])C(=O)[NH]>>[C]=CC=1N=C(C(=[N])[N]C1[CH])C(=O)[NH]
2: O=C1N[C]NC2=C1NC([CH])=[C]N2[CH2]>>O=C1N[C]N=C2C1=NC([CH])=[C]N2[CH2]
3: O=c1[nH]c(=O)c2N[C]=C([CH])N(c2[nH]1)C[CH]>>O=C1N=C2C(=N[C]=C([CH])N2C[CH])C(=O)N1
4: [C]1=CNc2ccccc12>>Clc1cccc2[C]=CNc12
5: O=O>>O
6: [C]N([CH2])C=1NC(=O)N[C]C1[NH]>>[C]N([CH2])C1=NC(=O)N[C]C1=[N]
7: [Cl-]>>[C]=C([NH])C(Cl)=C[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00005	[O=C(O)C(N)Cc1c[nH]c2ccccc21>>O=C(O)C(N)Cc1c[nH]c2c(Cl)cccc12]
2: R:M00002, P:M00006	[O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C>>O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1, O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C>>O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1, O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C>>O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1, O=c1[nH]c(=O)c2Nc3cc(c(cc3N(c2[nH]1)CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C>>O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O)C)C)c(=O)[nH]1]
3: R:M00003, P:M00005	[[Cl-]>>O=C(O)C(N)Cc1c[nH]c2c(Cl)cccc12]
4: R:M00004, P:M00007	[O=O>>O]


//
SELECTED AAM MAPPING
[Cl-:70].[O:69]=[O:71].[O:54]=[C:55]([OH:56])[CH:57]([NH2:58])[CH2:59][C:60]:1:[CH:61]:[NH:62]:[C:63]:2:[CH:64]:[CH:65]:[CH:66]:[CH:67]:[C:68]21.[O:1]=[C:2]1[NH:3][C:4](=[O:5])[C:6]=2[NH:7][C:8]:3:[CH:9]:[C:10](:[C:11](:[CH:12]:[C:13]3[N:14]([C:15]2[NH:16]1)[CH2:17][CH:18]([OH:19])[CH:20]([OH:21])[CH:22]([OH:23])[CH2:24][O:25][P:26](=[O:27])([OH:28])[O:29][P:30](=[O:31])([OH:32])[O:33][CH2:34][CH:35]4[O:36][CH:37]([N:38]:5:[CH:39]:[N:40]:[C:41]:6:[C:42](:[N:43]:[CH:44]:[N:45]:[C:46]65)[NH2:47])[CH:48]([OH:49])[CH:50]4[OH:51])[CH3:52])[CH3:53]>>[O:54]=[C:55]([OH:56])[CH:57]([NH2:58])[CH2:59][C:60]:1:[CH:61]:[NH:62]:[C:63]:2:[C:64]([Cl:70]):[CH:65]:[CH:66]:[CH:67]:[C:68]21.[O:1]=[C:2]1[N:16]=[C:15]2[C:6](=[N:7][C:8]:3:[CH:9]:[C:10](:[C:11](:[CH:12]:[C:13]3[N:14]2[CH2:17][CH:18]([OH:19])[CH:20]([OH:21])[CH:22]([OH:23])[CH2:24][O:25][P:26](=[O:27])([OH:28])[O:29][P:30](=[O:31])([OH:32])[O:33][CH2:34][CH:35]4[O:36][CH:37]([N:38]:5:[CH:39]:[N:40]:[C:41]:6:[C:42](:[N:43]:[CH:44]:[N:45]:[C:46]65)[NH2:47])[CH:48]([OH:49])[CH:50]4[OH:51])[CH3:52])[CH3:53])[C:4](=[O:5])[NH:3]1.[OH2:69]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=66, 2=65, 3=64, 4=63, 5=68, 6=67, 7=60, 8=61, 9=62, 10=59, 11=57, 12=55, 13=54, 14=56, 15=58, 16=53, 17=10, 18=9, 19=8, 20=13, 21=12, 22=11, 23=52, 24=14, 25=15, 26=6, 27=7, 28=4, 29=5, 30=3, 31=2, 32=1, 33=16, 34=17, 35=18, 36=20, 37=22, 38=24, 39=25, 40=26, 41=27, 42=28, 43=29, 44=30, 45=31, 46=32, 47=33, 48=34, 49=35, 50=50, 51=48, 52=37, 53=36, 54=38, 55=39, 56=40, 57=41, 58=46, 59=45, 60=44, 61=43, 62=42, 63=47, 64=49, 65=51, 66=23, 67=21, 68=19, 69=71, 70=69, 71=70}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=67, 2=68, 3=69, 4=63, 5=64, 6=66, 7=65, 8=62, 9=61, 10=60, 11=59, 12=57, 13=55, 14=54, 15=56, 16=58, 17=50, 18=9, 19=8, 20=7, 21=12, 22=11, 23=10, 24=49, 25=13, 26=4, 27=3, 28=2, 29=1, 30=53, 31=51, 32=52, 33=5, 34=6, 35=14, 36=15, 37=17, 38=19, 39=21, 40=22, 41=23, 42=24, 43=25, 44=26, 45=27, 46=28, 47=29, 48=30, 49=31, 50=32, 51=47, 52=45, 53=34, 54=33, 55=35, 56=36, 57=37, 58=38, 59=43, 60=42, 61=41, 62=40, 63=39, 64=44, 65=46, 66=48, 67=20, 68=18, 69=16, 70=70}

