
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=P(O)(O[P])OC(O[CH])C([CH])O>>O=P(O)(O)O[P]:1.0, [CH]:2.0, [C]=C([CH])O:1.0, [C]=C([CH])O>>[C]=C([CH])OC([O])[CH]:1.0, [C]=C([CH])OC([O])[CH]:1.0, [C]C([O+])=C(O)C=[C]>>[C]C([O+])=C(OC(O[CH])C([CH])O)C=[C]:1.0, [C]O:1.0, [C]O>>[C]O[CH]:1.0, [C]OC(O[CH])C([CH])O:1.0, [C]O[CH]:1.0, [OH]:2.0, [O]:2.0, [O]C([CH])OP([O])(=O)O:1.0, [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]C([O])[CH]:2.0, [O]C([O])[CH]>>[O]C([O])[CH]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OC1OC([CH2])[CH]C(O)C1O>>[C]=C([CH])OC1OC([CH2])[CH]C(O)C1O:1.0, [P]O:1.0, [P]OC(O[CH])C([CH])O:1.0, [P]OC(O[CH])C([CH])O>>[C]OC(O[CH])C([CH])O:1.0, [P]O[CH]:1.0, [P]O[CH]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (3)
[[C]O[CH]:1.0, [O]C([O])[CH]:2.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]=C([CH])OC([O])[CH]:1.0, [C]OC(O[CH])C([CH])O:1.0, [O]C([CH])OP([O])(=O)O:1.0, [P]OC(O[CH])C([CH])O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (1)
[[O]C([O])[CH]:2.0]


ID=Reaction Center at Level: 2 (2)
[[C]OC(O[CH])C([CH])O:1.0, [P]OC(O[CH])C([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [P]O[CH]>>[P]O
2: [C]O>>[C]O[CH]
3: [O]C([O])[CH]>>[O]C([O])[CH]

MMP Level 2
1: [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O
2: [C]=C([CH])O>>[C]=C([CH])OC([O])[CH]
3: [P]OC(O[CH])C([CH])O>>[C]OC(O[CH])C([CH])O

MMP Level 3
1: O=P(O)(O[P])OC(O[CH])C([CH])O>>O=P(O)(O)O[P]
2: [C]C([O+])=C(O)C=[C]>>[C]C([O+])=C(OC(O[CH])C([CH])O)C=[C]
3: [O]P(=O)(O)OC1OC([CH2])[CH]C(O)C1O>>[C]=C([CH])OC1OC([CH2])[CH]C(O)C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3O)C(O)C2O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)O)C(O)C2O]
2: R:M00001, P:M00004	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3O)C(O)C2O>>Oc1cc(O)c2cc(OC3OC(CO)C(O)C(O)C3O)c([o+]c2c1)-c4cc(O)c(O)c(O)c4]
3: R:M00002, P:M00004	[Oc1cc(O)c2cc(O)c([o+]c2c1)-c3cc(O)c(O)c(O)c3>>Oc1cc(O)c2cc(OC3OC(CO)C(O)C(O)C3O)c([o+]c2c1)-c4cc(O)c(O)c(O)c4]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][CH:22]3[O:23][CH:24]([CH2:25][OH:26])[CH:27]([OH:28])[CH:29]([OH:30])[CH:31]3[OH:32])[CH:33]([OH:34])[CH:35]2[OH:36].[OH:37][C:38]:1:[CH:39]:[C:40]([OH:41]):[C:42]:2:[CH:43]:[C:44]([OH:45]):[C:46](:[O+:47]:[C:48]2:[CH:49]1)[C:50]:3:[CH:51]:[C:52]([OH:53]):[C:54]([OH:55]):[C:56]([OH:57]):[CH:58]3>>[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[OH:21])[CH:33]([OH:34])[CH:35]2[OH:36].[OH:37][C:38]:1:[CH:39]:[C:40]([OH:41]):[C:42]:2:[CH:43]:[C:44]([O:45][CH:22]3[O:23][CH:24]([CH2:25][OH:26])[CH:27]([OH:28])[CH:29]([OH:30])[CH:31]3[OH:32]):[C:46](:[O+:47]:[C:48]2:[CH:49]1)[C:50]:4:[CH:58]:[C:56]([OH:57]):[C:54]([OH:55]):[C:52]([OH:53]):[CH:51]4


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=3, 2=4, 3=5, 4=6, 5=7, 6=8, 7=2, 8=1, 9=9, 10=35, 11=33, 12=11, 13=10, 14=12, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=31, 26=29, 27=27, 28=24, 29=23, 30=25, 31=26, 32=28, 33=30, 34=32, 35=34, 36=36, 37=51, 38=50, 39=58, 40=56, 41=54, 42=52, 43=53, 44=55, 45=57, 46=46, 47=44, 48=43, 49=42, 50=40, 51=39, 52=38, 53=49, 54=48, 55=47, 56=37, 57=41, 58=45}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=36, 2=37, 3=38, 4=39, 5=40, 6=41, 7=35, 8=34, 9=42, 10=57, 11=55, 12=44, 13=43, 14=45, 15=46, 16=47, 17=48, 18=49, 19=50, 20=51, 21=52, 22=53, 23=54, 24=56, 25=58, 26=26, 27=25, 28=33, 29=31, 30=29, 31=27, 32=28, 33=30, 34=32, 35=21, 36=8, 37=7, 38=6, 39=4, 40=3, 41=2, 42=24, 43=23, 44=22, 45=1, 46=5, 47=9, 48=10, 49=19, 50=17, 51=15, 52=12, 53=11, 54=13, 55=14, 56=16, 57=18, 58=20}

