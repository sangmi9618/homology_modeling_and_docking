
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:2.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)[CH2]:1.0, O=C(OC)C[NH]>>O=C(O)C[NH]:1.0, O=C(OC)[CH2]:1.0, O=C(OC)[CH2]>>O=C(O)[CH2]:1.0, O=C(OC)[CH2]>>OC:1.0, O>>OC:3.0, OC:4.0, [CH3]:2.0, [C]O:1.0, [C]OC:2.0, [C]OC>>OC:1.0, [C]OC>>[C]O:1.0, [OH]:2.0, [O]:1.0, [O]C:1.0, [O]C>>OC:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [CH3]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O:1.0, OC:2.0, [C]OC:1.0, [O]C:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=C(OC)[CH2]:1.0, OC:2.0, [C]OC:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]OC>>[C]O
2: [O]C>>OC
3: O>>OC

MMP Level 2
1: O=C(OC)[CH2]>>O=C(O)[CH2]
2: [C]OC>>OC
3: O>>OC

MMP Level 3
1: O=C(OC)C[NH]>>O=C(O)C[NH]
2: O=C(OC)[CH2]>>OC
3: O>>OC


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(OC)CNC(=O)Cc1ccccc1>>O=C(O)CNC(=O)Cc1ccccc1]
2: R:M00001, P:M00004	[O=C(OC)CNC(=O)Cc1ccccc1>>OC]
3: R:M00002, P:M00004	[O>>OC]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([O:3][CH3:4])[CH2:5][NH:6][C:7](=[O:8])[CH2:9][C:10]:1:[CH:11]:[CH:12]:[CH:13]:[CH:14]:[CH:15]1.[OH2:16]>>[O:1]=[C:2]([OH:3])[CH2:5][NH:6][C:7](=[O:8])[CH2:9][C:10]:1:[CH:11]:[CH:12]:[CH:13]:[CH:14]:[CH:15]1.[OH:16][CH3:4]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=4, 2=3, 3=2, 4=1, 5=5, 6=6, 7=7, 8=8, 9=9, 10=10, 11=11, 12=12, 13=13, 14=14, 15=15, 16=16}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=12, 2=11, 3=10, 4=9, 5=14, 6=13, 7=8, 8=6, 9=7, 10=5, 11=4, 12=2, 13=1, 14=3, 15=16, 16=15}

