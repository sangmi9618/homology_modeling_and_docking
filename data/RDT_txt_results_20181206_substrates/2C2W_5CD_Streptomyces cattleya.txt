
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-Cl:1.0, C-S:1.0]

//
FINGERPRINTS RC
[ClC[CH]:2.0, Cl[CH2]:1.0, OC1[CH][CH]OC1C[S+](C)C[CH2]>>ClCC1O[CH][CH]C1O:1.0, S(C)C[CH2]:1.0, S([CH2])C:1.0, [CH2]:2.0, [CH2][S+]([CH2])C:1.0, [CH2][S+]([CH2])C>>S([CH2])C:1.0, [CH]C[S+](C)C[CH2]:1.0, [CH]C[S+](C)C[CH2]>>S(C)C[CH2]:1.0, [Cl-]:3.0, [Cl-]>>ClC[CH]:1.0, [Cl-]>>Cl[CH2]:1.0, [Cl-]>>[O]C([CH])CCl:1.0, [Cl]:1.0, [O]C([CH])CCl:1.0, [O]C([CH])C[S+](C)CC[CH]>>[CH]CCSC:1.0, [O]C([CH])C[S+]([CH2])C:1.0, [O]C([CH])C[S+]([CH2])C>>[O]C([CH])CCl:1.0, [S+]:1.0, [S+]C[CH]:1.0, [S+]C[CH]>>ClC[CH]:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[CH2]:2.0, [Cl-]:1.0, [Cl]:1.0, [S+]:1.0]


ID=Reaction Center at Level: 1 (5)
[ClC[CH]:1.0, Cl[CH2]:1.0, [CH2][S+]([CH2])C:1.0, [Cl-]:1.0, [S+]C[CH]:1.0]


ID=Reaction Center at Level: 2 (5)
[ClC[CH]:1.0, [CH]C[S+](C)C[CH2]:1.0, [Cl-]:1.0, [O]C([CH])CCl:1.0, [O]C([CH])C[S+]([CH2])C:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [S+]C[CH]>>ClC[CH]
2: [CH2][S+]([CH2])C>>S([CH2])C
3: [Cl-]>>Cl[CH2]

MMP Level 2
1: [O]C([CH])C[S+]([CH2])C>>[O]C([CH])CCl
2: [CH]C[S+](C)C[CH2]>>S(C)C[CH2]
3: [Cl-]>>ClC[CH]

MMP Level 3
1: OC1[CH][CH]OC1C[S+](C)C[CH2]>>ClCC1O[CH][CH]C1O
2: [O]C([CH])C[S+](C)CC[CH]>>[CH]CCSC
3: [Cl-]>>[O]C([CH])CCl


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>ClCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(O)C(N)CCSC]
3: R:M00002, P:M00003	[[Cl-]>>ClCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]


//
SELECTED AAM MAPPING
[Cl-:28].[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][S+:8]([CH3:9])[CH2:10][CH:11]1[O:12][CH:13]([N:14]:2:[CH:15]:[N:16]:[C:17]:3:[C:18](:[N:19]:[CH:20]:[N:21]:[C:22]32)[NH2:23])[CH:24]([OH:25])[CH:26]1[OH:27]>>[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][S:8][CH3:9].[Cl:28][CH2:10][CH:11]1[O:12][CH:13]([N:14]:2:[CH:15]:[N:16]:[C:17]:3:[C:18](:[N:19]:[CH:20]:[N:21]:[C:22]32)[NH2:23])[CH:24]([OH:25])[CH:26]1[OH:27]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=4, 6=2, 7=1, 8=3, 9=5, 10=10, 11=11, 12=26, 13=24, 14=13, 15=12, 16=14, 17=15, 18=16, 19=17, 20=22, 21=21, 22=20, 23=19, 24=18, 25=23, 26=25, 27=27, 28=28}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=12, 2=13, 3=14, 4=9, 5=10, 6=11, 7=15, 8=8, 9=7, 10=6, 11=5, 12=16, 13=18, 14=3, 15=4, 16=2, 17=1, 18=19, 19=17, 20=28, 21=27, 22=26, 23=25, 24=23, 25=21, 26=20, 27=22, 28=24}

