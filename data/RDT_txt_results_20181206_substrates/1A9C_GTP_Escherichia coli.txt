
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%C:3.0, C%O:1.0, C-C:1.0, C-O:1.0, C@N:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O:3.0, O=CO:4.0, O=[CH]:1.0, O>>O=CO:2.0, O>>[CH]O:1.0, OC1[CH]OC([CH2])C1O:1.0, OC1[CH]OC([CH2])C1O>>[N]=C([CH2])C(O)C(O)[CH2]:1.0, [CH2]:1.0, [CH]:6.0, [CH]C(O)[CH2]:1.0, [CH]C([CH])O:3.0, [CH]C([CH])O>>O=CO:2.0, [CH]C([CH])O>>[C]C([CH])O:1.0, [CH]N1[C]=[C]N=C1:1.0, [CH]N1[C]=[C]N=C1>>[C]N=C(C[NH])C([CH])O:1.0, [CH]O:3.0, [CH]O>>O=[CH]:1.0, [CH]O[CH]:1.0, [CH]O[CH]>>[CH]O:1.0, [C]:1.0, [C]C([CH])O:1.0, [C]CNC(=[C])[NH]:1.0, [C]C[NH]:1.0, [C]N([CH])C1OC(C[O])C(O)C1O>>[C]C(O)C(O)C[O]:1.0, [C]N([CH])C1OC([CH2])C(O)C1O>>O=CO:1.0, [C]N([CH])C1O[CH][CH]C1O:1.0, [C]N([CH])C1O[CH][CH]C1O>>[C]NCC(=[N])[CH]:1.0, [C]N([CH])[CH]:1.0, [C]N([CH])[CH]>>[C]N[CH2]:1.0, [C]N=C(C[NH])C([CH])O:1.0, [C]NCC(=[N])[CH]:1.0, [C]N[CH2]:1.0, [C]Nc1c([C])ncn1C2O[CH][CH]C2O>>[C]NC=1NCC([CH])=NC1[C]:1.0, [C]c1ncn(c1[NH])C([O])[CH]>>[C]C1=[C]NCC(=N1)C(O)C(O)[CH2]:1.0, [NH]:1.0, [NH]C1=[C]N=CN1C2OC([CH2])C(O)C2O>>[CH]C(O)C1=N[C]=C([NH])NC1:1.0, [N]:1.0, [N]=C([CH2])C(O)C(O)[CH2]:1.0, [N]=C([CH])[CH2]:1.0, [N]C([O])[CH]:1.0, [N]C([O])[CH]>>[C]C[NH]:1.0, [N]C1OC(C[O])C(O)C1O>>[C]N=C(C[NH])C(O)C(O)C[O]:1.0, [N]C1OC([CH2])[CH][CH]1:1.0, [N]C1OC([CH2])[CH][CH]1>>[CH]C(O)[CH2]:1.0, [N]C1O[CH]C(O)C1O:1.0, [N]C1O[CH]C(O)C1O>>O=CO:2.0, [N]C=[N]:1.0, [N]C=[N]>>[N]=C([CH])[CH2]:1.0, [OH]:3.0, [O]:2.0, [O]C([CH])N1C=N[C]=C1[NH]:1.0, [O]C([CH])N1C=N[C]=C1[NH]>>[C]CNC(=[C])[NH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (7)
[O:1.0, [CH2]:1.0, [CH]:8.0, [C]:2.0, [N]:1.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (11)
[O:1.0, O=CO:1.0, [CH]C([CH])O:3.0, [CH]O:1.0, [CH]O[CH]:1.0, [C]C([CH])O:1.0, [C]C[NH]:1.0, [C]N([CH])[CH]:1.0, [N]=C([CH])[CH2]:2.0, [N]C([O])[CH]:2.0, [N]C=[N]:1.0]


ID=Reaction Center at Level: 2 (11)
[O:1.0, O=CO:2.0, OC1[CH]OC([CH2])C1O:1.0, [CH]N1[C]=[C]N=C1:1.0, [C]N([CH])C1O[CH][CH]C1O:2.0, [C]N=C(C[NH])C([CH])O:2.0, [C]NCC(=[N])[CH]:1.0, [N]=C([CH2])C(O)C(O)[CH2]:1.0, [N]C1OC([CH2])[CH][CH]1:1.0, [N]C1O[CH]C(O)C1O:2.0, [O]C([CH])N1C=N[C]=C1[NH]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=CO:1.0, O=[CH]:1.0, [CH]C([CH])O:1.0, [CH]O:1.0]


ID=Reaction Center at Level: 2 (3)
[O=CO:2.0, [CH]C([CH])O:1.0, [N]C1O[CH]C(O)C1O:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH2]:1.0, [CH]:3.0]


ID=Reaction Center at Level: 1 (4)
[O=CO:1.0, [CH]C([CH])O:1.0, [C]C[NH]:1.0, [N]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=CO:1.0, [C]N([CH])C1O[CH][CH]C1O:1.0, [C]NCC(=[N])[CH]:1.0, [N]C1O[CH]C(O)C1O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]O[CH]>>[CH]O
2: O>>[CH]O
3: [N]C([O])[CH]>>[C]C[NH]
4: [CH]C([CH])O>>[C]C([CH])O
5: [CH]O>>O=[CH]
6: [C]N([CH])[CH]>>[C]N[CH2]
7: [CH]C([CH])O>>O=CO
8: [N]C=[N]>>[N]=C([CH])[CH2]

MMP Level 2
1: [N]C1OC([CH2])[CH][CH]1>>[CH]C(O)[CH2]
2: O>>O=CO
3: [C]N([CH])C1O[CH][CH]C1O>>[C]NCC(=[N])[CH]
4: OC1[CH]OC([CH2])C1O>>[N]=C([CH2])C(O)C(O)[CH2]
5: [CH]C([CH])O>>O=CO
6: [O]C([CH])N1C=N[C]=C1[NH]>>[C]CNC(=[C])[NH]
7: [N]C1O[CH]C(O)C1O>>O=CO
8: [CH]N1[C]=[C]N=C1>>[C]N=C(C[NH])C([CH])O

MMP Level 3
1: [C]N([CH])C1OC(C[O])C(O)C1O>>[C]C(O)C(O)C[O]
2: O>>O=CO
3: [NH]C1=[C]N=CN1C2OC([CH2])C(O)C2O>>[CH]C(O)C1=N[C]=C([NH])NC1
4: [N]C1OC(C[O])C(O)C1O>>[C]N=C(C[NH])C(O)C(O)C[O]
5: [N]C1O[CH]C(O)C1O>>O=CO
6: [C]Nc1c([C])ncn1C2O[CH][CH]C2O>>[C]NC=1NCC([CH])=NC1[C]
7: [C]N([CH])C1OC([CH2])C(O)C1O>>O=CO
8: [C]c1ncn(c1[NH])C([O])[CH]>>[C]C1=[C]NCC(=N1)C(O)C(O)[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C3O>>O=CO, O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C3O>>O=CO]
2: R:M00001, P:M00004	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C3O>>O=c1nc(N)[nH]c2NCC(=Nc12)C(O)C(O)COP(=O)(O)OP(=O)(O)OP(=O)(O)O, O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C3O>>O=c1nc(N)[nH]c2NCC(=Nc12)C(O)C(O)COP(=O)(O)OP(=O)(O)OP(=O)(O)O, O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C3O>>O=c1nc(N)[nH]c2NCC(=Nc12)C(O)C(O)COP(=O)(O)OP(=O)(O)OP(=O)(O)O, O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C3O>>O=c1nc(N)[nH]c2NCC(=Nc12)C(O)C(O)COP(=O)(O)OP(=O)(O)OP(=O)(O)O, O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C3O>>O=c1nc(N)[nH]c2NCC(=Nc12)C(O)C(O)COP(=O)(O)OP(=O)(O)OP(=O)(O)O]
3: R:M00002, P:M00003	[O>>O=CO]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[C:8]1:[N:9]:[CH:10]:[N:11]2[CH:12]3[O:13][CH:14]([CH2:15][O:16][P:17](=[O:18])([OH:19])[O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[OH:28])[CH:29]([OH:30])[CH:31]3[OH:32].[OH2:33]>>[O:32]=[CH:31][OH:33].[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]=2[NH:11][CH2:12][C:10](=[N:9][C:8]12)[CH:29]([OH:30])[CH:14]([OH:13])[CH2:15][O:16][P:17](=[O:18])([OH:19])[O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[OH:28]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=10, 2=9, 3=8, 4=7, 5=11, 6=12, 7=31, 8=29, 9=14, 10=13, 11=15, 12=16, 13=17, 14=18, 15=19, 16=20, 17=21, 18=22, 19=23, 20=24, 21=25, 22=26, 23=27, 24=28, 25=30, 26=32, 27=6, 28=4, 29=3, 30=2, 31=1, 32=5, 33=33}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=32, 2=31, 3=33, 4=9, 5=10, 6=11, 7=12, 8=7, 9=8, 10=6, 11=4, 12=3, 13=2, 14=1, 15=5, 16=13, 17=15, 18=17, 19=18, 20=19, 21=20, 22=21, 23=22, 24=23, 25=24, 26=25, 27=26, 28=27, 29=28, 30=29, 31=30, 32=16, 33=14}

