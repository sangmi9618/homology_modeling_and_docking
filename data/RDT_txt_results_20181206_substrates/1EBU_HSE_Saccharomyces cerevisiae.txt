
//
FINGERPRINTS BC

ORDER_CHANGED
[C-O*C=O:1.0]

//
FINGERPRINTS RC
[O=C(N)C=1C=[N+]C=CC1>>O=C(N)C1=C[N]C=CC1:1.0, O=C(O)C(N)C[CH2]>>[O-]C(=O)C([NH3+])C[CH]:1.0, O=C(O)C([CH2])N>>[O-]C(=O)C([CH2])[NH3+]:1.0, O=C([CH])O:1.0, O=C([CH])O>>[O-]C(=O)[CH]:1.0, O=CC[CH]:1.0, O=C[CH2]:2.0, O=[CH]:1.0, OC[CH2]:2.0, OC[CH2]>>O=C[CH2]:2.0, O[CH2]:1.0, O[CH2]>>O=[CH]:1.0, [CH2]:2.0, [CH]:2.0, [CH]CCO:1.0, [CH]CCO>>O=CC[CH]:2.0, [CH]N:1.0, [CH]N([CH])[CH]:1.0, [CH]N>>[CH][NH3+]:1.0, [CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]>>[CH]N([CH])[CH]:1.0, [CH][NH3+]:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN(C=[CH])C([O])[CH]:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])C=C[CH]>>[C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C(N)CCO>>[C]C([NH3+])CC=O:1.0, [C]C([CH2])N:1.0, [C]C([CH2])N>>[C]C([CH2])[NH3+]:1.0, [C]C([CH2])[NH3+]:1.0, [C]C=[CH]:1.0, [C]C=[CH]>>[C]C[CH]:1.0, [C]C[CH]:1.0, [C]O:1.0, [C]O>>[C][O-]:1.0, [C][O-]:1.0, [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]C1=CN(C=CC1)C2O[CH][CH]C2O:1.0, [N+]:1.0, [NH2]:1.0, [NH3+]:1.0, [N]:1.0, [O-]:1.0, [O-]C(=O)[CH]:1.0, [OH]:2.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH2]:1.0, [CH]:1.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C[CH2]:1.0, O=[CH]:1.0, OC[CH2]:1.0, O[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=CC[CH]:1.0, O=C[CH2]:1.0, OC[CH2]:1.0, [CH]CCO:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O[CH2]>>O=[CH]
2: OC[CH2]>>O=C[CH2]
3: [CH][N+]([CH])=[CH]>>[CH]N([CH])[CH]
4: [CH]N>>[CH][NH3+]
5: [C]O>>[C][O-]
6: [C]C=[CH]>>[C]C[CH]

MMP Level 2
1: OC[CH2]>>O=C[CH2]
2: [CH]CCO>>O=CC[CH]
3: [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN(C=[CH])C([O])[CH]
4: [C]C([CH2])N>>[C]C([CH2])[NH3+]
5: O=C([CH])O>>[O-]C(=O)[CH]
6: [C]C(=[CH])C=C[CH]>>[C]C(=[CH])CC=[CH]

MMP Level 3
1: [CH]CCO>>O=CC[CH]
2: [C]C(N)CCO>>[C]C([NH3+])CC=O
3: [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]C1=CN(C=CC1)C2O[CH][CH]C2O
4: O=C(O)C(N)C[CH2]>>[O-]C(=O)C([NH3+])C[CH]
5: O=C(O)C([CH2])N>>[O-]C(=O)C([CH2])[NH3+]
6: O=C(N)C=1C=[N+]C=CC1>>O=C(N)C1=C[N]C=CC1


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)CCO>>[O-]C(=O)C([NH3+])CC=O, O=C(O)C(N)CCO>>[O-]C(=O)C([NH3+])CC=O, O=C(O)C(N)CCO>>[O-]C(=O)C([NH3+])CC=O, O=C(O)C(N)CCO>>[O-]C(=O)C([NH3+])CC=O]
2: R:M00002, P:M00004	[O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O, O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O]


//
SELECTED AAM MAPPING
[O:49]=[C:50]([OH:51])[CH:52]([NH2:53])[CH2:54][CH2:55][OH:56].[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:5]:[CH:6]:[CH:7]:[N+:8](:[CH:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48]>>[O:56]=[CH:55][CH2:54][CH:52]([C:50](=[O:49])[O-:51])[NH3+:53].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:9][N:8]([CH:7]=[CH:6][CH2:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=54, 2=55, 3=56, 4=52, 5=50, 6=49, 7=51, 8=53, 9=6, 10=5, 11=4, 12=9, 13=8, 14=7, 15=10, 16=47, 17=45, 18=12, 19=11, 20=13, 21=14, 22=15, 23=16, 24=17, 25=18, 26=19, 27=20, 28=21, 29=22, 30=23, 31=24, 32=43, 33=37, 34=26, 35=25, 36=27, 37=28, 38=29, 39=30, 40=35, 41=34, 42=33, 43=32, 44=31, 45=36, 46=38, 47=39, 48=40, 49=41, 50=42, 51=44, 52=46, 53=48, 54=2, 55=1, 56=3}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=51, 2=50, 3=49, 4=52, 5=53, 6=54, 7=55, 8=56, 9=9, 10=8, 11=7, 12=6, 13=5, 14=4, 15=2, 16=1, 17=3, 18=10, 19=47, 20=45, 21=12, 22=11, 23=13, 24=14, 25=15, 26=16, 27=17, 28=18, 29=19, 30=20, 31=21, 32=22, 33=23, 34=24, 35=43, 36=37, 37=26, 38=25, 39=27, 40=28, 41=29, 42=30, 43=35, 44=34, 45=33, 46=32, 47=31, 48=36, 49=38, 50=39, 51=40, 52=41, 53=42, 54=44, 55=46, 56=48}

