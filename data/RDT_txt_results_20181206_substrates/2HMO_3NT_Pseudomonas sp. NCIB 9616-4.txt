
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:2.0, C-O:1.0, O=O:1.0]

//
FINGERPRINTS RC
[O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1:1.0, O=O:4.0, O=O>>O[CH2]:1.0, O=O>>[CH]C(=[CH])CO:1.0, O=O>>[C]CO:1.0, O[CH2]:1.0, [CH2]:2.0, [CH3]:1.0, [CH]:3.0, [CH]C(=[CH])C:2.0, [CH]C(=[CH])C>>[CH]C(=[CH])CO:1.0, [CH]C(=[CH])C>>[C]C=[CH]:1.0, [CH]C(=[CH])CO:1.0, [CH]C(=[CH])[CH2]:1.0, [CH]C=C(C=[CH])CO:1.0, [CH]C=CC(=[CH])C:1.0, [CH]C=CC(=[CH])C>>[CH]C=C(C=[CH])CO:1.0, [CH]N([CH])[CH]:1.0, [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]:1.0, [C]:2.0, [C]=1C=CC=C(C1)C>>OCC=1C=C[C]=CC1:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]C:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]:1.0, [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O:1.0, [C]C=C(C=[CH])C:1.0, [C]C=C(C=[CH])C>>[CH]C=C(C=[CH])CO:1.0, [C]C=C(C=[CH])C>>[C]C=CC(=[CH])[CH2]:1.0, [C]C=CC(=[CH])[CH2]:1.0, [C]C=[CH]:3.0, [C]C=[CH]>>[CH]C(=[CH])[CH2]:1.0, [C]C>>[C]CO:1.0, [C]CO:2.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C=[CH]:1.0, [N+]:1.0, [N+]c1cccc(c1)C>>[N+]c1ccc(cc1)CO:1.0, [N]:1.0, [OH]:1.0, [O]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[CH2]:2.0, [CH3]:1.0, [C]:2.0, [OH]:1.0, [O]:2.0]


ID=Reaction Center at Level: 1 (6)
[O=O:2.0, O[CH2]:1.0, [CH]C(=[CH])C:1.0, [CH]C(=[CH])[CH2]:1.0, [C]C:1.0, [C]CO:2.0]


ID=Reaction Center at Level: 2 (6)
[O=O:2.0, [CH]C(=[CH])C:1.0, [CH]C(=[CH])CO:2.0, [CH]C=C(C=[CH])CO:1.0, [C]C=C(C=[CH])C:1.0, [C]CO:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH]C(=[CH])C>>[C]C=[CH]
2: O=O>>O[CH2]
3: [C]C[CH]>>[C]C=[CH]
4: [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]
5: [C]C=[CH]>>[CH]C(=[CH])[CH2]
6: [C]C>>[C]CO

MMP Level 2
1: [C]C=C(C=[CH])C>>[C]C=CC(=[CH])[CH2]
2: O=O>>[C]CO
3: [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]
4: [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]
5: [CH]C=CC(=[CH])C>>[CH]C=C(C=[CH])CO
6: [CH]C(=[CH])C>>[CH]C(=[CH])CO

MMP Level 3
1: [N+]c1cccc(c1)C>>[N+]c1ccc(cc1)CO
2: O=O>>[CH]C(=[CH])CO
3: O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1
4: [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O
5: [C]=1C=CC=C(C1)C>>OCC=1C=C[C]=CC1
6: [C]C=C(C=[CH])C>>[CH]C=C(C=[CH])CO


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[[O-][N+](=O)c1cccc(c1)C>>[O-][N+](=O)c1ccc(cc1)CO, [O-][N+](=O)c1cccc(c1)C>>[O-][N+](=O)c1ccc(cc1)CO, [O-][N+](=O)c1cccc(c1)C>>[O-][N+](=O)c1ccc(cc1)CO]
2: R:M00002, P:M00005	[O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O]
3: R:M00003, P:M00004	[O=O>>[O-][N+](=O)c1ccc(cc1)CO]


//
SELECTED AAM MAPPING
[O:55]=[O:56].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:5][N:6]([CH:7]=[CH:8][CH2:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44].[O:45]=[N+:46]([O-:47])[C:48]:1:[CH:49]:[CH:50]:[CH:51]:[C:52](:[CH:53]1)[CH3:54]>>[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:9]:[CH:8]:[CH:7]:[N+:6](:[CH:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44].[O:45]=[N+:46]([O-:47])[C:48]:1:[CH:53]:[CH:52]:[C:51](:[CH:50]:[CH:49]1)[CH2:54][OH:55]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=54, 2=52, 3=53, 4=48, 5=49, 6=50, 7=51, 8=46, 9=45, 10=47, 11=9, 12=8, 13=7, 14=6, 15=5, 16=4, 17=2, 18=1, 19=3, 20=10, 21=43, 22=41, 23=12, 24=11, 25=13, 26=14, 27=15, 28=16, 29=17, 30=18, 31=19, 32=20, 33=21, 34=22, 35=23, 36=24, 37=39, 38=37, 39=26, 40=25, 41=27, 42=28, 43=29, 44=30, 45=35, 46=34, 47=33, 48=32, 49=31, 50=36, 51=38, 52=40, 53=42, 54=44, 55=55, 56=56}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=50, 2=49, 3=48, 4=53, 5=52, 6=51, 7=54, 8=55, 9=46, 10=45, 11=47, 12=6, 13=5, 14=4, 15=9, 16=8, 17=7, 18=10, 19=43, 20=41, 21=12, 22=11, 23=13, 24=14, 25=15, 26=16, 27=17, 28=18, 29=19, 30=20, 31=21, 32=22, 33=23, 34=24, 35=39, 36=37, 37=26, 38=25, 39=27, 40=28, 41=29, 42=30, 43=35, 44=34, 45=33, 46=32, 47=31, 48=36, 49=38, 50=40, 51=42, 52=44, 53=2, 54=1, 55=3}

