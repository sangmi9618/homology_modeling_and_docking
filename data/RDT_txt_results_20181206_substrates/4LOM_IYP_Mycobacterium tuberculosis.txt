
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O:3.0, O=C([CH2])CC(=[CH])[NH]:1.0, O=C([CH2])[CH2]:2.0, [CH2]:1.0, [CH]:2.0, [CH]=C([NH])C(O)C(O)[CH2]:1.0, [CH]=C([NH])C(O)C(O)[CH2]>>O:1.0, [CH]=C([NH])C(O)C(O)[CH2]>>O=C([CH2])CC(=[CH])[NH]:1.0, [CH]C(O)[CH2]:2.0, [CH]C(O)[CH2]>>O=C([CH2])[CH2]:2.0, [CH]O:2.0, [CH]O>>O:1.0, [CH]O>>[C]=O:1.0, [C]:1.0, [C]=O:1.0, [C]C(O)C(O)C[O]:1.0, [C]C(O)C(O)C[O]>>[C]CC(=O)C[O]:2.0, [C]C([CH])O:2.0, [C]C([CH])O>>O:1.0, [C]C([CH])O>>[C]C[C]:1.0, [C]CC(=O)C[O]:1.0, [C]C[C]:1.0, [OH]:2.0, [O]:1.0, [O]CC(O)C(O)c1cnc[nH]1>>[O]CC(=O)Cc1cnc[nH]1:1.0, [P]OCC(O)C(O)C(=[CH])[NH]>>O=C(CO[P])CC(=[CH])[NH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[O:1.0, [CH]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (3)
[O:1.0, [CH]O:1.0, [C]C([CH])O:1.0]


ID=Reaction Center at Level: 2 (3)
[O:1.0, [CH]=C([NH])C(O)C(O)[CH2]:1.0, [C]C([CH])O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH]:1.0, [C]:1.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([CH2])[CH2]:1.0, [CH]C(O)[CH2]:1.0, [CH]O:1.0, [C]=O:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C([CH2])[CH2]:1.0, [CH]C(O)[CH2]:1.0, [C]C(O)C(O)C[O]:1.0, [C]CC(=O)C[O]:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (3)
[[CH2]:1.0, [CH]:2.0, [C]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([CH2])[CH2]:1.0, [CH]C(O)[CH2]:1.0, [C]C([CH])O:1.0, [C]C[C]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C([CH2])CC(=[CH])[NH]:1.0, [CH]=C([NH])C(O)C(O)[CH2]:1.0, [C]C(O)C(O)C[O]:1.0, [C]CC(=O)C[O]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C([CH])O>>[C]C[C]
2: [CH]O>>O
3: [CH]O>>[C]=O
4: [CH]C(O)[CH2]>>O=C([CH2])[CH2]

MMP Level 2
1: [CH]=C([NH])C(O)C(O)[CH2]>>O=C([CH2])CC(=[CH])[NH]
2: [C]C([CH])O>>O
3: [CH]C(O)[CH2]>>O=C([CH2])[CH2]
4: [C]C(O)C(O)C[O]>>[C]CC(=O)C[O]

MMP Level 3
1: [O]CC(O)C(O)c1cnc[nH]1>>[O]CC(=O)Cc1cnc[nH]1
2: [CH]=C([NH])C(O)C(O)[CH2]>>O
3: [C]C(O)C(O)C[O]>>[C]CC(=O)C[O]
4: [P]OCC(O)C(O)C(=[CH])[NH]>>O=C(CO[P])CC(=[CH])[NH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=P(O)(O)OCC(O)C(O)c1cnc[nH]1>>O]
2: R:M00001, P:M00003	[O=P(O)(O)OCC(O)C(O)c1cnc[nH]1>>O=C(COP(=O)(O)O)Cc1cnc[nH]1, O=P(O)(O)OCC(O)C(O)c1cnc[nH]1>>O=C(COP(=O)(O)O)Cc1cnc[nH]1, O=P(O)(O)OCC(O)C(O)c1cnc[nH]1>>O=C(COP(=O)(O)O)Cc1cnc[nH]1]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH2:6][CH:7]([OH:8])[CH:9]([OH:10])[C:11]:1:[CH:12]:[N:13]:[CH:14]:[NH:15]1>>[O:8]=[C:7]([CH2:6][O:5][P:2](=[O:1])([OH:3])[OH:4])[CH2:9][C:11]:1:[CH:12]:[N:13]:[CH:14]:[NH:15]1.[OH2:10]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=12, 2=11, 3=15, 4=14, 5=13, 6=9, 7=7, 8=6, 9=5, 10=2, 11=1, 12=3, 13=4, 14=8, 15=10}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=15, 2=11, 3=10, 4=14, 5=13, 6=12, 7=9, 8=2, 9=1, 10=3, 11=4, 12=5, 13=6, 14=7, 15=8}

