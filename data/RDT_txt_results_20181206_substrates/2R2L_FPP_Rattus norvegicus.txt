
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0, C-S:1.0]

//
FINGERPRINTS RC
[S([CH2])[CH2]:1.0, S[CH2]:1.0, S[CH2]>>S([CH2])[CH2]:1.0, [CH2]:2.0, [CH]CS:1.0, [CH]CS>>[CH]CSC[CH]:1.0, [CH]CSC[CH]:1.0, [C]=CCOP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, [C]=CCO[P]:1.0, [C]=CCO[P]>>[C]=CCS[CH2]:1.0, [C]=CCS[CH2]:1.0, [C]C(N)CS>>[C]C(N)CSCC=[C]:1.0, [OH]:1.0, [O]:1.0, [O]C[CH]:1.0, [O]C[CH]>>[S]C[CH]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OCC=C([CH2])C>>[CH]CSCC=C([CH2])C:1.0, [O]P(=O)(O)OC[CH]:1.0, [O]P(=O)(O)OC[CH]>>[O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[CH2]>>[P]O:1.0, [SH]:1.0, [S]:1.0, [S]C[CH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH2]:2.0, [O]:1.0, [S]:1.0]


ID=Reaction Center at Level: 1 (4)
[S([CH2])[CH2]:1.0, [O]C[CH]:1.0, [P]O[CH2]:1.0, [S]C[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[CH]CSC[CH]:1.0, [C]=CCO[P]:1.0, [C]=CCS[CH2]:1.0, [O]P(=O)(O)OC[CH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: S[CH2]>>S([CH2])[CH2]
2: [P]O[CH2]>>[P]O
3: [O]C[CH]>>[S]C[CH]

MMP Level 2
1: [CH]CS>>[CH]CSC[CH]
2: [O]P(=O)(O)OC[CH]>>[O]P(=O)(O)O
3: [C]=CCO[P]>>[C]=CCS[CH2]

MMP Level 3
1: [C]C(N)CS>>[C]C(N)CSCC=[C]
2: [C]=CCOP(=O)(O)O[P]>>O=P(O)(O)O[P]
3: [O]P(=O)(O)OCC=C([CH2])C>>[CH]CSCC=C([CH2])C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)C>>O=C(O)C(NC(=O)C(NC(=O)C(NC(=O)C(NC(=O)C(NC(=O)C(N)CSCC=C(C)CCC=C(C)CCC=C(C)C)CC(=O)N)CO)Cc1ccc(O)cc1)CCCCN)C(O)C]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OCC=C(C)CCC=C(C)CCC=C(C)C>>O=P(O)(O)OP(=O)(O)O]
3: R:M00002, P:M00003	[O=C(O)C(NC(=O)C(NC(=O)C(NC(=O)C(NC(=O)C(NC(=O)C(N)CS)CC(=O)N)CO)Cc1ccc(O)cc1)CCCCN)C(O)C>>O=C(O)C(NC(=O)C(NC(=O)C(NC(=O)C(NC(=O)C(NC(=O)C(N)CSCC=C(C)CCC=C(C)CCC=C(C)C)CC(=O)N)CO)Cc1ccc(O)cc1)CCCCN)C(O)C]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([NH:5][C:6](=[O:7])[CH:8]([NH:9][C:10](=[O:11])[CH:12]([NH:13][C:14](=[O:15])[CH:16]([NH:17][C:18](=[O:19])[CH:20]([NH:21][C:22](=[O:23])[CH:24]([NH2:25])[CH2:26][SH:27])[CH2:28][C:29](=[O:30])[NH2:31])[CH2:32][OH:33])[CH2:34][C:35]:1:[CH:36]:[CH:37]:[C:38]([OH:39]):[CH:40]:[CH:41]1)[CH2:42][CH2:43][CH2:44][CH2:45][NH2:46])[CH:47]([OH:48])[CH3:49].[O:50]=[P:51]([OH:52])([OH:53])[O:54][P:55](=[O:56])([OH:57])[O:58][CH2:59][CH:60]=[C:61]([CH3:62])[CH2:63][CH2:64][CH:65]=[C:66]([CH3:67])[CH2:68][CH2:69][CH:70]=[C:71]([CH3:72])[CH3:73]>>[O:1]=[C:2]([OH:3])[CH:4]([NH:5][C:6](=[O:7])[CH:8]([NH:9][C:10](=[O:11])[CH:12]([NH:13][C:14](=[O:15])[CH:16]([NH:17][C:18](=[O:19])[CH:20]([NH:21][C:22](=[O:23])[CH:24]([NH2:25])[CH2:26][S:27][CH2:59][CH:60]=[C:61]([CH3:62])[CH2:63][CH2:64][CH:65]=[C:66]([CH3:67])[CH2:68][CH2:69][CH:70]=[C:71]([CH3:72])[CH3:73])[CH2:28][C:29](=[O:30])[NH2:31])[CH2:32][OH:33])[CH2:34][C:35]:1:[CH:41]:[CH:40]:[C:38]([OH:39]):[CH:37]:[CH:36]1)[CH2:42][CH2:43][CH2:44][CH2:45][NH2:46])[CH:47]([OH:48])[CH3:49].[O:50]=[P:51]([OH:52])([OH:53])[O:54][P:55](=[O:56])([OH:57])[OH:58]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=72, 2=71, 3=70, 4=69, 5=68, 6=66, 7=65, 8=64, 9=63, 10=61, 11=60, 12=59, 13=58, 14=55, 15=56, 16=57, 17=54, 18=51, 19=50, 20=52, 21=53, 22=62, 23=67, 24=73, 25=49, 26=47, 27=4, 28=2, 29=1, 30=3, 31=5, 32=6, 33=7, 34=8, 35=42, 36=43, 37=44, 38=45, 39=46, 40=9, 41=10, 42=11, 43=12, 44=34, 45=35, 46=41, 47=40, 48=38, 49=37, 50=36, 51=39, 52=13, 53=14, 54=15, 55=16, 56=32, 57=33, 58=17, 59=18, 60=19, 61=20, 62=28, 63=29, 64=30, 65=31, 66=21, 67=22, 68=23, 69=24, 70=26, 71=27, 72=25, 73=48}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=64, 2=62, 3=4, 4=2, 5=1, 6=3, 7=5, 8=6, 9=7, 10=8, 11=57, 12=58, 13=59, 14=60, 15=61, 16=9, 17=10, 18=11, 19=12, 20=49, 21=50, 22=51, 23=52, 24=53, 25=55, 26=56, 27=54, 28=13, 29=14, 30=15, 31=16, 32=47, 33=48, 34=17, 35=18, 36=19, 37=20, 38=43, 39=44, 40=45, 41=46, 42=21, 43=22, 44=23, 45=24, 46=26, 47=27, 48=28, 49=29, 50=30, 51=31, 52=32, 53=33, 54=34, 55=35, 56=36, 57=37, 58=38, 59=39, 60=40, 61=41, 62=42, 63=25, 64=63, 65=67, 66=66, 67=65, 68=68, 69=69, 70=70, 71=71, 72=72, 73=73}

