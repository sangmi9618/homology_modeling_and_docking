
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:2.0, C-O:2.0, O-P:2.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)C(N)C(O)C>>[C]NC(=O)NC(C(=O)O)C(O)C:1.0, O=C(O)C(N)C(O)C>>[C]NC(C(=O)O)C(O)C:1.0, O=C([NH])[NH]:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[CH2]:1.0, O=P(O)(O[P])O[CH2]>>O=P(O)(O)O[CH2]:1.0, [CH]:4.0, [CH]C(O)C:2.0, [CH]C(O)C>>[CH]C(O)C:1.0, [CH]N:1.0, [CH]N>>[C]N[CH]:1.0, [C]:2.0, [C]=C([N])C(=N[CH])N>>[C]=C([N])C(=N[CH])NC(=O)N[CH]:1.0, [C]C(=[N])N:1.0, [C]C(=[N])N>>[C]C(=[N])NC(=O)[NH]:1.0, [C]C(=[N])NC(=O)[NH]:1.0, [C]C(N)C(O)C:1.0, [C]C(N)C(O)C>>[C]C([NH])C(O)C:1.0, [C]C([CH])N:1.0, [C]C([CH])N>>[C]C([CH])NC(=O)[NH]:1.0, [C]C([CH])NC(=O)[NH]:1.0, [C]C([NH])C(O)C:1.0, [C]N:1.0, [C]N([CH])C1O[CH][CH]C1O:2.0, [C]N([CH])C1O[CH][CH]C1O>>[C]N([CH])C1O[CH][CH]C1O:1.0, [C]N>>[C]N[C]:1.0, [C]NC(=O)N[CH]:1.0, [C]N[CH]:1.0, [C]N[C]:1.0, [C]O:1.0, [C]O>>[P]O:1.0, [C][O-]:1.0, [C][O-]>>O:1.0, [NH2]:2.0, [NH]:2.0, [N]C([O])[CH]:2.0, [N]C([O])[CH]>>[N]C([O])[CH]:1.0, [N]C1=[C]N=CN1C2OC([CH2])C(O)C2O>>[N]C1=[C]N=CN1C2OC([CH2])C(O)C2O:1.0, [O-]:1.0, [O-]C(=O)O:4.0, [O-]C(=O)O>>O:2.0, [O-]C(=O)O>>O=C([NH])[NH]:1.0, [O-]C(=O)O>>O=P(O)(O)O[CH2]:1.0, [O-]C(=O)O>>[C]C(=[N])NC(=O)NC([C])[CH]:1.0, [O-]C(=O)O>>[C]NC(=O)N[CH]:1.0, [O-]C(=O)O>>[O]P(=O)(O)O:1.0, [OH]:3.0, [O]:1.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)OP(=O)(O)OC[CH]>>O=P(O)(O)OC[CH]:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [O]P([O])(=O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (6)
[[C]:4.0, [NH]:2.0, [O-]:1.0, [OH]:2.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (10)
[O=C([NH])[NH]:2.0, [C]N[CH]:1.0, [C]N[C]:1.0, [C]O:1.0, [C][O-]:1.0, [O-]C(=O)O:2.0, [O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (8)
[O=P(O)(O)O[CH2]:1.0, O=P(O)(O[P])O[CH2]:1.0, [C]C(=[N])NC(=O)[NH]:1.0, [C]C([CH])NC(=O)[NH]:1.0, [C]NC(=O)N[CH]:2.0, [O-]C(=O)O:4.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:4.0]


ID=Reaction Center at Level: 1 (2)
[[CH]C(O)C:2.0, [N]C([O])[CH]:2.0]


ID=Reaction Center at Level: 2 (3)
[[C]C(N)C(O)C:1.0, [C]C([NH])C(O)C:1.0, [C]N([CH])C1O[CH][CH]C1O:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: [N]C([O])[CH]>>[N]C([O])[CH]
2: [CH]C(O)C>>[CH]C(O)C
3: [O-]C(=O)O>>O=C([NH])[NH]
4: [C]N>>[C]N[C]
5: [C][O-]>>O
6: [C]O>>[P]O
7: [CH]N>>[C]N[CH]
8: [P]O[P]>>[P]O
9: [O]P([O])(=O)O>>[O]P(=O)(O)O

MMP Level 2
1: [C]N([CH])C1O[CH][CH]C1O>>[C]N([CH])C1O[CH][CH]C1O
2: [C]C(N)C(O)C>>[C]C([NH])C(O)C
3: [O-]C(=O)O>>[C]NC(=O)N[CH]
4: [C]C(=[N])N>>[C]C(=[N])NC(=O)[NH]
5: [O-]C(=O)O>>O
6: [O-]C(=O)O>>[O]P(=O)(O)O
7: [C]C([CH])N>>[C]C([CH])NC(=O)[NH]
8: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
9: O=P(O)(O[P])O[CH2]>>O=P(O)(O)O[CH2]

MMP Level 3
1: [N]C1=[C]N=CN1C2OC([CH2])C(O)C2O>>[N]C1=[C]N=CN1C2OC([CH2])C(O)C2O
2: O=C(O)C(N)C(O)C>>[C]NC(C(=O)O)C(O)C
3: [O-]C(=O)O>>[C]C(=[N])NC(=O)NC([C])[CH]
4: [C]=C([N])C(=N[CH])N>>[C]=C([N])C(=N[CH])NC(=O)N[CH]
5: [O-]C(=O)O>>O
6: [O-]C(=O)O>>O=P(O)(O)O[CH2]
7: O=C(O)C(N)C(O)C>>[C]NC(=O)NC(C(=O)O)C(O)C
8: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]
9: [O]P(=O)(O)OP(=O)(O)OC[CH]>>O=P(O)(O)OC[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C(O)C(N)C(O)C>>O=C(O)C(NC(=O)Nc1ncnc2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O)C(O)C, O=C(O)C(N)C(O)C>>O=C(O)C(NC(=O)Nc1ncnc2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O)C(O)C]
2: R:M00002, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(O)C(NC(=O)Nc1ncnc2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O)C(O)C, O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(O)C(NC(=O)Nc1ncnc2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O)C(O)C, O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(O)C(NC(=O)Nc1ncnc2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O)C(O)C]
3: R:M00002, P:M00005	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)O]
4: R:M00003, P:M00004	[[O-]C(=O)O>>O=C(O)C(NC(=O)Nc1ncnc2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O)C(O)C, [O-]C(=O)O>>O=C(O)C(NC(=O)Nc1ncnc2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O)C(O)C]
5: R:M00003, P:M00006	[[O-]C(=O)O>>O]


//
SELECTED AAM MAPPING
[O:40]=[C:41]([O-:42])[OH:43].[O:32]=[C:33]([OH:34])[CH:35]([NH2:36])[CH:37]([OH:38])[CH3:39].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:32]=[C:33]([OH:34])[CH:35]([NH:36][C:41](=[O:40])[NH:27][C:22]:1:[N:23]:[CH:24]:[N:25]:[C:26]:2:[C:21]1:[N:20]:[CH:19]:[N:18]2[CH:17]3[O:16][CH:15]([CH2:14][O:13][P:10](=[O:11])([OH:43])[OH:12])[CH:30]([OH:31])[CH:28]3[OH:29])[CH:37]([OH:38])[CH3:39].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[OH:9].[OH2:42]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=39, 2=37, 3=35, 4=33, 5=32, 6=34, 7=36, 8=38, 9=24, 10=25, 11=26, 12=21, 13=22, 14=23, 15=27, 16=20, 17=19, 18=18, 19=17, 20=28, 21=30, 22=15, 23=16, 24=14, 25=13, 26=10, 27=11, 28=12, 29=9, 30=6, 31=7, 32=8, 33=5, 34=2, 35=1, 36=3, 37=4, 38=31, 39=29, 40=41, 41=40, 42=43, 43=42}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=33, 2=31, 3=4, 4=2, 5=1, 6=3, 7=5, 8=6, 9=7, 10=8, 11=9, 12=10, 13=11, 14=12, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=29, 22=27, 23=20, 24=19, 25=21, 26=22, 27=23, 28=24, 29=25, 30=26, 31=28, 32=30, 33=32, 34=36, 35=35, 36=34, 37=37, 38=38, 39=39, 40=40, 41=41, 42=42, 43=43}

