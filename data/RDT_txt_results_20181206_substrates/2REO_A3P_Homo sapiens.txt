
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-S:2.0]

//
FINGERPRINTS RC
[O=P(O)(O[CH2])OS(=O)(=O)O>>O=P(O)(O)O[CH2]:1.0, O=S(=O)(O)OC([CH2])[CH2]:1.0, O=S(=O)(O)O[CH]:1.0, O=S(=O)(O)O[P]:1.0, O=S(=O)(O)O[P]>>O=S(=O)(O)O[CH]:1.0, OC([CH2])[CH2]:1.0, OC([CH2])[CH2]>>O=S(=O)(O)OC([CH2])[CH2]:1.0, [CH]O:1.0, [CH]O>>[S]O[CH]:1.0, [C]CC(O)C[CH2]>>[C]CC(OS(=O)(=O)O)C[CH2]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OS(=O)(=O)O:1.0, [O]P(=O)(O)OS(=O)(=O)O>>O=S(=O)(O)OC([CH2])[CH2]:1.0, [O]P(=O)(O)OS(=O)(=O)O>>[O]P(=O)(O)O:1.0, [O]S(=O)(=O)O:2.0, [O]S(=O)(=O)O>>[O]S(=O)(=O)O:1.0, [P]O:1.0, [P]O[S]:1.0, [P]O[S]>>[P]O:1.0, [S]:2.0, [S]O[CH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [S]:2.0]


ID=Reaction Center at Level: 1 (3)
[[O]S(=O)(=O)O:2.0, [P]O[S]:1.0, [S]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=S(=O)(O)OC([CH2])[CH2]:1.0, O=S(=O)(O)O[CH]:1.0, O=S(=O)(O)O[P]:1.0, [O]P(=O)(O)OS(=O)(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[S]>>[P]O
2: [O]S(=O)(=O)O>>[O]S(=O)(=O)O
3: [CH]O>>[S]O[CH]

MMP Level 2
1: [O]P(=O)(O)OS(=O)(=O)O>>[O]P(=O)(O)O
2: O=S(=O)(O)O[P]>>O=S(=O)(O)O[CH]
3: OC([CH2])[CH2]>>O=S(=O)(O)OC([CH2])[CH2]

MMP Level 3
1: O=P(O)(O[CH2])OS(=O)(=O)O>>O=P(O)(O)O[CH2]
2: [O]P(=O)(O)OS(=O)(=O)O>>O=S(=O)(O)OC([CH2])[CH2]
3: [C]CC(O)C[CH2]>>[C]CC(OS(=O)(=O)O)C[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OC1C(O)C(OC1COP(=O)(O)OS(=O)(=O)O)n2cnc3c(ncnc32)N>>O=P(O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O]
2: R:M00001, P:M00004	[O=P(O)(O)OC1C(O)C(OC1COP(=O)(O)OS(=O)(=O)O)n2cnc3c(ncnc32)N>>O=C1CCC2C3CC=C4CC(OS(=O)(=O)O)CCC4(C)C3CCC12C]
3: R:M00002, P:M00004	[O=C1CCC2C3CC=C4CC(O)CCC4(C)C3CCC12C>>O=C1CCC2C3CC=C4CC(OS(=O)(=O)O)CCC4(C)C3CCC12C]


//
SELECTED AAM MAPPING
[O:32]=[C:33]1[CH2:34][CH2:35][CH:36]2[CH:37]3[CH2:38][CH:39]=[C:40]4[CH2:41][CH:42]([OH:43])[CH2:44][CH2:45][C:46]4([CH3:47])[CH:48]3[CH2:49][CH2:50][C:51]12[CH3:52].[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH:6]1[CH:7]([OH:8])[CH:9]([O:10][CH:11]1[CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][S:18](=[O:19])(=[O:20])[OH:21])[N:22]:2:[CH:23]:[N:24]:[C:25]:3:[C:26](:[N:27]:[CH:28]:[N:29]:[C:30]32)[NH2:31]>>[O:32]=[C:33]1[CH2:34][CH2:35][CH:36]2[CH:37]3[CH2:38][CH:39]=[C:40]4[CH2:41][CH:42]([O:43][S:18](=[O:19])(=[O:20])[OH:21])[CH2:44][CH2:45][C:46]4([CH3:47])[CH:48]3[CH2:49][CH2:50][C:51]12[CH3:52].[O:15]=[P:14]([OH:17])([OH:16])[O:13][CH2:12][CH:11]1[O:10][CH:9]([N:22]:2:[CH:23]:[N:24]:[C:25]:3:[C:26](:[N:27]:[CH:28]:[N:29]:[C:30]32)[NH2:31])[CH:7]([OH:8])[CH:6]1[O:5][P:2](=[O:1])([OH:3])[OH:4]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=28, 2=29, 3=30, 4=25, 5=26, 6=27, 7=31, 8=24, 9=23, 10=22, 11=9, 12=7, 13=6, 14=11, 15=10, 16=12, 17=13, 18=14, 19=15, 20=16, 21=17, 22=18, 23=19, 24=20, 25=21, 26=5, 27=2, 28=1, 29=3, 30=4, 31=8, 32=52, 33=51, 34=50, 35=49, 36=48, 37=37, 38=36, 39=35, 40=34, 41=33, 42=32, 43=38, 44=39, 45=40, 46=46, 47=45, 48=44, 49=42, 50=41, 51=43, 52=47}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=16, 2=17, 3=18, 4=13, 5=14, 6=15, 7=19, 8=12, 9=11, 10=10, 11=9, 12=20, 13=22, 14=7, 15=8, 16=6, 17=5, 18=2, 19=1, 20=3, 21=4, 22=23, 23=24, 24=25, 25=26, 26=27, 27=21, 28=52, 29=51, 30=50, 31=49, 32=48, 33=33, 34=32, 35=31, 36=30, 37=29, 38=28, 39=34, 40=35, 41=36, 42=46, 43=45, 44=44, 45=38, 46=37, 47=39, 48=40, 49=41, 50=42, 51=43, 52=47}

