
//
FINGERPRINTS BC

FORMED_CLEAVED
[O%P:1.0, O-P:1.0]

//
FINGERPRINTS RC
[O:3.0, O=P(O)(O)O[CH]:1.0, O=P1(O)OC([CH])C([CH])O1>>O=P(O)(O)OC([CH])[CH]:1.0, O=P1(O)OC2[CH]OC([CH2])C2O1>>[O]C1[CH]OC([CH2])C1O:1.0, O=P1(O)O[CH]C([CH])O1:1.0, O=P1(O)O[CH]C([CH])O1>>[CH]C([CH])O:1.0, O=P1(O)O[CH][CH]O1:1.0, O=P1(O)O[CH][CH]O1>>O=P(O)(O)O[CH]:1.0, O>>O=P(O)(O)O[CH]:1.0, O>>[O]P(=O)(O)O:1.0, O>>[P]O:1.0, [CH]C([CH])O:1.0, [CH]O:1.0, [OH]:2.0, [O]:1.0, [O]P(=O)(O)O:2.0, [O]P([O])(=O)O:1.0, [O]P([O])(=O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH]:1.0, [P]O[CH]>>[CH]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, [O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [P]O:1.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=P(O)(O)O[CH]:1.0, O=P1(O)O[CH]C([CH])O1:1.0, O=P1(O)O[CH][CH]O1:1.0, [O]P(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O>>[P]O
2: [O]P([O])(=O)O>>[O]P(=O)(O)O
3: [P]O[CH]>>[CH]O

MMP Level 2
1: O>>[O]P(=O)(O)O
2: O=P1(O)O[CH][CH]O1>>O=P(O)(O)O[CH]
3: O=P1(O)O[CH]C([CH])O1>>[CH]C([CH])O

MMP Level 3
1: O>>O=P(O)(O)O[CH]
2: O=P1(O)OC([CH])C([CH])O1>>O=P(O)(O)OC([CH])[CH]
3: O=P1(O)OC2[CH]OC([CH2])C2O1>>[O]C1[CH]OC([CH2])C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P1(O)OC2C(OC(CO)C2O1)n3cnc4c(ncnc43)N>>O=P(O)(O)OC1C(O)C(OC1n2cnc3c(ncnc32)N)CO, O=P1(O)OC2C(OC(CO)C2O1)n3cnc4c(ncnc43)N>>O=P(O)(O)OC1C(O)C(OC1n2cnc3c(ncnc32)N)CO]
2: R:M00002, P:M00003	[O>>O=P(O)(O)OC1C(O)C(OC1n2cnc3c(ncnc32)N)CO]


//
SELECTED AAM MAPPING
[O:1]=[P:2]1([OH:3])[O:4][CH:5]2[CH:6]([O:7][CH:8]([CH2:9][OH:10])[CH:11]2[O:12]1)[N:13]:3:[CH:14]:[N:15]:[C:16]:4:[C:17](:[N:18]:[CH:19]:[N:20]:[C:21]43)[NH2:22].[OH2:23]>>[O:1]=[P:2]([OH:23])([OH:3])[O:4][CH:5]1[CH:11]([OH:12])[CH:8]([O:7][CH:6]1[N:13]:2:[CH:14]:[N:15]:[C:16]:3:[C:17](:[N:18]:[CH:19]:[N:20]:[C:21]32)[NH2:22])[CH2:9][OH:10]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=19, 2=20, 3=21, 4=16, 5=17, 6=18, 7=22, 8=15, 9=14, 10=13, 11=6, 12=5, 13=11, 14=8, 15=7, 16=9, 17=10, 18=12, 19=2, 20=1, 21=4, 22=3, 23=23}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=18, 2=19, 3=20, 4=15, 5=16, 6=17, 7=21, 8=14, 9=13, 10=12, 11=11, 12=6, 13=7, 14=9, 15=10, 16=22, 17=23, 18=8, 19=5, 20=2, 21=1, 22=3, 23=4}

