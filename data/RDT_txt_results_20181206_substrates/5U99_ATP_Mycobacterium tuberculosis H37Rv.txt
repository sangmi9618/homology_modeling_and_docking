
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

ORDER_CHANGED
[C%N*C@N:1.0, C-N*C=N:1.0]

//
FINGERPRINTS RC
[O=P(O)(O[P])OC1O[CH][CH]C1O>>O=P(O)(O)O[P]:1.0, [CH]:2.0, [C]:2.0, [C]=C([N])C(=N)N([CH])[CH]:1.0, [C]=C([N])C(=N[CH])N:1.0, [C]=C([N])C(=N[CH])N>>[C]=C([N])C(=N)N([CH])[CH]:2.0, [C]=N:1.0, [C]=N[CH]:1.0, [C]=N[CH]>>[C]N([CH])[CH]:1.0, [C]C(=N)N(C=[N])C([O])[CH]:1.0, [C]C(=NC=[N])N:1.0, [C]C(=NC=[N])N>>[C]C(=N)N(C=[N])C([O])[CH]:1.0, [C]C(=[N])N:2.0, [C]C(=[N])N>>[C]C([N])=N:2.0, [C]C([N])=N:2.0, [C]N:1.0, [C]N([CH])C1O[CH][CH]C1O:1.0, [C]N([CH])[CH]:1.0, [C]N>>[C]=N:1.0, [NH2]:1.0, [NH]:1.0, [N]:2.0, [N]1C=Nc2c1ncnc2N>>[O]C([CH])n1cnc2[N]C=Nc2c1=N:1.0, [N]C([O])[CH]:1.0, [N]C1=[C]N=CN=C1N>>[N]C1=[C]N=CN(C1=N)C2O[CH][CH]C2O:1.0, [OH]:1.0, [O]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]C([O])[CH]:1.0, [O]C([O])[CH]>>[N]C([O])[CH]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OC1OC([CH2])C(O)C1O>>[C]C(=N)N(C=[N])C1OC([CH2])C(O)C1O:1.0, [P]O:1.0, [P]OC1O[CH][CH]C1O:1.0, [P]OC1O[CH][CH]C1O>>[C]N([CH])C1O[CH][CH]C1O:1.0, [P]O[CH]:1.0, [P]O[CH]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [N]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[C]N([CH])[CH]:1.0, [N]C([O])[CH]:1.0, [O]C([O])[CH]:1.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]C(=N)N(C=[N])C([O])[CH]:1.0, [C]N([CH])C1O[CH][CH]C1O:1.0, [O]C([CH])OP([O])(=O)O:1.0, [P]OC1O[CH][CH]C1O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[C]:2.0, [NH2]:1.0, [NH]:1.0, [N]:2.0]


ID=Reaction Center at Level: 1 (6)
[[C]=N:1.0, [C]=N[CH]:1.0, [C]C(=[N])N:1.0, [C]C([N])=N:1.0, [C]N:1.0, [C]N([CH])[CH]:1.0]


ID=Reaction Center at Level: 2 (6)
[[C]=C([N])C(=N)N([CH])[CH]:1.0, [C]=C([N])C(=N[CH])N:1.0, [C]C(=N)N(C=[N])C([O])[CH]:1.0, [C]C(=NC=[N])N:1.0, [C]C(=[N])N:1.0, [C]C([N])=N:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]=N[CH]>>[C]N([CH])[CH]
2: [C]C(=[N])N>>[C]C([N])=N
3: [P]O[CH]>>[P]O
4: [C]N>>[C]=N
5: [O]C([O])[CH]>>[N]C([O])[CH]

MMP Level 2
1: [C]C(=NC=[N])N>>[C]C(=N)N(C=[N])C([O])[CH]
2: [C]=C([N])C(=N[CH])N>>[C]=C([N])C(=N)N([CH])[CH]
3: [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O
4: [C]C(=[N])N>>[C]C([N])=N
5: [P]OC1O[CH][CH]C1O>>[C]N([CH])C1O[CH][CH]C1O

MMP Level 3
1: [N]C1=[C]N=CN=C1N>>[N]C1=[C]N=CN(C1=N)C2O[CH][CH]C2O
2: [N]1C=Nc2c1ncnc2N>>[O]C([CH])n1cnc2[N]C=Nc2c1=N
3: O=P(O)(O[P])OC1O[CH][CH]C1O>>O=P(O)(O)O[P]
4: [C]=C([N])C(=N[CH])N>>[C]=C([N])C(=N)N([CH])[CH]
5: [O]P(=O)(O)OC1OC([CH2])C(O)C1O>>[C]C(=N)N(C=[N])C1OC([CH2])C(O)C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OCC1OC(n2cnc3c(ncn3C4OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C4O)c2=N)C(O)C1O, O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OCC1OC(n2cnc3c(ncn3C4OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C4O)c2=N)C(O)C1O, O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OCC1OC(n2cnc3c(ncn3C4OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C4O)c2=N)C(O)C1O]
2: R:M00002, P:M00003	[O=P(O)(O)OCC1OC(OP(=O)(O)OP(=O)(O)O)C(O)C1O>>O=P(O)(O)OP(=O)(O)O]
3: R:M00002, P:M00004	[O=P(O)(O)OCC1OC(OP(=O)(O)OP(=O)(O)O)C(O)C1O>>O=P(O)(O)OCC1OC(n2cnc3c(ncn3C4OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C4O)c2=N)C(O)C1O]


//
SELECTED AAM MAPPING
[O:32]=[P:33]([OH:34])([OH:35])[O:36][CH2:37][CH:38]1[O:39][CH:40]([O:41][P:42](=[O:43])([OH:44])[O:45][P:46](=[O:47])([OH:48])[OH:49])[CH:50]([OH:51])[CH:52]1[OH:53].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:32]=[P:33]([OH:34])([OH:35])[O:36][CH2:37][CH:38]1[O:39][CH:40]([N:23]2[CH:24]=[N:25][C:26]:3:[C:21](:[N:20]:[CH:19]:[N:18]3[CH:17]4[O:16][CH:15]([CH2:14][O:13][P:10](=[O:11])([OH:12])[O:9][P:6](=[O:7])([OH:8])[O:5][P:2](=[O:1])([OH:3])[OH:4])[CH:30]([OH:31])[CH:28]4[OH:29])[C:22]2=[NH:27])[CH:50]([OH:51])[CH:52]1[OH:53].[O:43]=[P:42]([OH:41])([OH:44])[O:45][P:46](=[O:47])([OH:48])[OH:49]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=37, 33=38, 34=52, 35=50, 36=40, 37=39, 38=41, 39=42, 40=43, 41=44, 42=45, 43=46, 44=47, 45=48, 46=49, 47=51, 48=53, 49=36, 50=33, 51=32, 52=34, 53=35}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=47, 2=46, 3=45, 4=48, 5=49, 6=50, 7=51, 8=52, 9=53, 10=16, 11=15, 12=14, 13=13, 14=17, 15=18, 16=37, 17=35, 18=20, 19=19, 20=21, 21=22, 22=23, 23=24, 24=25, 25=26, 26=27, 27=28, 28=29, 29=30, 30=31, 31=32, 32=33, 33=34, 34=36, 35=38, 36=12, 37=11, 38=10, 39=39, 40=40, 41=9, 42=41, 43=43, 44=7, 45=8, 46=6, 47=5, 48=2, 49=1, 50=3, 51=4, 52=44, 53=42}

