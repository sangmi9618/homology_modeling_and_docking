
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O:3.0, O=P(O)(O)O:3.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O:1.0, O>>O=P(O)(O)O:2.0, O>>[P]O:1.0, [OH]:2.0, [O]:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=P(O)(O)O:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=P(O)(O)O:2.0, O=P(O)(O)O[P]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]P(=O)(O)O>>O=P(O)(O)O
2: [P]O[P]>>[P]O
3: O>>[P]O

MMP Level 2
1: O=P(O)(O)O[P]>>O=P(O)(O)O
2: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
3: O>>O=P(O)(O)O

MMP Level 3
1: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O
2: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
3: O>>O=P(O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)O]
3: R:M00002, P:M00004	[O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][P:14](=[O:15])([OH:16])[O:17][CH2:18][CH:19]1[O:20][CH:21]([N:22]:2:[CH:23]:[N:24]:[C:25]:3:[C:26](:[N:27]:[CH:28]:[N:29]:[C:30]32)[NH2:31])[CH:32]([OH:33])[CH:34]1[OH:35].[OH2:36]>>[O:1]=[P:2]([OH:4])([OH:36])[OH:3].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][P:14](=[O:15])([OH:16])[O:17][CH2:18][CH:19]1[O:20][CH:21]([N:22]:2:[CH:23]:[N:24]:[C:25]:3:[C:26](:[N:27]:[CH:28]:[N:29]:[C:30]32)[NH2:31])[CH:32]([OH:33])[CH:34]1[OH:35]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=28, 2=29, 3=30, 4=25, 5=26, 6=27, 7=31, 8=24, 9=23, 10=22, 11=21, 12=32, 13=34, 14=19, 15=20, 16=18, 17=17, 18=14, 19=15, 20=16, 21=13, 22=10, 23=11, 24=12, 25=9, 26=6, 27=7, 28=8, 29=5, 30=2, 31=1, 32=3, 33=4, 34=35, 35=33, 36=36}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=34, 33=33, 34=32, 35=35, 36=36}

