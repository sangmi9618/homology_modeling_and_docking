
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O:3.0, O=P(O)(O)O:3.0, O=P(O)(O)OC[CH]:1.0, O=P(O)(O)OC[CH]>>O=P(O)(O)O:1.0, O=P(O)(O)OC[CH]>>[CH]CO:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[CH2]>>O=P(O)(O)O:1.0, O>>O=P(O)(O)O:2.0, O>>[P]O:1.0, O[CH2]:1.0, [CH]CO:1.0, [OH]:2.0, [O]:1.0, [O]C([CH])COP(=O)(O)O>>[O]C([CH])CO:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[CH2]>>O[CH2]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=P(O)(O)O:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=P(O)(O)O:2.0, O=P(O)(O)OC[CH]:1.0, O=P(O)(O)O[CH2]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O>>[P]O
2: [O]P(=O)(O)O>>O=P(O)(O)O
3: [P]O[CH2]>>O[CH2]

MMP Level 2
1: O>>O=P(O)(O)O
2: O=P(O)(O)O[CH2]>>O=P(O)(O)O
3: O=P(O)(O)OC[CH]>>[CH]CO

MMP Level 3
1: O>>O=P(O)(O)O
2: O=P(O)(O)OC[CH]>>O=P(O)(O)O
3: [O]C([CH])COP(=O)(O)O>>[O]C([CH])CO


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O>>O=c1nc(N)[nH]c2c1ncn2C3OC(CO)C(O)C3O]
2: R:M00001, P:M00004	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)O)C(O)C3O>>O=P(O)(O)O]
3: R:M00002, P:M00004	[O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[C:8]1:[N:9]:[CH:10]:[N:11]2[CH:12]3[O:13][CH:14]([CH2:15][O:16][P:17](=[O:18])([OH:19])[OH:20])[CH:21]([OH:22])[CH:23]3[OH:24].[OH2:25]>>[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[C:8]1:[N:9]:[CH:10]:[N:11]2[CH:12]3[O:13][CH:14]([CH2:15][OH:16])[CH:21]([OH:22])[CH:23]3[OH:24].[O:18]=[P:17]([OH:20])([OH:25])[OH:19]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=10, 2=9, 3=8, 4=7, 5=11, 6=12, 7=23, 8=21, 9=14, 10=13, 11=15, 12=16, 13=17, 14=18, 15=19, 16=20, 17=22, 18=24, 19=6, 20=4, 21=3, 22=2, 23=1, 24=5, 25=25}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=10, 2=9, 3=8, 4=7, 5=11, 6=12, 7=19, 8=17, 9=14, 10=13, 11=15, 12=16, 13=18, 14=20, 15=6, 16=4, 17=3, 18=2, 19=1, 20=5, 21=23, 22=22, 23=21, 24=24, 25=25}

