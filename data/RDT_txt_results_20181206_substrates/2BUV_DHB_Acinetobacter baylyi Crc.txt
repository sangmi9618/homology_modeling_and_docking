
//
FINGERPRINTS BC

FORMED_CLEAVED
[C=O:2.0, C@C:1.0, O=O:1.0]

//
FINGERPRINTS RC
[O=C(O)C=[CH]:1.0, O=C([CH])O:4.0, O=O:4.0, O=O>>O=C(O)C=[CH]:1.0, O=O>>O=C([CH])O:2.0, O=O>>[C]=CC(=O)O:1.0, O=O>>[C]=O:2.0, OC=1C=[C]C=CC1O>>[C]C=CC(=O)O:1.0, [CH]C(O)=C(O)C=[CH]:1.0, [CH]C(O)=C(O)C=[CH]>>O=C(O)C=[CH]:1.0, [C]:4.0, [C]=C([CH])O:2.0, [C]=C([CH])O>>O=C([CH])O:2.0, [C]=CC(=O)O:1.0, [C]=CC(O)=C([CH])O:1.0, [C]=CC(O)=C([CH])O>>[C]=CC(=O)O:1.0, [C]=O:2.0, [C]c1ccc(O)c(O)c1>>[C]C([CH])=CC(=O)O:1.0, [O]:4.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[C]:4.0, [O]:4.0]


ID=Reaction Center at Level: 1 (4)
[O=C([CH])O:2.0, O=O:2.0, [C]=C([CH])O:2.0, [C]=O:2.0]


ID=Reaction Center at Level: 2 (6)
[O=C(O)C=[CH]:1.0, O=C([CH])O:2.0, O=O:2.0, [CH]C(O)=C(O)C=[CH]:1.0, [C]=CC(=O)O:1.0, [C]=CC(O)=C([CH])O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]=C([CH])O>>O=C([CH])O
2: O=O>>[C]=O
3: O=O>>[C]=O
4: [C]=C([CH])O>>O=C([CH])O

MMP Level 2
1: [C]=CC(O)=C([CH])O>>[C]=CC(=O)O
2: O=O>>O=C([CH])O
3: O=O>>O=C([CH])O
4: [CH]C(O)=C(O)C=[CH]>>O=C(O)C=[CH]

MMP Level 3
1: [C]c1ccc(O)c(O)c1>>[C]C([CH])=CC(=O)O
2: O=O>>O=C(O)C=[CH]
3: O=O>>[C]=CC(=O)O
4: OC=1C=[C]C=CC1O>>[C]C=CC(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=O>>O=C(O)C=CC(=CC(=O)O)C(=O)O, O=O>>O=C(O)C=CC(=CC(=O)O)C(=O)O]
2: R:M00002, P:M00003	[O=C(O)c1ccc(O)c(O)c1>>O=C(O)C=CC(=CC(=O)O)C(=O)O, O=C(O)c1ccc(O)c(O)c1>>O=C(O)C=CC(=CC(=O)O)C(=O)O]


//
SELECTED AAM MAPPING
[O:12]=[O:13].[O:1]=[C:2]([OH:3])[C:4]:1:[CH:5]:[CH:6]:[C:7]([OH:8]):[C:9]([OH:10]):[CH:11]1>>[O:12]=[C:7]([OH:8])[CH:6]=[CH:5][C:4](=[CH:11][C:9](=[O:13])[OH:10])[C:2](=[O:1])[OH:3]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=12, 2=13, 3=5, 4=6, 5=7, 6=9, 7=11, 8=4, 9=2, 10=1, 11=3, 12=10, 13=8}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=5, 2=4, 3=2, 4=1, 5=3, 6=6, 7=7, 8=8, 9=9, 10=10, 11=11, 12=12, 13=13}

