
//
FINGERPRINTS BC

FORMED_CLEAVED
[O%P:1.0, O-P:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=P(O)(O[CH2])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]:1.0, O=P(O)(O[P])O[CH2]:2.0, O=P(O)(O[P])O[CH2]>>O=P(O)(O[P])O[CH2]:1.0, [C]:2.0, [C]OP(=O)(O)O>>[C]OP(=O)(O)OP(=O)(O)O[CH2]:1.0, [OH]:2.0, [O]:2.0, [O]C([CH])([CH2])C:2.0, [O]C([CH])([CH2])C>>[O]C([CH])([CH2])C:1.0, [O]CC(O)C(OP(=O)(O)O)(C)CO>>[O]CC(O)C(OP([O])(=O)O)(C)CO:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>[O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP(=O)(O)OC[CH]>>[O]P(=O)(O)OP(=O)(O)OC[CH]:1.0, [O]P(=O)(O)OP([O])(=O)O:2.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:2.0, [O]P([O])(=O)O>>[O]P([O])(=O)O:1.0, [P]:2.0, [P]O:2.0, [P]O>>[P]O[P]:1.0, [P]OC(C)(CO)C(O)[CH2]:2.0, [P]OC(C)(CO)C(O)[CH2]>>[P]OC(C)(CO)C(O)[CH2]:1.0, [P]O[P]:2.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]P([O])(=O)O:2.0, [P]O[P]:2.0]


ID=Reaction Center at Level: 2 (2)
[O=P(O)(O[P])O[CH2]:2.0, [O]P(=O)(O)OP([O])(=O)O:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[C]:2.0]


ID=Reaction Center at Level: 1 (1)
[[O]C([CH])([CH2])C:2.0]


ID=Reaction Center at Level: 2 (1)
[[P]OC(C)(CO)C(O)[CH2]:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: [O]C([CH])([CH2])C>>[O]C([CH])([CH2])C
2: [P]O[P]>>[P]O
3: [O]P([O])(=O)O>>[O]P([O])(=O)O
4: [P]O>>[P]O[P]

MMP Level 2
1: [P]OC(C)(CO)C(O)[CH2]>>[P]OC(C)(CO)C(O)[CH2]
2: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
3: O=P(O)(O[P])O[CH2]>>O=P(O)(O[P])O[CH2]
4: [O]P(=O)(O)O>>[O]P(=O)(O)OP([O])(=O)O

MMP Level 3
1: [O]CC(O)C(OP(=O)(O)O)(C)CO>>[O]CC(O)C(OP([O])(=O)O)(C)CO
2: O=P(O)(O[CH2])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]
3: [O]P(=O)(O)OP(=O)(O)OC[CH]>>[O]P(=O)(O)OP(=O)(O)OC[CH]
4: [C]OP(=O)(O)O>>[C]OP(=O)(O)OP(=O)(O)O[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)OCC(O)C(OP(=O)(O)O)(C)CO)C(O)C2O>>O=P1(O)OCC(O)C(OP(=O)(O)O1)(C)CO, O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)OCC(O)C(OP(=O)(O)O)(C)CO)C(O)C2O>>O=P1(O)OCC(O)C(OP(=O)(O)O1)(C)CO, O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)OCC(O)C(OP(=O)(O)O)(C)CO)C(O)C2O>>O=P1(O)OCC(O)C(OP(=O)(O)O1)(C)CO]
2: R:M00001, P:M00003	[O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)OCC(O)C(OP(=O)(O)O)(C)CO)C(O)C2O>>O=c1nc(N)ccn1C2OC(COP(=O)(O)O)C(O)C2O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[CH:6]=[CH:7][N:8]1[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][CH2:22][CH:23]([OH:24])[C:25]([O:26][P:27](=[O:28])([OH:29])[OH:30])([CH3:31])[CH2:32][OH:33])[CH:34]([OH:35])[CH:36]2[OH:37]>>[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[CH:6]=[CH:7][N:8]1[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[OH:17])[CH:34]([OH:35])[CH:36]2[OH:37].[O:19]=[P:18]1([OH:20])[O:21][CH2:22][CH:23]([OH:24])[C:25]([O:26][P:27](=[O:28])([OH:29])[O:30]1)([CH3:31])[CH2:32][OH:33]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=31, 2=25, 3=32, 4=33, 5=23, 6=22, 7=21, 8=18, 9=19, 10=20, 11=17, 12=14, 13=15, 14=16, 15=13, 16=12, 17=11, 18=34, 19=36, 20=9, 21=10, 22=8, 23=7, 24=6, 25=4, 26=3, 27=2, 28=1, 29=5, 30=37, 31=35, 32=24, 33=26, 34=27, 35=28, 36=29, 37=30}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=35, 2=29, 3=27, 4=26, 5=25, 6=23, 7=22, 8=34, 9=31, 10=32, 11=30, 12=33, 13=24, 14=28, 15=36, 16=37, 17=6, 18=7, 19=8, 20=2, 21=1, 22=3, 23=4, 24=5, 25=9, 26=20, 27=18, 28=11, 29=10, 30=12, 31=13, 32=14, 33=15, 34=16, 35=17, 36=19, 37=21}

