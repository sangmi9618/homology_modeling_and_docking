
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(N([CH])[CH2])C([CH2])N:1.0, O=C(N([CH])[CH2])C([CH2])N>>O=C(O)C([CH2])N:1.0, O=C(O)C([CH2])N:1.0, O=C([CH])O:2.0, O=C([NH])C1N(C(=O)C([CH2])N)CCC1>>O=C([NH])C1NCCC1:1.0, O>>O=C(O)C([CH2])N:1.0, O>>O=C([CH])O:1.0, O>>[C]O:1.0, [CH]N[CH2]:1.0, [C]:2.0, [C]C1N(C(=O)C(N)C[CH2])CCC1>>O=C(O)C(N)C[CH2]:1.0, [C]C1N(C(=O)[CH])CCC1:1.0, [C]C1N(C(=O)[CH])CCC1>>[C]C1NCCC1:1.0, [C]C1NCCC1:1.0, [C]N([CH])[CH2]:1.0, [C]N([CH])[CH2]>>[CH]N[CH2]:1.0, [C]O:1.0, [NH]:1.0, [N]:1.0, [N]C(=O)[CH]:1.0, [N]C(=O)[CH]>>O=C([CH])O:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [C]:2.0, [N]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=C([CH])O:1.0, [C]N([CH])[CH2]:1.0, [C]O:1.0, [N]C(=O)[CH]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=C(N([CH])[CH2])C([CH2])N:1.0, O=C(O)C([CH2])N:1.0, O=C([CH])O:1.0, [C]C1N(C(=O)[CH])CCC1:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [N]C(=O)[CH]>>O=C([CH])O
2: O>>[C]O
3: [C]N([CH])[CH2]>>[CH]N[CH2]

MMP Level 2
1: O=C(N([CH])[CH2])C([CH2])N>>O=C(O)C([CH2])N
2: O>>O=C([CH])O
3: [C]C1N(C(=O)[CH])CCC1>>[C]C1NCCC1

MMP Level 3
1: [C]C1N(C(=O)C(N)C[CH2])CCC1>>O=C(O)C(N)C[CH2]
2: O>>O=C(O)C([CH2])N
3: O=C([NH])C1N(C(=O)C([CH2])N)CCC1>>O=C([NH])C1NCCC1


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)CNC(=O)CNC(=O)C1N(C(=O)C(N)CCSC)CCC1>>O=C(O)C(N)CCSC]
2: R:M00001, P:M00004	[O=C(O)CNC(=O)CNC(=O)C1N(C(=O)C(N)CCSC)CCC1>>O=C(O)CNC(=O)CNC(=O)C1NCCC1]
3: R:M00002, P:M00003	[O>>O=C(O)C(N)CCSC]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH2:4][NH:5][C:6](=[O:7])[CH2:8][NH:9][C:10](=[O:11])[CH:12]1[N:13]([C:14](=[O:15])[CH:16]([NH2:17])[CH2:18][CH2:19][S:20][CH3:21])[CH2:22][CH2:23][CH2:24]1.[OH2:25]>>[O:1]=[C:2]([OH:3])[CH2:4][NH:5][C:6](=[O:7])[CH2:8][NH:9][C:10](=[O:11])[CH:12]1[NH:13][CH2:22][CH2:23][CH2:24]1.[O:15]=[C:14]([OH:25])[CH:16]([NH2:17])[CH2:18][CH2:19][S:20][CH3:21]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=21, 2=20, 3=19, 4=18, 5=16, 6=14, 7=15, 8=13, 9=22, 10=23, 11=24, 12=12, 13=10, 14=11, 15=9, 16=8, 17=6, 18=7, 19=5, 20=4, 21=2, 22=1, 23=3, 24=17, 25=25}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=25, 2=24, 3=23, 4=22, 5=20, 6=18, 7=17, 8=19, 9=21, 10=15, 11=16, 12=12, 13=13, 14=14, 15=10, 16=11, 17=9, 18=8, 19=6, 20=7, 21=5, 22=4, 23=2, 24=1, 25=3}

