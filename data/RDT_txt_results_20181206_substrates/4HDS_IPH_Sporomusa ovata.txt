
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[[CH]:2.0, [CH]C(=[CH])O:1.0, [CH]C(=[CH])O>>[O]C([CH])OC([CH])=[CH]:1.0, [CH]C=C(O)C=[CH]>>[CH]C=C(OC1O[CH][CH]C1O)C=[CH]:1.0, [CH]N=[CH]:1.0, [CH][N+](=[CH])C1O[CH][CH]C1O:1.0, [CH][N+](=[CH])C1O[CH][CH]C1O>>[C]OC1O[CH][CH]C1O:1.0, [CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]>>[CH]N=[CH]:1.0, [C]=CN=C[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN=C[CH]:1.0, [C]=C[N+](=C[CH])C1OC([CH2])C(O)C1O>>[CH]C(=[CH])OC1OC([CH2])C(O)C1O:1.0, [C]O:1.0, [C]O>>[C]O[CH]:1.0, [C]OC1O[CH][CH]C1O:1.0, [C]O[CH]:1.0, [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]c1cnccc1:1.0, [N+]:1.0, [N+]C([O])[CH]:1.0, [N+]C([O])[CH]>>[O]C([O])[CH]:1.0, [N]:1.0, [OH]:1.0, [O]:1.0, [O]C([CH])OC([CH])=[CH]:1.0, [O]C([O])[CH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [N+]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[CH][N+]([CH])=[CH]:1.0, [C]O[CH]:1.0, [N+]C([O])[CH]:1.0, [O]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[CH][N+](=[CH])C1O[CH][CH]C1O:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]OC1O[CH][CH]C1O:1.0, [O]C([CH])OC([CH])=[CH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]O>>[C]O[CH]
2: [N+]C([O])[CH]>>[O]C([O])[CH]
3: [CH][N+]([CH])=[CH]>>[CH]N=[CH]

MMP Level 2
1: [CH]C(=[CH])O>>[O]C([CH])OC([CH])=[CH]
2: [CH][N+](=[CH])C1O[CH][CH]C1O>>[C]OC1O[CH][CH]C1O
3: [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN=C[CH]

MMP Level 3
1: [CH]C=C(O)C=[CH]>>[CH]C=C(OC1O[CH][CH]C1O)C=[CH]
2: [C]=C[N+](=C[CH])C1OC([CH2])C(O)C1O>>[CH]C(=[CH])OC1OC([CH2])C(O)C1O
3: [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]c1cnccc1


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[Oc1ccccc1>>O=P(O)(O)OCC1OC(Oc2ccccc2)C(O)C1O]
2: R:M00002, P:M00003	[O=C(O)c1ccc[n+](c1)C2OC(COP(=O)(O)O)C(O)C2O>>O=C(O)c1cnccc1]
3: R:M00002, P:M00004	[O=C(O)c1ccc[n+](c1)C2OC(COP(=O)(O)O)C(O)C2O>>O=P(O)(O)OCC1OC(Oc2ccccc2)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[C:4]:1:[CH:5]:[CH:6]:[CH:7]:[N+:8](:[CH:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[OH:18])[CH:19]([OH:20])[CH:21]2[OH:22].[OH:23][C:24]:1:[CH:25]:[CH:26]:[CH:27]:[CH:28]:[CH:29]1>>[O:1]=[C:2]([OH:3])[C:4]:1:[CH:9]:[N:8]:[CH:7]:[CH:6]:[CH:5]1.[O:16]=[P:15]([OH:18])([OH:17])[O:14][CH2:13][CH:12]1[O:11][CH:10]([O:23][C:24]:2:[CH:29]:[CH:28]:[CH:27]:[CH:26]:[CH:25]2)[CH:21]([OH:22])[CH:19]1[OH:20]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=27, 2=26, 3=25, 4=24, 5=29, 6=28, 7=23, 8=6, 9=5, 10=4, 11=9, 12=8, 13=7, 14=10, 15=21, 16=19, 17=12, 18=11, 19=13, 20=14, 21=15, 22=16, 23=17, 24=18, 25=20, 26=22, 27=2, 28=1, 29=3}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=28, 2=29, 3=24, 4=25, 5=26, 6=27, 7=22, 8=21, 9=23, 10=14, 11=13, 12=12, 13=11, 14=16, 15=15, 16=10, 17=9, 18=17, 19=19, 20=7, 21=8, 22=6, 23=5, 24=2, 25=1, 26=3, 27=4, 28=20, 29=18}

