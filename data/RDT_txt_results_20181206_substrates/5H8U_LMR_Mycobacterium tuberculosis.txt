
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0, C-O:1.0, C-S:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)C[CH]:1.0, O=C(O)[CH2]:2.0, O=C(SCC[NH])C>>[NH]CCS:1.0, O=C(SC[CH2])C:1.0, O=C(SC[CH2])C>>SC[CH2]:1.0, O=C(SC[CH2])C>>[C]C(O)CC(=O)O:1.0, O=C(S[CH2])C:1.0, O=C(S[CH2])C>>O=C(O)CC(O)C(=O)O:1.0, O=C(S[CH2])C>>O=C(O)C[CH]:1.0, O=C([S])C:2.0, O=C([S])C>>O=C(O)[CH2]:1.0, O=C([S])C>>[C]C(O)CC(=O)O:1.0, O=CC(=O)O:1.0, O=CC(=O)O>>O=C(O)CC(O)C(=O)O:1.0, O=CC(=O)O>>[C]CC(O)C(=O)O:2.0, O=[CH]:1.0, O=[CH]>>[CH]O:1.0, O>>O=C(O)C[CH]:1.0, O>>O=C(O)[CH2]:1.0, O>>[C]O:1.0, SC[CH2]:1.0, S[CH2]:1.0, [CH2]:1.0, [CH3]:1.0, [CH]:2.0, [CH]O:1.0, [C]:2.0, [C]C:1.0, [C]C(O)CC(=O)O:1.0, [C]C(O)[CH2]:2.0, [C]C=O:2.0, [C]C=O>>[C]C(O)[CH2]:2.0, [C]C>>[C]C[CH]:1.0, [C]CC(O)C(=O)O:1.0, [C]C[CH]:1.0, [C]O:1.0, [C]S[CH2]:1.0, [C]S[CH2]>>S[CH2]:1.0, [OH]:2.0, [O]:1.0, [SH]:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (6)
[O:1.0, [CH2]:1.0, [CH]:1.0, [C]:2.0, [OH]:1.0, [S]:1.0]


ID=Reaction Center at Level: 1 (7)
[O:1.0, O=C(O)[CH2]:1.0, O=C([S])C:1.0, [C]C(O)[CH2]:1.0, [C]C[CH]:1.0, [C]O:1.0, [C]S[CH2]:1.0]


ID=Reaction Center at Level: 2 (7)
[O:1.0, O=C(O)C[CH]:1.0, O=C(O)[CH2]:1.0, O=C(SC[CH2])C:1.0, O=C(S[CH2])C:1.0, [C]C(O)CC(=O)O:1.0, [C]CC(O)C(=O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=[CH]:1.0, [CH]O:1.0, [C]C(O)[CH2]:1.0, [C]C=O:1.0]


ID=Reaction Center at Level: 2 (4)
[O=CC(=O)O:1.0, [C]C(O)[CH2]:1.0, [C]C=O:1.0, [C]CC(O)C(=O)O:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(O)[CH2]:1.0, [C]C=O:1.0]


ID=Reaction Center at Level: 2 (2)
[O=CC(=O)O:1.0, [C]CC(O)C(=O)O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C>>[C]C[CH]
2: [C]S[CH2]>>S[CH2]
3: O=C([S])C>>O=C(O)[CH2]
4: O>>[C]O
5: O=[CH]>>[CH]O
6: [C]C=O>>[C]C(O)[CH2]

MMP Level 2
1: O=C([S])C>>[C]C(O)CC(=O)O
2: O=C(SC[CH2])C>>SC[CH2]
3: O=C(S[CH2])C>>O=C(O)C[CH]
4: O>>O=C(O)[CH2]
5: [C]C=O>>[C]C(O)[CH2]
6: O=CC(=O)O>>[C]CC(O)C(=O)O

MMP Level 3
1: O=C(S[CH2])C>>O=C(O)CC(O)C(=O)O
2: O=C(SCC[NH])C>>[NH]CCS
3: O=C(SC[CH2])C>>[C]C(O)CC(=O)O
4: O>>O=C(O)C[CH]
5: O=CC(=O)O>>[C]CC(O)C(=O)O
6: O=CC(=O)O>>O=C(O)CC(O)C(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C>>O=C(O)CC(O)C(=O)O, O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C>>O=C(O)CC(O)C(=O)O]
2: R:M00001, P:M00005	[O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C>>O=C(NCCS)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O]
3: R:M00002, P:M00004	[O>>O=C(O)CC(O)C(=O)O]
4: R:M00003, P:M00004	[O=CC(=O)O>>O=C(O)CC(O)C(=O)O, O=CC(=O)O>>O=C(O)CC(O)C(=O)O]


//
SELECTED AAM MAPPING
[O:52]=[CH:53][C:54](=[O:55])[OH:56].[O:1]=[C:2]([S:3][CH2:4][CH2:5][NH:6][C:7](=[O:8])[CH2:9][CH2:10][NH:11][C:12](=[O:13])[CH:14]([OH:15])[C:16]([CH3:17])([CH3:18])[CH2:19][O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[O:28][CH2:29][CH:30]1[O:31][CH:32]([N:33]:2:[CH:34]:[N:35]:[C:36]:3:[C:37](:[N:38]:[CH:39]:[N:40]:[C:41]32)[NH2:42])[CH:43]([OH:44])[CH:45]1[O:46][P:47](=[O:48])([OH:49])[OH:50])[CH3:51].[OH2:57]>>[O:1]=[C:2]([OH:57])[CH2:51][CH:53]([OH:52])[C:54](=[O:55])[OH:56].[O:8]=[C:7]([NH:6][CH2:5][CH2:4][SH:3])[CH2:9][CH2:10][NH:11][C:12](=[O:13])[CH:14]([OH:15])[C:16]([CH3:17])([CH3:18])[CH2:19][O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[O:28][CH2:29][CH:30]1[O:31][CH:32]([N:33]:2:[CH:34]:[N:35]:[C:36]:3:[C:37](:[N:38]:[CH:39]:[N:40]:[C:41]32)[NH2:42])[CH:43]([OH:44])[CH:45]1[O:46][P:47](=[O:48])([OH:49])[OH:50]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=51, 2=2, 3=1, 4=3, 5=4, 6=5, 7=6, 8=7, 9=8, 10=9, 11=10, 12=11, 13=12, 14=13, 15=14, 16=16, 17=17, 18=18, 19=19, 20=20, 21=21, 22=22, 23=23, 24=24, 25=25, 26=26, 27=27, 28=28, 29=29, 30=30, 31=45, 32=43, 33=32, 34=31, 35=33, 36=34, 37=35, 38=36, 39=41, 40=40, 41=39, 42=38, 43=37, 44=42, 45=44, 46=46, 47=47, 48=48, 49=49, 50=50, 51=15, 52=57, 53=53, 54=52, 55=54, 56=55, 57=56}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=52, 2=53, 3=55, 4=56, 5=57, 6=54, 7=50, 8=49, 9=51, 10=15, 11=14, 12=16, 13=17, 14=18, 15=19, 16=20, 17=21, 18=22, 19=23, 20=24, 21=25, 22=26, 23=27, 24=28, 25=43, 26=41, 27=30, 28=29, 29=31, 30=32, 31=33, 32=34, 33=39, 34=38, 35=37, 36=36, 37=35, 38=40, 39=42, 40=44, 41=45, 42=46, 43=47, 44=48, 45=12, 46=10, 47=11, 48=9, 49=8, 50=7, 51=2, 52=1, 53=3, 54=4, 55=5, 56=6, 57=13}

