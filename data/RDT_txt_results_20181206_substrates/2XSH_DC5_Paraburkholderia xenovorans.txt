
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-Cl:1.0, C-O:2.0, O=O:1.0]

//
FINGERPRINTS RC
[O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1:1.0, O=O:4.0, O=O>>[C]=C(O)C(O)=C[CH]:1.0, O=O>>[C]C(=[CH])O:1.0, O=O>>[C]C(=[C])O:1.0, O=O>>[C]C([CH])=C(O)C(=[CH])O:1.0, O=O>>[C]O:2.0, [CH2]:1.0, [CH]:4.0, [CH]C=[CH]:1.0, [CH]C=[CH]>>[C]C(=[CH])O:1.0, [CH]N([CH])[CH]:1.0, [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]:1.0, [C]:3.0, [C]=C(O)C(O)=C[CH]:1.0, [C]=CC=C[CH]:1.0, [C]=CC=C[CH]>>[C]=C(O)C(O)=C[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[CH]:1.0, [C]=C[CH]>>[C]C(=[C])O:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]C(=[CH])C(Cl)=C[CH]:1.0, [C]C(=[CH])C(Cl)=C[CH]>>[C]C(=[C])C=C[CH]:1.0, [C]C(=[CH])C(Cl)=C[CH]>>[Cl-]:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])O:2.0, [C]C(=[CH])c1ccccc1Cl>>[C]C(=[CH])C1=CC=C[C]=C1O:1.0, [C]C(=[CH])c1ccccc1Cl>>[C]C(=[CH])c1cccc(O)c1O:1.0, [C]C(=[C])C=C[CH]:1.0, [C]C(=[C])O:2.0, [C]C(Cl)=[CH]:2.0, [C]C(Cl)=[CH]>>[C]C=[CH]:1.0, [C]C(Cl)=[CH]>>[Cl-]:1.0, [C]C([CH])=C(O)C(=[CH])O:1.0, [C]C([C])=CC=[CH]:1.0, [C]C([C])=CC=[CH]>>[C]C([CH])=C(O)C(=[CH])O:1.0, [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O:1.0, [C]C1=[C]C=CC=C1>>[C]c1cccc(O)c1O:1.0, [C]C=[CH]:2.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C=[CH]:1.0, [C]Cl:1.0, [C]Cl>>[Cl-]:1.0, [C]O:2.0, [Cl-]:3.0, [Cl]:1.0, [N+]:1.0, [N]:1.0, [OH]:2.0, [O]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[C]:3.0, [Cl-]:1.0, [Cl]:1.0, [OH]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (7)
[O=O:2.0, [C]C(=[CH])O:1.0, [C]C(=[C])O:1.0, [C]C(Cl)=[CH]:1.0, [C]Cl:1.0, [C]O:2.0, [Cl-]:1.0]


ID=Reaction Center at Level: 2 (8)
[O=O:2.0, [C]=C(O)C(O)=C[CH]:1.0, [C]C(=[CH])C(Cl)=C[CH]:1.0, [C]C(=[CH])O:1.0, [C]C(=[C])O:1.0, [C]C(Cl)=[CH]:1.0, [C]C([CH])=C(O)C(=[CH])O:1.0, [Cl-]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH]C=[CH]>>[C]C(=[CH])O
2: [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]
3: O=O>>[C]O
4: [C]Cl>>[Cl-]
5: [C]=C[CH]>>[C]C(=[C])O
6: [C]C[CH]>>[C]C=[CH]
7: [C]C(Cl)=[CH]>>[C]C=[CH]
8: O=O>>[C]O

MMP Level 2
1: [C]=CC=C[CH]>>[C]=C(O)C(O)=C[CH]
2: [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]
3: O=O>>[C]C(=[C])O
4: [C]C(Cl)=[CH]>>[Cl-]
5: [C]C([C])=CC=[CH]>>[C]C([CH])=C(O)C(=[CH])O
6: [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]
7: [C]C(=[CH])C(Cl)=C[CH]>>[C]C(=[C])C=C[CH]
8: O=O>>[C]C(=[CH])O

MMP Level 3
1: [C]C1=[C]C=CC=C1>>[C]c1cccc(O)c1O
2: [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O
3: O=O>>[C]C([CH])=C(O)C(=[CH])O
4: [C]C(=[CH])C(Cl)=C[CH]>>[Cl-]
5: [C]C(=[CH])c1ccccc1Cl>>[C]C(=[CH])c1cccc(O)c1O
6: O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1
7: [C]C(=[CH])c1ccccc1Cl>>[C]C(=[CH])C1=CC=C[C]=C1O
8: O=O>>[C]=C(O)C(O)=C[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00005	[Clc1ccccc1-c2ccccc2Cl>>Clc1ccccc1-c2cccc(O)c2O, Clc1ccccc1-c2ccccc2Cl>>Clc1ccccc1-c2cccc(O)c2O, Clc1ccccc1-c2ccccc2Cl>>Clc1ccccc1-c2cccc(O)c2O]
2: R:M00001, P:M00007	[Clc1ccccc1-c2ccccc2Cl>>[Cl-]]
3: R:M00002, P:M00006	[O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O]
4: R:M00004, P:M00005	[O=O>>Clc1ccccc1-c2cccc(O)c2O, O=O>>Clc1ccccc1-c2cccc(O)c2O]


//
SELECTED AAM MAPPING
[H+:61].[O:59]=[O:60].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:5][N:6]([CH:7]=[CH:8][CH2:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44].[Cl:45][C:46]:1:[CH:47]:[CH:48]:[CH:49]:[CH:50]:[C:51]1[C:52]:2:[CH:53]:[CH:54]:[CH:55]:[CH:56]:[C:57]2[Cl:58]>>[Cl-:45].[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:9]:[CH:8]:[CH:7]:[N+:6](:[CH:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44].[Cl:58][C:57]:1:[CH:56]:[CH:55]:[CH:54]:[CH:53]:[C:52]1[C:51]:2:[CH:46]:[CH:47]:[CH:48]:[C:49]([OH:59]):[C:50]2[OH:60]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=54, 2=55, 3=56, 4=57, 5=52, 6=53, 7=51, 8=50, 9=49, 10=48, 11=47, 12=46, 13=45, 14=58, 15=9, 16=8, 17=7, 18=6, 19=5, 20=4, 21=2, 22=1, 23=3, 24=10, 25=43, 26=41, 27=12, 28=11, 29=13, 30=14, 31=15, 32=16, 33=17, 34=18, 35=19, 36=20, 37=21, 38=22, 39=23, 40=24, 41=39, 42=37, 43=26, 44=25, 45=27, 46=28, 47=29, 48=30, 49=35, 50=34, 51=33, 52=32, 53=31, 54=36, 55=38, 56=40, 57=42, 58=44, 59=61, 60=59, 61=60}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=49, 2=48, 3=47, 4=46, 5=51, 6=50, 7=52, 8=58, 9=56, 10=55, 11=54, 12=53, 13=57, 14=59, 15=45, 16=6, 17=5, 18=4, 19=9, 20=8, 21=7, 22=10, 23=43, 24=41, 25=12, 26=11, 27=13, 28=14, 29=15, 30=16, 31=17, 32=18, 33=19, 34=20, 35=21, 36=22, 37=23, 38=24, 39=39, 40=37, 41=26, 42=25, 43=27, 44=28, 45=29, 46=30, 47=35, 48=34, 49=33, 50=32, 51=31, 52=36, 53=38, 54=40, 55=42, 56=44, 57=2, 58=1, 59=3, 60=60}

