
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0, O-P:2.0]

ORDER_CHANGED
[C-C*C=C:1.0, C-O*C=O:2.0]

//
FINGERPRINTS RC
[O=C(O)C(=O)CC(=O)O>>O=C(O)C(OP(=O)(O)O)=C:1.0, O=C(O)C(=O)CC(=O)O>>O=C(O)C(O[P])=C:1.0, O=C(O)C(O[P])=C:1.0, O=C(O)[CH2]:2.0, O=C(O)[CH2]>>O=C=O:2.0, O=C=O:3.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>[C]OP(=O)(O)O:1.0, [CH2]:2.0, [C]:4.0, [C]=C:1.0, [C]=O:2.0, [C]=O>>[C]O[P]:1.0, [C]C(=O)CC(=O)O:1.0, [C]C(=O)CC(=O)O>>O=C=O:1.0, [C]C(=O)CC(=O)O>>[C]C([O])=C:1.0, [C]C(=O)[CH2]:2.0, [C]C(=O)[CH2]>>[C]C(OP(=O)(O)O)=C:1.0, [C]C(=O)[CH2]>>[C]C([O])=C:1.0, [C]C(OP(=O)(O)O)=C:1.0, [C]C([O])=C:2.0, [C]CC(=O)C(=O)O:1.0, [C]CC(=O)C(=O)O>>O=C(O)C(OP(=O)(O)O)=C:1.0, [C]CC(=O)C(=O)O>>O=C(O)C(O[P])=C:1.0, [C]CC(=O)O:1.0, [C]CC(=O)O>>O=C=O:2.0, [C]C[C]:1.0, [C]C[C]>>[C]=C:1.0, [C]O:1.0, [C]O>>[C]=O:1.0, [C]OP(=O)(O)O:1.0, [C]O[P]:1.0, [OH]:2.0, [O]:4.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[C]C(OP(=O)(O)O)=C:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[CH2]:1.0, [C]:1.0, [O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[O=C(O)[CH2]:1.0, [C]C[C]:1.0, [C]O[P]:1.0, [O]P(=O)(O)O:2.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (6)
[O=P(O)(O)O[P]:1.0, [C]C(=O)CC(=O)O:1.0, [C]C(OP(=O)(O)O)=C:1.0, [C]CC(=O)O:1.0, [C]OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH2]:2.0, [C]:4.0, [OH]:1.0, [O]:3.0]


ID=Reaction Center at Level: 1 (9)
[O=C(O)[CH2]:1.0, O=C=O:1.0, [C]=C:1.0, [C]=O:2.0, [C]C(=O)[CH2]:1.0, [C]C([O])=C:1.0, [C]C[C]:1.0, [C]O:1.0, [C]O[P]:1.0]


ID=Reaction Center at Level: 2 (9)
[O=C(O)C(O[P])=C:1.0, O=C(O)[CH2]:1.0, O=C=O:2.0, [C]C(=O)CC(=O)O:1.0, [C]C(=O)[CH2]:1.0, [C]C(OP(=O)(O)O)=C:1.0, [C]C([O])=C:1.0, [C]CC(=O)C(=O)O:1.0, [C]CC(=O)O:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]P(=O)(O)O>>[O]P(=O)(O)O
2: [P]O[P]>>[P]O
3: [C]C(=O)[CH2]>>[C]C([O])=C
4: [C]=O>>[C]O[P]
5: [C]O>>[C]=O
6: O=C(O)[CH2]>>O=C=O
7: [C]C[C]>>[C]=C

MMP Level 2
1: O=P(O)(O)O[P]>>[C]OP(=O)(O)O
2: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
3: [C]CC(=O)C(=O)O>>O=C(O)C(O[P])=C
4: [C]C(=O)[CH2]>>[C]C(OP(=O)(O)O)=C
5: O=C(O)[CH2]>>O=C=O
6: [C]CC(=O)O>>O=C=O
7: [C]C(=O)CC(=O)O>>[C]C([O])=C

MMP Level 3
1: [O]P(=O)(O)OP(=O)(O)O>>[C]C(OP(=O)(O)O)=C
2: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
3: O=C(O)C(=O)CC(=O)O>>O=C(O)C(OP(=O)(O)O)=C
4: [C]CC(=O)C(=O)O>>O=C(O)C(OP(=O)(O)O)=C
5: [C]CC(=O)O>>O=C=O
6: [C]C(=O)CC(=O)O>>O=C=O
7: O=C(O)C(=O)CC(=O)O>>O=C(O)C(O[P])=C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1nc[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C3O>>O=c1nc[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)O)C(O)C3O]
2: R:M00001, P:M00004	[O=c1nc[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C3O>>O=C(O)C(OP(=O)(O)O)=C]
3: R:M00002, P:M00004	[O=C(O)C(=O)CC(=O)O>>O=C(O)C(OP(=O)(O)O)=C, O=C(O)C(=O)CC(=O)O>>O=C(O)C(OP(=O)(O)O)=C, O=C(O)C(=O)CC(=O)O>>O=C(O)C(OP(=O)(O)O)=C]
4: R:M00002, P:M00005	[O=C(O)C(=O)CC(=O)O>>O=C=O, O=C(O)C(=O)CC(=O)O>>O=C=O]


//
SELECTED AAM MAPPING
[O:32]=[C:33]([OH:34])[C:35](=[O:36])[CH2:37][C:38](=[O:39])[OH:40].[O:1]=[C:2]1[N:3]=[CH:4][NH:5][C:6]:2:[C:7]1:[N:8]:[CH:9]:[N:10]2[CH:11]3[O:12][CH:13]([CH2:14][O:15][P:16](=[O:17])([OH:18])[O:19][P:20](=[O:21])([OH:22])[O:23][P:24](=[O:25])([OH:26])[OH:27])[CH:28]([OH:29])[CH:30]3[OH:31]>>[O:39]=[C:38]=[O:40].[O:32]=[C:33]([OH:34])[C:35]([O:36][P:24](=[O:25])([OH:26])[OH:27])=[CH2:37].[O:1]=[C:2]1[N:3]=[CH:4][NH:5][C:6]:2:[C:7]1:[N:8]:[CH:9]:[N:10]2[CH:11]3[O:12][CH:13]([CH2:14][O:15][P:16](=[O:17])([OH:18])[O:19][P:20](=[O:21])([OH:22])[OH:23])[CH:28]([OH:29])[CH:30]3[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=4, 2=3, 3=2, 4=1, 5=7, 6=6, 7=5, 8=10, 9=9, 10=8, 11=11, 12=30, 13=28, 14=13, 15=12, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=25, 28=26, 29=27, 30=29, 31=31, 32=37, 33=35, 34=36, 35=33, 36=32, 37=34, 38=38, 39=39, 40=40}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=4, 2=3, 3=2, 4=1, 5=7, 6=6, 7=5, 8=10, 9=9, 10=8, 11=11, 12=26, 13=24, 14=13, 15=12, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=25, 27=27, 28=37, 29=31, 30=29, 31=28, 32=30, 33=32, 34=33, 35=34, 36=35, 37=36, 38=39, 39=38, 40=40}

