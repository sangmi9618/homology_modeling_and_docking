
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:2.0, C=O:2.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O=C(O)C(=O)C[CH2]:2.0, O=C(O)C(=O)C[CH2]>>O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(=O)C[CH2]>>O=C(O)C(N)C[CH2]:1.0, O=C(O)C(N)CC[CH2]>>O=C(O)C(=O)CC[CH2]:1.0, O=C(O)C(N)C[CH2]:2.0, O=C(O)C(N)C[CH2]>>O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(N)C[CH2]>>O=C(O)C(N)C[CH2]:1.0, [CH]:2.0, [CH]N:2.0, [CH]N>>[CH]N:1.0, [C]:2.0, [C]=O:2.0, [C]=O>>[C]=O:1.0, [C]C(=O)[CH2]:4.0, [C]C(=O)[CH2]>>[C]C(=O)[CH2]:1.0, [C]C(=O)[CH2]>>[C]C([CH2])N:1.0, [C]C([CH2])N:4.0, [C]C([CH2])N>>[C]C(=O)[CH2]:1.0, [C]C([CH2])N>>[C]C([CH2])N:1.0, [C]CCC(=O)C(=O)O>>[C]CCC(N)C(=O)O:1.0, [NH2]:2.0, [O]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[CH]:2.0, [C]:2.0, [NH2]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (4)
[[CH]N:2.0, [C]=O:2.0, [C]C(=O)[CH2]:2.0, [C]C([CH2])N:2.0]


ID=Reaction Center at Level: 2 (4)
[O=C(O)C(=O)C[CH2]:2.0, O=C(O)C(N)C[CH2]:2.0, [C]C(=O)[CH2]:2.0, [C]C([CH2])N:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:2.0, [C]:2.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(=O)[CH2]:2.0, [C]C([CH2])N:2.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C(=O)C[CH2]:2.0, O=C(O)C(N)C[CH2]:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C(=O)[CH2]>>[C]C([CH2])N
2: [CH]N>>[CH]N
3: [C]C([CH2])N>>[C]C(=O)[CH2]
4: [C]=O>>[C]=O

MMP Level 2
1: O=C(O)C(=O)C[CH2]>>O=C(O)C(N)C[CH2]
2: [C]C([CH2])N>>[C]C([CH2])N
3: O=C(O)C(N)C[CH2]>>O=C(O)C(=O)C[CH2]
4: [C]C(=O)[CH2]>>[C]C(=O)[CH2]

MMP Level 3
1: [C]CCC(=O)C(=O)O>>[C]CCC(N)C(=O)O
2: O=C(O)C(N)C[CH2]>>O=C(O)C(N)C[CH2]
3: O=C(O)C(N)CC[CH2]>>O=C(O)C(=O)CC[CH2]
4: O=C(O)C(=O)C[CH2]>>O=C(O)C(=O)C[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)CCCC(N)C(=O)O>>O=C(O)C(=O)CCCC(=O)O]
2: R:M00001, P:M00004	[O=C(O)CCCC(N)C(=O)O>>O=C(O)CCC(N)C(=O)O]
3: R:M00002, P:M00003	[O=C(O)C(=O)CCC(=O)O>>O=C(O)C(=O)CCCC(=O)O]
4: R:M00002, P:M00004	[O=C(O)C(=O)CCC(=O)O>>O=C(O)CCC(N)C(=O)O]


//
SELECTED AAM MAPPING
[O:12]=[C:13]([OH:14])[C:15](=[O:16])[CH2:17][CH2:18][C:19](=[O:20])[OH:21].[O:1]=[C:2]([OH:3])[CH2:4][CH2:5][CH2:6][CH:7]([NH2:8])[C:9](=[O:10])[OH:11]>>[O:10]=[C:9]([OH:11])[C:7](=[O:16])[CH2:6][CH2:5][CH2:4][C:2](=[O:1])[OH:3].[O:20]=[C:19]([OH:21])[CH2:18][CH2:17][CH:15]([NH2:8])[C:13](=[O:12])[OH:14]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=5, 2=6, 3=7, 4=9, 5=10, 6=11, 7=8, 8=4, 9=2, 10=1, 11=3, 12=17, 13=18, 14=19, 15=20, 16=21, 17=15, 18=16, 19=13, 20=12, 21=14}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=7, 2=6, 3=4, 4=5, 5=2, 6=1, 7=3, 8=8, 9=9, 10=10, 11=11, 12=16, 13=15, 14=13, 15=12, 16=14, 17=17, 18=19, 19=20, 20=21, 21=18}

