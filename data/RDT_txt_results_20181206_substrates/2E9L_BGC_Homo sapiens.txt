
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O>>[CH]O:1.0, O>>[CH]OC(O)C([CH])O:1.0, O>>[O]C([CH])O:1.0, [CH]:2.0, [CH]C(=[CH])O:1.0, [CH]C(=[CH])OC1OC([CH2])[CH]C(O)C1O>>OC1OC([CH2])[CH]C(O)C1O:1.0, [CH]O:1.0, [CH]OC(O)C([CH])O:1.0, [C]C=C(OC(O[CH])C([CH])O)C=[C]>>[C]C=C(O)C=[C]:1.0, [C]O:1.0, [C]OC(O[CH])C([CH])O:1.0, [C]OC(O[CH])C([CH])O>>[CH]OC(O)C([CH])O:1.0, [C]O[CH]:1.0, [C]O[CH]>>[C]O:1.0, [OH]:2.0, [O]:1.0, [O]C([CH])O:2.0, [O]C([CH])OC([CH])=[CH]:1.0, [O]C([CH])OC([CH])=[CH]>>[CH]C(=[CH])O:1.0, [O]C([O])[CH]:1.0, [O]C([O])[CH]>>[O]C([CH])O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [CH]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, [CH]O:1.0, [C]O[CH]:1.0, [O]C([CH])O:1.0, [O]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, [CH]OC(O)C([CH])O:1.0, [C]OC(O[CH])C([CH])O:1.0, [O]C([CH])O:1.0, [O]C([CH])OC([CH])=[CH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]C([CH])O:1.0, [O]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[CH]OC(O)C([CH])O:1.0, [C]OC(O[CH])C([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: O>>[CH]O
2: [C]O[CH]>>[C]O
3: [O]C([O])[CH]>>[O]C([CH])O

MMP Level 2
1: O>>[O]C([CH])O
2: [O]C([CH])OC([CH])=[CH]>>[CH]C(=[CH])O
3: [C]OC(O[CH])C([CH])O>>[CH]OC(O)C([CH])O

MMP Level 3
1: O>>[CH]OC(O)C([CH])O
2: [C]C=C(OC(O[CH])C([CH])O)C=[C]>>[C]C=C(O)C=[C]
3: [CH]C(=[CH])OC1OC([CH2])[CH]C(O)C1O>>OC1OC([CH2])[CH]C(O)C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1c(O)c(oc2cc(OC3OC(CO)C(O)C(O)C3O)cc(O)c12)-c4ccc(O)c(O)c4>>O=c1c(O)c(oc2cc(O)cc(O)c12)-c3ccc(O)c(O)c3]
2: R:M00001, P:M00004	[O=c1c(O)c(oc2cc(OC3OC(CO)C(O)C(O)C3O)cc(O)c12)-c4ccc(O)c(O)c4>>OCC1OC(O)C(O)C(O)C1O]
3: R:M00002, P:M00004	[O>>OCC1OC(O)C(O)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[C:3]([OH:4])=[C:5]([O:6][C:7]:2:[CH:8]:[C:9]([O:10][CH:11]3[O:12][CH:13]([CH2:14][OH:15])[CH:16]([OH:17])[CH:18]([OH:19])[CH:20]3[OH:21]):[CH:22]:[C:23]([OH:24]):[C:25]12)[C:26]:4:[CH:27]:[CH:28]:[C:29]([OH:30]):[C:31]([OH:32]):[CH:33]4.[OH2:34]>>[O:1]=[C:2]1[C:3]([OH:4])=[C:5]([O:6][C:7]:2:[CH:8]:[C:9]([OH:10]):[CH:22]:[C:23]([OH:24]):[C:25]12)[C:26]:3:[CH:27]:[CH:28]:[C:29]([OH:30]):[C:31]([OH:32]):[CH:33]3.[OH:15][CH2:14][CH:13]1[O:12][CH:11]([OH:34])[CH:20]([OH:21])[CH:18]([OH:19])[CH:16]1[OH:17]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=27, 2=28, 3=29, 4=31, 5=33, 6=26, 7=5, 8=3, 9=2, 10=1, 11=25, 12=23, 13=22, 14=9, 15=8, 16=7, 17=6, 18=10, 19=11, 20=20, 21=18, 22=16, 23=13, 24=12, 25=14, 26=15, 27=17, 28=19, 29=21, 30=24, 31=4, 32=32, 33=30, 34=34}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=16, 2=17, 3=18, 4=20, 5=22, 6=15, 7=5, 8=3, 9=2, 10=1, 11=14, 12=12, 13=11, 14=9, 15=8, 16=7, 17=6, 18=10, 19=13, 20=4, 21=21, 22=19, 23=24, 24=25, 25=33, 26=31, 27=29, 28=27, 29=26, 30=28, 31=30, 32=32, 33=34, 34=23}

