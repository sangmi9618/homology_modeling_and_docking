
//
FINGERPRINTS BC

FORMED_CLEAVED
[N-P:1.0, O-P:1.0]

//
FINGERPRINTS RC
[N([CH2])=C(N)N>>O=P(O)(O)NC(=N[CH2])N:1.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>[C]NP(=O)(O)O:1.0, O=P([NH])(O)O:1.0, [C]N:1.0, [C]N>>[C]N[P]:1.0, [C]NP(=O)(O)O:1.0, [C]N[P]:1.0, [NH2]:1.0, [NH]:1.0, [N]=C(N)N:1.0, [N]=C(N)N>>[N]=C(N)NP(=O)(O)O:1.0, [N]=C(N)NP(=O)(O)O:1.0, [OH]:1.0, [O]:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>O=P([NH])(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[N]=C(N)NP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[NH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (4)
[O=P([NH])(O)O:1.0, [C]N[P]:1.0, [O]P(=O)(O)O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)O[P]:1.0, [C]NP(=O)(O)O:1.0, [N]=C(N)NP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[P]>>[P]O
2: [C]N>>[C]N[P]
3: [O]P(=O)(O)O>>O=P([NH])(O)O

MMP Level 2
1: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
2: [N]=C(N)N>>[N]=C(N)NP(=O)(O)O
3: O=P(O)(O)O[P]>>[C]NP(=O)(O)O

MMP Level 3
1: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
2: N([CH2])=C(N)N>>O=P(O)(O)NC(=N[CH2])N
3: [O]P(=O)(O)OP(=O)(O)O>>[N]=C(N)NP(=O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C(O)CN=C(N)N>>O=C(O)CN=C(N)NP(=O)(O)O]
2: R:M00002, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
3: R:M00002, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(O)CN=C(N)NP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:32]=[C:33]([OH:34])[CH2:35][N:36]=[C:37]([NH2:38])[NH2:39].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:32]=[C:33]([OH:34])[CH2:35][N:36]=[C:37]([NH2:38])[NH:39][P:2](=[O:1])([OH:3])[OH:4].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=35, 2=33, 3=32, 4=34, 5=36, 6=37, 7=38, 8=39, 9=24, 10=25, 11=26, 12=21, 13=22, 14=23, 15=27, 16=20, 17=19, 18=18, 19=17, 20=28, 21=30, 22=15, 23=16, 24=14, 25=13, 26=10, 27=11, 28=12, 29=9, 30=6, 31=7, 32=8, 33=5, 34=2, 35=1, 36=3, 37=4, 38=31, 39=29}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=17, 5=18, 6=19, 7=23, 8=16, 9=15, 10=14, 11=13, 12=24, 13=26, 14=11, 15=12, 16=10, 17=9, 18=6, 19=7, 20=8, 21=5, 22=2, 23=1, 24=3, 25=4, 26=27, 27=25, 28=31, 29=29, 30=28, 31=30, 32=32, 33=33, 34=34, 35=35, 36=36, 37=37, 38=38, 39=39}

