
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)OC[CH]:1.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O[CH2]:1.0, O[CH2]:1.0, O[CH2]>>[P]O[CH2]:1.0, [CH]C(O)CO>>O=P(O)(O)OCC([CH])O:1.0, [CH]CO:1.0, [CH]CO>>O=P(O)(O)OC[CH]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC[CH]:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[O]P(=O)(O)O:2.0, [P]O[CH2]:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)OC[CH]:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[P]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[P]>>[P]O
2: O[CH2]>>[P]O[CH2]
3: [O]P(=O)(O)O>>[O]P(=O)(O)O

MMP Level 2
1: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
2: [CH]CO>>O=P(O)(O)OC[CH]
3: O=P(O)(O)O[P]>>O=P(O)(O)O[CH2]

MMP Level 3
1: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
2: [CH]C(O)CO>>O=P(O)(O)OCC([CH])O
3: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)O)C)C)c(=O)[nH]1]
3: R:M00002, P:M00004	[O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)CO)C)C)c(=O)[nH]1>>O=c1nc2-c(nc3cc(c(cc3n2CC(O)C(O)C(O)COP(=O)(O)O)C)C)c(=O)[nH]1]


//
SELECTED AAM MAPPING
[O:32]=[C:33]1[N:34]=[C:35]2[C:36](=[N:37][C:38]:3:[CH:39]:[C:40](:[C:41](:[CH:42]:[C:43]3[N:44]2[CH2:45][CH:46]([OH:47])[CH:48]([OH:49])[CH:50]([OH:51])[CH2:52][OH:53])[CH3:54])[CH3:55])[C:56](=[O:57])[NH:58]1.[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:32]=[C:33]1[N:34]=[C:35]2[C:36](=[N:37][C:38]:3:[CH:39]:[C:40](:[C:41](:[CH:42]:[C:43]3[N:44]2[CH2:45][CH:46]([OH:47])[CH:48]([OH:49])[CH:50]([OH:51])[CH2:52][O:53][P:2](=[O:1])([OH:3])[OH:4])[CH3:54])[CH3:55])[C:56](=[O:57])[NH:58]1.[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=55, 33=40, 34=39, 35=38, 36=43, 37=42, 38=41, 39=54, 40=44, 41=35, 42=34, 43=33, 44=32, 45=58, 46=56, 47=57, 48=36, 49=37, 50=45, 51=46, 52=48, 53=50, 54=52, 55=53, 56=51, 57=49, 58=47}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=51, 2=52, 3=53, 4=48, 5=49, 6=50, 7=54, 8=47, 9=46, 10=45, 11=44, 12=55, 13=57, 14=42, 15=43, 16=41, 17=40, 18=37, 19=38, 20=39, 21=36, 22=33, 23=32, 24=34, 25=35, 26=58, 27=56, 28=28, 29=9, 30=8, 31=7, 32=12, 33=11, 34=10, 35=27, 36=13, 37=4, 38=3, 39=2, 40=1, 41=31, 42=29, 43=30, 44=5, 45=6, 46=14, 47=15, 48=17, 49=19, 50=21, 51=22, 52=23, 53=24, 54=25, 55=26, 56=20, 57=18, 58=16}

