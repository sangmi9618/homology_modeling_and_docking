
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-S:1.0]

//
FINGERPRINTS RC
[S(C)C[CH2]:1.0, S([CH2])C:2.0, SC[CH2]:1.0, SC[CH2]>>S(C)C[CH2]:1.0, S[CH2]:1.0, S[CH2]>>S([CH2])C:1.0, [CH3]:2.0, [CH]CCS>>[CH]CCSC:1.0, [C]C(=[C])N(C)C([CH2])[CH2]:1.0, [C]C(=[C])N(C)C([CH2])[CH2]>>S(C)C[CH2]:1.0, [C]C(=[C])N(C)C([CH2])[CH2]>>[C]C(=[C])NC([CH2])[CH2]:1.0, [C]C(=[C])NC([CH2])[CH2]:1.0, [C]N([CH])C:2.0, [C]N([CH])C>>S([CH2])C:1.0, [C]N([CH])C>>[C]N[CH]:1.0, [C]N[CH]:1.0, [NH]:1.0, [N]:1.0, [N]C:1.0, [N]C(=O)C1=C([NH])NCC(N1C)C[NH]>>[N]C(=O)C=1NC(CNC1[NH])C[NH]:1.0, [N]C>>[S]C:1.0, [SH]:1.0, [S]:1.0, [S]C:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH3]:2.0, [N]:1.0, [S]:1.0]


ID=Reaction Center at Level: 1 (4)
[S([CH2])C:1.0, [C]N([CH])C:1.0, [N]C:1.0, [S]C:1.0]


ID=Reaction Center at Level: 2 (4)
[S(C)C[CH2]:1.0, S([CH2])C:1.0, [C]C(=[C])N(C)C([CH2])[CH2]:1.0, [C]N([CH])C:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]N([CH])C>>[C]N[CH]
2: S[CH2]>>S([CH2])C
3: [N]C>>[S]C

MMP Level 2
1: [C]C(=[C])N(C)C([CH2])[CH2]>>[C]C(=[C])NC([CH2])[CH2]
2: SC[CH2]>>S(C)C[CH2]
3: [C]N([CH])C>>S([CH2])C

MMP Level 3
1: [N]C(=O)C1=C([NH])NCC(N1C)C[NH]>>[N]C(=O)C=1NC(CNC1[NH])C[NH]
2: [CH]CCS>>[CH]CCSC
3: [C]C(=[C])N(C)C([CH2])[CH2]>>S(C)C[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)CCS>>O=C(O)C(N)CCSC]
2: R:M00002, P:M00003	[O=c1nc(N)[nH]c2NCC(N(c12)C)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)NC(C(=O)O)CCC(=O)NC(C(=O)O)CCC(=O)O>>O=C(O)C(N)CCSC]
3: R:M00002, P:M00004	[O=c1nc(N)[nH]c2NCC(N(c12)C)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)NC(C(=O)O)CCC(=O)NC(C(=O)O)CCC(=O)O>>O=c1nc(N)[nH]c2NCC(Nc12)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)NC(C(=O)O)CCC(=O)NC(C(=O)O)CCC(=O)O]


//
SELECTED AAM MAPPING
[O:50]=[C:49]([OH:51])[CH2:48][CH2:47][CH:43]([NH:42][C:40](=[O:41])[CH2:39][CH2:38][CH:34]([NH:33][C:31](=[O:32])[CH2:30][CH2:29][CH:25]([NH:24][C:22](=[O:23])[C:19]:1:[CH:18]:[CH:17]:[C:16](:[CH:21]:[CH:20]1)[NH:15][CH2:14][CH:10]2[N:11]([C:12]=3[C:2](=[O:1])[N:3]=[C:4]([NH2:5])[NH:6][C:7]3[NH:8][CH2:9]2)[CH3:13])[C:26](=[O:27])[OH:28])[C:35](=[O:36])[OH:37])[C:44](=[O:45])[OH:46].[O:52]=[C:53]([OH:54])[CH:55]([NH2:56])[CH2:57][CH2:58][SH:59]>>[O:50]=[C:49]([OH:51])[CH2:48][CH2:47][CH:43]([NH:42][C:40](=[O:41])[CH2:39][CH2:38][CH:34]([NH:33][C:31](=[O:32])[CH2:30][CH2:29][CH:25]([NH:24][C:22](=[O:23])[C:19]:1:[CH:18]:[CH:17]:[C:16](:[CH:21]:[CH:20]1)[NH:15][CH2:14][CH:10]2[NH:11][C:12]=3[C:2](=[O:1])[N:3]=[C:4]([NH2:5])[NH:6][C:7]3[NH:8][CH2:9]2)[C:26](=[O:27])[OH:28])[C:35](=[O:36])[OH:37])[C:44](=[O:45])[OH:46].[O:52]=[C:53]([OH:54])[CH:55]([NH2:56])[CH2:57][CH2:58][S:59][CH3:13]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=57, 2=58, 3=59, 4=55, 5=53, 6=52, 7=54, 8=56, 9=13, 10=11, 11=10, 12=9, 13=8, 14=7, 15=12, 16=2, 17=1, 18=3, 19=4, 20=6, 21=5, 22=14, 23=15, 24=16, 25=17, 26=18, 27=19, 28=20, 29=21, 30=22, 31=23, 32=24, 33=25, 34=29, 35=30, 36=31, 37=32, 38=33, 39=34, 40=38, 41=39, 42=40, 43=41, 44=42, 45=43, 46=47, 47=48, 48=49, 49=50, 50=51, 51=44, 52=45, 53=46, 54=35, 55=36, 56=37, 57=26, 58=27, 59=28}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=59, 2=58, 3=57, 4=56, 5=54, 6=52, 7=51, 8=53, 9=55, 10=9, 11=10, 12=11, 13=12, 14=7, 15=8, 16=6, 17=4, 18=3, 19=2, 20=1, 21=5, 22=13, 23=14, 24=15, 25=16, 26=17, 27=18, 28=19, 29=20, 30=21, 31=22, 32=23, 33=24, 34=28, 35=29, 36=30, 37=31, 38=32, 39=33, 40=37, 41=38, 42=39, 43=40, 44=41, 45=42, 46=46, 47=47, 48=48, 49=49, 50=50, 51=43, 52=44, 53=45, 54=34, 55=35, 56=36, 57=25, 58=26, 59=27}

