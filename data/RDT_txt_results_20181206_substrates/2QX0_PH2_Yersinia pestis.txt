
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]:1.0, O=P(O)(O[P])O[CH2]:1.0, O=P(O)(O[P])O[P]:1.0, O=P(O)(O[P])O[P]>>O=P(O)(O[P])O[CH2]:1.0, O[CH2]:1.0, O[CH2]>>[P]O[CH2]:1.0, [C]CO:1.0, [C]CO>>[C]COP([O])(=O)O:1.0, [C]COP([O])(=O)O:1.0, [N]=C([CH2])CO>>[N]=C([CH2])COP(=O)(O)O[P]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)OP(=O)(O)O>>[C]COP(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:2.0, [O]P([O])(=O)O>>[O]P([O])(=O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[O]P([O])(=O)O:2.0, [P]O[CH2]:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O[P])O[CH2]:1.0, O=P(O)(O[P])O[P]:1.0, [C]COP([O])(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O[CH2]>>[P]O[CH2]
2: [P]O[P]>>[P]O
3: [O]P([O])(=O)O>>[O]P([O])(=O)O

MMP Level 2
1: [C]CO>>[C]COP([O])(=O)O
2: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
3: O=P(O)(O[P])O[P]>>O=P(O)(O[P])O[CH2]

MMP Level 3
1: [N]=C([CH2])CO>>[N]=C([CH2])COP(=O)(O)O[P]
2: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH2]
3: [O]P(=O)(O)OP(=O)(O)OP(=O)(O)O>>[C]COP(=O)(O)OP(=O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=c1nc(N)[nH]c2NCC(=Nc12)COP(=O)(O)OP(=O)(O)O]
3: R:M00002, P:M00004	[O=c1nc(N)[nH]c2NCC(=Nc12)CO>>O=c1nc(N)[nH]c2NCC(=Nc12)COP(=O)(O)OP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:32]=[C:33]1[N:34]=[C:35]([NH2:36])[NH:37][C:38]=2[NH:39][CH2:40][C:41](=[N:42][C:43]12)[CH2:44][OH:45].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:32]=[C:33]1[N:34]=[C:35]([NH2:36])[NH:37][C:38]=2[NH:39][CH2:40][C:41](=[N:42][C:43]12)[CH2:44][O:45][P:6](=[O:7])([OH:8])[O:5][P:2](=[O:1])([OH:3])[OH:4].[O:11]=[P:10]([OH:9])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=40, 33=41, 34=42, 35=43, 36=38, 37=39, 38=37, 39=35, 40=34, 41=33, 42=32, 43=36, 44=44, 45=45}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=16, 2=17, 3=18, 4=13, 5=14, 6=15, 7=19, 8=12, 9=11, 10=10, 11=9, 12=20, 13=22, 14=7, 15=8, 16=6, 17=5, 18=2, 19=1, 20=3, 21=4, 22=23, 23=21, 24=32, 25=33, 26=34, 27=35, 28=30, 29=31, 30=29, 31=27, 32=26, 33=25, 34=24, 35=28, 36=36, 37=37, 38=38, 39=39, 40=40, 41=41, 42=42, 43=43, 44=44, 45=45}

