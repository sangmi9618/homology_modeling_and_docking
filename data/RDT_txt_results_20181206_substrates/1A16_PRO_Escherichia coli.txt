
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(N([CH])[CH2])C([CH])N:1.0, O=C(N([CH])[CH2])C([CH])N>>O=C(O)C([CH])N:1.0, O=C(O)C([CH])N:1.0, O=C(O)C1N(C(=O)C([CH])N)CCC1>>O=C(O)C1NCCC1:1.0, O=C([CH])O:2.0, O>>O=C(O)C([CH])N:1.0, O>>O=C([CH])O:1.0, O>>[C]O:1.0, [CH]N[CH2]:1.0, [C]:2.0, [C]C1N(C(=O)C(N)C(C)C)CCC1>>O=C(O)C(N)C(C)C:1.0, [C]C1N(C(=O)[CH])CCC1:1.0, [C]C1N(C(=O)[CH])CCC1>>[C]C1NCCC1:1.0, [C]C1NCCC1:1.0, [C]N([CH])[CH2]:1.0, [C]N([CH])[CH2]>>[CH]N[CH2]:1.0, [C]O:1.0, [NH]:1.0, [N]:1.0, [N]C(=O)[CH]:1.0, [N]C(=O)[CH]>>O=C([CH])O:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [C]:2.0, [N]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=C([CH])O:1.0, [C]N([CH])[CH2]:1.0, [C]O:1.0, [N]C(=O)[CH]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=C(N([CH])[CH2])C([CH])N:1.0, O=C(O)C([CH])N:1.0, O=C([CH])O:1.0, [C]C1N(C(=O)[CH])CCC1:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]N([CH])[CH2]>>[CH]N[CH2]
2: [N]C(=O)[CH]>>O=C([CH])O
3: O>>[C]O

MMP Level 2
1: [C]C1N(C(=O)[CH])CCC1>>[C]C1NCCC1
2: O=C(N([CH])[CH2])C([CH])N>>O=C(O)C([CH])N
3: O>>O=C([CH])O

MMP Level 3
1: O=C(O)C1N(C(=O)C([CH])N)CCC1>>O=C(O)C1NCCC1
2: [C]C1N(C(=O)C(N)C(C)C)CCC1>>O=C(O)C(N)C(C)C
3: O>>O=C(O)C([CH])N


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C1N(C(=O)C(N)C(C)C)CCC1>>O=C(O)C(N)C(C)C]
2: R:M00001, P:M00004	[O=C(O)C1N(C(=O)C(N)C(C)C)CCC1>>O=C(O)C1NCCC1]
3: R:M00002, P:M00003	[O>>O=C(O)C(N)C(C)C]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]1[N:5]([C:6](=[O:7])[CH:8]([NH2:9])[CH:10]([CH3:11])[CH3:12])[CH2:13][CH2:14][CH2:15]1.[OH2:16]>>[O:7]=[C:6]([OH:16])[CH:8]([NH2:9])[CH:10]([CH3:12])[CH3:11].[O:1]=[C:2]([OH:3])[CH:4]1[NH:5][CH2:13][CH2:14][CH2:15]1


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=11, 2=10, 3=12, 4=8, 5=6, 6=7, 7=5, 8=13, 9=14, 10=15, 11=4, 12=2, 13=1, 14=3, 15=9, 16=16}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=15, 2=14, 3=16, 4=12, 5=10, 6=9, 7=11, 8=13, 9=7, 10=8, 11=4, 12=5, 13=6, 14=2, 15=1, 16=3}

