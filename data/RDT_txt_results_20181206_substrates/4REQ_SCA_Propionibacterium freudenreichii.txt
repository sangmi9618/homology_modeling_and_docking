
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:2.0, C-S:2.0, C=O:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(O)C(C(=O)SC[CH2])C>>O=C(O)CCC(=O)S[CH2]:1.0, O=C(O)C(C(=O)S[CH2])C>>O=C([S])CCC(=O)O:1.0, O=C(SC[CH2])[CH2]:1.0, O=C([CH])SC[CH2]:1.0, O=C([CH])SC[CH2]>>O=C(SC[CH2])[CH2]:1.0, O=C([S])C(C(=O)O)C:1.0, O=C([S])C(C(=O)O)C>>[C]CCC(=O)O:1.0, O=C([S])[CH2]:1.0, O=C([S])[CH]:2.0, O=C([S])[CH]>>O=C([S])[CH2]:1.0, O=C([S])[CH]>>[C]C[CH2]:1.0, [CH2]:2.0, [CH3]:1.0, [CH]:1.0, [CH]C:1.0, [C]:1.0, [C]=O:2.0, [C]=O>>[C]=O:1.0, [C]C(C(=O)SCC[NH])C>>O=C(SCC[NH])C[CH2]:1.0, [C]C(C(=O)S[CH2])C:1.0, [C]C(C(=O)S[CH2])C>>O=C(S[CH2])C[CH2]:1.0, [C]C(C(=O)S[CH2])C>>[C]CCC(=O)[S]:1.0, [C]C([C])C:2.0, [C]C([C])C>>[C]C[CH2]:1.0, [C]CCC(=O)O:1.0, [C]CCC(=O)[S]:1.0, [C]C[CH2]:2.0, [C]S[CH2]:2.0, [C]S[CH2]>>[C]S[CH2]:1.0, [O]:2.0, [S]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (6)
[[CH2]:1.0, [CH3]:1.0, [CH]:1.0, [C]:5.0, [O]:2.0, [S]:2.0]


ID=Reaction Center at Level: 1 (7)
[O=C([S])[CH2]:3.0, O=C([S])[CH]:2.0, [CH]C:1.0, [C]=O:2.0, [C]C([C])C:1.0, [C]C[CH2]:1.0, [C]S[CH2]:2.0]


ID=Reaction Center at Level: 2 (9)
[O=C(SC[CH2])[CH2]:1.0, O=C(S[CH2])C[CH2]:3.0, O=C([CH])SC[CH2]:1.0, O=C([S])C(C(=O)O)C:1.0, O=C([S])[CH2]:1.0, O=C([S])[CH]:1.0, [C]C(C(=O)S[CH2])C:2.0, [C]C([C])C:1.0, [C]CCC(=O)[S]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH2]:1.0, [CH]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]C([C])C:1.0, [C]C[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C([S])C(C(=O)O)C:1.0, [C]CCC(=O)O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]=O>>[C]=O
2: [C]C([C])C>>[C]C[CH2]
3: O=C([S])[CH]>>[C]C[CH2]
4: [C]S[CH2]>>[C]S[CH2]

MMP Level 2
1: O=C([S])[CH]>>O=C([S])[CH2]
2: O=C([S])C(C(=O)O)C>>[C]CCC(=O)O
3: [C]C(C(=O)S[CH2])C>>[C]CCC(=O)[S]
4: O=C([CH])SC[CH2]>>O=C(SC[CH2])[CH2]

MMP Level 3
1: [C]C(C(=O)S[CH2])C>>O=C(S[CH2])C[CH2]
2: O=C(O)C(C(=O)S[CH2])C>>O=C([S])CCC(=O)O
3: O=C(O)C(C(=O)SC[CH2])C>>O=C(O)CCC(=O)S[CH2]
4: [C]C(C(=O)SCC[NH])C>>O=C(SCC[NH])C[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)C(C(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C>>O=C(O)CCC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O, O=C(O)C(C(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C>>O=C(O)CCC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O, O=C(O)C(C(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C>>O=C(O)CCC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O, O=C(O)C(C(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C>>O=C(O)CCC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([C:5](=[O:6])[S:7][CH2:8][CH2:9][NH:10][C:11](=[O:12])[CH2:13][CH2:14][NH:15][C:16](=[O:17])[CH:18]([OH:19])[C:20]([CH3:21])([CH3:22])[CH2:23][O:24][P:25](=[O:26])([OH:27])[O:28][P:29](=[O:30])([OH:31])[O:32][CH2:33][CH:34]1[O:35][CH:36]([N:37]:2:[CH:38]:[N:39]:[C:40]:3:[C:41](:[N:42]:[CH:43]:[N:44]:[C:45]32)[NH2:46])[CH:47]([OH:48])[CH:49]1[O:50][P:51](=[O:52])([OH:53])[OH:54])[CH3:55]>>[O:1]=[C:2]([OH:3])[CH2:4][CH2:5][C:56](=[O:6])[S:7][CH2:8][CH2:9][NH:10][C:11](=[O:12])[CH2:13][CH2:14][NH:15][C:16](=[O:17])[CH:18]([OH:19])[C:20]([CH3:21])([CH3:22])[CH2:23][O:24][P:25](=[O:26])([OH:27])[O:28][P:29](=[O:30])([OH:31])[O:32][CH2:33][CH:34]1[O:35][CH:36]([N:37]:2:[CH:38]:[N:39]:[C:40]:3:[C:41](:[N:42]:[CH:43]:[N:44]:[C:45]32)[NH2:46])[CH:47]([OH:48])[CH:49]1[O:50][P:51](=[O:52])([OH:53])[OH:54]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=55, 2=4, 3=2, 4=1, 5=3, 6=5, 7=6, 8=7, 9=8, 10=9, 11=10, 12=11, 13=12, 14=13, 15=14, 16=15, 17=16, 18=17, 19=18, 20=20, 21=21, 22=22, 23=23, 24=24, 25=25, 26=26, 27=27, 28=28, 29=29, 30=30, 31=31, 32=32, 33=33, 34=34, 35=49, 36=47, 37=36, 38=35, 39=37, 40=38, 41=39, 42=40, 43=45, 44=44, 45=43, 46=42, 47=41, 48=46, 49=48, 50=50, 51=51, 52=52, 53=53, 54=54, 55=19}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=22, 2=21, 3=23, 4=24, 5=25, 6=26, 7=27, 8=28, 9=29, 10=30, 11=31, 12=32, 13=33, 14=34, 15=35, 16=50, 17=48, 18=37, 19=36, 20=38, 21=39, 22=40, 23=41, 24=46, 25=45, 26=44, 27=43, 28=42, 29=47, 30=49, 31=51, 32=52, 33=53, 34=54, 35=55, 36=19, 37=17, 38=18, 39=16, 40=15, 41=14, 42=12, 43=13, 44=11, 45=10, 46=9, 47=8, 48=6, 49=7, 50=5, 51=4, 52=2, 53=1, 54=3, 55=20}

