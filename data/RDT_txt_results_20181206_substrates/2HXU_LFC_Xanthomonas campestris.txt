
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):3.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)C(=O)C[CH]:1.0, O=C(O)C(O)C(O)C(O)C(O)C>>O=C(O)C(=O)CC(O)C(O)C:1.0, O=C(O)C(O)C(O)C([CH])O>>O=C(O)C(=O)CC([CH])O:1.0, O=C(O)C(O)C([CH])O:1.0, O=C(O)C(O)C([CH])O>>O=C(O)C(=O)C[CH]:2.0, [CH2]:1.0, [CH]:4.0, [CH]C(O)C(O)C(O)C:1.0, [CH]C(O)C(O)C(O)C>>[C]CC(O)C(O)C:1.0, [CH]C(O)[CH2]:1.0, [CH]C([CH])O:3.0, [CH]C([CH])O>>O:1.0, [CH]C([CH])O>>[CH]C(O)[CH2]:1.0, [CH]C([CH])O>>[C]C[CH]:1.0, [CH]O:2.0, [CH]O>>O:1.0, [CH]O>>[C]=O:1.0, [C]:1.0, [C]=O:1.0, [C]C(=O)CC([CH])O:1.0, [C]C(=O)[CH2]:2.0, [C]C(O)C(O)C(O)C(O)C>>[C]C(=O)CC(O)C(O)C:1.0, [C]C(O)C(O)C([CH])O:1.0, [C]C(O)C(O)C([CH])O>>O:1.0, [C]C(O)C(O)C([CH])O>>[C]C(=O)CC([CH])O:1.0, [C]C([CH])O:2.0, [C]C([CH])O>>[C]C(=O)[CH2]:2.0, [C]CC(O)C(O)C:1.0, [C]C[CH]:1.0, [OH]:2.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[O:1.0, [CH]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (3)
[O:1.0, [CH]C([CH])O:1.0, [CH]O:1.0]


ID=Reaction Center at Level: 2 (3)
[O:1.0, [CH]C([CH])O:1.0, [C]C(O)C(O)C([CH])O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH]:1.0, [C]:1.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[CH]O:1.0, [C]=O:1.0, [C]C(=O)[CH2]:1.0, [C]C([CH])O:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(O)C(=O)C[CH]:1.0, O=C(O)C(O)C([CH])O:1.0, [C]C(=O)[CH2]:1.0, [C]C([CH])O:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (3)
[[CH2]:1.0, [CH]:4.0, [C]:1.0]


ID=Reaction Center at Level: 1 (5)
[[CH]C(O)[CH2]:1.0, [CH]C([CH])O:2.0, [C]C(=O)[CH2]:1.0, [C]C([CH])O:1.0, [C]C[CH]:1.0]


ID=Reaction Center at Level: 2 (6)
[O=C(O)C(=O)C[CH]:1.0, O=C(O)C(O)C([CH])O:1.0, [CH]C(O)C(O)C(O)C:1.0, [C]C(=O)CC([CH])O:1.0, [C]C(O)C(O)C([CH])O:1.0, [C]CC(O)C(O)C:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C([CH])O>>[C]C(=O)[CH2]
2: [CH]O>>O
3: [CH]O>>[C]=O
4: [CH]C([CH])O>>[C]C[CH]
5: [CH]C([CH])O>>[CH]C(O)[CH2]

MMP Level 2
1: O=C(O)C(O)C([CH])O>>O=C(O)C(=O)C[CH]
2: [CH]C([CH])O>>O
3: [C]C([CH])O>>[C]C(=O)[CH2]
4: [C]C(O)C(O)C([CH])O>>[C]C(=O)CC([CH])O
5: [CH]C(O)C(O)C(O)C>>[C]CC(O)C(O)C

MMP Level 3
1: O=C(O)C(O)C(O)C([CH])O>>O=C(O)C(=O)CC([CH])O
2: [C]C(O)C(O)C([CH])O>>O
3: O=C(O)C(O)C([CH])O>>O=C(O)C(=O)C[CH]
4: O=C(O)C(O)C(O)C(O)C(O)C>>O=C(O)C(=O)CC(O)C(O)C
5: [C]C(O)C(O)C(O)C(O)C>>[C]C(=O)CC(O)C(O)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)C(O)C(O)C(O)C(O)C>>O=C(O)C(=O)CC(O)C(O)C, O=C(O)C(O)C(O)C(O)C(O)C>>O=C(O)C(=O)CC(O)C(O)C, O=C(O)C(O)C(O)C(O)C(O)C>>O=C(O)C(=O)CC(O)C(O)C, O=C(O)C(O)C(O)C(O)C(O)C>>O=C(O)C(=O)CC(O)C(O)C]
2: R:M00001, P:M00003	[O=C(O)C(O)C(O)C(O)C(O)C>>O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([OH:5])[CH:6]([OH:7])[CH:8]([OH:9])[CH:10]([OH:11])[CH3:12]>>[O:1]=[C:2]([OH:3])[C:4](=[O:5])[CH2:6][CH:8]([OH:9])[CH:10]([OH:11])[CH3:12].[OH2:7]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=12, 2=10, 3=8, 4=6, 5=4, 6=2, 7=1, 8=3, 9=5, 10=7, 11=9, 12=11}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=11, 2=9, 3=7, 4=6, 5=4, 6=5, 7=2, 8=1, 9=3, 10=8, 11=10, 12=12}

