
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-S:1.0]

//
FINGERPRINTS RC
[O=C(SC[CH2])CC([CH])=[CH]>>[C]C([CH])NC(=O)CC([CH])=[CH]:1.0, O=C(SC[CH2])[CH2]:1.0, O=C(SC[CH2])[CH2]>>SC[CH2]:1.0, O=C([NH])[CH2]:1.0, O=C([S])[CH2]:1.0, O=C([S])[CH2]>>O=C([NH])[CH2]:1.0, O=C1[N]C([S])C1N>>[C]CC(=O)NC1C(=O)[N]C1[S]:1.0, SC[CH2]:1.0, S[CH2]:1.0, [CH]N:1.0, [CH]N>>[C]N[CH]:1.0, [C]:2.0, [C]C([CH])N:1.0, [C]C([CH])N>>[C]C([CH])NC(=O)[CH2]:1.0, [C]C([CH])NC(=O)[CH2]:1.0, [C]CC(=O)N[CH]:1.0, [C]CC(=O)SCC[NH]>>[NH]CCS:1.0, [C]CC(=O)S[CH2]:1.0, [C]CC(=O)S[CH2]>>[C]CC(=O)N[CH]:1.0, [C]N[CH]:1.0, [C]S[CH2]:1.0, [C]S[CH2]>>S[CH2]:1.0, [NH2]:1.0, [NH]:1.0, [SH]:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [NH]:1.0, [S]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([NH])[CH2]:1.0, O=C([S])[CH2]:1.0, [C]N[CH]:1.0, [C]S[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(SC[CH2])[CH2]:1.0, [C]C([CH])NC(=O)[CH2]:1.0, [C]CC(=O)N[CH]:1.0, [C]CC(=O)S[CH2]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]S[CH2]>>S[CH2]
2: [CH]N>>[C]N[CH]
3: O=C([S])[CH2]>>O=C([NH])[CH2]

MMP Level 2
1: O=C(SC[CH2])[CH2]>>SC[CH2]
2: [C]C([CH])N>>[C]C([CH])NC(=O)[CH2]
3: [C]CC(=O)S[CH2]>>[C]CC(=O)N[CH]

MMP Level 3
1: [C]CC(=O)SCC[NH]>>[NH]CCS
2: O=C1[N]C([S])C1N>>[C]CC(=O)NC1C(=O)[N]C1[S]
3: O=C(SC[CH2])CC([CH])=[CH]>>[C]C([CH])NC(=O)CC([CH])=[CH]


//
REACTION MMP (RPAIR)
1: R:M00002, P:M00004	[O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)Cc4ccccc4>>O=C(NCCS)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O]
2: R:M00002, P:M00005	[O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)Cc4ccccc4>>O=C1N2C(SC(C)(C)C2C(=O)O)C1NC(=O)Cc3ccccc3]
3: R:M00003, P:M00005	[O=C1N2C(SC(C)(C)C2C(=O)O)C1N>>O=C1N2C(SC(C)(C)C2C(=O)O)C1NC(=O)Cc3ccccc3]


//
SELECTED AAM MAPPING
[O:58]=[C:59]([OH:60])[CH:61]1[N:62]2[C:63](=[O:64])[CH:65]([NH2:66])[CH:67]2[S:68][C:69]1([CH3:70])[CH3:71].[O:1]=[C:2]([S:3][CH2:4][CH2:5][NH:6][C:7](=[O:8])[CH2:9][CH2:10][NH:11][C:12](=[O:13])[CH:14]([OH:15])[C:16]([CH3:17])([CH3:18])[CH2:19][O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[O:28][CH2:29][CH:30]1[O:31][CH:32]([N:33]:2:[CH:34]:[N:35]:[C:36]:3:[C:37](:[N:38]:[CH:39]:[N:40]:[C:41]32)[NH2:42])[CH:43]([OH:44])[CH:45]1[O:46][P:47](=[O:48])([OH:49])[OH:50])[CH2:51][C:52]:4:[CH:53]:[CH:54]:[CH:55]:[CH:56]:[CH:57]4.[OH2:72]>>[O:58]=[C:59]([OH:60])[CH:61]1[N:62]2[C:63](=[O:64])[CH:65]([NH:66][C:2](=[O:1])[CH2:51][C:52]:3:[CH:53]:[CH:54]:[CH:55]:[CH:56]:[CH:57]3)[CH:67]2[S:68][C:69]1([CH3:70])[CH3:71].[O:8]=[C:7]([NH:6][CH2:5][CH2:4][SH:3])[CH2:9][CH2:10][NH:11][C:12](=[O:13])[CH:14]([OH:15])[C:16]([CH3:17])([CH3:18])[CH2:19][O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[O:28][CH2:29][CH:30]1[O:31][CH:32]([N:33]:2:[CH:34]:[N:35]:[C:36]:3:[C:37](:[N:38]:[CH:39]:[N:40]:[C:41]32)[NH2:42])[CH:43]([OH:44])[CH:45]1[O:46][P:47](=[O:48])([OH:49])[OH:50]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=72, 2=17, 3=16, 4=18, 5=19, 6=20, 7=21, 8=22, 9=23, 10=24, 11=25, 12=26, 13=27, 14=28, 15=29, 16=30, 17=45, 18=43, 19=32, 20=31, 21=33, 22=34, 23=35, 24=36, 25=41, 26=40, 27=39, 28=38, 29=37, 30=42, 31=44, 32=46, 33=47, 34=48, 35=49, 36=50, 37=14, 38=12, 39=13, 40=11, 41=10, 42=9, 43=7, 44=8, 45=6, 46=5, 47=4, 48=3, 49=2, 50=1, 51=51, 52=52, 53=53, 54=54, 55=55, 56=56, 57=57, 58=15, 59=70, 60=69, 61=61, 62=62, 63=67, 64=68, 65=65, 66=63, 67=64, 68=66, 69=59, 70=58, 71=60, 72=71}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=15, 2=14, 3=16, 4=17, 5=18, 6=19, 7=20, 8=21, 9=22, 10=23, 11=24, 12=25, 13=26, 14=27, 15=28, 16=43, 17=41, 18=30, 19=29, 20=31, 21=32, 22=33, 23=34, 24=39, 25=38, 26=37, 27=36, 28=35, 29=40, 30=42, 31=44, 32=45, 33=46, 34=47, 35=48, 36=12, 37=10, 38=11, 39=9, 40=8, 41=7, 42=2, 43=1, 44=3, 45=4, 46=5, 47=6, 48=13, 49=70, 50=69, 51=52, 52=53, 53=67, 54=68, 55=56, 56=54, 57=55, 58=57, 59=58, 60=59, 61=60, 62=61, 63=62, 64=63, 65=64, 66=65, 67=66, 68=50, 69=49, 70=51, 71=71}

