
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0, C-O:1.0, C-S:1.0]

//
FINGERPRINTS RC
[O=C(SC[CH2])[CH2]:1.0, O=C(S[CH2])CC(=O)C>>O=C(S[CH2])C:1.0, O=C([CH2])C:2.0, O=C([CH2])C>>O=C([S])CO[CH]:1.0, O=C([CH2])C>>O=C([S])[CH2]:1.0, O=C([NH])C(O)C([CH2])(C)C>>O=C([S])COC(C(=O)[NH])C([CH2])(C)C:1.0, O=C([S])C:1.0, O=C([S])CC(=O)C:1.0, O=C([S])CC(=O)C>>O=C(SC[CH2])CO[CH]:1.0, O=C([S])CC(=O)C>>O=C([S])C:1.0, O=C([S])CO[CH]:1.0, O=C([S])[CH2]:1.0, SC[CH2]:1.0, SC[CH2]>>O=C(SC[CH2])[CH2]:1.0, S[CH2]:1.0, S[CH2]>>[C]S[CH2]:1.0, [CH2]:2.0, [CH3]:2.0, [CH]O:1.0, [CH]O>>[CH]O[CH2]:1.0, [CH]O[CH2]:1.0, [C]:2.0, [C]C:2.0, [C]C([C])O:1.0, [C]C([C])O>>[C]COC([C])[C]:1.0, [C]C>>[C]C[O]:1.0, [C]CC(=O)C:1.0, [C]CC(=O)C>>[C]C([C])OCC(=O)S[CH2]:1.0, [C]CC(=O)C>>[O]CC(=O)S[CH2]:1.0, [C]COC([C])[C]:1.0, [C]C[C]:1.0, [C]C[C]>>[C]C:1.0, [C]C[O]:1.0, [C]S[CH2]:1.0, [NH]CCS>>[O]CC(=O)SCC[NH]:1.0, [OH]:1.0, [O]:1.0, [O]CC(=O)S[CH2]:1.0, [SH]:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[CH2]:2.0, [C]:2.0, [O]:1.0, [S]:1.0]


ID=Reaction Center at Level: 1 (6)
[O=C([CH2])C:1.0, O=C([S])[CH2]:1.0, [CH]O[CH2]:1.0, [C]C[C]:1.0, [C]C[O]:1.0, [C]S[CH2]:1.0]


ID=Reaction Center at Level: 2 (6)
[O=C(SC[CH2])[CH2]:1.0, O=C([S])CC(=O)C:1.0, O=C([S])CO[CH]:1.0, [C]CC(=O)C:1.0, [C]COC([C])[C]:1.0, [O]CC(=O)S[CH2]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C[C]>>[C]C
2: S[CH2]>>[C]S[CH2]
3: [CH]O>>[CH]O[CH2]
4: O=C([CH2])C>>O=C([S])[CH2]
5: [C]C>>[C]C[O]

MMP Level 2
1: O=C([S])CC(=O)C>>O=C([S])C
2: SC[CH2]>>O=C(SC[CH2])[CH2]
3: [C]C([C])O>>[C]COC([C])[C]
4: [C]CC(=O)C>>[O]CC(=O)S[CH2]
5: O=C([CH2])C>>O=C([S])CO[CH]

MMP Level 3
1: O=C(S[CH2])CC(=O)C>>O=C(S[CH2])C
2: [NH]CCS>>[O]CC(=O)SCC[NH]
3: O=C([NH])C(O)C([CH2])(C)C>>O=C([S])COC(C(=O)[NH])C([CH2])(C)C
4: O=C([S])CC(=O)C>>O=C(SC[CH2])CO[CH]
5: [C]CC(=O)C>>[C]C([C])OCC(=O)S[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(NCCS)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O>>O=C(SCCNC(=O)CCNC(=O)C(OCC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4OP(=O)(O)O)C]
2: R:M00002, P:M00003	[O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)CC(=O)C>>O=C(SCCNC(=O)CCNC(=O)C(OCC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4OP(=O)(O)O)C, O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)CC(=O)C>>O=C(SCCNC(=O)CCNC(=O)C(OCC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4OP(=O)(O)O)C, O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)CC(=O)C>>O=C(SCCNC(=O)CCNC(=O)C(OCC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4OP(=O)(O)O)C, O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)CC(=O)C>>O=C(SCCNC(=O)CCNC(=O)C(OCC(=O)SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4OP(=O)(O)O)C]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([S:3][CH2:4][CH2:5][NH:6][C:7](=[O:8])[CH2:9][CH2:10][NH:11][C:12](=[O:13])[CH:14]([OH:15])[C:16]([CH3:17])([CH3:18])[CH2:19][O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[O:28][CH2:29][CH:30]1[O:31][CH:32]([N:33]:2:[CH:34]:[N:35]:[C:36]:3:[C:37](:[N:38]:[CH:39]:[N:40]:[C:41]32)[NH2:42])[CH:43]([OH:44])[CH:45]1[O:46][P:47](=[O:48])([OH:49])[OH:50])[CH2:51][C:52](=[O:53])[CH3:54].[O:55]=[C:56]([NH:57][CH2:58][CH2:59][SH:60])[CH2:61][CH2:62][NH:63][C:64](=[O:65])[CH:66]([OH:67])[C:68]([CH3:69])([CH3:70])[CH2:71][O:72][P:73](=[O:74])([OH:75])[O:76][P:77](=[O:78])([OH:79])[O:80][CH2:81][CH:82]1[O:83][CH:84]([N:85]:2:[CH:86]:[N:87]:[C:88]:3:[C:89](:[N:90]:[CH:91]:[N:92]:[C:93]32)[NH2:94])[CH:95]([OH:96])[CH:97]1[O:98][P:99](=[O:100])([OH:101])[OH:102]>>[O:1]=[C:2]([S:3][CH2:4][CH2:5][NH:6][C:7](=[O:8])[CH2:9][CH2:10][NH:11][C:12](=[O:13])[CH:14]([O:15][CH2:54][C:52](=[O:53])[S:60][CH2:59][CH2:58][NH:57][C:56](=[O:55])[CH2:61][CH2:62][NH:63][C:64](=[O:65])[CH:66]([OH:67])[C:68]([CH3:69])([CH3:70])[CH2:71][O:72][P:73](=[O:74])([OH:75])[O:76][P:77](=[O:78])([OH:79])[O:80][CH2:81][CH:82]1[O:83][CH:84]([N:85]:2:[CH:86]:[N:87]:[C:88]:3:[C:89](:[N:90]:[CH:91]:[N:92]:[C:93]32)[NH2:94])[CH:95]([OH:96])[CH:97]1[O:98][P:99](=[O:100])([OH:101])[OH:102])[C:16]([CH3:18])([CH3:17])[CH2:19][O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[O:28][CH2:29][CH:30]4[O:31][CH:32]([N:33]:5:[CH:34]:[N:35]:[C:36]:6:[C:37](:[N:38]:[CH:39]:[N:40]:[C:41]65)[NH2:42])[CH:43]([OH:44])[CH:45]4[O:46][P:47](=[O:48])([OH:50])[OH:49])[CH3:51]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=69, 2=68, 3=70, 4=71, 5=72, 6=73, 7=74, 8=75, 9=76, 10=77, 11=78, 12=79, 13=80, 14=81, 15=82, 16=97, 17=95, 18=84, 19=83, 20=85, 21=86, 22=87, 23=88, 24=93, 25=92, 26=91, 27=90, 28=89, 29=94, 30=96, 31=98, 32=99, 33=100, 34=101, 35=102, 36=66, 37=64, 38=65, 39=63, 40=62, 41=61, 42=56, 43=55, 44=57, 45=58, 46=59, 47=60, 48=67, 49=54, 50=52, 51=53, 52=51, 53=2, 54=1, 55=3, 56=4, 57=5, 58=6, 59=7, 60=8, 61=9, 62=10, 63=11, 64=12, 65=13, 66=14, 67=16, 68=17, 69=18, 70=19, 71=20, 72=21, 73=22, 74=23, 75=24, 76=25, 77=26, 78=27, 79=28, 80=29, 81=30, 82=45, 83=43, 84=32, 85=31, 86=33, 87=34, 88=35, 89=36, 90=41, 91=40, 92=39, 93=38, 94=37, 95=42, 96=44, 97=46, 98=47, 99=48, 100=49, 101=50, 102=15}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=102, 2=2, 3=1, 4=3, 5=4, 6=5, 7=6, 8=7, 9=8, 10=9, 11=10, 12=11, 13=12, 14=13, 15=14, 16=67, 17=68, 18=69, 19=70, 20=71, 21=72, 22=73, 23=74, 24=75, 25=76, 26=77, 27=78, 28=79, 29=80, 30=81, 31=96, 32=94, 33=83, 34=82, 35=84, 36=85, 37=86, 38=87, 39=92, 40=91, 41=90, 42=89, 43=88, 44=93, 45=95, 46=97, 47=98, 48=99, 49=100, 50=101, 51=15, 52=16, 53=17, 54=18, 55=19, 56=20, 57=21, 58=22, 59=23, 60=24, 61=25, 62=26, 63=27, 64=28, 65=29, 66=30, 67=32, 68=33, 69=34, 70=35, 71=36, 72=37, 73=38, 74=39, 75=40, 76=41, 77=42, 78=43, 79=44, 80=45, 81=46, 82=61, 83=59, 84=48, 85=47, 86=49, 87=50, 88=51, 89=52, 90=57, 91=56, 92=55, 93=54, 94=53, 95=58, 96=60, 97=62, 98=63, 99=64, 100=65, 101=66, 102=31}

