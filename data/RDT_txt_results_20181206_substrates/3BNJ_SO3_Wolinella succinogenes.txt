
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-S:2.0, O=S:1.0]

//
FINGERPRINTS RC
[O:3.0, O=S:1.0, O=S>>O:1.0, S:3.0, [O-]:2.0, [O-]S([O-])=O:5.0, [O-]S([O-])=O>>O:2.0, [O-]S([O-])=O>>S:3.0, [O-][S]:2.0, [O]:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[O:1.0, S:1.0, [O-]:2.0, [O]:1.0, [S]:3.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=S:1.0, S:1.0, [O-]S([O-])=O:3.0, [O-][S]:2.0]


ID=Reaction Center at Level: 2 (3)
[O:1.0, S:1.0, [O-]S([O-])=O:6.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O-]S([O-])=O>>S
2: O=S>>O

MMP Level 2
1: [O-]S([O-])=O>>S
2: [O-]S([O-])=O>>O

MMP Level 3
1: [O-]S([O-])=O>>S
2: [O-]S([O-])=O>>O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[[O-]S([O-])=O>>S]
2: R:M00001, P:M00004	[[O-]S([O-])=O>>O]


//
SELECTED AAM MAPPING
[H+:5].[O:1]=[S:2]([O-:3])[O-:4]>>[OH2:1].[SH2:2]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=3, 2=2, 3=1, 4=4, 5=5}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=1, 2=2}

