
//
FINGERPRINTS BC

ORDER_CHANGED
[C-C*C=C:1.0, C-O*C=O:2.0]

//
FINGERPRINTS RC
[O=C(O)CCC(=O)O>>O=C(O)C=CC(=O)O:2.0, O=C1C(=C[CH])C(=[CH])[C]C(=C1C[CH])C>>[CH]C=C1C(=[CH])[C]=C(C(=C1O)C[CH])C:1.0, O=C1C(=C[CH])C(=[CH])[C]C([CH2])=C1C>>[CH]C=C1C(=[CH])[C]=C([CH2])C(=C1O)C:1.0, [CH2]:2.0, [CH]:2.0, [C]:4.0, [C]=O:2.0, [C]=O>>[C]O:2.0, [C]C(=[CH])C(=O)C(=[C])C:1.0, [C]C(=[CH])C(=O)C(=[C])C>>[C]C(=[CH])C(O)=C([C])C:2.0, [C]C(=[CH])C(=O)C(=[C])[CH2]:1.0, [C]C(=[CH])C(=O)C(=[C])[CH2]>>[C]C(=[CH])C(O)=C([C])[CH2]:2.0, [C]C(=[CH])C(O)=C([C])C:1.0, [C]C(=[CH])C(O)=C([C])[CH2]:1.0, [C]C(=[C])O:4.0, [C]C([C])=O:4.0, [C]C([C])=O>>[C]C(=[C])O:4.0, [C]C=CC(=O)O:2.0, [C]C=[CH]:2.0, [C]CCC(=O)O:2.0, [C]CCC(=O)O>>[C]C=CC(=O)O:2.0, [C]C[CH2]:2.0, [C]C[CH2]>>[C]C=[CH]:2.0, [C]O:2.0, [OH]:2.0, [O]:2.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (5)
[[CH2]:2.0, [CH]:2.0, [C]:4.0, [OH]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (6)
[[C]=O:2.0, [C]C(=[C])O:2.0, [C]C([C])=O:2.0, [C]C=[CH]:2.0, [C]C[CH2]:2.0, [C]O:2.0]


ID=Reaction Center at Level: 2 (8)
[[C]C(=[CH])C(=O)C(=[C])C:1.0, [C]C(=[CH])C(=O)C(=[C])[CH2]:1.0, [C]C(=[CH])C(O)=C([C])C:1.0, [C]C(=[CH])C(O)=C([C])[CH2]:1.0, [C]C(=[C])O:2.0, [C]C([C])=O:2.0, [C]C=CC(=O)O:2.0, [C]CCC(=O)O:2.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C([C])=O>>[C]C(=[C])O
2: [C]=O>>[C]O
3: [C]C([C])=O>>[C]C(=[C])O
4: [C]C[CH2]>>[C]C=[CH]
5: [C]=O>>[C]O

MMP Level 2
1: [C]C(=[CH])C(=O)C(=[C])C>>[C]C(=[CH])C(O)=C([C])C
2: [C]C([C])=O>>[C]C(=[C])O
3: [C]C(=[CH])C(=O)C(=[C])[CH2]>>[C]C(=[CH])C(O)=C([C])[CH2]
4: [C]CCC(=O)O>>[C]C=CC(=O)O
5: [C]C([C])=O>>[C]C(=[C])O

MMP Level 3
1: O=C1C(=C[CH])C(=[CH])[C]C([CH2])=C1C>>[CH]C=C1C(=[CH])[C]=C([CH2])C(=C1O)C
2: [C]C(=[CH])C(=O)C(=[C])C>>[C]C(=[CH])C(O)=C([C])C
3: O=C1C(=C[CH])C(=[CH])[C]C(=C1C[CH])C>>[CH]C=C1C(=[CH])[C]=C(C(=C1O)C[CH])C
4: O=C(O)CCC(=O)O>>O=C(O)C=CC(=O)O
5: [C]C(=[CH])C(=O)C(=[C])[CH2]>>[C]C(=[CH])C(O)=C([C])[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)CCC(=O)O>>O=C(O)C=CC(=O)O]
2: R:M00002, P:M00004	[O=C1c2ccccc2C(=O)C(=C1C)CC=C(C)CCC=C(C)C>>Oc1c2ccccc2c(O)c(c1C)CC=C(C)CCC=C(C)C, O=C1c2ccccc2C(=O)C(=C1C)CC=C(C)CCC=C(C)C>>Oc1c2ccccc2c(O)c(c1C)CC=C(C)CCC=C(C)C, O=C1c2ccccc2C(=O)C(=C1C)CC=C(C)CCC=C(C)C>>Oc1c2ccccc2c(O)c(c1C)CC=C(C)CCC=C(C)C, O=C1c2ccccc2C(=O)C(=C1C)CC=C(C)CCC=C(C)C>>Oc1c2ccccc2c(O)c(c1C)CC=C(C)CCC=C(C)C]


//
SELECTED AAM MAPPING
[O:24]=[C:25]([OH:26])[CH2:27][CH2:28][C:29](=[O:30])[OH:31].[O:1]=[C:2]1[C:3]:2:[CH:4]:[CH:5]:[CH:6]:[CH:7]:[C:8]2[C:9](=[O:10])[C:11](=[C:12]1[CH3:13])[CH2:14][CH:15]=[C:16]([CH3:17])[CH2:18][CH2:19][CH:20]=[C:21]([CH3:22])[CH3:23]>>[O:24]=[C:25]([OH:26])[CH:27]=[CH:28][C:29](=[O:30])[OH:31].[OH:1][C:2]:1:[C:3]:2:[CH:4]:[CH:5]:[CH:6]:[CH:7]:[C:8]2:[C:9]([OH:10]):[C:11](:[C:12]1[CH3:13])[CH2:14][CH:15]=[C:16]([CH3:17])[CH2:18][CH2:19][CH:20]=[C:21]([CH3:22])[CH3:23]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=28, 2=27, 3=25, 4=24, 5=26, 6=29, 7=30, 8=31, 9=13, 10=12, 11=11, 12=9, 13=10, 14=8, 15=7, 16=6, 17=5, 18=4, 19=3, 20=2, 21=1, 22=14, 23=15, 24=16, 25=17, 26=18, 27=19, 28=20, 29=21, 30=22, 31=23}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=28, 2=27, 3=25, 4=24, 5=26, 6=29, 7=30, 8=31, 9=13, 10=12, 11=2, 12=3, 13=4, 14=5, 15=6, 16=7, 17=8, 18=9, 19=11, 20=14, 21=15, 22=16, 23=17, 24=18, 25=19, 26=20, 27=21, 28=22, 29=23, 30=10, 31=1}

