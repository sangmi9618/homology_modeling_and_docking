
//
FINGERPRINTS BC

ORDER_CHANGED
[C-O*C=O:1.0]

//
FINGERPRINTS RC
[O=C(N)C=1C=[N+]C=CC1>>O=C(N)C1=C[N]C=CC1:1.0, O=C=O:3.0, O=CO:3.0, O=CO>>O=C=O:5.0, [CH2]:1.0, [CH]:2.0, [CH]N([CH])[CH]:1.0, [CH]O:1.0, [CH]O>>[C]=O:1.0, [CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]>>[CH]N([CH])[CH]:1.0, [C]:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN(C=[CH])C([O])[CH]:1.0, [C]=O:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])C=C[CH]>>[C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C=[CH]:1.0, [C]C=[CH]>>[C]C[CH]:1.0, [C]C[CH]:1.0, [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]C1=CN(C=CC1)C2O[CH][CH]C2O:1.0, [N+]:1.0, [N]:1.0, [OH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH]:1.0, [C]:1.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C=O:1.0, O=CO:1.0, [CH]O:1.0, [C]=O:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C=O:2.0, O=CO:2.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C=[CH]>>[C]C[CH]
2: [CH][N+]([CH])=[CH]>>[CH]N([CH])[CH]
3: [CH]O>>[C]=O
4: O=CO>>O=C=O

MMP Level 2
1: [C]C(=[CH])C=C[CH]>>[C]C(=[CH])CC=[CH]
2: [C]=C[N+](=C[CH])C([O])[CH]>>[C]=CN(C=[CH])C([O])[CH]
3: O=CO>>O=C=O
4: O=CO>>O=C=O

MMP Level 3
1: O=C(N)C=1C=[N+]C=CC1>>O=C(N)C1=C[N]C=CC1
2: [C]c1ccc[n+](c1)C2O[CH][CH]C2O>>[C]C1=CN(C=CC1)C2O[CH][CH]C2O
3: O=CO>>O=C=O
4: O=CO>>O=C=O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O, O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O]
2: R:M00002, P:M00003	[O=CO>>O=C=O, O=CO>>O=C=O]


//
SELECTED AAM MAPPING
[O:45]=[CH:46][OH:47].[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:5]:[CH:6]:[CH:7]:[N+:8](:[CH:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44]>>[O:45]=[C:46]=[O:47].[O:1]=[C:2]([NH2:3])[C:4]1=[CH:9][N:8]([CH:7]=[CH:6][CH2:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=5, 3=4, 4=9, 5=8, 6=7, 7=10, 8=43, 9=41, 10=12, 11=11, 12=13, 13=14, 14=15, 15=16, 16=17, 17=18, 18=19, 19=20, 20=21, 21=22, 22=23, 23=24, 24=39, 25=37, 26=26, 27=25, 28=27, 29=28, 30=29, 31=30, 32=35, 33=34, 34=33, 35=32, 36=31, 37=36, 38=38, 39=40, 40=42, 41=44, 42=2, 43=1, 44=3, 45=46, 46=45, 47=47}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=46, 2=45, 3=47, 4=9, 5=8, 6=7, 7=6, 8=5, 9=4, 10=2, 11=1, 12=3, 13=10, 14=43, 15=41, 16=12, 17=11, 18=13, 19=14, 20=15, 21=16, 22=17, 23=18, 24=19, 25=20, 26=21, 27=22, 28=23, 29=24, 30=39, 31=37, 32=26, 33=25, 34=27, 35=28, 36=29, 37=30, 38=35, 39=34, 40=33, 41=32, 42=31, 43=36, 44=38, 45=40, 46=42, 47=44}

