
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0, C=O:2.0]

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O=C(C)C(O)[CH2]:1.0, O=C(CO)C(O)C(O)[CH2]>>O=C(C)C(O)[CH2]:1.0, O=C(CO)C([CH])O:1.0, O=C(CO)C([CH])O>>O=C([CH])C:1.0, O=C(CO)C([CH])O>>O=CO:2.0, O=C([CH2])C(O)C(O)CO[P]>>O=C(C)C(O)CO[P]:1.0, O=C([CH2])C(O)C(O)[CH2]:1.0, O=C([CH2])C(O)C(O)[CH2]>>O=C(C)C(O)[CH2]:2.0, O=C([CH])C:3.0, O=C([CH])CO:1.0, O=C([CH])CO>>O=CO:1.0, O=C([CH])[CH2]:2.0, O=C([CH])[CH2]>>O=CO:1.0, O=C([CH])[CH2]>>[C]C:1.0, O=CO:3.0, O=[CH]:1.0, [CH2]:1.0, [CH3]:1.0, [CH]:4.0, [CH]C(O)[CH2]:1.0, [CH]C(O)[CH2]>>[C]C(O)[CH2]:1.0, [CH]O:1.0, [CH]O>>[C]=O:1.0, [C]:2.0, [C]=O:2.0, [C]=O>>O=[CH]:1.0, [C]C:1.0, [C]C(O)C(O)C[O]:1.0, [C]C(O)C(O)C[O]>>[O]CC(O)C(=O)C:1.0, [C]C(O)[CH2]:1.0, [C]C([CH])O:2.0, [C]C([CH])O>>O=C([CH])C:2.0, [C]CO:1.0, [C]CO>>O=CO:1.0, [OH]:1.0, [O]:3.0, [O]CC(O)C(=O)C:1.0, [O]CC(O)C(O)C(=O)CO>>[O]CC(O)C(=O)C:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[CH2]:1.0, [CH]:1.0, [C]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (5)
[O=C([CH])[CH2]:2.0, O=CO:1.0, O=[CH]:1.0, [C]=O:1.0, [C]CO:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(CO)C([CH])O:2.0, O=C([CH])CO:1.0, O=C([CH])[CH2]:1.0, O=CO:2.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH]:1.0, [C]:1.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([CH])C:1.0, [CH]O:1.0, [C]=O:1.0, [C]C([CH])O:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(C)C(O)[CH2]:1.0, O=C([CH2])C(O)C(O)[CH2]:1.0, O=C([CH])C:1.0, [C]C([CH])O:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:3.0, [C]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([CH])C:1.0, [CH]C(O)[CH2]:1.0, [C]C(O)[CH2]:1.0, [C]C([CH])O:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(C)C(O)[CH2]:1.0, O=C([CH2])C(O)C(O)[CH2]:1.0, [C]C(O)C(O)C[O]:1.0, [O]CC(O)C(=O)C:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]=O>>O=[CH]
2: O=C([CH])[CH2]>>[C]C
3: [CH]C(O)[CH2]>>[C]C(O)[CH2]
4: [C]CO>>O=CO
5: [C]C([CH])O>>O=C([CH])C
6: [CH]O>>[C]=O

MMP Level 2
1: O=C([CH])[CH2]>>O=CO
2: O=C(CO)C([CH])O>>O=C([CH])C
3: [C]C(O)C(O)C[O]>>[O]CC(O)C(=O)C
4: O=C([CH])CO>>O=CO
5: O=C([CH2])C(O)C(O)[CH2]>>O=C(C)C(O)[CH2]
6: [C]C([CH])O>>O=C([CH])C

MMP Level 3
1: O=C(CO)C([CH])O>>O=CO
2: O=C(CO)C(O)C(O)[CH2]>>O=C(C)C(O)[CH2]
3: O=C([CH2])C(O)C(O)CO[P]>>O=C(C)C(O)CO[P]
4: O=C(CO)C([CH])O>>O=CO
5: [O]CC(O)C(O)C(=O)CO>>[O]CC(O)C(=O)C
6: O=C([CH2])C(O)C(O)[CH2]>>O=C(C)C(O)[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(CO)C(O)C(O)COP(=O)(O)O>>O=CO, O=C(CO)C(O)C(O)COP(=O)(O)O>>O=CO]
2: R:M00001, P:M00003	[O=C(CO)C(O)C(O)COP(=O)(O)O>>O=C(C)C(O)COP(=O)(O)O, O=C(CO)C(O)C(O)COP(=O)(O)O>>O=C(C)C(O)COP(=O)(O)O, O=C(CO)C(O)C(O)COP(=O)(O)O>>O=C(C)C(O)COP(=O)(O)O, O=C(CO)C(O)C(O)COP(=O)(O)O>>O=C(C)C(O)COP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([CH2:3][OH:4])[CH:5]([OH:6])[CH:7]([OH:8])[CH2:9][O:10][P:11](=[O:12])([OH:13])[OH:14]>>[O:1]=[CH:3][OH:4].[O:6]=[C:5]([CH3:2])[CH:7]([OH:8])[CH2:9][O:10][P:11](=[O:12])([OH:13])[OH:14]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=7, 3=5, 4=2, 5=1, 6=3, 7=4, 8=6, 9=8, 10=10, 11=11, 12=12, 13=13, 14=14}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=13, 2=12, 3=14, 4=3, 5=2, 6=1, 7=4, 8=6, 9=7, 10=8, 11=9, 12=10, 13=11, 14=5}

