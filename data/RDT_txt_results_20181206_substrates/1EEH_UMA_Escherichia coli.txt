
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0, O-P:2.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O=C(O)C(N)(C[CH])C[CH2]:1.0, O=C(O)C(N)(C[CH])C[CH2]>>O=C(O)C(NC(=O)C([NH])C)(C[CH])C[CH2]:1.0, O=C(O)C(N)(C[CH])C[CH2]>>[C]NC(C(=O)O)(C[CH])C[CH2]:1.0, O=C(O)C([NH])C:1.0, O=C(O)C([NH])C>>O=P(O)(O)O:1.0, O=C(O)C([NH])C>>[C]NC(=O)C([NH])C:1.0, O=C([CH])O:2.0, O=C([CH])O>>O=C([CH])[NH]:1.0, O=C([CH])O>>O=P(O)(O)O:1.0, O=C([CH])[NH]:1.0, O=P(O)(O)O:3.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O:1.0, [CH]:2.0, [C]:4.0, [C]C([CH2])([CH2])N:2.0, [C]C([CH2])([CH2])N>>[C]C([CH2])([CH2])NC(=O)[CH]:1.0, [C]C([CH2])([CH2])N>>[C]C([NH])([CH2])[CH2]:1.0, [C]C([CH2])([CH2])NC(=O)[CH]:1.0, [C]C([CH2])[CH2]:2.0, [C]C([CH2])[CH2]>>[C]C([CH2])[CH2]:1.0, [C]C([NH])([CH2])[CH2]:1.0, [C]C1(N)CCCC(C(=O)O)C1>>[C]C1([NH])CCCC(C(=O)O)C1:1.0, [C]C1CCCC(N)(C(=O)O)C1>>[C]C1CCCC(NC(=O)[CH])(C(=O)O)C1:1.0, [C]CC(C(=O)O)C[CH2]:2.0, [C]CC(C(=O)O)C[CH2]>>[C]CC(C(=O)O)C[CH2]:1.0, [C]N:1.0, [C]N>>[C]N[C]:1.0, [C]NC(=O)C([NH])C:1.0, [C]NC(C(=O)O)(C[CH])C[CH2]:1.0, [C]NC(C(=O)O)C>>[C]NC(C(=O)NC([C])([CH2])[CH2])C:1.0, [C]N[C]:1.0, [C]O:1.0, [C]O>>[P]O:1.0, [NH2]:1.0, [NH]:1.0, [OH]:3.0, [O]:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[C]:2.0, [NH]:1.0, [OH]:2.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (8)
[O=C([CH])O:1.0, O=C([CH])[NH]:1.0, O=P(O)(O)O:1.0, [C]N[C]:1.0, [C]O:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (7)
[O=C(O)C([NH])C:1.0, O=C([CH])O:1.0, O=P(O)(O)O:2.0, O=P(O)(O)O[P]:1.0, [C]C([CH2])([CH2])NC(=O)[CH]:1.0, [C]NC(=O)C([NH])C:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:2.0, [C]:2.0]


ID=Reaction Center at Level: 1 (3)
[[C]C([CH2])([CH2])N:1.0, [C]C([CH2])[CH2]:2.0, [C]C([NH])([CH2])[CH2]:1.0]


ID=Reaction Center at Level: 2 (3)
[O=C(O)C(N)(C[CH])C[CH2]:1.0, [C]CC(C(=O)O)C[CH2]:2.0, [C]NC(C(=O)O)(C[CH])C[CH2]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [O]P(=O)(O)O>>O=P(O)(O)O
2: [C]N>>[C]N[C]
3: [C]O>>[P]O
4: [C]C([CH2])([CH2])N>>[C]C([NH])([CH2])[CH2]
5: O=C([CH])O>>O=C([CH])[NH]
6: [P]O[P]>>[P]O
7: [C]C([CH2])[CH2]>>[C]C([CH2])[CH2]

MMP Level 2
1: O=P(O)(O)O[P]>>O=P(O)(O)O
2: [C]C([CH2])([CH2])N>>[C]C([CH2])([CH2])NC(=O)[CH]
3: O=C([CH])O>>O=P(O)(O)O
4: O=C(O)C(N)(C[CH])C[CH2]>>[C]NC(C(=O)O)(C[CH])C[CH2]
5: O=C(O)C([NH])C>>[C]NC(=O)C([NH])C
6: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
7: [C]CC(C(=O)O)C[CH2]>>[C]CC(C(=O)O)C[CH2]

MMP Level 3
1: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O
2: O=C(O)C(N)(C[CH])C[CH2]>>O=C(O)C(NC(=O)C([NH])C)(C[CH])C[CH2]
3: O=C(O)C([NH])C>>O=P(O)(O)O
4: [C]C1CCCC(N)(C(=O)O)C1>>[C]C1CCCC(NC(=O)[CH])(C(=O)O)C1
5: [C]NC(C(=O)O)C>>[C]NC(C(=O)NC([C])([CH2])[CH2])C
6: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
7: [C]C1(N)CCCC(C(=O)O)C1>>[C]C1([NH])CCCC(C(=O)O)C1


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00005	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)O]
3: R:M00002, P:M00005	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(OC(C(=O)NC(C(=O)O)C)C)C3NC(=O)C)C(O)C2O>>O=P(O)(O)O]
4: R:M00002, P:M00006	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(OC(C(=O)NC(C(=O)O)C)C)C3NC(=O)C)C(O)C2O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(OC(C(=O)NC(C(=O)NC4(C(=O)O)CCCC(C(=O)O)C4)C)C)C3NC(=O)C)C(O)C2O]
5: R:M00003, P:M00006	[O=C(O)C1CCCC(N)(C(=O)O)C1>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(OC(C(=O)NC(C(=O)NC4(C(=O)O)CCCC(C(=O)O)C4)C)C)C3NC(=O)C)C(O)C2O, O=C(O)C1CCCC(N)(C(=O)O)C1>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(OC(C(=O)NC(C(=O)NC4(C(=O)O)CCCC(C(=O)O)C4)C)C)C3NC(=O)C)C(O)C2O, O=C(O)C1CCCC(N)(C(=O)O)C1>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(OC(C(=O)NC(C(=O)NC4(C(=O)O)CCCC(C(=O)O)C4)C)C)C3NC(=O)C)C(O)C2O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([NH:5][C:6](=[O:7])[CH:8]([O:9][CH:10]1[CH:11]([OH:12])[CH:13]([O:14][CH:15]([O:16][P:17](=[O:18])([OH:19])[O:20][P:21](=[O:22])([OH:23])[O:24][CH2:25][CH:26]2[O:27][CH:28]([N:29]3[CH:30]=[CH:31][C:32](=[O:33])[NH:34][C:35]3=[O:36])[CH:37]([OH:38])[CH:39]2[OH:40])[CH:41]1[NH:42][C:43](=[O:44])[CH3:45])[CH2:46][OH:47])[CH3:48])[CH3:49].[O:81]=[C:82]([OH:83])[CH:84]1[CH2:85][CH2:86][CH2:87][C:88]([NH2:89])([C:90](=[O:91])[OH:92])[CH2:93]1.[O:50]=[P:51]([OH:52])([OH:53])[O:54][P:55](=[O:56])([OH:57])[O:58][P:59](=[O:60])([OH:61])[O:62][CH2:63][CH:64]1[O:65][CH:66]([N:67]:2:[CH:68]:[N:69]:[C:70]:3:[C:71](:[N:72]:[CH:73]:[N:74]:[C:75]32)[NH2:76])[CH:77]([OH:78])[CH:79]1[OH:80]>>[O:81]=[C:82]([OH:83])[CH:84]1[CH2:85][CH2:86][CH2:87][C:88]([NH:89][C:2](=[O:1])[CH:4]([NH:5][C:6](=[O:7])[CH:8]([O:9][CH:10]2[CH:11]([OH:12])[CH:13]([O:14][CH:15]([O:16][P:17](=[O:18])([OH:19])[O:20][P:21](=[O:22])([OH:23])[O:24][CH2:25][CH:26]3[O:27][CH:28]([N:29]4[CH:30]=[CH:31][C:32](=[O:33])[NH:34][C:35]4=[O:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]2[NH:42][C:43](=[O:44])[CH3:45])[CH2:46][OH:47])[CH3:48])[CH3:49])([C:90](=[O:91])[OH:92])[CH2:93]1.[O:50]=[P:51]([OH:52])([OH:53])[OH:3].[O:56]=[P:55]([OH:54])([OH:57])[O:58][P:59](=[O:60])([OH:61])[O:62][CH2:63][CH:64]1[O:65][CH:66]([N:67]:2:[CH:68]:[N:69]:[C:70]:3:[C:71](:[N:72]:[CH:73]:[N:74]:[C:75]32)[NH2:76])[CH:77]([OH:78])[CH:79]1[OH:80]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=73, 2=74, 3=75, 4=70, 5=71, 6=72, 7=76, 8=69, 9=68, 10=67, 11=66, 12=77, 13=79, 14=64, 15=65, 16=63, 17=62, 18=59, 19=60, 20=61, 21=58, 22=55, 23=56, 24=57, 25=54, 26=51, 27=50, 28=52, 29=53, 30=80, 31=78, 32=49, 33=4, 34=2, 35=1, 36=3, 37=5, 38=6, 39=7, 40=8, 41=48, 42=9, 43=10, 44=41, 45=15, 46=14, 47=13, 48=11, 49=12, 50=46, 51=47, 52=16, 53=17, 54=18, 55=19, 56=20, 57=21, 58=22, 59=23, 60=24, 61=25, 62=26, 63=39, 64=37, 65=28, 66=27, 67=29, 68=30, 69=31, 70=32, 71=33, 72=34, 73=35, 74=36, 75=38, 76=40, 77=42, 78=43, 79=44, 80=45, 81=86, 82=85, 83=84, 84=93, 85=88, 86=87, 87=90, 88=91, 89=92, 90=89, 91=82, 92=81, 93=83}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=81, 2=82, 3=83, 4=78, 5=79, 6=80, 7=84, 8=77, 9=76, 10=75, 11=74, 12=85, 13=87, 14=72, 15=73, 16=71, 17=70, 18=67, 19=68, 20=69, 21=66, 22=63, 23=62, 24=64, 25=65, 26=88, 27=86, 28=91, 29=90, 30=89, 31=92, 32=93, 33=57, 34=12, 35=10, 36=11, 37=9, 38=8, 39=7, 40=6, 41=5, 42=4, 43=61, 44=2, 45=1, 46=3, 47=58, 48=59, 49=60, 50=13, 51=14, 52=15, 53=16, 54=56, 55=17, 56=18, 57=49, 58=23, 59=22, 60=21, 61=19, 62=20, 63=54, 64=55, 65=24, 66=25, 67=26, 68=27, 69=28, 70=29, 71=30, 72=31, 73=32, 74=33, 75=34, 76=47, 77=45, 78=36, 79=35, 80=37, 81=38, 82=39, 83=40, 84=41, 85=42, 86=43, 87=44, 88=46, 89=48, 90=50, 91=51, 92=52, 93=53}

