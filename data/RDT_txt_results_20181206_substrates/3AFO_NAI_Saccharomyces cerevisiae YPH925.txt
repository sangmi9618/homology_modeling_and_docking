
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=P(O)(O)OC([CH])[CH]:1.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[CH]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O[CH]:1.0, OC1[CH]OC([CH2])C1O:1.0, OC1[CH]OC([CH2])C1O>>[O]C1[CH]OC([CH2])C1O:1.0, [CH]:2.0, [CH]C([CH])O:3.0, [CH]C([CH])O>>O=P(O)(O)OC([CH])[CH]:1.0, [CH]C([CH])O>>[CH]C([CH])O:1.0, [CH]O:1.0, [CH]O>>[P]O[CH]:1.0, [N]C1OC(C[O])C(O)C1O>>[N]C1OC(C[O])C(O)C1O[P]:1.0, [N]C1O[CH]C(O)C1O>>[N]C1O[CH]C(O)C1OP(=O)(O)O:1.0, [OH]:2.0, [O]:2.0, [O]C1[CH]OC([CH2])C1O:1.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC([CH])[CH]:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[O]P(=O)(O)O:2.0, [P]O[CH]:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)OC([CH])[CH]:1.0, O=P(O)(O)O[CH]:1.0, O=P(O)(O)O[P]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (1)
[[CH]C([CH])O:2.0]


ID=Reaction Center at Level: 2 (2)
[OC1[CH]OC([CH2])C1O:1.0, [O]C1[CH]OC([CH2])C1O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]C([CH])O>>[CH]C([CH])O
2: [P]O[P]>>[P]O
3: [CH]O>>[P]O[CH]
4: [O]P(=O)(O)O>>[O]P(=O)(O)O

MMP Level 2
1: OC1[CH]OC([CH2])C1O>>[O]C1[CH]OC([CH2])C1O
2: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
3: [CH]C([CH])O>>O=P(O)(O)OC([CH])[CH]
4: O=P(O)(O)O[P]>>O=P(O)(O)O[CH]

MMP Level 3
1: [N]C1OC(C[O])C(O)C1O>>[N]C1OC(C[O])C(O)C1O[P]
2: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
3: [N]C1O[CH]C(O)C1O>>[N]C1O[CH]C(O)C1OP(=O)(O)O
4: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC([CH])[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
3: R:M00002, P:M00003	[O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(O)C3O)C(O)C2O>>O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([NH2:3])[C:4]1=[CH:5][N:6]([CH:7]=[CH:8][CH2:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([OH:38])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44].[O:45]=[P:46]([OH:47])([OH:48])[O:49][P:50](=[O:51])([OH:52])[O:53][P:54](=[O:55])([OH:56])[O:57][CH2:58][CH:59]1[O:60][CH:61]([N:62]:2:[CH:63]:[N:64]:[C:65]:3:[C:66](:[N:67]:[CH:68]:[N:69]:[C:70]32)[NH2:71])[CH:72]([OH:73])[CH:74]1[OH:75]>>[O:1]=[C:2]([NH2:3])[C:4]1=[CH:5][N:6]([CH:7]=[CH:8][CH2:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:46](=[O:45])([OH:47])[OH:48])[CH:39]3[OH:40])[CH:41]([OH:42])[CH:43]2[OH:44].[O:51]=[P:50]([OH:49])([OH:52])[O:53][P:54](=[O:55])([OH:56])[O:57][CH2:58][CH:59]1[O:60][CH:61]([N:62]:2:[CH:63]:[N:64]:[C:65]:3:[C:66](:[N:67]:[CH:68]:[N:69]:[C:70]32)[NH2:71])[CH:72]([OH:73])[CH:74]1[OH:75]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=68, 2=69, 3=70, 4=65, 5=66, 6=67, 7=71, 8=64, 9=63, 10=62, 11=61, 12=72, 13=74, 14=59, 15=60, 16=58, 17=57, 18=54, 19=55, 20=56, 21=53, 22=50, 23=51, 24=52, 25=49, 26=46, 27=45, 28=47, 29=48, 30=75, 31=73, 32=9, 33=8, 34=7, 35=6, 36=5, 37=4, 38=2, 39=1, 40=3, 41=10, 42=43, 43=41, 44=12, 45=11, 46=13, 47=14, 48=15, 49=16, 50=17, 51=18, 52=19, 53=20, 54=21, 55=22, 56=23, 57=24, 58=39, 59=37, 60=26, 61=25, 62=27, 63=28, 64=29, 65=30, 66=35, 67=34, 68=33, 69=32, 70=31, 71=36, 72=38, 73=40, 74=42, 75=44}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=6, 5=5, 6=4, 7=2, 8=1, 9=3, 10=10, 11=47, 12=45, 13=12, 14=11, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=43, 28=37, 29=26, 30=25, 31=27, 32=28, 33=29, 34=30, 35=35, 36=34, 37=33, 38=32, 39=31, 40=36, 41=38, 42=39, 43=40, 44=41, 45=42, 46=44, 47=46, 48=48, 49=68, 50=69, 51=70, 52=65, 53=66, 54=67, 55=71, 56=64, 57=63, 58=62, 59=61, 60=72, 61=74, 62=59, 63=60, 64=58, 65=57, 66=54, 67=55, 68=56, 69=53, 70=50, 71=49, 72=51, 73=52, 74=75, 75=73}

