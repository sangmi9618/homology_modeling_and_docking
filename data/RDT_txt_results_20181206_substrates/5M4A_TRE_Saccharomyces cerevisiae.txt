
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:3.0]

STEREO_CHANGED
[C(R/S):3.0]

//
FINGERPRINTS RC
[O:3.0, O>>[CH]O:1.0, O>>[CH]OC(O)C([CH])O:1.0, O>>[O]C([CH])O:1.0, O[CH2]:1.0, O[CH2]>>[CH]O[CH2]:1.0, [CH2]:1.0, [CH]:7.0, [CH]CO:2.0, [CH]CO>>[CH]COC([CH])O:1.0, [CH]CO>>[O]C([CH])O:1.0, [CH]COC([CH])O:1.0, [CH]O:2.0, [CH]OC(CO)C([CH])O:1.0, [CH]OC(CO)C([CH])O>>[CH]OC(C([CH])O)C(O)OC[CH]:1.0, [CH]OC(CO)C([CH])O>>[O]C(O)C(O[CH])C([CH])O:1.0, [CH]OC(O)C([CH])O:2.0, [CH]OC(OC(O[CH])C([CH])O)C([CH])O>>[CH]OC(O)C([CH])O:1.0, [CH]OC(O[CH])C([CH])O:2.0, [CH]OC(O[CH])C([CH])O>>[CH]OC(O)C([CH])O:2.0, [CH]O[CH2]:1.0, [CH]O[CH]:1.0, [CH]O[CH]>>[CH]O:1.0, [OH]:3.0, [O]:2.0, [O]C(O)C(O[CH])C([CH])O:1.0, [O]C([CH])C(O)O[CH2]:1.0, [O]C([CH])CO:1.0, [O]C([CH])CO>>[O]C([CH])C(O)O[CH2]:1.0, [O]C([CH])CO>>[O]C([CH])COC(O)C([O])[CH]:1.0, [O]C([CH])O:5.0, [O]C([CH])OC([O])[CH]:1.0, [O]C([CH])OC([O])[CH]>>[O]C([CH])O:1.0, [O]C([CH])OC1OC([CH2])[CH]C(O)C1O>>OC1OC([CH2])[CH]C(O)C1O:1.0, [O]C([CH])OC1OC([CH2])[CH]C(O)C1O>>[CH]C1OC(O)C(O)C(O)[CH]1:1.0, [O]C([CH])[CH2]:1.0, [O]C([CH])[CH2]>>[O]C([CH])[CH]:1.0, [O]C([CH])[CH]:1.0, [O]C([O])[CH]:2.0, [O]C([O])[CH]>>[O]C([CH])O:2.0, [O]C1OC(CO)C(O)C(O)[CH]1>>OC1OC(C(O)C(O)[CH]1)C(O)O[CH2]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [CH]:3.0, [OH]:1.0, [O]:2.0]


ID=Reaction Center at Level: 1 (6)
[O:1.0, [CH]O:1.0, [CH]O[CH2]:1.0, [CH]O[CH]:1.0, [O]C([CH])O:2.0, [O]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (7)
[O:1.0, [CH]COC([CH])O:1.0, [CH]OC(O)C([CH])O:1.0, [CH]OC(O[CH])C([CH])O:1.0, [O]C([CH])C(O)O[CH2]:1.0, [O]C([CH])O:1.0, [O]C([CH])OC([O])[CH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:6.0]


ID=Reaction Center at Level: 1 (4)
[[O]C([CH])O:2.0, [O]C([CH])[CH2]:1.0, [O]C([CH])[CH]:1.0, [O]C([O])[CH]:2.0]


ID=Reaction Center at Level: 2 (4)
[[CH]OC(CO)C([CH])O:1.0, [CH]OC(O)C([CH])O:2.0, [CH]OC(O[CH])C([CH])O:2.0, [O]C(O)C(O[CH])C([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]O[CH]>>[CH]O
2: O>>[CH]O
3: [O]C([CH])[CH2]>>[O]C([CH])[CH]
4: [O]C([O])[CH]>>[O]C([CH])O
5: [CH]CO>>[O]C([CH])O
6: O[CH2]>>[CH]O[CH2]
7: [O]C([O])[CH]>>[O]C([CH])O

MMP Level 2
1: [O]C([CH])OC([O])[CH]>>[O]C([CH])O
2: O>>[O]C([CH])O
3: [CH]OC(CO)C([CH])O>>[O]C(O)C(O[CH])C([CH])O
4: [CH]OC(O[CH])C([CH])O>>[CH]OC(O)C([CH])O
5: [O]C([CH])CO>>[O]C([CH])C(O)O[CH2]
6: [CH]CO>>[CH]COC([CH])O
7: [CH]OC(O[CH])C([CH])O>>[CH]OC(O)C([CH])O

MMP Level 3
1: [CH]OC(OC(O[CH])C([CH])O)C([CH])O>>[CH]OC(O)C([CH])O
2: O>>[CH]OC(O)C([CH])O
3: [O]C1OC(CO)C(O)C(O)[CH]1>>OC1OC(C(O)C(O)[CH]1)C(O)O[CH2]
4: [O]C([CH])OC1OC([CH2])[CH]C(O)C1O>>OC1OC([CH2])[CH]C(O)C1O
5: [CH]OC(CO)C([CH])O>>[CH]OC(C([CH])O)C(O)OC[CH]
6: [O]C([CH])CO>>[O]C([CH])COC(O)C([O])[CH]
7: [O]C([CH])OC1OC([CH2])[CH]C(O)C1O>>[CH]C1OC(O)C(O)C(O)[CH]1


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[OCC1OC(OC2OC(CO)C(O)C(O)C2O)C(O)C(O)C1O>>OC1OC(COC(O)C2OC(O)C(O)C(O)C2O)C(O)C(O)C1O, OCC1OC(OC2OC(CO)C(O)C(O)C2O)C(O)C(O)C1O>>OC1OC(COC(O)C2OC(O)C(O)C(O)C2O)C(O)C(O)C1O, OCC1OC(OC2OC(CO)C(O)C(O)C2O)C(O)C(O)C1O>>OC1OC(COC(O)C2OC(O)C(O)C(O)C2O)C(O)C(O)C1O, OCC1OC(OC2OC(CO)C(O)C(O)C2O)C(O)C(O)C1O>>OC1OC(COC(O)C2OC(O)C(O)C(O)C2O)C(O)C(O)C1O, OCC1OC(OC2OC(CO)C(O)C(O)C2O)C(O)C(O)C1O>>OC1OC(COC(O)C2OC(O)C(O)C(O)C2O)C(O)C(O)C1O, OCC1OC(OC2OC(CO)C(O)C(O)C2O)C(O)C(O)C1O>>OC1OC(COC(O)C2OC(O)C(O)C(O)C2O)C(O)C(O)C1O]
2: R:M00002, P:M00003	[O>>OC1OC(COC(O)C2OC(O)C(O)C(O)C2O)C(O)C(O)C1O]


//
SELECTED AAM MAPPING
[OH2:24].[OH:1][CH2:2][CH:3]1[O:4][CH:5]([O:6][CH:7]2[O:8][CH:9]([CH2:10][OH:11])[CH:12]([OH:13])[CH:14]([OH:15])[CH:16]2[OH:17])[CH:18]([OH:19])[CH:20]([OH:21])[CH:22]1[OH:23]>>[OH:6][CH:7]1[O:8][CH:9]([CH2:10][O:11][CH:2]([OH:1])[CH:3]2[O:4][CH:5]([OH:24])[CH:18]([OH:19])[CH:20]([OH:21])[CH:22]2[OH:23])[CH:12]([OH:13])[CH:14]([OH:15])[CH:16]1[OH:17]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=10, 2=9, 3=12, 4=14, 5=16, 6=7, 7=8, 8=6, 9=5, 10=18, 11=20, 12=22, 13=3, 14=4, 15=2, 16=1, 17=23, 18=21, 19=19, 20=17, 21=15, 22=13, 23=11, 24=24}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=5, 2=4, 3=19, 4=21, 5=23, 6=2, 7=3, 8=1, 9=24, 10=22, 11=20, 12=6, 13=7, 14=9, 15=17, 16=15, 17=13, 18=11, 19=10, 20=12, 21=14, 22=16, 23=18, 24=8}

