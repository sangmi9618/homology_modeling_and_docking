
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0, C-S:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(O)C:1.0, O=C(O)C(N)COC(=O)C>>O=C(O)C(N)CS:1.0, O=C(O)C(N)CS:1.0, O=C(OC[CH])C:1.0, O=C(OC[CH])C>>O=C(O)C:1.0, S:3.0, S>>S[CH2]:1.0, S>>[CH]CS:1.0, S>>[C]C(N)CS:1.0, S[CH2]:1.0, [CH2]:2.0, [CH]:2.0, [CH]CS:2.0, [C]C(N)COC(=O)C>>O=C(O)C:1.0, [C]C(N)CS:1.0, [C]C([CH2])N:2.0, [C]C([CH2])N>>[C]C([CH2])N:1.0, [C]O:1.0, [C]OCC(N)C(=O)O>>O=C(O)C(N)CS:1.0, [C]OCC([C])N:1.0, [C]OCC([C])N>>[C]C(N)CS:1.0, [C]O[CH2]:1.0, [C]O[CH2]>>[C]O:1.0, [OH]:1.0, [O]:1.0, [O]CC(N)C(=O)O:1.0, [O]CC(N)C(=O)O>>O=C(O)C(N)CS:1.0, [O]C[CH]:1.0, [O]C[CH]>>[CH]CS:1.0, [SH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[S:1.0, [CH2]:2.0, [O]:1.0, [SH]:1.0]


ID=Reaction Center at Level: 1 (5)
[S:1.0, S[CH2]:1.0, [CH]CS:1.0, [C]O[CH2]:1.0, [O]C[CH]:1.0]


ID=Reaction Center at Level: 2 (5)
[O=C(OC[CH])C:1.0, S:1.0, [CH]CS:1.0, [C]C(N)CS:1.0, [C]OCC([C])N:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (1)
[[C]C([CH2])N:2.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C(N)CS:1.0, [O]CC(N)C(=O)O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C([CH2])N>>[C]C([CH2])N
2: [O]C[CH]>>[CH]CS
3: [C]O[CH2]>>[C]O
4: S>>S[CH2]

MMP Level 2
1: [O]CC(N)C(=O)O>>O=C(O)C(N)CS
2: [C]OCC([C])N>>[C]C(N)CS
3: O=C(OC[CH])C>>O=C(O)C
4: S>>[CH]CS

MMP Level 3
1: [C]OCC(N)C(=O)O>>O=C(O)C(N)CS
2: O=C(O)C(N)COC(=O)C>>O=C(O)C(N)CS
3: [C]C(N)COC(=O)C>>O=C(O)C
4: S>>[C]C(N)CS


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)COC(=O)C>>O=C(O)C(N)CS, O=C(O)C(N)COC(=O)C>>O=C(O)C(N)CS]
2: R:M00001, P:M00004	[O=C(O)C(N)COC(=O)C>>O=C(O)C]
3: R:M00002, P:M00003	[S>>O=C(O)C(N)CS]


//
SELECTED AAM MAPPING
[O:9]=[C:8]([O:7][CH2:6][CH:4]([NH2:5])[C:2](=[O:1])[OH:3])[CH3:10].[SH2:11]>>[O:9]=[C:8]([OH:7])[CH3:10].[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][SH:11]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=10, 2=8, 3=9, 4=7, 5=6, 6=4, 7=2, 8=1, 9=3, 10=5, 11=11}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=4, 3=2, 4=1, 5=3, 6=5, 7=7, 8=11, 9=9, 10=8, 11=10}

