
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C=O:1.0]

ORDER_CHANGED
[O-O*O=O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=CC[CH2]:1.0, O=C[CH2]:2.0, O=O:4.0, O=O>>OO:6.0, O=[CH]:1.0, O>>O=CC[CH2]:1.0, O>>O=C[CH2]:1.0, O>>O=[CH]:1.0, OO:4.0, [CH2]:1.0, [CH2]CCNCC[CH2]>>[CH2]CCN:1.0, [CH2]CN:1.0, [CH2]CNCCCN>>O=CCCN:1.0, [CH2]CNC[CH2]:1.0, [CH2]CNC[CH2]>>[CH2]CN:1.0, [CH2]N:1.0, [CH2]NCC[CH2]:1.0, [CH2]NCC[CH2]>>O=CC[CH2]:1.0, [CH2]N[CH2]:1.0, [CH2]N[CH2]>>[CH2]N:1.0, [CH]:1.0, [NH2]:1.0, [NH]:1.0, [NH]C[CH2]:1.0, [NH]C[CH2]>>O=C[CH2]:1.0, [OH]:2.0, [O]:3.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[O:1.0, [CH2]:1.0, [CH]:1.0, [NH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=C[CH2]:1.0, O=[CH]:1.0, [CH2]N[CH2]:1.0, [NH]C[CH2]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=CC[CH2]:1.0, O=C[CH2]:1.0, [CH2]CNC[CH2]:1.0, [CH2]NCC[CH2]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (2)
[[OH]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (2)
[O=O:2.0, OO:2.0]


ID=Reaction Center at Level: 2 (2)
[O=O:2.0, OO:2.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [NH]C[CH2]>>O=C[CH2]
2: [CH2]N[CH2]>>[CH2]N
3: O=O>>OO
4: O>>O=[CH]

MMP Level 2
1: [CH2]NCC[CH2]>>O=CC[CH2]
2: [CH2]CNC[CH2]>>[CH2]CN
3: O=O>>OO
4: O>>O=C[CH2]

MMP Level 3
1: [CH2]CNCCCN>>O=CCCN
2: [CH2]CCNCC[CH2]>>[CH2]CCN
3: O=O>>OO
4: O>>O=CC[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[NCCCNCCCCNCCCN>>NCCCNCCCCN]
2: R:M00001, P:M00005	[NCCCNCCCCNCCCN>>O=CCCN]
3: R:M00002, P:M00006	[O=O>>OO]
4: R:M00003, P:M00005	[O>>O=CCCN]


//
SELECTED AAM MAPPING
[O:15]=[O:16].[OH2:17].[NH2:1][CH2:2][CH2:3][CH2:4][NH:5][CH2:6][CH2:7][CH2:8][CH2:9][NH:10][CH2:11][CH2:12][CH2:13][NH2:14]>>[O:17]=[CH:11][CH2:12][CH2:13][NH2:14].[OH:15][OH:16].[NH2:1][CH2:2][CH2:3][CH2:4][NH:5][CH2:6][CH2:7][CH2:8][CH2:9][NH2:10]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=8, 2=7, 3=6, 4=5, 5=4, 6=3, 7=2, 8=1, 9=9, 10=10, 11=11, 12=12, 13=13, 14=14, 15=15, 16=16, 17=17}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=8, 2=7, 3=6, 4=5, 5=4, 6=3, 7=2, 8=1, 9=9, 10=10, 11=13, 12=14, 13=15, 14=12, 15=11, 16=16, 17=17}

