
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:2.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O:3.0, O>>[CH]C(O)C(O)O[CH2]:1.0, O>>[CH]O:1.0, O>>[O]C([CH])O:1.0, [CH]:4.0, [CH]C(O)C(O)C(O)[CH2]:1.0, [CH]C(O)C(O)O[CH2]:1.0, [CH]C(O)[CH2]:1.0, [CH]C(OC1OC[CH]C(O)C1O)[CH2]>>OC1OC[CH]C(O)C1O:1.0, [CH]C([CH])O:2.0, [CH]C([CH])O>>[CH]C([CH])O:1.0, [CH]O:2.0, [CH]OC(O[CH2])C([CH])O:1.0, [CH]OC(O[CH2])C([CH])O>>[CH]C(O)C(O)O[CH2]:1.0, [CH]OC1COC(O)C(O)C1O>>OC1OCC(O)C(O)C1O:1.0, [CH]O[CH]:1.0, [CH]O[CH]>>[CH]O:1.0, [OH]:2.0, [O]:1.0, [O]C([CH2])C(O)C([CH])O:1.0, [O]C([CH2])C(O)C([CH])O>>[CH]C(O)C(O)C(O)[CH2]:1.0, [O]C([CH])O:2.0, [O]C([CH])OC([CH])[CH2]:1.0, [O]C([CH])OC([CH])[CH2]>>[CH]C(O)[CH2]:1.0, [O]C([O])[CH]:1.0, [O]C([O])[CH]>>[O]C([CH])O:1.0, [O]CC(OC(O[CH2])C([CH])O)C([CH])O>>[O]CC(O)C([CH])O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [CH]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, [CH]O:1.0, [CH]O[CH]:1.0, [O]C([CH])O:1.0, [O]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, [CH]C(O)C(O)O[CH2]:1.0, [CH]OC(O[CH2])C([CH])O:1.0, [O]C([CH])O:1.0, [O]C([CH])OC([CH])[CH2]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:4.0]


ID=Reaction Center at Level: 1 (3)
[[CH]C([CH])O:2.0, [O]C([CH])O:1.0, [O]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[CH]C(O)C(O)C(O)[CH2]:1.0, [CH]C(O)C(O)O[CH2]:1.0, [CH]OC(O[CH2])C([CH])O:1.0, [O]C([CH2])C(O)C([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [O]C([O])[CH]>>[O]C([CH])O
2: O>>[CH]O
3: [CH]C([CH])O>>[CH]C([CH])O
4: [CH]O[CH]>>[CH]O

MMP Level 2
1: [CH]OC(O[CH2])C([CH])O>>[CH]C(O)C(O)O[CH2]
2: O>>[O]C([CH])O
3: [O]C([CH2])C(O)C([CH])O>>[CH]C(O)C(O)C(O)[CH2]
4: [O]C([CH])OC([CH])[CH2]>>[CH]C(O)[CH2]

MMP Level 3
1: [CH]C(OC1OC[CH]C(O)C1O)[CH2]>>OC1OC[CH]C(O)C1O
2: O>>[CH]C(O)C(O)O[CH2]
3: [CH]OC1COC(O)C(O)C1O>>OC1OCC(O)C(O)C1O
4: [O]CC(OC(O[CH2])C([CH])O)C([CH])O>>[O]CC(O)C([CH])O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[OC1OCC(OC2OCC(O)C(O)C2O)C(O)C1O>>OC1OCC(O)C(O)C1O, OC1OCC(OC2OCC(O)C(O)C2O)C(O)C1O>>OC1OCC(O)C(O)C1O]
2: R:M00001, P:M00004	[OC1OCC(OC2OCC(O)C(O)C2O)C(O)C1O>>OC1OCC(O)C(O)C1O]
3: R:M00002, P:M00004	[O>>OC1OCC(O)C(O)C1O]


//
SELECTED AAM MAPPING
[OH2:20].[OH:1][CH:2]1[O:3][CH2:4][CH:5]([O:6][CH:7]2[O:8][CH2:9][CH:10]([OH:11])[CH:12]([OH:13])[CH:14]2[OH:15])[CH:16]([OH:17])[CH:18]1[OH:19]>>[OH:1][CH:2]1[O:3][CH2:4][CH:5]([OH:6])[CH:16]([OH:17])[CH:18]1[OH:19].[OH:20][CH:7]1[O:8][CH2:9][CH:10]([OH:11])[CH:12]([OH:13])[CH:14]1[OH:15]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=10, 3=12, 4=14, 5=7, 6=8, 7=6, 8=5, 9=4, 10=3, 11=2, 12=18, 13=16, 14=17, 15=19, 16=1, 17=15, 18=13, 19=11, 20=20}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=4, 2=5, 3=7, 4=9, 5=2, 6=3, 7=1, 8=10, 9=8, 10=6, 11=14, 12=15, 13=17, 14=19, 15=12, 16=13, 17=11, 18=20, 19=18, 20=16}

