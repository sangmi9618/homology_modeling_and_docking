
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0]

ORDER_CHANGED
[C-C*C=C:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)C(OP(=O)(O)O)CO>>O=C(O)C(OP(=O)(O)O)=C:1.0, O=C(O)C(O[P])=C:1.0, O=C(O)C(O[P])CO:1.0, O=C(O)C(O[P])CO>>O=C(O)C(O[P])=C:2.0, O[CH2]:1.0, O[CH2]>>O:1.0, [CH2]:2.0, [CH]:1.0, [CH]CO:2.0, [CH]CO>>O:1.0, [CH]CO>>[C]=C:1.0, [C]:1.0, [C]=C:1.0, [C]C([O])=C:2.0, [C]C([O])CO:1.0, [C]C([O])CO>>O:1.0, [C]C([O])CO>>[C]C([O])=C:1.0, [C]C([O])[CH2]:1.0, [C]C([O])[CH2]>>[C]C([O])=C:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[O:1.0, [CH2]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (3)
[O:1.0, O[CH2]:1.0, [CH]CO:1.0]


ID=Reaction Center at Level: 2 (3)
[O:1.0, [CH]CO:1.0, [C]C([O])CO:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[CH2]:2.0, [CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (4)
[[CH]CO:1.0, [C]=C:1.0, [C]C([O])=C:1.0, [C]C([O])[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(O)C(O[P])=C:1.0, O=C(O)C(O[P])CO:1.0, [C]C([O])=C:1.0, [C]C([O])CO:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]C([O])=C:1.0, [C]C([O])[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C(O[P])=C:1.0, O=C(O)C(O[P])CO:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]CO>>[C]=C
2: [C]C([O])[CH2]>>[C]C([O])=C
3: O[CH2]>>O

MMP Level 2
1: [C]C([O])CO>>[C]C([O])=C
2: O=C(O)C(O[P])CO>>O=C(O)C(O[P])=C
3: [CH]CO>>O

MMP Level 3
1: O=C(O)C(O[P])CO>>O=C(O)C(O[P])=C
2: O=C(O)C(OP(=O)(O)O)CO>>O=C(O)C(OP(=O)(O)O)=C
3: [C]C([O])CO>>O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)C(OP(=O)(O)O)CO>>O=C(O)C(OP(=O)(O)O)=C, O=C(O)C(OP(=O)(O)O)CO>>O=C(O)C(OP(=O)(O)O)=C]
2: R:M00001, P:M00003	[O=C(O)C(OP(=O)(O)O)CO>>O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([O:5][P:6](=[O:7])([OH:8])[OH:9])[CH2:10][OH:11]>>[O:1]=[C:2]([OH:3])[C:4]([O:5][P:6](=[O:7])([OH:8])[OH:9])=[CH2:10].[OH2:11]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=10, 2=4, 3=2, 4=1, 5=3, 6=5, 7=6, 8=7, 9=8, 10=9, 11=11}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=10, 2=4, 3=2, 4=1, 5=3, 6=5, 7=6, 8=7, 9=8, 10=9, 11=11}

