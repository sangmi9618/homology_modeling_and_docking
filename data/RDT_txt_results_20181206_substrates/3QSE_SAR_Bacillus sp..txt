
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C=O:1.0]

ORDER_CHANGED
[O-O*O=O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C:4.0, O=C(O)CNC>>O=C(O)CN:1.0, O=O:4.0, O=O>>OO:6.0, O>>O=C:3.0, OO:4.0, [CH2]:1.0, [CH2]N:1.0, [CH2]NC:2.0, [CH2]NC>>O=C:1.0, [CH2]NC>>[CH2]N:1.0, [CH3]:1.0, [C]CN:1.0, [C]CNC:1.0, [C]CNC>>O=C:1.0, [C]CNC>>[C]CN:1.0, [NH2]:1.0, [NH]:1.0, [NH]C:1.0, [NH]C>>O=C:1.0, [OH]:2.0, [O]:3.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[O:1.0, [CH2]:1.0, [CH3]:1.0, [NH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O:1.0, O=C:2.0, [CH2]NC:1.0, [NH]C:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=C:2.0, [CH2]NC:1.0, [C]CNC:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (2)
[[OH]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (2)
[O=O:2.0, OO:2.0]


ID=Reaction Center at Level: 2 (2)
[O=O:2.0, OO:2.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=O>>OO
2: [CH2]NC>>[CH2]N
3: O>>O=C
4: [NH]C>>O=C

MMP Level 2
1: O=O>>OO
2: [C]CNC>>[C]CN
3: O>>O=C
4: [CH2]NC>>O=C

MMP Level 3
1: O=O>>OO
2: O=C(O)CNC>>O=C(O)CN
3: O>>O=C
4: [C]CNC>>O=C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C(O)CNC>>O=C(O)CN]
2: R:M00001, P:M00005	[O=C(O)CNC>>O=C]
3: R:M00002, P:M00005	[O>>O=C]
4: R:M00003, P:M00006	[O=O>>OO]


//
SELECTED AAM MAPPING
[O:7]=[O:8].[O:1]=[C:2]([OH:3])[CH2:4][NH:5][CH3:6].[OH2:9]>>[O:9]=[CH2:6].[O:1]=[C:2]([OH:3])[CH2:4][NH2:5].[OH:7][OH:8]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=5, 3=4, 4=2, 5=1, 6=3, 7=9, 8=7, 9=8}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=4, 2=2, 3=1, 4=3, 5=5, 6=9, 7=8, 8=6, 9=7}

