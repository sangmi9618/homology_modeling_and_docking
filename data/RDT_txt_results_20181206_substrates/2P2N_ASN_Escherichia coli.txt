
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[N:3.0, O:3.0, O=C(N)C[CH]:1.0, O=C(N)C[CH]>>N:1.0, O=C(N)C[CH]>>O=C(O)C[CH]:1.0, O=C(O)C[CH]:1.0, O=C(O)[CH2]:2.0, O=C([CH2])N:2.0, O=C([CH2])N>>N:1.0, O=C([CH2])N>>O=C(O)[CH2]:1.0, O>>O=C(O)C[CH]:1.0, O>>O=C(O)[CH2]:1.0, O>>[C]O:1.0, [C]:2.0, [C]C(N)CC(=O)N>>[C]C(N)CC(=O)O:1.0, [C]N:1.0, [C]N>>N:1.0, [C]O:1.0, [NH2]:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[N:1.0, O:1.0, [C]:2.0, [NH2]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (6)
[N:1.0, O:1.0, O=C(O)[CH2]:1.0, O=C([CH2])N:1.0, [C]N:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (6)
[N:1.0, O:1.0, O=C(N)C[CH]:1.0, O=C(O)C[CH]:1.0, O=C(O)[CH2]:1.0, O=C([CH2])N:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=C([CH2])N>>O=C(O)[CH2]
2: [C]N>>N
3: O>>[C]O

MMP Level 2
1: O=C(N)C[CH]>>O=C(O)C[CH]
2: O=C([CH2])N>>N
3: O>>O=C(O)[CH2]

MMP Level 3
1: [C]C(N)CC(=O)N>>[C]C(N)CC(=O)O
2: O=C(N)C[CH]>>N
3: O>>O=C(O)C[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)CC(=O)N>>O=C(O)CC(N)C(=O)O]
2: R:M00001, P:M00004	[O=C(O)C(N)CC(=O)N>>N]
3: R:M00002, P:M00003	[O>>O=C(O)CC(N)C(=O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][C:7](=[O:8])[NH2:9].[OH2:10]>>[O:8]=[C:7]([OH:10])[CH2:6][CH:4]([NH2:5])[C:2](=[O:1])[OH:3].[NH3:9]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=4, 3=2, 4=1, 5=3, 6=5, 7=7, 8=8, 9=9, 10=10}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=4, 2=5, 3=7, 4=8, 5=9, 6=6, 7=2, 8=1, 9=3, 10=10}

