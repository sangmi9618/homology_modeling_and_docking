
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)O[P]>>O=P(O)(O[P])OP(=O)(O)O[CH2]:1.0, O=P(O)(O[CH])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH]:1.0, O=P(O)(O[P])O[CH2]:2.0, O=P(O)(O[P])O[CH2]>>O=P(O)(O[P])O[CH2]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>[O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP(=O)(O)OC[CH]>>[O]P(=O)(O)OP(=O)(O)OC[CH]:1.0, [O]P(=O)(O)OP([O])(=O)O:2.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:2.0, [O]P([O])(=O)O>>[O]P([O])(=O)O:1.0, [P]:2.0, [P]O:2.0, [P]O>>[P]O[P]:1.0, [P]O[P]:2.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]P([O])(=O)O:2.0, [P]O[P]:2.0]


ID=Reaction Center at Level: 2 (2)
[O=P(O)(O[P])O[CH2]:2.0, [O]P(=O)(O)OP([O])(=O)O:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[P]>>[P]O
2: [P]O>>[P]O[P]
3: [O]P([O])(=O)O>>[O]P([O])(=O)O

MMP Level 2
1: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
2: [O]P(=O)(O)O>>[O]P(=O)(O)OP([O])(=O)O
3: O=P(O)(O[P])O[CH2]>>O=P(O)(O[P])O[CH2]

MMP Level 3
1: O=P(O)(O[CH])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH]
2: O=P(O)(O)O[P]>>O=P(O)(O[P])OP(=O)(O)O[CH2]
3: [O]P(=O)(O)OP(=O)(O)OC[CH]>>[O]P(=O)(O)OP(=O)(O)OC[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3NC(=O)C)C(O)C2O>>O=C(NC1C(O)C(O)C(OC1OP(=O)(O)O)CO)C]
2: R:M00001, P:M00004	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3NC(=O)C)C(O)C2O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2O]
3: R:M00002, P:M00004	[O=P(O)(O)OP(=O)(O)O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][CH:22]3[O:23][CH:24]([CH2:25][OH:26])[CH:27]([OH:28])[CH:29]([OH:30])[CH:31]3[NH:32][C:33](=[O:34])[CH3:35])[CH:36]([OH:37])[CH:38]2[OH:39].[O:40]=[P:41]([OH:42])([OH:43])[O:44][P:45](=[O:46])([OH:47])[OH:48]>>[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:42][P:41](=[O:40])([OH:43])[O:44][P:45](=[O:46])([OH:47])[OH:48])[CH:36]([OH:37])[CH:38]2[OH:39].[O:34]=[C:33]([NH:32][CH:31]1[CH:29]([OH:30])[CH:27]([OH:28])[CH:24]([O:23][CH:22]1[O:21][P:18](=[O:19])([OH:20])[OH:17])[CH2:25][OH:26])[CH3:35]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=35, 2=33, 3=34, 4=32, 5=31, 6=29, 7=27, 8=24, 9=23, 10=22, 11=21, 12=18, 13=19, 14=20, 15=17, 16=14, 17=15, 18=16, 19=13, 20=12, 21=11, 22=36, 23=38, 24=9, 25=10, 26=5, 27=4, 28=3, 29=2, 30=1, 31=8, 32=6, 33=7, 34=39, 35=37, 36=25, 37=26, 38=28, 39=30, 40=42, 41=41, 42=40, 43=43, 44=44, 45=45, 46=46, 47=47, 48=48}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=48, 2=31, 3=30, 4=32, 5=33, 6=34, 7=36, 8=38, 9=39, 10=40, 11=41, 12=42, 13=43, 14=44, 15=45, 16=46, 17=47, 18=37, 19=35, 20=3, 21=4, 22=5, 23=6, 24=7, 25=8, 26=2, 27=1, 28=9, 29=28, 30=26, 31=11, 32=10, 33=12, 34=13, 35=14, 36=15, 37=16, 38=17, 39=18, 40=19, 41=20, 42=21, 43=22, 44=23, 45=24, 46=25, 47=27, 48=29}

