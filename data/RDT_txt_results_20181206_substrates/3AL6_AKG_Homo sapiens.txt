
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0, C-O:1.0, O=O:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

//
FINGERPRINTS RC
[O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(=O)C[CH2]>>O=C(O)C[CH2]:1.0, O=C(O)C(=O)C[CH2]>>O=C=O:1.0, O=C(O)C(=O)[CH2]:1.0, O=C(O)C(=O)[CH2]>>O=C=O:2.0, O=C(O)C[CH2]:1.0, O=C(O)[CH2]:2.0, O=C=O:3.0, O=O:4.0, O=O>>O=C(O)C[CH2]:1.0, O=O>>O=C(O)[CH2]:1.0, O=O>>[C]O:1.0, [C]:4.0, [C]=O:1.0, [C]C(=O)O:2.0, [C]C(=O)O>>O=C=O:2.0, [C]C(=O)[CH2]:1.0, [C]C(=O)[CH2]>>O=C(O)[CH2]:1.0, [C]CCC(=O)C(=O)O>>[C]CCC(=O)O:1.0, [C]O:2.0, [C]O>>[C]=O:1.0, [OH]:2.0, [O]:3.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[C]:3.0, [OH]:1.0, [O]:2.0]


ID=Reaction Center at Level: 1 (5)
[O=C(O)[CH2]:1.0, O=O:2.0, [C]C(=O)O:1.0, [C]C(=O)[CH2]:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (5)
[O=C(O)C(=O)C[CH2]:1.0, O=C(O)C(=O)[CH2]:1.0, O=C(O)C[CH2]:1.0, O=C(O)[CH2]:1.0, O=O:2.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C=O:1.0, [C]=O:1.0, [C]C(=O)O:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (3)
[O=C(O)C(=O)[CH2]:1.0, O=C=O:2.0, [C]C(=O)O:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]O>>[C]=O
2: O=O>>[C]O
3: [C]C(=O)O>>O=C=O
4: [C]C(=O)[CH2]>>O=C(O)[CH2]

MMP Level 2
1: [C]C(=O)O>>O=C=O
2: O=O>>O=C(O)[CH2]
3: O=C(O)C(=O)[CH2]>>O=C=O
4: O=C(O)C(=O)C[CH2]>>O=C(O)C[CH2]

MMP Level 3
1: O=C(O)C(=O)[CH2]>>O=C=O
2: O=O>>O=C(O)C[CH2]
3: O=C(O)C(=O)C[CH2]>>O=C=O
4: [C]CCC(=O)C(=O)O>>[C]CCC(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=O>>O=C(O)CCC(=O)O]
2: R:M00002, P:M00003	[O=C(O)C(=O)CCC(=O)O>>O=C=O, O=C(O)C(=O)CCC(=O)O>>O=C=O]
3: R:M00002, P:M00004	[O=C(O)C(=O)CCC(=O)O>>O=C(O)CCC(=O)O]


//
SELECTED AAM MAPPING
[O:11]=[O:12].[O:1]=[C:2]([OH:3])[C:4](=[O:5])[CH2:6][CH2:7][C:8](=[O:9])[OH:10]>>[O:1]=[C:2]=[O:3].[O:5]=[C:4]([OH:11])[CH2:6][CH2:7][C:8](=[O:9])[OH:10]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=11, 2=12, 3=6, 4=7, 5=8, 6=9, 7=10, 8=4, 9=5, 10=2, 11=1, 12=3}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=10, 2=9, 3=11, 4=5, 5=4, 6=2, 7=1, 8=3, 9=6, 10=7, 11=8}

