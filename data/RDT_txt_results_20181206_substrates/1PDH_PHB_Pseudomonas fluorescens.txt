
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0, O=O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1:1.0, O=O:4.0, O=O>>O:3.0, O=O>>[C]=C([CH])O:1.0, O=O>>[C]=CC(O)=C([CH])O:1.0, O=O>>[C]O:1.0, [CH2]:1.0, [CH]:2.0, [CH]N([CH])[CH]:1.0, [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]:1.0, [CH][N+]([CH])=[CH]:1.0, [C]:1.0, [C]=C([CH])O:2.0, [C]=CC(O)=C([CH])O:1.0, [C]=CC=C([CH])O:1.0, [C]=CC=C([CH])O>>[C]=CC(O)=C([CH])O:1.0, [C]=CN(C=[CH])C([O])[CH]:1.0, [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]=C[CH]:1.0, [C]=C[CH]>>[C]=C([CH])O:1.0, [C]=C[N+](=C[CH])C([O])[CH]:1.0, [C]C(=[CH])C=C[CH]:1.0, [C]C(=[CH])CC=[CH]:1.0, [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]:1.0, [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O:1.0, [C]C=[CH]:1.0, [C]C[CH]:1.0, [C]C[CH]>>[C]C=[CH]:1.0, [C]O:1.0, [C]c1ccc(O)cc1>>[C]c1ccc(O)c(O)c1:1.0, [N+]:1.0, [N]:1.0, [OH]:1.0, [O]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [C]:1.0, [OH]:1.0, [O]:2.0]


ID=Reaction Center at Level: 1 (4)
[O:1.0, O=O:2.0, [C]=C([CH])O:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=O:2.0, [C]=C([CH])O:1.0, [C]=CC(O)=C([CH])O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C[CH]>>[C]C=[CH]
2: O=O>>O
3: [CH]N([CH])[CH]>>[CH][N+]([CH])=[CH]
4: [C]=C[CH]>>[C]=C([CH])O
5: O=O>>[C]O

MMP Level 2
1: [C]C(=[CH])CC=[CH]>>[C]C(=[CH])C=C[CH]
2: O=O>>O
3: [C]=CN(C=[CH])C([O])[CH]>>[C]=C[N+](=C[CH])C([O])[CH]
4: [C]=CC=C([CH])O>>[C]=CC(O)=C([CH])O
5: O=O>>[C]=C([CH])O

MMP Level 3
1: O=C(N)C1=C[N]C=CC1>>O=C(N)C=1C=[N+]C=CC1
2: O=O>>O
3: [C]C1=CN(C=CC1)C2O[CH][CH]C2O>>[C]c1ccc[n+](c1)C2O[CH][CH]C2O
4: [C]c1ccc(O)cc1>>[C]c1ccc(O)c(O)c1
5: O=O>>[C]=CC(O)=C([CH])O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00005	[O=C(O)c1ccc(O)cc1>>O=C(O)c1ccc(O)c(O)c1]
2: R:M00002, P:M00006	[O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O, O=C(N)C1=CN(C=CC1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O>>O=C(N)c1ccc[n+](c1)C2OC(COP(=O)(O)OP(=O)(O)OCC3OC(n4cnc5c(ncnc54)N)C(OP(=O)(O)O)C3O)C(O)C2O]
3: R:M00004, P:M00005	[O=O>>O=C(O)c1ccc(O)c(O)c1]
4: R:M00004, P:M00007	[O=O>>O]


//
SELECTED AAM MAPPING
[H+:61].[O:59]=[O:60].[O:49]=[C:50]([OH:51])[C:52]:1:[CH:58]:[CH:57]:[C:55]([OH:56]):[CH:54]:[CH:53]1.[O:1]=[C:2]([NH2:3])[C:4]1=[CH:5][N:6]([CH:7]=[CH:8][CH2:9]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48]>>[O:49]=[C:50]([OH:51])[C:52]:1:[CH:53]:[CH:54]:[C:55]([OH:56]):[C:57]([OH:59]):[CH:58]1.[O:1]=[C:2]([NH2:3])[C:4]:1:[CH:9]:[CH:8]:[CH:7]:[N+:6](:[CH:5]1)[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]3[O:25][CH:26]([N:27]:4:[CH:28]:[N:29]:[C:30]:5:[C:31](:[N:32]:[CH:33]:[N:34]:[C:35]54)[NH2:36])[CH:37]([O:38][P:39](=[O:40])([OH:41])[OH:42])[CH:43]3[OH:44])[CH:45]([OH:46])[CH:47]2[OH:48].[OH2:60]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=53, 2=54, 3=55, 4=57, 5=58, 6=52, 7=50, 8=49, 9=51, 10=56, 11=9, 12=8, 13=7, 14=6, 15=5, 16=4, 17=2, 18=1, 19=3, 20=10, 21=47, 22=45, 23=12, 24=11, 25=13, 26=14, 27=15, 28=16, 29=17, 30=18, 31=19, 32=20, 33=21, 34=22, 35=23, 36=24, 37=43, 38=37, 39=26, 40=25, 41=27, 42=28, 43=29, 44=30, 45=35, 46=34, 47=33, 48=32, 49=31, 50=36, 51=38, 52=39, 53=40, 54=41, 55=42, 56=44, 57=46, 58=48, 59=61, 60=59, 61=60}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=53, 2=54, 3=55, 4=57, 5=59, 6=52, 7=50, 8=49, 9=51, 10=58, 11=56, 12=6, 13=5, 14=4, 15=9, 16=8, 17=7, 18=10, 19=47, 20=45, 21=12, 22=11, 23=13, 24=14, 25=15, 26=16, 27=17, 28=18, 29=19, 30=20, 31=21, 32=22, 33=23, 34=24, 35=43, 36=37, 37=26, 38=25, 39=27, 40=28, 41=29, 42=30, 43=35, 44=34, 45=33, 46=32, 47=31, 48=36, 49=38, 50=39, 51=40, 52=41, 53=42, 54=44, 55=46, 56=48, 57=2, 58=1, 59=3, 60=60}

