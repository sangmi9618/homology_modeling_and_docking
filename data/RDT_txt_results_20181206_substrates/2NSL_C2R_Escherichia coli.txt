
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0, C-N:1.0]

//
FINGERPRINTS RC
[O=C(O)C=1N=C[N]C1N:1.0, O=C(O)C=1N=C[N]C1N>>[NH]C=1[N]C=NC1:1.0, O=C(O)C=1N=C[N]C1N>>[N]C(=[CH])NC(=O)O:1.0, O=C(O)c1ncn([CH])c1N>>[C]Nc1cncn1[CH]:1.0, O=C([NH])O:1.0, [CH]:1.0, [C]:3.0, [C]=C([N])C(=O)O:1.0, [C]=C([N])C(=O)O>>[C]NC(=O)O:1.0, [C]=C([N])N:1.0, [C]=C([N])N>>[N]C(=[CH])NC(=O)O:1.0, [C]=C[N]:1.0, [C]C(=O)O:1.0, [C]C(=O)O>>O=C([NH])O:1.0, [C]C(=[C])[N]:1.0, [C]C(=[C])[N]>>[C]=C[N]:1.0, [C]N:1.0, [C]N>>[C]N[C]:1.0, [C]NC(=O)O:1.0, [C]N[C]:1.0, [C]c1ncn([CH])c1N>>O=C(O)Nc1cncn1[CH]:1.0, [NH2]:1.0, [NH]:1.0, [NH]C=1[N]C=NC1:1.0, [N]C(=[CH])NC(=O)O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[C]:3.0, [NH]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([NH])O:1.0, [C]C(=O)O:1.0, [C]C(=[C])[N]:1.0, [C]N[C]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(O)C=1N=C[N]C1N:1.0, [C]=C([N])C(=O)O:1.0, [C]NC(=O)O:1.0, [N]C(=[CH])NC(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C(=[C])[N]>>[C]=C[N]
2: [C]C(=O)O>>O=C([NH])O
3: [C]N>>[C]N[C]

MMP Level 2
1: O=C(O)C=1N=C[N]C1N>>[NH]C=1[N]C=NC1
2: [C]=C([N])C(=O)O>>[C]NC(=O)O
3: [C]=C([N])N>>[N]C(=[CH])NC(=O)O

MMP Level 3
1: O=C(O)c1ncn([CH])c1N>>[C]Nc1cncn1[CH]
2: O=C(O)C=1N=C[N]C1N>>[N]C(=[CH])NC(=O)O
3: [C]c1ncn([CH])c1N>>O=C(O)Nc1cncn1[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)c1ncn(c1N)C2OC(COP(=O)(O)O)C(O)C2O>>O=C(O)Nc1cncn1C2OC(COP(=O)(O)O)C(O)C2O, O=C(O)c1ncn(c1N)C2OC(COP(=O)(O)O)C(O)C2O>>O=C(O)Nc1cncn1C2OC(COP(=O)(O)O)C(O)C2O, O=C(O)c1ncn(c1N)C2OC(COP(=O)(O)O)C(O)C2O>>O=C(O)Nc1cncn1C2OC(COP(=O)(O)O)C(O)C2O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[C:4]:1:[N:5]:[CH:6]:[N:7](:[C:8]1[NH2:9])[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[OH:18])[CH:19]([OH:20])[CH:21]2[OH:22]>>[O:1]=[C:2]([OH:3])[NH:9][C:8]:1:[CH:4]:[N:5]:[CH:6]:[N:7]1[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[OH:18])[CH:19]([OH:20])[CH:21]2[OH:22]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=5, 3=4, 4=8, 5=7, 6=10, 7=21, 8=19, 9=12, 10=11, 11=13, 12=14, 13=15, 14=16, 15=17, 16=18, 17=20, 18=22, 19=9, 20=2, 21=1, 22=3}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=5, 3=9, 4=8, 5=7, 6=10, 7=21, 8=19, 9=12, 10=11, 11=13, 12=14, 13=15, 14=16, 15=17, 16=18, 17=20, 18=22, 19=4, 20=2, 21=1, 22=3}

