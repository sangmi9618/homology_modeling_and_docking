
//
FINGERPRINTS BC

FORMED_CLEAVED
[O%P:2.0, O-P:2.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O=P(O)(O[CH])O[CH2]:2.0, O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[CH2]:2.0, O=P(O)(O[P])O[CH2]>>O=P(O)(O[CH])O[CH2]:2.0, OC1[CH]OC([CH2])C1O:2.0, OC1[CH]OC([CH2])C1O>>[O]C1[CH]OC([CH2])C1O:2.0, [CH]:4.0, [CH]C([CH])O:6.0, [CH]C([CH])O>>[CH]C([CH])O:2.0, [CH]C([CH])O>>[O]P(=O)(O)OC([CH])[CH]:2.0, [CH]O:2.0, [CH]O>>[P]O[CH]:2.0, [N]C1OC(C[O])C(O)C1O>>[N]C1OC(C[O])C(O)C1O[P]:2.0, [N]C1O[CH]C(O)C1O>>[N]C1O[CH]C(O)C1OP(=O)(O)O[CH2]:2.0, [OH]:3.0, [O]:4.0, [O]C1[CH]OC([CH2])C1O:2.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OC([CH])[CH]:2.0, [O]P(=O)(O)OP(=O)(O)OC[CH]>>O=P(O)(OC[CH])OC([CH])[CH]:2.0, [O]P(=O)(O)OP([O])(=O)O:2.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:4.0, [O]P([O])(=O)O>>[O]P([O])(=O)O:2.0, [P]:4.0, [P]O:1.0, [P]O[CH]:2.0, [P]O[P]:2.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:4.0, [P]:4.0]


ID=Reaction Center at Level: 1 (3)
[[O]P([O])(=O)O:4.0, [P]O[CH]:2.0, [P]O[P]:2.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O[CH])O[CH2]:2.0, O=P(O)(O[P])O[CH2]:2.0, [O]P(=O)(O)OC([CH])[CH]:2.0, [O]P(=O)(O)OP([O])(=O)O:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:4.0]


ID=Reaction Center at Level: 1 (1)
[[CH]C([CH])O:4.0]


ID=Reaction Center at Level: 2 (2)
[OC1[CH]OC([CH2])C1O:2.0, [O]C1[CH]OC([CH2])C1O:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: [P]O[P]>>[P]O
2: [O]P([O])(=O)O>>[O]P([O])(=O)O
3: [O]P([O])(=O)O>>[O]P([O])(=O)O
4: [CH]C([CH])O>>[CH]C([CH])O
5: [CH]O>>[P]O[CH]
6: [CH]O>>[P]O[CH]
7: [CH]C([CH])O>>[CH]C([CH])O

MMP Level 2
1: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
2: O=P(O)(O[P])O[CH2]>>O=P(O)(O[CH])O[CH2]
3: O=P(O)(O[P])O[CH2]>>O=P(O)(O[CH])O[CH2]
4: OC1[CH]OC([CH2])C1O>>[O]C1[CH]OC([CH2])C1O
5: [CH]C([CH])O>>[O]P(=O)(O)OC([CH])[CH]
6: [CH]C([CH])O>>[O]P(=O)(O)OC([CH])[CH]
7: OC1[CH]OC([CH2])C1O>>[O]C1[CH]OC([CH2])C1O

MMP Level 3
1: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]
2: [O]P(=O)(O)OP(=O)(O)OC[CH]>>O=P(O)(OC[CH])OC([CH])[CH]
3: [O]P(=O)(O)OP(=O)(O)OC[CH]>>O=P(O)(OC[CH])OC([CH])[CH]
4: [N]C1OC(C[O])C(O)C1O>>[N]C1OC(C[O])C(O)C1O[P]
5: [N]C1O[CH]C(O)C1O>>[N]C1O[CH]C(O)C1OP(=O)(O)O[CH2]
6: [N]C1O[CH]C(O)C1O>>[N]C1O[CH]C(O)C1OP(=O)(O)O[CH2]
7: [N]C1OC(C[O])C(O)C1O>>[N]C1OC(C[O])C(O)C1O[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)O]
2: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=c1nc(N)[nH]c2c1ncn2C3OC4COP(=O)(O)OC5C(O)C(OC5n6cnc7c(ncnc76)N)COP(=O)(O)OC3C4O, O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=c1nc(N)[nH]c2c1ncn2C3OC4COP(=O)(O)OC5C(O)C(OC5n6cnc7c(ncnc76)N)COP(=O)(O)OC3C4O, O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=c1nc(N)[nH]c2c1ncn2C3OC4COP(=O)(O)OC5C(O)C(OC5n6cnc7c(ncnc76)N)COP(=O)(O)OC3C4O]
3: R:M00002, P:M00004	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C3O>>O=c1nc(N)[nH]c2c1ncn2C3OC4COP(=O)(O)OC5C(O)C(OC5n6cnc7c(ncnc76)N)COP(=O)(O)OC3C4O, O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C3O>>O=c1nc(N)[nH]c2c1ncn2C3OC4COP(=O)(O)OC5C(O)C(OC5n6cnc7c(ncnc76)N)COP(=O)(O)OC3C4O, O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C3O>>O=c1nc(N)[nH]c2c1ncn2C3OC4COP(=O)(O)OC5C(O)C(OC5n6cnc7c(ncnc76)N)COP(=O)(O)OC3C4O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[C:8]1:[N:9]:[CH:10]:[N:11]2[CH:12]3[O:13][CH:14]([CH2:15][O:16][P:17](=[O:18])([OH:19])[O:55][P:56](=[O:57])([OH:58])[O:59][P:60](=[O:61])([OH:62])[OH:63])[CH:20]([OH:21])[CH:22]3[OH:23].[O:24]=[P:25]([OH:26])([OH:27])[O:28][P:29](=[O:30])([OH:31])[O:32][P:33](=[O:34])([OH:35])[O:36][CH2:37][CH:38]1[O:39][CH:40]([N:41]:2:[CH:42]:[N:43]:[C:44]:3:[C:45](:[N:46]:[CH:47]:[N:48]:[C:49]32)[NH2:50])[CH:51]([OH:52])[CH:53]1[OH:54]>>[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[C:8]1:[N:9]:[CH:10]:[N:11]2[CH:12]3[O:13][CH:14]4[CH2:15][O:16][P:17](=[O:18])([OH:19])[O:52][CH:51]5[CH:53]([OH:54])[CH:38]([O:39][CH:40]5[N:41]:6:[CH:42]:[N:43]:[C:44]:7:[C:45](:[N:46]:[CH:47]:[N:48]:[C:49]76)[NH2:50])[CH2:37][O:36][P:33](=[O:34])([OH:35])[O:23][CH:22]3[CH:20]4[OH:21].[O:24]=[P:25]([OH:26])([OH:27])[O:28][P:29](=[O:30])([OH:31])[OH:32]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=56, 2=57, 3=58, 4=53, 5=54, 6=55, 7=59, 8=52, 9=51, 10=50, 11=49, 12=60, 13=62, 14=47, 15=48, 16=46, 17=45, 18=42, 19=43, 20=44, 21=41, 22=38, 23=39, 24=40, 25=37, 26=34, 27=33, 28=35, 29=36, 30=63, 31=61, 32=10, 33=9, 34=8, 35=7, 36=11, 37=12, 38=31, 39=29, 40=14, 41=13, 42=15, 43=16, 44=17, 45=18, 46=19, 47=20, 48=21, 49=22, 50=23, 51=24, 52=25, 53=26, 54=27, 55=28, 56=30, 57=32, 58=6, 59=4, 60=3, 61=2, 62=1, 63=5}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=48, 2=47, 3=46, 4=49, 5=50, 6=51, 7=52, 8=53, 9=54, 10=15, 11=14, 12=44, 13=43, 14=12, 15=13, 16=11, 17=10, 18=9, 19=8, 20=7, 21=6, 22=4, 23=3, 24=2, 25=1, 26=5, 27=42, 28=39, 29=40, 30=38, 31=37, 32=24, 33=22, 34=21, 35=26, 36=25, 37=27, 38=28, 39=29, 40=30, 41=35, 42=34, 43=33, 44=32, 45=31, 46=36, 47=20, 48=17, 49=18, 50=16, 51=19, 52=23, 53=41, 54=45}

