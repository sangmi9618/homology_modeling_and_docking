
//
FINGERPRINTS BC

ORDER_CHANGED
[C%C*C%C:2.0]

//
FINGERPRINTS RC
[O=C1CC(=C[CH2])C([CH])(C)CC1>>O=C1C=C(C[CH2])C([CH])(C)CC1:1.0, O=C1CC2=CC[CH]C([CH2])C2(C)CC1>>O=C1C=C2CC[CH]C([CH2])C2(C)CC1:1.0, [CH2]:2.0, [CH]:2.0, [C]:2.0, [C]=C[CH2]:1.0, [C]=C[CH2]>>[C]C[CH2]:1.0, [C]C(=[CH])CC(=O)[CH2]:1.0, [C]C(=[CH])CC(=O)[CH2]>>[C]C([CH2])=CC(=O)[CH2]:1.0, [C]C(=[CH])CC[CH]:1.0, [C]C(=[CH])[CH2]:2.0, [C]C(=[CH])[CH2]>>[C]C(=[CH])[CH2]:1.0, [C]C([CH2])=CC(=O)[CH2]:1.0, [C]C([CH2])=CC[CH]:1.0, [C]C([CH2])=CC[CH]>>[C]C(=[CH])CC[CH]:1.0, [C]C=C(C[CH2])C([CH])([CH2])C:1.0, [C]C=[C]:1.0, [C]CC(=C[CH2])C([CH])([CH2])C:1.0, [C]CC(=C[CH2])C([CH])([CH2])C>>[C]C=C(C[CH2])C([CH])([CH2])C:1.0, [C]CC1=CCC([CH])[CH]C1([CH2])C>>[C]C=C1CCC([CH])[CH]C1([CH2])C:1.0, [C]C[CH2]:1.0, [C]C[C]:1.0, [C]C[C]>>[C]C=[C]:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[CH2]:2.0, [CH]:2.0, [C]:2.0]


ID=Reaction Center at Level: 1 (5)
[[C]=C[CH2]:1.0, [C]C(=[CH])[CH2]:2.0, [C]C=[C]:1.0, [C]C[CH2]:1.0, [C]C[C]:1.0]


ID=Reaction Center at Level: 2 (6)
[[C]C(=[CH])CC(=O)[CH2]:1.0, [C]C(=[CH])CC[CH]:1.0, [C]C([CH2])=CC(=O)[CH2]:1.0, [C]C([CH2])=CC[CH]:1.0, [C]C=C(C[CH2])C([CH])([CH2])C:1.0, [C]CC(=C[CH2])C([CH])([CH2])C:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C[C]>>[C]C=[C]
2: [C]C(=[CH])[CH2]>>[C]C(=[CH])[CH2]
3: [C]=C[CH2]>>[C]C[CH2]

MMP Level 2
1: [C]C(=[CH])CC(=O)[CH2]>>[C]C([CH2])=CC(=O)[CH2]
2: [C]CC(=C[CH2])C([CH])([CH2])C>>[C]C=C(C[CH2])C([CH])([CH2])C
3: [C]C([CH2])=CC[CH]>>[C]C(=[CH])CC[CH]

MMP Level 3
1: O=C1CC(=C[CH2])C([CH])(C)CC1>>O=C1C=C(C[CH2])C([CH])(C)CC1
2: O=C1CC2=CC[CH]C([CH2])C2(C)CC1>>O=C1C=C2CC[CH]C([CH2])C2(C)CC1
3: [C]CC1=CCC([CH])[CH]C1([CH2])C>>[C]C=C1CCC([CH])[CH]C1([CH2])C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C1CC2=CCC3C4CCC(=O)C4(C)CCC3C2(C)CC1>>O=C1C=C2CCC3C4CCC(=O)C4(C)CCC3C2(C)CC1, O=C1CC2=CCC3C4CCC(=O)C4(C)CCC3C2(C)CC1>>O=C1C=C2CCC3C4CCC(=O)C4(C)CCC3C2(C)CC1, O=C1CC2=CCC3C4CCC(=O)C4(C)CCC3C2(C)CC1>>O=C1C=C2CCC3C4CCC(=O)C4(C)CCC3C2(C)CC1]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[CH2:3][C:4]2=[CH:5][CH2:6][CH:7]3[CH:8]4[CH2:9][CH2:10][C:11](=[O:12])[C:13]4([CH3:14])[CH2:15][CH2:16][CH:17]3[C:18]2([CH3:19])[CH2:20][CH2:21]1>>[O:1]=[C:2]1[CH:3]=[C:4]2[CH2:5][CH2:6][CH:7]3[CH:8]4[CH2:9][CH2:10][C:11](=[O:12])[C:13]4([CH3:14])[CH2:15][CH2:16][CH:17]3[C:18]2([CH3:19])[CH2:20][CH2:21]1


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=14, 2=13, 3=15, 4=16, 5=17, 6=7, 7=8, 8=9, 9=10, 10=11, 11=12, 12=6, 13=5, 14=4, 15=18, 16=20, 17=21, 18=2, 19=1, 20=3, 21=19}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=19, 2=18, 3=20, 4=21, 5=2, 6=1, 7=3, 8=4, 9=5, 10=6, 11=7, 12=17, 13=16, 14=15, 15=13, 16=8, 17=9, 18=10, 19=11, 20=12, 21=14}

