
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[N:3.0, O:3.0, O=C(N)CC:1.0, O=C(N)CC>>N:1.0, O=C(N)CC>>O=C(O)CC:2.0, O=C(O)CC:1.0, O=C(O)[CH2]:2.0, O=C([CH2])N:2.0, O=C([CH2])N>>N:1.0, O=C([CH2])N>>O=C(O)[CH2]:1.0, O>>O=C(O)CC:1.0, O>>O=C(O)[CH2]:1.0, O>>[C]O:1.0, [C]:2.0, [C]N:1.0, [C]N>>N:1.0, [C]O:1.0, [NH2]:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[N:1.0, O:1.0, [C]:2.0, [NH2]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (6)
[N:1.0, O:1.0, O=C(O)[CH2]:1.0, O=C([CH2])N:1.0, [C]N:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (6)
[N:1.0, O:1.0, O=C(N)CC:1.0, O=C(O)CC:1.0, O=C(O)[CH2]:1.0, O=C([CH2])N:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O>>[C]O
2: [C]N>>N
3: O=C([CH2])N>>O=C(O)[CH2]

MMP Level 2
1: O>>O=C(O)[CH2]
2: O=C([CH2])N>>N
3: O=C(N)CC>>O=C(O)CC

MMP Level 3
1: O>>O=C(O)CC
2: O=C(N)CC>>N
3: O=C(N)CC>>O=C(O)CC


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(N)CC>>O=C(O)CC]
2: R:M00001, P:M00004	[O=C(N)CC>>N]
3: R:M00002, P:M00003	[O>>O=C(O)CC]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([NH2:3])[CH2:4][CH3:5].[OH2:6]>>[O:1]=[C:2]([OH:6])[CH2:4][CH3:5].[NH3:3]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=5, 2=4, 3=2, 4=1, 5=3, 6=6}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=5, 2=4, 3=2, 4=1, 5=3, 6=6}

