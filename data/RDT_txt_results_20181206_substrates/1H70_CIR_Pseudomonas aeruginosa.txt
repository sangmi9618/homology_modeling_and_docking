
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-S:1.0, C=O:1.0]

ORDER_CHANGED
[C-N*C=N:1.0]

//
FINGERPRINTS RC
[N(=C(SC)N)CC[CH2]>>O=C(N)NCC[CH2]:1.0, N(=C(SC)N)C[CH2]>>O=C(N)NC[CH2]:1.0, N([CH2])=C(SC)N:1.0, N([CH2])=C(SC)N>>O=C(N)N[CH2]:1.0, N([CH2])=C(SC)N>>SC:1.0, O:3.0, O=C(N)NC[CH2]:1.0, O=C(N)N[CH2]:1.0, O=C([NH])N:2.0, O>>O=C(N)N[CH2]:1.0, O>>O=C([NH])N:1.0, O>>[C]=O:1.0, SC:2.0, [C]:2.0, [C]=N[CH2]:1.0, [C]=N[CH2]>>[C]N[CH2]:1.0, [C]=O:1.0, [C]N[CH2]:1.0, [C]SC:1.0, [C]SC>>SC:1.0, [NH]:1.0, [N]:1.0, [N]=C(SC)N:1.0, [N]=C(SC)N>>SC:1.0, [N]=C([S])N:1.0, [N]=C([S])N>>O=C([NH])N:1.0, [O]:1.0, [SH]:1.0, [S]:1.0, [S]C(=NC[CH2])N:1.0, [S]C(=NC[CH2])N>>O=C(N)NC[CH2]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [C]:2.0, [O]:1.0, [S]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=C([NH])N:1.0, [C]=O:1.0, [C]SC:1.0, [N]=C([S])N:1.0]


ID=Reaction Center at Level: 2 (5)
[N([CH2])=C(SC)N:1.0, O:1.0, O=C(N)N[CH2]:1.0, O=C([NH])N:1.0, [N]=C(SC)N:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [NH]:1.0, [N]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([NH])N:1.0, [C]=N[CH2]:1.0, [C]N[CH2]:1.0, [N]=C([S])N:1.0]


ID=Reaction Center at Level: 2 (4)
[N([CH2])=C(SC)N:1.0, O=C(N)NC[CH2]:1.0, O=C(N)N[CH2]:1.0, [S]C(=NC[CH2])N:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]=N[CH2]>>[C]N[CH2]
2: [N]=C([S])N>>O=C([NH])N
3: O>>[C]=O
4: [C]SC>>SC

MMP Level 2
1: [S]C(=NC[CH2])N>>O=C(N)NC[CH2]
2: N([CH2])=C(SC)N>>O=C(N)N[CH2]
3: O>>O=C([NH])N
4: [N]=C(SC)N>>SC

MMP Level 3
1: N(=C(SC)N)CC[CH2]>>O=C(N)NCC[CH2]
2: N(=C(SC)N)C[CH2]>>O=C(N)NC[CH2]
3: O>>O=C(N)N[CH2]
4: N([CH2])=C(SC)N>>SC


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)CCCN=C(SC)N>>SC]
2: R:M00001, P:M00004	[O=C(O)C(N)CCCN=C(SC)N>>O=C(N)NCCCC(N)C(=O)O, O=C(O)C(N)CCCN=C(SC)N>>O=C(N)NCCCC(N)C(=O)O]
3: R:M00002, P:M00004	[O>>O=C(N)NCCCC(N)C(=O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][CH2:8][N:9]=[C:10]([S:11][CH3:12])[NH2:13].[OH2:14]>>[O:14]=[C:10]([NH2:13])[NH:9][CH2:8][CH2:7][CH2:6][CH:4]([NH2:5])[C:2](=[O:1])[OH:3].[SH:11][CH3:12]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=12, 2=11, 3=10, 4=9, 5=8, 6=7, 7=6, 8=4, 9=2, 10=1, 11=3, 12=5, 13=13, 14=14}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=14, 2=13, 3=6, 4=7, 5=8, 6=10, 7=11, 8=12, 9=9, 10=5, 11=4, 12=2, 13=1, 14=3}

