
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0, O-P:2.0]

ORDER_CHANGED
[O-P*O=P:2.0]

STEREO_CHANGED
[C(R/S):5.0]

//
FINGERPRINTS RC
[O:3.0, O=P(O)(O)O[P]:2.0, O=P(O)(O[P])OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[P]:2.0, O=P(O)(O[P])O[P]>>O=P(O)(O)O[P]:4.0, O=[P]:2.0, O=[P]>>[P]O:1.0, O>>O=P(O)(O)O[P]:1.0, O>>[O]P(=O)(O)O:1.0, O>>[P]O:1.0, OC1[CH]OC([CH2])C1O:3.0, OC1[CH]OC([CH2])C1O>>OC1[CH]OC([CH2])C1O:1.0, OC1[CH]OC([CH2])C1O>>[O]C1[CH]OC([CH2])C1O:1.0, [CH]:11.0, [CH]C([CH])O:7.0, [CH]C([CH])O>>[CH]C([CH])O:3.0, [CH]C([CH])O>>[N]C(=[N])OC([CH])[CH]:1.0, [CH]O:1.0, [CH]O>>[C]O[CH]:1.0, [C]:1.0, [C]N([CH])C1OC([CH2])C(O)C1O>>[C]N([CH])C1OC([CH2])C(O)C1O:1.0, [C]N([CH])C1O[CH][CH]C1O:2.0, [C]N([CH])C1O[CH][CH]C1O>>[C]N([CH])C1O[CH][CH]C1O:1.0, [C]N=C(N=[C])O[CH]:1.0, [C]N=CN=[C]:1.0, [C]N=CN=[C]>>[C]N=C(N=[C])O[CH]:1.0, [C]O[CH]:1.0, [N]C(=[N])OC([CH])[CH]:1.0, [N]C(=[N])[O]:1.0, [N]C([O])[CH]:2.0, [N]C([O])[CH]>>[N]C([O])[CH]:1.0, [N]C1=[C]N=CN1C2OC([CH2])C(O)C2O>>[N]C1=[C]N=CN1C2OC([CH2])C(O)C2O:1.0, [N]C1OC(CO[P])C(O)C1O>>[N]C1OC(CO[P])C(O)C1O:1.0, [N]C1OC(C[O])C(O)C1O>>[C]OC1C([N])OC(C[O])C1O:1.0, [N]C1OC(C[O])C(O)C1O>>[N]C1OC(C[O])C(O)C1O:1.0, [N]C1O[CH]C(O)C1O:2.0, [N]C1O[CH]C(O)C1O>>[C]N=C(N=[C])OC1C([N])O[CH]C1O:1.0, [N]C1O[CH]C(O)C1O>>[N]C1O[CH]C(O)C1O:1.0, [N]C=1[C]=C(N=CN1)N>>[N]C=1[C]=C(N=C(N1)OC([CH])[CH])N:1.0, [N]C=[N]:1.0, [N]C=[N]>>[N]C(=[N])[O]:1.0, [OH]:5.0, [O]:4.0, [O]C([CH])[CH2]:2.0, [O]C([CH])[CH2]>>[O]C([CH])[CH2]:1.0, [O]C1[CH]OC([CH2])C1O:1.0, [O]CC1O[CH][CH]C1O:2.0, [O]CC1O[CH][CH]C1O>>[O]CC1O[CH][CH]C1O:1.0, [O]P(=O)(O)O:6.0, [O]P(=O)(O)OP(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)OP(=O)(O)O:2.0, [O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:4.0, [O]P([O])(=O)O>>[O]P(=O)(O)O:4.0, [P]:4.0, [P]O:4.0, [P]O>>O=[P]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[O:1.0, [C]:1.0, [OH]:1.0, [O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (7)
[O:1.0, [C]O[CH]:1.0, [N]C(=[N])[O]:1.0, [O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (7)
[O:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[P]:1.0, [C]N=C(N=[C])O[CH]:1.0, [N]C(=[N])OC([CH])[CH]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[OH]:2.0, [O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (4)
[O=[P]:2.0, [O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [P]O:2.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[P]:1.0, [O]P(=O)(O)O:2.0, [O]P([O])(=O)O:2.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:10.0]


ID=Reaction Center at Level: 1 (3)
[[CH]C([CH])O:6.0, [N]C([O])[CH]:2.0, [O]C([CH])[CH2]:2.0]


ID=Reaction Center at Level: 2 (5)
[OC1[CH]OC([CH2])C1O:3.0, [C]N([CH])C1O[CH][CH]C1O:2.0, [N]C1O[CH]C(O)C1O:2.0, [O]C1[CH]OC([CH2])C1O:1.0, [O]CC1O[CH][CH]C1O:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]C([CH])O>>[CH]C([CH])O
2: [CH]C([CH])O>>[CH]C([CH])O
3: [N]C([O])[CH]>>[N]C([O])[CH]
4: [N]C=[N]>>[N]C(=[N])[O]
5: [P]O[P]>>[P]O
6: O=[P]>>[P]O
7: O>>[P]O
8: [CH]C([CH])O>>[CH]C([CH])O
9: [O]P([O])(=O)O>>[O]P(=O)(O)O
10: [O]P([O])(=O)O>>[O]P(=O)(O)O
11: [O]C([CH])[CH2]>>[O]C([CH])[CH2]
12: [P]O>>O=[P]
13: [CH]O>>[C]O[CH]

MMP Level 2
1: [N]C1O[CH]C(O)C1O>>[N]C1O[CH]C(O)C1O
2: OC1[CH]OC([CH2])C1O>>OC1[CH]OC([CH2])C1O
3: [C]N([CH])C1O[CH][CH]C1O>>[C]N([CH])C1O[CH][CH]C1O
4: [C]N=CN=[C]>>[C]N=C(N=[C])O[CH]
5: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
6: [O]P([O])(=O)O>>[O]P(=O)(O)O
7: O>>[O]P(=O)(O)O
8: OC1[CH]OC([CH2])C1O>>[O]C1[CH]OC([CH2])C1O
9: O=P(O)(O[P])O[P]>>O=P(O)(O)O[P]
10: O=P(O)(O[P])O[P]>>O=P(O)(O)O[P]
11: [O]CC1O[CH][CH]C1O>>[O]CC1O[CH][CH]C1O
12: [O]P([O])(=O)O>>[O]P(=O)(O)O
13: [CH]C([CH])O>>[N]C(=[N])OC([CH])[CH]

MMP Level 3
1: [C]N([CH])C1OC([CH2])C(O)C1O>>[C]N([CH])C1OC([CH2])C(O)C1O
2: [N]C1OC(C[O])C(O)C1O>>[N]C1OC(C[O])C(O)C1O
3: [N]C1=[C]N=CN1C2OC([CH2])C(O)C2O>>[N]C1=[C]N=CN1C2OC([CH2])C(O)C2O
4: [N]C=1[C]=C(N=CN1)N>>[N]C=1[C]=C(N=C(N1)OC([CH])[CH])N
5: O=P(O)(O[P])OP(=O)(O)O[P]>>O=P(O)(O)O[P]
6: O=P(O)(O[P])O[P]>>O=P(O)(O)O[P]
7: O>>O=P(O)(O)O[P]
8: [N]C1OC(C[O])C(O)C1O>>[C]OC1C([N])OC(C[O])C1O
9: [O]P(=O)(O)OP(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)OP(=O)(O)O
10: [O]P(=O)(O)OP(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)OP(=O)(O)O
11: [N]C1OC(CO[P])C(O)C1O>>[N]C1OC(CO[P])C(O)C1O
12: O=P(O)(O[P])O[P]>>O=P(O)(O)O[P]
13: [N]C1O[CH]C(O)C1O>>[C]N=C(N=[C])OC1C([N])O[CH]C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O>>O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(nc(nc32)OC4C(O)C(OC4n5cnc6c(ncnc65)N)COP(=O)(O)OP(=O)(O)OP(=O)(O)O)N)C(O)C1O, O=P(O)(OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O>>O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(nc(nc32)OC4C(O)C(OC4n5cnc6c(ncnc65)N)COP(=O)(O)OP(=O)(O)OP(=O)(O)O)N)C(O)C1O, O=P(O)(OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O>>O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(nc(nc32)OC4C(O)C(OC4n5cnc6c(ncnc65)N)COP(=O)(O)OP(=O)(O)OP(=O)(O)O)N)C(O)C1O, O=P(O)(OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O>>O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(nc(nc32)OC4C(O)C(OC4n5cnc6c(ncnc65)N)COP(=O)(O)OP(=O)(O)OP(=O)(O)O)N)C(O)C1O, O=P(O)(OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O>>O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(nc(nc32)OC4C(O)C(OC4n5cnc6c(ncnc65)N)COP(=O)(O)OP(=O)(O)OP(=O)(O)O)N)C(O)C1O, O=P(O)(OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O>>O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(nc(nc32)OC4C(O)C(OC4n5cnc6c(ncnc65)N)COP(=O)(O)OP(=O)(O)OP(=O)(O)O)N)C(O)C1O, O=P(O)(OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O>>O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(nc(nc32)OC4C(O)C(OC4n5cnc6c(ncnc65)N)COP(=O)(O)OP(=O)(O)OP(=O)(O)O)N)C(O)C1O, O=P(O)(OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O>>O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(nc(nc32)OC4C(O)C(OC4n5cnc6c(ncnc65)N)COP(=O)(O)OP(=O)(O)OP(=O)(O)O)N)C(O)C1O, O=P(O)(OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O>>O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(nc(nc32)OC4C(O)C(OC4n5cnc6c(ncnc65)N)COP(=O)(O)OP(=O)(O)OP(=O)(O)O)N)C(O)C1O, O=P(O)(OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O>>O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(nc(nc32)OC4C(O)C(OC4n5cnc6c(ncnc65)N)COP(=O)(O)OP(=O)(O)OP(=O)(O)O)N)C(O)C1O, O=P(O)(OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O>>O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(nc(nc32)OC4C(O)C(OC4n5cnc6c(ncnc65)N)COP(=O)(O)OP(=O)(O)OP(=O)(O)O)N)C(O)C1O, O=P(O)(OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O>>O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(nc(nc32)OC4C(O)C(OC4n5cnc6c(ncnc65)N)COP(=O)(O)OP(=O)(O)OP(=O)(O)O)N)C(O)C1O]
2: R:M00002, P:M00003	[O>>O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(nc(nc32)OC4C(O)C(OC4n5cnc6c(ncnc65)N)COP(=O)(O)OP(=O)(O)OP(=O)(O)O)N)C(O)C1O]


//
SELECTED AAM MAPPING
[O:41]=[P:40]([OH:42])([O:43][CH2:44][CH:45]1[O:46][CH:47]([N:48]:2:[CH:49]:[N:50]:[C:51]:3:[C:52](:[N:53]:[CH:54]:[N:55]:[C:56]32)[NH2:57])[CH:58]([OH:59])[CH:60]1[OH:61])[O:39][P:36](=[O:37])([OH:38])[O:35][P:32](=[O:33])([OH:34])[O:31][P:28](=[O:29])([OH:30])[O:27][P:24](=[O:25])([OH:26])[O:23][P:2](=[O:1])([OH:3])[O:4][CH2:5][CH:6]4[O:7][CH:8]([N:9]:5:[CH:10]:[N:11]:[C:12]:6:[C:13](:[N:14]:[CH:15]:[N:16]:[C:17]65)[NH2:18])[CH:19]([OH:20])[CH:21]4[OH:22].[OH2:62]>>[O:30]=[P:28]([OH:31])([OH:29])[O:27][P:24](=[O:25])([OH:26])[O:23][P:2](=[O:1])([OH:3])[O:4][CH2:5][CH:6]1[O:7][CH:8]([N:9]:2:[CH:10]:[N:11]:[C:12]:3:[C:13](:[N:14]:[C:15](:[N:16]:[C:17]32)[O:59][CH:58]4[CH:60]([OH:61])[CH:45]([O:46][CH:47]4[N:48]:5:[CH:49]:[N:50]:[C:51]:6:[C:52](:[N:53]:[CH:54]:[N:55]:[C:56]65)[NH2:57])[CH2:44][O:43][P:40](=[O:41])([OH:42])[O:39][P:36](=[O:37])([OH:38])[O:35][P:32](=[O:33])([OH:34])[OH:62])[NH2:18])[CH:19]([OH:20])[CH:21]1[OH:22]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=54, 2=55, 3=56, 4=51, 5=52, 6=53, 7=57, 8=50, 9=49, 10=48, 11=47, 12=58, 13=60, 14=45, 15=46, 16=44, 17=43, 18=40, 19=41, 20=42, 21=39, 22=36, 23=37, 24=38, 25=35, 26=32, 27=33, 28=34, 29=31, 30=28, 31=29, 32=30, 33=27, 34=24, 35=25, 36=26, 37=23, 38=2, 39=1, 40=3, 41=4, 42=5, 43=6, 44=21, 45=19, 46=8, 47=7, 48=9, 49=10, 50=11, 51=12, 52=17, 53=16, 54=15, 55=14, 56=13, 57=18, 58=20, 59=22, 60=61, 61=59, 62=62}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=40, 2=41, 3=42, 4=37, 5=38, 6=39, 7=43, 8=36, 9=35, 10=34, 11=33, 12=28, 13=29, 14=31, 15=32, 16=44, 17=45, 18=46, 19=47, 20=48, 21=49, 22=50, 23=51, 24=52, 25=53, 26=54, 27=55, 28=56, 29=57, 30=30, 31=27, 32=24, 33=25, 34=26, 35=21, 36=22, 37=23, 38=58, 39=20, 40=19, 41=18, 42=17, 43=59, 44=61, 45=15, 46=16, 47=14, 48=13, 49=10, 50=11, 51=12, 52=9, 53=6, 54=7, 55=8, 56=5, 57=2, 58=1, 59=3, 60=4, 61=62, 62=60}

