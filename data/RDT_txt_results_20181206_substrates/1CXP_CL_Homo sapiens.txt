
//
FINGERPRINTS BC

FORMED_CLEAVED
[Cl-O:1.0, O-O:1.0]

//
FINGERPRINTS RC
[ClO:4.0, O:3.0, OO:4.0, OO>>ClO:3.0, OO>>O:3.0, [Cl-]:3.0, [Cl-]>>ClO:3.0, [Cl]:1.0, [OH]:3.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [Cl-]:1.0, [Cl]:1.0, [OH]:3.0]


ID=Reaction Center at Level: 1 (4)
[ClO:2.0, O:1.0, OO:2.0, [Cl-]:1.0]


ID=Reaction Center at Level: 2 (4)
[ClO:2.0, O:1.0, OO:2.0, [Cl-]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [Cl-]>>ClO
2: OO>>O
3: OO>>ClO

MMP Level 2
1: [Cl-]>>ClO
2: OO>>O
3: OO>>ClO

MMP Level 3
1: [Cl-]>>ClO
2: OO>>O
3: OO>>ClO


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[[Cl-]>>ClO]
2: R:M00002, P:M00003	[OO>>ClO]
3: R:M00002, P:M00004	[OO>>O]


//
SELECTED AAM MAPPING
[Cl-:3].[OH:1][OH:2]>>[Cl:3][OH:2].[OH2:1]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=3, 2=1, 3=2}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=2, 2=1, 3=3}

