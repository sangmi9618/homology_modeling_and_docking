
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O:3.0, O=P(O)(O)O[CH]:1.0, O=P(O)(OC[CH])OC([CH])[CH]>>O=P(O)(O)OC([CH])[CH]:1.0, O=P(O)(O[CH])O[CH2]:1.0, O=P(O)(O[CH])O[CH2]>>O=P(O)(O)O[CH]:1.0, O>>O=P(O)(O)O[CH]:1.0, O>>[O]P(=O)(O)O:1.0, O>>[P]O:1.0, O[CH2]:1.0, [CH]CO:1.0, [OH]:2.0, [O]:1.0, [O]C([CH])COP(=O)(O)O[CH]>>[O]C([CH])CO:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)OC[CH]:1.0, [O]P(=O)(O)OC[CH]>>[CH]CO:1.0, [O]P([O])(=O)O:1.0, [O]P([O])(=O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[CH2]>>O[CH2]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, [O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [P]O:1.0, [P]O[CH2]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=P(O)(O)O[CH]:1.0, O=P(O)(O[CH])O[CH2]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OC[CH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[CH2]>>O[CH2]
2: [O]P([O])(=O)O>>[O]P(=O)(O)O
3: O>>[P]O

MMP Level 2
1: [O]P(=O)(O)OC[CH]>>[CH]CO
2: O=P(O)(O[CH])O[CH2]>>O=P(O)(O)O[CH]
3: O>>[O]P(=O)(O)O

MMP Level 3
1: [O]C([CH])COP(=O)(O)O[CH]>>[O]C([CH])CO
2: O=P(O)(OC[CH])OC([CH])[CH]>>O=P(O)(O)OC([CH])[CH]
3: O>>O=P(O)(O)O[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1nc(N)[nH]c2c1ncn2C3OC(CO)C(OP(=O)(O)OCC4OC(n5ccc(=O)[nH]c5=O)C(O)C4O)C3O>>O=c1nc(N)[nH]c2c1ncn2C3OC(CO)C(OP(=O)(O)O)C3O]
2: R:M00001, P:M00004	[O=c1nc(N)[nH]c2c1ncn2C3OC(CO)C(OP(=O)(O)OCC4OC(n5ccc(=O)[nH]c5=O)C(O)C4O)C3O>>O=c1ccn(c(=O)[nH]1)C2OC(CO)C(O)C2O]
3: R:M00002, P:M00003	[O>>O=c1nc(N)[nH]c2c1ncn2C3OC(CO)C(OP(=O)(O)O)C3O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[C:8]1:[N:9]:[CH:10]:[N:11]2[CH:12]3[O:13][CH:14]([CH2:15][OH:16])[CH:17]([O:18][P:19](=[O:20])([OH:21])[O:22][CH2:23][CH:24]4[O:25][CH:26]([N:27]5[CH:28]=[CH:29][C:30](=[O:31])[NH:32][C:33]5=[O:34])[CH:35]([OH:36])[CH:37]4[OH:38])[CH:39]3[OH:40].[OH2:41]>>[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[C:8]1:[N:9]:[CH:10]:[N:11]2[CH:12]3[O:13][CH:14]([CH2:15][OH:16])[CH:17]([O:18][P:19](=[O:20])([OH:41])[OH:21])[CH:39]3[OH:40].[O:31]=[C:30]1[CH:29]=[CH:28][N:27]([C:33](=[O:34])[NH:32]1)[CH:26]2[O:25][CH:24]([CH2:23][OH:22])[CH:37]([OH:38])[CH:35]2[OH:36]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=29, 2=28, 3=27, 4=33, 5=34, 6=32, 7=30, 8=31, 9=26, 10=35, 11=37, 12=24, 13=25, 14=23, 15=22, 16=19, 17=20, 18=21, 19=18, 20=17, 21=14, 22=13, 23=12, 24=39, 25=40, 26=11, 27=10, 28=9, 29=8, 30=7, 31=6, 32=4, 33=3, 34=2, 35=1, 36=5, 37=15, 38=16, 39=38, 40=36, 41=41}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=10, 2=9, 3=8, 4=7, 5=11, 6=12, 7=23, 8=17, 9=14, 10=13, 11=15, 12=16, 13=18, 14=19, 15=20, 16=21, 17=22, 18=24, 19=6, 20=4, 21=3, 22=2, 23=1, 24=5, 25=27, 26=28, 27=29, 28=30, 29=31, 30=32, 31=26, 32=25, 33=33, 34=40, 35=38, 36=35, 37=34, 38=36, 39=37, 40=39, 41=41}

