
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:2.0]

//
FINGERPRINTS RC
[O=P(O)(O[P])OC(O[CH])C([CH])O>>O=P(O)(O)O[P]:1.0, [CH]:2.0, [CH]O:1.0, [CH]O>>[CH]O[CH]:1.0, [CH]OC(O[CH])C([CH])O:1.0, [CH]O[CH]:1.0, [C]C(O)[CH2]:1.0, [C]C(O)[CH2]>>[C]C(OC([O])[CH])[CH2]:1.0, [C]C(OC([O])[CH])[CH2]:1.0, [OH]:2.0, [O]:2.0, [O]C([CH])OP([O])(=O)O:1.0, [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]C([O])[CH]:2.0, [O]C([O])[CH]>>[O]C([O])[CH]:1.0, [O]CC(O)C(=O)O>>[O]CC(OC(O[CH])C([CH])O)C(=O)O:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OC1OC([CH2])[CH]C(O)C1O>>[C]C(OC1OC([CH2])[CH]C(O)C1O)[CH2]:1.0, [P]O:1.0, [P]OC(O[CH])C([CH])O:1.0, [P]OC(O[CH])C([CH])O>>[CH]OC(O[CH])C([CH])O:1.0, [P]O[CH]:1.0, [P]O[CH]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH]:2.0, [O]:2.0]


ID=Reaction Center at Level: 1 (3)
[[CH]O[CH]:1.0, [O]C([O])[CH]:2.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[CH]OC(O[CH])C([CH])O:1.0, [C]C(OC([O])[CH])[CH2]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [P]OC(O[CH])C([CH])O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[CH]>>[P]O
2: [O]C([O])[CH]>>[O]C([O])[CH]
3: [CH]O>>[CH]O[CH]

MMP Level 2
1: [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O
2: [P]OC(O[CH])C([CH])O>>[CH]OC(O[CH])C([CH])O
3: [C]C(O)[CH2]>>[C]C(OC([O])[CH])[CH2]

MMP Level 3
1: O=P(O)(O[P])OC(O[CH])C([CH])O>>O=P(O)(O)O[P]
2: [O]P(=O)(O)OC1OC([CH2])[CH]C(O)C1O>>[C]C(OC1OC([CH2])[CH]C(O)C1O)[CH2]
3: [O]CC(O)C(=O)O>>[O]CC(OC(O[CH])C([CH])O)C(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C(O)C(O)COP(=O)(O)O>>O=C(O)C(OC1OC(CO)C(O)C(O)C1O)COP(=O)(O)O]
2: R:M00002, P:M00003	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OC4OC(CO)C(O)C(O)C4O)C(O)C3O>>O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)O)C(O)C3O]
3: R:M00002, P:M00004	[O=c1nc(N)[nH]c2c1ncn2C3OC(COP(=O)(O)OP(=O)(O)OC4OC(CO)C(O)C(O)C4O)C(O)C3O>>O=C(O)C(OC1OC(CO)C(O)C(O)C1O)COP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:40]=[C:41]([OH:42])[CH:43]([OH:44])[CH2:45][O:46][P:47](=[O:48])([OH:49])[OH:50].[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[C:8]1:[N:9]:[CH:10]:[N:11]2[CH:12]3[O:13][CH:14]([CH2:15][O:16][P:17](=[O:18])([OH:19])[O:20][P:21](=[O:22])([OH:23])[O:24][CH:25]4[O:26][CH:27]([CH2:28][OH:29])[CH:30]([OH:31])[CH:32]([OH:33])[CH:34]4[OH:35])[CH:36]([OH:37])[CH:38]3[OH:39]>>[O:40]=[C:41]([OH:42])[CH:43]([O:44][CH:25]1[O:26][CH:27]([CH2:28][OH:29])[CH:30]([OH:31])[CH:32]([OH:33])[CH:34]1[OH:35])[CH2:45][O:46][P:47](=[O:48])([OH:49])[OH:50].[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[C:8]1:[N:9]:[CH:10]:[N:11]2[CH:12]3[O:13][CH:14]([CH2:15][O:16][P:17](=[O:18])([OH:19])[O:20][P:21](=[O:22])([OH:23])[OH:24])[CH:36]([OH:37])[CH:38]3[OH:39]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=45, 2=43, 3=41, 4=40, 5=42, 6=44, 7=46, 8=47, 9=48, 10=49, 11=50, 12=10, 13=9, 14=8, 15=7, 16=11, 17=12, 18=38, 19=36, 20=14, 21=13, 22=15, 23=16, 24=17, 25=18, 26=19, 27=20, 28=21, 29=22, 30=23, 31=24, 32=25, 33=34, 34=32, 35=30, 36=27, 37=26, 38=28, 39=29, 40=31, 41=33, 42=35, 43=37, 44=39, 45=6, 46=4, 47=3, 48=2, 49=1, 50=5}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=10, 2=9, 3=8, 4=7, 5=11, 6=12, 7=27, 8=25, 9=14, 10=13, 11=15, 12=16, 13=17, 14=18, 15=19, 16=20, 17=21, 18=22, 19=23, 20=24, 21=26, 22=28, 23=6, 24=4, 25=3, 26=2, 27=1, 28=5, 29=37, 30=36, 31=39, 32=41, 33=43, 34=34, 35=35, 36=33, 37=32, 38=45, 39=46, 40=47, 41=48, 42=49, 43=50, 44=30, 45=29, 46=31, 47=44, 48=42, 49=40, 50=38}

