
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O:3.0, O=P(O)(O)O:3.0, O=P(O)(O)OC[CH]:1.0, O=P(O)(O)OC[CH]>>O=P(O)(O)O:1.0, O=P(O)(O)OC[CH]>>[CH]CO:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[CH2]>>O=P(O)(O)O:1.0, O>>O=P(O)(O)O:2.0, O>>[P]O:1.0, O[CH2]:1.0, [CH]CO:1.0, [OH]:2.0, [O]:1.0, [O]C([CH])COP(=O)(O)O>>[O]C([CH])CO:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[CH2]>>O[CH2]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=P(O)(O)O:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=P(O)(O)O:2.0, O=P(O)(O)OC[CH]:1.0, O=P(O)(O)O[CH2]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[CH2]>>O[CH2]
2: O>>[P]O
3: [O]P(=O)(O)O>>O=P(O)(O)O

MMP Level 2
1: O=P(O)(O)OC[CH]>>[CH]CO
2: O>>O=P(O)(O)O
3: O=P(O)(O)O[CH2]>>O=P(O)(O)O

MMP Level 3
1: [O]C([CH])COP(=O)(O)O>>[O]C([CH])CO
2: O>>O=P(O)(O)O
3: O=P(O)(O)OC[CH]>>O=P(O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1nc(N)ccn1C2OC(COP(=O)(O)O)C(O)C2O>>O=c1nc(N)ccn1C2OC(CO)C(O)C2O]
2: R:M00001, P:M00004	[O=c1nc(N)ccn1C2OC(COP(=O)(O)O)C(O)C2O>>O=P(O)(O)O]
3: R:M00002, P:M00004	[O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[CH:6]=[CH:7][N:8]1[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[OH:17])[CH:18]([OH:19])[CH:20]2[OH:21].[OH2:22]>>[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[CH:6]=[CH:7][N:8]1[CH:9]2[O:10][CH:11]([CH2:12][OH:13])[CH:18]([OH:19])[CH:20]2[OH:21].[O:15]=[P:14]([OH:17])([OH:22])[OH:16]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=8, 4=2, 5=1, 6=3, 7=4, 8=5, 9=9, 10=20, 11=18, 12=11, 13=10, 14=12, 15=13, 16=14, 17=15, 18=16, 19=17, 20=19, 21=21, 22=22}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=8, 4=2, 5=1, 6=3, 7=4, 8=5, 9=9, 10=16, 11=14, 12=11, 13=10, 14=12, 15=13, 16=15, 17=17, 18=20, 19=19, 20=18, 21=21, 22=22}

