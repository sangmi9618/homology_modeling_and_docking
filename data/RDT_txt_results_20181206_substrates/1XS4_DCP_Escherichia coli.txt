
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C=O:1.0]

ORDER_CHANGED
[C%N*C%N:1.0]

//
FINGERPRINTS RC
[N:3.0, O:3.0, O=C([CH])[NH]:2.0, O=C1[N]C=CC(=N1)N>>O=C1[N]C=CC(=O)N1:1.0, O=c1nc(N)ccn1[CH]>>O=c1ccn([CH])c(=O)[nH]1:1.0, O>>O=C([CH])[NH]:1.0, O>>[C]=O:1.0, O>>[C]NC(=O)C=[CH]:1.0, [C]:2.0, [C]=O:1.0, [C]N:1.0, [C]N=C(N)C=[CH]:1.0, [C]N=C(N)C=[CH]>>N:1.0, [C]N=C(N)C=[CH]>>[C]NC(=O)C=[CH]:1.0, [C]N=[C]:1.0, [C]N=[C]>>[C]N[C]:1.0, [C]N>>N:1.0, [C]NC(=O)C=[CH]:1.0, [C]N[C]:1.0, [NH2]:1.0, [NH]:1.0, [N]:1.0, [N]=C([CH])N:2.0, [N]=C([CH])N>>N:1.0, [N]=C([CH])N>>O=C([CH])[NH]:1.0, [N]C(=O)N=C([CH])N:1.0, [N]C(=O)N=C([CH])N>>[N]C(=O)NC(=O)[CH]:1.0, [N]C(=O)NC(=O)[CH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[N:1.0, O:1.0, [C]:2.0, [NH2]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (6)
[N:1.0, O:1.0, O=C([CH])[NH]:1.0, [C]=O:1.0, [C]N:1.0, [N]=C([CH])N:1.0]


ID=Reaction Center at Level: 2 (6)
[N:1.0, O:1.0, O=C([CH])[NH]:1.0, [C]N=C(N)C=[CH]:1.0, [C]NC(=O)C=[CH]:1.0, [N]=C([CH])N:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [NH]:1.0, [N]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([CH])[NH]:1.0, [C]N=[C]:1.0, [C]N[C]:1.0, [N]=C([CH])N:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]N=C(N)C=[CH]:1.0, [C]NC(=O)C=[CH]:1.0, [N]C(=O)N=C([CH])N:1.0, [N]C(=O)NC(=O)[CH]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [N]=C([CH])N>>O=C([CH])[NH]
2: O>>[C]=O
3: [C]N>>N
4: [C]N=[C]>>[C]N[C]

MMP Level 2
1: [C]N=C(N)C=[CH]>>[C]NC(=O)C=[CH]
2: O>>O=C([CH])[NH]
3: [N]=C([CH])N>>N
4: [N]C(=O)N=C([CH])N>>[N]C(=O)NC(=O)[CH]

MMP Level 3
1: O=C1[N]C=CC(=N1)N>>O=C1[N]C=CC(=O)N1
2: O>>[C]NC(=O)C=[CH]
3: [C]N=C(N)C=[CH]>>N
4: O=c1nc(N)ccn1[CH]>>O=c1ccn([CH])c(=O)[nH]1


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2, O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2]
2: R:M00001, P:M00004	[O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2>>N]
3: R:M00002, P:M00003	[O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[CH:6]=[CH:7][N:8]1[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][P:22](=[O:23])([OH:24])[OH:25])[CH:26]([OH:27])[CH2:28]2.[OH2:29]>>[O:29]=[C:4]1[CH:6]=[CH:7][N:8]([C:2](=[O:1])[NH:3]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][P:22](=[O:23])([OH:25])[OH:24])[CH:26]([OH:27])[CH2:28]2.[NH3:5]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=28, 2=26, 3=11, 4=10, 5=9, 6=8, 7=7, 8=6, 9=4, 10=3, 11=2, 12=1, 13=5, 14=12, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=25, 28=27, 29=29}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=28, 2=26, 3=11, 4=10, 5=9, 6=5, 7=4, 8=3, 9=2, 10=1, 11=8, 12=6, 13=7, 14=12, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=25, 28=27, 29=29}

