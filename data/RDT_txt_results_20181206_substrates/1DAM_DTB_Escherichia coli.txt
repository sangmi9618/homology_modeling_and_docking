
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%N:2.0, C=O:1.0, O-P:2.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O=C([NH])[NH]:1.0, O=C1N[CH]C(N1)C:1.0, O=C1N[CH]C([CH2])N1:1.0, O=C1N[CH][CH]N1:1.0, O=C=O:3.0, O=C=O>>O=C([NH])[NH]:1.0, O=C=O>>O=C1NC([CH2])C(N1)C:1.0, O=C=O>>O=C1N[CH][CH]N1:1.0, O=C=O>>O=P(O)(O)O:2.0, O=P(O)(O)O:3.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O:1.0, [CH2]C(N)C(N)C:1.0, [CH2]C(N)C(N)C>>O=C1NC([CH2])C(N1)C:1.0, [CH2]C(N)C(N)C>>[C]1NC([CH2])C(N1)C:1.0, [CH2]CC(N)C(N)C:1.0, [CH2]CC(N)C(N)C>>O=C1NC(C)C(N1)C[CH2]:2.0, [CH2]CC(N)C(N)C>>[C]1NC(C)C(N1)C[CH2]:1.0, [CH2]CCC(N)C(N)C>>O=C1NC(C)C(N1)CC[CH2]:1.0, [CH]:4.0, [CH]C(N)C:2.0, [CH]C(N)C>>O=C1N[CH]C(N1)C:1.0, [CH]C(N)C>>[CH]C([NH])C:1.0, [CH]C([CH2])N:2.0, [CH]C([CH2])N>>O=C1N[CH]C([CH2])N1:1.0, [CH]C([CH2])N>>[CH]C([NH])[CH2]:1.0, [CH]C([NH])C:1.0, [CH]C([NH])[CH2]:1.0, [CH]N:2.0, [CH]N>>[C]N[CH]:2.0, [C]:2.0, [C]1NC(C)C(N1)C[CH2]:1.0, [C]1NC([CH2])C(N1)C:1.0, [C]=O:1.0, [C]=O>>[P]O:1.0, [C]N[CH]:2.0, [NH2]:2.0, [NH]:2.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[C]:3.0, [NH]:2.0, [OH]:1.0, [O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (8)
[O=C([NH])[NH]:2.0, O=C=O:1.0, O=P(O)(O)O:1.0, [C]=O:1.0, [C]N[CH]:2.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (7)
[O=C1N[CH]C(N1)C:1.0, O=C1N[CH]C([CH2])N1:1.0, O=C1N[CH][CH]N1:2.0, O=C=O:2.0, O=P(O)(O)O:2.0, O=P(O)(O)O[P]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:4.0]


ID=Reaction Center at Level: 1 (4)
[[CH]C(N)C:1.0, [CH]C([CH2])N:1.0, [CH]C([NH])C:1.0, [CH]C([NH])[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[[CH2]C(N)C(N)C:1.0, [CH2]CC(N)C(N)C:1.0, [C]1NC(C)C(N1)C[CH2]:1.0, [C]1NC([CH2])C(N1)C:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]=O>>[P]O
2: [CH]N>>[C]N[CH]
3: [CH]C(N)C>>[CH]C([NH])C
4: [P]O[P]>>[P]O
5: O=C=O>>O=C([NH])[NH]
6: [CH]N>>[C]N[CH]
7: [CH]C([CH2])N>>[CH]C([NH])[CH2]
8: [O]P(=O)(O)O>>O=P(O)(O)O

MMP Level 2
1: O=C=O>>O=P(O)(O)O
2: [CH]C([CH2])N>>O=C1N[CH]C([CH2])N1
3: [CH2]C(N)C(N)C>>[C]1NC([CH2])C(N1)C
4: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
5: O=C=O>>O=C1N[CH][CH]N1
6: [CH]C(N)C>>O=C1N[CH]C(N1)C
7: [CH2]CC(N)C(N)C>>[C]1NC(C)C(N1)C[CH2]
8: O=P(O)(O)O[P]>>O=P(O)(O)O

MMP Level 3
1: O=C=O>>O=P(O)(O)O
2: [CH2]CC(N)C(N)C>>O=C1NC(C)C(N1)C[CH2]
3: [CH2]CC(N)C(N)C>>O=C1NC(C)C(N1)C[CH2]
4: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
5: O=C=O>>O=C1NC([CH2])C(N1)C
6: [CH2]C(N)C(N)C>>O=C1NC([CH2])C(N1)C
7: [CH2]CCC(N)C(N)C>>O=C1NC(C)C(N1)CC[CH2]
8: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00005	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)O]
3: R:M00002, P:M00005	[O=C=O>>O=P(O)(O)O]
4: R:M00002, P:M00006	[O=C=O>>O=C1NC(C)C(N1)CCCCCC(=O)O]
5: R:M00003, P:M00006	[O=C(O)CCCCCC(N)C(N)C>>O=C1NC(C)C(N1)CCCCCC(=O)O, O=C(O)CCCCCC(N)C(N)C>>O=C1NC(C)C(N1)CCCCCC(=O)O, O=C(O)CCCCCC(N)C(N)C>>O=C1NC(C)C(N1)CCCCCC(=O)O, O=C(O)CCCCCC(N)C(N)C>>O=C1NC(C)C(N1)CCCCCC(=O)O]


//
SELECTED AAM MAPPING
[O:45]=[C:46]=[O:47].[O:32]=[C:33]([OH:34])[CH2:35][CH2:36][CH2:37][CH2:38][CH2:39][CH:40]([NH2:41])[CH:42]([NH2:43])[CH3:44].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:32]=[C:33]([OH:34])[CH2:35][CH2:36][CH2:37][CH2:38][CH2:39][CH:40]1[NH:41][C:46](=[O:45])[NH:43][CH:42]1[CH3:44].[O:1]=[P:2]([OH:3])([OH:4])[OH:47].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=24, 2=25, 3=26, 4=21, 5=22, 6=23, 7=27, 8=20, 9=19, 10=18, 11=17, 12=28, 13=30, 14=15, 15=16, 16=14, 17=13, 18=10, 19=11, 20=12, 21=9, 22=6, 23=7, 24=8, 25=5, 26=2, 27=1, 28=3, 29=4, 30=31, 31=29, 32=46, 33=45, 34=47, 35=44, 36=42, 37=40, 38=39, 39=38, 40=37, 41=36, 42=35, 43=33, 44=32, 45=34, 46=41, 47=43}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=17, 5=18, 6=19, 7=23, 8=16, 9=15, 10=14, 11=13, 12=24, 13=26, 14=11, 15=12, 16=10, 17=9, 18=6, 19=7, 20=8, 21=5, 22=2, 23=1, 24=3, 25=4, 26=27, 27=25, 28=45, 29=44, 30=43, 31=46, 32=47, 33=32, 34=31, 35=33, 36=34, 37=29, 38=28, 39=30, 40=35, 41=36, 42=37, 43=38, 44=39, 45=40, 46=41, 47=42}

