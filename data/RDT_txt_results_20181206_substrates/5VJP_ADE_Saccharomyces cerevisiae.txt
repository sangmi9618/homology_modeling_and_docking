
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O=P(O)(O[P])OC1O[CH][CH]C1O>>O=P(O)(O)O[P]:1.0, [CH]:2.0, [C]C1=[C]N=CN1:1.0, [C]C1=[C]N=CN1>>[C]C1=[C][N]C=N1:1.0, [C]C1=[C][N]C=N1:1.0, [C]N([CH])C1O[CH][CH]C1O:1.0, [C]N=[CH]:1.0, [C]N[CH]:1.0, [C]N[CH]>>[C]N=[CH]:1.0, [NH]:1.0, [N]:1.0, [N]C([O])[CH]:1.0, [N]c1nc[nH]c1C(=[N])N>>[N]c1c(ncn1[CH])C(=[N])N:1.0, [OH]:1.0, [O]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]C([O])[CH]:1.0, [O]C([O])[CH]>>[N]C([O])[CH]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OC1OC([CH2])C(O)C1O>>[N]C1=[C]N=CN1C2OC([CH2])C(O)C2O:1.0, [P]O:1.0, [P]OC1O[CH][CH]C1O:1.0, [P]OC1O[CH][CH]C1O>>[C]N([CH])C1O[CH][CH]C1O:1.0, [P]O[CH]:1.0, [P]O[CH]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [N]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[C]N([CH])[CH]:1.0, [N]C([O])[CH]:1.0, [O]C([O])[CH]:1.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]N([CH])C1O[CH][CH]C1O:1.0, [N]C1=[C]N=CN1C([O])[CH]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [P]OC1O[CH][CH]C1O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]C([O])[CH]>>[N]C([O])[CH]
2: [C]N[CH]>>[C]N=[CH]
3: [P]O[CH]>>[P]O

MMP Level 2
1: [P]OC1O[CH][CH]C1O>>[C]N([CH])C1O[CH][CH]C1O
2: [C]C1=[C]N=CN1>>[C]C1=[C][N]C=N1
3: [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O

MMP Level 3
1: [O]P(=O)(O)OC1OC([CH2])C(O)C1O>>[N]C1=[C]N=CN1C2OC([CH2])C(O)C2O
2: [N]c1nc[nH]c1C(=[N])N>>[N]c1c(ncn1[CH])C(=[N])N
3: O=P(O)(O[P])OC1O[CH][CH]C1O>>O=P(O)(O)O[P]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[n1cnc(N)c2[nH]cnc12>>O=P(O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00002, P:M00003	[O=P(O)(O)OCC1OC(OP(=O)(O)OP(=O)(O)O)C(O)C1O>>O=P(O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
3: R:M00002, P:M00004	[O=P(O)(O)OCC1OC(OP(=O)(O)OP(=O)(O)O)C(O)C1O>>O=P(O)(O)OP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH2:6][CH:7]1[O:8][CH:9]([O:10][P:11](=[O:12])([OH:13])[O:14][P:15](=[O:16])([OH:17])[OH:18])[CH:19]([OH:20])[CH:21]1[OH:22].[N:23]:1:[CH:24]:[N:25]:[C:26]([NH2:27]):[C:28]:2:[NH:29]:[CH:30]:[N:31]:[C:32]12>>[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH2:6][CH:7]1[O:8][CH:9]([N:31]:2:[CH:30]:[N:29]:[C:28]:3:[C:26](:[N:25]:[CH:24]:[N:23]:[C:32]32)[NH2:27])[CH:19]([OH:20])[CH:21]1[OH:22].[O:12]=[P:11]([OH:10])([OH:13])[O:14][P:15](=[O:16])([OH:17])[OH:18]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=30, 2=31, 3=32, 4=28, 5=29, 6=26, 7=25, 8=24, 9=23, 10=27, 11=6, 12=7, 13=21, 14=19, 15=9, 16=8, 17=10, 18=11, 19=12, 20=13, 21=14, 22=15, 23=16, 24=17, 25=18, 26=20, 27=22, 28=5, 29=2, 30=1, 31=3, 32=4}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=16, 2=17, 3=18, 4=13, 5=14, 6=15, 7=19, 8=12, 9=11, 10=10, 11=9, 12=20, 13=22, 14=7, 15=8, 16=6, 17=5, 18=2, 19=1, 20=3, 21=4, 22=23, 23=21, 24=26, 25=25, 26=24, 27=27, 28=28, 29=29, 30=30, 31=31, 32=32}

