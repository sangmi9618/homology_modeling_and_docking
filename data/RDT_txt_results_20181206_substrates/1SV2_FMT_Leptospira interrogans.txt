
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O:3.0, O=C([CH])NC(C(=O)N[CH])C>>O=C([CH])NC(C(=O)N[CH])C:1.0, O=C([NH])C(N)C[CH2]:1.0, O=C([NH])C(N[CH])C[CH2]:1.0, O=C([NH])C(N[CH])C[CH2]>>O=C([NH])C(N)C[CH2]:1.0, O=CNC(C(=O)N[CH])CC[S]>>O=C(N[CH])C(N)CC[S]:1.0, O=CNC(C(=O)[NH])C[CH2]>>O=C([NH])C(N)C[CH2]:1.0, O=CN[CH]:1.0, O=CN[CH]>>O=CO:1.0, O=CO:3.0, O=C[NH]:1.0, O=C[NH]>>O=CO:1.0, O>>O=CO:2.0, O>>[CH]O:1.0, [CH]:6.0, [CH]N:1.0, [CH]N[CH]:1.0, [CH]N[CH]>>[CH]N:1.0, [CH]O:1.0, [C]C([CH2])N:2.0, [C]C([CH2])NC=O:1.0, [C]C([CH2])NC=O>>O=CO:1.0, [C]C([CH2])NC=O>>[C]C([CH2])N:1.0, [C]C([NH])C:2.0, [C]C([NH])C>>[C]C([NH])C:1.0, [C]C([NH])[CH2]:1.0, [C]C([NH])[CH2]>>[C]C([CH2])N:1.0, [C]NC(C(=O)[NH])C:2.0, [C]NC(C(=O)[NH])C>>[C]NC(C(=O)[NH])C:1.0, [NH2]:1.0, [NH]:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [CH]:2.0, [NH]:1.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=CO:1.0, O=C[NH]:1.0, [CH]N[CH]:1.0, [CH]O:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=CN[CH]:1.0, O=CO:2.0, [C]C([CH2])NC=O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:4.0]


ID=Reaction Center at Level: 1 (3)
[[C]C([CH2])N:1.0, [C]C([NH])C:2.0, [C]C([NH])[CH2]:1.0]


ID=Reaction Center at Level: 2 (3)
[O=C([NH])C(N)C[CH2]:1.0, O=C([NH])C(N[CH])C[CH2]:1.0, [C]NC(C(=O)[NH])C:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C([NH])[CH2]>>[C]C([CH2])N
2: O>>[CH]O
3: [C]C([NH])C>>[C]C([NH])C
4: [CH]N[CH]>>[CH]N
5: O=C[NH]>>O=CO

MMP Level 2
1: O=C([NH])C(N[CH])C[CH2]>>O=C([NH])C(N)C[CH2]
2: O>>O=CO
3: [C]NC(C(=O)[NH])C>>[C]NC(C(=O)[NH])C
4: [C]C([CH2])NC=O>>[C]C([CH2])N
5: O=CN[CH]>>O=CO

MMP Level 3
1: O=CNC(C(=O)N[CH])CC[S]>>O=C(N[CH])C(N)CC[S]
2: O>>O=CO
3: O=C([CH])NC(C(=O)N[CH])C>>O=C([CH])NC(C(=O)N[CH])C
4: O=CNC(C(=O)[NH])C[CH2]>>O=C([NH])C(N)C[CH2]
5: [C]C([CH2])NC=O>>O=CO


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=CNC(C(=O)NC(C(=O)NC(C(=O)O)CO)C)CCSC>>O=CO]
2: R:M00001, P:M00004	[O=CNC(C(=O)NC(C(=O)NC(C(=O)O)CO)C)CCSC>>O=C(O)C(NC(=O)C(NC(=O)C(N)CCSC)C)CO, O=CNC(C(=O)NC(C(=O)NC(C(=O)O)CO)C)CCSC>>O=C(O)C(NC(=O)C(NC(=O)C(N)CCSC)C)CO, O=CNC(C(=O)NC(C(=O)NC(C(=O)O)CO)C)CCSC>>O=C(O)C(NC(=O)C(NC(=O)C(N)CCSC)C)CO]
3: R:M00002, P:M00003	[O>>O=CO]


//
SELECTED AAM MAPPING
[O:1]=[CH:2][NH:3][CH:4]([C:5](=[O:6])[NH:7][CH:8]([C:9](=[O:10])[NH:11][CH:12]([C:13](=[O:14])[OH:15])[CH2:16][OH:17])[CH3:18])[CH2:19][CH2:20][S:21][CH3:22].[OH2:23]>>[O:1]=[CH:2][OH:23].[O:14]=[C:13]([OH:15])[CH:12]([NH:11][C:9](=[O:10])[CH:8]([NH:7][C:5](=[O:6])[CH:4]([NH2:3])[CH2:19][CH2:20][S:21][CH3:22])[CH3:18])[CH2:16][OH:17]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=18, 2=8, 3=9, 4=10, 5=11, 6=12, 7=16, 8=17, 9=13, 10=14, 11=15, 12=7, 13=5, 14=6, 15=4, 16=19, 17=20, 18=21, 19=22, 20=3, 21=2, 22=1, 23=23}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=22, 2=21, 3=23, 4=18, 5=8, 6=6, 7=7, 8=5, 9=4, 10=19, 11=20, 12=2, 13=1, 14=3, 15=9, 16=10, 17=11, 18=12, 19=14, 20=15, 21=16, 22=17, 23=13}

