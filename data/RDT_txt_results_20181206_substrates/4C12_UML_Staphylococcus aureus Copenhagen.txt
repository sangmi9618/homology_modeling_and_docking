
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0, O-P:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(N[CH])C[CH2]:1.0, O=C(O)C(N)C[CH2]>>O=C(O)C(NC(=O)C[CH2])C[CH2]:1.0, O=C(O)CC[CH]>>[C]C([CH2])NC(=O)CC[CH]:1.0, O=C(O)C[CH2]:1.0, O=C(O)C[CH2]>>O=C(N[CH])C[CH2]:1.0, O=C(O)C[CH2]>>O=P(O)(O)O:1.0, O=C(O)[CH2]:2.0, O=C(O)[CH2]>>O=C([NH])[CH2]:1.0, O=C(O)[CH2]>>O=P(O)(O)O:1.0, O=C([NH])[CH2]:1.0, O=P(O)(O)O:3.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O:1.0, [CH]:2.0, [CH]N:1.0, [CH]N>>[C]N[CH]:1.0, [C]:2.0, [C]C([CH2])N:1.0, [C]C([CH2])N>>[C]C([CH2])NC(=O)[CH2]:1.0, [C]C([CH2])NC(=O)[CH2]:1.0, [C]NC1C([O])[CH]C(OC1OP([O])(=O)O)[CH2]>>[C]NC1C([O])[CH]C(OC1OP([O])(=O)O)[CH2]:1.0, [C]N[CH]:1.0, [C]O:1.0, [C]O>>[P]O:1.0, [NH2]:1.0, [NH]:1.0, [OH]:3.0, [O]:1.0, [O]C([O])[CH]:2.0, [O]C([O])[CH]>>[O]C([O])[CH]:1.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]OC(O[CH])C([CH])[NH]:2.0, [P]OC(O[CH])C([CH])[NH]>>[P]OC(O[CH])C([CH])[NH]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[[C]:2.0, [NH]:1.0, [OH]:2.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (8)
[O=C(O)[CH2]:1.0, O=C([NH])[CH2]:1.0, O=P(O)(O)O:1.0, [C]N[CH]:1.0, [C]O:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (7)
[O=C(N[CH])C[CH2]:1.0, O=C(O)C[CH2]:1.0, O=C(O)[CH2]:1.0, O=P(O)(O)O:2.0, O=P(O)(O)O[P]:1.0, [C]C([CH2])NC(=O)[CH2]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (1)
[[O]C([O])[CH]:2.0]


ID=Reaction Center at Level: 2 (1)
[[P]OC(O[CH])C([CH])[NH]:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: [P]O[P]>>[P]O
2: [C]O>>[P]O
3: [O]C([O])[CH]>>[O]C([O])[CH]
4: O=C(O)[CH2]>>O=C([NH])[CH2]
5: [O]P(=O)(O)O>>O=P(O)(O)O
6: [CH]N>>[C]N[CH]

MMP Level 2
1: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
2: O=C(O)[CH2]>>O=P(O)(O)O
3: [P]OC(O[CH])C([CH])[NH]>>[P]OC(O[CH])C([CH])[NH]
4: O=C(O)C[CH2]>>O=C(N[CH])C[CH2]
5: O=P(O)(O)O[P]>>O=P(O)(O)O
6: [C]C([CH2])N>>[C]C([CH2])NC(=O)[CH2]

MMP Level 3
1: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
2: O=C(O)C[CH2]>>O=P(O)(O)O
3: [C]NC1C([O])[CH]C(OC1OP([O])(=O)O)[CH2]>>[C]NC1C([O])[CH]C(OC1OP([O])(=O)O)[CH2]
4: O=C(O)CC[CH]>>[C]C([CH2])NC(=O)CC[CH]
5: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O
6: O=C(O)C(N)C[CH2]>>O=C(O)C(NC(=O)C[CH2])C[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00005	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)O]
3: R:M00002, P:M00006	[O=C(O)C(N)CCCCN>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(OC(C(=O)NC(C(=O)NC(C(=O)O)CCC(=O)NC(C(=O)O)CCCCN)C)C)C3NC(=O)C)C(O)C2O]
4: R:M00003, P:M00005	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(OC(C(=O)NC(C(=O)NC(C(=O)O)CCC(=O)O)C)C)C3NC(=O)C)C(O)C2O>>O=P(O)(O)O]
5: R:M00003, P:M00006	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(OC(C(=O)NC(C(=O)NC(C(=O)O)CCC(=O)O)C)C)C3NC(=O)C)C(O)C2O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(OC(C(=O)NC(C(=O)NC(C(=O)O)CCC(=O)NC(C(=O)O)CCCCN)C)C)C3NC(=O)C)C(O)C2O, O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(OC(C(=O)NC(C(=O)NC(C(=O)O)CCC(=O)O)C)C)C3NC(=O)C)C(O)C2O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(OC(C(=O)NC(C(=O)NC(C(=O)O)CCC(=O)NC(C(=O)O)CCCCN)C)C)C3NC(=O)C)C(O)C2O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH2:4][CH2:5][CH:6]([NH:7][C:8](=[O:9])[CH:10]([NH:11][C:12](=[O:13])[CH:14]([O:15][CH:16]1[CH:17]([OH:18])[CH:19]([O:20][CH:21]([O:22][P:23](=[O:24])([OH:25])[O:26][P:27](=[O:28])([OH:29])[O:30][CH2:31][CH:32]2[O:33][CH:34]([N:35]3[CH:36]=[CH:37][C:38](=[O:39])[NH:40][C:41]3=[O:42])[CH:43]([OH:44])[CH:45]2[OH:46])[CH:47]1[NH:48][C:49](=[O:50])[CH3:51])[CH2:52][OH:53])[CH3:54])[CH3:55])[C:56](=[O:57])[OH:58].[O:90]=[C:91]([OH:92])[CH:93]([NH2:94])[CH2:95][CH2:96][CH2:97][CH2:98][NH2:99].[O:59]=[P:60]([OH:61])([OH:62])[O:63][P:64](=[O:65])([OH:66])[O:67][P:68](=[O:69])([OH:70])[O:71][CH2:72][CH:73]1[O:74][CH:75]([N:76]:2:[CH:77]:[N:78]:[C:79]:3:[C:80](:[N:81]:[CH:82]:[N:83]:[C:84]32)[NH2:85])[CH:86]([OH:87])[CH:88]1[OH:89]>>[O:90]=[C:91]([OH:92])[CH:93]([NH:94][C:2](=[O:1])[CH2:4][CH2:5][CH:6]([NH:7][C:8](=[O:9])[CH:10]([NH:11][C:12](=[O:13])[CH:14]([O:15][CH:16]1[CH:17]([OH:18])[CH:19]([O:20][CH:21]([O:22][P:23](=[O:24])([OH:25])[O:26][P:27](=[O:28])([OH:29])[O:30][CH2:31][CH:32]2[O:33][CH:34]([N:35]3[CH:36]=[CH:37][C:38](=[O:39])[NH:40][C:41]3=[O:42])[CH:43]([OH:44])[CH:45]2[OH:46])[CH:47]1[NH:48][C:49](=[O:50])[CH3:51])[CH2:52][OH:53])[CH3:54])[CH3:55])[C:56](=[O:57])[OH:58])[CH2:95][CH2:96][CH2:97][CH2:98][NH2:99].[O:59]=[P:60]([OH:61])([OH:62])[OH:3].[O:65]=[P:64]([OH:63])([OH:66])[O:67][P:68](=[O:69])([OH:70])[O:71][CH2:72][CH:73]1[O:74][CH:75]([N:76]:2:[CH:77]:[N:78]:[C:79]:3:[C:80](:[N:81]:[CH:82]:[N:83]:[C:84]32)[NH2:85])[CH:86]([OH:87])[CH:88]1[OH:89]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=82, 2=83, 3=84, 4=79, 5=80, 6=81, 7=85, 8=78, 9=77, 10=76, 11=75, 12=86, 13=88, 14=73, 15=74, 16=72, 17=71, 18=68, 19=69, 20=70, 21=67, 22=64, 23=65, 24=66, 25=63, 26=60, 27=59, 28=61, 29=62, 30=89, 31=87, 32=96, 33=97, 34=98, 35=99, 36=95, 37=93, 38=91, 39=90, 40=92, 41=94, 42=55, 43=10, 44=8, 45=9, 46=7, 47=6, 48=5, 49=4, 50=2, 51=1, 52=3, 53=56, 54=57, 55=58, 56=11, 57=12, 58=13, 59=14, 60=54, 61=15, 62=16, 63=17, 64=19, 65=20, 66=21, 67=47, 68=48, 69=49, 70=50, 71=51, 72=22, 73=23, 74=24, 75=25, 76=26, 77=27, 78=28, 79=29, 80=30, 81=31, 82=32, 83=45, 84=43, 85=34, 86=33, 87=35, 88=36, 89=37, 90=38, 91=39, 92=40, 93=41, 94=42, 95=44, 96=46, 97=52, 98=53, 99=18}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=87, 2=88, 3=89, 4=84, 5=85, 6=86, 7=90, 8=83, 9=82, 10=81, 11=80, 12=91, 13=93, 14=78, 15=79, 16=77, 17=76, 18=73, 19=74, 20=75, 21=72, 22=69, 23=68, 24=70, 25=71, 26=94, 27=92, 28=97, 29=96, 30=95, 31=98, 32=99, 33=59, 34=14, 35=12, 36=13, 37=11, 38=10, 39=9, 40=8, 41=6, 42=7, 43=5, 44=4, 45=63, 46=64, 47=65, 48=66, 49=67, 50=2, 51=1, 52=3, 53=60, 54=61, 55=62, 56=15, 57=16, 58=17, 59=18, 60=58, 61=19, 62=20, 63=51, 64=25, 65=24, 66=23, 67=21, 68=22, 69=56, 70=57, 71=26, 72=27, 73=28, 74=29, 75=30, 76=31, 77=32, 78=33, 79=34, 80=35, 81=36, 82=49, 83=47, 84=38, 85=37, 86=39, 87=40, 88=41, 89=42, 90=43, 91=44, 92=45, 93=46, 94=48, 95=50, 96=52, 97=53, 98=54, 99=55}

