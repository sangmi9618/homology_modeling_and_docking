
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0, C-O:1.0, C-S:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)C(=O)C(C)C:1.0, O=C(O)C(=O)C(C)C>>O=C(O)CC(O)(C(=O)O)C(C)C:1.0, O=C(O)C(=O)C(C)C>>[C]CC(O)(C(=O)O)C(C)C:2.0, O=C(O)[CH2]:2.0, O=C(SCC[NH])C>>[NH]CCS:1.0, O=C(SC[CH2])C:1.0, O=C(SC[CH2])C>>SC[CH2]:1.0, O=C(SC[CH2])C>>[C]C([CH])(O)CC(=O)O:1.0, O=C(S[CH2])C:1.0, O=C(S[CH2])C>>O=C(O)CC(O)(C(=O)O)C(C)C:1.0, O=C(S[CH2])C>>[C]CC(=O)O:1.0, O=C([S])C:2.0, O=C([S])C>>O=C(O)[CH2]:1.0, O=C([S])C>>[C]C([CH])(O)CC(=O)O:1.0, O>>O=C(O)[CH2]:1.0, O>>[C]CC(=O)O:1.0, O>>[C]O:1.0, SC[CH2]:1.0, S[CH2]:1.0, [CH2]:1.0, [CH3]:1.0, [C]:4.0, [C]=O:1.0, [C]=O>>[C]O:1.0, [C]C:1.0, [C]C(=O)[CH]:2.0, [C]C(=O)[CH]>>[C]C([CH])(O)[CH2]:2.0, [C]C([CH])(O)CC(=O)O:1.0, [C]C([CH])(O)[CH2]:2.0, [C]C>>[C]C[C]:1.0, [C]CC(=O)O:1.0, [C]CC(O)(C(=O)O)C(C)C:1.0, [C]C[C]:1.0, [C]O:2.0, [C]S[CH2]:1.0, [C]S[CH2]>>S[CH2]:1.0, [OH]:2.0, [O]:1.0, [SH]:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[O:1.0, [CH2]:1.0, [C]:3.0, [OH]:1.0, [S]:1.0]


ID=Reaction Center at Level: 1 (7)
[O:1.0, O=C(O)[CH2]:1.0, O=C([S])C:1.0, [C]C([CH])(O)[CH2]:1.0, [C]C[C]:1.0, [C]O:1.0, [C]S[CH2]:1.0]


ID=Reaction Center at Level: 2 (7)
[O:1.0, O=C(O)[CH2]:1.0, O=C(SC[CH2])C:1.0, O=C(S[CH2])C:1.0, [C]C([CH])(O)CC(=O)O:1.0, [C]CC(=O)O:1.0, [C]CC(O)(C(=O)O)C(C)C:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[C]=O:1.0, [C]C(=O)[CH]:1.0, [C]C([CH])(O)[CH2]:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(O)C(=O)C(C)C:1.0, [C]C(=O)[CH]:1.0, [C]C([CH])(O)[CH2]:1.0, [C]CC(O)(C(=O)O)C(C)C:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[C]:2.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(=O)[CH]:1.0, [C]C([CH])(O)[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C(=O)C(C)C:1.0, [C]CC(O)(C(=O)O)C(C)C:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C>>[C]C[C]
2: O>>[C]O
3: [C]C(=O)[CH]>>[C]C([CH])(O)[CH2]
4: [C]=O>>[C]O
5: O=C([S])C>>O=C(O)[CH2]
6: [C]S[CH2]>>S[CH2]

MMP Level 2
1: O=C([S])C>>[C]C([CH])(O)CC(=O)O
2: O>>O=C(O)[CH2]
3: O=C(O)C(=O)C(C)C>>[C]CC(O)(C(=O)O)C(C)C
4: [C]C(=O)[CH]>>[C]C([CH])(O)[CH2]
5: O=C(S[CH2])C>>[C]CC(=O)O
6: O=C(SC[CH2])C>>SC[CH2]

MMP Level 3
1: O=C(S[CH2])C>>O=C(O)CC(O)(C(=O)O)C(C)C
2: O>>[C]CC(=O)O
3: O=C(O)C(=O)C(C)C>>O=C(O)CC(O)(C(=O)O)C(C)C
4: O=C(O)C(=O)C(C)C>>[C]CC(O)(C(=O)O)C(C)C
5: O=C(SC[CH2])C>>[C]C([CH])(O)CC(=O)O
6: O=C(SCC[NH])C>>[NH]CCS


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C>>O=C(O)CC(O)(C(=O)O)C(C)C, O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C>>O=C(O)CC(O)(C(=O)O)C(C)C]
2: R:M00001, P:M00005	[O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C>>O=C(NCCS)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O]
3: R:M00002, P:M00004	[O=C(O)C(=O)C(C)C>>O=C(O)CC(O)(C(=O)O)C(C)C, O=C(O)C(=O)C(C)C>>O=C(O)CC(O)(C(=O)O)C(C)C]
4: R:M00003, P:M00004	[O>>O=C(O)CC(O)(C(=O)O)C(C)C]


//
SELECTED AAM MAPPING
[O:52]=[C:53]([OH:54])[C:55](=[O:56])[CH:57]([CH3:58])[CH3:59].[O:1]=[C:2]([S:3][CH2:4][CH2:5][NH:6][C:7](=[O:8])[CH2:9][CH2:10][NH:11][C:12](=[O:13])[CH:14]([OH:15])[C:16]([CH3:17])([CH3:18])[CH2:19][O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[O:28][CH2:29][CH:30]1[O:31][CH:32]([N:33]:2:[CH:34]:[N:35]:[C:36]:3:[C:37](:[N:38]:[CH:39]:[N:40]:[C:41]32)[NH2:42])[CH:43]([OH:44])[CH:45]1[O:46][P:47](=[O:48])([OH:49])[OH:50])[CH3:51].[OH2:60]>>[O:1]=[C:2]([OH:60])[CH2:51][C:55]([OH:56])([C:53](=[O:52])[OH:54])[CH:57]([CH3:59])[CH3:58].[O:8]=[C:7]([NH:6][CH2:5][CH2:4][SH:3])[CH2:9][CH2:10][NH:11][C:12](=[O:13])[CH:14]([OH:15])[C:16]([CH3:17])([CH3:18])[CH2:19][O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[O:28][CH2:29][CH:30]1[O:31][CH:32]([N:33]:2:[CH:34]:[N:35]:[C:36]:3:[C:37](:[N:38]:[CH:39]:[N:40]:[C:41]32)[NH2:42])[CH:43]([OH:44])[CH:45]1[O:46][P:47](=[O:48])([OH:49])[OH:50]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=51, 2=2, 3=1, 4=3, 5=4, 6=5, 7=6, 8=7, 9=8, 10=9, 11=10, 12=11, 13=12, 14=13, 15=14, 16=16, 17=17, 18=18, 19=19, 20=20, 21=21, 22=22, 23=23, 24=24, 25=25, 26=26, 27=27, 28=28, 29=29, 30=30, 31=45, 32=43, 33=32, 34=31, 35=33, 36=34, 37=35, 38=36, 39=41, 40=40, 41=39, 42=38, 43=37, 44=42, 45=44, 46=46, 47=47, 48=48, 49=49, 50=50, 51=15, 52=58, 53=57, 54=59, 55=55, 56=56, 57=53, 58=52, 59=54, 60=60}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=59, 2=58, 3=60, 4=53, 5=52, 6=50, 7=49, 8=51, 9=55, 10=56, 11=57, 12=54, 13=15, 14=14, 15=16, 16=17, 17=18, 18=19, 19=20, 20=21, 21=22, 22=23, 23=24, 24=25, 25=26, 26=27, 27=28, 28=43, 29=41, 30=30, 31=29, 32=31, 33=32, 34=33, 35=34, 36=39, 37=38, 38=37, 39=36, 40=35, 41=40, 42=42, 43=44, 44=45, 45=46, 46=47, 47=48, 48=12, 49=10, 50=11, 51=9, 52=8, 53=7, 54=2, 55=1, 56=3, 57=4, 58=5, 59=6, 60=13}

