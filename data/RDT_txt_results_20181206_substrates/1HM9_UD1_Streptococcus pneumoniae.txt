
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)O[P]>>O=P(O)(O[P])OP(=O)(O)O[CH2]:1.0, O=P(O)(O[CH])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH]:1.0, O=P(O)(O[P])O[CH2]:2.0, O=P(O)(O[P])O[CH2]>>O=P(O)(O[P])O[CH2]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>[O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP(=O)(O)OC[CH]>>[O]P(=O)(O)OP(=O)(O)OC[CH]:1.0, [O]P(=O)(O)OP([O])(=O)O:2.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:2.0, [O]P([O])(=O)O>>[O]P([O])(=O)O:1.0, [P]:2.0, [P]O:2.0, [P]O>>[P]O[P]:1.0, [P]O[P]:2.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]P([O])(=O)O:2.0, [P]O[P]:2.0]


ID=Reaction Center at Level: 2 (2)
[O=P(O)(O[P])O[CH2]:2.0, [O]P(=O)(O)OP([O])(=O)O:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]P([O])(=O)O>>[O]P([O])(=O)O
2: [P]O[P]>>[P]O
3: [P]O>>[P]O[P]

MMP Level 2
1: O=P(O)(O[P])O[CH2]>>O=P(O)(O[P])O[CH2]
2: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
3: [O]P(=O)(O)O>>[O]P(=O)(O)OP([O])(=O)O

MMP Level 3
1: [O]P(=O)(O)OP(=O)(O)OC[CH]>>[O]P(=O)(O)OP(=O)(O)OC[CH]
2: O=P(O)(O[CH])OP(=O)(O)O[CH2]>>O=P(O)(O)O[CH]
3: O=P(O)(O)O[P]>>O=P(O)(O[P])OP(=O)(O)O[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)OP(=O)(O)O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2O]
2: R:M00002, P:M00003	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3NC(=O)C)C(O)C2O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2O]
3: R:M00002, P:M00004	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3NC(=O)C)C(O)C2O>>O=C(NC1C(O)C(O)C(OC1OP(=O)(O)O)CO)C]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][CH:22]3[O:23][CH:24]([CH2:25][OH:26])[CH:27]([OH:28])[CH:29]([OH:30])[CH:31]3[NH:32][C:33](=[O:34])[CH3:35])[CH:36]([OH:37])[CH:38]2[OH:39].[O:40]=[P:41]([OH:42])([OH:43])[O:44][P:45](=[O:46])([OH:47])[OH:48]>>[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:42][P:41](=[O:40])([OH:43])[O:44][P:45](=[O:46])([OH:47])[OH:48])[CH:36]([OH:37])[CH:38]2[OH:39].[O:34]=[C:33]([NH:32][CH:31]1[CH:29]([OH:30])[CH:27]([OH:28])[CH:24]([O:23][CH:22]1[O:21][P:18](=[O:19])([OH:20])[OH:17])[CH2:25][OH:26])[CH3:35]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=42, 2=41, 3=40, 4=43, 5=44, 6=45, 7=46, 8=47, 9=48, 10=35, 11=33, 12=34, 13=32, 14=31, 15=29, 16=27, 17=24, 18=23, 19=22, 20=21, 21=18, 22=19, 23=20, 24=17, 25=14, 26=15, 27=16, 28=13, 29=12, 30=11, 31=36, 32=38, 33=9, 34=10, 35=5, 36=4, 37=3, 38=2, 39=1, 40=8, 41=6, 42=7, 43=39, 44=37, 45=25, 46=26, 47=28, 48=30}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=3, 2=4, 3=5, 4=6, 5=7, 6=8, 7=2, 8=1, 9=9, 10=28, 11=26, 12=11, 13=10, 14=12, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=25, 28=27, 29=29, 30=48, 31=31, 32=30, 33=32, 34=33, 35=34, 36=36, 37=38, 38=39, 39=40, 40=41, 41=42, 42=43, 43=44, 44=45, 45=46, 46=47, 47=37, 48=35}

