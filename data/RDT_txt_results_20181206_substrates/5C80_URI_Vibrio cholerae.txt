
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O=C([NH])N(C=[CH])C1OC([CH2])C(O)C1O>>O=P(O)(O)OC1OC([CH2])C(O)C1O:1.0, O=C([NH])NC=[CH]:1.0, O=C1N[C]C=CN1C2O[CH][CH]C2O>>O=C1N[C]C=CN1:1.0, O=P(O)(O)O:1.0, O=P(O)(O)O>>O=P(O)(O)OC1O[CH][CH]C1O:1.0, O=P(O)(O)O>>[O]C([CH])OP(=O)(O)O:1.0, [CH]:2.0, [C]N([CH])C1O[CH][CH]C1O:1.0, [C]N([CH])C1O[CH][CH]C1O>>[P]OC1O[CH][CH]C1O:1.0, [C]N([CH])[CH]:1.0, [C]N([CH])[CH]>>[C]N[CH]:1.0, [C]N[CH]:1.0, [NH]:1.0, [N]:1.0, [N]C([O])[CH]:1.0, [N]C([O])[CH]>>[O]C([O])[CH]:1.0, [OH]:1.0, [O]:1.0, [O]C([CH])N(C=[CH])C(=O)[NH]:1.0, [O]C([CH])N(C=[CH])C(=O)[NH]>>O=C([NH])NC=[CH]:1.0, [O]C([CH])OP(=O)(O)O:1.0, [O]C([O])[CH]:1.0, [P]O:1.0, [P]O>>[P]O[CH]:1.0, [P]OC1O[CH][CH]C1O:1.0, [P]O[CH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [N]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[C]N([CH])[CH]:1.0, [N]C([O])[CH]:1.0, [O]C([O])[CH]:1.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]N([CH])C1O[CH][CH]C1O:1.0, [O]C([CH])N(C=[CH])C(=O)[NH]:1.0, [O]C([CH])OP(=O)(O)O:1.0, [P]OC1O[CH][CH]C1O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [N]C([O])[CH]>>[O]C([O])[CH]
2: [C]N([CH])[CH]>>[C]N[CH]
3: [P]O>>[P]O[CH]

MMP Level 2
1: [C]N([CH])C1O[CH][CH]C1O>>[P]OC1O[CH][CH]C1O
2: [O]C([CH])N(C=[CH])C(=O)[NH]>>O=C([NH])NC=[CH]
3: O=P(O)(O)O>>[O]C([CH])OP(=O)(O)O

MMP Level 3
1: O=C([NH])N(C=[CH])C1OC([CH2])C(O)C1O>>O=P(O)(O)OC1OC([CH2])C(O)C1O
2: O=C1N[C]C=CN1C2O[CH][CH]C2O>>O=C1N[C]C=CN1
3: O=P(O)(O)O>>O=P(O)(O)OC1O[CH][CH]C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1ccn(c(=O)[nH]1)C2OC(CO)C(O)C2O>>O=c1cc[nH]c(=O)[nH]1]
2: R:M00001, P:M00004	[O=c1ccn(c(=O)[nH]1)C2OC(CO)C(O)C2O>>O=P(O)(O)OC1OC(CO)C(O)C1O]
3: R:M00002, P:M00004	[O=P(O)(O)O>>O=P(O)(O)OC1OC(CO)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][OH:13])[CH:14]([OH:15])[CH:16]2[OH:17].[O:18]=[P:19]([OH:20])([OH:21])[OH:22]>>[O:1]=[C:2]1[CH:3]=[CH:4][NH:5][C:6](=[O:7])[NH:8]1.[O:18]=[P:19]([OH:20])([OH:21])[O:22][CH:9]1[O:10][CH:11]([CH2:12][OH:13])[CH:14]([OH:15])[CH:16]1[OH:17]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=3, 2=4, 3=5, 4=6, 5=7, 6=8, 7=2, 8=1, 9=9, 10=16, 11=14, 12=11, 13=10, 14=12, 15=13, 16=15, 17=17, 18=20, 19=19, 20=18, 21=21, 22=22}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=17, 2=18, 3=19, 4=20, 5=21, 6=22, 7=16, 8=15, 9=9, 10=8, 11=11, 12=13, 13=6, 14=7, 15=5, 16=2, 17=1, 18=3, 19=4, 20=14, 21=12, 22=10}

