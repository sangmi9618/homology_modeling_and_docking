
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-S:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(O)C(N)C:1.0, O=C(O)C(N)CS:1.0, O=C(O)C(N)CS>>O=C(O)C(N)C:3.0, S[CH2]:1.0, [CH2]:1.0, [CH3]:1.0, [CH]:2.0, [CH]C:1.0, [CH]CS:2.0, [CH]CS>>[CH]C:1.0, [C]C(N)C:2.0, [C]C(N)CS:1.0, [C]C(N)CS>>[C]C(N)C:1.0, [C]C([CH2])N:1.0, [C]C([CH2])N>>[C]C(N)C:1.0, [SH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH2]:1.0, [SH]:1.0]


ID=Reaction Center at Level: 1 (2)
[S[CH2]:1.0, [CH]CS:1.0]


ID=Reaction Center at Level: 2 (2)
[[CH]CS:1.0, [C]C(N)CS:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (2)
[[C]C(N)C:1.0, [C]C([CH2])N:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C(N)C:1.0, O=C(O)C(N)CS:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C([CH2])N>>[C]C(N)C
2: [CH]CS>>[CH]C

MMP Level 2
1: O=C(O)C(N)CS>>O=C(O)C(N)C
2: [C]C(N)CS>>[C]C(N)C

MMP Level 3
1: O=C(O)C(N)CS>>O=C(O)C(N)C
2: O=C(O)C(N)CS>>O=C(O)C(N)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)C(N)CS>>O=C(O)C(N)C, O=C(O)C(N)CS>>O=C(O)C(N)C]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][SH:7]>>[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH3:6]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=4, 3=2, 4=1, 5=3, 6=5, 7=7}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=4, 3=2, 4=1, 5=3, 6=5}

