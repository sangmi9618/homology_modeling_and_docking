
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%O:1.0, C-O:2.0]

//
FINGERPRINTS RC
[O:3.0, OCC1(O)O[CH][CH]C1O>>[O]C([CH])COC1(O[CH][CH]C1O)CO:1.0, O[CH2]:1.0, O[CH2]>>O:1.0, [CH2]:3.0, [CH]CO:2.0, [CH]CO>>O:1.0, [C]O:1.0, [C]O>>[C]O[CH2]:1.0, [C]OCC([O])[CH]:2.0, [C]OCC([O])[CH]>>[C]OCC([O])[CH]:1.0, [C]O[CH2]:2.0, [OH]:2.0, [O]:2.0, [O]C([CH])(O)[CH2]:1.0, [O]C([CH])(O)[CH2]>>[O]C([CH])(OC[CH])[CH2]:1.0, [O]C([CH])(OCC1O[C][CH]C1O)[CH2]>>[O]C([CH])(OCC1O[C][CH]C1O)[CH2]:1.0, [O]C([CH])(OC[CH])[CH2]:2.0, [O]C([CH])CO:1.0, [O]C([CH])CO>>O:1.0, [O]C[CH]:2.0, [O]C[CH]>>[O]C[CH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [CH2]:3.0, [OH]:1.0, [O]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O[CH2]:1.0, [CH]CO:1.0, [C]O[CH2]:2.0, [O]C[CH]:2.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, [CH]CO:1.0, [C]OCC([O])[CH]:2.0, [O]C([CH])(OC[CH])[CH2]:2.0, [O]C([CH])CO:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O[CH2]>>O
2: [O]C[CH]>>[O]C[CH]
3: [C]O>>[C]O[CH2]

MMP Level 2
1: [CH]CO>>O
2: [C]OCC([O])[CH]>>[C]OCC([O])[CH]
3: [O]C([CH])(O)[CH2]>>[O]C([CH])(OC[CH])[CH2]

MMP Level 3
1: [O]C([CH])CO>>O
2: [O]C([CH])(OCC1O[C][CH]C1O)[CH2]>>[O]C([CH])(OCC1O[C][CH]C1O)[CH2]
3: OCC1(O)O[CH][CH]C1O>>[O]C([CH])COC1(O[CH][CH]C1O)CO


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[OCC1OC(OCC2OC(OCC3OC(O)(CO)C(O)C3O)(CO)C(O)C2O)(CO)C(O)C1O>>OCC12OCC3OC(OCC(O1)C(O)C2O)(CO)C(O)C3O, OCC1OC(OCC2OC(OCC3OC(O)(CO)C(O)C3O)(CO)C(O)C2O)(CO)C(O)C1O>>OCC12OCC3OC(OCC(O1)C(O)C2O)(CO)C(O)C3O]
2: R:M00001, P:M00003	[OCC1OC(OCC2OC(OCC3OC(O)(CO)C(O)C3O)(CO)C(O)C2O)(CO)C(O)C1O>>O]


//
SELECTED AAM MAPPING
[OH:1][CH2:24][CH:25]1[O:26][C:27]([O:28][CH2:2][CH:3]2[O:4][C:5]([O:6][CH2:7][CH:8]3[O:9][C:10]([OH:11])([CH2:12][OH:13])[CH:14]([OH:15])[CH:16]3[OH:17])([CH2:18][OH:19])[CH:20]([OH:21])[CH:22]2[OH:23])([CH2:29][OH:30])[CH:31]([OH:32])[CH:33]1[OH:34]>>[OH2:1].[OH:13][CH2:12][C:10]12[O:11][CH2:2][CH:3]3[O:4][C:5]([O:6][CH2:7][CH:8]([O:9]1)[CH:16]([OH:17])[CH:14]2[OH:15])([CH2:18][OH:19])[CH:20]([OH:21])[CH:22]3[OH:23]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=2, 2=3, 3=33, 4=31, 5=5, 6=4, 7=29, 8=30, 9=6, 10=7, 11=8, 12=27, 13=25, 14=10, 15=9, 16=23, 17=24, 18=11, 19=12, 20=13, 21=21, 22=19, 23=15, 24=14, 25=17, 26=18, 27=16, 28=20, 29=22, 30=26, 31=28, 32=32, 33=34, 34=1}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=5, 2=6, 3=21, 4=19, 5=8, 6=7, 7=9, 8=10, 9=11, 10=13, 11=15, 12=3, 13=4, 14=12, 15=2, 16=1, 17=16, 18=14, 19=17, 20=18, 21=20, 22=22, 23=23}

