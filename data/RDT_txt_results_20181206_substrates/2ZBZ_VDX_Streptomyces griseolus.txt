
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:2.0, C-O:1.0, O=O:1.0]

STEREO_CHANGED
[C(E/Z):2.0, C(R/S):2.0]

//
FINGERPRINTS RC
[O:3.0, O=O:4.0, O=O>>O:3.0, O=O>>OC([CH2])(C)C(O)(C)C:1.0, O=O>>[C]C(O)(C)C:1.0, O=O>>[C]O:1.0, OC(C)(C)C(O)(C)C[CH2]:1.0, OC(C)(C)CC[CH2]>>OC(C)(C)C(O)(C)CC[CH2]:1.0, OC(C)(C)C[CH2]:1.0, OC(C)(C)C[CH2]>>OC(C)(C)C(O)(C)C[CH2]:2.0, OC([CH2])(C)C:2.0, OC([CH2])(C)C(O)(C)C:1.0, OC([CH2])(C)C>>OC([CH2])(C)C(O)(C)C:1.0, OC([CH2])(C)C>>[C]C(O)([CH2])C:1.0, [CH3]:1.0, [CH]:4.0, [CH]=C([CH2])C1CC[CH]C1([CH2])C:2.0, [CH]=C([CH2])C1CC[CH]C1([CH2])C>>[CH]=C([CH2])C1CC[CH]C1([CH2])C:1.0, [CH]C([CH2])=CC=C(C([CH])=C)C[CH]>>[CH]C([CH2])=CC=C(C([CH])=C)C[CH]:1.0, [CH]C=C(C([CH])=C)C[CH]:2.0, [CH]C=C(C([CH])=C)C[CH]>>[CH]C=C(C([CH])=C)C[CH]:1.0, [CH]C=C1CCCC2(C)C([CH])CCC12>>[CH]C=C1CCCC2(C)C([CH])CCC12:1.0, [C]:5.0, [C]=CC=C1C(=C)C(O)CC(O)C1>>[C]=CC=C1C(=C)C(O)CC(O)C1:1.0, [C]=C[CH]:2.0, [C]=C[CH]>>[C]=C[CH]:1.0, [C]C:1.0, [C]C(=[CH])[CH2]:2.0, [C]C(=[CH])[CH2]>>[C]C(=[CH])[CH2]:1.0, [C]C(O)(C)C:2.0, [C]C(O)([CH2])C:1.0, [C]C([CH2])=CC=[C]:2.0, [C]C([CH2])=CC=[C]>>[C]C([CH2])=CC=[C]:1.0, [C]C([C])[CH2]:2.0, [C]C([C])[CH2]>>[C]C([C])[CH2]:1.0, [C]C>>[C]C(O)(C)C:1.0, [C]O:1.0, [OH]:1.0, [O]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[O:1.0, [CH3]:2.0, [C]:3.0, [OH]:1.0, [O]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=O:2.0, [C]C:2.0, [C]C(O)(C)C:3.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=O:2.0, OC([CH2])(C)C(O)(C)C:3.0, [C]C(O)(C)C:3.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:4.0, [C]:4.0]


ID=Reaction Center at Level: 1 (5)
[OC([CH2])(C)C:1.0, [C]=C[CH]:2.0, [C]C(=[CH])[CH2]:2.0, [C]C(O)([CH2])C:1.0, [C]C([C])[CH2]:2.0]


ID=Reaction Center at Level: 2 (5)
[OC(C)(C)C(O)(C)C[CH2]:1.0, OC(C)(C)C[CH2]:1.0, [CH]=C([CH2])C1CC[CH]C1([CH2])C:2.0, [CH]C=C(C([CH])=C)C[CH]:2.0, [C]C([CH2])=CC=[C]:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: O=O>>[C]O
2: [C]C(=[CH])[CH2]>>[C]C(=[CH])[CH2]
3: OC([CH2])(C)C>>[C]C(O)([CH2])C
4: [C]=C[CH]>>[C]=C[CH]
5: [C]C>>[C]C(O)(C)C
6: [C]C([C])[CH2]>>[C]C([C])[CH2]
7: O=O>>O

MMP Level 2
1: O=O>>[C]C(O)(C)C
2: [CH]C=C(C([CH])=C)C[CH]>>[CH]C=C(C([CH])=C)C[CH]
3: OC(C)(C)C[CH2]>>OC(C)(C)C(O)(C)C[CH2]
4: [C]C([CH2])=CC=[C]>>[C]C([CH2])=CC=[C]
5: OC([CH2])(C)C>>OC([CH2])(C)C(O)(C)C
6: [CH]=C([CH2])C1CC[CH]C1([CH2])C>>[CH]=C([CH2])C1CC[CH]C1([CH2])C
7: O=O>>O

MMP Level 3
1: O=O>>OC([CH2])(C)C(O)(C)C
2: [C]=CC=C1C(=C)C(O)CC(O)C1>>[C]=CC=C1C(=C)C(O)CC(O)C1
3: OC(C)(C)CC[CH2]>>OC(C)(C)C(O)(C)CC[CH2]
4: [CH]C([CH2])=CC=C(C([CH])=C)C[CH]>>[CH]C([CH2])=CC=C(C([CH])=C)C[CH]
5: OC(C)(C)C[CH2]>>OC(C)(C)C(O)(C)C[CH2]
6: [CH]C=C1CCCC2(C)C([CH])CCC12>>[CH]C=C1CCCC2(C)C([CH])CCC12
7: O=O>>O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[OC1C(=C)C(=CC=C2CCCC3(C)C2CCC3C(C)CCCC(O)(C)C)CC(O)C1>>OC1C(=C)C(=CC=C2CCCC3(C)C2CCC3C(C)CCCC(O)(C)C(O)(C)C)CC(O)C1, OC1C(=C)C(=CC=C2CCCC3(C)C2CCC3C(C)CCCC(O)(C)C)CC(O)C1>>OC1C(=C)C(=CC=C2CCCC3(C)C2CCC3C(C)CCCC(O)(C)C(O)(C)C)CC(O)C1, OC1C(=C)C(=CC=C2CCCC3(C)C2CCC3C(C)CCCC(O)(C)C)CC(O)C1>>OC1C(=C)C(=CC=C2CCCC3(C)C2CCC3C(C)CCCC(O)(C)C(O)(C)C)CC(O)C1, OC1C(=C)C(=CC=C2CCCC3(C)C2CCC3C(C)CCCC(O)(C)C)CC(O)C1>>OC1C(=C)C(=CC=C2CCCC3(C)C2CCC3C(C)CCCC(O)(C)C(O)(C)C)CC(O)C1, OC1C(=C)C(=CC=C2CCCC3(C)C2CCC3C(C)CCCC(O)(C)C)CC(O)C1>>OC1C(=C)C(=CC=C2CCCC3(C)C2CCC3C(C)CCCC(O)(C)C(O)(C)C)CC(O)C1]
2: R:M00003, P:M00004	[O=O>>OC1C(=C)C(=CC=C2CCCC3(C)C2CCC3C(C)CCCC(O)(C)C(O)(C)C)CC(O)C1]
3: R:M00003, P:M00006	[O=O>>O]


//
SELECTED AAM MAPPING
[H+:33].[O:31]=[O:32].[OH:1][CH:2]1[C:3](=[CH2:4])[C:5](=[CH:6][CH:7]=[C:8]2[CH2:9][CH2:10][CH2:11][C:12]3([CH3:13])[CH:14]2[CH2:15][CH2:16][CH:17]3[CH:18]([CH3:19])[CH2:20][CH2:21][CH2:22][C:23]([OH:24])([CH3:25])[CH3:26])[CH2:27][CH:28]([OH:29])[CH2:30]1>>[OH2:31].[OH:36][CH:37]1[C:38](=[CH2:39])[C:40](=[CH:41][CH:42]=[C:43]2[CH2:44][CH2:45][CH2:46][C:47]3([CH3:48])[CH:49]2[CH2:50][CH2:51][CH:52]3[CH:53]([CH3:54])[CH2:55][CH2:56][CH2:57][C:58]([OH:59])([CH3:60])[C:61]([OH:62])([CH3:63])[CH3:64])[CH2:65][CH:66]([OH:67])[CH2:68]1.[OH:1][CH:2]1[C:3](=[CH2:4])[C:5](=[CH:6][CH:7]=[C:8]2[CH2:9][CH2:10][CH2:11][C:12]3([CH3:13])[CH:14]2[CH2:15][CH2:16][CH:17]3[CH:18]([CH3:19])[CH2:20][CH2:21][CH2:22][C:23]([OH:24])([CH3:25])[C:26]([OH:32])([CH3:34])[CH3:35])[CH2:27][CH:28]([OH:29])[CH2:30]1


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=19, 2=18, 3=20, 4=21, 5=22, 6=23, 7=25, 8=26, 9=24, 10=17, 11=16, 12=15, 13=14, 14=12, 15=11, 16=10, 17=9, 18=8, 19=7, 20=6, 21=5, 22=27, 23=28, 24=30, 25=2, 26=3, 27=4, 28=1, 29=29, 30=13, 31=33, 32=31, 33=32}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=19, 2=18, 3=20, 4=21, 5=22, 6=23, 7=25, 8=26, 9=28, 10=29, 11=27, 12=24, 13=17, 14=16, 15=15, 16=14, 17=12, 18=11, 19=10, 20=9, 21=8, 22=7, 23=6, 24=5, 25=30, 26=31, 27=33, 28=2, 29=3, 30=4, 31=1, 32=32, 33=13, 34=52, 35=51, 36=53, 37=54, 38=55, 39=56, 40=58, 41=59, 42=61, 43=62, 44=60, 45=57, 46=50, 47=49, 48=48, 49=47, 50=45, 51=44, 52=43, 53=42, 54=41, 55=40, 56=39, 57=38, 58=63, 59=64, 60=66, 61=35, 62=36, 63=37, 64=34, 65=65, 66=46, 67=67}

