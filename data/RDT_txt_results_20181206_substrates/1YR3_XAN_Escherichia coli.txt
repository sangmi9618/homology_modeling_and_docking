
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O=C([NH])c1ncn([CH])c1[NH]>>O=C([NH])c1[nH]cnc1[NH]:1.0, O=P(O)(O)O:1.0, O=P(O)(O)O>>O=P(O)(O)OC1O[CH][CH]C1O:1.0, O=P(O)(O)O>>[O]C([CH])OP(=O)(O)O:1.0, [CH]:2.0, [C]C1=[C]N=CN1:1.0, [C]C1=[C][N]C=N1:1.0, [C]C1=[C][N]C=N1>>[C]C1=[C]N=CN1:1.0, [C]N([CH])C1O[CH][CH]C1O:1.0, [C]N([CH])C1O[CH][CH]C1O>>[P]OC1O[CH][CH]C1O:1.0, [C]N([CH])[CH]:1.0, [C]N([CH])[CH]>>[C]N=[CH]:1.0, [C]N=[CH]:2.0, [C]N=[CH]>>[C]N[CH]:1.0, [C]N[CH]:1.0, [C]Nc1c([C])ncn1C2O[CH][CH]C2O>>[C]Nc1nc[nH]c1[C]:1.0, [NH]:1.0, [NH]C1=[C]N=CN1C2OC([CH2])C(O)C2O>>O=P(O)(O)OC1OC([CH2])C(O)C1O:1.0, [NH]C1=[C]NC=N1:1.0, [N]:3.0, [N]C([O])[CH]:1.0, [N]C([O])[CH]>>[O]C([O])[CH]:1.0, [OH]:1.0, [O]:1.0, [O]C([CH])N1C=N[C]=C1[NH]:1.0, [O]C([CH])N1C=N[C]=C1[NH]>>[NH]C1=[C]NC=N1:1.0, [O]C([CH])OP(=O)(O)O:1.0, [O]C([O])[CH]:1.0, [P]O:1.0, [P]O>>[P]O[CH]:1.0, [P]OC1O[CH][CH]C1O:1.0, [P]O[CH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [N]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[C]N([CH])[CH]:1.0, [N]C([O])[CH]:1.0, [O]C([O])[CH]:1.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]N([CH])C1O[CH][CH]C1O:1.0, [O]C([CH])N1C=N[C]=C1[NH]:1.0, [O]C([CH])OP(=O)(O)O:1.0, [P]OC1O[CH][CH]C1O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]N([CH])[CH]>>[C]N=[CH]
2: [C]N=[CH]>>[C]N[CH]
3: [P]O>>[P]O[CH]
4: [N]C([O])[CH]>>[O]C([O])[CH]

MMP Level 2
1: [O]C([CH])N1C=N[C]=C1[NH]>>[NH]C1=[C]NC=N1
2: [C]C1=[C][N]C=N1>>[C]C1=[C]N=CN1
3: O=P(O)(O)O>>[O]C([CH])OP(=O)(O)O
4: [C]N([CH])C1O[CH][CH]C1O>>[P]OC1O[CH][CH]C1O

MMP Level 3
1: [C]Nc1c([C])ncn1C2O[CH][CH]C2O>>[C]Nc1nc[nH]c1[C]
2: O=C([NH])c1ncn([CH])c1[NH]>>O=C([NH])c1[nH]cnc1[NH]
3: O=P(O)(O)O>>O=P(O)(O)OC1O[CH][CH]C1O
4: [NH]C1=[C]N=CN1C2OC([CH2])C(O)C2O>>O=P(O)(O)OC1OC([CH2])C(O)C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1[nH]c(=O)c2ncn(c2[nH]1)C3OC(CO)C(O)C3O>>O=P(O)(O)OC1OC(CO)C(O)C1O]
2: R:M00001, P:M00004	[O=c1[nH]c(=O)c2ncn(c2[nH]1)C3OC(CO)C(O)C3O>>O=c1[nH]c(=O)c2[nH]cnc2[nH]1, O=c1[nH]c(=O)c2ncn(c2[nH]1)C3OC(CO)C(O)C3O>>O=c1[nH]c(=O)c2[nH]cnc2[nH]1]
3: R:M00002, P:M00003	[O=P(O)(O)O>>O=P(O)(O)OC1OC(CO)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[NH:3][C:4](=[O:5])[C:6]:2:[N:7]:[CH:8]:[N:9](:[C:10]2[NH:11]1)[CH:12]3[O:13][CH:14]([CH2:15][OH:16])[CH:17]([OH:18])[CH:19]3[OH:20].[O:21]=[P:22]([OH:23])([OH:24])[OH:25]>>[O:1]=[C:2]1[NH:3][C:4](=[O:5])[C:6]:2:[NH:7]:[CH:8]:[N:9]:[C:10]2[NH:11]1.[O:21]=[P:22]([OH:23])([OH:24])[O:25][CH:12]1[O:13][CH:14]([CH2:15][OH:16])[CH:17]([OH:18])[CH:19]1[OH:20]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=8, 2=7, 3=6, 4=10, 5=9, 6=12, 7=19, 8=17, 9=14, 10=13, 11=15, 12=16, 13=18, 14=20, 15=11, 16=2, 17=1, 18=3, 19=4, 20=5, 21=23, 22=22, 23=21, 24=24, 25=25}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=11, 4=13, 5=6, 6=7, 7=5, 8=2, 9=1, 10=3, 11=4, 12=14, 13=12, 14=10, 15=22, 16=23, 17=24, 18=20, 19=21, 20=18, 21=19, 22=17, 23=16, 24=15, 25=25}

