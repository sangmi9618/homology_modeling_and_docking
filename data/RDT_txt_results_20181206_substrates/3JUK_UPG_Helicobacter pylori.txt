
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)O[CH]>>O=P(O)(O[CH])OP(=O)(O)O[CH2]:1.0, O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[CH2]:2.0, O=P(O)(O[P])O[CH2]>>O=P(O)(O[P])O[CH2]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>[O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP(=O)(O)OC[CH]>>[O]P(=O)(O)OP(=O)(O)OC[CH]:1.0, [O]P(=O)(O)OP([O])(=O)O:2.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:2.0, [O]P([O])(=O)O>>[O]P([O])(=O)O:1.0, [P]:2.0, [P]O:2.0, [P]O>>[P]O[P]:1.0, [P]O[P]:2.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]P([O])(=O)O:2.0, [P]O[P]:2.0]


ID=Reaction Center at Level: 2 (2)
[O=P(O)(O[P])O[CH2]:2.0, [O]P(=O)(O)OP([O])(=O)O:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]P([O])(=O)O>>[O]P([O])(=O)O
2: [P]O[P]>>[P]O
3: [P]O>>[P]O[P]

MMP Level 2
1: O=P(O)(O[P])O[CH2]>>O=P(O)(O[P])O[CH2]
2: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
3: [O]P(=O)(O)O>>[O]P(=O)(O)OP([O])(=O)O

MMP Level 3
1: [O]P(=O)(O)OP(=O)(O)OC[CH]>>[O]P(=O)(O)OP(=O)(O)OC[CH]
2: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]
3: O=P(O)(O)O[CH]>>O=P(O)(O[CH])OP(=O)(O)O[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2O>>O=P(O)(O)OP(=O)(O)O]
2: R:M00001, P:M00004	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3O)C(O)C2O]
3: R:M00002, P:M00004	[O=P(O)(O)OC1OC(CO)C(O)C(O)C1O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3O)C(O)C2O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][P:22](=[O:23])([OH:24])[OH:25])[CH:26]([OH:27])[CH:28]2[OH:29].[O:30]=[P:31]([OH:32])([OH:33])[O:34][CH:35]1[O:36][CH:37]([CH2:38][OH:39])[CH:40]([OH:41])[CH:42]([OH:43])[CH:44]1[OH:45]>>[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:32][P:31](=[O:30])([OH:33])[O:34][CH:35]3[O:36][CH:37]([CH2:38][OH:39])[CH:40]([OH:41])[CH:42]([OH:43])[CH:44]3[OH:45])[CH:26]([OH:27])[CH:28]2[OH:29].[O:19]=[P:18]([OH:17])([OH:20])[O:21][P:22](=[O:23])([OH:24])[OH:25]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=3, 2=4, 3=5, 4=6, 5=7, 6=8, 7=2, 8=1, 9=9, 10=28, 11=26, 12=11, 13=10, 14=12, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=25, 28=27, 29=29, 30=38, 31=37, 32=40, 33=42, 34=44, 35=35, 36=36, 37=34, 38=31, 39=30, 40=32, 41=33, 42=45, 43=43, 44=41, 45=39}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=39, 2=38, 3=37, 4=40, 5=41, 6=42, 7=43, 8=44, 9=45, 10=3, 11=4, 12=5, 13=6, 14=7, 15=8, 16=2, 17=1, 18=9, 19=35, 20=33, 21=11, 22=10, 23=12, 24=13, 25=14, 26=15, 27=16, 28=17, 29=18, 30=19, 31=20, 32=21, 33=22, 34=31, 35=29, 36=27, 37=24, 38=23, 39=25, 40=26, 41=28, 42=30, 43=32, 44=34, 45=36}

