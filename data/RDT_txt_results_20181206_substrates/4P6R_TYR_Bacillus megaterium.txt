
//
FINGERPRINTS BC

FORMED_CLEAVED
[C=O:1.0, O=O:1.0]

ORDER_CHANGED
[C%C*C@C:1.0, C-O*C=O:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C([CH])C(=O)C=[CH]:1.0, O=O:4.0, O=O>>O:3.0, O=O>>[C]=CC(=O)C(=O)[CH]:1.0, O=O>>[C]=O:1.0, O=O>>[C]C(=O)[CH]:1.0, OC=1C=C[C]=CC1>>O=C1C=[C]C=CC1=O:1.0, Oc1ccc([CH2])cc1>>O=C1C=CC([CH2])=CC1=O:1.0, [CH]:1.0, [CH]C(=[CH])O:2.0, [CH]C(=[CH])O>>[C]C(=O)[CH]:2.0, [CH]C=C(O)C=[CH]:1.0, [CH]C=C(O)C=[CH]>>O=C([CH])C(=O)C=[CH]:2.0, [C]:3.0, [C]=CC(=O)C(=O)[CH]:1.0, [C]=CC=C([CH])O:1.0, [C]=CC=C([CH])O>>[C]=CC(=O)C(=O)[CH]:1.0, [C]=C[CH]:1.0, [C]=C[CH]>>[C]C(=O)[CH]:1.0, [C]=O:2.0, [C]C(=O)[CH]:4.0, [C]O:1.0, [C]O>>[C]=O:1.0, [OH]:1.0, [O]:4.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[O:1.0, [C]:1.0, [O]:3.0]


ID=Reaction Center at Level: 1 (4)
[O:1.0, O=O:2.0, [C]=O:1.0, [C]C(=O)[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=O:2.0, [C]=CC(=O)C(=O)[CH]:1.0, [C]C(=O)[CH]:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH]:1.0, [C]:3.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (5)
[[CH]C(=[CH])O:1.0, [C]=C[CH]:1.0, [C]=O:1.0, [C]C(=O)[CH]:2.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (6)
[O=C([CH])C(=O)C=[CH]:1.0, [CH]C(=[CH])O:1.0, [CH]C=C(O)C=[CH]:1.0, [C]=CC(=O)C(=O)[CH]:1.0, [C]=CC=C([CH])O:1.0, [C]C(=O)[CH]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=O>>O
2: O=O>>[C]=O
3: [C]=C[CH]>>[C]C(=O)[CH]
4: [CH]C(=[CH])O>>[C]C(=O)[CH]
5: [C]O>>[C]=O

MMP Level 2
1: O=O>>O
2: O=O>>[C]C(=O)[CH]
3: [C]=CC=C([CH])O>>[C]=CC(=O)C(=O)[CH]
4: [CH]C=C(O)C=[CH]>>O=C([CH])C(=O)C=[CH]
5: [CH]C(=[CH])O>>[C]C(=O)[CH]

MMP Level 3
1: O=O>>O
2: O=O>>[C]=CC(=O)C(=O)[CH]
3: Oc1ccc([CH2])cc1>>O=C1C=CC([CH2])=CC1=O
4: OC=1C=C[C]=CC1>>O=C1C=[C]C=CC1=O
5: [CH]C=C(O)C=[CH]>>O=C([CH])C(=O)C=[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)Cc1ccc(O)cc1>>O=C1C=CC(=CC1=O)CC(N)C(=O)O, O=C(O)C(N)Cc1ccc(O)cc1>>O=C1C=CC(=CC1=O)CC(N)C(=O)O, O=C(O)C(N)Cc1ccc(O)cc1>>O=C1C=CC(=CC1=O)CC(N)C(=O)O]
2: R:M00002, P:M00003	[O=O>>O=C1C=CC(=CC1=O)CC(N)C(=O)O]
3: R:M00002, P:M00004	[O=O>>O]


//
SELECTED AAM MAPPING
[O:14]=[O:15].[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][C:7]:1:[CH:8]:[CH:9]:[C:10]([OH:11]):[CH:12]:[CH:13]1>>[O:11]=[C:10]1[CH:9]=[CH:8][C:7](=[CH:13][C:12]1=[O:15])[CH2:6][CH:4]([NH2:5])[C:2](=[O:1])[OH:3].[OH2:14]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=8, 2=9, 3=10, 4=12, 5=13, 6=7, 7=6, 8=4, 9=2, 10=1, 11=3, 12=5, 13=11, 14=14, 15=15}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=4, 2=3, 3=2, 4=1, 5=7, 6=8, 7=6, 8=5, 9=9, 10=10, 11=12, 12=13, 13=14, 14=11, 15=15}

