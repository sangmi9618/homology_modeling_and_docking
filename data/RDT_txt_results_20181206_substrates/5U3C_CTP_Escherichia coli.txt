
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C=O:1.0, O-P:2.0]

ORDER_CHANGED
[C%N*C%N:1.0]

//
FINGERPRINTS RC
[N:3.0, N>>[C]N:1.0, N>>[C]N=C(N)C=[CH]:1.0, N>>[N]=C([CH])N:1.0, O=C([CH])[NH]:2.0, O=C([CH])[NH]>>O=P(O)(O)O:1.0, O=C([CH])[NH]>>[N]=C([CH])N:1.0, O=C1[N]C=CC(=O)N1>>O=C1[N]C=CC(=N1)N:1.0, O=P(O)(O)O:3.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O:1.0, O=c1ccn([CH])c(=O)[nH]1>>O=c1nc(N)ccn1[CH]:1.0, [C]:2.0, [C]=O:1.0, [C]=O>>[P]O:1.0, [C]N:1.0, [C]N=C(N)C=[CH]:1.0, [C]N=[C]:1.0, [C]NC(=O)C=[CH]:1.0, [C]NC(=O)C=[CH]>>O=P(O)(O)O:1.0, [C]NC(=O)C=[CH]>>[C]N=C(N)C=[CH]:1.0, [C]N[C]:1.0, [C]N[C]>>[C]N=[C]:1.0, [NH2]:1.0, [NH]:1.0, [N]:1.0, [N]=C([CH])N:2.0, [N]C(=O)N=C([CH])N:1.0, [N]C(=O)NC(=O)[CH]:1.0, [N]C(=O)NC(=O)[CH]>>[N]C(=O)N=C([CH])N:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:2.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (6)
[N:1.0, [C]:2.0, [NH2]:1.0, [OH]:1.0, [O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (9)
[N:1.0, O=C([CH])[NH]:1.0, O=P(O)(O)O:1.0, [C]=O:1.0, [C]N:1.0, [N]=C([CH])N:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (8)
[N:1.0, O=C([CH])[NH]:1.0, O=P(O)(O)O:2.0, O=P(O)(O)O[P]:1.0, [C]N=C(N)C=[CH]:1.0, [C]NC(=O)C=[CH]:1.0, [N]=C([CH])N:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [NH]:1.0, [N]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([CH])[NH]:1.0, [C]N=[C]:1.0, [C]N[C]:1.0, [N]=C([CH])N:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]N=C(N)C=[CH]:1.0, [C]NC(=O)C=[CH]:1.0, [N]C(=O)N=C([CH])N:1.0, [N]C(=O)NC(=O)[CH]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: N>>[C]N
2: [P]O[P]>>[P]O
3: [C]=O>>[P]O
4: O=C([CH])[NH]>>[N]=C([CH])N
5: [O]P(=O)(O)O>>O=P(O)(O)O
6: [C]N[C]>>[C]N=[C]

MMP Level 2
1: N>>[N]=C([CH])N
2: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
3: O=C([CH])[NH]>>O=P(O)(O)O
4: [C]NC(=O)C=[CH]>>[C]N=C(N)C=[CH]
5: O=P(O)(O)O[P]>>O=P(O)(O)O
6: [N]C(=O)NC(=O)[CH]>>[N]C(=O)N=C([CH])N

MMP Level 3
1: N>>[C]N=C(N)C=[CH]
2: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
3: [C]NC(=O)C=[CH]>>O=P(O)(O)O
4: O=C1[N]C=CC(=O)N1>>O=C1[N]C=CC(=N1)N
5: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)O
6: O=c1ccn([CH])c(=O)[nH]1>>O=c1nc(N)ccn1[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2O>>O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2O, O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2O>>O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2O]
2: R:M00001, P:M00006	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2O>>O=P(O)(O)O]
3: R:M00002, P:M00005	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
4: R:M00002, P:M00006	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)O]
5: R:M00003, P:M00004	[N>>O=c1nc(N)ccn1C2OC(COP(=O)(O)OP(=O)(O)OP(=O)(O)O)C(O)C2O]


//
SELECTED AAM MAPPING
[O:32]=[C:33]1[CH:34]=[CH:35][N:36]([C:37](=[O:38])[NH:39]1)[CH:40]2[O:41][CH:42]([CH2:43][O:44][P:45](=[O:46])([OH:47])[O:48][P:49](=[O:50])([OH:51])[O:52][P:53](=[O:54])([OH:55])[OH:56])[CH:57]([OH:58])[CH:59]2[OH:60].[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31].[NH3:61]>>[O:38]=[C:37]1[N:39]=[C:33]([NH2:61])[CH:34]=[CH:35][N:36]1[CH:40]2[O:41][CH:42]([CH2:43][O:44][P:45](=[O:46])([OH:47])[O:48][P:49](=[O:50])([OH:51])[O:52][P:53](=[O:54])([OH:56])[OH:55])[CH:57]([OH:58])[CH:59]2[OH:60].[O:1]=[P:2]([OH:3])([OH:4])[OH:32].[O:7]=[P:6]([OH:8])([OH:5])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=34, 2=35, 3=36, 4=37, 5=38, 6=39, 7=33, 8=32, 9=40, 10=59, 11=57, 12=42, 13=41, 14=43, 15=44, 16=45, 17=46, 18=47, 19=48, 20=49, 21=50, 22=51, 23=52, 24=53, 25=54, 26=55, 27=56, 28=58, 29=60, 30=24, 31=25, 32=26, 33=21, 34=22, 35=23, 36=27, 37=20, 38=19, 39=18, 40=17, 41=28, 42=30, 43=15, 44=16, 45=14, 46=13, 47=10, 48=11, 49=12, 50=9, 51=6, 52=7, 53=8, 54=5, 55=2, 56=1, 57=3, 58=4, 59=31, 60=29, 61=61}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=7, 3=8, 4=2, 5=1, 6=3, 7=4, 8=5, 9=9, 10=28, 11=26, 12=11, 13=10, 14=12, 15=13, 16=14, 17=15, 18=16, 19=17, 20=18, 21=19, 22=20, 23=21, 24=22, 25=23, 26=24, 27=25, 28=27, 29=29, 30=49, 31=50, 32=51, 33=46, 34=47, 35=48, 36=52, 37=45, 38=44, 39=43, 40=42, 41=53, 42=55, 43=40, 44=41, 45=39, 46=38, 47=35, 48=36, 49=37, 50=34, 51=31, 52=30, 53=32, 54=33, 55=56, 56=54, 57=59, 58=58, 59=57, 60=60, 61=61}

