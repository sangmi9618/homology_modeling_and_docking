
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-C:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O=C(O)C(N)CC[CH2]>>[CH2]CCCN:1.0, O=C(O)C(N)C[CH2]:1.0, O=C(O)C(N)C[CH2]>>O=C=O:1.0, O=C(O)C(N)C[CH2]>>[CH2]CCN:1.0, O=C(O)C([CH2])N:1.0, O=C(O)C([CH2])N>>O=C=O:2.0, O=C([CH])O:2.0, O=C([CH])O>>O=C=O:2.0, O=C=O:3.0, [CH2]:1.0, [CH2]CCN:1.0, [CH2]CN:1.0, [CH]:1.0, [C]:2.0, [C]=O:1.0, [C]C([CH2])N:1.0, [C]C([CH2])N>>[CH2]CN:1.0, [C]O:1.0, [C]O>>[C]=O:1.0, [OH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[O=C([CH])O:1.0, [C]C([CH2])N:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C(N)C[CH2]:1.0, O=C(O)C([CH2])N:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([CH])O:1.0, O=C=O:1.0, [C]=O:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (3)
[O=C(O)C([CH2])N:1.0, O=C([CH])O:1.0, O=C=O:2.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH2]:1.0, [CH]:1.0]


ID=Reaction Center at Level: 1 (2)
[[CH2]CN:1.0, [C]C([CH2])N:1.0]


ID=Reaction Center at Level: 2 (2)
[O=C(O)C(N)C[CH2]:1.0, [CH2]CCN:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: O=C([CH])O>>O=C=O
2: [C]C([CH2])N>>[CH2]CN
3: [C]O>>[C]=O

MMP Level 2
1: O=C(O)C([CH2])N>>O=C=O
2: O=C(O)C(N)C[CH2]>>[CH2]CCN
3: O=C([CH])O>>O=C=O

MMP Level 3
1: O=C(O)C(N)C[CH2]>>O=C=O
2: O=C(O)C(N)CC[CH2]>>[CH2]CCCN
3: O=C(O)C([CH2])N>>O=C=O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=C(O)C(N)CCCN=C(N)N>>N(=C(N)N)CCCCN]
2: R:M00001, P:M00003	[O=C(O)C(N)CCCN=C(N)N>>O=C=O, O=C(O)C(N)CCCN=C(N)N>>O=C=O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][CH2:8][N:9]=[C:10]([NH2:11])[NH2:12]>>[O:1]=[C:2]=[O:3].[N:9](=[C:10]([NH2:12])[NH2:11])[CH2:8][CH2:7][CH2:6][CH2:4][NH2:5]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=7, 2=6, 3=4, 4=2, 5=1, 6=3, 7=5, 8=8, 9=9, 10=10, 11=11, 12=12}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=7, 2=6, 3=5, 4=1, 5=2, 6=3, 7=4, 8=8, 9=9, 10=11, 11=10, 12=12}

