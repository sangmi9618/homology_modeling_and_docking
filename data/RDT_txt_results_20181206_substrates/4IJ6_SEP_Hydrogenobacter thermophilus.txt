
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O:3.0, O=P(O)(O)O:3.0, O=P(O)(O)OC[CH]:1.0, O=P(O)(O)OC[CH]>>O=P(O)(O)O:1.0, O=P(O)(O)OC[CH]>>[CH]CO:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[CH2]>>O=P(O)(O)O:1.0, O>>O=P(O)(O)O:2.0, O>>[P]O:1.0, O[CH2]:1.0, [CH]CO:1.0, [C]C(N)COP(=O)(O)O>>[C]C(N)CO:1.0, [OH]:2.0, [O]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[CH2]>>O[CH2]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=P(O)(O)O:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=P(O)(O)O:2.0, O=P(O)(O)OC[CH]:1.0, O=P(O)(O)O[CH2]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [O]P(=O)(O)O>>O=P(O)(O)O
2: [P]O[CH2]>>O[CH2]
3: O>>[P]O

MMP Level 2
1: O=P(O)(O)O[CH2]>>O=P(O)(O)O
2: O=P(O)(O)OC[CH]>>[CH]CO
3: O>>O=P(O)(O)O

MMP Level 3
1: O=P(O)(O)OC[CH]>>O=P(O)(O)O
2: [C]C(N)COP(=O)(O)O>>[C]C(N)CO
3: O>>O=P(O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)COP(=O)(O)O>>O=C(O)C(N)CO]
2: R:M00001, P:M00004	[O=C(O)C(N)COP(=O)(O)O>>O=P(O)(O)O]
3: R:M00002, P:M00004	[O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][O:7][P:8](=[O:9])([OH:10])[OH:11].[OH2:12]>>[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][OH:7].[O:9]=[P:8]([OH:11])([OH:12])[OH:10]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=6, 2=4, 3=2, 4=1, 5=3, 6=5, 7=7, 8=8, 9=9, 10=10, 11=11, 12=12}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=6, 2=4, 3=2, 4=1, 5=3, 6=5, 7=7, 8=10, 9=9, 10=8, 11=11, 12=12}

