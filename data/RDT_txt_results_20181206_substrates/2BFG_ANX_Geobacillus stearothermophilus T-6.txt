
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O>>[CH]C(O)C(O)O[CH2]:1.0, O>>[CH]O:1.0, O>>[O]C([CH])O:1.0, [CH]:2.0, [CH]C(O)C(O)O[CH2]:1.0, [CH]O:1.0, [C]=C([CH])O:1.0, [C]=C([CH])OC([O])[CH]:1.0, [C]=C([CH])OC([O])[CH]>>[C]=C([CH])O:1.0, [C]=C([CH])OC1OC[CH]C(O)C1O>>OC1OC[CH]C(O)C1O:1.0, [C]=CC(OC(O[CH2])C([CH])O)=C([N+])[CH]>>[C]=CC(O)=C([N+])[CH]:1.0, [C]O:1.0, [C]OC(O[CH2])C([CH])O:1.0, [C]OC(O[CH2])C([CH])O>>[CH]C(O)C(O)O[CH2]:1.0, [C]O[CH]:1.0, [C]O[CH]>>[C]O:1.0, [OH]:2.0, [O]:1.0, [O]C([CH])O:2.0, [O]C([O])[CH]:1.0, [O]C([O])[CH]>>[O]C([CH])O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [CH]:2.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, [CH]O:1.0, [C]O[CH]:1.0, [O]C([CH])O:1.0, [O]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, [CH]C(O)C(O)O[CH2]:1.0, [C]=C([CH])OC([O])[CH]:1.0, [C]OC(O[CH2])C([CH])O:1.0, [O]C([CH])O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]C([CH])O:1.0, [O]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[CH]C(O)C(O)O[CH2]:1.0, [C]OC(O[CH2])C([CH])O:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [O]C([O])[CH]>>[O]C([CH])O
2: O>>[CH]O
3: [C]O[CH]>>[C]O

MMP Level 2
1: [C]OC(O[CH2])C([CH])O>>[CH]C(O)C(O)O[CH2]
2: O>>[O]C([CH])O
3: [C]=C([CH])OC([O])[CH]>>[C]=C([CH])O

MMP Level 3
1: [C]=C([CH])OC1OC[CH]C(O)C1O>>OC1OC[CH]C(O)C1O
2: O>>[CH]C(O)C(O)O[CH2]
3: [C]=CC(OC(O[CH2])C([CH])O)=C([N+])[CH]>>[C]=CC(O)=C([N+])[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[[O-][N+](=O)c1ccc(c(OC2OCC(O)C(O)C2O)c1)[N+]([O-])=O>>[O-][N+](=O)c1ccc(c(O)c1)[N+]([O-])=O]
2: R:M00001, P:M00004	[[O-][N+](=O)c1ccc(c(OC2OCC(O)C(O)C2O)c1)[N+]([O-])=O>>OC1OCC(O)C(O)C1O]
3: R:M00002, P:M00004	[O>>OC1OCC(O)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[N+:2]([O-:3])[C:4]:1:[CH:5]:[CH:6]:[C:7](:[C:8]([O:9][CH:10]2[O:11][CH2:12][CH:13]([OH:14])[CH:15]([OH:16])[CH:17]2[OH:18]):[CH:19]1)[N+:20](=[O:21])[O-:22].[OH2:23]>>[O:1]=[N+:2]([O-:3])[C:4]:1:[CH:5]:[CH:6]:[C:7](:[C:8]([OH:9]):[CH:19]1)[N+:20](=[O:21])[O-:22].[OH:23][CH:10]1[O:11][CH2:12][CH:13]([OH:14])[CH:15]([OH:16])[CH:17]1[OH:18]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=12, 2=13, 3=15, 4=17, 5=10, 6=11, 7=9, 8=8, 9=7, 10=6, 11=5, 12=4, 13=19, 14=2, 15=1, 16=3, 17=20, 18=21, 19=22, 20=18, 21=16, 22=14, 23=23}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=5, 2=6, 3=7, 4=8, 5=10, 6=4, 7=2, 8=1, 9=3, 10=9, 11=11, 12=12, 13=13, 14=17, 15=18, 16=20, 17=22, 18=15, 19=16, 20=14, 21=23, 22=21, 23=19}

