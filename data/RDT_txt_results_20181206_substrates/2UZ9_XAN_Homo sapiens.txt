
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C=O:1.0]

ORDER_CHANGED
[C%N*C%N:1.0]

//
FINGERPRINTS RC
[N:3.0, O:3.0, O=C([NH])[NH]:2.0, O=C1N=C(N)N[C]=C1[NH]>>O=C1N[C]=C([NH])C(=O)N1:1.0, O>>O=C([NH])[NH]:1.0, O>>[C]=O:1.0, O>>[C]NC(=O)N[C]:1.0, [C]:2.0, [C]=O:1.0, [C]C(=O)N=C([NH])N:1.0, [C]C(=O)N=C([NH])N>>[C]C(=O)NC(=O)[NH]:1.0, [C]C(=O)NC(=O)[NH]:1.0, [C]N:1.0, [C]N=C(N)N[C]:1.0, [C]N=C(N)N[C]>>N:1.0, [C]N=C(N)N[C]>>[C]NC(=O)N[C]:1.0, [C]N=[C]:1.0, [C]N=[C]>>[C]N[C]:1.0, [C]N>>N:1.0, [C]NC(=O)N[C]:1.0, [C]N[C]:1.0, [NH2]:1.0, [NH]:1.0, [N]:1.0, [N]=C([NH])N:2.0, [N]=C([NH])N>>N:1.0, [N]=C([NH])N>>O=C([NH])[NH]:1.0, [N]C1=[C]C(=O)N=C(N)N1>>[N]C1=[C]C(=O)NC(=O)N1:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[N:1.0, O:1.0, [C]:2.0, [NH2]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (6)
[N:1.0, O:1.0, O=C([NH])[NH]:1.0, [C]=O:1.0, [C]N:1.0, [N]=C([NH])N:1.0]


ID=Reaction Center at Level: 2 (6)
[N:1.0, O:1.0, O=C([NH])[NH]:1.0, [C]N=C(N)N[C]:1.0, [C]NC(=O)N[C]:1.0, [N]=C([NH])N:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [NH]:1.0, [N]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([NH])[NH]:1.0, [C]N=[C]:1.0, [C]N[C]:1.0, [N]=C([NH])N:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]C(=O)N=C([NH])N:1.0, [C]C(=O)NC(=O)[NH]:1.0, [C]N=C(N)N[C]:1.0, [C]NC(=O)N[C]:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [N]=C([NH])N>>O=C([NH])[NH]
2: O>>[C]=O
3: [C]N>>N
4: [C]N=[C]>>[C]N[C]

MMP Level 2
1: [C]N=C(N)N[C]>>[C]NC(=O)N[C]
2: O>>O=C([NH])[NH]
3: [N]=C([NH])N>>N
4: [C]C(=O)N=C([NH])N>>[C]C(=O)NC(=O)[NH]

MMP Level 3
1: [N]C1=[C]C(=O)N=C(N)N1>>[N]C1=[C]C(=O)NC(=O)N1
2: O>>[C]NC(=O)N[C]
3: [C]N=C(N)N[C]>>N
4: O=C1N=C(N)N[C]=C1[NH]>>O=C1N[C]=C([NH])C(=O)N1


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1nc(N)[nH]c2nc[nH]c12>>O=c1[nH]c(=O)c2[nH]cnc2[nH]1, O=c1nc(N)[nH]c2nc[nH]c12>>O=c1[nH]c(=O)c2[nH]cnc2[nH]1]
2: R:M00001, P:M00004	[O=c1nc(N)[nH]c2nc[nH]c12>>N]
3: R:M00002, P:M00003	[O>>O=c1[nH]c(=O)c2[nH]cnc2[nH]1]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]:2:[N:8]:[CH:9]:[NH:10]:[C:11]12.[OH2:12]>>[O:12]=[C:4]1[NH:3][C:2](=[O:1])[C:11]:2:[NH:10]:[CH:9]:[N:8]:[C:7]2[NH:6]1.[NH3:5]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=8, 3=7, 4=11, 5=10, 6=2, 7=1, 8=3, 9=4, 10=6, 11=5, 12=12}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=8, 2=9, 3=10, 4=6, 5=7, 6=4, 7=5, 8=3, 9=2, 10=1, 11=11, 12=12}

