
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-S:1.0]

//
FINGERPRINTS RC
[S(C)C[CH2]:1.0, S([CH2])C:2.0, SC[CH2]:1.0, SC[CH2]>>S(C)C[CH2]:1.0, S[CH2]:1.0, S[CH2]>>S([CH2])C:1.0, [CH3]:2.0, [CH]CCS>>[CH]CCSC:1.0, [C]C(=[C])N(C)C([CH2])[CH2]:1.0, [C]C(=[C])N(C)C([CH2])[CH2]>>S(C)C[CH2]:1.0, [C]C(=[C])N(C)C([CH2])[CH2]>>[C]C(=[C])NC([CH2])[CH2]:1.0, [C]C(=[C])NC([CH2])[CH2]:1.0, [C]N([CH])C:2.0, [C]N([CH])C>>S([CH2])C:1.0, [C]N([CH])C>>[C]N[CH]:1.0, [C]N[CH]:1.0, [NH]:1.0, [N]:1.0, [N]C:1.0, [N]C(=O)C1=C([NH])NCC(N1C)C[NH]>>[N]C(=O)C=1NC(CNC1[NH])C[NH]:1.0, [N]C>>[S]C:1.0, [SH]:1.0, [S]:1.0, [S]C:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH3]:2.0, [N]:1.0, [S]:1.0]


ID=Reaction Center at Level: 1 (4)
[S([CH2])C:1.0, [C]N([CH])C:1.0, [N]C:1.0, [S]C:1.0]


ID=Reaction Center at Level: 2 (4)
[S(C)C[CH2]:1.0, S([CH2])C:1.0, [C]C(=[C])N(C)C([CH2])[CH2]:1.0, [C]N([CH])C:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [N]C>>[S]C
2: [C]N([CH])C>>[C]N[CH]
3: S[CH2]>>S([CH2])C

MMP Level 2
1: [C]N([CH])C>>S([CH2])C
2: [C]C(=[C])N(C)C([CH2])[CH2]>>[C]C(=[C])NC([CH2])[CH2]
3: SC[CH2]>>S(C)C[CH2]

MMP Level 3
1: [C]C(=[C])N(C)C([CH2])[CH2]>>S(C)C[CH2]
2: [N]C(=O)C1=C([NH])NCC(N1C)C[NH]>>[N]C(=O)C=1NC(CNC1[NH])C[NH]
3: [CH]CCS>>[CH]CCSC


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1nc(N)[nH]c2NCC(N(c12)C)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)NC(C(=O)O)CCC(=O)NC(C(=O)O)CCC(=O)O>>O=c1nc(N)[nH]c2NCC(Nc12)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)NC(C(=O)O)CCC(=O)NC(C(=O)O)CCC(=O)O]
2: R:M00001, P:M00004	[O=c1nc(N)[nH]c2NCC(N(c12)C)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)NC(C(=O)O)CCC(=O)NC(C(=O)O)CCC(=O)O>>O=C(O)C(N)CCSC]
3: R:M00002, P:M00004	[O=C(O)C(N)CCS>>O=C(O)C(N)CCSC]


//
SELECTED AAM MAPPING
[O:50]=[C:49]([OH:51])[CH2:48][CH2:47][CH:43]([NH:42][C:40](=[O:41])[CH2:39][CH2:38][CH:34]([NH:33][C:31](=[O:32])[CH2:30][CH2:29][CH:25]([NH:24][C:22](=[O:23])[C:19]:1:[CH:18]:[CH:17]:[C:16](:[CH:21]:[CH:20]1)[NH:15][CH2:14][CH:10]2[N:11]([C:12]=3[C:2](=[O:1])[N:3]=[C:4]([NH2:5])[NH:6][C:7]3[NH:8][CH2:9]2)[CH3:13])[C:26](=[O:27])[OH:28])[C:35](=[O:36])[OH:37])[C:44](=[O:45])[OH:46].[O:52]=[C:53]([OH:54])[CH:55]([NH2:56])[CH2:57][CH2:58][SH:59]>>[O:50]=[C:49]([OH:51])[CH2:48][CH2:47][CH:43]([NH:42][C:40](=[O:41])[CH2:39][CH2:38][CH:34]([NH:33][C:31](=[O:32])[CH2:30][CH2:29][CH:25]([NH:24][C:22](=[O:23])[C:19]:1:[CH:18]:[CH:17]:[C:16](:[CH:21]:[CH:20]1)[NH:15][CH2:14][CH:10]2[NH:11][C:12]=3[C:2](=[O:1])[N:3]=[C:4]([NH2:5])[NH:6][C:7]3[NH:8][CH2:9]2)[C:26](=[O:27])[OH:28])[C:35](=[O:36])[OH:37])[C:44](=[O:45])[OH:46].[O:52]=[C:53]([OH:54])[CH:55]([NH2:56])[CH2:57][CH2:58][S:59][CH3:13]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=13, 2=11, 3=10, 4=9, 5=8, 6=7, 7=12, 8=2, 9=1, 10=3, 11=4, 12=6, 13=5, 14=14, 15=15, 16=16, 17=17, 18=18, 19=19, 20=20, 21=21, 22=22, 23=23, 24=24, 25=25, 26=29, 27=30, 28=31, 29=32, 30=33, 31=34, 32=38, 33=39, 34=40, 35=41, 36=42, 37=43, 38=47, 39=48, 40=49, 41=50, 42=51, 43=44, 44=45, 45=46, 46=35, 47=36, 48=37, 49=26, 50=27, 51=28, 52=57, 53=58, 54=59, 55=55, 56=53, 57=52, 58=54, 59=56}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=9, 2=10, 3=11, 4=12, 5=7, 6=8, 7=6, 8=4, 9=3, 10=2, 11=1, 12=5, 13=13, 14=14, 15=15, 16=16, 17=17, 18=18, 19=19, 20=20, 21=21, 22=22, 23=23, 24=24, 25=28, 26=29, 27=30, 28=31, 29=32, 30=33, 31=37, 32=38, 33=39, 34=40, 35=41, 36=42, 37=46, 38=47, 39=48, 40=49, 41=50, 42=43, 43=44, 44=45, 45=34, 46=35, 47=36, 48=25, 49=26, 50=27, 51=59, 52=58, 53=57, 54=56, 55=54, 56=52, 57=51, 58=53, 59=55}

