
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:2.0, C=O:1.0, C@N:2.0]

//
FINGERPRINTS RC
[O=c1nccc(N)[nH]1>>[N]C1=[C]N=CN=C1N:1.0, O=c1nccc(N)[nH]1>>[N]C=1[C]=C(N=CN1)N:1.0, O=c1nccc(N)[nH]1>>[O]C([CH2])n1cnc2c(ncnc21)N:1.0, [CH]:5.0, [C]:3.0, [C]1N=CC=C(N)N1>>[CH]n1cnc2c(ncnc21)N:1.0, [C]=C([N])[N]:1.0, [C]=C[CH]:1.0, [C]=C[CH]>>[C]C(=[C])[N]:1.0, [C]=N[CH]:1.0, [C]=O:1.0, [C]C(=NC=[N])N:1.0, [C]C(=[C])[N]:1.0, [C]C1=[C]N=CN1:1.0, [C]C1=[C][N]C=N1:1.0, [C]C1=[C][N]C=N1>>[C]C1=[C]N=CN1:1.0, [C]N([CH])C1O[CH][CH]C1:2.0, [C]N([CH])C1O[CH][CH]C1>>[C]N([CH])C1O[CH][CH]C1:1.0, [C]N([CH])[CH]:1.0, [C]N([CH])[CH]>>[C]N=[CH]:1.0, [C]N=CC=[C]:1.0, [C]N=CC=[C]>>[C]c1ncn([CH])c1N=[CH]:1.0, [C]N=CN=[C]:1.0, [C]N=[CH]:2.0, [C]N=[CH]>>[C]N[CH]:1.0, [C]NC(=O)N=[CH]:1.0, [C]NC(=O)N=[CH]>>[C]N=CN=[C]:1.0, [C]N[CH]:1.0, [C]N[C]:1.0, [C]N[C]>>[C]=N[CH]:1.0, [C]c1ncn([CH])c1N=[CH]:1.0, [C]c1ncn(c1N[CH])C2O[CH][CH]C2>>[C]c1[nH]cnc1N[CH]:1.0, [NH]:2.0, [NH]C1=[C]N=CN1C2OC([CH2])C(O)C2>>[N]C1=[C]N=CN1C2OC([CH2])C(O)C2:1.0, [NH]C1=[C]NC=N1:1.0, [N]:4.0, [N]=CC=C([NH])N:1.0, [N]=CC=C([NH])N>>[N]C=1[N]C=NC1C(=[N])N:1.0, [N]=C[CH]:1.0, [N]=C[CH]>>[C]=C([N])[N]:1.0, [N]C(=O)NC(=[CH])N:1.0, [N]C(=O)NC(=[CH])N>>[C]C(=NC=[N])N:1.0, [N]C(=O)[NH]:2.0, [N]C(=O)[NH]>>[N]C=[N]:1.0, [N]C(=O)c1ncn([CH])c1[NH]>>[N]C(=O)c1[nH]cnc1[NH]:1.0, [N]C([O])[CH2]:2.0, [N]C([O])[CH2]>>[N]C([O])[CH2]:1.0, [N]C=1[N]C=NC1C(=[N])N:1.0, [N]C=[N]:1.0, [O]:1.0, [O]C([CH2])N1C=N[C]=C1[NH]:1.0, [O]C([CH2])N1C=N[C]=C1[NH]>>[NH]C1=[C]NC=N1:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[CH]:2.0, [C]:3.0, [N]:4.0, [O]:1.0]


ID=Reaction Center at Level: 1 (7)
[[C]=C([N])[N]:1.0, [C]=O:1.0, [C]C(=[C])[N]:1.0, [C]N([CH])[CH]:3.0, [C]N=[CH]:1.0, [N]C(=O)[NH]:1.0, [N]C([O])[CH2]:2.0]


ID=Reaction Center at Level: 2 (8)
[[C]C1=[C][N]C=N1:1.0, [C]N([CH])C1O[CH][CH]C1:2.0, [C]NC(=O)N=[CH]:1.0, [C]c1ncn([CH])c1N=[CH]:1.0, [N]C(=O)[NH]:1.0, [N]C1=[C]N=CN1C([O])[CH2]:2.0, [N]C=1[N]C=NC1C(=[N])N:1.0, [O]C([CH2])N1C=N[C]=C1[NH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]=C[CH]>>[C]C(=[C])[N]
2: [N]C([O])[CH2]>>[N]C([O])[CH2]
3: [C]N[C]>>[C]=N[CH]
4: [C]N([CH])[CH]>>[C]N=[CH]
5: [C]N=[CH]>>[C]N[CH]
6: [N]=C[CH]>>[C]=C([N])[N]
7: [N]C(=O)[NH]>>[N]C=[N]

MMP Level 2
1: [N]=CC=C([NH])N>>[N]C=1[N]C=NC1C(=[N])N
2: [C]N([CH])C1O[CH][CH]C1>>[C]N([CH])C1O[CH][CH]C1
3: [N]C(=O)NC(=[CH])N>>[C]C(=NC=[N])N
4: [O]C([CH2])N1C=N[C]=C1[NH]>>[NH]C1=[C]NC=N1
5: [C]C1=[C][N]C=N1>>[C]C1=[C]N=CN1
6: [C]N=CC=[C]>>[C]c1ncn([CH])c1N=[CH]
7: [C]NC(=O)N=[CH]>>[C]N=CN=[C]

MMP Level 3
1: [C]1N=CC=C(N)N1>>[CH]n1cnc2c(ncnc21)N
2: [NH]C1=[C]N=CN1C2OC([CH2])C(O)C2>>[N]C1=[C]N=CN1C2OC([CH2])C(O)C2
3: O=c1nccc(N)[nH]1>>[N]C1=[C]N=CN=C1N
4: [C]c1ncn(c1N[CH])C2O[CH][CH]C2>>[C]c1[nH]cnc1N[CH]
5: [N]C(=O)c1ncn([CH])c1[NH]>>[N]C(=O)c1[nH]cnc1[NH]
6: O=c1nccc(N)[nH]1>>[O]C([CH2])n1cnc2c(ncnc21)N
7: O=c1nccc(N)[nH]1>>[N]C=1[C]=C(N=CN1)N


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00004	[O=c1nccc(N)[nH]1>>OCC1OC(n2cnc3c(ncnc32)N)CC1O, O=c1nccc(N)[nH]1>>OCC1OC(n2cnc3c(ncnc32)N)CC1O, O=c1nccc(N)[nH]1>>OCC1OC(n2cnc3c(ncnc32)N)CC1O, O=c1nccc(N)[nH]1>>OCC1OC(n2cnc3c(ncnc32)N)CC1O]
2: R:M00002, P:M00003	[O=c1nc[nH]c2c1ncn2C3OC(CO)C(O)C3>>O=c1nc[nH]c2nc[nH]c12, O=c1nc[nH]c2c1ncn2C3OC(CO)C(O)C3>>O=c1nc[nH]c2nc[nH]c12]
3: R:M00002, P:M00004	[O=c1nc[nH]c2c1ncn2C3OC(CO)C(O)C3>>OCC1OC(n2cnc3c(ncnc32)N)CC1O]


//
SELECTED AAM MAPPING
[O:26]=[C:19]1[N:20]=[CH:21][CH:22]=[C:23]([NH2:24])[NH:25]1.[O:1]=[C:2]1[N:3]=[CH:4][NH:5][C:6]:2:[C:7]1:[N:8]:[CH:9]:[N:10]2[CH:11]3[O:12][CH:13]([CH2:14][OH:15])[CH:16]([OH:17])[CH2:18]3>>[O:1]=[C:2]1[N:3]=[CH:4][NH:5][C:6]:2:[N:10]:[CH:9]:[NH:8]:[C:7]12.[OH:15][CH2:14][CH:13]1[O:12][CH:11]([N:27]:2:[CH:28]:[N:29]:[C:22]:3:[C:23](:[N:25]:[CH:19]:[N:20]:[C:21]32)[NH2:24])[CH2:18][CH:16]1[OH:17]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=23, 2=24, 3=26, 4=20, 5=19, 6=21, 7=22, 8=25, 9=18, 10=16, 11=13, 12=12, 13=11, 14=10, 15=9, 16=8, 17=7, 18=6, 19=5, 20=4, 21=3, 22=2, 23=1, 24=14, 25=15, 26=17}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=26, 2=25, 3=24, 4=28, 5=27, 6=20, 7=19, 8=21, 9=22, 10=23, 11=16, 12=17, 13=3, 14=4, 15=5, 16=6, 17=7, 18=8, 19=9, 20=14, 21=13, 22=12, 23=11, 24=10, 25=15, 26=2, 27=1, 28=18}

