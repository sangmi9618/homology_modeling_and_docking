
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%C:1.0, C%N:2.0, C%O:1.0, C-C:1.0]

ORDER_CHANGED
[C#N*C-N:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[N#Cc1cc(=O)[nH]c(=O)n1C([O])[CH]>>[O]C([CH])n1ccc(=O)[nH]c1=O:1.0, [CH]:2.0, [C]:2.0, [C]#N:1.0, [C]#N>>[C]N([C])[CH]:1.0, [C]C#N:2.0, [C]C#N>>[N]C([O])[CH]:1.0, [C]C#N>>[O]C([CH])N(C(=O)[NH])C(=[CH])O:1.0, [C]C([N])=[CH]:1.0, [C]C([N])=[CH]>>[N]C=[CH]:1.0, [C]C=C(C#N)N([C])[CH]:1.0, [C]C=C(C#N)N([C])[CH]>>O=C([NH])N(C(=[CH])O)C1OC([CH2])C(O)C1O:1.0, [C]C=C(C#N)N([C])[CH]>>[C]C=CN([C])[CH]:1.0, [C]C=CN([C])[CH]:1.0, [C]N([C])C1O[CH][CH]C1O:1.0, [C]N([C])[CH]:1.0, [N]:2.0, [N]C(=[CH])C#N:1.0, [N]C(=[CH])C#N>>O=C1N[C]C=C(O)N1C2O[CH][CH]C2O:1.0, [N]C(=[CH])C#N>>[C]N([C])C1O[CH][CH]C1O:1.0, [N]C([O])[CH]:1.0, [N]C=[CH]:1.0, [O]C([CH])N(C(=O)[NH])C(=[CH])O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[[CH]:3.0, [C]:4.0, [N]:2.0, [O]:1.0]


ID=Reaction Center at Level: 1 (8)
[[CH]C([CH])O:1.0, [CH]O[CH]:1.0, [C]C#N:1.0, [C]C([N])=[CH]:1.0, [C]N([C])[CH]:2.0, [N]C(=O)[NH]:1.0, [N]C(=[CH])O:1.0, [N]C([O])[CH]:2.0]


ID=Reaction Center at Level: 2 (8)
[[C]C=C(C#N)N([C])[CH]:1.0, [C]C=C(O)N([C])[CH]:1.0, [C]N([C])C1O[CH][CH]C1O:2.0, [C]NC(=O)N([C])[CH]:1.0, [N]C(=[CH])C#N:1.0, [N]C1OC([CH2])[CH][CH]1:1.0, [N]C1O[CH]C(O)C1O:1.0, [O]C([CH])N(C(=O)[NH])C(=[CH])O:2.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (3)
[[CH]:1.0, [C]:1.0, [N]:2.0]


ID=Reaction Center at Level: 1 (4)
[[C]#N:1.0, [C]C#N:1.0, [C]N([C])[CH]:1.0, [N]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]C#N:1.0, [C]N([C])C1O[CH][CH]C1O:1.0, [N]C(=[CH])C#N:1.0, [O]C([CH])N(C(=O)[NH])C(=[CH])O:1.0]


Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:1.0, [C]:1.0]


ID=Reaction Center at Level: 1 (2)
[[C]C#N:1.0, [N]C([O])[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[[C]N([C])C1O[CH][CH]C1O:1.0, [N]C(=[CH])C#N:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]#N>>[C]N([C])[CH]
2: [C]C([N])=[CH]>>[N]C=[CH]
3: [C]C#N>>[N]C([O])[CH]

MMP Level 2
1: [C]C#N>>[O]C([CH])N(C(=O)[NH])C(=[CH])O
2: [C]C=C(C#N)N([C])[CH]>>[C]C=CN([C])[CH]
3: [N]C(=[CH])C#N>>[C]N([C])C1O[CH][CH]C1O

MMP Level 3
1: [N]C(=[CH])C#N>>O=C1N[C]C=C(O)N1C2O[CH][CH]C2O
2: N#Cc1cc(=O)[nH]c(=O)n1C([O])[CH]>>[O]C([CH])n1ccc(=O)[nH]c1=O
3: [C]C=C(C#N)N([C])[CH]>>O=C([NH])N(C(=[CH])O)C1OC([CH2])C(O)C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[N#Cc1cc(=O)[nH]c(=O)n1C2OC(COP(=O)(O)O)C(O)C2O>>O=c1cc(O)n(c(=O)[nH]1)C2OC(COP(=O)(O)O)C(O)C2O, N#Cc1cc(=O)[nH]c(=O)n1C2OC(COP(=O)(O)O)C(O)C2O>>O=c1cc(O)n(c(=O)[nH]1)C2OC(COP(=O)(O)O)C(O)C2O]
2: R:M00001, P:M00003	[N#Cc1cc(=O)[nH]c(=O)n1C2OC(COP(=O)(O)O)C(O)C2O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)O)C(O)C2O]


//
SELECTED AAM MAPPING
[N:1]#[C:2][C:3]1=[CH:4][C:5](=[O:6])[NH:7][C:8](=[O:9])[N:10]1[CH:11]2[O:12][CH:13]([CH2:14][O:15][P:16](=[O:17])([OH:18])[OH:19])[CH:20]([OH:21])[CH:22]2[OH:23]>>[O:6]=[C:5]1[CH:4]=[CH:3][N:10]([C:8](=[O:9])[NH:7]1)[CH:11]2[O:12][CH:13]([CH2:14][O:15][P:16](=[O:17])([OH:18])[OH:19])[CH:20]([OH:21])[CH:22]2[OH:23].[O:24]=[C:25]1[CH:26]=[C:27]([OH:28])[N:1]([C:29](=[O:30])[NH:31]1)[CH:2]2[O:32][CH:33]([CH2:34][O:35][P:36](=[O:37])([OH:38])[OH:39])[CH:40]([OH:41])[CH:42]2[OH:43]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=4, 2=3, 3=10, 4=8, 5=9, 6=7, 7=5, 8=6, 9=11, 10=22, 11=20, 12=13, 13=12, 14=14, 15=15, 16=16, 17=17, 18=18, 19=19, 20=21, 21=23, 22=2, 23=1}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=3, 2=4, 3=6, 4=7, 5=8, 6=9, 7=2, 8=1, 9=10, 10=21, 11=19, 12=12, 13=11, 14=13, 15=14, 16=15, 17=16, 18=17, 19=18, 20=20, 21=22, 22=5, 23=25, 24=26, 25=27, 26=28, 27=29, 28=30, 29=24, 30=23, 31=31, 32=42, 33=40, 34=33, 35=32, 36=34, 37=35, 38=36, 39=37, 40=38, 41=39, 42=41, 43=43}

