
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0, C-S:1.0]

//
FINGERPRINTS RC
[S([CH2])[CH2]:1.0, [CH2][S+]([CH2])C:2.0, [CH2][S+]([CH2])C>>S([CH2])[CH2]:1.0, [CH2][S+]([CH2])C>>[C]OC:1.0, [CH3]:2.0, [CH]CSC[CH2]:1.0, [CH]C[S+](C)C[CH2]:1.0, [CH]C[S+](C)C[CH2]>>[CH]CSC[CH2]:1.0, [CH]C[S+](C)C[CH2]>>[C]C(=[CH])OC:1.0, [C]C(=[CH])O:1.0, [C]C(=[CH])O>>[C]C(=[CH])OC:1.0, [C]C(=[CH])OC:1.0, [C]C=C(O)C(=[C])O>>[C]C=C(OC)C(=[C])O:1.0, [C]O:1.0, [C]O>>[C]OC:1.0, [C]OC:2.0, [OH]:1.0, [O]:1.0, [O]C:1.0, [O]C([CH])C[S+](C)CC[CH]>>[O]C([CH])CSCC[CH]:1.0, [S+]:1.0, [S+]C:1.0, [S+]C>>[O]C:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH3]:2.0, [O]:1.0, [S+]:1.0]


ID=Reaction Center at Level: 1 (4)
[[CH2][S+]([CH2])C:1.0, [C]OC:1.0, [O]C:1.0, [S+]C:1.0]


ID=Reaction Center at Level: 2 (4)
[[CH2][S+]([CH2])C:1.0, [CH]C[S+](C)C[CH2]:1.0, [C]C(=[CH])OC:1.0, [C]OC:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]O>>[C]OC
2: [S+]C>>[O]C
3: [CH2][S+]([CH2])C>>S([CH2])[CH2]

MMP Level 2
1: [C]C(=[CH])O>>[C]C(=[CH])OC
2: [CH2][S+]([CH2])C>>[C]OC
3: [CH]C[S+](C)C[CH2]>>[CH]CSC[CH2]

MMP Level 3
1: [C]C=C(O)C(=[C])O>>[C]C=C(OC)C(=[C])O
2: [CH]C[S+](C)C[CH2]>>[C]C(=[CH])OC
3: [O]C([CH])C[S+](C)CC[CH]>>[O]C([CH])CSCC[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[[O-][N+](=O)c1cc(O)c(O)c(c1)[N+]([O-])=O>>[O-][N+](=O)c1cc(OC)c(O)c(c1)[N+]([O-])=O]
2: R:M00002, P:M00003	[O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>[O-][N+](=O)c1cc(OC)c(O)c(c1)[N+]([O-])=O]
3: R:M00002, P:M00004	[O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(O)C(N)CCSCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][S+:8]([CH3:9])[CH2:10][CH:11]1[O:12][CH:13]([N:14]:2:[CH:15]:[N:16]:[C:17]:3:[C:18](:[N:19]:[CH:20]:[N:21]:[C:22]32)[NH2:23])[CH:24]([OH:25])[CH:26]1[OH:27].[O:28]=[N+:29]([O-:30])[C:31]:1:[CH:32]:[C:33]([OH:34]):[C:35]([OH:36]):[C:37](:[CH:38]1)[N+:39](=[O:40])[O-:41]>>[O:1]=[C:2]([OH:3])[CH:4]([NH2:5])[CH2:6][CH2:7][S:8][CH2:10][CH:11]1[O:12][CH:13]([N:14]:2:[CH:15]:[N:16]:[C:17]:3:[C:18](:[N:19]:[CH:20]:[N:21]:[C:22]32)[NH2:23])[CH:24]([OH:25])[CH:26]1[OH:27].[O:28]=[N+:29]([O-:30])[C:31]:1:[CH:32]:[C:33]([O:34][CH3:9]):[C:35]([OH:36]):[C:37](:[CH:38]1)[N+:39](=[O:40])[O-:41]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=38, 2=31, 3=32, 4=33, 5=35, 6=37, 7=39, 8=40, 9=41, 10=36, 11=34, 12=29, 13=28, 14=30, 15=9, 16=8, 17=7, 18=6, 19=4, 20=2, 21=1, 22=3, 23=5, 24=10, 25=11, 26=26, 27=24, 28=13, 29=12, 30=14, 31=15, 32=16, 33=17, 34=22, 35=21, 36=20, 37=19, 38=18, 39=23, 40=25, 41=27}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=34, 2=33, 3=32, 4=31, 5=30, 6=38, 7=37, 8=35, 9=36, 10=39, 11=40, 12=41, 13=28, 14=27, 15=29, 16=19, 17=20, 18=21, 19=16, 20=17, 21=18, 22=22, 23=15, 24=14, 25=13, 26=12, 27=23, 28=25, 29=10, 30=11, 31=9, 32=8, 33=7, 34=6, 35=4, 36=2, 37=1, 38=3, 39=5, 40=26, 41=24}

