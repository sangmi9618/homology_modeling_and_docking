
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0]

ORDER_CHANGED
[C-O*C=O:1.0]

//
FINGERPRINTS RC
[N(C)(C)C:2.0, O=CC:4.0, O=[CH]:1.0, OCC[N+](C)(C)C:1.0, OCC[N+](C)(C)C>>N(C)(C)C:1.0, OCC[N+](C)(C)C>>O=CC:3.0, OC[CH2]:2.0, OC[CH2]>>O=CC:2.0, O[CH2]:1.0, O[CH2]>>O=[CH]:1.0, [CH2]:2.0, [CH2]C[N+](C)(C)C:1.0, [CH2]C[N+](C)(C)C>>N(C)(C)C:1.0, [CH2][N+](C)(C)C:1.0, [CH2][N+](C)(C)C>>N(C)(C)C:1.0, [CH3]:1.0, [CH]:1.0, [CH]C:1.0, [N+]:1.0, [N+]CCO:1.0, [N+]CCO>>O=CC:2.0, [N+]C[CH2]:1.0, [N+]C[CH2]>>[CH]C:1.0, [N]:1.0, [OH]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[CH2]:1.0, [N+]:1.0]


ID=Reaction Center at Level: 1 (2)
[[CH2][N+](C)(C)C:1.0, [N+]C[CH2]:1.0]


ID=Reaction Center at Level: 2 (2)
[OCC[N+](C)(C)C:1.0, [CH2]C[N+](C)(C)C:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[CH2]:1.0, [CH]:1.0, [OH]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=CC:1.0, O=[CH]:1.0, OC[CH2]:1.0, O[CH2]:1.0]


ID=Reaction Center at Level: 2 (3)
[O=CC:2.0, OC[CH2]:1.0, [N+]CCO:1.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O[CH2]>>O=[CH]
2: [CH2][N+](C)(C)C>>N(C)(C)C
3: [N+]C[CH2]>>[CH]C
4: OC[CH2]>>O=CC

MMP Level 2
1: OC[CH2]>>O=CC
2: [CH2]C[N+](C)(C)C>>N(C)(C)C
3: OCC[N+](C)(C)C>>O=CC
4: [N+]CCO>>O=CC

MMP Level 3
1: [N+]CCO>>O=CC
2: OCC[N+](C)(C)C>>N(C)(C)C
3: OCC[N+](C)(C)C>>O=CC
4: OCC[N+](C)(C)C>>O=CC


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[OCC[N+](C)(C)C>>O=CC, OCC[N+](C)(C)C>>O=CC, OCC[N+](C)(C)C>>O=CC]
2: R:M00001, P:M00003	[OCC[N+](C)(C)C>>N(C)(C)C]


//
SELECTED AAM MAPPING
[OH:1][CH2:2][CH2:3][N+:4]([CH3:5])([CH3:6])[CH3:7]>>[O:1]=[CH:2][CH3:3].[N:4]([CH3:5])([CH3:6])[CH3:7]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=5, 2=4, 3=6, 4=7, 5=3, 6=2, 7=1}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=7, 2=6, 3=5, 4=2, 5=1, 6=3, 7=4}

