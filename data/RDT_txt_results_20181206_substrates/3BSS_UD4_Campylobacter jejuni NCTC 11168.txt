
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-S:1.0]

//
FINGERPRINTS RC
[O=C(NC([CH])[CH])C:1.0, O=C(N[CH])C:1.0, O=C(SCC[NH])C>>[NH]CCS:1.0, O=C(SC[CH2])C:1.0, O=C(SC[CH2])C>>O=C(NC([CH])[CH])C:1.0, O=C(SC[CH2])C>>SC[CH2]:1.0, O=C(S[CH2])C:1.0, O=C(S[CH2])C>>O=C(N[CH])C:1.0, O=C([NH])C:1.0, O=C([S])C:1.0, O=C([S])C>>O=C([NH])C:1.0, SC[CH2]:1.0, S[CH2]:1.0, [CH]C([CH])N:1.0, [CH]C([CH])N>>O=C(NC([CH])[CH])C:1.0, [CH]N:1.0, [CH]N>>[C]N[CH]:1.0, [C]:2.0, [C]N[CH]:1.0, [C]S[CH2]:1.0, [C]S[CH2]>>S[CH2]:1.0, [NH2]:1.0, [NH]:1.0, [O]C(C)C(N)C([CH])O>>[O]C(C)C(NC(=O)C)C([CH])O:1.0, [SH]:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[C]:2.0, [NH]:1.0, [S]:1.0]


ID=Reaction Center at Level: 1 (4)
[O=C([NH])C:1.0, O=C([S])C:1.0, [C]N[CH]:1.0, [C]S[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(NC([CH])[CH])C:1.0, O=C(N[CH])C:1.0, O=C(SC[CH2])C:1.0, O=C(S[CH2])C:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O=C([S])C>>O=C([NH])C
2: [C]S[CH2]>>S[CH2]
3: [CH]N>>[C]N[CH]

MMP Level 2
1: O=C(S[CH2])C>>O=C(N[CH])C
2: O=C(SC[CH2])C>>SC[CH2]
3: [CH]C([CH])N>>O=C(NC([CH])[CH])C

MMP Level 3
1: O=C(SC[CH2])C>>O=C(NC([CH])[CH])C
2: O=C(SCC[NH])C>>[NH]CCS
3: [O]C(C)C(N)C([CH])O>>[O]C(C)C(NC(=O)C)C([CH])O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C>>O=C(NCCS)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O]
2: R:M00001, P:M00004	[O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)C>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(C)C(NC(=O)C)C(O)C3NC(=O)C)C(O)C2O]
3: R:M00002, P:M00004	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(C)C(N)C(O)C3NC(=O)C)C(O)C2O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(C)C(NC(=O)C)C(O)C3NC(=O)C)C(O)C2O]


//
SELECTED AAM MAPPING
[O:52]=[C:53]1[CH:54]=[CH:55][N:56]([C:57](=[O:58])[NH:59]1)[CH:60]2[O:61][CH:62]([CH2:63][O:64][P:65](=[O:66])([OH:67])[O:68][P:69](=[O:70])([OH:71])[O:72][CH:73]3[O:74][CH:75]([CH3:76])[CH:77]([NH2:78])[CH:79]([OH:80])[CH:81]3[NH:82][C:83](=[O:84])[CH3:85])[CH:86]([OH:87])[CH:88]2[OH:89].[O:1]=[C:2]([S:3][CH2:4][CH2:5][NH:6][C:7](=[O:8])[CH2:9][CH2:10][NH:11][C:12](=[O:13])[CH:14]([OH:15])[C:16]([CH3:17])([CH3:18])[CH2:19][O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[O:28][CH2:29][CH:30]1[O:31][CH:32]([N:33]:2:[CH:34]:[N:35]:[C:36]:3:[C:37](:[N:38]:[CH:39]:[N:40]:[C:41]32)[NH2:42])[CH:43]([OH:44])[CH:45]1[O:46][P:47](=[O:48])([OH:49])[OH:50])[CH3:51]>>[O:52]=[C:53]1[CH:54]=[CH:55][N:56]([C:57](=[O:58])[NH:59]1)[CH:60]2[O:61][CH:62]([CH2:63][O:64][P:65](=[O:66])([OH:67])[O:68][P:69](=[O:70])([OH:71])[O:72][CH:73]3[O:74][CH:75]([CH3:76])[CH:77]([NH:78][C:2](=[O:1])[CH3:51])[CH:79]([OH:80])[CH:81]3[NH:82][C:83](=[O:84])[CH3:85])[CH:86]([OH:87])[CH:88]2[OH:89].[O:8]=[C:7]([NH:6][CH2:5][CH2:4][SH:3])[CH2:9][CH2:10][NH:11][C:12](=[O:13])[CH:14]([OH:15])[C:16]([CH3:17])([CH3:18])[CH2:19][O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[O:28][CH2:29][CH:30]1[O:31][CH:32]([N:33]:2:[CH:34]:[N:35]:[C:36]:3:[C:37](:[N:38]:[CH:39]:[N:40]:[C:41]32)[NH2:42])[CH:43]([OH:44])[CH:45]1[O:46][P:47](=[O:48])([OH:49])[OH:50]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=51, 2=2, 3=1, 4=3, 5=4, 6=5, 7=6, 8=7, 9=8, 10=9, 11=10, 12=11, 13=12, 14=13, 15=14, 16=16, 17=17, 18=18, 19=19, 20=20, 21=21, 22=22, 23=23, 24=24, 25=25, 26=26, 27=27, 28=28, 29=29, 30=30, 31=45, 32=43, 33=32, 34=31, 35=33, 36=34, 37=35, 38=36, 39=41, 40=40, 41=39, 42=38, 43=37, 44=42, 45=44, 46=46, 47=47, 48=48, 49=49, 50=50, 51=15, 52=76, 53=75, 54=77, 55=79, 56=81, 57=73, 58=74, 59=72, 60=69, 61=70, 62=71, 63=68, 64=65, 65=66, 66=67, 67=64, 68=63, 69=62, 70=86, 71=88, 72=60, 73=61, 74=56, 75=55, 76=54, 77=53, 78=52, 79=59, 80=57, 81=58, 82=89, 83=87, 84=82, 85=83, 86=84, 87=85, 88=80, 89=78}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=15, 2=14, 3=16, 4=17, 5=18, 6=19, 7=20, 8=21, 9=22, 10=23, 11=24, 12=25, 13=26, 14=27, 15=28, 16=43, 17=41, 18=30, 19=29, 20=31, 21=32, 22=33, 23=34, 24=39, 25=38, 26=37, 27=36, 28=35, 29=40, 30=42, 31=44, 32=45, 33=46, 34=47, 35=48, 36=12, 37=10, 38=11, 39=9, 40=8, 41=7, 42=2, 43=1, 44=3, 45=4, 46=5, 47=6, 48=13, 49=73, 50=72, 51=74, 52=79, 53=81, 54=70, 55=71, 56=69, 57=66, 58=67, 59=68, 60=65, 61=62, 62=63, 63=64, 64=61, 65=60, 66=59, 67=86, 68=88, 69=57, 70=58, 71=53, 72=52, 73=51, 74=50, 75=49, 76=56, 77=54, 78=55, 79=89, 80=87, 81=82, 82=83, 83=84, 84=85, 85=80, 86=75, 87=76, 88=77, 89=78}

