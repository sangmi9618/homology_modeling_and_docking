
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:2.0, C=O:2.0, C@C:1.0, O=O:1.0]

ORDER_CHANGED
[C-C*C@C:2.0, C=C*C@C:2.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)C(=O)C=[CH]:1.0, O=C([CH])C(=O)O:1.0, O=O:4.0, O=O>>O=C(O)C(=O)C=[CH]:1.0, O=O>>O=C([CH])C(=O)O:1.0, O=O>>[C]=O:2.0, O=O>>[C]C(=O)O:1.0, O=O>>[C]C(=O)[CH]:1.0, OC1=[C]C=CC=C1O>>[C]=CC=CC(=O)C(=O)O:1.0, [CH]:6.0, [CH]C(=[CH])C1=CC=C[C]=C1O>>[C]C=CC=C(O)C([CH])=[CH]:1.0, [CH]C(=[CH])c1cccc(O)c1O>>O=C(O)C(=O)C=[CH]:1.0, [CH]C=C(C=[CH])c1cccc(O)c1O>>[CH]C=C(C=[CH])C(O)=CC=[CH]:1.0, [CH]C=C(O)C([CH])=[CH]:1.0, [CH]C=[CH]:2.0, [CH]C=[CH]>>[CH]C=[CH]:1.0, [C]:6.0, [C]=C(O)C(O)=C[CH]:1.0, [C]=C(O)C(O)=C[CH]>>O:1.0, [C]=C(O)C(O)=C[CH]>>O=C(O)C(=O)C=[CH]:1.0, [C]=C[CH]:2.0, [C]=C[CH]>>[C]C=[CH]:1.0, [C]=O:2.0, [C]C(=O)C=C[CH]:1.0, [C]C(=O)O:2.0, [C]C(=O)[CH]:2.0, [C]C(=[CH])O:3.0, [C]C(=[CH])O>>O:1.0, [C]C(=[CH])O>>[C]C(=O)[CH]:1.0, [C]C(=[C])C=C[CH]:1.0, [C]C(=[C])C=C[CH]>>[C]C(O)=CC=[CH]:1.0, [C]C(=[C])O:1.0, [C]C(=[C])O>>[C]C(=O)O:1.0, [C]C(=[C])[CH]:1.0, [C]C(=[C])[CH]>>[C]C(=[CH])O:1.0, [C]C(O)=C(C=[CH])C([CH])=[CH]:1.0, [C]C(O)=C(C=[CH])C([CH])=[CH]>>[CH]C=C(O)C([CH])=[CH]:1.0, [C]C(O)=CC=[CH]:2.0, [C]C(O)=CC=[CH]>>[C]C(=O)C=C[CH]:1.0, [C]C([CH])=C(O)C(=[CH])O:1.0, [C]C([CH])=C(O)C(=[CH])O>>O=C([CH])C(=O)O:1.0, [C]C=1[C]=C(O)C=CC1>>[C]C(=O)C=CC=C([C])O:1.0, [C]C=CC=[C]:2.0, [C]C=CC=[C]>>[C]C=CC=[C]:1.0, [C]C=[CH]:2.0, [C]C=[CH]>>[C]=C[CH]:1.0, [C]O:1.0, [C]O>>O:1.0, [C]c1cccc(O)c1O>>O=C(O)C(=O)C=C[CH]:1.0, [OH]:1.0, [O]:4.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [C]:6.0, [OH]:2.0, [O]:4.0]


ID=Reaction Center at Level: 1 (9)
[O:1.0, O=O:2.0, [C]=O:2.0, [C]C(=O)O:1.0, [C]C(=O)[CH]:1.0, [C]C(=[CH])O:2.0, [C]C(=[C])O:1.0, [C]C(=[C])[CH]:1.0, [C]O:2.0]


ID=Reaction Center at Level: 2 (11)
[O:1.0, O=C(O)C(=O)C=[CH]:1.0, O=C([CH])C(=O)O:1.0, O=O:2.0, [CH]C=C(O)C([CH])=[CH]:1.0, [C]=C(O)C(O)=C[CH]:1.0, [C]C(=O)O:1.0, [C]C(=O)[CH]:1.0, [C]C(=[CH])O:2.0, [C]C(O)=C(C=[CH])C([CH])=[CH]:1.0, [C]C([CH])=C(O)C(=[CH])O:1.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (2)
[[CH]:6.0, [C]:4.0]


ID=Reaction Center at Level: 1 (6)
[[CH]C=[CH]:2.0, [C]=C[CH]:2.0, [C]C(=O)[CH]:1.0, [C]C(=[CH])O:2.0, [C]C(=[C])[CH]:1.0, [C]C=[CH]:2.0]


ID=Reaction Center at Level: 2 (8)
[O=C(O)C(=O)C=[CH]:1.0, [CH]C=C(O)C([CH])=[CH]:1.0, [C]=C(O)C(O)=C[CH]:1.0, [C]C(=O)C=C[CH]:1.0, [C]C(=[C])C=C[CH]:1.0, [C]C(O)=C(C=[CH])C([CH])=[CH]:1.0, [C]C(O)=CC=[CH]:2.0, [C]C=CC=[C]:2.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]C(=[C])[CH]>>[C]C(=[CH])O
2: [C]C(=[CH])O>>[C]C(=O)[CH]
3: [C]C(=[C])O>>[C]C(=O)O
4: [CH]C=[CH]>>[CH]C=[CH]
5: [C]=C[CH]>>[C]C=[CH]
6: O=O>>[C]=O
7: O=O>>[C]=O
8: [C]O>>O
9: [C]C=[CH]>>[C]=C[CH]

MMP Level 2
1: [C]C(O)=C(C=[CH])C([CH])=[CH]>>[CH]C=C(O)C([CH])=[CH]
2: [C]=C(O)C(O)=C[CH]>>O=C(O)C(=O)C=[CH]
3: [C]C([CH])=C(O)C(=[CH])O>>O=C([CH])C(=O)O
4: [C]C=CC=[C]>>[C]C=CC=[C]
5: [C]C(O)=CC=[CH]>>[C]C(=O)C=C[CH]
6: O=O>>[C]C(=O)O
7: O=O>>[C]C(=O)[CH]
8: [C]C(=[CH])O>>O
9: [C]C(=[C])C=C[CH]>>[C]C(O)=CC=[CH]

MMP Level 3
1: [CH]C=C(C=[CH])c1cccc(O)c1O>>[CH]C=C(C=[CH])C(O)=CC=[CH]
2: [C]c1cccc(O)c1O>>O=C(O)C(=O)C=C[CH]
3: [CH]C(=[CH])c1cccc(O)c1O>>O=C(O)C(=O)C=[CH]
4: [C]C=1[C]=C(O)C=CC1>>[C]C(=O)C=CC=C([C])O
5: OC1=[C]C=CC=C1O>>[C]=CC=CC(=O)C(=O)O
6: O=O>>O=C([CH])C(=O)O
7: O=O>>O=C(O)C(=O)C=[CH]
8: [C]=C(O)C(O)=C[CH]>>O
9: [CH]C(=[CH])C1=CC=C[C]=C1O>>[C]C=CC=C(O)C([CH])=[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[Oc1cccc(-c2ccccc2)c1O>>O=C(O)C(=O)C=CC=C(O)c1ccccc1, Oc1cccc(-c2ccccc2)c1O>>O=C(O)C(=O)C=CC=C(O)c1ccccc1, Oc1cccc(-c2ccccc2)c1O>>O=C(O)C(=O)C=CC=C(O)c1ccccc1, Oc1cccc(-c2ccccc2)c1O>>O=C(O)C(=O)C=CC=C(O)c1ccccc1, Oc1cccc(-c2ccccc2)c1O>>O=C(O)C(=O)C=CC=C(O)c1ccccc1, Oc1cccc(-c2ccccc2)c1O>>O=C(O)C(=O)C=CC=C(O)c1ccccc1]
2: R:M00001, P:M00004	[Oc1cccc(-c2ccccc2)c1O>>O]
3: R:M00002, P:M00003	[O=O>>O=C(O)C(=O)C=CC=C(O)c1ccccc1, O=O>>O=C(O)C(=O)C=CC=C(O)c1ccccc1]


//
SELECTED AAM MAPPING
[O:15]=[O:16].[OH:1][C:2]:1:[CH:3]:[CH:4]:[CH:5]:[C:6]([C:7]:2:[CH:8]:[CH:9]:[CH:10]:[CH:11]:[CH:12]2):[C:13]1[OH:14]>>[O:15]=[C:13]([OH:14])[C:2](=[O:16])[CH:3]=[CH:4][CH:5]=[C:6]([OH:17])[C:7]:1:[CH:8]:[CH:9]:[CH:10]:[CH:11]:[CH:12]1.[OH2:1]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=10, 2=9, 3=8, 4=7, 5=12, 6=11, 7=6, 8=13, 9=2, 10=3, 11=4, 12=5, 13=1, 14=14, 15=15, 16=16}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=14, 2=13, 3=12, 4=11, 5=16, 6=15, 7=9, 8=8, 9=7, 10=6, 11=4, 12=5, 13=2, 14=1, 15=3, 16=10, 17=17}

