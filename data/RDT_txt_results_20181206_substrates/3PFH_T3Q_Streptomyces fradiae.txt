
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:2.0, C-S:1.0]

//
FINGERPRINTS RC
[S([CH2])[CH2]:1.0, [CH2][S+]([CH2])C:2.0, [CH2][S+]([CH2])C>>S([CH2])[CH2]:1.0, [CH2][S+]([CH2])C>>[CH]N(C)C:1.0, [CH3]:2.0, [CH]C(O)C(N)C([CH])O>>[CH]C(O)C(N(C)C)C([CH])O:1.0, [CH]C([CH])N:1.0, [CH]C([CH])N(C)C:1.0, [CH]C([CH])N>>[CH]C([CH])N(C)C:1.0, [CH]CSC[CH2]:1.0, [CH]C[S+](C)C[CH2]:1.0, [CH]C[S+](C)C[CH2]>>[CH]C([CH])N(C)C:1.0, [CH]C[S+](C)C[CH2]>>[CH]CSC[CH2]:1.0, [CH]N:1.0, [CH]N(C)C:2.0, [CH]N>>[CH]N(C)C:1.0, [NH2]:1.0, [N]:1.0, [N]C:1.0, [O]C([CH])C[S+](C)CC[CH]>>[O]C([CH])CSCC[CH]:1.0, [S+]:1.0, [S+]C:1.0, [S+]C>>[N]C:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH3]:3.0, [N]:2.0, [S+]:1.0]


ID=Reaction Center at Level: 1 (4)
[[CH2][S+]([CH2])C:1.0, [CH]N(C)C:2.0, [N]C:2.0, [S+]C:1.0]


ID=Reaction Center at Level: 2 (4)
[[CH2][S+]([CH2])C:1.0, [CH]C([CH])N(C)C:2.0, [CH]C[S+](C)C[CH2]:1.0, [CH]N(C)C:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH2][S+]([CH2])C>>S([CH2])[CH2]
2: [CH]N>>[CH]N(C)C
3: [S+]C>>[N]C

MMP Level 2
1: [CH]C[S+](C)C[CH2]>>[CH]CSC[CH2]
2: [CH]C([CH])N>>[CH]C([CH])N(C)C
3: [CH2][S+]([CH2])C>>[CH]N(C)C

MMP Level 3
1: [O]C([CH])C[S+](C)CC[CH]>>[O]C([CH])CSCC[CH]
2: [CH]C(O)C(N)C([CH])O>>[CH]C(O)C(N(C)C)C([CH])O
3: [CH]C[S+](C)C[CH2]>>[CH]C([CH])N(C)C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=C(O)C(N)CCSCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=C(O)C(N)CC[S+](C)CC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(C)C(O)C(N(C)C)C3O)C(O)C2]
3: R:M00002, P:M00004	[O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(C)C(O)C(N)C3O)C(O)C2>>O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(C)C(O)C(N(C)C)C3O)C(O)C2]


//
SELECTED AAM MAPPING
[O:36]=[C:37]([OH:38])[CH:39]([NH2:40])[CH2:41][CH2:42][S+:43]([CH3:44])[CH2:45][CH:46]1[O:47][CH:48]([N:49]:2:[CH:50]:[N:51]:[C:52]:3:[C:53](:[N:54]:[CH:55]:[N:56]:[C:57]32)[NH2:58])[CH:59]([OH:60])[CH:61]1[OH:62].[O:1]=[C:2]1[NH:3][C:4](=[O:5])[N:6]([CH:7]=[C:8]1[CH3:9])[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH:23]3[O:24][CH:25]([CH3:26])[CH:27]([OH:28])[CH:29]([NH2:30])[CH:31]3[OH:32])[CH:33]([OH:34])[CH2:35]2>>[O:36]=[C:37]([OH:38])[CH:39]([NH2:40])[CH2:41][CH2:42][S:43][CH2:45][CH:46]1[O:47][CH:48]([N:49]:2:[CH:50]:[N:51]:[C:52]:3:[C:53](:[N:54]:[CH:55]:[N:56]:[C:57]32)[NH2:58])[CH:59]([OH:60])[CH:61]1[OH:62].[O:1]=[C:2]1[NH:3][C:4](=[O:5])[N:6]([CH:7]=[C:8]1[CH3:9])[CH:10]2[O:11][CH:12]([CH2:13][O:14][P:15](=[O:16])([OH:17])[O:18][P:19](=[O:20])([OH:21])[O:22][CH:23]3[O:24][CH:25]([CH3:26])[CH:27]([OH:28])[CH:29]([N:30]([CH3:44])[CH3:63])[CH:31]3[OH:32])[CH:33]([OH:34])[CH2:35]2


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=44, 2=43, 3=42, 4=41, 5=39, 6=37, 7=36, 8=38, 9=40, 10=45, 11=46, 12=61, 13=59, 14=48, 15=47, 16=49, 17=50, 18=51, 19=52, 20=57, 21=56, 22=55, 23=54, 24=53, 25=58, 26=60, 27=62, 28=26, 29=25, 30=27, 31=29, 32=31, 33=23, 34=24, 35=22, 36=19, 37=20, 38=21, 39=18, 40=15, 41=16, 42=17, 43=14, 44=13, 45=12, 46=33, 47=35, 48=10, 49=11, 50=6, 51=7, 52=8, 53=2, 54=1, 55=3, 56=4, 57=5, 58=9, 59=34, 60=32, 61=30, 62=28}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=56, 2=57, 3=58, 4=53, 5=54, 6=55, 7=59, 8=52, 9=51, 10=50, 11=49, 12=60, 13=62, 14=47, 15=48, 16=46, 17=45, 18=44, 19=43, 20=41, 21=39, 22=38, 23=40, 24=42, 25=63, 26=61, 27=26, 28=25, 29=27, 30=29, 31=33, 32=23, 33=24, 34=22, 35=19, 36=20, 37=21, 38=18, 39=15, 40=16, 41=17, 42=14, 43=13, 44=12, 45=35, 46=37, 47=10, 48=11, 49=6, 50=7, 51=8, 52=2, 53=1, 54=3, 55=4, 56=5, 57=9, 58=36, 59=34, 60=30, 61=31, 62=32, 63=28}

