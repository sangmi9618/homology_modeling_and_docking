
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0, O=O:1.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O:3.0, O=C(C)C1(O)CC[CH]C1([CH2])C:1.0, O=C(C)C1CCC([CH])C1(C)C[CH2]>>O=C(C)C1(O)CCC([CH])C1(C)C[CH2]:1.0, O=C(C)C1CC[CH]C1([CH2])C:1.0, O=C(C)C1CC[CH]C1([CH2])C>>O=C(C)C1(O)CC[CH]C1([CH2])C:1.0, O=O:4.0, O=O>>O:3.0, O=O>>O=C(C)C1(O)CC[CH]C1([CH2])C:1.0, O=O>>[C]C([C])(O)[CH2]:1.0, O=O>>[C]O:1.0, [CH]:3.0, [CH]C([CH])[CH2]:2.0, [CH]C([CH])[CH2]>>[CH]C([CH])[CH2]:1.0, [C]:1.0, [C]1=CCC2C(CCC3([CH]CCC23)C)C1([CH2])C>>[C]1CCC2C3CC=[C]C([CH2])(C)C3CCC12C:1.0, [C]C([CH2])C(C[CH])C([C])[CH2]:2.0, [C]C([CH2])C(C[CH])C([C])[CH2]>>[C]C([CH2])C(C[CH])C([C])[CH2]:1.0, [C]C([C])(O)[CH2]:2.0, [C]C([C])[CH2]:1.0, [C]C([C])[CH2]>>[C]C([C])(O)[CH2]:1.0, [C]O:1.0, [OH]:1.0, [O]:2.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [C]:1.0, [OH]:1.0, [O]:2.0]


ID=Reaction Center at Level: 1 (4)
[O:1.0, O=O:2.0, [C]C([C])(O)[CH2]:1.0, [C]O:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=C(C)C1(O)CC[CH]C1([CH2])C:1.0, O=O:2.0, [C]C([C])(O)[CH2]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH]:3.0, [C]:1.0]


ID=Reaction Center at Level: 1 (3)
[[CH]C([CH])[CH2]:2.0, [C]C([C])(O)[CH2]:1.0, [C]C([C])[CH2]:1.0]


ID=Reaction Center at Level: 2 (3)
[O=C(C)C1(O)CC[CH]C1([CH2])C:1.0, O=C(C)C1CC[CH]C1([CH2])C:1.0, [C]C([CH2])C(C[CH])C([C])[CH2]:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]C([C])[CH2]>>[C]C([C])(O)[CH2]
2: O=O>>O
3: [CH]C([CH])[CH2]>>[CH]C([CH])[CH2]
4: O=O>>[C]O

MMP Level 2
1: O=C(C)C1CC[CH]C1([CH2])C>>O=C(C)C1(O)CC[CH]C1([CH2])C
2: O=O>>O
3: [C]C([CH2])C(C[CH])C([C])[CH2]>>[C]C([CH2])C(C[CH])C([C])[CH2]
4: O=O>>[C]C([C])(O)[CH2]

MMP Level 3
1: O=C(C)C1CCC([CH])C1(C)C[CH2]>>O=C(C)C1(O)CCC([CH])C1(C)C[CH2]
2: O=O>>O
3: [C]1=CCC2C(CCC3([CH]CCC23)C)C1([CH2])C>>[C]1CCC2C3CC=[C]C([CH2])(C)C3CCC12C
4: O=O>>O=C(C)C1(O)CC[CH]C1([CH2])C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(C)C1CCC2C3CC=C4CC(O)CCC4(C)C3CCC12C>>O=C(C)C1(O)CCC2C3CC=C4CC(O)CCC4(C)C3CCC21C, O=C(C)C1CCC2C3CC=C4CC(O)CCC4(C)C3CCC12C>>O=C(C)C1(O)CCC2C3CC=C4CC(O)CCC4(C)C3CCC21C]
2: R:M00002, P:M00003	[O=O>>O=C(C)C1(O)CCC2C3CC=C4CC(O)CCC4(C)C3CCC21C]
3: R:M00002, P:M00004	[O=O>>O]


//
SELECTED AAM MAPPING
[O:24]=[O:25].[O:1]=[C:2]([CH3:3])[CH:4]1[CH2:5][CH2:6][CH:7]2[CH:8]3[CH2:9][CH:10]=[C:11]4[CH2:12][CH:13]([OH:14])[CH2:15][CH2:16][C:17]4([CH3:18])[CH:19]3[CH2:20][CH2:21][C:22]12[CH3:23]>>[O:1]=[C:2]([CH3:3])[C:4]1([OH:24])[CH2:5][CH2:6][CH:7]2[CH:8]3[CH2:9][CH:10]=[C:11]4[CH2:12][CH:13]([OH:14])[CH2:15][CH2:16][C:17]4([CH3:18])[CH:19]3[CH2:20][CH2:21][C:22]21[CH3:23].[OH2:25]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=3, 2=2, 3=1, 4=4, 5=5, 6=6, 7=7, 8=22, 9=21, 10=20, 11=19, 12=8, 13=9, 14=10, 15=11, 16=17, 17=16, 18=15, 19=13, 20=12, 21=14, 22=18, 23=23, 24=24, 25=25}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=3, 2=2, 3=1, 4=4, 5=6, 6=7, 7=8, 8=23, 9=22, 10=21, 11=20, 12=9, 13=10, 14=11, 15=12, 16=18, 17=17, 18=16, 19=14, 20=13, 21=15, 22=19, 23=24, 24=5, 25=25}

