
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O=P(O)(O)O:1.0, O=P(O)(O)O>>O=P(O)(O)OC1O[CH][CH]C1O:1.0, O=P(O)(O)O>>[O]C([CH])OP(=O)(O)O:1.0, [CH]:2.0, [C]C1=[C]N=CN1:1.0, [C]C1=[C][N]C=N1:1.0, [C]C1=[C][N]C=N1>>[C]C1=[C]N=CN1:1.0, [C]N([CH])C1O[CH][CH]C1O:1.0, [C]N([CH])C1O[CH][CH]C1O>>[P]OC1O[CH][CH]C1O:1.0, [C]N([CH])[CH]:1.0, [C]N([CH])[CH]>>[C]N=[CH]:1.0, [C]N=[CH]:2.0, [C]N=[CH]>>[C]N[CH]:1.0, [C]N[CH]:1.0, [C]c1ncn(c1N=[CH])C2O[CH][CH]C2O>>[C]c1[nH]cnc1N=[CH]:1.0, [NH]:1.0, [N]:3.0, [N]C([O])[CH]:1.0, [N]C([O])[CH]>>[O]C([O])[CH]:1.0, [N]C1=[C]N=CN1C([O])[CH]:1.0, [N]C1=[C]N=CN1C([O])[CH]>>[N]C1=[C]NC=N1:1.0, [N]C1=[C]N=CN1C2OC([CH2])C(O)C2O>>O=P(O)(O)OC1OC([CH2])C(O)C1O:1.0, [N]C1=[C]NC=N1:1.0, [N]c1c(ncn1[CH])C(=[N])N>>[N]c1nc[nH]c1C(=[N])N:1.0, [OH]:1.0, [O]:1.0, [O]C([CH])OP(=O)(O)O:1.0, [O]C([O])[CH]:1.0, [P]O:1.0, [P]O>>[P]O[CH]:1.0, [P]OC1O[CH][CH]C1O:1.0, [P]O[CH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [N]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[C]N([CH])[CH]:1.0, [N]C([O])[CH]:1.0, [O]C([O])[CH]:1.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]N([CH])C1O[CH][CH]C1O:1.0, [N]C1=[C]N=CN1C([O])[CH]:1.0, [O]C([CH])OP(=O)(O)O:1.0, [P]OC1O[CH][CH]C1O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [N]C([O])[CH]>>[O]C([O])[CH]
2: [P]O>>[P]O[CH]
3: [C]N=[CH]>>[C]N[CH]
4: [C]N([CH])[CH]>>[C]N=[CH]

MMP Level 2
1: [C]N([CH])C1O[CH][CH]C1O>>[P]OC1O[CH][CH]C1O
2: O=P(O)(O)O>>[O]C([CH])OP(=O)(O)O
3: [C]C1=[C][N]C=N1>>[C]C1=[C]N=CN1
4: [N]C1=[C]N=CN1C([O])[CH]>>[N]C1=[C]NC=N1

MMP Level 3
1: [N]C1=[C]N=CN1C2OC([CH2])C(O)C2O>>O=P(O)(O)OC1OC([CH2])C(O)C1O
2: O=P(O)(O)O>>O=P(O)(O)OC1O[CH][CH]C1O
3: [N]c1c(ncn1[CH])C(=[N])N>>[N]c1nc[nH]c1C(=[N])N
4: [C]c1ncn(c1N=[CH])C2O[CH][CH]C2O>>[C]c1[nH]cnc1N=[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>n1cnc(N)c2[nH]cnc12, OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>n1cnc(N)c2[nH]cnc12]
2: R:M00001, P:M00004	[OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OC1OC(CO)C(O)C1O]
3: R:M00002, P:M00004	[O=P(O)(O)O>>O=P(O)(O)OC1OC(CO)C(O)C1O]


//
SELECTED AAM MAPPING
[O:20]=[P:21]([OH:22])([OH:23])[OH:24].[OH:1][CH2:2][CH:3]1[O:4][CH:5]([N:6]:2:[CH:7]:[N:8]:[C:9]:3:[C:10](:[N:11]:[CH:12]:[N:13]:[C:14]32)[NH2:15])[CH:16]([OH:17])[CH:18]1[OH:19]>>[O:20]=[P:21]([OH:23])([OH:22])[O:24][CH:5]1[O:4][CH:3]([CH2:2][OH:1])[CH:18]([OH:19])[CH:16]1[OH:17].[N:13]:1:[CH:12]:[N:11]:[C:10]([NH2:15]):[C:9]:2:[NH:8]:[CH:7]:[N:6]:[C:14]12


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=12, 2=13, 3=14, 4=9, 5=10, 6=11, 7=15, 8=8, 9=7, 10=6, 11=5, 12=16, 13=18, 14=3, 15=4, 16=2, 17=1, 18=19, 19=17, 20=22, 21=21, 22=20, 23=23, 24=24}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=22, 2=23, 3=24, 4=20, 5=21, 6=18, 7=17, 8=16, 9=15, 10=19, 11=9, 12=8, 13=11, 14=13, 15=6, 16=7, 17=5, 18=2, 19=1, 20=3, 21=4, 22=14, 23=12, 24=10}

