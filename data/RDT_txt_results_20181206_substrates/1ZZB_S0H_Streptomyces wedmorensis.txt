
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%O:1.0, O-O:1.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O1[CH][CH]1:1.0, O=P(O)(O)C1OC1C:1.0, O=P(O)(O)CC(O)C:1.0, O=P(O)(O)CC(O)C>>O=P(O)(O)C1OC1C:2.0, OC([CH2])C:1.0, OC([CH2])C>>[P]C1OC1C:1.0, OO:4.0, OO>>O:3.0, [CH2]:1.0, [CH]:1.0, [CH]O:1.0, [CH]O>>O1[CH][CH]1:1.0, [OH]:3.0, [O]:1.0, [P]C1OC1C:1.0, [P]C1O[CH]1:1.0, [P]CC(O)C>>O=P(O)(O)C1OC1C:1.0, [P]C[CH]:1.0, [P]C[CH]>>[P]C1O[CH]1:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [CH]:1.0, [OH]:2.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[O:1.0, O1[CH][CH]1:1.0, OO:2.0, [P]C1O[CH]1:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=P(O)(O)C1OC1C:1.0, OO:2.0, [P]C1OC1C:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH2]:1.0, [CH]:1.0]


ID=Reaction Center at Level: 1 (2)
[[P]C1O[CH]1:1.0, [P]C[CH]:1.0]


ID=Reaction Center at Level: 2 (2)
[O=P(O)(O)C1OC1C:1.0, O=P(O)(O)CC(O)C:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: OO>>O
2: [CH]O>>O1[CH][CH]1
3: [P]C[CH]>>[P]C1O[CH]1

MMP Level 2
1: OO>>O
2: OC([CH2])C>>[P]C1OC1C
3: O=P(O)(O)CC(O)C>>O=P(O)(O)C1OC1C

MMP Level 3
1: OO>>O
2: [P]CC(O)C>>O=P(O)(O)C1OC1C
3: O=P(O)(O)CC(O)C>>O=P(O)(O)C1OC1C


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(O)CC(O)C>>O=P(O)(O)C1OC1C, O=P(O)(O)CC(O)C>>O=P(O)(O)C1OC1C]
2: R:M00002, P:M00004	[OO>>O]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[CH2:5][CH:6]([OH:7])[CH3:8].[OH:9][OH:10]>>[O:1]=[P:2]([OH:3])([OH:4])[CH:5]1[O:7][CH:6]1[CH3:8].[OH2:9]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=8, 2=6, 3=5, 4=2, 5=1, 6=3, 7=4, 8=7, 9=9, 10=10}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=8, 2=7, 3=5, 4=6, 5=2, 6=1, 7=3, 8=4, 9=9}

