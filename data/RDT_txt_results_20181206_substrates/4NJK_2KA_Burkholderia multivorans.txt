
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%N:2.0, C@C:1.0]

//
FINGERPRINTS RC
[N:3.0, O=C(O)C1N[C]=C([NH])NC1>>[C]c1c([NH])[nH]cc1C(=O)O:1.0, [CH2]:1.0, [CH]:2.0, [CH]C[NH]:1.0, [CH]C[NH]>>[C]=C[NH]:1.0, [C]:3.0, [C]=C[NH]:1.0, [C]C(=[C])NC([C])[CH2]:1.0, [C]C(=[C])NC([C])[CH2]>>N:1.0, [C]C(=[C])[NH]:1.0, [C]C(=[C])[NH]>>[C]C([C])=[C]:1.0, [C]C([C])=[CH]:1.0, [C]C([C])=[C]:1.0, [C]C([NH])[CH2]:1.0, [C]C([NH])[CH2]>>[C]C([C])=[CH]:1.0, [C]C1=[C]NC=C1C(=O)O:1.0, [C]C1=[C]NCC(N1)C(=O)O>>[N]C(=O)c1c([NH])[nH]cc1C(=O)O:1.0, [C]C1NC=2C(=O)N=[C]NC2NC1>>O=C1N=[C]Nc2[nH]cc(c12)C(=O)O:1.0, [C]C=1[C]=[C]NC1:1.0, [C]NC(C(=O)O)C[NH]:1.0, [C]NC(C(=O)O)C[NH]>>[C]C1=[C]NC=C1C(=O)O:1.0, [C]NCC([C])[NH]:1.0, [C]NCC([C])[NH]>>[C]C=1[C]=[C]NC1:1.0, [C]N[CH]:1.0, [C]N[CH]>>N:1.0, [C]c1c[nH]c([NH])c1C([N])=O:1.0, [NH]:1.0, [N]C(=O)C(N[CH])=C([NH])[NH]:1.0, [N]C(=O)C(N[CH])=C([NH])[NH]>>[C]c1c[nH]c([NH])c1C([N])=O:1.0, [N]C(=O)C=1NC(C(=O)O)CNC1[NH]>>N:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[N:1.0, [CH]:1.0, [C]:3.0, [NH]:2.0]


ID=Reaction Center at Level: 1 (6)
[N:1.0, [C]C(=[C])[NH]:1.0, [C]C([C])=[CH]:1.0, [C]C([C])=[C]:1.0, [C]C([NH])[CH2]:1.0, [C]N[CH]:2.0]


ID=Reaction Center at Level: 2 (6)
[N:1.0, [C]C(=[C])NC([C])[CH2]:2.0, [C]C1=[C]NC=C1C(=O)O:1.0, [C]NC(C(=O)O)C[NH]:1.0, [C]c1c[nH]c([NH])c1C([N])=O:1.0, [N]C(=O)C(N[CH])=C([NH])[NH]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [CH]C[NH]>>[C]=C[NH]
2: [C]C([NH])[CH2]>>[C]C([C])=[CH]
3: [C]N[CH]>>N
4: [C]C(=[C])[NH]>>[C]C([C])=[C]

MMP Level 2
1: [C]NCC([C])[NH]>>[C]C=1[C]=[C]NC1
2: [C]NC(C(=O)O)C[NH]>>[C]C1=[C]NC=C1C(=O)O
3: [C]C(=[C])NC([C])[CH2]>>N
4: [N]C(=O)C(N[CH])=C([NH])[NH]>>[C]c1c[nH]c([NH])c1C([N])=O

MMP Level 3
1: O=C(O)C1N[C]=C([NH])NC1>>[C]c1c([NH])[nH]cc1C(=O)O
2: [C]C1=[C]NCC(N1)C(=O)O>>[N]C(=O)c1c([NH])[nH]cc1C(=O)O
3: [N]C(=O)C=1NC(C(=O)O)CNC1[NH]>>N
4: [C]C1NC=2C(=O)N=[C]NC2NC1>>O=C1N=[C]Nc2[nH]cc(c12)C(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=c1nc(N)[nH]c2NCC(Nc12)C(=O)O>>N]
2: R:M00001, P:M00003	[O=c1nc(N)[nH]c2NCC(Nc12)C(=O)O>>O=c1nc(N)[nH]c2[nH]cc(c12)C(=O)O, O=c1nc(N)[nH]c2NCC(Nc12)C(=O)O>>O=c1nc(N)[nH]c2[nH]cc(c12)C(=O)O, O=c1nc(N)[nH]c2NCC(Nc12)C(=O)O>>O=c1nc(N)[nH]c2[nH]cc(c12)C(=O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]=2[NH:8][CH2:9][CH:10]([NH:11][C:12]12)[C:13](=[O:14])[OH:15]>>[O:14]=[C:13]([OH:15])[C:10]:1:[CH:9]:[NH:8]:[C:7]:2[NH:6][C:4](=[N:3][C:2](=[O:1])[C:12]21)[NH2:5].[NH3:11]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=9, 2=10, 3=11, 4=12, 5=7, 6=8, 7=6, 8=4, 9=3, 10=2, 11=1, 12=5, 13=13, 14=14, 15=15}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=15, 2=5, 3=4, 4=13, 5=7, 6=6, 7=8, 8=9, 9=10, 10=11, 11=12, 12=14, 13=2, 14=1, 15=3}

