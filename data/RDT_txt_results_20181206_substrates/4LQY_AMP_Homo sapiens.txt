
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O:3.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]:1.0, O=P(O)(O[P])O[CH2]:1.0, O=P(O)(O[P])O[CH2]>>O=P(O)(O)O[CH2]:1.0, O>>O=P(O)(O)O[CH2]:1.0, O>>[O]P(=O)(O)O:1.0, O>>[P]O:1.0, [OH]:2.0, [O]:1.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)OP(=O)(O)OC[CH]>>O=P(O)(O)OC[CH]:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0, [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [O]P([O])(=O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, [O]P(=O)(O)O:1.0, [O]P([O])(=O)O:1.0, [P]O:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O[P])O[CH2]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OP([O])(=O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[P]>>[P]O
2: O>>[P]O
3: [O]P([O])(=O)O>>[O]P(=O)(O)O

MMP Level 2
1: [O]P(=O)(O)OP([O])(=O)O>>[O]P(=O)(O)O
2: O>>[O]P(=O)(O)O
3: O=P(O)(O[P])O[CH2]>>O=P(O)(O)O[CH2]

MMP Level 3
1: O=P(O)(O[P])OP(=O)(O)O[CH2]>>O=P(O)(O)O[P]
2: O>>O=P(O)(O)O[CH2]
3: [O]P(=O)(O)OP(=O)(O)OC[CH]>>O=P(O)(O)OC[CH]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=P(O)(OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O)OP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
2: R:M00001, P:M00004	[O=P(O)(OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O)OP(=O)(O)OP(=O)(O)OCC4OC(n5cnc6c(ncnc65)N)C(O)C4O>>O=P(O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]
3: R:M00002, P:M00004	[O>>O=P(O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]


//
SELECTED AAM MAPPING
[O:29]=[P:28]([OH:30])([O:31][CH2:32][CH:33]1[O:34][CH:35]([N:36]:2:[CH:37]:[N:38]:[C:39]:3:[C:40](:[N:41]:[CH:42]:[N:43]:[C:44]32)[NH2:45])[CH:46]([OH:47])[CH:48]1[OH:49])[O:27][P:24](=[O:25])([OH:26])[O:23][P:2](=[O:1])([OH:3])[O:4][CH2:5][CH:6]4[O:7][CH:8]([N:9]:5:[CH:10]:[N:11]:[C:12]:6:[C:13](:[N:14]:[CH:15]:[N:16]:[C:17]65)[NH2:18])[CH:19]([OH:20])[CH:21]4[OH:22].[OH2:50]>>[O:1]=[P:2]([OH:50])([OH:3])[O:4][CH2:5][CH:6]1[O:7][CH:8]([N:9]:2:[CH:10]:[N:11]:[C:12]:3:[C:13](:[N:14]:[CH:15]:[N:16]:[C:17]32)[NH2:18])[CH:19]([OH:20])[CH:21]1[OH:22].[O:25]=[P:24]([OH:23])([OH:26])[O:27][P:28](=[O:29])([OH:30])[O:31][CH2:32][CH:33]1[O:34][CH:35]([N:36]:2:[CH:37]:[N:38]:[C:39]:3:[C:40](:[N:41]:[CH:42]:[N:43]:[C:44]32)[NH2:45])[CH:46]([OH:47])[CH:48]1[OH:49]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=42, 2=43, 3=44, 4=39, 5=40, 6=41, 7=45, 8=38, 9=37, 10=36, 11=35, 12=46, 13=48, 14=33, 15=34, 16=32, 17=31, 18=28, 19=29, 20=30, 21=27, 22=24, 23=25, 24=26, 25=23, 26=2, 27=1, 28=3, 29=4, 30=5, 31=6, 32=21, 33=19, 34=8, 35=7, 36=9, 37=10, 38=11, 39=12, 40=17, 41=16, 42=15, 43=14, 44=13, 45=18, 46=20, 47=22, 48=49, 49=47, 50=50}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=20, 2=21, 3=22, 4=17, 5=18, 6=19, 7=23, 8=16, 9=15, 10=14, 11=13, 12=24, 13=26, 14=11, 15=12, 16=10, 17=9, 18=6, 19=7, 20=8, 21=5, 22=2, 23=1, 24=3, 25=4, 26=27, 27=25, 28=43, 29=44, 30=45, 31=40, 32=41, 33=42, 34=46, 35=39, 36=38, 37=37, 38=36, 39=47, 40=49, 41=34, 42=35, 43=33, 44=32, 45=29, 46=28, 47=30, 48=31, 49=50, 50=48}

