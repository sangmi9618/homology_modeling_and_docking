
//
FINGERPRINTS BC

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[[CH]:2.0, [CH]C([CH])O:2.0, [CH]C([CH])O>>[CH]C([CH])O:1.0, [NH]C1[CH]OC(CO)C(O)C1O>>[NH]C1[CH]OC(CO)C(O)C1O:1.0, [O]C([CH2])C(O)C([CH])O:2.0, [O]C([CH2])C(O)C([CH])O>>[O]C([CH2])C(O)C([CH])O:1.0]

//
Reaction Centre Formed/Cleaved

Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[CH]:2.0]


ID=Reaction Center at Level: 1 (1)
[[CH]C([CH])O:2.0]


ID=Reaction Center at Level: 2 (1)
[[O]C([CH2])C(O)C([CH])O:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: [CH]C([CH])O>>[CH]C([CH])O

MMP Level 2
1: [O]C([CH2])C(O)C([CH])O>>[O]C([CH2])C(O)C([CH])O

MMP Level 3
1: [NH]C1[CH]OC(CO)C(O)C1O>>[NH]C1[CH]OC(CO)C(O)C1O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00002	[O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3NC(=O)C)C(O)C2O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)OP(=O)(O)OC3OC(CO)C(O)C(O)C3NC(=O)C)C(O)C2O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][CH:22]3[O:23][CH:24]([CH2:25][OH:26])[CH:27]([OH:28])[CH:29]([OH:30])[CH:31]3[NH:32][C:33](=[O:34])[CH3:35])[CH:36]([OH:37])[CH:38]2[OH:39]>>[O:1]=[C:2]1[CH:3]=[CH:4][N:5]([C:6](=[O:7])[NH:8]1)[CH:9]2[O:10][CH:11]([CH2:12][O:13][P:14](=[O:15])([OH:16])[O:17][P:18](=[O:19])([OH:20])[O:21][CH:22]3[O:23][CH:24]([CH2:25][OH:26])[CH:27]([OH:28])[CH:29]([OH:30])[CH:31]3[NH:32][C:33](=[O:34])[CH3:35])[CH:36]([OH:37])[CH:38]2[OH:39]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=35, 2=33, 3=34, 4=32, 5=31, 6=29, 7=27, 8=24, 9=23, 10=22, 11=21, 12=18, 13=19, 14=20, 15=17, 16=14, 17=15, 18=16, 19=13, 20=12, 21=11, 22=36, 23=38, 24=9, 25=10, 26=5, 27=4, 28=3, 29=2, 30=1, 31=8, 32=6, 33=7, 34=39, 35=37, 36=25, 37=26, 38=28, 39=30}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=35, 2=33, 3=34, 4=32, 5=31, 6=29, 7=27, 8=24, 9=23, 10=22, 11=21, 12=18, 13=19, 14=20, 15=17, 16=14, 17=15, 18=16, 19=13, 20=12, 21=11, 22=36, 23=38, 24=9, 25=10, 26=5, 27=4, 28=3, 29=2, 30=1, 31=8, 32=6, 33=7, 34=39, 35=37, 36=25, 37=26, 38=28, 39=30}

