
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[CH2]>>O=P(O)(O)OP(=O)(O)O[CH2]:1.0, O=P(O)(O)O[P]:2.0, O=P(O)(O)O[P]>>O=P(O)(O)O[P]:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:4.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:2.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O:1.0, [P]:2.0, [P]O:2.0, [P]O>>[P]O[P]:1.0, [P]O[P]:2.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (2)
[[O]P(=O)(O)O:2.0, [P]O[P]:2.0]


ID=Reaction Center at Level: 2 (2)
[O=P(O)(O)O[P]:2.0, [O]P(=O)(O)OP(=O)(O)O:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O>>[P]O[P]
2: [P]O[P]>>[P]O
3: [O]P(=O)(O)O>>[O]P(=O)(O)O

MMP Level 2
1: [O]P(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O
2: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
3: O=P(O)(O)O[P]>>O=P(O)(O)O[P]

MMP Level 3
1: O=P(O)(O)O[CH2]>>O=P(O)(O)OP(=O)(O)O[CH2]
2: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
3: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)OP(=O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)O)C(O)C2>>O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)OP(=O)(O)O)C(O)C2]
2: R:M00002, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=c1[nH]c(=O)n(cc1C)C2OC(COP(=O)(O)OP(=O)(O)O)C(O)C2]
3: R:M00002, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]


//
SELECTED AAM MAPPING
[O:32]=[C:33]1[NH:34][C:35](=[O:36])[N:37]([CH:38]=[C:39]1[CH3:40])[CH:41]2[O:42][CH:43]([CH2:44][O:45][P:46](=[O:47])([OH:48])[OH:49])[CH:50]([OH:51])[CH2:52]2.[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]>>[O:32]=[C:33]1[NH:34][C:35](=[O:36])[N:37]([CH:38]=[C:39]1[CH3:40])[CH:41]2[O:42][CH:43]([CH2:44][O:45][P:46](=[O:47])([OH:48])[O:49][P:2](=[O:1])([OH:3])[OH:4])[CH:50]([OH:51])[CH2:52]2.[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=40, 2=39, 3=38, 4=37, 5=35, 6=36, 7=34, 8=33, 9=32, 10=41, 11=52, 12=50, 13=43, 14=42, 15=44, 16=45, 17=46, 18=47, 19=48, 20=49, 21=51, 22=24, 23=25, 24=26, 25=21, 26=22, 27=23, 28=27, 29=20, 30=19, 31=18, 32=17, 33=28, 34=30, 35=15, 36=16, 37=14, 38=13, 39=10, 40=11, 41=12, 42=9, 43=6, 44=7, 45=8, 46=5, 47=2, 48=1, 49=3, 50=4, 51=31, 52=29}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=36, 2=35, 3=34, 4=33, 5=31, 6=32, 7=30, 8=29, 9=28, 10=37, 11=52, 12=50, 13=39, 14=38, 15=40, 16=41, 17=42, 18=43, 19=44, 20=45, 21=46, 22=47, 23=48, 24=49, 25=51, 26=20, 27=21, 28=22, 29=17, 30=18, 31=19, 32=23, 33=16, 34=15, 35=14, 36=13, 37=24, 38=26, 39=11, 40=12, 41=10, 42=9, 43=6, 44=7, 45=8, 46=5, 47=2, 48=1, 49=3, 50=4, 51=27, 52=25}

