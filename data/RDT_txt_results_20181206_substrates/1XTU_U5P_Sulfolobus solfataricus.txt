
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-N:1.0, C-O:1.0]

//
FINGERPRINTS RC
[O=C([NH])NC=[CH]:1.0, O=C([NH])NC=[CH]>>[O]C([CH])N(C=[CH])C(=O)[NH]:1.0, O=C1N[C]C=CN1>>O=C1N[C]C=CN1C2O[CH][CH]C2O:1.0, O=P(O)(O[P])OC1O[CH][CH]C1O>>O=P(O)(O)O[P]:1.0, [CH]:2.0, [C]N([CH])C1O[CH][CH]C1O:1.0, [C]N([CH])[CH]:1.0, [C]N[CH]:1.0, [C]N[CH]>>[C]N([CH])[CH]:1.0, [NH]:1.0, [N]:1.0, [N]C([O])[CH]:1.0, [OH]:1.0, [O]:1.0, [O]C([CH])N(C=[CH])C(=O)[NH]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O:1.0, [O]C([O])[CH]:1.0, [O]C([O])[CH]>>[N]C([O])[CH]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)OC1OC([CH2])C(O)C1O>>O=C([NH])N(C=[CH])C1OC([CH2])C(O)C1O:1.0, [P]O:1.0, [P]OC1O[CH][CH]C1O:1.0, [P]OC1O[CH][CH]C1O>>[C]N([CH])C1O[CH][CH]C1O:1.0, [P]O[CH]:1.0, [P]O[CH]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (3)
[[CH]:2.0, [N]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (4)
[[C]N([CH])[CH]:1.0, [N]C([O])[CH]:1.0, [O]C([O])[CH]:1.0, [P]O[CH]:1.0]


ID=Reaction Center at Level: 2 (4)
[[C]N([CH])C1O[CH][CH]C1O:1.0, [O]C([CH])N(C=[CH])C(=O)[NH]:1.0, [O]C([CH])OP([O])(=O)O:1.0, [P]OC1O[CH][CH]C1O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [P]O[CH]>>[P]O
2: [O]C([O])[CH]>>[N]C([O])[CH]
3: [C]N[CH]>>[C]N([CH])[CH]

MMP Level 2
1: [O]C([CH])OP([O])(=O)O>>[O]P(=O)(O)O
2: [P]OC1O[CH][CH]C1O>>[C]N([CH])C1O[CH][CH]C1O
3: O=C([NH])NC=[CH]>>[O]C([CH])N(C=[CH])C(=O)[NH]

MMP Level 3
1: O=P(O)(O[P])OC1O[CH][CH]C1O>>O=P(O)(O)O[P]
2: [O]P(=O)(O)OC1OC([CH2])C(O)C1O>>O=C([NH])N(C=[CH])C1OC([CH2])C(O)C1O
3: O=C1N[C]C=CN1>>O=C1N[C]C=CN1C2O[CH][CH]C2O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=c1cc[nH]c(=O)[nH]1>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)O)C(O)C2O]
2: R:M00002, P:M00003	[O=P(O)(O)OCC1OC(OP(=O)(O)OP(=O)(O)O)C(O)C1O>>O=c1ccn(c(=O)[nH]1)C2OC(COP(=O)(O)O)C(O)C2O]
3: R:M00002, P:M00004	[O=P(O)(O)OCC1OC(OP(=O)(O)OP(=O)(O)O)C(O)C1O>>O=P(O)(O)OP(=O)(O)O]


//
SELECTED AAM MAPPING
[O:23]=[C:24]1[CH:25]=[CH:26][NH:27][C:28](=[O:29])[NH:30]1.[O:1]=[P:2]([OH:3])([OH:4])[O:5][CH2:6][CH:7]1[O:8][CH:9]([O:10][P:11](=[O:12])([OH:13])[O:14][P:15](=[O:16])([OH:17])[OH:18])[CH:19]([OH:20])[CH:21]1[OH:22]>>[O:23]=[C:24]1[CH:25]=[CH:26][N:27]([C:28](=[O:29])[NH:30]1)[CH:9]2[O:8][CH:7]([CH2:6][O:5][P:2](=[O:1])([OH:3])[OH:4])[CH:21]([OH:22])[CH:19]2[OH:20].[O:12]=[P:11]([OH:10])([OH:13])[O:14][P:15](=[O:16])([OH:17])[OH:18]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=25, 2=26, 3=27, 4=28, 5=29, 6=30, 7=24, 8=23, 9=6, 10=7, 11=21, 12=19, 13=9, 14=8, 15=10, 16=11, 17=12, 18=13, 19=14, 20=15, 21=16, 22=17, 23=18, 24=20, 25=22, 26=5, 27=2, 28=1, 29=3, 30=4}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=3, 2=4, 3=5, 4=6, 5=7, 6=8, 7=2, 8=1, 9=9, 10=20, 11=18, 12=11, 13=10, 14=12, 15=13, 16=14, 17=15, 18=16, 19=17, 20=19, 21=21, 22=24, 23=23, 24=22, 25=25, 26=26, 27=27, 28=28, 29=29, 30=30}

