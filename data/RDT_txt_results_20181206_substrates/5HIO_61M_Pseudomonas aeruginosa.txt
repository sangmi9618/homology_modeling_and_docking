
//
FINGERPRINTS BC

FORMED_CLEAVED
[C-O:1.0, C-S:1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)[CH2]:2.0, O=C(SC[CH2])[CH2]:1.0, O=C(SC[CH2])[CH2]>>SC[CH2]:1.0, O=C([S])[CH2]:1.0, O=C([S])[CH2]>>O=C(O)[CH2]:1.0, O>>O=C(O)[CH2]:1.0, O>>[C]CC(=O)O:1.0, O>>[C]O:1.0, SC[CH2]:1.0, S[CH2]:1.0, [C]:2.0, [C]C(=O)CC(=O)SC[CH2]>>[C]C(=O)CC(=O)O:1.0, [C]CC(=O)O:1.0, [C]CC(=O)SCC[NH]>>[NH]CCS:1.0, [C]CC(=O)S[CH2]:1.0, [C]CC(=O)S[CH2]>>[C]CC(=O)O:1.0, [C]O:1.0, [C]S[CH2]:1.0, [C]S[CH2]>>S[CH2]:1.0, [OH]:1.0, [SH]:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [C]:2.0, [OH]:1.0, [S]:1.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=C(O)[CH2]:1.0, O=C([S])[CH2]:1.0, [C]O:1.0, [C]S[CH2]:1.0]


ID=Reaction Center at Level: 2 (5)
[O:1.0, O=C(O)[CH2]:1.0, O=C(SC[CH2])[CH2]:1.0, [C]CC(=O)O:1.0, [C]CC(=O)S[CH2]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [C]S[CH2]>>S[CH2]
2: O=C([S])[CH2]>>O=C(O)[CH2]
3: O>>[C]O

MMP Level 2
1: O=C(SC[CH2])[CH2]>>SC[CH2]
2: [C]CC(=O)S[CH2]>>[C]CC(=O)O
3: O>>O=C(O)[CH2]

MMP Level 3
1: [C]CC(=O)SCC[NH]>>[NH]CCS
2: [C]C(=O)CC(=O)SC[CH2]>>[C]C(=O)CC(=O)O
3: O>>[C]CC(=O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)CC(=O)c4ccccc4N>>O=C(O)CC(=O)c1ccccc1N]
2: R:M00001, P:M00004	[O=C(SCCNC(=O)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O)CC(=O)c4ccccc4N>>O=C(NCCS)CCNC(=O)C(O)C(C)(C)COP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1OP(=O)(O)O]
3: R:M00002, P:M00003	[O>>O=C(O)CC(=O)c1ccccc1N]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([S:3][CH2:4][CH2:5][NH:6][C:7](=[O:8])[CH2:9][CH2:10][NH:11][C:12](=[O:13])[CH:14]([OH:15])[C:16]([CH3:17])([CH3:18])[CH2:19][O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[O:28][CH2:29][CH:30]1[O:31][CH:32]([N:33]:2:[CH:34]:[N:35]:[C:36]:3:[C:37](:[N:38]:[CH:39]:[N:40]:[C:41]32)[NH2:42])[CH:43]([OH:44])[CH:45]1[O:46][P:47](=[O:48])([OH:49])[OH:50])[CH2:51][C:52](=[O:53])[C:54]:4:[CH:55]:[CH:56]:[CH:57]:[CH:58]:[C:59]4[NH2:60].[OH2:61]>>[O:1]=[C:2]([OH:61])[CH2:51][C:52](=[O:53])[C:54]:1:[CH:55]:[CH:56]:[CH:57]:[CH:58]:[C:59]1[NH2:60].[O:8]=[C:7]([NH:6][CH2:5][CH2:4][SH:3])[CH2:9][CH2:10][NH:11][C:12](=[O:13])[CH:14]([OH:15])[C:16]([CH3:17])([CH3:18])[CH2:19][O:20][P:21](=[O:22])([OH:23])[O:24][P:25](=[O:26])([OH:27])[O:28][CH2:29][CH:30]1[O:31][CH:32]([N:33]:2:[CH:34]:[N:35]:[C:36]:3:[C:37](:[N:38]:[CH:39]:[N:40]:[C:41]32)[NH2:42])[CH:43]([OH:44])[CH:45]1[O:46][P:47](=[O:48])([OH:49])[OH:50]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=17, 2=16, 3=18, 4=19, 5=20, 6=21, 7=22, 8=23, 9=24, 10=25, 11=26, 12=27, 13=28, 14=29, 15=30, 16=45, 17=43, 18=32, 19=31, 20=33, 21=34, 22=35, 23=36, 24=41, 25=40, 26=39, 27=38, 28=37, 29=42, 30=44, 31=46, 32=47, 33=48, 34=49, 35=50, 36=14, 37=12, 38=13, 39=11, 40=10, 41=9, 42=7, 43=8, 44=6, 45=5, 46=4, 47=3, 48=2, 49=1, 50=51, 51=52, 52=53, 53=54, 54=55, 55=56, 56=57, 57=58, 58=59, 59=60, 60=15, 61=61}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=57, 2=58, 3=59, 4=60, 5=55, 6=56, 7=53, 8=54, 9=52, 10=50, 11=49, 12=51, 13=61, 14=15, 15=14, 16=16, 17=17, 18=18, 19=19, 20=20, 21=21, 22=22, 23=23, 24=24, 25=25, 26=26, 27=27, 28=28, 29=43, 30=41, 31=30, 32=29, 33=31, 34=32, 35=33, 36=34, 37=39, 38=38, 39=37, 40=36, 41=35, 42=40, 43=42, 44=44, 45=45, 46=46, 47=47, 48=48, 49=12, 50=10, 51=11, 52=9, 53=8, 54=7, 55=2, 56=1, 57=3, 58=4, 59=5, 60=6, 61=13}

