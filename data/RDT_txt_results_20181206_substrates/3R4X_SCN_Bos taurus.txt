
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-O:1.0, O-S:1.0]

//
FINGERPRINTS RC
[N#CS[O-]:1.0, N#C[S-]:1.0, N#C[S-]>>N#CS[O-]:2.0, O:3.0, OO:4.0, OO>>N#CS[O-]:1.0, OO>>O:3.0, OO>>[C]S[O-]:1.0, OO>>[O-][S]:1.0, [C]S[O-]:2.0, [C][S-]:1.0, [C][S-]>>[C]S[O-]:1.0, [O-]:1.0, [O-][S]:1.0, [OH]:2.0, [S-]:1.0, [S]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [O-]:1.0, [OH]:2.0, [S]:1.0]


ID=Reaction Center at Level: 1 (4)
[O:1.0, OO:2.0, [C]S[O-]:1.0, [O-][S]:1.0]


ID=Reaction Center at Level: 2 (4)
[N#CS[O-]:1.0, O:1.0, OO:2.0, [C]S[O-]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: OO>>[O-][S]
2: [C][S-]>>[C]S[O-]
3: OO>>O

MMP Level 2
1: OO>>[C]S[O-]
2: N#C[S-]>>N#CS[O-]
3: OO>>O

MMP Level 3
1: OO>>N#CS[O-]
2: N#C[S-]>>N#CS[O-]
3: OO>>O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[N#C[S-]>>N#CS[O-]]
2: R:M00002, P:M00003	[OO>>N#CS[O-]]
3: R:M00002, P:M00004	[OO>>O]


//
SELECTED AAM MAPPING
[N:1]#[C:2][S-:3].[OH:4][OH:5]>>[N:1]#[C:2][S:3][O-:4].[OH2:5]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=2, 2=1, 3=3, 4=4, 5=5}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=2, 2=1, 3=3, 4=4, 5=5}

