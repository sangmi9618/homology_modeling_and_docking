
//
FINGERPRINTS BC

FORMED_CLEAVED
[N#N:1.0]

ORDER_CHANGED
[N-O*N=O:1.0]

//
FINGERPRINTS RC
[N#[N+][O-]:3.0, [N+]:1.0, [N+][O-]:1.0, [N]:1.0, [N]=O:4.0, [N]=O>>N#[N+][O-]:5.0, [N]=O>>[N+][O-]:1.0, [O-]:1.0, [O]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[N+]:1.0, [N]:1.0]


ID=Reaction Center at Level: 1 (2)
[N#[N+]:1.0, N#[N+][O-]:1.0]


ID=Reaction Center at Level: 2 (1)
[N#[N+][O-]:2.0]


Reaction Centre Order Changed

ID=Reaction Center at Level: 0 (4)
[[N+]:1.0, [N]:1.0, [O-]:1.0, [O]:1.0]


ID=Reaction Center at Level: 1 (3)
[N#[N+][O-]:1.0, [N+][O-]:1.0, [N]=O:2.0]


ID=Reaction Center at Level: 2 (2)
[N#[N+][O-]:2.0, [N]=O:2.0]


Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: [N]=O>>N#[N+][O-]
2: [N]=O>>[N+][O-]

MMP Level 2
1: [N]=O>>N#[N+][O-]
2: [N]=O>>N#[N+][O-]

MMP Level 3
1: [N]=O>>N#[N+][O-]
2: [N]=O>>N#[N+][O-]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[[N]=O>>N#[N+][O-], [N]=O>>N#[N+][O-]]


//
SELECTED AAM MAPPING
[O:2]=[NH:1]>>[N:3]#[N+:1][O-:2].[OH2:4]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=1, 2=2}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=4, 2=1, 3=2, 4=3}

