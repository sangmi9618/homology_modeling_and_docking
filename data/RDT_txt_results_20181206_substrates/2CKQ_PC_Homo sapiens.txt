
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

//
FINGERPRINTS RC
[O=P(O)(O)OC[CH2]:1.0, O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[P]:1.0, O=P(O)(O)O[P]>>O=P(O)(O)O[CH2]:1.0, OC[CH2]:1.0, OC[CH2]>>O=P(O)(O)OC[CH2]:1.0, O[CH2]:1.0, O[CH2]>>[P]O[CH2]:1.0, [N+]CCO>>[N+]CCOP(=O)(O)O:1.0, [OH]:2.0, [O]:2.0, [O]P(=O)(O)O:3.0, [O]P(=O)(O)O>>[O]P(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0, [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC[CH2]:1.0, [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[P]:1.0, [P]O[P]>>[P]O:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (2)
[[O]:2.0, [P]:2.0]


ID=Reaction Center at Level: 1 (3)
[[O]P(=O)(O)O:2.0, [P]O[CH2]:1.0, [P]O[P]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=P(O)(O)OC[CH2]:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[P]:1.0, [O]P(=O)(O)OP(=O)(O)O:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes


//
TRANSFORMATIONS
MMP Level 1
1: O[CH2]>>[P]O[CH2]
2: [P]O[P]>>[P]O
3: [O]P(=O)(O)O>>[O]P(=O)(O)O

MMP Level 2
1: OC[CH2]>>O=P(O)(O)OC[CH2]
2: [O]P(=O)(O)OP(=O)(O)O>>[O]P(=O)(O)O
3: O=P(O)(O)O[P]>>O=P(O)(O)O[CH2]

MMP Level 3
1: [N+]CCO>>[N+]CCOP(=O)(O)O
2: O=P(O)(O)OP(=O)(O)O[P]>>O=P(O)(O)O[P]
3: [O]P(=O)(O)OP(=O)(O)O>>O=P(O)(O)OC[CH2]


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[OCC[N+](C)(C)C>>O=P(O)(O)OCC[N+](C)(C)C]
2: R:M00002, P:M00003	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OCC[N+](C)(C)C]
3: R:M00002, P:M00004	[O=P(O)(O)OP(=O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O>>O=P(O)(O)OP(=O)(O)OCC1OC(n2cnc3c(ncnc32)N)C(O)C1O]


//
SELECTED AAM MAPPING
[O:1]=[P:2]([OH:3])([OH:4])[O:5][P:6](=[O:7])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31].[OH:32][CH2:33][CH2:34][N+:35]([CH3:36])([CH3:37])[CH3:38]>>[O:1]=[P:2]([OH:3])([OH:4])[O:32][CH2:33][CH2:34][N+:35]([CH3:36])([CH3:37])[CH3:38].[O:7]=[P:6]([OH:5])([OH:8])[O:9][P:10](=[O:11])([OH:12])[O:13][CH2:14][CH:15]1[O:16][CH:17]([N:18]:2:[CH:19]:[N:20]:[C:21]:3:[C:22](:[N:23]:[CH:24]:[N:25]:[C:26]32)[NH2:27])[CH:28]([OH:29])[CH:30]1[OH:31]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=36, 2=35, 3=37, 4=38, 5=34, 6=33, 7=32, 8=24, 9=25, 10=26, 11=21, 12=22, 13=23, 14=27, 15=20, 16=19, 17=18, 18=17, 19=28, 20=30, 21=15, 22=16, 23=14, 24=13, 25=10, 26=11, 27=12, 28=9, 29=6, 30=7, 31=8, 32=5, 33=2, 34=1, 35=3, 36=4, 37=31, 38=29}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=36, 2=35, 3=37, 4=38, 5=34, 6=33, 7=32, 8=29, 9=28, 10=30, 11=31, 12=20, 13=21, 14=22, 15=17, 16=18, 17=19, 18=23, 19=16, 20=15, 21=14, 22=13, 23=24, 24=26, 25=11, 26=12, 27=10, 28=9, 29=6, 30=7, 31=8, 32=5, 33=2, 34=1, 35=3, 36=4, 37=27, 38=25}

