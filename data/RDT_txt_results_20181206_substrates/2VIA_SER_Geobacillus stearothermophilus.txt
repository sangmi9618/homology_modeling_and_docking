
//
FINGERPRINTS BC

FORMED_CLEAVED
[C%N:2.0, C-C:1.0, C-O:1.0]

STEREO_CHANGED
[C(R/S):2.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)C(N)CO:1.0, O=C(O)C(N)CO>>O=C(O)CN:2.0, O=C(O)C(N)CO>>[C]C(=[C])N1CN(C([CH])=[CH])CC1[CH2]:1.0, O=C(O)CN:1.0, O[CH2]:1.0, O[CH2]>>O:1.0, [CH2]:3.0, [CH]:3.0, [CH]C(=[CH])N1C[N][CH]C1:1.0, [CH]C(=[CH])NC[CH]:1.0, [CH]C(=[CH])NC[CH]>>[CH]C(=[CH])N1C[N][CH]C1:1.0, [CH]C=C(C=[CH])NCC([NH])[CH2]>>[C]N1CN(C(=C[CH])C=[CH])CC1[CH2]:1.0, [CH]CO:2.0, [CH]CO>>O:1.0, [CH]CO>>[N]C[N]:1.0, [C]C(=[C])N1C[N]CC1[CH2]:1.0, [C]C(=[C])NC([CH2])[CH2]:1.0, [C]C(=[C])NC([CH2])[CH2]>>[C]C(=[C])N1C[N]CC1[CH2]:1.0, [C]C(N)CO:1.0, [C]C(N)CO>>O:1.0, [C]C(N)CO>>[C]N1[CH]CN([C])C1:1.0, [C]C([CH2])N:1.0, [C]C([CH2])N>>[C]CN:1.0, [C]CN:1.0, [C]N([CH2])[CH2]:1.0, [C]N([CH])[CH2]:1.0, [C]N1C[N]CC1C[NH]:1.0, [C]N1[CH]CN([C])C1:1.0, [C]NC(C[NH])C[NH]:1.0, [C]NC(C[NH])C[NH]>>[C]N1C[N]CC1C[NH]:1.0, [C]NCC1NC([C])=[C]NC1>>[C]C1=[C]NCC2N1CN([C])C2:1.0, [C]N[CH2]:1.0, [C]N[CH2]>>[C]N([CH2])[CH2]:1.0, [C]N[CH]:1.0, [C]N[CH]>>[C]N([CH])[CH2]:1.0, [NH]:2.0, [NH]C([CH2])[CH2]:1.0, [NH]C([CH2])[CH2]>>[N]C([CH2])[CH2]:1.0, [N]:2.0, [N]C(=O)C=1NC(CNC1[NH])C[NH]>>[C]N1CN2C(=C([NH])NCC2C1)C([N])=O:1.0, [N]C([CH2])[CH2]:1.0, [N]C[N]:1.0, [OH]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (5)
[O:1.0, [CH2]:4.0, [CH]:1.0, [N]:2.0, [OH]:1.0]


ID=Reaction Center at Level: 1 (7)
[O:1.0, O[CH2]:1.0, [CH]CO:2.0, [C]C([CH2])N:1.0, [C]N([CH2])[CH2]:1.0, [C]N([CH])[CH2]:1.0, [N]C[N]:2.0]


ID=Reaction Center at Level: 2 (7)
[O:1.0, O=C(O)C(N)CO:1.0, [CH]C(=[CH])N1C[N][CH]C1:1.0, [CH]CO:1.0, [C]C(=[C])N1C[N]CC1[CH2]:1.0, [C]C(N)CO:2.0, [C]N1[CH]CN([C])C1:2.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (2)
[[CH2]:1.0, [CH]:3.0]


ID=Reaction Center at Level: 1 (4)
[[C]C([CH2])N:1.0, [C]CN:1.0, [NH]C([CH2])[CH2]:1.0, [N]C([CH2])[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[O=C(O)C(N)CO:1.0, O=C(O)CN:1.0, [C]N1C[N]CC1C[NH]:1.0, [C]NC(C[NH])C[NH]:1.0]



//
TRANSFORMATIONS
MMP Level 1
1: [C]N[CH2]>>[C]N([CH2])[CH2]
2: [NH]C([CH2])[CH2]>>[N]C([CH2])[CH2]
3: [CH]CO>>[N]C[N]
4: O[CH2]>>O
5: [C]C([CH2])N>>[C]CN
6: [C]N[CH]>>[C]N([CH])[CH2]

MMP Level 2
1: [CH]C(=[CH])NC[CH]>>[CH]C(=[CH])N1C[N][CH]C1
2: [C]NC(C[NH])C[NH]>>[C]N1C[N]CC1C[NH]
3: [C]C(N)CO>>[C]N1[CH]CN([C])C1
4: [CH]CO>>O
5: O=C(O)C(N)CO>>O=C(O)CN
6: [C]C(=[C])NC([CH2])[CH2]>>[C]C(=[C])N1C[N]CC1[CH2]

MMP Level 3
1: [CH]C=C(C=[CH])NCC([NH])[CH2]>>[C]N1CN(C(=C[CH])C=[CH])CC1[CH2]
2: [C]NCC1NC([C])=[C]NC1>>[C]C1=[C]NCC2N1CN([C])C2
3: O=C(O)C(N)CO>>[C]C(=[C])N1CN(C([CH])=[CH])CC1[CH2]
4: [C]C(N)CO>>O
5: O=C(O)C(N)CO>>O=C(O)CN
6: [N]C(=O)C=1NC(CNC1[NH])C[NH]>>[C]N1CN2C(=C([NH])NCC2C1)C([N])=O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C(N)CO>>O=C(O)CN]
2: R:M00001, P:M00004	[O=C(O)C(N)CO>>O=c1nc(N)[nH]c2NCC3N(c12)CN(c4ccc(cc4)C(=O)NC(C(=O)O)CCC(=O)O)C3]
3: R:M00001, P:M00005	[O=C(O)C(N)CO>>O]
4: R:M00002, P:M00004	[O=c1nc(N)[nH]c2NCC(Nc12)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)O>>O=c1nc(N)[nH]c2NCC3N(c12)CN(c4ccc(cc4)C(=O)NC(C(=O)O)CCC(=O)O)C3, O=c1nc(N)[nH]c2NCC(Nc12)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)O>>O=c1nc(N)[nH]c2NCC3N(c12)CN(c4ccc(cc4)C(=O)NC(C(=O)O)CCC(=O)O)C3, O=c1nc(N)[nH]c2NCC(Nc12)CNc3ccc(cc3)C(=O)NC(C(=O)O)CCC(=O)O>>O=c1nc(N)[nH]c2NCC3N(c12)CN(c4ccc(cc4)C(=O)NC(C(=O)O)CCC(=O)O)C3]


//
SELECTED AAM MAPPING
[O:31]=[C:30]([OH:32])[CH2:29][CH2:28][CH:24]([NH:23][C:21](=[O:22])[C:18]:1:[CH:17]:[CH:16]:[C:15](:[CH:20]:[CH:19]1)[NH:14][CH2:13][CH:10]2[NH:11][C:12]=3[C:2](=[O:1])[N:3]=[C:4]([NH2:5])[NH:6][C:7]3[NH:8][CH2:9]2)[C:25](=[O:26])[OH:27].[O:33]=[C:34]([OH:35])[CH:36]([NH2:37])[CH2:38][OH:39]>>[O:1]=[C:2]1[N:3]=[C:4]([NH2:5])[NH:6][C:7]=2[NH:8][CH2:9][CH:10]3[N:11]([C:12]12)[CH2:38][N:14]([C:15]:4:[CH:16]:[CH:17]:[C:18](:[CH:19]:[CH:20]4)[C:21](=[O:22])[NH:23][CH:24]([C:25](=[O:26])[OH:27])[CH2:28][CH2:29][C:30](=[O:31])[OH:32])[CH2:13]3.[O:33]=[C:34]([OH:35])[CH2:36][NH2:37].[OH2:39]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=38, 2=36, 3=34, 4=33, 5=35, 6=37, 7=39, 8=9, 9=10, 10=11, 11=12, 12=7, 13=8, 14=6, 15=4, 16=3, 17=2, 18=1, 19=5, 20=13, 21=14, 22=15, 23=16, 24=17, 25=18, 26=19, 27=20, 28=21, 29=22, 30=23, 31=24, 32=28, 33=29, 34=30, 35=31, 36=32, 37=25, 38=26, 39=27}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=37, 2=35, 3=34, 4=36, 5=38, 6=9, 7=10, 8=33, 9=14, 10=13, 11=11, 12=12, 13=7, 14=8, 15=6, 16=4, 17=3, 18=2, 19=1, 20=5, 21=15, 22=16, 23=17, 24=18, 25=19, 26=20, 27=21, 28=22, 29=23, 30=24, 31=28, 32=29, 33=30, 34=31, 35=32, 36=25, 37=26, 38=27, 39=39}

