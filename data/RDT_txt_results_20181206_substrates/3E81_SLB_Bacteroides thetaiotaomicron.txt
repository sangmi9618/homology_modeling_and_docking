
//
FINGERPRINTS BC

FORMED_CLEAVED
[O-P:2.0]

STEREO_CHANGED
[C(R/S):1.0]

//
FINGERPRINTS RC
[O:3.0, O=C(O)C(O)(O[CH])C[CH]:2.0, O=C(O)C(O)(O[CH])C[CH]>>O=C(O)C(O)(O[CH])C[CH]:1.0, O=C(O)C1(O)OC([CH])[CH]C(O)C1>>O=C(O)C1(O)OC([CH])[CH]C(O)C1:1.0, O=P(O)(O)O:3.0, O=P(O)(O)OCC([CH])O>>[CH]C(O)CO:1.0, O=P(O)(O)OC[CH]:1.0, O=P(O)(O)OC[CH]>>O=P(O)(O)O:1.0, O=P(O)(O)OC[CH]>>[CH]CO:1.0, O=P(O)(O)O[CH2]:1.0, O=P(O)(O)O[CH2]>>O=P(O)(O)O:1.0, O>>O=P(O)(O)O:2.0, O>>[P]O:1.0, O[CH2]:1.0, [CH]CO:1.0, [C]:2.0, [C]C([O])(O)[CH2]:2.0, [C]C([O])(O)[CH2]>>[C]C([O])(O)[CH2]:1.0, [OH]:2.0, [O]:1.0, [O]P(=O)(O)O:1.0, [O]P(=O)(O)O>>O=P(O)(O)O:1.0, [P]:2.0, [P]O:1.0, [P]O[CH2]:1.0, [P]O[CH2]>>O[CH2]:1.0]

//
Reaction Centre Formed/Cleaved

ID=Reaction Center at Level: 0 (4)
[O:1.0, [OH]:1.0, [O]:1.0, [P]:2.0]


ID=Reaction Center at Level: 1 (5)
[O:1.0, O=P(O)(O)O:1.0, [O]P(=O)(O)O:1.0, [P]O:1.0, [P]O[CH2]:1.0]


ID=Reaction Center at Level: 2 (4)
[O:1.0, O=P(O)(O)O:2.0, O=P(O)(O)OC[CH]:1.0, O=P(O)(O)O[CH2]:1.0]


Reaction Centre Order Changed

Reaction Centre Stereo Changes

ID=Reaction Center at Level: 0 (1)
[[C]:2.0]


ID=Reaction Center at Level: 1 (1)
[[C]C([O])(O)[CH2]:2.0]


ID=Reaction Center at Level: 2 (1)
[O=C(O)C(O)(O[CH])C[CH]:2.0]



//
TRANSFORMATIONS
MMP Level 1
1: O>>[P]O
2: [P]O[CH2]>>O[CH2]
3: [C]C([O])(O)[CH2]>>[C]C([O])(O)[CH2]
4: [O]P(=O)(O)O>>O=P(O)(O)O

MMP Level 2
1: O>>O=P(O)(O)O
2: O=P(O)(O)OC[CH]>>[CH]CO
3: O=C(O)C(O)(O[CH])C[CH]>>O=C(O)C(O)(O[CH])C[CH]
4: O=P(O)(O)O[CH2]>>O=P(O)(O)O

MMP Level 3
1: O>>O=P(O)(O)O
2: O=P(O)(O)OCC([CH])O>>[CH]C(O)CO
3: O=C(O)C1(O)OC([CH])[CH]C(O)C1>>O=C(O)C1(O)OC([CH])[CH]C(O)C1
4: O=P(O)(O)OC[CH]>>O=P(O)(O)O


//
REACTION MMP (RPAIR)
1: R:M00001, P:M00003	[O=C(O)C1(O)OC(C(NC(=O)C)C(O)C1)C(O)C(O)COP(=O)(O)O>>O=C(O)C1(O)OC(C(NC(=O)C)C(O)C1)C(O)C(O)CO, O=C(O)C1(O)OC(C(NC(=O)C)C(O)C1)C(O)C(O)COP(=O)(O)O>>O=C(O)C1(O)OC(C(NC(=O)C)C(O)C1)C(O)C(O)CO]
2: R:M00001, P:M00004	[O=C(O)C1(O)OC(C(NC(=O)C)C(O)C1)C(O)C(O)COP(=O)(O)O>>O=P(O)(O)O]
3: R:M00002, P:M00004	[O>>O=P(O)(O)O]


//
SELECTED AAM MAPPING
[O:1]=[C:2]([OH:3])[C:4]1([OH:5])[O:6][CH:7]([CH:8]([OH:9])[CH:10]([OH:11])[CH2:12][O:13][P:14](=[O:15])([OH:16])[OH:17])[CH:18]([NH:19][C:20](=[O:21])[CH3:22])[CH:23]([OH:24])[CH2:25]1.[OH2:26]>>[O:1]=[C:2]([OH:3])[C:4]1([OH:5])[O:6][CH:7]([CH:8]([OH:9])[CH:10]([OH:11])[CH2:12][OH:13])[CH:18]([NH:19][C:20](=[O:21])[CH3:22])[CH:23]([OH:24])[CH2:25]1.[O:15]=[P:14]([OH:17])([OH:26])[OH:16]


//
REACTANT INPUT ATOM INDEX<->AAM ID
{1=22, 2=20, 3=21, 4=19, 5=18, 6=23, 7=25, 8=4, 9=6, 10=7, 11=8, 12=10, 13=12, 14=13, 15=14, 16=15, 17=16, 18=17, 19=11, 20=9, 21=2, 22=1, 23=3, 24=5, 25=24, 26=26}
PRODUCT INPUT ATOM INDEX<->AAM ID
{1=18, 2=16, 3=17, 4=15, 5=14, 6=19, 7=21, 8=4, 9=6, 10=7, 11=8, 12=10, 13=12, 14=13, 15=11, 16=9, 17=2, 18=1, 19=3, 20=5, 21=20, 22=24, 23=23, 24=22, 25=25, 26=26}

