# homology_modeling_and_docking
Automatic computational pipeline for high-throughout protein modeling and ligand–protein docking simulation. 
This work is based on MELI-3D, MODELLER, and AutoDock Vina.

### MELI-3D 
Mechanism-based Enzyme-Ligand Interactions in 3D structures (MELI-3D) is an automatic computational pipeline for enzyme selection.
MELI-3D provides a shortlist for feasible enzyme gene sequences, which catalyze the similar reactions to the query orphan reaction.

### How to install
---
### Requirements:
An Anaconda python environment is required for easy installation of third-party programs.
```
- python==3.6.7
- autodock-vina==1.1.2
- mgltoos==1.5.6
- modeller==9.21
- rdkit==2018.09.1.0
- openbabel=2.4.1
- chemvae==1.0.0
- biopython==1.72
```
It works on Linux and has been tested on Ubuntu 16.04.6 LTS.
### homology_modeling_and_docking
1.Clone the repository
```
git clone git clone https://sangmi9618@bitbucket.org/sangmi9618/homology_modeling_and_docking.git
```
2.Create and activate a conda environment
```
conda env create -f environment.yml
source activate meli3d
```
---
Initial settings for third-party programs and structure datasets (Modeller, Chemical VAE, MGLTools, and BioLiP)
### Modeller
1.Obtain the Modeller license key (Modeller is available free of charge only to academic non-profit institutions)

(<https://salilab.org/modeller/registration.html>)

2.Edit /[YOUR_HOME_PATH]/anaconda3/envs/meli3d/lib/modeller-9.21/modlib/modeller/config.py

Replace XXXX with the Modeller license key
### Chemical VAE
Install Chemical VAE
```
cd /meli3d/bin/chemical_vae
python setup.py install
```
### MGLTools
1.Edit /[YOUR_HOME_PATH]/anaconda3/pkgs/mgltools-1.5.6-1/bin/pythonsh (in line 9)

Replace MGL_ROOT="/opt/anaconda1anaconda2anaconda3" with **MGL_ROOT="[YOUR_HOME_PATH]/anaconda3/pkgs/mgltools-1.5.6-1/"**

2.Edit run_meli3d.py (in line 115)

Replace MGLTools_DIR="/XXXX/anaconda3/pkgs/mgltools-1.5.6-1/" with **MGLTools_DIR="/[YOUR_HOME_PATH]/anaconda3/pkgs/mgltools-1.5.6-1/"**
### BioLiP download
```
cd /meli3d/data
./download_biolip_datasets.pl
```
BioLiP datasets (3D structures of enzymes and ligands) will be automatically downloaded at BioLiP_updated_set folder (~3hr)

### Implementation
```
create csv file in input directory (heder: ligand_name, ligand_smiles, pdb_id)
e.g.) 4-Hydroxybenzaldehyde, C1=CC(=CC=C1C=O)O, 1iep
python run_multi.py 
```



